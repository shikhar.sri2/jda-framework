//package com.jda.dct.e2e.ui.supply;
//
//import BaseTest;
//import LeftMenu;
//import TopMenuPage;
//import TopSubMenuPage;
//import org.openqa.selenium.WebDriver;
//import org.testng.Assert;
//import org.testng.annotations.BeforeClass;
//import org.testng.annotations.Test;
//
//public class SupplyStatesTests extends BaseTest {
//
//
//    private LeftMenu leftMenu;
//    private TopMenuPage topMenu;
//    private TopSubMenuPage topSubMenu;
//    private WebDriver driver;
//
//    @BeforeClass
//    public void setUp() throws InterruptedException {
//        System.out.println("Starting tests for :" + this.getClass().getName());
////Initialize required objects
//        driver = getDriver();
//
//        leftMenu = new LeftMenu(driver);
//        topMenu = new TopMenuPage(driver);
//        topSubMenu = new TopSubMenuPage(driver);
//
//        //loginAndNavigateToSupplyShipmentLandingPage
//        logInToApplication();
//        leftMenu.clickSupplyMenuOption();
//    }
//
//
//    @Test(priority = 80, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
//    public void validateStatesStatusPlannedShipmentTC80(String shipmentNo80a, String shipmentNo80b)
//    {
//        topMenu.goToSupplyShipmentsTab();
//        topSubMenu.clickRoadIcon();
//        topMenu.clickStatesTab();
//        topMenu.clickSupplyShipmentStatesPlannedShipment(); //PlannedShipment
//        topSubMenu.clickSearchButton();
//        topSubMenu.goToSearchFeature(shipmentNo80a);
//        topMenu.assertActualAndExpectedSupplyShipmentNumberEqual(shipmentNo80b);
//        Assert.assertTrue(topMenu.isSupplyShipmentPlannedShipmentVisible());
//        topMenu.clearFilter();
//        driver.navigate().refresh();
//    }
//
//
//    @Test(priority = 81, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
//    public void validateStatesStatusShippedTC81(String shipmentNo81a, String shipmentNo81b)
//    {
//        topMenu.goToSupplyShipmentsTab();
//        topSubMenu.clickRoadIcon();
//        topMenu.clickStatesTab();
//        topMenu.clickSupplyShipmentStatesShipped();  //Shipped 81
//        topSubMenu.clickSearchButton();
//        topSubMenu.goToSearchFeature(shipmentNo81a);
//        topMenu.assertActualAndExpectedSupplyShipmentNumberEqual(shipmentNo81b);
//        Assert.assertTrue(topMenu.isSupplyShipmentShippedVisible());
//        topMenu.clearFilter();
//        driver.navigate().refresh();
//
//    }
//    @Test(priority = 83, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
//    public void validateStatesStatusDeliveredTC83(String shipmentNo83a, String shipmentNo83b)
//    {
//        topMenu.goToSupplyShipmentsTab();
//        topSubMenu.clickRoadIcon();
//        topMenu.clickStatesTab();
//        topMenu.clickSupplyShipmentStatesDelivered(); //Delivered  83
//        topSubMenu.clickSearchButton();
//        topSubMenu.goToSearchFeature(shipmentNo83a);
//        topMenu.assertActualAndExpectedSupplyShipmentNumberEqual(shipmentNo83b);
//        Assert.assertTrue(topMenu.isSupplyShipmentDeliveredVisible());
//        topMenu.clearFilter();
//        driver.navigate().refresh();
//
//    }
//
//    @Test(priority = 84, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
//    public void validateStatesStatusReceivedTC84(String shipmentNo84a, String shipmentNo84b) throws InterruptedException {
//        topMenu.goToSupplyShipmentsTab();
//        topSubMenu.clickRoadIcon();
//        topMenu.clickStatesTab();
//        topSubMenu.clickDemandOrderSliderIcon();  // Slider Icon
//        topMenu.clickSupplyShipmentStatesReceived();//Delivered  84
//        topSubMenu.clickSearchButton();
//        topSubMenu.goToSearchFeature(shipmentNo84a);
//        topMenu.assertActualAndExpectedSupplyShipmentNumberEqual(shipmentNo84b);
//        Assert.assertTrue(topMenu.isSupplyShipmentReceivedVisible());
//        topMenu.clearFilter();
//        driver.navigate().refresh();
//
//    }
//    @Test(priority = 85, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
//    public void validateStatesStatusPlannedShipmentTC85(String shipmentNo85a, String shipmentNo85b)
//    {
//        topMenu.goToSupplyShipmentsTab();
//        topSubMenu.clickRoadIcon();
//        topMenu.clickStatesTab();
//        topMenu.clickSupplyShipmentStatesPlannedShipment();
//        topSubMenu.clickSearchButton();
//        topSubMenu.goToSearchFeature(shipmentNo85a);
//        topMenu.assertActualAndExpectedSupplyShipmentNumberEqual(shipmentNo85b);
//        Assert.assertTrue(topMenu.isSupplyShipmentPlannedShipmentVisible());
//        topMenu.clearFilter();
//        driver.navigate().refresh();
//    }
//
//    @Test(priority = 86, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
//    public void validateStatesStatusShippedTC86(String shipmentNo86a, String shipmentNo86b)
//    {
//        topMenu.goToSupplyShipmentsTab();
//        topSubMenu.clickRoadIcon();
//        topMenu.clickStatesTab();
//        topMenu.clickSupplyShipmentStatesShipped();
//        topSubMenu.clickSearchButton();
//        topSubMenu.goToSearchFeature(shipmentNo86a);
//        topMenu.assertActualAndExpectedSupplyShipmentNumberEqual(shipmentNo86b);
//        Assert.assertTrue(topMenu.isSupplyShipmentShippedVisible());
//        topMenu.clearFilter();
//        driver.navigate().refresh();
//    }
//
//    @Test(priority = 88, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
//    public void validateStatesStatusDeliveredTC88(String shipmentNo88a, String shipmentNo88b)
//    {
//        topMenu.goToSupplyShipmentsTab();
//        topSubMenu.clickRoadIcon();
//        topMenu.clickStatesTab();
//        topMenu.clickSupplyShipmentStatesDelivered();
//        topSubMenu.clickSearchButton();
//        topSubMenu.goToSearchFeature(shipmentNo88a);
//        topMenu.assertActualAndExpectedSupplyShipmentNumberEqual(shipmentNo88b);
//        Assert.assertTrue(topMenu.isSupplyShipmentDeliveredVisible());
//        topMenu.clearFilter();
//        driver.navigate().refresh();
//    }
//
//    @Test(priority = 90, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
//    public void validateStatesStatusReceivedTC90(String shipmentNo90a, String shipmentNo90b) throws InterruptedException {
//        topMenu.goToSupplyDeliveriesTab();
//        topSubMenu.clickRoadIcon();
//        topMenu.clickStatesTab();
//        topMenu.clickSupplyDeliveryStatesReceived();
//        topSubMenu.clickSearchButton();
//        topSubMenu.goToSearchFeature(shipmentNo90a);
//        topMenu.assertActualAndExpectedDemandDeliveryNumberEqualOcean(shipmentNo90b);
//        Assert.assertTrue(topMenu.isSupplyDeliveryReceivedVisible());
//        topMenu.clearFilter();
//        driver.navigate().refresh();
//    }
//
//    @Test(priority = 91, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
//    public void validateStatesStatusPlannedShipmentTC91(String shipmentNo91a, String shipmentNo91b) throws InterruptedException {
//        topMenu.goToSupplyDeliveriesTab();
//        topSubMenu.clickRoadIcon();
//        topMenu.clickStatesTab();
//        topMenu.clickSupplyDeliveryStatesPlannedShipment();
//        topSubMenu.clickSearchButton();
//        topSubMenu.goToSearchFeature(shipmentNo91a);
//        topMenu.assertActualAndExpectedDemandDeliveryNumberEqualOcean(shipmentNo91b);
//        Assert.assertTrue(topMenu.isSupplyDeliveryShippedVisible());
//        topMenu.clearFilter();
//        driver.navigate().refresh();
//    }
//
//
//    @Test(priority = 92, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
//    public void validateStatesStatusReceivedTC92(String shipmentNo92a, String shipmentNo92b) throws InterruptedException {
//        topMenu.goToSupplyDeliveriesTab();
//        topSubMenu.clickRoadIcon();
//        topMenu.clickStatesTab();
//        topMenu.clickSupplyDeliveryStatesReceived();
//        topSubMenu.clickSearchButton();
//        topSubMenu.goToSearchFeature(shipmentNo92a);
//        topMenu.assertActualAndExpectedDemandDeliveryNumberEqualOcean(shipmentNo92b);
//        Assert.assertTrue(topMenu.isSupplyDeliveryShippedVisible());
//        topMenu.clearFilter();
//        driver.navigate().refresh();
//    }
//
//// Note Elements no longer appears
////    @Test(priority = 93, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////    public void validatingSearchFeatureCreatedSSTC93(String SSTC93)
//=======
////package com.jda.dct.e2e.testscripts.uitests.supply;
////
////import BaseTest;
////import LeftMenu;
////import TopMenuPage;
////import TopSubMenuPage;
////import org.openqa.selenium.WebDriver;
////import org.testng.Assert;
////import org.testng.annotations.BeforeClass;
////import org.testng.annotations.Test;
////
////public class SupplyStatesTests extends BaseTest {
////
////
////    private LeftMenu leftMenu;
////    private TopMenuPage topMenu;
////    private TopSubMenuPage topSubMenu;
////    private WebDriver driver;
////
////    @BeforeClass
////    public void setUp() throws InterruptedException {
////        System.out.println("Starting tests for :" + this.getClass().getName());
//////Initialize required objects
////        driver = getDriver();
////
////        leftMenu = new LeftMenu(driver);
////        topMenu = new TopMenuPage(driver);
////        topSubMenu = new TopSubMenuPage(driver);
////
////        //loginAndNavigateToSupplyShipmentLandingPage
////        logInToApplication();
////        leftMenu.clickSupplyMenuOption();
////    }
////
////
////    @Test(priority = 79, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////    public void validateStatesStatusNewTC79(String shipmentNo79a, String shipmentNo79b)
////    {
////        topMenu.goToSupplyShipmentsTab();
////        topSubMenu.clickOceanIcon();
////        topMenu.clickStatesTab();
////        topMenu.clickSupplyShipmentStatesNew();             //New
////        topSubMenu.clickSearchButton();
////        topSubMenu.goToSearchFeature(shipmentNo79a);
////        Assert.assertTrue(topMenu.isSupplyShipmentPlannedShipmentVisible());
////      //  topMenu.assertActualAndExpectedInventoryNumberEqual(shipmentNo79b);
////        topMenu.clearFilter();
////        driver.navigate().refresh();
////    }
////
////
////    @Test(priority = 80, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////    public void validateStatesStatusPlannedShipmentTC80(String shipmentNo80a, String shipmentNo80b)
////    {
////        topMenu.goToSupplyShipmentsTab();
////        topSubMenu.clickOceanIcon();
////
////        topMenu.clickStatesTab();
////        topMenu.clickSupplyShipmentStatesPlannedShipment(); //PlannedShipment
////        topSubMenu.clickSearchButton();
////        topSubMenu.goToSearchFeature(shipmentNo80a);
////        topMenu.assertActualAndExpectedSupplyShipmentNumberEqualOcean(shipmentNo80b);
////        Assert.assertTrue(topMenu.isSupplyShipmentPlannedShipmentVisible());
////        topMenu.clearFilter();
////        driver.navigate().refresh();
////    }
////
////    @Test(priority = 81, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////    public void validateStatesStatusShippedTC81(String shipmentNo81a, String shipmentNo81b)
////    {
////        topMenu.goToSupplyShipmentsTab();
////        topSubMenu.clickOceanIcon();
////        topMenu.clickStatesTab();
////
////        topMenu.clickSupplyShipmentStatesShipped();  //Shipped 81
////
////        topSubMenu.clickSearchButton();
////        topSubMenu.goToSearchFeature(shipmentNo81a);
////        topMenu.assertActualAndExpectedSupplyShipmentNumberEqualOcean(shipmentNo81b);
////        Assert.assertTrue(topMenu.isSupplyShipmentShippedVisible());
////        topMenu.clearFilter();
////        driver.navigate().refresh();
////
////    }
////
////    @Test(priority = 82, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////    public void validateStatesStatusInTransitTC82(String shipmentNo82a, String shipmentNo82b)
////    {
////        topMenu.goToSupplyShipmentsTab();
////        topSubMenu.clickOceanIcon();
////
////        topMenu.clickStatesTab();
////        topMenu.clickSupplyShipmentStatesInTransit(); //InTransit
////        topSubMenu.clickSearchButton();
////        topSubMenu.goToSearchFeature(shipmentNo82a);
////        topMenu.assertActualAndExpectedSupplyShipmentNumberEqualOcean(shipmentNo82b);
////        Assert.assertTrue(topMenu.isSupplyShipmentPlannedShipmentVisible());
////
////        topMenu.clearFilter();
////        driver.navigate().refresh();
////
////    }
////
////    @Test(priority = 83, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////    public void validateStatesStatusDeliveredTC83(String shipmentNo83a, String shipmentNo83b)
////    {
////        topMenu.goToSupplyShipmentsTab();
////        topSubMenu.clickOceanIcon();
////
////        topMenu.clickStatesTab();
////        topMenu.clickSupplyShipmentStatesDelivered(); //Delivered  83
////        topSubMenu.clickSearchButton();
////        topSubMenu.goToSearchFeature(shipmentNo83a);
////        topMenu.assertActualAndExpectedSupplyShipmentNumberEqualOcean(shipmentNo83b);
////        Assert.assertTrue(topMenu.isSupplyShipmentDeliveredVisible());
////
////        topMenu.clearFilter();
////        driver.navigate().refresh();
////
////    }
////
////    @Test(priority = 84, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////    public void validateStatesStatusReceivedTC84(String shipmentNo84a, String shipmentNo84b) throws InterruptedException {
////        topMenu.goToSupplyShipmentsTab();
////        topSubMenu.clickOceanIcon();
////
////        topMenu.clickStatesTab();
////        topSubMenu.clickDemandOrderSliderIcon();  // Slider Icon
////        topMenu.clickSupplyShipmentStatesReceived();//Delivered  84
////        topSubMenu.clickSearchButton();
////        topSubMenu.goToSearchFeature(shipmentNo84a);
////        topMenu.assertActualAndExpectedSupplyShipmentNumberEqualOcean(shipmentNo84b);
////        Assert.assertTrue(topMenu.isSupplyShipmentReceivedVisible());
////        topMenu.clearFilter();
////        driver.navigate().refresh();
////
////    }
////
////    @Test(priority = 85, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////    public void validateStatesStatusPlannedShipmentTC85(String shipmentNo85a, String shipmentNo85b)
////    {
////        topMenu.goToSupplyShipmentsTab();
////        topSubMenu.clickOceanIcon();
////
////        topMenu.clickStatesTab();
////        topMenu.clickSupplyShipmentStatesPlannedShipment();
////        topSubMenu.clickSearchButton();
////        topSubMenu.goToSearchFeature(shipmentNo85a);
////        topMenu.assertActualAndExpectedSupplyShipmentNumberEqualOcean(shipmentNo85b);
////        Assert.assertTrue(topMenu.isSupplyShipmentPlannedShipmentVisible());
////        topMenu.clearFilter();
////        driver.navigate().refresh();
////    }
////
////    @Test(priority = 86, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////    public void validateStatesStatusShippedTC86(String shipmentNo86a, String shipmentNo86b)
////    {
////        topMenu.goToSupplyShipmentsTab();
////        topSubMenu.clickOceanIcon();
////
////        topMenu.clickStatesTab();
////
////        topMenu.clickSupplyShipmentStatesShipped();
////        topSubMenu.clickSearchButton();
////        topSubMenu.goToSearchFeature(shipmentNo86a);
////        topMenu.assertActualAndExpectedSupplyShipmentNumberEqualOcean(shipmentNo86b);
////        Assert.assertTrue(topMenu.isSupplyShipmentShippedVisible());
////
////        topMenu.clearFilter();
////        driver.navigate().refresh();
////
////    }
////
////
////    @Test(priority = 87, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////    public void validateStatesStatusInTransitTC87(String shipmentNo87a, String shipmentNo87b)
////    {
////        topMenu.goToSupplyShipmentsTab();
////        topSubMenu.clickOceanIcon();
////
////        topMenu.clickStatesTab();
////        topMenu.clickSupplyShipmentStatesInTransit();
////        topSubMenu.clickSearchButton();
////        topSubMenu.goToSearchFeature(shipmentNo87a);
////        topMenu.assertActualAndExpectedSupplyShipmentNumberEqualOcean(shipmentNo87b);
////        Assert.assertTrue(topMenu.isSupplyShipmentInTransitVisible());
////
////        topMenu.clearFilter();
////        driver.navigate().refresh();
////
////
////    }
////
////    @Test(priority = 88, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////    public void validateStatesStatusDeliveredTC88(String shipmentNo88a, String shipmentNo88b)
////    {
////        topMenu.goToSupplyShipmentsTab();
////        topSubMenu.clickOceanIcon();
////
////        topMenu.clickStatesTab();
////        topMenu.clickSupplyShipmentStatesDelivered();
////        topSubMenu.clickSearchButton();
////        topSubMenu.goToSearchFeature(shipmentNo88a);
////        topMenu.assertActualAndExpectedSupplyShipmentNumberEqualOcean(shipmentNo88b);
////        Assert.assertTrue(topMenu.isSupplyShipmentDeliveredVisible());
////
////        topMenu.clearFilter();
////        driver.navigate().refresh();
////    }
////
////    @Test(priority = 89, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////    public void validateStatesStatusInTransitTC89(String shipmentNo89a, String shipmentNo89b) throws InterruptedException {
////        topMenu.goToSupplyDeliveriesTab();
////        topSubMenu.clickOceanIcon();
////
////        topMenu.clickStatesTab();
////        topMenu.clickSupplyDeliveryStatesDelivered();
////        topSubMenu.clickSearchButton();
////        topSubMenu.goToSearchFeature(shipmentNo89a);
////        topMenu.assertActualAndExpectedDemandDeliveryNumberEqualOcean(shipmentNo89b);
////        Assert.assertTrue(topMenu.isSupplyDeliveryDeliveredVisible());
////        topMenu.clearFilter();
////        driver.navigate().refresh();
////
////
////    }
////
////    @Test(priority = 90, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////    public void validateStatesStatusDeliveredTC90(String shipmentNo90a, String shipmentNo90b) throws InterruptedException {
////        topMenu.goToSupplyDeliveriesTab();
////        topSubMenu.clickOceanIcon();
////
////        topMenu.clickStatesTab();
////        topMenu.clickSupplyDeliveryStatesReceived();
////        topSubMenu.clickSearchButton();
////        topSubMenu.goToSearchFeature(shipmentNo90a);
////        topMenu.assertActualAndExpectedDemandDeliveryNumberEqualOcean(shipmentNo90b);
////        Assert.assertTrue(topMenu.isSupplyDeliveryReceivedVisible());
////
////        topMenu.clearFilter();
////        driver.navigate().refresh();
////    }
////
////    @Test(priority = 91, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////    public void validateStatesStatusPlannedShipmentTC91(String shipmentNo91a, String shipmentNo91b) throws InterruptedException {
////        topMenu.goToSupplyDeliveriesTab();
////        topSubMenu.clickOceanIcon();
////
////        topMenu.clickStatesTab();
////        topMenu.clickSupplyDeliveryStatesShipped();
////        topSubMenu.clickSearchButton();
////        topSubMenu.goToSearchFeature(shipmentNo91a);
////        topMenu.assertActualAndExpectedDemandDeliveryNumberEqualOcean(shipmentNo91b);
////        Assert.assertTrue(topMenu.isSupplyDeliveryShippedVisible());
////        topMenu.clearFilter();
////        driver.navigate().refresh();
////    }
////
////
////    @Test(priority = 92, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////    public void validateStatesStatusShippedTC92(String shipmentNo92a, String shipmentNo92b) throws InterruptedException {
////        topMenu.goToSupplyDeliveriesTab();
////        topSubMenu.clickOceanIcon();
////
////        topMenu.clickStatesTab();
////        topMenu.clickSupplyDeliveryStatesShipped();
////        topSubMenu.clickSearchButton();
////        topSubMenu.goToSearchFeature(shipmentNo92a);
////        topMenu.assertActualAndExpectedDemandDeliveryNumberEqualOcean(shipmentNo92b);
////        Assert.assertTrue(topMenu.isSupplyDeliveryShippedVisible());
////
////        topMenu.clearFilter();
////        driver.navigate().refresh();
////    }
////
////// Note Elements no longer appears
//////    @Test(priority = 93, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
//////    public void validatingSearchFeatureCreatedSSTC93(String SSTC93)
//////    {
//////        topMenu.goToSupplyOrdersTab();// tops
//////        topMenu.clickStatesTab();
//////        topMenu.clickSupplyOrdersStatesCreated();
//////        topSubMenu.clickSearchButton();
//////        topSubMenu.goToSearchFeature(SSTC93);
//////        Assert.assertTrue(topMenu.isSupplyOrdersSearchResultDisplayed());
//////        Assert.assertTrue(topMenu.isSupplyOrdersStatesCreatedVisible());
//////        topMenu.clearFilter();
//////        driver.navigate().refresh();
//////    }
////
////    @Test(priority = 94, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////    public void validateStatesStatusShippedTC94(String shipmentNo93a, String shipmentNo93b)
////    {
////        topMenu.goToSupplyOrdersTab();  // tops
////
////        topMenu.clickStatesTab();
////        topMenu.clickSupplyOrdersStatesShipped();
////        topSubMenu.clickSearchButton();
////        topSubMenu.goToSearchFeature(shipmentNo93a);
////        topMenu.assertActualAndExpectedDemandOrdersNumberEqual(shipmentNo93b);
////        Assert.assertTrue(topMenu.isSupplyOrdersStatesShippedVisible());
////
////        topMenu.clearFilter();
////        driver.navigate().refresh();
////    }
////
////    @Test(priority = 95, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////    public void validateStatesStatusPlannedShipmentTC95(String shipmentNo95a, String shipmentNo95b)
//>>>>>>> 917759310f4f19d2dacf0bffd1ea0b4641759c4d
////    {
////        topMenu.goToSupplyOrdersTab();// tops
////
////        topMenu.clickStatesTab();
////        topMenu.clickSupplyOrdersStatesPlannedShipment();
////        topSubMenu.clickSearchButton();
////        topSubMenu.goToSearchFeature(shipmentNo95a);
////        topMenu.assertActualAndExpectedDemandOrdersNumberEqual(shipmentNo95b);
////        Assert.assertTrue(topMenu.isSupplyOrdersStatesPlannedShipmentVisible());
////
////        topMenu.clearFilter();
////        driver.navigate().refresh();
////    }
////
////    @Test(priority = 96, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////    public void validateStatesStatusOpenTC96(String shipmentNo96a, String shipmentNo96b)
////    {
////        topMenu.goToSupplyOrdersTab();  //wired behavior
////
////        topMenu.clickStatesTab();
////        topMenu.clickSupplyOrdersStatesOpen();
////        topSubMenu.clickSearchButton();
////        topSubMenu.goToSearchFeature(shipmentNo96a);
////        topMenu.assertActualAndExpectedDemandOrdersNumberEqual(shipmentNo96b);
////        Assert.assertTrue(topMenu.isSupplyOrdersStatesOpenVisible());
////
////        topMenu.clearFilter();
////        driver.navigate().refresh();
////    }
////
////    @Test(priority = 97, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////    public void validateStatesStatusNewTC97(String shipmentNo97a, String shipmentNo97b)
////    {
////        topMenu.goToSupplyOrdersTab();
////
////        topMenu.clickStatesTab();
////        topMenu.clickSupplyOrdersStatesNew();
////        topSubMenu.clickSearchButton();
////        topSubMenu.goToSearchFeature(shipmentNo97a);
////        topMenu.assertActualAndExpectedDemandOrdersNumberEqual(shipmentNo97b);
////        Assert.assertTrue(topMenu.isSupplyOrdersStatesNewVisible());
////
////        topMenu.clearFilter();
////        driver.navigate().refresh();
////    }
////
////    @Test(priority = 98, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////    public void validateStatesStatusCancelledTC98(String shipmentNo98a, String shipmentNo98b) throws InterruptedException {
////        topMenu.goToSupplyOrdersTab();
////
////        topMenu.clickStatesTab();
////        topSubMenu.clickDemandOrderSliderIcon(); // SliderIcon
////        topMenu.clickSupplyOrdersStatesCancelled();
////        topSubMenu.clickSearchButton();
////        topSubMenu.goToSearchFeature(shipmentNo98a);
////        topMenu.assertActualAndExpectedDemandOrdersNumberEqual(shipmentNo98b);
////        Assert.assertTrue(topMenu.isSupplyOrdersStatesCancelledVisible());
////
////        topMenu.clearFilter();
////        driver.navigate().refresh();
////    }
////
////    @Test(priority = 99, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////    public void validateStatesStatusConfirmedTC99(String shipmentNo99a, String shipmentNo99b) throws InterruptedException {
////        topMenu.goToSupplyOrdersTab();
////
////        topMenu.clickStatesTab();
////        topSubMenu.clickDemandOrderSliderIcon(); // SliderIcon
////        topSubMenu.clickDemandOrderSliderIcon(); // SliderIcon
////        topMenu.clickSupplyOrdersStatesConfirmed();
////        topSubMenu.clickSearchButton();
////        topSubMenu.goToSearchFeature(shipmentNo99a);
////        topMenu.assertActualAndExpectedDemandOrdersNumberEqual(shipmentNo99b);
////        Assert.assertTrue(topMenu.isSupplyOrdersStatesConfirmedVisible());
////
////        topMenu.clearFilter();
////        driver.navigate().refresh();
////    }
////
////    @Test(priority = 100, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////    public void validateStatesStatusConfirmedWithChangesTC100(String shipmentNo100a, String shipmentNo100b) throws InterruptedException {
////        topMenu.goToSupplyOrdersTab();
////        topMenu.clickExceptionsTab();
////
////        topMenu.clickStatesTab();
////        topSubMenu.clickDemandOrderSliderIcon(); // SliderIcon
////        topSubMenu.clickDemandOrderSliderIcon(); // SliderIcon
////        topSubMenu.clickDemandOrderSliderIcon(); // SliderIcon
////        topMenu.clickSupplyOrdersStatesConfirmedWithChanges();
////        topSubMenu.clickSearchButton();
////        topSubMenu.goToSearchFeature(shipmentNo100a);
////        //topMenu.assertActualAndExpectedDemandOrdersNumberEqual(shipmentNo100b);
////        Assert.assertTrue(topMenu.isSupplyOrdersStatesConfirmedWithChangesVisible());
////
////        topMenu.clearFilter();
////        driver.navigate().refresh();
////    }
////
////    @Test(priority = 101, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////    public void validateStatesStatusClosedTC101(String shipmentNo101a, String shipmentNo101b) throws InterruptedException {
////        topMenu.goToSupplyOrdersTab();
////        topMenu.clickExceptionsTab();
////
////        topMenu.clickStatesTab();
////        topSubMenu.clickDemandOrderSliderIcon(); // SliderIcon
////        topSubMenu.clickDemandOrderSliderIcon(); // SliderIcon
////        topSubMenu.clickDemandOrderSliderIcon(); // SliderIcon
////        topSubMenu.clickDemandOrderSliderIcon(); // SliderIcon
////        topMenu.clickSupplyOrdersStatesClosed();
////        topSubMenu.clickSearchButton();
////        topSubMenu.goToSearchFeature(shipmentNo101a);
////        //topMenu.assertActualAndExpectedDemandOrdersNumberEqual(shipmentNo101b);
////        Assert.assertTrue(topMenu.isSupplyOrdersStatesClosedVisible());
////
////        topMenu.clearFilter();
////        driver.navigate().refresh();
////    }
////
////    @Test(priority = 102, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////    public void validateStatesStatusReceivedTC102(String shipmentNo102a, String shipmentNo102b) throws InterruptedException {
////        topMenu.goToSupplyOrdersTab();
////        topMenu.clickExceptionsTab();
////
////        topMenu.clickStatesTab();
////        topSubMenu.clickDemandOrderSliderIcon(); // SliderIcon
////        topSubMenu.clickDemandOrderSliderIcon(); // SliderIcon
////        topSubMenu.clickDemandOrderSliderIcon(); // SliderIcon
////        topSubMenu.clickDemandOrderSliderIcon(); // SliderIcon
////        topSubMenu.clickDemandOrderSliderIcon(); // SliderIcon
////        topMenu.clickSupplyOrdersStatesReceived();
////        topSubMenu.clickSearchButton();
////        topSubMenu.goToSearchFeature(shipmentNo102a);
////        //topMenu.assertActualAndExpectedDemandOrdersNumberEqual(shipmentNo102b);
////        Assert.assertTrue(topMenu.isSupplyOrdersStatesReceivedVisible());
////
////        topMenu.clearFilter();
////        driver.navigate().refresh();
////    }
////
////    @Test(priority = 103, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////    public void validateStatesStatusRejectedTC103(String shipmentNo103a, String shipmentNo103b) throws InterruptedException {
////        topMenu.goToSupplyOrdersTab();
////        topMenu.clickExceptionsTab();
////
////        topMenu.clickStatesTab();
////        topSubMenu.clickDemandOrderSliderIcon(); // SliderIcon
////        topSubMenu.clickDemandOrderSliderIcon(); // SliderIcon
////        topSubMenu.clickDemandOrderSliderIcon(); // SliderIcon
////        topSubMenu.clickDemandOrderSliderIcon(); // SliderIcon
////        topSubMenu.clickDemandOrderSliderIcon(); // SliderIcon
////        topSubMenu.clickDemandOrderSliderIcon(); // SliderIcon
////        topMenu.clickSupplyOrdersStatesRejected();
////        topSubMenu.clickSearchButton();
////        topSubMenu.goToSearchFeature(shipmentNo103a);
////       // topMenu.assertActualAndExpectedDemandOrdersNumberEqual(shipmentNo103b);
////        Assert.assertTrue(topMenu.isSupplyOrdersStatesRejectedVisible());
////
////        topMenu.clearFilter();
////        driver.navigate().refresh();
////        leftMenu.clickInventoryMenuOption();
////
////    }
////
////
////// Inventory
////
////    @Test(priority = 104, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////    public void validateStatesStatusStockOutTC104(String shipmentNo104a, String shipmentNo104b) throws InterruptedException {
////        leftMenu.clickInventoryMenuOption();
////
////        topMenu.clickSupplyInventoryStockOut();
////        topSubMenu.clickSearchButton();
////        topSubMenu.goToSearchFeature(shipmentNo104a);
////        topMenu.assertActualAndExpectedInventoryNumberEqual(shipmentNo104b);
////        Assert.assertTrue(topMenu.isSupplyInventoryStockedOutVisible());
////        topMenu.clearFilter();
////        driver.navigate().refresh();
////    }
////
////
////    @Test(priority = 105, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////    public void validateStatesStatusLowStockTC105(String shipmentNo105a, String shipmentNo105b) throws InterruptedException {
////        leftMenu.clickInventoryMenuOption();
////        topMenu.clickSupplyInventoryLowStock();
////        topSubMenu.clickSearchButton();
////        topSubMenu.goToSearchFeature(shipmentNo105a);
////        topMenu.assertActualAndExpectedInventoryNumberEqual(shipmentNo105b);
////        Assert.assertTrue(topMenu.isSupplyInventoryLowStockVisible());
////
////        topMenu.clearFilter();
////        driver.navigate().refresh();
////    }
////
////    @Test(priority = 106, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////    public void validateStatesStatusOverstockTC106(String shipmentNo106a, String shipmentNo106b) throws InterruptedException {
////        leftMenu.clickInventoryMenuOption();
////        topMenu.clickSupplyInventoryOverstock();
////        topSubMenu.clickSearchButton();
////        topSubMenu.goToSearchFeature(shipmentNo106a);
////        topMenu.assertActualAndExpectedInventoryNumberEqual(shipmentNo106b);
////        Assert.assertTrue(topMenu.isSupplyInventoryOverstockVisible());
////
////        topMenu.clearFilter();
////        driver.navigate().refresh();
////    }
////
////    @Test(priority = 107, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////
////    public void validateStatesStatusOnTrackTC107(String shipmentNo107a, String shipmentNo107b) throws InterruptedException {
////        leftMenu.clickInventoryMenuOption();
////        topMenu.clickSupplyInventoryOnTrack();
////        topSubMenu.clickSearchButton();
////        topSubMenu.goToSearchFeature(shipmentNo107a);
////        topMenu.assertActualAndExpectedInventoryNumberEqual(shipmentNo107b);
////        Assert.assertTrue(topMenu.isSupplyInventoryOnTrackVisible());
////
////        topMenu.clearFilter();
////        driver.navigate().refresh();
////    }
//<<<<<<< HEAD
//
//    @Test(priority = 94, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
//    public void validateStatesStatusShippedTC94(String shipmentNo93a, String shipmentNo93b)
//    {
//        topMenu.goToSupplyOrdersTab();  // tops
//        topMenu.clickStatesTab();
//        topMenu.clickSupplyOrdersStatesShipped();
//        topSubMenu.clickSearchButton();
//        topSubMenu.goToSearchFeature(shipmentNo93a);
//        topMenu.assertActualAndExpectedDemandOrdersNumberEqual(shipmentNo93b);
//        Assert.assertTrue(topMenu.isSupplyOrdersStatesShippedVisible());
//        topMenu.clearFilter();
//        driver.navigate().refresh();
//    }
//
//    @Test(priority = 95, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
//    public void validateStatesStatusPlannedShipmentTC95(String shipmentNo95a, String shipmentNo95b)
//    {
//        topMenu.goToSupplyOrdersTab();// tops
//        topMenu.clickStatesTab();
//        topMenu.clickSupplyOrdersStatesPlannedShipment();
//        topSubMenu.clickSearchButton();
//        topSubMenu.goToSearchFeature(shipmentNo95a);
//        topMenu.assertActualAndExpectedDemandOrdersNumberEqual(shipmentNo95b);
//        Assert.assertTrue(topMenu.isSupplyOrdersStatesPlannedShipmentVisible());
//        topMenu.clearFilter();
//        driver.navigate().refresh();
//    }
//
//    @Test(priority = 96, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
//    public void validateStatesStatusOpenTC96(String shipmentNo96a, String shipmentNo96b)
//    {
//        topMenu.goToSupplyOrdersTab();  //wired behavior
//        topMenu.clickStatesTab();
//        topMenu.clickSupplyOrdersStatesOpen();
//        topSubMenu.clickSearchButton();
//        topSubMenu.goToSearchFeature(shipmentNo96a);
//        topMenu.assertActualAndExpectedDemandOrdersNumberEqual(shipmentNo96b);
//        Assert.assertTrue(topMenu.isSupplyOrdersStatesOpenVisible());
//        topMenu.clearFilter();
//        driver.navigate().refresh();
//    }
//
//    @Test(priority = 97, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
//    public void validateStatesStatusNewTC97(String shipmentNo97a, String shipmentNo97b)
//    {
//        topMenu.goToSupplyOrdersTab();
//        topMenu.clickStatesTab();
//        topMenu.clickSupplyOrdersStatesNew();
//        topSubMenu.clickSearchButton();
//        topSubMenu.goToSearchFeature(shipmentNo97a);
//        topMenu.assertActualAndExpectedDemandOrdersNumberEqual(shipmentNo97b);
//        Assert.assertTrue(topMenu.isSupplyOrdersStatesNewVisible());
//        topMenu.clearFilter();
//        driver.navigate().refresh();
//    }
//
//
//
//
//
////    @Test(priority = 98, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////    public void validateStatesStatusCancelledTC98(String shipmentNo98a, String shipmentNo98b) throws InterruptedException {
////        topMenu.goToSupplyOrdersTab();
////        topMenu.clickStatesTab();
////        topSubMenu.clickDemandOrderSliderIcon(); // SliderIcon
////        topMenu.clickSupplyOrdersStatesCancelled();// cancel icon disappears after a search
////        topSubMenu.clickSearchButton();
////        topSubMenu.goToSearchFeature(shipmentNo98a);
////        topMenu.assertActualAndExpectedDemandOrdersNumberEqual(shipmentNo98b);
////        Assert.assertTrue(topMenu.isSupplyOrdersStatesCancelledVisible());
////        topMenu.clearFilter();
////        driver.navigate().refresh();
////    }
//
//    @Test(priority = 99, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
//    public void validateStatesStatusConfirmedTC99(String shipmentNo99a, String shipmentNo99b) throws InterruptedException {
//        topMenu.goToSupplyOrdersTab();
//
//        topMenu.clickStatesTab();
//        topSubMenu.clickDemandOrderSliderIcon(); // SliderIcon
//        topSubMenu.clickDemandOrderSliderIcon(); // SliderIcon
//        topSubMenu.clickDemandOrderSliderIcon(); // SliderIcon
//        topMenu.clickSupplyOrdersStatesConfirmed();
//        topSubMenu.clickSearchButton();
//        topSubMenu.goToSearchFeature(shipmentNo99a);
//        topMenu.assertActualAndExpectedDemandOrdersNumberEqual(shipmentNo99b);
//        Assert.assertTrue(topMenu.isSupplyOrdersStatesConfirmedVisible());
//
//        topMenu.clearFilter();
//        driver.navigate().refresh();
//    }
//
//    @Test(priority = 100, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
//    public void validateStatesStatusConfirmedWithChangesTC100(String shipmentNo100a, String shipmentNo100b) throws InterruptedException {
//        topMenu.goToSupplyOrdersTab();
//        topMenu.clickExceptionsTab();
//
//        topMenu.clickStatesTab();
//        topSubMenu.clickDemandOrderSliderIcon(); // SliderIcon
//        topSubMenu.clickDemandOrderSliderIcon(); // SliderIcon
//        topSubMenu.clickDemandOrderSliderIcon(); // SliderIcon
//        topMenu.clickSupplyOrdersStatesConfirmedWithChanges();
//        topSubMenu.clickSearchButton();
//        topSubMenu.goToSearchFeature(shipmentNo100a);
//        //topMenu.assertActualAndExpectedDemandOrdersNumberEqual(shipmentNo100b);
//        Assert.assertTrue(topMenu.isSupplyOrdersStatesConfirmedWithChangesVisible());
//
//        topMenu.clearFilter();
//        driver.navigate().refresh();
//    }
//
//    @Test(priority = 101, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
//    public void validateStatesStatusClosedTC101(String shipmentNo101a, String shipmentNo101b) throws InterruptedException {
//        topMenu.goToSupplyOrdersTab();
//        topMenu.clickExceptionsTab();
//
//        topMenu.clickStatesTab();
//        topSubMenu.clickDemandOrderSliderIcon(); // SliderIcon
//        topSubMenu.clickDemandOrderSliderIcon(); // SliderIcon
//        topSubMenu.clickDemandOrderSliderIcon(); // SliderIcon
//        topSubMenu.clickDemandOrderSliderIcon(); // SliderIcon
//        topMenu.clickSupplyOrdersStatesClosed();
//        topSubMenu.clickSearchButton();
//        topSubMenu.goToSearchFeature(shipmentNo101a);
//        //topMenu.assertActualAndExpectedDemandOrdersNumberEqual(shipmentNo101b);
//        Assert.assertTrue(topMenu.isSupplyOrdersStatesClosedVisible());
//
//        topMenu.clearFilter();
//        driver.navigate().refresh();
//    }
//
//    @Test(priority = 102, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
//    public void validateStatesStatusReceivedTC102(String shipmentNo102a, String shipmentNo102b) throws InterruptedException {
//        topMenu.goToSupplyOrdersTab();
//        topMenu.clickExceptionsTab();
//
//        topMenu.clickStatesTab();
//        topSubMenu.clickDemandOrderSliderIcon(); // SliderIcon
//        topSubMenu.clickDemandOrderSliderIcon(); // SliderIcon
//        topSubMenu.clickDemandOrderSliderIcon(); // SliderIcon
//        topSubMenu.clickDemandOrderSliderIcon(); // SliderIcon
//        topSubMenu.clickDemandOrderSliderIcon(); // SliderIcon
//        topMenu.clickSupplyOrdersStatesReceived();
//        topSubMenu.clickSearchButton();
//        topSubMenu.goToSearchFeature(shipmentNo102a);
//        //topMenu.assertActualAndExpectedDemandOrdersNumberEqual(shipmentNo102b);
//        Assert.assertTrue(topMenu.isSupplyOrdersStatesReceivedVisible());
//
//        topMenu.clearFilter();
//        driver.navigate().refresh();
//    }
//
//    @Test(priority = 103, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
//    public void validateStatesStatusRejectedTC103(String shipmentNo103a, String shipmentNo103b) throws InterruptedException {
//        topMenu.goToSupplyOrdersTab();
//        topMenu.clickExceptionsTab();
//
//        topMenu.clickStatesTab();
//        topSubMenu.clickDemandOrderSliderIcon(); // SliderIcon
//        topSubMenu.clickDemandOrderSliderIcon(); // SliderIcon
//        topSubMenu.clickDemandOrderSliderIcon(); // SliderIcon
//        topSubMenu.clickDemandOrderSliderIcon(); // SliderIcon
//        topSubMenu.clickDemandOrderSliderIcon(); // SliderIcon
//        topSubMenu.clickDemandOrderSliderIcon(); // SliderIcon
//        topMenu.clickSupplyOrdersStatesRejected();
//        topSubMenu.clickSearchButton();
//        topSubMenu.goToSearchFeature(shipmentNo103a);
//        // topMenu.assertActualAndExpectedDemandOrdersNumberEqual(shipmentNo103b);
//        Assert.assertTrue(topMenu.isSupplyOrdersStatesRejectedVisible());
//
//        topMenu.clearFilter();
//        driver.navigate().refresh();
//        leftMenu.clickInventoryMenuOption();
//
//    }
//
//
//// Inventory
//
////    @Test(priority = 104, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////    public void validateStatesStatusStockOutTC104(String shipmentNo104a, String shipmentNo104b) throws InterruptedException {
////        leftMenu.clickInventoryMenuOption();
////
////        topMenu.clickSupplyInventoryStockOut();
////        topSubMenu.clickSearchButton();
////        topSubMenu.goToSearchFeature(shipmentNo104a);
////        topMenu.assertActualAndExpectedInventoryNumberEqual(shipmentNo104b);
////        Assert.assertTrue(topMenu.isSupplyInventoryStockedOutVisible());
////        topMenu.clearFilter();
////        driver.navigate().refresh();
////    }
////
////
////    @Test(priority = 105, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////    public void validateStatesStatusLowStockTC105(String shipmentNo105a, String shipmentNo105b) throws InterruptedException {
////        leftMenu.clickInventoryMenuOption();
////        topMenu.clickSupplyInventoryLowStock();
////        topSubMenu.clickSearchButton();
////        topSubMenu.goToSearchFeature(shipmentNo105a);
////        topMenu.assertActualAndExpectedInventoryNumberEqual(shipmentNo105b);
////        Assert.assertTrue(topMenu.isSupplyInventoryLowStockVisible());
////
////        topMenu.clearFilter();
////        driver.navigate().refresh();
////    }
////
////    @Test(priority = 106, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////    public void validateStatesStatusOverstockTC106(String shipmentNo106a, String shipmentNo106b) throws InterruptedException {
////        leftMenu.clickInventoryMenuOption();
////        topMenu.clickSupplyInventoryOverstock();
////        topSubMenu.clickSearchButton();
////        topSubMenu.goToSearchFeature(shipmentNo106a);
////        topMenu.assertActualAndExpectedInventoryNumberEqual(shipmentNo106b);
////        Assert.assertTrue(topMenu.isSupplyInventoryOverstockVisible());
////
////        topMenu.clearFilter();
////        driver.navigate().refresh();
////    }
////
////    @Test(priority = 107, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////
////    public void validateStatesStatusOnTrackTC107(String shipmentNo107a, String shipmentNo107b) throws InterruptedException {
////        leftMenu.clickInventoryMenuOption();
////        topMenu.clickSupplyInventoryOnTrack();
////        topSubMenu.clickSearchButton();
////        topSubMenu.goToSearchFeature(shipmentNo107a);
////        topMenu.assertActualAndExpectedInventoryNumberEqual(shipmentNo107b);
////        Assert.assertTrue(topMenu.isSupplyInventoryOnTrackVisible());
////
////        topMenu.clearFilter();
////        driver.navigate().refresh();
////    }
//
//}
//=======
////
////
////
////
////
////
////}
//>>>>>>> 917759310f4f19d2dacf0bffd1ea0b4641759c4d
