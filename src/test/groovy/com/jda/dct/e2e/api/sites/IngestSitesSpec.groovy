package com.jda.dct.e2e.api.sites

import com.fasterxml.jackson.databind.ObjectMapper
import com.jda.dct.domain.Site
import com.jda.dct.e2e.api.categories.Regression
import com.jda.dct.e2e.api.categories.Smoke
import com.jda.dct.e2e.api.utility.IngestionHelper
import org.junit.experimental.categories.Category
import org.springframework.http.HttpMethod

import spock.lang.Specification
import spock.lang.Stepwise
import spock.util.concurrent.PollingConditions

import groovy.json.JsonOutput
import groovy.json.JsonSlurper

import java.util.Map.Entry

/**
 * End-to-end tests for {@code Site} ingestion.
 *
 * @version 1.0-SNAPSHOT
 * @since 01-June-2018
 */

class IngestSitesSpec extends Specification implements IngestionHelper {

    def setupSpec() {
        init()

        def sitePath = properties.getProperty('ingestSitesPath')
        def getPath = properties.getProperty('getSitesPath')

        ingestionURL = protocol + ip + sitePath
        getURLPrefix = protocol + ip + getPath

        sampleSite = this.getClass().getClassLoader().getResource('sample-site-1.json').text
    }

    @Category(Regression)
    def "Ingestion status of an invalid site becomes complete with error"() {
        given:
        def conditions = new PollingConditions(timeout: 120, initialDelay: 1)
        def modifiedSampleSite = sampleSite.replaceAll("\"siteType\": \"DC\"", "\"siteType\": \"wrong-site-type\"")

        when: 'a site json ingested'
        def response = executeHttpCall(header, ingestionURL, modifiedSampleSite, HttpMethod.POST)
        System.out.println("333ingestionId: " + response.ingestionId)
        def ingestionId = response.ingestionId

        then: 'ingestion status eventually becomes COMPLETED'
        conditions.eventually {
            def status = getStatus(ingestionId)
            assert status.contains('COMPLETED_WITH_ERROR')
        }
    }

    // evaluate and rename if necessary
    @Category(Regression)
    def "field in process model updates upon a new ingest"() {
        given: 'original site ingested'
        ingestData(sampleSite)
        def siteIds = getSiteIds(sampleSite)
        def url = getURLPrefix + siteIds[0]

        when: 'ingest updated site'
        def modifiedSampleSite = sampleSite.replaceAll("\"siteDescription\": \"Dallas Site 005\"", "\"siteDescription\": \"Dallas Site 002\"")
        def expectedSite = getJsonAt(modifiedSampleSite, 0)

        ingestData(modifiedSampleSite)

        def siteText = getHttpClient(header, url, null, HttpMethod.GET).body
        def siteRetrieved = new JsonSlurper().parseText(siteText).get('data').get(0)
        ['creationDate', 'id', 'lmd', 'refObjects','geoLocation'].each { siteRetrieved.remove(it) }

        siteRetrieved.notes = null
        expectedSite.notes = null

        then: 'the process model field updated'
        System.out.println("Expected: " + expectedSite.toString())
        System.out.println("Actual: " + siteRetrieved.toString())


        siteRetrieved.siteDescription == expectedSite.siteDescription
    }

    // evaluate and rename if necessary
    @Category(Regression)
    def "field not in process model not updated upon a new ingest"() {
        given: 'original site ingested'
        ingestData(sampleSite)
        def siteIds = getSiteIds(sampleSite)
        def url = getURLPrefix + siteIds[0]

        when: 'ingest updated site'
        def modifiedSampleSite = sampleSite.replaceAll("\"flexFloat1\": 123.45", "\"flexFloat1\": 987.65")
        def expectedSite = getJsonAt(sampleSite, 0)

        ingestData(modifiedSampleSite)

        def siteText = getHttpClient(header, url, null, HttpMethod.GET).body
        def siteRetrieved = new JsonSlurper().parseText(siteText).get('data').get(0)
        ['creationDate', 'id', 'lmd', 'geoLocation'].each { siteRetrieved.remove(it) }

        def mapper = new ObjectMapper()
        def expected = mapper.readValue(JsonOutput.toJson(expectedSite), Site)
        def received = mapper.readValue(JsonOutput.toJson(siteRetrieved), Site)

        then: 'non-process model field not updated'

        System.out.println("Expected: " + expected.toString())
        System.out.println("Actual: " + received.toString())
        received == expected
    }

    // does this work??
    @Category(Smoke)
    def "Add a Note to a Site and get it back"() {
        given:
        def conditions = new PollingConditions(timeout: 120, initialDelay: 1)

        when: 'A Note is added to the Site'

        def siteIds = getSiteIds(sampleSite)
        def url = getURLPrefix + 'notes/' + siteIds[0]
        def sampleNote = '{ "note": "Site Note added"}'
        def response = getHttpClient(header, url, sampleNote, HttpMethod.POST)
        assert response.status == 200

        and: "Get the Site "

        url = getURLPrefix + siteIds[0]
        def siteText = getHttpClient(header, url, null, HttpMethod.GET).body
        def siteRetrieved = new JsonSlurper().parseText(siteText).get('data').get(0)

        then: 'Get the Site  back and verify the note'
        assert siteRetrieved.getAt('notes').toString().contains("Site Note added") == true
    }
}
