package com.jda.dct.e2e.api.shipments

import com.jda.dct.e2e.api.categories.Smoke
import com.jda.dct.e2e.api.utility.IngestionHelper
import com.jda.dct.e2e.api.utility.QueryObject
import com.jda.dct.e2e.api.utility.QueryResultHandler
import org.junit.experimental.categories.Category
import spock.lang.Specification

class ShipmentTextSpec extends Specification implements IngestionHelper {

    def setupSpec() {
        init()
    }
/*
    @Category(Smoke)
    def "Search a full text"() {
        given: "all inboundShipments"
        def queryObject = new QueryObject()
        queryObject.setType("shipments")
        queryObject.addFullTextQuery()
                .searchText("Delivered")
                .searchField("state")
        queryObject.addQuery("shipmentType", "inboundShipment")

        when: "searchText is Delivered "
        def response = QueryResultHandler.getQueryResultHandler().executeWithRetry(queryObject, 30, 2, 2, 1, 1)

        then: "all shipments that include Delivered as text will return"
        response.body.totalCount != null
        and:
        response.body.data.size() > 0
        response.body.data.state.each({v ->
          assert   v == "Delivered"
        })
    }
 */
}
