package com.jda.dct.e2e.ui.utility.helper.AssertHelper;

import com.jda.dct.e2e.ui.utility.common.BaseTest;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

public class AssertHelper extends BaseTest {

    private WebDriver driver;
    private final Logger oLog = Logger.getLogger(AssertHelper.class);

    public AssertHelper(WebDriver driver) {
        this.driver = driver;
        oLog.debug("AssertHelper : " + this.driver.hashCode());
    }

    /**
     * This method Asserts for true
     *
     * @param condition
     * @param message
     * @return
     */

    public boolean assertTrue(boolean condition, String message) {
        try {
            Assert.assertTrue(condition, message);
            Reporter("Assertion: "+ message, "Pass");
            return true;
        } catch (AssertionError e) {
            Reporter("Assertion Error: "+ message + ": expected True but found False", "Fail", oLog);
            throw new RuntimeException("Assertion Error: "+ message + ": expected True but found False"+ ":" + e.getMessage());
        }catch (Exception e) {
            Reporter("Assertion Error: "+ message + ": expected True but found False", "Fail", oLog);
            throw new RuntimeException("Assertion Error: "+ message + ": expected True but found False"+ ":" + e.getMessage());
        }
    }

    /**
     * This method Asserts for false
     *
     * @param condition
     * @param message
     * @return
     */

    public boolean assertFalse(boolean condition, String message) {
        try {
            Assert.assertFalse(condition, message);
            Reporter("Assertion: "+ message, "Pass");
            return true;
        } catch (AssertionError e) {
            Reporter("Assertion Error: "+ message + ": expected True but found False", "Fail", oLog);
            throw new RuntimeException("Assertion Error: "+ message + ": expected True but found False"+ ":" + e.getMessage());
        }catch (Exception e) {
            Reporter("Assertion Error: "+ message + ": expected True but found False", "Fail", oLog);
            throw new RuntimeException("Assertion Error: "+ message + ": expected True but found False"+ ":" + e.getMessage());
        }
    }

}
