package com.jda.dct.e2e.clientUi.pages.SupplyModule

import geb.Page
import org.openqa.selenium.By

class SupplyPurchaseOrderdetailsPage extends Page {


    static url = "/tempraturecontrolled/purchase-orders"

    static atCheckWaiting = true

    static at = {
        $( "#purchase-order-title")

    }


  static content = {

      purchaseorderlines(wait:true) {

             $("#PurchaseOrderLines-Panel-Cell")
      }

      navigateback(wait: true) {

          $("a", text: "Orders")

      }
  }

    def gobacktosupplyOrder() {

        purchaseorderlines.displayed

        println "welcome to Purchase order Details Page"

        navigateback.click()

    }


  }

