package com.jda.dct.e2e.api

import com.jda.dct.e2e.api.utility.AuthHelper
import groovyx.net.http.RESTClient
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll

/**
 * GET method to retrieve demand shipment detail configuration
 *
 */

// may be redundant, check and delete on confirmation
class GetShipmentDetailConfigSpec extends Specification implements AuthHelper {
    static shipmentDetailConfigURL
    @Shared
    def client

    def setupSpec() {
        init()

        def shipmentDetailConfigPath = properties.getProperty('getShipmentsDetailConfigPath')
        shipmentDetailConfigURL = protocol + ip + shipmentDetailConfigPath
        client = new RESTClient(shipmentDetailConfigURL)
        client.handler.failure = { resp -> resp }
        header[HttpHeaders.CONTENT_TYPE] = MediaType.APPLICATION_JSON
    }

    @Unroll
    def "Get shipment detail configuration API #url"() {
        given: "The API URL is #url "
        when: "HTTP Method is GET: should return demand shipment detail configuration"
        def response = client.get(headers: header)
        then: "the response should be 200"
        with(response) {
            status == 200
        }
        where:
        url << [shipmentDetailConfigURL]

    }
}
