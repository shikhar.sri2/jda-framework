package com.jda.dct.e2e.ui.utility.ExcelHelper;

import com.jda.dct.e2e.ui.utility.common.BaseTest;
import com.jda.dct.e2e.ui.utility.helper.Resource.ResourceHelper;
import com.jda.dct.e2e.ui.utility.helper.Utility.UtilityHelper;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.util.*;

public class ExcelReader extends BaseTest {

    private String fileName;
    private String sheetName;
    private Workbook workbook;
    private Sheet sheet;

    private Workbook setExcelFileObject(String fileName) {
        try {
            FileInputStream inputStream = new FileInputStream(fileName);
            if (fileName.endsWith(".xlsx")) {
                workbook = new XSSFWorkbook(inputStream);
            } else if (fileName.endsWith(".xls")) {
                workbook = new HSSFWorkbook(inputStream);
            } else {
                throw new Exception(" Invalid file extension fo parsing ");
            }
            return workbook;
        } catch (NullPointerException ne) {
            throw new RuntimeException("File Name or Sheet Name is not defined.");
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Exception file set excel file object");
        }
    }

    public Workbook setFilenameAndSheetName(String fileName, String sheetName) {
        fileName = ResourceHelper.getResourcePath(UtilityHelper.getProperty("testFramework","default.download.path")) + File.separator + fileName;
        return setExcelFileObject(fileName);
    }

    public Workbook setFilenameAndSheetName(String fileName) {
        fileName = ResourceHelper.getResourcePath(UtilityHelper.getProperty("testFramework","default.download.path")) + File.separator + fileName;
        return setExcelFileObject(fileName);
    }

    public int getNoOfRows(String fileName, int sheetIndex) {
        try {
            setFilenameAndSheetName(fileName);
            sheet = workbook.getSheetAt(sheetIndex);
            int rowCount = sheet.getLastRowNum() - sheet.getFirstRowNum();
            return rowCount;
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Exception in getting row count");
        }
    }

    public int getNoOfColumns(String fileName, int sheetIndex) {
        try {
            setFilenameAndSheetName(fileName);
            sheet = workbook.getSheetAt(sheetIndex);
            Row row = sheet.getRow(0);
            int colNo = row.getLastCellNum() - row.getFirstCellNum();
            return colNo;
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Exception in getting column count");
        }
    }

    public String[] getColumnHeaders(String fileName, int sheetIndex) {
        try {
            setFilenameAndSheetName(fileName);
            sheet = workbook.getSheetAt(sheetIndex);
            //sheet = workbook.getSheet(sheetName);
            Row row = sheet.getRow(0);

            int cols = row.getLastCellNum();
            String[] header = new String[cols];

            for (int j = 0; j < cols; j++)
                header[j] = row.getCell(j).getStringCellValue();
            return header;
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Exception in getting column headers");
        }
    }

    public List getColumnDataForRowsMentioned(String fileName, int sheetIndex, String headerName) {
        List data = new ArrayList();
        try {
            setFilenameAndSheetName(fileName);
            sheet = workbook.getSheetAt(sheetIndex);
            int rows = sheet.getLastRowNum();

            String[] header = getColumnHeaders(fileName, sheetIndex);
            int cols = header.length;

            for (int i = 1; i <= rows; i++) {
                Row row = sheet.getRow(i);
                for (int j = 0; j < cols; j++) {
                    String cellValue = getCellData(row.getCell(j));
                    if( header[j].equalsIgnoreCase(headerName) ) {
                        data.add(cellValue);
                    }
                }
            }

            if (data.size() == 0){
                Reporter("No data is present for the header in the file", "Info");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return data;
    }


    public String getCellData(Cell cell) {

        String cellText = "";

        try {

            CellType type = cell.getCellTypeEnum();

            if (type == CellType.STRING)
                cellText = cell.getStringCellValue();
            else if (type == CellType.NUMERIC)
                cellText = cell.getNumericCellValue() + "";
            else if (type == CellType.BOOLEAN)
                cellText = cell.getBooleanCellValue() + "";
            else
                cellText = "";

        } catch (Exception e) {

        }

        return cellText;
    }
}
