package com.jda.dct.e2e.ui.utility.pageobjects;

import com.jda.dct.e2e.ui.utility.action.CommonActions;
import com.jda.dct.e2e.ui.utility.common.BasePage;
import com.jda.dct.e2e.ui.utility.helper.Action.ActionHelper;
import com.jda.dct.e2e.ui.utility.helper.Wait.WaitHelper;
import com.jda.dct.e2e.ui.utility.helper.genericHelper.GenericHelper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class InventoryListPage extends BasePage {

    WebDriver driver;
    WebDriverWait webDriverWait;
    WaitHelper objWaitHelper;
    GenericHelper objGenericHelper;
    ActionHelper actionHelper;
    CommonActions commonActions;

    public InventoryListPage(WebDriver webdriver) {

        this.driver = webdriver;
        PageFactory.initElements(driver, this);
        webDriverWait = new WebDriverWait(driver, getTimeOutInSeconds());
        objWaitHelper = new WaitHelper(driver);
        objGenericHelper = new GenericHelper(driver);
        actionHelper = new ActionHelper(driver);
        commonActions = new CommonActions(driver);
    }



    //span[text()='INVENTORY']")
    @FindBy(id = "PanelSummary-RenderedItem-Item-1-LinkItem")
    private WebElement inventoryListPage;
   // @FindBy(xpath = "//span[contains(text(),'   Inventory Details   ')]")
    @FindBy(id = "breadcrumb-inventory-details")
    private WebElement inventoryDetailsPage;
    //@FindBy(id = "PanelSummary-RenderedItem-Item")
    @FindBy(id = "PanelSummary-RenderedItem-Item-1-LinkItem")
    private WebElement inventoryItemNumber;



    //@FindBy(xpath = "//a[@href=\"/inventory\"] ")
    @FindBy(xpath ="//*[@href='/inventory']")
    private WebElement backToInventoryPage;

    @FindBy(xpath = "//*[@id='PanelSummary-RenderedItem-Notes-1-NotesItem']/a")
    private WebElement inventoryNotes;

    @FindBy(id="PanelSummary-RenderedItem-On-Hand-2-drawer-link")
    private WebElement onHand;

    @FindBy(xpath="//h1[contains(text(),'On Hand')]")
    private WebElement onHandWindow;

    @FindBy(id="PanelSummary-RenderedItem-Monetary-Impact-1-drawer-link")
     private WebElement monetaryImpact;

      @FindBy(xpath="//h1[contains(text(),'Monetary Impact')]')]")
    private WebElement monetaryImpactWindow;

    @FindBy(xpath = "//*[contains(@id,'demandSupply-inventory-Filter-List-Card')]//span")
    private List<WebElement> InventoryExceptionStatusCount;

    @FindBy(id ="PanelSummary-RenderedItem-EOW-0-LinkItem")
    private WebElement EOW;

    @FindBy(id ="PanelSummary-RenderedItem-EOM-0-LinkItem")
    private WebElement EOM;

    @FindBy(id ="PanelSummary-RenderedItem-EOQ-0-LinkItem")
    private WebElement EOQ;

    @FindBy(id ="PanelSummary-RenderedItem-EOY-0-LinkItem")
    private WebElement EOY;


    //========Methods=====

    public boolean isInventoryListPageDisplayed() {
        objWaitHelper.waitElementToBeVisible(inventoryListPage);
        return objGenericHelper.isDisplayed(inventoryListPage,"inventoryListPage");
    }


    public boolean isInventoryDetailsPageDisplayed() {
        objWaitHelper.waitElementToBeVisible(inventoryDetailsPage);
        return objGenericHelper.isDisplayed(inventoryDetailsPage,"inventoryDetailsPageDisplayed");
    }

    public void clickInventoryItemNumber() {
        objWaitHelper.waitElementToBeClickable(inventoryItemNumber);
        objGenericHelper.elementClick(inventoryItemNumber, "inventoryItemNumber");
    }

    public void clickSearchInventoryItemNumber(String item) {
        objWaitHelper.waitElementToBeClickable(inventoryItemNumber);
        objGenericHelper.elementClick(inventoryItemNumber, "inventoryItemNumber");
    }

    public void clickBackToInventoryPage() {
        objWaitHelper.waitElementToBeClickable( backToInventoryPage);
        objGenericHelper.elementClick( backToInventoryPage, " backToInventoryPage");
    }


    public void clickInventoryNotes() {
        objWaitHelper.waitAndClickElement(inventoryNotes, "Inventory Notes");
    }

    public void clickOnHand() {
        objWaitHelper.waitElementToBeClickable(onHand);
        objGenericHelper.elementClick(onHand, "onHand");
    }

    public void clickMonetaryImapact() {
        objWaitHelper.waitElementToBeClickable(monetaryImpact);
        objGenericHelper.elementClick(monetaryImpact, "onHand");
    }

    public boolean isEOWDisplayed() {
        objWaitHelper.waitElementToBeVisible(EOW);
        return objGenericHelper.isDisplayed(EOW,"EOWisDisplayed");
    }

    public boolean isEOMDisplayed() {
        objWaitHelper.waitElementToBeVisible(EOM);
        return objGenericHelper.isDisplayed(EOM,"EOWisDisplayed");
    }
    public boolean isEOQDisplayed() {
        objWaitHelper.waitElementToBeVisible(EOQ);
        return objGenericHelper.isDisplayed(EOQ,"EOWisDisplayed");
    }
    public boolean isEOYDisplayed() {
        objWaitHelper.waitElementToBeVisible(EOY);
        return objGenericHelper.isDisplayed(EOY,"EOWisDisplayed");
    }
    public boolean isOnHandWindowDisplayed() {
        objWaitHelper.waitElementToBeVisible(onHandWindow);
        return objGenericHelper.isDisplayed(onHandWindow,"onHandWindow");
    }


    public boolean isMonetaryImpactWindowDisplayed() {
        objWaitHelper.waitElementToBeVisible(monetaryImpactWindow);
        return objGenericHelper.isDisplayed(monetaryImpactWindow,"monetaryImpactWindow");
    }


    public void clickInventoryExceptionsHavingData() {
        try {
            Thread.sleep(1000);
            commonActions.clickExceptionStatusHavingData(InventoryExceptionStatusCount);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}


