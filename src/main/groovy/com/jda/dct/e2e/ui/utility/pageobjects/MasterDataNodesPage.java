package com.jda.dct.e2e.ui.utility.pageobjects;

import com.jda.dct.e2e.ui.utility.common.BasePage;
import com.jda.dct.e2e.ui.utility.helper.Wait.WaitHelper;
import com.jda.dct.e2e.ui.utility.helper.genericHelper.GenericHelper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MasterDataNodesPage extends BasePage {

    WebDriver driver;
    WebDriverWait webDriverWait;
    WaitHelper waitHelper;
    GenericHelper genericHelper;

    public MasterDataNodesPage(WebDriver webdriver) {

        this.driver = webdriver;
        PageFactory.initElements(driver, this);
        webDriverWait = new WebDriverWait(driver, getTimeOutInSeconds());
        waitHelper = new WaitHelper(driver);
        genericHelper = new GenericHelper(driver);
    }


 @FindBy(id="PanelSummary-RenderedItem-Item-0-LinkItem")
    private WebElement masterDataNodesListPage;



//---Methods--


    public boolean isMasterDataNodesListPageDisplayed(){
        waitHelper.waitElementToBeVisible(masterDataNodesListPage);
        return genericHelper.isDisplayed(masterDataNodesListPage, "MasterDataNodes list page");
    }

}