//package com.jda.dct.e2e.clientUi.pages.tempraturecontrolled
//
//import geb.Page
//
//class SupplyInventoryListPage extends Page{
//    static url = "/tempraturecontrolled/inventory"
//
//    static atCheckWaiting = true
//
//    static at = {
//        waitFor{
//            $("div", class: "css-jigo8f").first().isDisplayed()
//        }
//    }
//
//    static content = {
//        firstInventoryItem(wait: true) {
//            $("div", role:"button", 'aria-expanded':"false").first()
//        }
//        firstItemLink(wait: true){
//            $("div", role:"button", 'aria-expanded':"false").first().find("a", "href":contains("tempraturecontrolled/inventory"))
//        }
//    }
//
//    def getFirstItem(){
//        waitFor {
//            firstInventoryItem.displayed
//        }
//        return firstInventoryItem
//    }
//
//    def getItemLink() {
//        waitFor {
//           firstItemLink.displayed
//        }
//        return firstItemLink
//    }
//
//    def navigateToDetails(){
//        firstItemLink.click()
//        waitFor {
//            exceptionPage.getCurrentUrl().contains("Node")
//        }
//    }
//}
