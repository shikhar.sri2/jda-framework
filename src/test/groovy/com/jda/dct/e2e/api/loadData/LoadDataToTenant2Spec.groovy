package com.jda.dct.e2e.api.loadData


import com.jda.dct.e2e.api.utility.FetchFileHelper
import com.jda.dct.e2e.api.utility.IngestionHelper
import com.jda.dct.e2e.api.utility.LoadShowcase
import org.json.simple.JSONArray
import org.springframework.http.HttpMethod
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll
import spock.util.concurrent.PollingConditions

class LoadDataToTenant2Spec extends Specification implements IngestionHelper, FetchFileHelper {

    @Shared String tid = "t35b19221b21b458b"
    @Shared tenantHeader
    @Shared List<File> allCSVFiles
    @Shared List<File> allControlFiles
    @Shared List<File> allJSONFiles
    @Shared List<File> umsFiles
    @Shared processModelUrl
    @Shared processModelFile

    @Shared umsUrl
    @Shared List<File> userJsonFiles
    @Shared List<File> tenantParticipantJsonFiles
    @Shared List<File> tenantUserJsonFiles
    @Shared List<File> tenantRoleJsonFiles
    @Shared JSONArray assignUserRoleFiles

    @Shared List<File> hotItemFiles

    def setupSpec() {
        tenantHeader = initAndGetTokenMultiTenant(tid)
        System.out.println(tenantHeader)
        processModelUrl = protocol + ip + processModelPostPath
        processModelFile = getProcessModel("show-case")

        allCSVFiles = getShowcaseData(tid).get(0)
        allControlFiles = getShowcaseData(tid).get(1)
        umsFiles = getShowcaseData(tid).get(2)
        allJSONFiles = getShowcaseData(tid).get(3)
        umsUrl = protocol + ip + umsPath

        hotItemFiles = new ArrayList<>()
        getFilesFromDir("show-case/simulate/hot_items", hotItemFiles)
        hotItemFiles = reorderCSV(hotItemFiles)

        userJsonFiles = new ArrayList<>()
        selectByKeyword("users", umsFiles, userJsonFiles, true)
        tenantParticipantJsonFiles = new ArrayList<>()
        selectByKeyword("tenant-participants", umsFiles, tenantParticipantJsonFiles, true)
        tenantUserJsonFiles = new ArrayList<>()
        selectByKeyword("tenant-users", umsFiles, tenantUserJsonFiles, true)
        tenantRoleJsonFiles = new ArrayList<>()
        selectByKeyword("tenant-roles-", umsFiles, tenantRoleJsonFiles, false)
        assignUserRoleFiles = selectJsonArrayByKeyword("assign-role-to-user-", umsFiles, new ArrayList<>(), false)
    }

    @Unroll
    def "Ingest process model"() {
        when: "Clear override process model"
        println()
        println "Ingest default process model..."
        def response = postFile(processModelFile, processModelUrl, tenantHeader)
        println "Post URL: " + processModelUrl

        then: "Ingested default process model successfully"
        response.toString().contains("model")
        println "End......"
    }

    @Unroll
    def "Ingest tenant users and assign roles"() {
        given: "Create a polling conditions"
        def conditions = new PollingConditions(timeout: 5, initialDelay: 1)

        when: "POST data"
        def userPostUrl = umsUrl + 'users'
        def tenantParticipantPostUrl = umsUrl + 'tenantParticipants'
        def tenantUserPostUrl = umsUrl + 'tenantUsers'
        def tenantRolesPostUrl = umsUrl + 'tenantRoles'
        def assignUserRoleUrl = umsUrl + 'tenantUsers/assignTenantRole'

        def response0
        for (File userJson : userJsonFiles) {
            response0 = executeHttpCall(tenantHeader, userPostUrl, userJson.text, HttpMethod.POST)
        }
        def response1
        for (File tenantUserJson : tenantUserJsonFiles) {
            response1 = executeHttpCall(tenantHeader, tenantUserPostUrl, tenantUserJson.text, HttpMethod.POST)
        }
        def response2
        for (File tenantParticipantJson : tenantParticipantJsonFiles) {
            response2 = executeHttpCall(tenantHeader, tenantParticipantPostUrl, tenantParticipantJson.text, HttpMethod.POST)
        }
        def response3
        for (File tenantRoleJson : tenantRoleJsonFiles) {
            response3 = executeHttpCall(tenantHeader, tenantRolesPostUrl, tenantRoleJson.text, HttpMethod.POST)
        }
        def response4
        Iterator i = assignUserRoleFiles.iterator()
        while (i.hasNext()) {
            def userRoleAssociation = i.next()
            String tenantId = userRoleAssociation["tid"]
            String userName = userRoleAssociation["userName"]
            String roleName = userRoleAssociation["roleName"]
            def url = assignUserRoleUrl + "?tid=" + tenantId + "&userName=" + userName + "&roleName=" + roleName
            response4 = executeHttpCall(tenantHeader, url, null, HttpMethod.POST)
        }

        then: "Roles data ingested successfully"
        conditions.eventually {
            assert response0 != null && response0["count"] > 0
            assert response1 != null && response1["count"] > 0
            assert response2 != null && response2["count"] > 0
            assert response3 != null && response3["count"] > 0
            assert response4 != null && response4["count"] > 0
        }
    }

    @Unroll
    def "Ingest master-data"() {
        given: "get showcase data"
        List<File> data = selectedFile("sites, items", allCSVFiles)

        when: "Load data parallel"
        List<Boolean> results = new LoadShowcase().loadParallel(false, tenantHeader, 10, data, allControlFiles)

        then: "sites, items data ingest successfully"
        results.every {
            it
        }
    }

    @Unroll
    def "Ingest hot-items"() {
        when: "Load hot items files"
        System.out.println("\n\nLoading file: " + file.getName())
        String fileName = file.getName()
        def postUrl = getPostUrl(fileName)
        def response = postFile(file, postUrl, tenantHeader)
        def ingestionId = getIngestionId(response)
        def status = getFinalStatus(ingestionId, fileName, tenantHeader)

        then:
        status == 'COMPLETED'

        where:
        file << hotItemFiles
    }
}
