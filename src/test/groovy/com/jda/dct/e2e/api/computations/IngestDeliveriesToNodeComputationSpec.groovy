package com.jda.dct.e2e.api.computations

import com.fasterxml.jackson.databind.ObjectMapper
import com.jda.dct.domain.stateful.Delivery
import com.jda.dct.e2e.api.utility.DateUtils
import com.jda.dct.e2e.api.utility.IngestionHelper
import groovy.json.JsonSlurper
import org.springframework.http.HttpMethod
import spock.lang.Shared
import spock.lang.Specification
import spock.util.concurrent.PollingConditions

/**
 * End-to-end tests for delivery to node computation.
 *
 * @version 1.0-SNAPSHOT
 * @since 29-Jun-2018
 */

// figure out if needs to be in ReadyAPI
class IngestDeliveriesToNodeComputationSpec extends Specification implements IngestionHelper {
    @Shared tmpNodeIngestionURL
    @Shared tmpMeasureIngestionURL
    @Shared tmpGetNodeURLPrefix

    def setupSpec() {
        init()

        def deliveryPath = properties.getProperty('ingestDeliveriesPath')
        def getPath = properties.getProperty('getDeliveryPath')

        ingestionURL = protocol + ip + deliveryPath

        def tmpIngestNodePath = properties.getProperty('nodePostPath')
        tmpNodeIngestionURL =  protocol + ip + tmpIngestNodePath
        def tmpIngestMeasurePath = properties.getProperty('measurePath');
        tmpMeasureIngestionURL = protocol + ip + tmpIngestMeasurePath
        tmpGetNodeURLPrefix = protocol + ip + properties.getProperty('nodeGetPath')
        
        //sampleDelivery = this.getClass().getClassLoader().getResource('sample-delivery-to-node-compute.json').text
        //sampleNode = this.getClass().getClassLoader().getResource('sample-node-from-delivery-compute.json').text
    }

    // no need to ingest new testing data
    /*def "Ingest delivery and trigger node computation"() {
        given:
        def conditions = new PollingConditions(timeout: 60, initialDelay: 1)
        conditions.setDelay(10)
        def nodeId = getNodeIds(sampleNode)
        nodeId = nodeId.toString().substring(1, nodeId.toString().indexOf(']', 1))
        System.out.println(nodeId)

        when: 'a node json ingested and delivery ingested.'

        def responseNode = executeHttpCall(header, tmpNodeIngestionURL, sampleNode, HttpMethod.POST)

        def ingestionId = responseNode.ingestionId

        conditions.eventually {
            def status = getStatus(ingestionId)
            assert status.contains('COMPLETED')
        }

        def responseMeasure = executeHttpCall(header, tmpMeasureIngestionURL, sampleNode, HttpMethod.POST)
        def ingestionId1= responseMeasure.ingestionId

        conditions.eventually {
            def status = getStatus(ingestionId1)
            assert status.contains('COMPLETED')
        }

        conditions.setTimeout(30)
        //ensure node is ingested
        conditions.eventually {
            def nodeText = getHttpClient(header, tmpGetNodeURLPrefix + nodeId, null, HttpMethod.GET).body
            System.out.println(tmpGetNodeURLPrefix + nodeId)
            def nodeRetrieved = new JsonSlurper().parseText(nodeText).get('data').get(0)
            assert nodeRetrieved != null
        }

        def newDateStr = DateUtils.getDateStrWithIncrementalMs(new Date(), 5000000, "yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        def jsonDeliveries = new JsonSlurper().parseText(sampleDelivery);

        jsonDeliveries.getAt(0).put('plannedDeliveryDate', newDateStr);
        jsonDeliveries.getAt(0).put('predictedDeliveryDate', newDateStr);
        def modifiedSampleDelivery = new ObjectMapper().writeValueAsString(jsonDeliveries)

        ingestData(modifiedSampleDelivery)

        def deliveryId = getId(modifiedSampleDelivery, Delivery.class);

        //def retrievedMeasureDeliveryId0 = null;
        def retrievedMeasureDeliveryId1 = null;

        conditions.setTimeout(60)
        conditions.eventually {
            def nodeText = getHttpClient(header, tmpGetNodeURLPrefix + nodeId, null, HttpMethod.GET).body
            println(tmpGetNodeURLPrefix + nodeId)
            def nodeRetrieved = new JsonSlurper().parseText(nodeText).get('data').get(0)

            nodeRetrieved != null
            def measures = nodeRetrieved.get('measureMetaData');
            assert measures != null

            //def predictedArrivalsTsds = measures.get('predictedArrivals');
            //assert predictedArrivalsTsds != null
            //assert predictedArrivalsTsds.size() > 0
            //def predictedArrivals = predictedArrivalsTsds.get(0);

            def inTransitsTsds = measures.get('inTransits');
            assert inTransitsTsds != null
            assert inTransitsTsds.size() > 0
            def inTransits = inTransitsTsds
            assert inTransits != null

            //retrievedMeasureDeliveryId0 = predictedArrivals.get('deliveryInternalId');
            retrievedMeasureDeliveryId1 = inTransits.get('deliveryInternalId');
            assert retrievedMeasureDeliveryId1 == deliveryId
        }

        then: 'checked node measures of inTransits with delivery id in it .'
        //retrievedMeasureDeliveryId0 == deliveryId
        retrievedMeasureDeliveryId1 == deliveryId
    }*/

    def "Verify showcase data node computation are triggered"() {
        given:
        def conditions = new PollingConditions(timeout: 60, initialDelay: 1)
        conditions.setDelay(10)

        when: "check node computation"
        def url = "?query=customerItemName=AC-FG-006;shipToSiteName=AC_DC06"
        def response
        conditions.eventually{
            response = executeHttpCall(header, tmpGetNodeURLPrefix + url, null, HttpMethod.GET)
            println(tmpGetNodeURLPrefix + url)
            assert response != null
        }
        def nodeRetrieved = response['data'][0]
        def measures = nodeRetrieved.get('measures');
        def inTransits = measures.get('inTransits');

        then:'checked node measures of inTransits with delivery id in it .'
        inTransits[0].get('deliveryInternalId') != null
    }
}
