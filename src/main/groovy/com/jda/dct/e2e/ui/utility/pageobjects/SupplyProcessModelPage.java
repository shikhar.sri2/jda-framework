package com.jda.dct.e2e.ui.utility.pageobjects;

import com.jda.dct.e2e.ui.utility.action.CommonActions;
import com.jda.dct.e2e.ui.utility.common.BasePage;
import com.jda.dct.e2e.ui.utility.helper.Action.ActionHelper;
import com.jda.dct.e2e.ui.utility.helper.Wait.WaitHelper;
import com.jda.dct.e2e.ui.utility.helper.genericHelper.GenericHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;


public class SupplyProcessModelPage extends BasePage {
    WebDriver driver;
    WebDriverWait webDriverWait;
    WaitHelper objWaitHelper;
    GenericHelper objGenericHelper;
    CommonActions commonActions;
    ActionHelper actionHelper;

    public SupplyProcessModelPage(WebDriver webdriver) {
        this.driver = webdriver;
        PageFactory.initElements(driver, this);
        webDriverWait = new WebDriverWait(driver, 30);
        objWaitHelper = PageFactory.initElements(driver, WaitHelper.class);
        objGenericHelper = PageFactory.initElements(driver, GenericHelper.class);
        commonActions = new CommonActions(driver);
        actionHelper = new ActionHelper(driver);
    }

    @FindBy(id = "PanelSummary-RenderedItem-Supplier-1-LinkItem")
    private WebElement supplier;

    @FindBy(xpath = "//p[contains(text(),'ACOEM01')]")
    private WebElement suppliertext;

    @FindBy(xpath = "(//*[@id='PanelSummary-RenderedItem-Customer-1'])[2]")
    private WebElement customer;

    @FindBy(xpath = "(//p[contains(text(),'ACOEM01')])[2]")
    private WebElement customertext;

    @FindBy(id = "PanelSummary-RenderedItem-Carrier-1")
    private WebElement carrier;

    @FindBy(xpath = "//p[contains(text(),'BORCHARD LINE LTD.')]")
    private WebElement carriertext;


    @FindBy(id = "PanelSummary-RenderedItem-BOL-No-1")
    private WebElement bolNo;

    @FindBy(xpath = "//p[contains(text(),\"BORU278930\")]")
    private WebElement boltext;

    @FindBy(id = "PanelSummary-RenderedItem-Planned-Ship Date-1-DateItem")
    private WebElement plannedShipmentDate;

    @FindBy(xpath = "//p[contains(text(),\"ACOEM01\")]")
    private WebElement plannedShipmentDatetext;

    @FindBy(id = "PanelSummary-RenderedItem-State-1")
    private WebElement state;

    @FindBy(xpath = "//p[contains(text(),\"New\")]")
    private WebElement stateText;

    @FindBy(id = "PanelSummary-RenderedItem-LMD-1-DateItem")
    private WebElement lmd;

    @FindBy(xpath = "//*[@id=\"PanelSummary-RenderedItem-LMD-1-DateItem\"]/span")
    private WebElement lmdText;

    @FindBy(id = "PanelSummary-RenderedItem-ETA-1-DateItem")
    private WebElement eta;

    @FindBy(xpath = "//p[contains(text(),\"ACOEM01\")]")
    private WebElement etaDate;

    @FindBy(id = "PanelSummary-RenderedItem-PTA-1-DateItem")
    private WebElement pta;

    @FindBy(xpath = "//p[contains(text(),\"ACOEM01\")]")
    private WebElement ptaDate;

    @FindBy(id = "PanelSummary-RenderedItem-Creation-Date-1-DateItem")
    private WebElement creationDate;

    @FindBy(xpath = "//*[@id=\"PanelSummary-RenderedItem-Creation-Date-1-DateItem\"]/span")
    private WebElement creationDateText;


    public void fieldvalidation() {
        supplier.isDisplayed();
        suppliertext.isDisplayed();
        carrier.isDisplayed();
        carriertext.isDisplayed();
        customer.isDisplayed();
        customertext.isDisplayed();
        bolNo.isDisplayed();
        boltext.isDisplayed();
        state.isDisplayed();
        stateText.isDisplayed();
        eta.isDisplayed();
        etaDate.isDisplayed();
        pta.isDisplayed();
        lmd.isDisplayed();
        lmdText.isDisplayed();
        plannedShipmentDate.isDisplayed();
        plannedShipmentDatetext.isDisplayed();
        creationDate.isDisplayed();
        creationDateText.isDisplayed();

    }


}
