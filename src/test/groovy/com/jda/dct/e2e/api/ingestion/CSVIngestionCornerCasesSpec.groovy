package com.jda.dct.e2e.api.ingestion

import com.jda.dct.e2e.api.categories.Regression
import com.jda.dct.e2e.api.utility.IngestionHelper
import groovy.json.JsonSlurper
import org.junit.experimental.categories.Category
import org.springframework.http.HttpMethod

import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll


/**
 * test corner cases in ingesting csv files.
 *
 * @author Vida Bandara
 * @version 1.0-SNAPSHOT
 * @since 24-May-2018
 */


@Category(Regression)
class CSVIngestionCornerCasesSpec extends Specification implements IngestionHelper {

    @Shared deliveryURL

    def setupSpec() {
        init()

        def deliveryPath = properties.getProperty('ingestDeliveriesPath')
        def getPath = properties.getProperty('getDeliveryPath')

        deliveryURL = protocol + ip + deliveryPath + '?processType=tempraturecontrolled&deliveryType=inboundDelivery'
        getURLPrefix = protocol + ip + getPath
    }

    def "ingest deliveries with a hash character in header column"() {
        given: "csv file with a hash character header column"
        def filePath = getTmpDir() + 'sample_delivery.csv'
        new File(filePath).newWriter().withWriter {
            it << 'Process Type,Delivery Type,Delivery No,Customer,Customer Name,Supplier,Supplier Name,State,Sub State,Days Delayed,Creation Date,Planned Ship Date,Actual Ship Date,ETA,PTA,Actual Delivery Date,Ship From,Ship From(Site),Ship To,Ship To Owner,Ship To Name,Ship Mode,Experiment #,HAWB No,Carrier,Discharge port,Freight Terms ,Container No,Packing Slip No,Line No,Item,Item Name,Commodity Code,Material Class,Planner Code,Quantity,Shipping UOM,Order UOM,PO,PO Line,Delivery Line LMD,Line State,PO No\n' +
                    'tempraturecontrolled,inboundDelivery,183175501,MB-USA,Mercedez Benz USA,7050000006_US01,Supplier Site for Car parts,Shipped,Intransit,0,4/25/18 8:00,4/25/18 8:00,,6/11/18 8:00,6/11/18 8:00,,US01,GLC,US06,US06 Owner,Robbinsville,Ocean,MB125B3,,Hapag Lloyd,Antwerp,EXW,2040NLZS2,,10,A0000780392,REGULATOR FUEL INJCT PRSR,MEDIUM,B02-00-GENERAL PARTS,7050000006_A07,200,EA,EA,4503943297,10,,State1,123\n' +
                    'tempraturecontrolled,inboundDelivery,183175545,MB-USA,Mercedez Benz USA,7050000006_US01,Supplier Site for Car parts,Shipped,Delayed,1,4/25/18 8:00,4/25/18 8:00,,6/11/18 8:00,6/12/18 8:00,,US01,GLC,US06,US06 Owner,Robbinsville,Ocean,MB125B4,,Hapag Lloyd,Antwerp,EXW,2040NM42Y,,10,A000466090080,REMAN POWER STEERING PUMP,MEDIUM,B02-99-RM PARTS,7050000006_A15,200,EA,EA,4503947438,10,,State2,123'
        }

        def json = '[{"tid":"a4b7a825f67930965747445709011120","processType":"tempraturecontrolled","deliveryType":"inboundDelivery","deliveryId":"183175501","customerName":"MB-USA","customerDescription":"Mercedez Benz USA","supplierName":"7050000006_US01","supplierDescription":"Supplier Site for Car parts","state":"Shipped","subState":"Intransit","daysDelayed":0,"deliveryCreationDateInternal":"2018-04-25","plannedShipDate":"2018-04-25","actualShipDate":"","plannedDeliveryDate":"2018-06-11","predictedDeliveryDate":"2018-06-11","actualDeliveryDate":"","shipFromSiteName":"US01","shipFromSiteDescription":"GLC","shipToSiteName":"US06","shipToSiteOwnerName":"US06 Owner","shipToSiteDescription":"Robbinsville","shipMode":"Ocean","bolNumber":"MB125B3","hawbNumber":"","carrier":"Hapag Lloyd","dischargePort":"Antwerp","freightTerms":"EXW","containerNumber":"2040NLZS2","PackingslipNumber":"","deliveryLines":{"line 10": {"deliveryLineId":"10", "supplierItemName":"A0000780392","supplierItemDescription":"REGULATOR FUEL INJCT PRSR","commodityCode":"MEDIUM","materialGroup":"B02-00-GENERAL PARTS","plannerCode":"7050000006_A07","shippedQuantity":500,"shippingUom":"EA","orderUom":"EA","poNumber":"4503943297","poLineNumber":"10","Delivery Line LMD":""}}}]'
        def id = getIds(json).get(0)
        def url = getURLPrefix + id

        when: "file ingested"
        def response = postFile(filePath, deliveryURL, null)
        def ingestionId = getIngestionId(response)
        def status = waitUntilComplete(ingestionId)

        def deliveryText = getHttpClient(header, url, null, HttpMethod.GET).body
        def deliveryRetrieved = new JsonSlurper().parseText(deliveryText).get('data').get(0)

        then: "hash character header column parsed"
        status.contains('COMPLETED')
        deliveryRetrieved.get('experimentNumber') == 'MB125B3'
    }

    @Unroll
    def "ingest deliveries with quotation mark ' #quotation ' to declared field"() {
        given: "csv file with a hash character header column"
        def filePath = getTmpDir() + 'sample_delivery.csv'
        new File(filePath).newWriter().withWriter {
            it << '\'Process Type,Delivery Type,Delivery No,Customer,Customer Name,Supplier,Supplier Name,State,Sub State,Days Delayed,Creation Date,Planned Ship Date,Ship Date,PTA,ETA,ATA,Ship From,Ship From(Site),Ship To,Ship To Owner,Ship To Site Name,Ship Mode,Experiment #,HAWB No,Carrier,Discharge port,Freight Terms ,Container No,Packing Slip No,Line No,Item,Item Name,Commodity Code,Material Class,Planner Code,Quantity,Shipping UOM,Order UOM,PO No,PO Line,Line LMD,Line State\n' +
                    'tempraturecontrolled,inboundDelivery,183175501,MB-USA,Mercedez' + quotation + ' Benz USA,7050000006_US01,Supplier Site for Car parts,Shipped,Intransit,0,4/25/18 8:00,4/25/18 8:00,,6/11/18 8:00,6/11/18 8:00,,US01,GLC,US06,US06 Owner,Robbinsville,Ocean,MB125B3,,Hapag Lloyd,Antwerp,EXW,2040NLZS2,,10,A0000780392,REGULATOR FUEL INJCT PRSR,MEDIUM,B02-00-GENERAL PARTS,7050000006_A07,500,EA,EA,4503943297,10,,State1\n' +
                    'tempraturecontrolled,inboundDelivery,183175545,MB-USA,Mercedez Benz USA,7050000006_US01,Supplier Site for Car parts,Shipped,Intransit,0,4/25/18 8:00,4/25/18 8:00,,6/11/18 8:00,6/11/18 8:00,,US01,GLC,US06,US06 Owner,Robbinsville,Ocean,MB125B3,,Hapag Lloyd,Antwerp,EXW,2040NLZS2,,10,A0000780392,REGULATOR FUEL INJCT PRSR,MEDIUM,B02-00-GENERAL PARTS,7050000006_A07,500,EA,EA,4503943297,10,,State1'
        }

        def json = '[{"tid":"a4b7a825f67930965747445709011120","processType":"tempraturecontrolled","deliveryType":"inboundDelivery","deliveryId":"183175501","customerName":"MB-USA","customerDescription":"Mercedez' + escaped + ' Benz USA","supplierName":"7050000006_US01","supplierDescription":"Supplier Site for Car parts","state":"Shipped","subState":"Intransit","daysDelayed":0,"deliveryCreationDateInternal":"2018-04-25","plannedShipDate":"2018-04-25","actualShipDate":"","plannedDeliveryDate":"2018-06-11","predictedDeliveryDate":"2018-06-11","actualDeliveryDate":"","shipFromSiteName":"US01","shipFromSiteDescription":"GLC","shipToSiteName":"US06","shipToSiteOwnerName":"US06 Owner","shipToSiteDescription":"Robbinsville","shipMode":"Ocean","bolNumber":"MB125B3","hawbNumber":"","carrier":"Hapag Lloyd","dischargePort":"Antwerp","freightTerms":"EXW","containerNumber":"2040NLZS2","PackingslipNumber":"","deliveryLines":{"line 10": {"deliveryLineId":"10","supplierItemName":"A0000780392","supplierItemDescription":"REGULATOR FUEL INJCT PRSR","commodityCode":"MEDIUM","materialGroup":"B02-00-GENERAL PARTS","plannerCode":"7050000006_A07","shippedQuantity":500,"shippingUom":"EA","orderUom":"EA","poNumber":"4503943297","poLineNumber":"10","Delivery Line LMD":""}}}]'
        def id = getIds(json).get(0)
        def url = getURLPrefix + id

        when: "file ingested"
        def response = postFile(filePath, deliveryURL, null)
        def ingestionId = getIngestionId(response)
        def status = waitUntilComplete(ingestionId)

        def deliveryText = getHttpClient(header, url, null, HttpMethod.GET).body
        def deliveryRetrieved = new JsonSlurper().parseText(deliveryText).get('data').get(0)

        then: "hash character header column parsed"
        status.contains('COMPLETED')
        deliveryRetrieved.get('customerDescription') == 'Mercedez' + quotation + ' Benz USA'

        where: "different quotation marks"
        quotation | escaped
        '\''      | '\''
        '\"'      | '\\\"'
    }

    def "ingest deliveries with escaped quotation mark"() {
        given: "csv file with a hash character header column"
        def filePath = getTmpDir() + 'sample_delivery.csv'
        new File(filePath).newWriter().withWriter {
            it << '\'Process Type,Delivery Type,Delivery No,Customer,Customer Name,Supplier,Supplier Name,State,Sub State,Days Delayed,Creation Date,Planned Ship Date,Ship Date,PTA,ETA,ATA,Ship From,Ship From(Site),Ship To,Ship To Owner,Ship To Site Name,Ship Mode,Experiment #,HAWB No,Carrier,Discharge port,Freight Terms ,Container No,Packing Slip No,Line No,Item,Item Name,Commodity Code,Material Class,Planner Code,Quantity,Shipping UOM,Order UOM,PO No,PO Line,Line LMD,Line State\n' +
                    'tempraturecontrolled,inboundDelivery,183175501,MB-USA,"Mercedez \\"Benz USA",7050000006_US01,Supplier Site for Car parts,Shipped,Intransit,0,4/25/18 8:00,4/25/18 8:00,,6/11/18 8:00,6/11/18 8:00,,US01,GLC,US06,US06 Owner,Robbinsville,Ocean,MB125B3,,Hapag Lloyd,Antwerp,EXW,2040NLZS2,,10,A0000780392,REGULATOR FUEL INJCT PRSR,MEDIUM,B02-00-GENERAL PARTS,7050000006_A07,500,EA,EA,4503943297,10,,State1\n' +
                    'tempraturecontrolled,inboundDelivery,183175545,MB-USA,Mercedez Benz USA,7050000006_US01,Supplier Site for Car parts,Shipped,Intransit,0,4/25/18 8:00,4/25/18 8:00,,6/11/18 8:00,6/11/18 8:00,,US01,GLC,US06,US06 Owner,Robbinsville,Ocean,MB125B3,,Hapag Lloyd,Antwerp,EXW,2040NLZS2,,10,A0000780392,REGULATOR FUEL INJCT PRSR,MEDIUM,B02-00-GENERAL PARTS,7050000006_A07,500,EA,EA,4503943297,10,,State1'
        }

        def json = '[{"tid":"a4b7a825f67930965747445709011120","processType":"tempraturecontrolled","deliveryType":"inboundDelivery","deliveryId":"183175501","customerName":"MB-USA","customerDescription":"Mercedez Benz USA","supplierName":"7050000006_US01","supplierDescription":"Supplier Site for Car parts","state":"Shipped","subState":"Intransit","daysDelayed":0,"deliveryCreationDateInternal":"2018-04-25","plannedShipDate":"2018-04-25","actualShipDate":"","plannedDeliveryDate":"2018-06-11","predictedDeliveryDate":"2018-06-11","actualDeliveryDate":"","shipFromSiteName":"US01","shipFromSiteDescription":"GLC","shipToSiteName":"US06","shipToSiteOwnerName":"US06 Owner","shipToSiteDescription":"Robbinsville","shipMode":"Ocean","bolNumber":"MB125B3","hawbNumber":"","carrier":"Hapag Lloyd","dischargePort":"Antwerp","freightTerms":"EXW","containerNumber":"2040NLZS2","PackingslipNumber":"","deliveryLines":{"line 10": {"deliveryLineId":"10","supplierItemName":"A0000780392","supplierItemDescription":"REGULATOR FUEL INJCT PRSR","commodityCode":"MEDIUM","materialGroup":"B02-00-GENERAL PARTS","plannerCode":"7050000006_A07","shippedQuantity":500,"shippingUom":"EA","orderUom":"EA","poNumber":"4503943297","poLineNumber":"10","Delivery Line LMD":""}}}]'
        def id = getIds(json).get(0)
        def url = getURLPrefix + id

        when: "file ingested"
        def response = postFile(filePath, deliveryURL, null)
        def ingestionId = getIngestionId(response)
        def status = waitUntilComplete(ingestionId)

        def deliveryText = getHttpClient(header, url, null, HttpMethod.GET).body
        def deliveryRetrieved = new JsonSlurper().parseText(deliveryText).get('data').get(0)

        then: "hash character header column parsed"
        status.contains('COMPLETED')
        deliveryRetrieved.get('customerDescription') == 'Mercedez "Benz USA'
    }

    @Unroll
    def "ingest deliveries with quotation mark ' #quotation ' to extensible field"() {
        given: "csv file with a hash character header column"
        def filePath = getTmpDir() + 'sample_delivery.csv'
        new File(filePath).newWriter().withWriter {
            it << 'Process Type,Delivery Type,Delivery No,Customer,Customer Name,Supplier,Supplier Name,State,Sub State,Days Delayed,Creation Date,Planned Ship Date,Ship Date,PTA,ETA,ATA,Ship From,Ship From(Site),Ship To,Ship To Owner,Ship To Site Name,Ship Mode,Experiment #,HAWB No,Carrier,Discharge port,Freight Terms ,Container No,Packing Slip No,Line No,Item,Item Name,Commodity Code,Material Class,Planner Code,Quantity,Shipping UOM,Order UOM,PO No,PO Line,Line LMD,Line State\n' +
                    'tempraturecontrolled,inboundDelivery,183175501,MB-USA,Mercedez Benz USA,7050000006_US01,Supplier Site for Car parts,Shipped,Intransit,0,4/25/18 8:00,4/25/18 8:00,,6/11/18 8:00,6/11/18 8:00,,US01,GLC,US06,US06 Owner,Robbinsville,Ocean,MB125B3,,Hapag' + quotation + ' Lloyd,Antwerp,EXW,2040NLZS2,,10,A0000780392,REGULATOR FUEL INJCT PRSR,MEDIUM,B02-00-GENERAL PARTS,7050000006_A07,500,EA,EA,4503943297,10,,State1\n' +
                    'tempraturecontrolled,inboundDelivery,183175545,MB-USA,Mercedez Benz USA,7050000006_US01,Supplier Site for Car parts,Shipped,Intransit,0,4/25/18 8:00,4/25/18 8:00,,6/11/18 8:00,6/11/18 8:00,,US01,GLC,US06,US06 Owner,Robbinsville,Ocean,MB125B3,,Hapag Lloyd,Antwerp,EXW,2040NLZS2,,10,A0000780392,REGULATOR FUEL INJCT PRSR,MEDIUM,B02-00-GENERAL PARTS,7050000006_A07,500,EA,EA,4503943297,10,,State1'
        }

        def json = '[{"tid":"a4b7a825f67930965747445709011120","processType":"tempraturecontrolled","deliveryType":"inboundDelivery","deliveryId":"183175501","customerName":"MB-USA","customerDescription":"Mercedez Benz USA","supplierName":"7050000006_US01","supplierDescription":"Supplier Site for Car parts","state":"Shipped","subState":"Intransit","daysDelayed":0,"deliveryCreationDateInternal":"2018-04-25","plannedShipDate":"2018-04-25","actualShipDate":"","plannedDeliveryDate":"2018-06-11","predictedDeliveryDate":"2018-06-11","actualDeliveryDate":"","shipFromSiteName":"US01","shipFromSiteDescription":"GLC","shipToSiteName":"US06","shipToSiteOwnerName":"US06 Owner","shipToSiteDescription":"Robbinsville","shipMode":"Ocean","bolNumber":"MB125B3","hawbNumber":"","carrier":"Hapag' + escaped + ' Lloyd","dischargePort":"Antwerp","freightTerms":"EXW","containerNumber":"2040NLZS2","PackingslipNumber":"","deliveryLines":{"line 10": {"deliveryLineId":"10","supplierItemName":"A0000780392","supplierItemDescription":"REGULATOR FUEL INJCT PRSR","commodityCode":"MEDIUM","materialGroup":"B02-00-GENERAL PARTS","plannerCode":"7050000006_A07","shippedQuantity":500,"shippingUom":"EA","orderUom":"EA","poNumber":"4503943297","poLineNumber":"10","Delivery Line LMD":""}}}]'
        def id = getIds(json).get(0)
        def url = getURLPrefix + id

        when: "file ingested"
        def response = postFile(filePath, deliveryURL, null)
        def ingestionId = getIngestionId(response)
        def status = waitUntilComplete(ingestionId)

        def deliveryText = getHttpClient(header, url, null, HttpMethod.GET).body
        def deliveryRetrieved = new JsonSlurper().parseText(deliveryText).get('data').get(0)

        then: "hash character header column parsed"
        status.contains('COMPLETED')
        deliveryRetrieved.get('carrier') == 'Hapag' + quotation + ' Lloyd'

        where: "different quotation marks"
        quotation | escaped
        '\''      | '\''
        '\"'      | '\\\"'
    }
}