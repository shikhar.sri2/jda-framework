package com.jda.dct.e2e.api.utility;

import java.util.List;

public class QueryObjects {
    private List<QueryObject> queryObjects;

    public List<QueryObject> getQueryObjects() {
        return queryObjects;
    }

    public void setQueryObjects(List<QueryObject> queryObjects) {
        this.queryObjects = queryObjects;
    }

}
