/**
 *
 */
package com.jda.dct.e2e.ui.utility.helper.Resource;


import com.jda.dct.e2e.ui.utility.common.BaseTest;

import java.io.File;
import java.io.FileInputStream;
        import java.io.FileNotFoundException;
        import java.io.InputStream;


public class ResourceHelper extends BaseTest {

    public static String getResourcePath(String resource) {
        String path = getBaseResourcePath() + File.separator + resource;
        return path;
    }

    public static String getBaseResourcePath() {
        String path = System.getProperty("user.dir");
        return path;
    }

    public static InputStream getResourcePathInputStream(String path)
            throws FileNotFoundException {
        return new FileInputStream(ResourceHelper.getResourcePath(path));
    }

}
