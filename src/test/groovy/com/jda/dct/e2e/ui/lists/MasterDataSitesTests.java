package com.jda.dct.e2e.ui.lists;

import com.jda.dct.e2e.ui.utility.common.BaseTest;
import com.jda.dct.e2e.ui.utility.common.LeftMenu;

import com.jda.dct.e2e.ui.utility.pageobjects.MasterDataSitesPage;
import com.jda.dct.e2e.ui.utility.pageobjects.TopMenuPage;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class MasterDataSitesTests extends BaseTest {

    private LeftMenu leftMenu;
    private TopMenuPage topMenu;
    private WebDriver driver;
    private MasterDataSitesPage masterDataSites;

    @BeforeClass
    public void setUp() throws InterruptedException {
        System.out.println("Starting tests for :" + this.getClass().getName());
        //Initialize required objects
        driver = getDriver();
        leftMenu = new LeftMenu(driver);
        topMenu = new TopMenuPage(driver);
        masterDataSites = new MasterDataSitesPage(driver);
        //loginAndNavigateToMasterDataLandingPage
        logInToApplication();

    }

    @BeforeMethod
    public void commonMethods() throws InterruptedException {
        //navigates to Demand Page
        leftMenu.clickMasterDataMenuOption();
    }

    @Test(priority = 1,retryAnalyzer = BaseTest.class)
    public void validateMasterDataSitesListPageIsDisplayed() {
        //navigates to Master-Data Item Tab

        topMenu.goToMasterDataSitesTab();
        //verify user is at MasterDataSitesListPage
        Assert.assertTrue(masterDataSites.isMasterDataSitesPageDisplayed());

    }

}