package com.jda.dct.e2e.api.ums

import com.jda.dct.e2e.api.categories.Regression
import com.jda.dct.e2e.api.utility.IngestionHelper
import org.apache.commons.lang.ObjectUtils
import org.junit.experimental.categories.Category
import org.springframework.http.HttpMethod
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Stepwise

/**
 * End-to-end tests for adding new tenant.
 */
@Stepwise
class TenantsSpec extends Specification implements IngestionHelper {
    @Shared
            newTid
    @Shared
            sampleTenants
    @Shared
            tenantsURLPrefix

    def setupSpec() {
        init()

        def tenantsPath = properties.getProperty('tenantsPath')
        tenantsURLPrefix  = protocol + ip + tenantsPath
        sampleTenants = this.getClass().getClassLoader().getResource('sample-tendant.json').text
    }

    // what is the usecase for adding new tenants
    @Category(Regression)
    def "create a new tenant"() {
        when: 'post tenant json'
        println tenantsURLPrefix
        def response = executeHttpCall(header, tenantsURLPrefix, sampleTenants, HttpMethod.POST)
        newTid = response.data[0].tid
        println("new Tid: " + newTid)

        then: 'a valid tenant id is returned'
        response.data[0].tid != ObjectUtils.Null
    }

    @Category(Regression)
    def "verify the new tenant object exists in tenants list"() {
        when: 'get list of existing tenants'
        def response = executeHttpCall(header, tenantsURLPrefix, null, HttpMethod.GET)

        then: 'new tenants is added'
        isTidExisting(response.data)
    }

    @Category(Regression)
    def "delete the new added tenant object"() {
        given:'delete new added tenant'
        def deleteURL = tenantsURLPrefix  + newTid
        println("deleting tenant: " + newTid)
        getHttpClient(header, deleteURL, null, HttpMethod.DELETE).body

        when: 'get list of existing tenants'
        def response = executeHttpCall(header, tenantsURLPrefix, null, HttpMethod.GET)

        then: 'the new added tenant is removed'
        !isTidExisting(response.data)
    }

    def isTidExisting(tenants)
    {
        for(rec in tenants )
            if( rec.tid == newTid)
                return true
        return false
    }

}