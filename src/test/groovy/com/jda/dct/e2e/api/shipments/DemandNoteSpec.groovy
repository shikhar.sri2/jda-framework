package com.jda.dct.e2e.api.shipments

import com.jda.dct.e2e.api.categories.Regression
import com.jda.dct.e2e.api.utility.IngestionHelper
import com.jda.dct.e2e.api.utility.QueryObject
import com.jda.dct.e2e.api.utility.QueryResultHandler
import org.junit.experimental.categories.Category
import org.springframework.http.HttpMethod
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll

class DemandNoteSpec extends Specification implements IngestionHelper {

    static postUrl
    @Shared
    static randomNote

    def setupSpec() {
        init()

        def postPath = properties.getProperty('postShipmentNotesPath')
        postUrl = protocol + ip + postPath
    }

    @Category(Regression)
    @Unroll
    def "persist a note"(String shipmentId, String label) {

        given: "shipment Id : #shipmentId"

        def queryObject = new QueryObject()
        queryObject.setType("shipments")
        queryObject.addQuery("shipmentId", shipmentId)
//      queryObject.addQuery("shipmentType", "outboundShipment")
        def response = QueryResultHandler.getQueryResultHandler().execute(queryObject)


        response.body.data[0].id != null
        def Id = response.body.data[0].id

        when: "persist a note for the shipment"
        def generator = { String alphabet, int n ->
            new Random().with {
                (1..n).collect { alphabet[nextInt(alphabet.length())] }.join()
            }
        }

        randomNote = generator((('A'..'Z') + ('0'..'9')).join(), 9)
//        randomNote = "$" + " " + randomNote
//        randomNote = "!@#%^&*" + " " + randomNote
//        randomNote = randomNote + " "
//        randomNote = '$'+"500"

        println randomNote
        def sampleNote = '{ "note":"' + randomNote + '"}'
        def postRequest = postUrl + Id

        def response1 = executeHttpCall(header, postRequest, sampleNote, HttpMethod.POST)

        then: "response should have note details"
        response1.note == randomNote

        where:
        shipmentId     | label
        "OS0005_SE_S2" | "Shipment 1"
        "OS0012_SO_S1" | "Shipment 2"
        "OS0028_SD_S1" | "Shipment 3"
        "OS0007_SD_S1" | "Shipment 4"
    }
}
