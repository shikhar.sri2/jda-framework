package com.jda.dct.e2e.ui.utility.action;

import com.jda.dct.e2e.ui.utility.common.BaseTest;
import com.jda.dct.e2e.ui.utility.helper.Wait.WaitHelper;
import com.jda.dct.e2e.ui.utility.helper.genericHelper.GenericHelper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.Date;
import java.util.List;

public class LinkCheckerActions extends BaseTest {

    WebDriver driver;
    WaitHelper objWaitHelper;
    GenericHelper objGenericHelper;
    int reCheckCount = 0;

    public LinkCheckerActions(WebDriver webdriver) {

        this.driver = webdriver;
        PageFactory.initElements(driver, this);
        objWaitHelper = PageFactory.initElements(driver, WaitHelper.class);
        objGenericHelper = PageFactory.initElements(driver, GenericHelper.class);
    }

    @FindBy (xpath = "//*[@id='app']//img")
    private WebElement luminateIcon;

    public boolean ExtractJSLogs(String level, List<String> removeLinks) {
        try {
            Thread.sleep(10000);
            LogEntries logEntries = driver.manage().logs().get(LogType.BROWSER);
            int flag = 0;
            for (LogEntry entry : logEntries) {
                if(entry.getLevel().toString().equals(level)) {
                    if(removeLinks.size() == 0){
                        Reporter(new Date(entry.getTimestamp()) + " " + entry.getLevel() + " " + entry.getMessage(), "Fail");
                        flag ++;
                    }else {
                        for (int i = 0; i < removeLinks.size(); i++) {
                            String endPointURL = getInstanceURL() + removeLinks.get(i);
                            if (!entry.getMessage().contains(endPointURL)) {
                                Reporter(new Date(entry.getTimestamp()) + " " + entry.getLevel() + " " + entry.getMessage(), "Fail");
                                flag++;
                            }
                        }
                    }
                    System.out.println(new Date(entry.getTimestamp()) + " " + entry.getLevel() + " " + entry.getMessage());
                }
            }

            if(flag == 0){
                Reporter("No " + level + " logs found", "Pass");
                return true;
            }else {
                return false;
            }

        }catch (Exception e){
            e.printStackTrace();
            throw new RuntimeException("Exception while getting Console logs");
        }
    }

    public boolean ExtractJSLogs(String level) {
        try {
            Thread.sleep(10000);
            LogEntries logEntries = driver.manage().logs().get(LogType.BROWSER);
            int flag = 0;
            for (LogEntry entry : logEntries) {
                if(entry.getLevel().toString().equals(level)) {
                    Reporter(new Date(entry.getTimestamp()) + " " + entry.getLevel() + " " + entry.getMessage(), "Fail");
                    flag ++;
                }
            }

            if(flag == 0){
                Reporter("No " + level + " logs found", "Pass");
                return true;
            }else {
                return false;
            }

        }catch (Exception e){
            e.printStackTrace();
            throw new RuntimeException("Exception while getting Console logs");
        }
    }

    public boolean ExtractJSLogs() {
        try {
            Thread.sleep(10000);
            LogEntries logEntries = driver.manage().logs().get(LogType.BROWSER);
            for (LogEntry entry : logEntries) {
                    Reporter(new Date(entry.getTimestamp()) + " " + entry.getLevel() + " " + entry.getMessage(), "Info");

            }
            return true;
        }catch (Exception e){
            e.printStackTrace();
            throw new RuntimeException("Exceptin while getting Console logs");
        }
    }

    public void navigateTo(String url){
        try {
            String absoluteURL = String.format("https://%s", properties.getProperty("client.url")) + url;
            driver.get(absoluteURL);
            Reporter("User Navigated to '" + absoluteURL + "' successfully", "Pass");
            objWaitHelper.waitForPageToLoad();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public boolean checkForLuminateIcon(){
        return objGenericHelper.isDisplayed(luminateIcon, "Luminate Icon");
    }
}
