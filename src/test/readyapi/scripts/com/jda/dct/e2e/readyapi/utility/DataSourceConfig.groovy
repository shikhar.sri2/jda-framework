package com.jda.dct.e2e.readyapi.utility

import java.nio.file.Path

class DataSourceConfig {
    private context

    DataSourceConfig(def testCaseContext) {
        this.context = testCaseContext
    }

    void configureFileDataSourceStep(String dataSourceStepName, Path dataFilePath, List<String> properties) {
        def step = context.getTestStepByName(dataSourceStepName)
        // Remove existing properties
        step.removeProperties(Arrays.asList(step.getPropertyNames()))
        // Add properties from CSV
        step.addProperties(properties)
        step.getDataSource().setFileName(dataFilePath.toString())
        step.getDataSource().setCharset("utf-8")
    }
}
