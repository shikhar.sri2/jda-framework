package com.jda.dct.e2e.ui.lists;


import com.jda.dct.e2e.ui.utility.common.BaseTest;
import com.jda.dct.e2e.ui.utility.common.LeftMenu;
import com.jda.dct.e2e.ui.utility.helper.AssertHelper.AssertHelper;
import com.jda.dct.e2e.ui.utility.pageobjects.DemandDeliveryListPage;
import com.jda.dct.e2e.ui.utility.pageobjects.TopMenuPage;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


public class DemandDeliveryListTests extends BaseTest {

    private LeftMenu leftMenu;
    private TopMenuPage topMenu;
    private DemandDeliveryListPage demandDeliveryListPage;
    AssertHelper assertHelper;
    private WebDriver driver;


    @BeforeClass
    public void setUp() throws InterruptedException {
        //Initialize required objects
        driver = getDriver();
        leftMenu = new LeftMenu(driver);
        topMenu = new TopMenuPage(driver);
        demandDeliveryListPage = new DemandDeliveryListPage(driver);
        assertHelper = new AssertHelper(driver);
        //loginAndNavigateToDemandLandingPage
        logInToApplication();
    }

    @Test(priority = 1,retryAnalyzer = BaseTest.class)
    public void validateDemandDeliveryListPageIsDisplayed() throws InterruptedException {
        leftMenu.clickDemandMenuOption();
        // navigate to Delivery tab
        topMenu.gotoDemandDeliveryTab();
        demandDeliveryListPage.clickOceanIcon();
        //verify user is at demand Inventory List page
        assertHelper.assertTrue(demandDeliveryListPage.isDemandDeliveryListPageDisplayed(), "Demand delivery list page is displayed successfully");
    }
}