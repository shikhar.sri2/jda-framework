package com.jda.dct.e2e.api.utility

trait AuthHelper extends TestHelper {
    static header

    def protocol
    def ip
    Properties properties

    def init(asIntegrator) {
        def init = new Initialize()
        init.initConfig()

        def token = init.getAccessToken(asIntegrator)
        header = ['Authorization': "Bearer $token"]

        protocol = init.getProtocol()
        ip = init.getIP()

        def stream = this.getClass().getClassLoader().getResourceAsStream('endpoint.properties')
        properties = new Properties()
        properties.load(stream)
    }

}
