package com.jda.dct.e2e.ui.utility.pageobjects;

import java.util.*;
import java.lang.String;

import com.jda.dct.e2e.ui.utility.helper.Action.ActionHelper;
import com.jda.dct.e2e.ui.utility.helper.Wait.WaitHelper;
import com.jda.dct.e2e.ui.utility.helper.genericHelper.GenericHelper;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;


public class SortByPage {
    WebDriver driver;
    WebDriverWait webDriverWait;
    WaitHelper objWaitHelper;
    GenericHelper objGenericHelper;
    ActionHelper actionHelper;

    public SortByPage(WebDriver webdriver) {

        this.driver = webdriver;
        PageFactory.initElements(driver, this);
        webDriverWait = new WebDriverWait(driver, 90);
        objWaitHelper = PageFactory.initElements(driver, WaitHelper.class);
        objGenericHelper = PageFactory.initElements(driver, GenericHelper.class);
        actionHelper = PageFactory.initElements(driver, ActionHelper.class);

    }

    @FindBy(id ="ListContentHeader-supplierName")
    private WebElement supplierItem;
    @FindBy(id="ListContentHeader-bolNo")
    private WebElement bolItem;
    @FindBy(xpath ="//div[contains(@id,'PanelSummary-RenderedItem-BOL-No')]")
    private List<WebElement> bolShipmentList;

    @FindBy(xpath = "(//div[@style='font-size: 14px;']/span/span/following-sibling::*/*)[6]")
    private WebElement ascShipmentButton;

    @FindBy(xpath = "//div[@style='font-size: 14px;']/span/span[text()='Shipment No']/following-sibling::*/*")
    private WebElement descShipmentButton;


    //@FindBy(xpath = "//span[text()='Item']")
    @FindBy(id ="ListContentHeader-customerItemName")
    WebElement item;
    //@FindBy(xpath ="//span[@id='ListContentHeader-shipToSiteName')]")
    @FindBy(id ="ListContentHeader-shipToSiteName")
    private WebElement siteItem;
    @FindBy(xpath ="//div[contains(@id,'PanelSummary-RenderedItem-Site')]")
    private List<WebElement> siteItemList;
    @FindBy(xpath = "(//div[@style='font-size: 14px;']/span/span/following-sibling::*/*)[4]")
    private WebElement ascInventoryButton;
    @FindBy(xpath = "(//div[@style='font-size: 14px;']/span/span/following-sibling::*/*)[4]")
    private WebElement descInventoryButton;


    //Supply Shipment
    public void clickAscendingShipment() throws InterruptedException {
        //webDriverWait.until(ExpectedConditions.elementToBeClickable(bolItem));
        supplierItem.click();
        Thread.sleep(5000);

        if (ascShipmentButton.isDisplayed()) {
            webDriverWait.until(ExpectedConditions.elementToBeClickable(bolItem));
            Thread.sleep(8000);
            bolItem.click();
            Thread.sleep(5000);
            System.out.println("ascending list was displayed by default..");

        } else if (descShipmentButton.isDisplayed()) {
            System.out.println("ascending list is displayed after clicking the desc button at bol item..");
            webDriverWait.until(ExpectedConditions.elementToBeClickable(bolItem));
            Thread.sleep(5000);
            bolItem.click();
            System.out.println("clicked on bl item ....");
            Thread.sleep(8000);
            bolItem.click();
            Thread.sleep(8000);
        }

    }


    public void isAscendingShipmentListDisplayed() {

        String str1, str2;
        boolean blnOrder = false;
        System.out.println("List Size: " + bolShipmentList.size());
        for (int i = 0; i < bolShipmentList.size() - 1; i++) {
            str1 = bolShipmentList.get(i).getText();
            str2 = bolShipmentList.get(i + 1).getText();
            if (str1.compareToIgnoreCase(str2)<=0) {
                //blnOrder = false;
                System.out.println("String are : " + str1 + " -> " + str2);
                blnOrder = true;
            }
        }
        Assert.assertTrue(blnOrder);

    }


    public void clickDescendingShipment() throws InterruptedException {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(bolItem));
        supplierItem.click();
        Thread.sleep(5000);


        if (descShipmentButton.isDisplayed()) {
            webDriverWait.until(ExpectedConditions.elementToBeClickable(bolItem));
            bolItem.click();
            Thread.sleep(8000);
            System.out.println("ascending list is displayed after clicking the desc button at bol item..");

        } else if (ascShipmentButton.isDisplayed()) {
            System.out.println("clicking on bl item   ....");
            webDriverWait.until(ExpectedConditions.elementToBeClickable(bolItem));
            bolItem.click();
            System.out.println("clicked on bl item ....");
            Thread.sleep(5000);
            bolItem.click();
            Thread.sleep(5000);
        }
    }

    public void isDescendingShipmentListDisplayed() {

        String str1, str2;
        boolean blnOrder = false;
        System.out.println("List Size: " + bolShipmentList.size());
        for (int i = 0; i < bolShipmentList.size() - 1; i++) {
            str1 = bolShipmentList.get(i).getText();
            str2 = bolShipmentList.get(i + 1).getText();
            if (str1.compareToIgnoreCase(str2)>=0 ){
                blnOrder = false;
                System.out.println("String are : " + str1 + " -> " + str2);
                blnOrder = true;
            }
        }
        Assert.assertTrue(blnOrder);
    }


// inventory


    public void clickAscendingInventory() throws InterruptedException {
        objWaitHelper.waitElementToBeClickable(siteItem);
    objGenericHelper.elementClick(item,"Item");
       // objGenericHelper.elementClick(siteItem,"siteItem");


        if (ascInventoryButton.isDisplayed()) {
            objWaitHelper.waitElementToBeClickable(siteItem);
            objGenericHelper.elementClick(siteItem,"site");
            System.out.println("ascending list was displayed by default..");

        } else if (descInventoryButton.isDisplayed()) {
            System.out.println("ascending list is displayed after clicking the desc button at site item..");
            objWaitHelper.waitElementToBeClickable(siteItem);
            objGenericHelper.elementClick(siteItem,"site");
            System.out.println("clicked on site item ....");
            objWaitHelper.waitElementToBeClickable(siteItem);
            objGenericHelper.elementClick(siteItem,"site");
        }



 }


    public void clickDescendingInventory() throws InterruptedException {
        objWaitHelper.waitElementToBeClickable(item);
        objGenericHelper.elementClick(item,"Item");


        if (descInventoryButton.isDisplayed()) {
        objWaitHelper.waitElementToBeClickable(siteItem);
        objGenericHelper.elementClick(siteItem,"site");
        System.out.println("ascending list was displayed by default..");

    } else if (ascInventoryButton.isDisplayed()) {
        System.out.println("ascending list is displayed after clicking the desc button at site item..");
        objWaitHelper.waitElementToBeClickable(siteItem);
        objGenericHelper.elementClick(siteItem,"site");
        System.out.println("clicked on site item ....");
        objWaitHelper.waitElementToBeClickable(siteItem);
        objGenericHelper.elementClick(siteItem,"site");
    }
}

    public void isAscendingInventoryListDisplayed() {
        String str1, str2;
        boolean blnOrder = true;

        System.out.println("List Size: " + siteItemList.size());
        for (int i = 0; i < siteItemList.size() - 1; i++) {
            str1 = siteItemList.get(i).getText();
            str2 = siteItemList.get(i + 1).getText();
            if (str1.compareToIgnoreCase(str2) <=0) {
                blnOrder = false;
                System.out.println("String are : " + str1 + " -> " + str2);

            }
        }
        Assert.assertTrue(blnOrder);

    }

    public void isDescendingInventoryListDisplayed() {
        String str1, str2;
        boolean blnOrder = false;

        System.out.println("List Size: " + siteItemList.size());
        for (int i = 0; i < siteItemList.size() - 1; i++) {//prevents  out of boundexeption
            str1 = siteItemList.get(i).getText();
            str2 = siteItemList.get(i + 1).getText();
            if (str1.compareToIgnoreCase(str2) >=0) {
                System.out.println("String are : " + str1 + " -> " + str2);
                blnOrder = true;
            }
        }

        Assert.assertTrue(blnOrder);


    }

}





















