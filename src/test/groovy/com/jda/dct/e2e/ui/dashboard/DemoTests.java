package com.jda.dct.e2e.ui.dashboard;

import com.jda.dct.e2e.ui.utility.common.BaseTest;
import com.jda.dct.e2e.ui.utility.common.LeftMenu;
import com.jda.dct.e2e.ui.utility.pageobjects.*;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class DemoTests extends BaseTest {

    private LeftMenu leftMenu;
    private TopMenuPage topMenu;
    private TopSubMenuPage topSubMenu;
    private SupplyShipmentListPage supplyShipmentListPage;
    private DemoWorkFlow demoPage;
    private WebDriver driver;
    private InventoryListPage inventoryListPage;


    @BeforeClass
    public void setUp() throws InterruptedException {
        System.out.println("Starting tests for :" + this.getClass().getName());
        //Initialize required objects
        driver = getDriver();
        leftMenu = new LeftMenu(driver);
        topMenu = new TopMenuPage(driver);
        topSubMenu = new TopSubMenuPage(driver);
        supplyShipmentListPage = new SupplyShipmentListPage(this.driver);
        demoPage = new DemoWorkFlow(this.driver);
        inventoryListPage = new InventoryListPage(driver);
        //loginAndNavigateToSupplyShipmentsLandingPage
        logInToApplication();
        leftMenu.clickSupplyMenuOption();
    }


    //shipmentHotitems
    @Test(priority = 1)
    public void validateSupplyInventoryListPageIsDisplayedWhenShipmentHotItemsIsClicked() throws InterruptedException {

        String shipmentNo = "IS001_SD_S1";
        leftMenu.clickSupplyMenuOption();
        topMenu.goToSupplyShipmentsTab();
        topMenu.clickExceptionsTab();
        topMenu.clickSupplyShipmentExceptionsDelayed();
        supplyShipmentListPage.clickOceanIcon();
        topSubMenu.goToSearchFeature(shipmentNo);
        topMenu.clearFilterIfVisible();
        demoPage.clickSearchedShipmentNumber(shipmentNo);
        demoPage.clickShipmentHotitems();
        //verify that user lands on Inventory page
        //demoPage.clickFinishedGoods();
        topMenu.clickInventoryStockOut();
        Assert.assertTrue(demoPage.isItemOneDisplayed());
        Assert.assertTrue(demoPage.isItemTwoDisplayed());
        topMenu.clickInventoryLowStock();
        Assert.assertTrue(demoPage.isItemThreeDisplayed());
        Assert.assertTrue(demoPage.isItemFourDisplayed());
        driver.navigate().refresh();
    }


    @Test(priority = 2)
    public void validateInventoryDetailPageisDisplayed() throws InterruptedException {
        String shipmentNo = "IS001_SD_S1";
        leftMenu.clickSupplyMenuOption();
        topMenu.goToSupplyShipmentsTab();
        topMenu.clickExceptionsTab();
        topMenu.clickSupplyShipmentExceptionsDelayed();
        supplyShipmentListPage.clickOceanIcon();
        topSubMenu.clickSearchButton();
        topMenu.clearFilterIfVisible();
        topSubMenu.goToSearchFeature(shipmentNo);
        demoPage.clickSearchedShipmentNumber(shipmentNo);
        demoPage.clickShipmentAllitems();
      //verify that user lands on Inventory page
         // demoPage.clickFinishedGoods();
        topMenu.clickInventoryLowStock();
        Assert.assertTrue(demoPage.isItemThreeDisplayed());
        Assert.assertTrue(demoPage.isItemFourDisplayed());
        topMenu.clickInventoryStockOut();
        Assert.assertTrue(demoPage.isItemOneDisplayed());
        Assert.assertTrue(demoPage.isItemTwoDisplayed());

        // verify user lands at Inventory Details Page
        Assert.assertTrue(inventoryListPage.isInventoryDetailsPageDisplayed());
        demoPage.clickCompareButton();
        Assert.assertTrue(demoPage.isLocationOneDisplayed());
        Assert.assertTrue(demoPage.isLocationTwoDisplayed());
        Assert.assertTrue(demoPage.isLocationThreeDisplayed());
    }

    @Test(priority = 3)
    public void validateMonetaryImpactOnSalesAndDemand() throws InterruptedException {
        String shipmentNo = "IS001_SD_S1";
        leftMenu.clickSupplyMenuOption();
        topMenu.goToSupplyShipmentsTab();
        topMenu.clickExceptionsTab();
        topMenu.clickSupplyShipmentExceptionsDelayed();
        supplyShipmentListPage.clickOceanIcon();
        topSubMenu.clickSearchButton();
        topSubMenu.goToSearchFeature(shipmentNo);
        topMenu.clearFilterIfVisible();
        demoPage.clickSearchedShipmentNumber(shipmentNo);
        demoPage.clickShipmentAllitems();
        topMenu.clickInventoryLowStock();
        Assert.assertTrue(demoPage.isItemThreeDisplayed());
        Assert.assertTrue(demoPage.isItemFourDisplayed());
        topMenu.clickInventoryStockOut();
        Assert.assertTrue(demoPage.isItemOneDisplayed());
        Assert.assertTrue(demoPage.isItemTwoDisplayed());
        demoPage.clickItemone();
        Assert.assertTrue(inventoryListPage.isInventoryDetailsPageDisplayed());
        topMenu.clickInventoryMonetaryImpact();
        topMenu.clickInventoryMonetaryImpactSalesTab();
        topMenu.IsInventoryMonetaryImpactSalesAmountVisible();
        topMenu.clickInventoryMonetaryImpactDemandTab();
        topMenu.IsInventoryMonetaryImpactDemandAmountVisible();
        topMenu.clickInventoryMonetaryImpactSalesTab();
        topMenu.clickInventoryMonetaryImpactSalesOrderNumber();
        topMenu.IsInventoryMonetaryImpactSalesAmountVisible();
    }
}


