//package com.jda.dct.e2e.clientUi.pages.tempraturecontrolled
//
//import geb.Page
//
//class SupplyOrderListPage extends Page{
//    static url = "/tempraturecontrolled/purchase-orders"
//
//    static atCheckWaiting = true
//
//    static at = {
//        waitFor{
//            $("#tempraturecontrolled-page-tab-purchase-orders").isDisplayed()
//        }
//    }
//
//    static content = {
//        shipmentsButton(wait: true){
//            $("#tempraturecontrolled-page-tab-shipments")
//        }
//        inventoryButton(wait: true) {
//            $("#tempraturecontrolled-page-tab-inventory")
//        }
//        deliveryButton(wait: true) {
//            $("#tempraturecontrolled-page-tab-deliveries")
//        }
//    }
//
//    def goToShipment(){
//        shipmentsButton.click()
//        waitFor {
//            exceptionPage.getCurrentUrl().contains("shipment")
//        }
//    }
//
//    def goToInventory(){
//        inventoryButton.click()
//        waitFor {
//            exceptionPage.getCurrentUrl().contains("inventory")
//        }
//    }
//
//    def goToDelivery() {
//        deliveryButton.click()
//        waitFor {
//            exceptionPage.getCurrentUrl().contains("delivery")
//        }
//    }
//}
