package com.jda.dct.e2e.api.utility

import groovy.json.JsonSlurper

import org.springframework.http.ResponseEntity

class JdaApiResponse {

    def rawResponse
    def body

    public JdaApiResponse(ResponseEntity response) {
        this.rawResponse = response
        this.body = new JsonSlurper().parseText(this.rawResponse.body)
    }
}
