package com.jda.dct.e2e.api.sites

import com.jda.dct.e2e.api.categories.Smoke
import com.jda.dct.e2e.api.utility.AuthHelper
import groovyx.net.http.RESTClient
import org.junit.experimental.categories.Category
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll

class SitesAPISpec extends Specification implements AuthHelper {
    static sitesURL
    @Shared
    def client

    def setupSpec() {
        init(true)

        def sitesPath = properties.getProperty('getSitesPath')
        sitesURL = protocol + ip + sitesPath
        client = new RESTClient(sitesURL)
        client.handler.failure = { resp -> resp }
        header[HttpHeaders.CONTENT_TYPE] = MediaType.APPLICATION_JSON
    }

    // probably covered
    @Category(Smoke)
    @Unroll
    def "Get sites API #url"() {
        given: "The API URL is #url "
        when: "HTTP Method is GET"
        def response = client.get(headers: header)
        then: "the response should be 200"
        with(response) {
            status == 200
        }
        where:
        url << [sitesURL]
    }

    @Category(Smoke)
    def "integrator client should not be able to delete sites"() {
        when:"delete sites"
        def response = client.delete(headers: header)
        then: "the response should be 403"
        with(response) {
            status == 403
        }
    }
}
