package com.jda.dct.e2e.api.shipments

import com.jda.dct.e2e.api.categories.Regression
import com.jda.dct.e2e.api.utility.IngestionHelper
import com.jda.dct.e2e.api.utility.QueryObject
import com.jda.dct.e2e.api.utility.QueryResultHandler
import org.junit.experimental.categories.Category
import spock.lang.Specification

class ShipmentPageSizeSpec extends Specification implements IngestionHelper {

    def setupSpec() {
        init()
    }

    @Category(Regression)
    def "verify page size"() {
        given: "all inboundShipments"

        when: "searchText is Road and limit per page is 10 "
        def queryObject = new QueryObject()
        queryObject.setType("shipments")
        queryObject.setLimit(10)
        queryObject.addQuery("shipmentType", "inboundShipment")
        queryObject.addQuery("shipmentMode", "Road")
        def response = QueryResultHandler.getQueryResultHandler().executeWithRetry(queryObject, 20, 2, 2, 10, 10)

        then: "return object count should equal to page size"
        //  response.totalCount == response.count
        response.body.count==10
    }
}
