package com.jda.dct.e2e.api.purchaseOrders

import com.jda.dct.e2e.api.categories.Smoke
import com.jda.dct.e2e.api.utility.IngestionHelper
import com.jda.dct.e2e.api.utility.QueryObject
import com.jda.dct.e2e.api.utility.QueryResultHandler
import org.junit.experimental.categories.Category
import spock.lang.Specification
import spock.lang.Unroll

// smoke
class StandardPurchaseOrderPagingSpec extends Specification implements IngestionHelper {

    def setupSpec() {
        init()
    }

    @Category(Smoke)
    @Unroll
    def "verify standard purchase order paging behavior"(Integer limit, Integer expectedCount) {
        given: "standard purchase orders sorted by purchaseOrderId in descending order"
        def queryObject = new QueryObject()
        queryObject.setType("purchaseOrders")
        queryObject.addSortFields("purchaseOrderId", "desc", "string")
        queryObject.addQuery("purchaseOrderType", "standardPO")

        when: "attempt to get #limit standard purchase orders"
        queryObject.setLimit(limit)
        def response = QueryResultHandler.getQueryResultHandler().executeWithRetry(queryObject, 20, 5, 2,1, 1)

        then: "the api should return a success status code and #expectedCount orders"
        response.rawResponse.status == 200
        response.body.data.size() <= expectedCount

        where:
        limit | expectedCount
        10 | 10
        27 | 27
        30 | 30
    }
}