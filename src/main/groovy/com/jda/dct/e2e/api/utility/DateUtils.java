package com.jda.dct.e2e.api.utility;

import org.apache.commons.io.FileUtils;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.common.TemplateParserContext;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public abstract class DateUtils {
    private static String pattern = "M/d/yy";
    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);

    public static void convertDateInFile(File file) throws NoSuchMethodException {
        String csvContent = null;
        try {
            csvContent = FileUtils.readFileToString(file, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            FileUtils.writeStringToFile(file, parseContent(csvContent), "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String parseContent(String input) throws NoSuchMethodException {
        ExpressionParser parser = new SpelExpressionParser();
        StandardEvaluationContext context = new StandardEvaluationContext();

        context.registerFunction("currentDate", DateUtils.class.getDeclaredMethod("parseDate", Long.class));
        String parsedContent = parser.parseExpression(
                input,
                new TemplateParserContext("{", "}"))
                .getValue(context, String.class);
        return parsedContent;
    }

    public static String parseDate(Long days) {
        LocalDate today = LocalDate.now();

        if (days < 0 ) {
            today = today.minusDays(Math.abs(days));
        }
        else if (days > 0) {
            today = today.plusDays(days);
        }
        return today.format(formatter);
    }

    public static String getDateStrWithIncrementalMs(Date date, long incrementalMs, String format) {
        if (date == null) {
            date = new Date();
        }
        
        long ms = date.getTime();
        ms += incrementalMs;
        
        Date newDate = new Date(ms);
        
        SimpleDateFormat formatter = new SimpleDateFormat(format);
        return formatter.format(newDate);
    }
}
