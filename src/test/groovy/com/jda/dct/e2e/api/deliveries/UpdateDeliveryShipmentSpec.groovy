package com.jda.dct.e2e.api.deliveries

import com.jda.dct.e2e.api.categories.Regression
import com.jda.dct.e2e.api.utility.IngestionHelper
import org.junit.experimental.categories.Category
import org.springframework.http.HttpMethod
import spock.lang.Shared
import spock.lang.Specification

/**
 * End-to-end test for validating shipment information are updated on deliveries.
 *
 * @author Vida Bandara
 * @version 1.0-SNAPSHOT
 * @since 16-May-2018
 */

class UpdateDeliveryShipmentSpec extends Specification implements IngestionHelper {

    @Shared DeliveryShowcaseData sample = new DeliveryShowcaseData()
    @Shared Map<String, String> modifiedFields = new HashMap<>()

    def setup() {
        init()
    }

    @Category(Regression)
    def "validating shipment information are updated on deliveries"() {
        given: 'get ingested delivery from showcase and duplicate it as a testing data, ingest the testing delivery'
        modifiedFields.clear()
        modifiedFields.put("deliveryId", "delivery1")
        modifiedFields.put("shipmentId", "shipment1")
        modifiedFields.put("shipmentLineId", "shipmentLine1")
        def originalDelivery = sample.modifyDeliveryMultiFields(false, true,"inboundDelivery", modifiedFields, "temp-delivery.json")
        ingestData(originalDelivery, "deliveries?processType=supply&deliveryType=inboundDelivery")

        when: 'ingest an update to the same delivery - update shipment and line ids'
        modifiedFields.clear()
        modifiedFields.put("shipmentId", "shipment2")
        modifiedFields.put("shipmentLineId", "shipmentLine2")
        def deliveryWithUpdatedShipmentInfo = sample.modifyDeliveryMultiFields(true, false,"delivery1", modifiedFields, "temp-delivery.json")
        ingestData(deliveryWithUpdatedShipmentInfo, "deliveries?processType=supply&deliveryType=inboundDelivery")

        then: 'ids are updated'
        def updatedDelivery = sample.getDeliveryFromShowcase(true, false,"delivery1")
        executeHttpCall(header, protocol + ip + "/api/v1/deliveries?query=deliveryId=delivery1", null, HttpMethod.DELETE)
        assert updatedDelivery["shipmentId"] == "shipment2"
        assert updatedDelivery["shipmentLineId"] == "shipmentLine2"
    }
}
