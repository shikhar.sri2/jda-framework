package com.jda.dct.e2e.ui.utility.pageobjects;

import com.jda.dct.e2e.ui.utility.action.CommonActions;
import com.jda.dct.e2e.ui.utility.common.BasePage;
import com.jda.dct.e2e.ui.utility.helper.Wait.WaitHelper;
import com.jda.dct.e2e.ui.utility.helper.genericHelper.GenericHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class SupplyOrderListPage extends BasePage {

    WebDriver driver;
    WebDriverWait webDriverWait;
    WaitHelper objWaitHelper;
    GenericHelper objGenericHelper;
    CommonActions commonActions;

    @FindBy(id = "PanelSummary-RenderedItem-Order-No-1-LinkItem")
    private WebElement supplyOrderListPage;

    @FindBy(xpath = "//*[contains(@id,'purchase-order details')]")
    private WebElement supplyPurchaseOrderDetailsPage;
    @FindBy(xpath = "//*[@id='PanelSummary-RenderedItem-Order-No-1-LinkItem']")
    private WebElement supplyOrderNumber;

    @FindBy(xpath = "//*[contains(@id,'orders')]")
    private WebElement backToSupplyOrdersTab;
    @FindBy(id ="PanelSummary-RenderedItem-Item-1-LinkItem")
    private WebElement supplyOrderItem;
    @FindBy(xpath = "//*[contains(@id,'inventory-details')]")
    private WebElement atSupplyInventoryDetailsPage;

    @FindBy(xpath = "//*[@id='select-option']/../div[@role='button'][1]")
    private WebElement orderTypeFilter;
    @FindBy(xpath = "//*[@id='menu-']//li[@data-value='stockTransferPO']")
    private WebElement stockTransferPOFilter;
    @FindBy(xpath = "//*[@id='menu-']//li[@data-value='standardPO']")
    private WebElement standardPOFilter;

    @FindBy(xpath = "//*[contains(@id,'supply-standardPO-Filter-List-Card')]//span")
    private List<WebElement> orderExceptionStatusCount;

    @FindBy(xpath = "//*[@id='PanelSummary-RenderedItem-Order-No-1-LinkItem']/following::*[@id='ExpansionPanel-ExpandMoreIcon'][1]")
    private WebElement expandButton;
    @FindBy(xpath = "//*[@id='PanelSummary-RenderedItem-Notes-1-NotesItem']/a")
    private WebElement notes;
    @FindBy(xpath ="//*[@id='notesDrawer']//span[text()='Notes']")
    private WebElement notesTitle;

    public SupplyOrderListPage(WebDriver webdriver) {

        this.driver = webdriver;
        PageFactory.initElements(driver, this);
        webDriverWait = new WebDriverWait(driver, getTimeOutInSeconds());
        objWaitHelper = new WaitHelper(driver);
        objGenericHelper = new GenericHelper(driver);
        commonActions = new CommonActions(driver);
    }


    //========Methods=====

    public boolean isSupplyOrderListPageDisplayed() {
        objWaitHelper.waitElementToBeVisible(supplyOrderListPage);
        return objGenericHelper.isDisplayed(supplyOrderListPage, "Supply Order list page");
    }

    public boolean isSupplyPurchaseOrderDetailsPageDisplayed() {
        objWaitHelper.waitElementToBeVisible(supplyPurchaseOrderDetailsPage);
        return objGenericHelper.isDisplayed(supplyPurchaseOrderDetailsPage, "Purchase order details page");
    }


    public void clickOrderNumber() {
        objWaitHelper.waitAndClickElement(supplyOrderNumber, "Order Number");
    }

    public void clickSearchedOrderNumber(String orderNo) {
        String searchedOrderNoXpath = "//*[@id='PanelSummary-RenderedItem-Order-No-1-LinkItem'][text()='" + orderNo + "']";
        WebElement searchedOrderNo = driver.findElement(By.xpath(searchedOrderNoXpath));
        objWaitHelper.waitAndClickElement(searchedOrderNo, orderNo + " - Order Number");
    }


    public void clickBackToSupplyOrdersTab() {
        objWaitHelper.waitAndClickElement(backToSupplyOrdersTab, "Supply Orders Tab");
    }

    public void clickOrderTypeFilter(){
        objGenericHelper.elementClick(orderTypeFilter, "Order Type filter");
    }

    public void clickStandardPOOrderType(){
        objGenericHelper.elementClick(standardPOFilter, "Standard PO filter");
    }


    public void clickStockTransferPOOrderType(){
        objGenericHelper.elementClick(stockTransferPOFilter, "Stock Transfer PO filter");
    }

    public void clickOnOrderItem() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(supplyOrderItem));
        supplyOrderItem.click();

    }

    public void clickExpandButton() {
        objWaitHelper.waitAndClickElement(expandButton, "Expand Button");
    }

    public boolean inventoryDetailPageIsDisplayed() {
        objWaitHelper.waitElementToBeVisible(atSupplyInventoryDetailsPage);
        return objGenericHelper.isDisplayed(atSupplyInventoryDetailsPage, "Inventory details page");
    }

    public void clickShipmentExceptionHavingData() {
        commonActions.clickExceptionStatusHavingData(orderExceptionStatusCount);
    }

    public void clickNotes() {
        objWaitHelper.waitAndClickElement(notes, "Notes ");
    }

    public boolean isNotesPageDisplayed() {
        return objWaitHelper.waitAndClickElement(notesTitle, "Notes Title");
    }
}





