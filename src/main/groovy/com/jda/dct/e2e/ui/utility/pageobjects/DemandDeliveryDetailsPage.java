package com.jda.dct.e2e.ui.utility.pageobjects;

import com.jda.dct.e2e.ui.utility.common.BasePage;
import com.jda.dct.e2e.ui.utility.helper.Action.ActionHelper;
import com.jda.dct.e2e.ui.utility.helper.Wait.WaitHelper;
import com.jda.dct.e2e.ui.utility.helper.genericHelper.GenericHelper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DemandDeliveryDetailsPage extends BasePage {


    WebDriver driver;
    WebDriverWait webDriverWait;
    WaitHelper objWaitHelper;
    GenericHelper objGenericHelper;
    ActionHelper actionHelper;

    @FindBy(id = "demand-page-tab-shipments")
    private WebElement demandDeliveryTab;

    @FindBy(id ="PanelSummary-RenderedItem-Delivery-No-1-LinkItem")
    private WebElement deliveryNumber;

    @FindBy(id = "PanelSummary-RenderedItem-Container-No-1-LinkItem")
    private WebElement containerNumber;



    @FindBy(id = "PanelSummary-RenderedItem-SO-No-1-LinkItem")
    private WebElement salesOrderNumber;

    @FindBy(id ="PanelSummary-RenderedItem-Item-1-LinkItem")
    private WebElement itemNumber;

    @FindBy(id ="//a[contains(@id,'PanelSummary-RenderedItem')]")
    private WebElement RenderedItem;


    @FindBy(xpath = "//*[@id='ExpansionPanel-ExpandMoreIcon']")
    private WebElement dropDown;

    @FindBy(xpath = "//*[@id='breadcrumb-delivery-details']")
    private WebElement deliveryDetailsPage;

    //  @FindBy(xpath ="demand-page-tab-shipments")
    @FindBy(id ="supply-page-tab-shipments")
    private WebElement shipmentPage;


    @FindBy(xpath = "//*[@id='breadcrumb-inventory-details']")
    private WebElement inventoryDetailsPage;

    @FindBy(xpath = "//a[@href='/demand/inventory']")
    private WebElement inventory;

    @FindBy(xpath = "//*[@href='/demand/sales-orders']")
    private WebElement salesOrderDetailsPage;

    @FindBy(xpath = "//a[contains(@href,'demand/delivery')]")
    private WebElement delivery;

    @FindBy(xpath = "//a[@href='/demand/sales-orders']")
    private WebElement orders;

    @FindBy(id ="PanelSummary-RenderedItem-Shipment-No-1-LinkItem")
    private WebElement shipmentNumber;

    @FindBy(id ="PanelSummary-RenderedItem-Shipment-No-1-LinkItem")
    private WebElement shipmentNumberAssertionPoint;



    //Define the Page Factory-OR-Page Objects

    public DemandDeliveryDetailsPage(WebDriver webdriver) {
        this.driver = webdriver;
        PageFactory.initElements(driver, this);
        webDriverWait = new WebDriverWait(driver, getTimeOutInSeconds());
        objWaitHelper = new WaitHelper(driver);
        objGenericHelper = new GenericHelper(driver);
        actionHelper = new ActionHelper(driver);
    }


    public void clickOnDeliveryNumber() {
        objWaitHelper.waitAndClickElement(deliveryNumber, "Delivery Number");
    }

    public void clickOnContainerNumber() {
        objWaitHelper.waitAndClickElement(containerNumber, "Container Number");
    }

    public void clickOnSaleOrderNumber() {
        actionHelper.moveToElement(salesOrderNumber,"Order Number");
        objWaitHelper.waitAndClickElement(salesOrderNumber, "Order Number");
    }

    public void clickOnItemNumber() {
        objWaitHelper.waitAndClickElement(itemNumber, "Item Number");
    }

    public void expandDropDown() {
        objWaitHelper.waitAndClickElement(dropDown, "Expand drop down");
    }

    public boolean isDeliveryDetailsPageDisplayed() {
        objWaitHelper.waitElementToBeVisible(deliveryDetailsPage);
        return objGenericHelper.isDisplayed(deliveryDetailsPage, "Delivery Details page");
    }


    public boolean isShipmentNumberDisplayed() {
        objWaitHelper.waitElementToBeVisible(shipmentNumber);
        return objGenericHelper.isDisplayed(shipmentNumber, "Shipment Number");
    }


    public boolean isInventoryDetailsPageDisplayed() {
        return objGenericHelper.isDisplayed(inventoryDetailsPage, "Inventory Details page");
    }

//
//    public boolean isInventoryItemNumberDisplayed() {
//        return RenderedItem.isDisplayed();
//    }


    public boolean isSalesOrderDetailsPageDisplayed() {
        objWaitHelper.waitElementToBeVisible(salesOrderDetailsPage);
        return objGenericHelper.isDisplayed(salesOrderDetailsPage, "Sales Order details page");
    }

    public void gotoDeliveryPage() {
        objGenericHelper.elementClick(delivery,"Delivery tab");
    }
}
