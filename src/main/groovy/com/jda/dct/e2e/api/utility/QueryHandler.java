package com.jda.dct.e2e.api.utility;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

import java.io.File;

public class QueryHandler {

    public QueryObjects process(File file) {
        QueryObjects queryObjects = null;
        if (file == null) {
            return null;
        }
        try {
            ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
            queryObjects = mapper.readValue(file, QueryObjects.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return queryObjects;
    }
}
