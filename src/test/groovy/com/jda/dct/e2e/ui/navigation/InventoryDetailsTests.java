package com.jda.dct.e2e.ui.navigation;

import com.jda.dct.e2e.ui.utility.action.CommonActions;
import com.jda.dct.e2e.ui.utility.common.BaseTest;
import com.jda.dct.e2e.ui.utility.common.LeftMenu;
import com.jda.dct.e2e.ui.utility.helper.AssertHelper.AssertHelper;
import com.jda.dct.e2e.ui.utility.pageobjects.*;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class InventoryDetailsTests extends BaseTest {

    private LeftMenu leftMenu;
    private TopMenuPage topMenu;
    private TopSubMenuPage topSubMenu;
    private SupplyShipmentListPage supplyShipmentListPage;
    private SupplyShipmentDetailsPage supplyShipmentDetailsPage;
    private InventoryListPage inventoryListPage;
    private AssertHelper assertHelper;
    private CommonActions commonActions;
    private WebDriver driver;


    @BeforeClass
    public void setUp() throws InterruptedException {
        //Initialize required objects
        driver = getDriver();
        assertHelper = new AssertHelper(driver);
        leftMenu = new LeftMenu(driver);
        topMenu = new TopMenuPage(driver);
        topSubMenu = new TopSubMenuPage(driver);
        supplyShipmentListPage = new SupplyShipmentListPage(driver);
        supplyShipmentDetailsPage = new SupplyShipmentDetailsPage(driver);
        inventoryListPage = new InventoryListPage(driver);
        commonActions = new CommonActions(driver);
        //loginAndNavigateToSupplyLandingPage
        logInToApplication();
    }

    @Test(priority = 1,retryAnalyzer = BaseTest.class)
    public void validateInventoryDetailPageIsDisplayed() throws InterruptedException {
        leftMenu.clickInventoryMenuOption();
        topMenu.clearFilterIfVisible();
        commonActions.disableAllExceptionFilter();
        inventoryListPage.clickInventoryItemNumber();
//verify user lands at Inventory Details Page
        assertHelper.assertTrue(inventoryListPage.isEOWDisplayed(),"EOW is displayed successfully");
        assertHelper.assertTrue(inventoryListPage.isEOMDisplayed(), "EOM is displayed successfully");
        assertHelper.assertTrue(inventoryListPage.isEOQDisplayed(), "EOQ is displayed successfully");
        assertHelper.assertTrue(inventoryListPage.isEOYDisplayed(), "EOY is displayed successfully");
        inventoryListPage.clickBackToInventoryPage();
    }

    @Test(priority = 2,retryAnalyzer = BaseTest.class)
    public void validateNoteWindowIsDisplayed() throws InterruptedException {
        leftMenu.clickInventoryMenuOption();
        commonActions.disableAllExceptionFilter();
        supplyShipmentListPage.clickExpandButton();
        inventoryListPage.clickInventoryNotes();
        assertHelper.assertTrue(supplyShipmentDetailsPage.isNotesPageDisplayed(), "Notes page is displayed successfully");
    }
}

