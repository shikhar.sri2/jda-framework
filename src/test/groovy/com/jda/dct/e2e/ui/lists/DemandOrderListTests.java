package com.jda.dct.e2e.ui.lists;


import com.jda.dct.e2e.ui.utility.action.CommonActions;
import com.jda.dct.e2e.ui.utility.common.BaseTest;
import com.jda.dct.e2e.ui.utility.common.LeftMenu;
import com.jda.dct.e2e.ui.utility.helper.AssertHelper.AssertHelper;
import com.jda.dct.e2e.ui.utility.pageobjects.*;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


public class DemandOrderListTests extends BaseTest {

    private LeftMenu leftMenu;
    private TopMenuPage topMenu;
    private TopSubMenuPage topSubMenu;
    private SupplyOrderListPage supplyOrderlistPage;
    private DemandShipmentListPage demandShipmentListPage;
    private DemandShipmentDetailsPage demandShipmentDetailsPage;
    private DemandOrderListPage demandOrderListPage;
    private AssertHelper assertHelper;
    private CommonActions commonActions;
    private WebDriver driver;


    @BeforeClass
    public void setUp() throws InterruptedException {
        System.out.println("Starting tests for :" + this.getClass().getName());
        //Initialize required objects
        driver = getDriver();
        leftMenu = new LeftMenu(driver);
        topMenu = new TopMenuPage(driver);
        topSubMenu = new TopSubMenuPage(driver);
        supplyOrderlistPage = new SupplyOrderListPage(driver);
        demandOrderListPage = new DemandOrderListPage(driver);
        demandShipmentListPage = new DemandShipmentListPage(driver);
        demandShipmentDetailsPage = new DemandShipmentDetailsPage(driver);
        assertHelper = new AssertHelper(driver);
        commonActions = new CommonActions(driver);
        //loginAndNavigateToDemandLandingPage
        logInToApplication();
    }

    @BeforeMethod
    public void commonMethod() {
        leftMenu.clickDemandMenuOption();
    }

    @Test(priority = 1,retryAnalyzer = BaseTest.class)
    public void validateDemandOrderListPageIsDisplayed() throws InterruptedException {
        // navigate to Order tab
        topMenu.goToDemandOrdersTab();
        //topMenu.clearFilterIfVisible();
        commonActions.disableAllExceptionFilter();
        //verify user is at demand Orders List page
        assertHelper.assertTrue(demandOrderListPage.isDemandOrderListPageDisplayed(), "Demand Order List page is displayed successfully");
    }
}


