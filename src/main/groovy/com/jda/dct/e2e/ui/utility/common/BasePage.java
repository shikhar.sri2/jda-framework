package com.jda.dct.e2e.ui.utility.common;

public class BasePage extends BaseTest{

    private final int webDriverWaitinSeconds = 20;

    public int getTimeOutInSeconds() {

        return webDriverWaitinSeconds;

    }

}
