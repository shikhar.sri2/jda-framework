package com.jda.dct.e2e.ui.utility.pageobjects;

import com.jda.dct.e2e.ui.utility.common.BasePage;
import com.jda.dct.e2e.ui.utility.helper.Wait.WaitHelper;
import com.jda.dct.e2e.ui.utility.helper.genericHelper.GenericHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;


public class DemandShipmentDetailsPage extends BasePage {


    WebDriver driver;
    WebDriverWait webDriverWait;
    WaitHelper waitHelper;
    GenericHelper genericHelper;
    public DemandShipmentDetailsPage(WebDriver webdriver) {
        this.driver = webdriver;
        PageFactory.initElements(driver, this);
        webDriverWait = new WebDriverWait(driver, 50);
        waitHelper = new WaitHelper(driver);
        genericHelper = new GenericHelper(driver);
    }
        //page objects

    @FindBy(xpath ="//*[@id='PanelSummary-RenderedItem-Notes-0-NotesItem']/a")
    private WebElement notes;
    @FindBy(xpath = "//*[@id='notesDrawer']//span[text()='Notes']")
    private WebElement notesTitle;
    @FindBy(xpath = "//*[@id='notesDrawer']//span[text()='Attachments']")
    private WebElement attachmentsTab;
    @FindBy(xpath = "//*[@id='attachmentPage']//span[text()='+ Attachment']")
    private WebElement attachmentsButton;
    @FindBy(xpath = "//*[@id='note_message']")
    private WebElement notesTextArea;
    @FindBy(xpath = "//*[@id='notes_message_attach_file']")
    private WebElement attachmentsLinkInNotes;
    @FindBy(xpath = "//*[@id='notes_message_submit']")
    private WebElement submitAttachment;
    @FindBy(xpath = "//div[@id='notesDrawer']//p[1]")
    private WebElement downloadAttachment;
    @FindBy(xpath = "//*[@type='button'][@aria-label='Delete'][@tabindex='0']")
    private WebElement deleteAttachment;
    @FindBy(xpath = "//*[@type='button'][@aria-label='Delete'][@tabindex='0']")
    private List< WebElement> deleteAttachments;
    @FindBy(xpath = "//div[@id='dropzone']//p[text()]")
    private WebElement listOfAttachments;
    @FindBy(xpath = "//span[text()='Delete Attachment']")
    private WebElement confirmDeleteButton;
    @FindBy(xpath = "//div[@id='notesDrawer']//button/../p")
    private List<WebElement> listOfFiles;
    @FindBy(id="DrawerNav-CloseIcon")
    private WebElement backToNotes;
    @FindBy(xpath = "//textarea[@placeholder='add a note']")
    private WebElement noteSearchbox;
    @FindBy(xpath = "//button[@aria-label='Clear']")
    private WebElement deleteButton;
    @FindBy(xpath = "//button[@aria-label='Create']")
    private WebElement saveButton;
    @FindBy(css = "//aside[contains(.,'Supplytest')]")
    private WebElement saveNotes;
    @FindBy(xpath ="//*[@id='PanelSummary-RenderedItem-All-Orders-0']//*[@id='undefined-LinkItem'][1]")
    private WebElement shipmentAllorders;
    @FindBy(xpath ="//*[@id='PanelSummary-RenderedItem-All-Orders-0']//*[@id='undefined-LinkItem'][1]")
    private WebElement shipmentHotOrders;
    @FindBy(id ="PanelSummary-RenderedItem-Container-No-0-LinkItem")
    private WebElement containerNumber;
    @FindBy(id ="PanelSummary-RenderedItem-Shipment-No-1-LinkItem")
    private WebElement searchedShipmentNumber;




    //Webelements inside container panel
    @FindBy(xpath ="//*[@id='Shipments-Panel-Cell']//*[@id='PanelSummary-RenderedItem-All-Orders-0']//*[@id='undefined-LinkItem']")
    private WebElement containerAllOrders;
    @FindBy(xpath ="//*[@id='Shipments-Panel-Cell']//*[@id='PanelSummary-RenderedItem-Hot-Orders-0']//*[@id='undefined-LinkItem']")
    private WebElement containerHotOrders;
    // goto demand order Page
    @FindBy(xpath = "//*[@href='/demand/sales-orders']")
    private WebElement demandOrderListPage1;


    //------------webelements for navigation assertions
    // goto demand Delivery Page
    @FindBy (id ="demand-page-tab-deliveries")
    private WebElement demandDeliveryListPage1;
    @FindBy(xpath ="//a[@href=\"/demand/shipment\"]")
    private WebElement backToDemandShipmentPage;
    @FindBy(xpath = "//div [contains(@class,'headerSummaryContainer css-fbpawx')]")
    private WebElement displaySearchNumber;



    //-------methods for object navigation inside SupplyShipment NO

    public void clickShipmentHotorders() {
        waitHelper.waitAndClickElement(shipmentHotOrders, "Hot Orders Icon");
    }


    public void clickShipmentAllOrders() {
        waitHelper.waitAndClickElement(shipmentAllorders, "All Orders");
    }


    public void clickNotes() {
        waitHelper.waitAndClickElement(notes, "Notes");
    }


    public void clickBackToNotes() {
        waitHelper.waitAndClickElement(backToNotes, "Back to Notes arrow");
    }




//-----------methods to navigate container details

    public void clickContainerNumber() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(containerNumber));
        containerNumber.click();
    }


    public void clickContainerAllorders() {
        waitHelper.waitAndClickElement(containerAllOrders, "Container All Orders");
    }


    public void clickContainerHotorders() {
        waitHelper.waitAndClickElement(containerHotOrders, "Container Hot Orders");
    }

    public void clickBackToDemandShipmentLink() {
        waitHelper.waitAndClickElement(backToDemandShipmentPage, "Back to Shipment link");
    }


    public boolean isSearchShipmentNumberDisplayed(String searchValue) {

        WebElement webElement = driver.findElement(By.xpath("//a[contains(text(),'"+searchValue+"')]"));
        webDriverWait.until(ExpectedConditions.visibilityOf(webElement));
        return webElement.isDisplayed();
    }


    public void clickSearchedShipmentNumber(String searchedshipmentNumber) {
        waitHelper.waitAndClickElement(searchedShipmentNumber, "Searched Shipment No.");
    }




    //----------assertion methods--------

    //displays shipment line details

    public boolean isDemandOrderPageDisplayed() {
        waitHelper.waitElementToBeVisible(demandOrderListPage1);
        return genericHelper.isDisplayed(demandOrderListPage1, "Order list page");
    }

    public boolean isDemandDeliveryPageDisplayed() {
        waitHelper.waitElementToBeVisible(demandDeliveryListPage1);
        return genericHelper.isDisplayed(demandDeliveryListPage1, "Demand Delivery Page");
    }


    public boolean isNotesPageDisplayed() {
        waitHelper.waitElementToBeVisible(notesTitle);
        return genericHelper.isDisplayed(notesTitle, "Notes Title");
    }

    public boolean isAttachmentsTabDisplayed() {
        waitHelper.waitElementToBeVisible(attachmentsTab);
        return genericHelper.isDisplayed(attachmentsTab, "Attachments Tab");
    }

    public void clickNotesTab() {
        waitHelper.waitAndClickElement(notesTitle, "Notes Tab");
    }
    public void clickAttachmentsTab() {
        waitHelper.waitAndClickElement(attachmentsTab, "Attachments Tab");
    }
    public void clickAttachmentsButton() {
        waitHelper.waitAndClickElement(attachmentsButton, "Attachments Button");
    }

    public void setNotesTextArea()
    {
        genericHelper.setElementText(notesTextArea,"Notes  Text Area","Uploading the document");
    }

    public void clickAttachmentsLinkInNotes() {
        waitHelper.waitAndClickElement(attachmentsLinkInNotes, "Attachments Link in Notes Tab");
    }
    public void uploadAttachment(String fileName)
    {
        genericHelper.attachFile(fileName);
    }
    public void submitAttachment()
    {
        waitHelper.waitAndClickElement(submitAttachment, "submit attachment");
    }
    public void downloadAttachment()
    {
        waitHelper.waitAndClickElement(downloadAttachment, "download attachment");
    }

    public void deleteAttachment()
    {
        waitHelper.waitAndClickElement(deleteAttachment, "submit attachment");
    }
    public void deleteAllAttachments()
    {
      List<WebElement> deleteList=genericHelper.listOfWebElements(deleteAttachments,"List of attachments");
      for(int i=0;i<=deleteList.size();i++)
      {
          System.out.println("Delete link is: "+deleteList.get(i).getText());
          deleteList.get(i).click();
          confirmDeleteAttachment();
      }
    }
    public void  confirmDeleteAttachment()
    {
        waitHelper.waitAndClickElement(confirmDeleteButton, "delete button in dialoag box");

    }

    public List<String>  listOfAttachements()
    {
       return genericHelper.listOfStrings(listOfFiles,"List of attachments");
    }


}
