package com.jda.dct.e2e.ui.utility.pageobjects;

import com.jda.dct.e2e.ui.utility.action.CommonActions;
import com.jda.dct.e2e.ui.utility.common.BasePage;
import com.jda.dct.e2e.ui.utility.helper.Wait.WaitHelper;
import com.jda.dct.e2e.ui.utility.helper.genericHelper.GenericHelper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SupplyOrderDetailsPage extends BasePage {

    WebDriver driver;
    WebDriverWait webDriverWait;
    WaitHelper objWaitHelper;
    GenericHelper objGenericHelper;
    CommonActions commonActions;


    public SupplyOrderDetailsPage(WebDriver webdriver) {

        this.driver = webdriver;
        PageFactory.initElements(driver, this);
        webDriverWait = new WebDriverWait(driver, getTimeOutInSeconds());
        objWaitHelper = new WaitHelper(driver);
        objGenericHelper = new GenericHelper(driver);
        commonActions = new CommonActions(driver);
    }


    //========Methods=====
    @FindBy(xpath = "//*[@id='PanelSummary-RenderedItem-Total-Gr Qty-[object Object]-LinkItem']/a")
    private WebElement totalGRQtyList;
    @FindBy(xpath = "//*[@id='PanelSummary-RenderedItem-Receipt-No.-0-LinkItem']")
    private WebElement receiptNo;
    @FindBy(xpath = "//*[@id='PanelSummary-RenderedItem-State-0-LinkItem']")
    private WebElement receiptState;
    @FindBy(xpath = "//*[@id='PanelSummary-RenderedItem-Receipt-Date-0-DateItem']")
    private WebElement receiptDate;
    @FindBy(xpath = "//*[@id='PanelSummary-RenderedItem-Qty-Received-0-LinkItem']")
    private WebElement qtyReceived;
    @FindBy(xpath = "//*[@id='PanelSummary-RenderedItem-Receipt-No.-0-LinkItem']/../../../..")
    private List<WebElement> allReceiptList;
    @FindBy(xpath = "//*[@id='PanelSummary-RenderedItem-Receipt-No.-0-LinkItem']")
    private List<WebElement> receiptNoList;
    @FindBy(xpath = "//*[@id='PanelSummary-RenderedItem-State-0-LinkItem']")
    private List<WebElement> receiptStateList;
    @FindBy(xpath = "//*[@id='PanelSummary-RenderedItem-Receipt-Date-0-DateItem']")
    private List<WebElement> receiptDateList;
    @FindBy(xpath = "//*[@id='PanelSummary-RenderedItem-Qty-Received-0-LinkItem']")
    private List<WebElement> qtyReceivedList;


    public boolean clickTotalGRQty() {
        return objGenericHelper.elementClick(totalGRQtyList, "Total GR Quantity");
    }

    public String getTotalGRQTYValue() {
        return objGenericHelper.getTextFromElement(totalGRQtyList, "Total GR Quantity");
    }

    public String getReceiptNo() {
        return objGenericHelper.getTextFromElement(receiptNo, "Receipt No.");
    }

    public String getReceiptState() {
        return objGenericHelper.getTextFromElement(receiptState, "Receipt State");
    }

    public String getReceiptDate() {
        return objGenericHelper.getTextFromElement(receiptDate, "Receipt Date");
    }

    public String getQtyReceived() {
        return objGenericHelper.getTextFromElement(qtyReceived, "Quantity Received");
    }

    public List<Map<String, String>> getAllReceiptDetails() {
        try {
            List<Map<String, String>> allReceiptDetails = new ArrayList<>();

            for (int i = 0; i < allReceiptList.size(); i++) {
                HashMap<String, String> receiptDetails = new HashMap<>();
                receiptDetails.put("Receipt No.", receiptNoList.get(i).getText());
                receiptDetails.put("Receipt State", receiptStateList.get(i).getText());
                receiptDetails.put("Receipt Date", receiptDateList.get(i).getText());
                receiptDetails.put("Quantity Received", qtyReceivedList.get(i).getText());
                allReceiptDetails.add(i, receiptDetails);
            }
            return allReceiptDetails;
        } catch (Exception e) {
            Reporter("Exception while storing all Receipt Details: " + e, "Fail");
            throw new RuntimeException("Exception while storing all Receipt Details: " + e);
        }
    }
}