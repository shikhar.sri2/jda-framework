package com.jda.dct.e2e.ui.lists;

import com.jda.dct.e2e.ui.utility.common.BaseTest;
import com.jda.dct.e2e.ui.utility.common.LeftMenu;

import com.jda.dct.e2e.ui.utility.pageobjects.MasterDataParticipantsPage;
import com.jda.dct.e2e.ui.utility.pageobjects.TopMenuPage;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class MasterDataParticipantsTests extends BaseTest {

    private LeftMenu leftMenu;
    private TopMenuPage topMenu;
    private WebDriver driver;
    private MasterDataParticipantsPage masterDataParticipants;

    @BeforeClass
    public void setUp() throws InterruptedException {
        System.out.println("Starting tests for :" + this.getClass().getName());
        //Initialize required objects
        driver = getDriver();
        leftMenu = new LeftMenu(driver);
        topMenu = new TopMenuPage(driver);
        masterDataParticipants = new MasterDataParticipantsPage(driver);        //loginAndNavigateToSupplyLandingPage
        logInToApplication();
        leftMenu.clickMasterDataMenuOption();
    }

    @BeforeMethod
    public void commonMethods() throws InterruptedException {
        //navigates to Demand Page
        leftMenu.clickMasterDataMenuOption();
    }

    @Test(priority = 1,retryAnalyzer = BaseTest.class)
    public void validateMasterDataPaticipantsListPageIsDisplayed() {
        //navigates to Master-Data Item Tab
        topMenu.goToMasterDataParticipantsTab();
        //verify user is at MasterData Paticipants ListPage
        Assert.assertTrue(masterDataParticipants.iMasterDataParticipantsPageDisplayed());
    }

}