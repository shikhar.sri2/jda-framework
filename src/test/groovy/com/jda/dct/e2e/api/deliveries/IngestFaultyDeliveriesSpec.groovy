package com.jda.dct.e2e.api.deliveries

import com.jda.dct.e2e.api.categories.Regression
import com.jda.dct.e2e.api.utility.IngestionHelper
import org.junit.experimental.categories.Category
import spock.lang.Shared
import spock.lang.Specification
import spock.util.concurrent.PollingConditions


class IngestFaultyDeliveriesSpec extends Specification implements IngestionHelper {

    @Shared deliveryURL
    @Shared DeliveryShowcaseData sample = new DeliveryShowcaseData()
    Map<String, String> modifiedFields = new HashMap<>()


    def setupSpec() {
        init()

        def deliveryPath = properties.getProperty('ingestDeliveriesPath')
        deliveryURL = protocol + ip + deliveryPath + '?processType=tempraturecontrolled&deliveryType=inboundDelivery'
    }

    @Category(Regression)
    def "create and ingest a faulty delivery csv"() {
        given: "a faulty sample delivery, quantity is not a valid number"

        modifiedFields.put("deliveryLines.10.shippedQuantity", "wrong number")
        def modifiedDelivery = sample.modifyDeliveryMultiFields(true, false, "OD0024_SO", modifiedFields, "tmp-delivery.json")

        when: "ingestion into DCT"
        def ingestionId = ingestData(modifiedDelivery, "deliveries?processType=demand&deliveryType=outboundDelivery")
        def status = waitUntilComplete(ingestionId)

        then: "should have status - completed_with_error"
        status.contains('COMPLETED_WITH_ERROR')
    }

    @Category(Regression)
    def "Ingestion status of an invalid delivery becomes complete with error"() {
        given: "get showcase delivery and set deliveryType with invalid type"
        modifiedFields.clear()
        modifiedFields.put("deliveryType", "wrong-delivery-type")
        def modifiedSampleDelivery = sample.modifyDeliveryMultiFields(false, true, "outboundDelivery", modifiedFields,"temp-delivery.json")

        when: 'a delivery json ingested'
        def ingestionId = ingestData(modifiedSampleDelivery, "deliveries?processType=demand&deliveryType=outboundDelivery")

        then: 'ingestion status eventually becomes COMPLETED'
        def conditions = new PollingConditions(timeout: 15, initialDelay: 1)
        conditions.eventually {
            def status = getStatus(ingestionId)
            assert status.contains('COMPLETED_WITH_ERROR')
        }
    }
}