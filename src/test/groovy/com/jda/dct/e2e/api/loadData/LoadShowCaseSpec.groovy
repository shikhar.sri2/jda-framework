package com.jda.dct.e2e.api.loadData

import com.jda.dct.e2e.api.utility.FetchFileHelper
import com.jda.dct.e2e.api.utility.IngestionHelper
import com.jda.dct.e2e.api.utility.LoadShowcase
import org.springframework.http.HttpMethod
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll
import spock.util.concurrent.PollingConditions

class LoadShowCaseSpec extends Specification implements IngestionHelper, FetchFileHelper {
    @Shared String tid = "a4b7a825f67930965747445709011120"
    @Shared processModelUrl
    @Shared processModelFile
    @Shared List<File> allCSVFiles
    @Shared List<File> allControlFiles
    @Shared List<File> allJSONFiles
    @Shared List<File> umsFiles

    @Shared List<File> userJsonFiles
    @Shared List<File> tenantParticipantJsonFiles
    @Shared List<File> tenantUserJsonFiles
    @Shared List<File> tenantRoleJsonFiles
    @Shared List<File> assignUserRoleFiles
    @Shared List<File> tenantPolicyJsonFiles
    @Shared List<File> assignRolePGFiles

    @Shared umsUrl

    def setupSpec() {
        init()
        processModelUrl = protocol + ip + processModelPostPath
        processModelFile = getProcessModel("show-case")
        allCSVFiles = getShowcaseData(tid).get(0)
        allControlFiles = getShowcaseData(tid).get(1)
        umsFiles = getShowcaseData(tid).get(2)
        allJSONFiles = getShowcaseData(tid).get(3) //filter all the JSON files from the directory
        umsUrl = protocol + ip + umsPath

        userJsonFiles = new ArrayList<>()
        selectByKeyword("users", umsFiles, userJsonFiles, true)
        tenantParticipantJsonFiles = new ArrayList<>()
        selectByKeyword("tenant-participants", umsFiles, tenantParticipantJsonFiles, true)
        tenantUserJsonFiles = new ArrayList<>()
        selectByKeyword("tenant-users-default", umsFiles, tenantUserJsonFiles, true)
        tenantRoleJsonFiles = new ArrayList<>()
        selectByKeyword("tenant-roles-", umsFiles, tenantRoleJsonFiles, false)
        assignUserRoleFiles = selectJsonArrayByKeyword("assign-role-to-user-", umsFiles, new ArrayList<>(), false)
        tenantPolicyJsonFiles = new ArrayList<>()
        selectByKeyword("tenant-policies", umsFiles, tenantPolicyJsonFiles, true)
        assignRolePGFiles = selectJsonArrayByKeyword("assign-PG-to-role", umsFiles, new ArrayList<>(), true)
    }

    @Unroll
    def "Ingest process model"() {
        when: "Clear override process model"
        println()
        println "test......"
        println "Clearing override process model..."
        def response = postFile(processModelFile, processModelUrl, null)
        println "Post URL: " + processModelUrl

        then: "Ingested default process model successfully"
        response.toString().contains("model")
        println "End......"
        println()
    }

    @Unroll
    def "Ingest users showcase data"() {
        given: "Create a polling conditions"
        def conditions = new PollingConditions(timeout: 5, initialDelay: 1)

        when: "POST data"
        def userPostUrl = umsUrl + 'users'
        def response = executeHttpCall(header, userPostUrl, userJson.text, HttpMethod.POST)

        then: "Roles data ingested successfully"
        conditions.eventually {
            assert response != null && response["count"] > 0
        }

        where:
        userJson << userJsonFiles
    }

    @Unroll
    def "Ingest tenant-participants showcase data"() {
        given: "Create a polling conditions"
        def conditions = new PollingConditions(timeout: 5, initialDelay: 1)

        when: "POST data"
        def tenantParticipantUrl = umsUrl + 'tenantParticipants'
        def response = executeHttpCall(header, tenantParticipantUrl, tenantParticipantJson.text, HttpMethod.POST)

        then: "Roles data ingested successfully"
        conditions.eventually {
            assert response != null && response["count"] > 0
        }

        where:
        tenantParticipantJson << tenantParticipantJsonFiles
    }

    @Unroll
    def "Ingest tenant-users showcase data"() {
        given: "Create a polling conditions"
        def conditions = new PollingConditions(timeout: 5, initialDelay: 1)

        when: "POST data"
        def tenantUserPostUrl = umsUrl + 'tenantUsers'
        def response = executeHttpCall(header, tenantUserPostUrl, tenantUserJson.text, HttpMethod.POST)

        then: "Roles data ingested successfully"
        conditions.eventually {
            assert response != null && response["count"] > 0
        }

        where:
        tenantUserJson << tenantUserJsonFiles
    }

    def "Ingest tenant-roles showcase data"() {
        given: "Create a polling conditions"
        def conditions = new PollingConditions(timeout: 5, initialDelay: 1)

        when: "POST data"
        def tenantRolesPostUrl = umsUrl + 'tenantRoles'
        def response = executeHttpCall(header, tenantRolesPostUrl, tenantRoleJson.text, HttpMethod.POST)

        then: "Roles data ingested successfully"
        conditions.eventually {
            assert response != null && response["count"] > 0
        }

        where:
        tenantRoleJson << tenantRoleJsonFiles
    }

    def "Tenant-roles are assigned to tenant-users"() {
        given: "Create a polling conditions"
        def conditions = new PollingConditions(timeout: 5, initialDelay: 1)

        when: "Pass json to parameters, append to url, POST data"
        def assignUserRoleUrl = umsUrl + 'tenantUsers/assignTenantRole'
        String tenantId = userRoleAssociation["tid"]
        String userName = userRoleAssociation["userName"]
        String roleName = userRoleAssociation["roleName"]
        def url = assignUserRoleUrl + "?tid=" + tenantId + "&userName=" + userName + "&roleName=" + roleName
        def response = executeHttpCall(header, url, null,  HttpMethod.POST)

        then: "Assign successfully"
        conditions.eventually {
            assert response != null && response["count"] > 0
        }

        where:
        userRoleAssociation << assignUserRoleFiles
    }

    def "Ingest tenant-policyGroups with defined policies showcase data"() {
        given: "Create a polling conditions"
        def conditions = new PollingConditions(timeout: 5, initialDelay: 1)

        when: "POST data"
        def tenantPoliciesPostUrl = umsUrl + 'tenantPolicies'
        def response = executeHttpCall(header, tenantPoliciesPostUrl, tenantPolicyJson.text, HttpMethod.POST)

        then: "Roles data ingested successfully"
        conditions.eventually {
            assert response != null && response["count"] > 0
        }

        where:
        tenantPolicyJson << tenantPolicyJsonFiles
    }

    def "Tenant-policyGroups are assigned to tenant-roles"() {
        given: "Create a polling conditions"
        def conditions = new PollingConditions(timeout: 5, initialDelay: 1)

        when: "Pass json to parameters, append to url, POST data"
        def assignTenantPGToRoleUrl = umsUrl + 'tenantRoles/assignTenantPolicyGroup'
        String tenantId = rolePGAssociation["tid"]
        String roleName = rolePGAssociation["roleName"]
        String policyGroupName = rolePGAssociation["policyGroupName"]
        def url = assignTenantPGToRoleUrl + "?tid=" + tenantId + "&roleName=" + roleName + "&policyGroupName=" + policyGroupName
        def response = executeHttpCall(header, url, null,  HttpMethod.POST)

        then: "Assign successfully"
        conditions.eventually {
            assert response != null && response["data"].size() > 0
        }

        where:
        rolePGAssociation << assignRolePGFiles
    }

    @Unroll
    def "Ingest participants data"() {
        given: "get participants showcase data"
        List<File> data = selectedFile("participants", allCSVFiles)

        when: "Load participants data parallel"
        List<Boolean> results = new LoadShowcase().loadParallel(true, null, 10, data, allControlFiles)

        then: "participants data ingest successfully"
        results.every {
            it
        }
    }

    @Unroll
    def "Ingest sites, items, calendars data"() {
        given: "get showcase data"
        List<File> data = selectedFile("sites, items, calendars", allCSVFiles)

        when: "Load data parallel"
        List<Boolean> results = new LoadShowcase().loadParallel(true, null, 10, data, allControlFiles)

        then: "participants data ingest successfully"
        results.every {
            it
        }
    }

    @Unroll
    def "Ingest batches, sourcingLanes, boms data"(){
        given: "get showcase data"
        List<File> data = selectedFile("sourcingLanes, batches", allCSVFiles)
        List<File> staticDataJSON = selectedFile("boms", allJSONFiles)

        when: "Load boms, batches, sourcingLanes data parallel"
        List<Boolean> results0 = new LoadShowcase().loadParallel(true, null, 10, data, allControlFiles)
        List<Boolean> results1 = new LoadShowcase().loadParallel(true, null, 2, staticDataJSON, allControlFiles)

        then: "batches, sourcingLanes data ingest successfully"
        results0.every {
            it
        }
        and: "Boms data should ingest successfully"
        results1.every {
            it
        }
    }

    @Unroll
    def "Ingest nodes and measures"(){
        given: "All nodes and measures data"
        List<File> nodesData = selectedFile("nodes", allCSVFiles)
        List<File> measuresData = selectedFile("measures", allCSVFiles)

        when: "Load nodes data"
        List<Boolean> results1 = new LoadShowcase().loadParallel(true, null, 30, nodesData, allControlFiles)

        then: "Nodes data ingest successfully"
        results1.every {
            it
        }

        and:
        when: "load measures data"
        List<Boolean> results2 = new LoadShowcase().loadParallel(true, null, 30, measuresData, allControlFiles)

        then: "Nodes and measures data ingest successfully"
        results2.every {
            it
        }
    }

    @Unroll
    def "Ingest salesOrders data"() {
        given: "Get salesOrders data"
        List<File> salesOrdersData = selectedFile("salesOrders", allCSVFiles)

        when: "Load salesOrders data"
        List<Boolean> results = new LoadShowcase().loadParallel(true, null, 30, salesOrdersData, allControlFiles)

        then: "salesOrders data ingest successfully"
        results.every {
            it
        }
    }

    @Unroll
    def "Ingest standardPO data"() {
        given: "Get purchaseOrders data"
        List<File> data1 = selectedFile("standardPO", allCSVFiles)

        when: "Load standardPO data"
        List<Boolean> results1 = new LoadShowcase().loadParallel(true, null, 30, data1, allControlFiles)

        then: "standardPO data ingest successfully"
        results1.every {
            it
        }
    }

    @Unroll
    def "Ingest stockTransferPO data"() {
        given: "Get stockTransferPO data"
        List<File> data2 = selectedFile("stockTransferPO", allCSVFiles)

        when: "Load stockTransferPO data"
        List<Boolean> results2 = new LoadShowcase().loadParallel(true, null, 30, data2, allControlFiles)

        then: "stockTransferPO data ingest successfully"
        and:
        results2.every {
            it
        }
    }

    @Unroll
    def "Ingest receipts data"() {
        given: "Get receipts data"
        List<File> data2 = selectedFile("receipts", allCSVFiles)

        when: "Load receipts data"
        List<Boolean> results2 = new LoadShowcase().loadParallel(true, null, 1, data2, allControlFiles)

        then: "receipts data ingest successfully"
        and:
        results2.every {
            it
        }
    }

    @Unroll
    def "Ingest outboundDelivery data"() {
        given: "Get outboundDelivery data"
        List<File> outboundDeliveryData = selectedFile("outboundDelivery", allCSVFiles)

        when: "Load outboundDelivery data"
        List<Boolean> results = new LoadShowcase().loadParallel(true, null, 30, outboundDeliveryData, allControlFiles)

        then: "outboundDelivery data ingest successfully"
        results.every {
            it
        }
    }

    @Unroll
    def "Ingest inboundDelivery data"() {
        given: "Get inboundDelivery data"
        List<File> inboundDeliveryData = selectedFile("inboundDelivery", allCSVFiles)

        when: "Load inboundDelivery data"
        List<Boolean> results = new LoadShowcase().loadParallel(true, null, 30, inboundDeliveryData, allControlFiles)

        then: "inboundDelivery data ingest successfully"
        results.every {
            it
        }
    }

    @Unroll
    def "Ingest outboundShipment data"() {
        given: "Get outboundShipment data"
        List<File> outboundShipmentData = selectedFile("outboundShipment", allCSVFiles)

        when: "Load outboundShipment data"
        List<Boolean> results = new LoadShowcase().loadParallel(true, null, 30, outboundShipmentData, allControlFiles)

        then: "outboundShipment data ingest successfully"
        results.every {
            it
        }
    }

    @Unroll
    def "Ingest inboundShipment data"() {
        given: "Get inboundShipment data"
        List<File> inboundShipmentData = selectedFile("inboundShipment", allCSVFiles)

        when: "Load inboundShipment data"
        List<Boolean> results = new LoadShowcase().loadParallel(true, null, 30, inboundShipmentData, allControlFiles)

        then: "inboundShipment data ingest successfully"
        results.every {
            it
        }
    }
}
