package com.jda.dct.e2e.ui.lists;


import com.jda.dct.e2e.ui.utility.action.CommonActions;
import com.jda.dct.e2e.ui.utility.common.BaseTest;
import com.jda.dct.e2e.ui.utility.common.LeftMenu;
import com.jda.dct.e2e.ui.utility.helper.AssertHelper.AssertHelper;
import com.jda.dct.e2e.ui.utility.helper.Browser.BrowserHelper;
import com.jda.dct.e2e.ui.utility.pageobjects.*;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class DemandShipmentListTests extends BaseTest {

    private LeftMenu leftMenu;
    private TopMenuPage topMenu;
    private TopSubMenuPage topSubMenu;
    private DemandShipmentListPage demandShipmentListPage;
    private DemandShipmentDetailsPage demandShipmentDetailsPage;
    private WebDriver driver;
    private SupplyShipmentListPage supplyShipmentListPage;
    private AssertHelper assertHelper;
    private BrowserHelper browserHelper;
    private CommonActions commonActions;

    @BeforeClass
    public void setUp() throws InterruptedException {
        System.out.println("Starting tests for :" + this.getClass().getName());
        //Initialize required objects
        driver = getDriver();
        leftMenu = new LeftMenu(driver);
        topMenu = new TopMenuPage(driver);
        topSubMenu = new TopSubMenuPage(driver);
        supplyShipmentListPage = new SupplyShipmentListPage(driver);
        demandShipmentListPage = new DemandShipmentListPage(driver);
        demandShipmentDetailsPage = new DemandShipmentDetailsPage(driver);
        browserHelper = new BrowserHelper(driver);
        assertHelper = new AssertHelper(driver);
        commonActions = new CommonActions(driver);
        //loginAndNavigateToDemandLandingPage
        logInToApplication();
    }

    @BeforeMethod
    public void commonMethods() {
        leftMenu.clickDemandMenuOption();
        topMenu.goToDemandShipmentTab();
    }

    @Test(priority = 1,retryAnalyzer = BaseTest.class)
    public void validateDemandShipmentListPageIsDisplayed() throws InterruptedException {
        // verify user is at demand Shipment List Page
        //topMenu.clearFilterIfVisible();
        supplyShipmentListPage.clickOceanIcon();
        //demandShipmentListPage.clickDemandShipmentExceptionsHavingData();
        commonActions.disableAllExceptionFilter();
        assertHelper.assertTrue(demandShipmentListPage.isDemandShipmentListPageDisplayed(), "Demand Shipment List is displayed successfully");
    }

}


