package com.jda.dct.e2e.ui.situationrooms;


import com.jda.dct.e2e.ui.utility.common.BaseTest;
import com.jda.dct.e2e.ui.utility.common.LeftMenu;
import com.jda.dct.e2e.ui.utility.helper.Browser.BrowserHelper;
import com.jda.dct.e2e.ui.utility.helper.Wait.WaitHelper;
import com.jda.dct.e2e.ui.utility.helper.genericHelper.GenericHelper;
import com.jda.dct.e2e.ui.utility.pageobjects.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class SupplySituationRoomTests extends BaseTest {

    private LeftMenu leftMenu;
    private TopMenuPage topMenu;
    private TopSubMenuPage topSubMenu;
    private SituationRoomPage situationRoom;
    private SupplyShipmentListPage supplyShipmentListPage;
    private SupplyShipmentDetailsPage supplyShipmentDetailsPage;
    private WaitHelper waitHelper;
    private GenericHelper genricHelper;
    BrowserHelper browserHelper;
    private DemandShipmentDetailsPage demandShipmentDetailsPage;
    private WebDriver driver;


    @BeforeClass
    public void setUp() throws InterruptedException {
        //Initialize required objects
        driver = getDriver();
        leftMenu = new LeftMenu(driver);
        topMenu = new TopMenuPage(driver);
        topSubMenu = new TopSubMenuPage(driver);
        situationRoom = new SituationRoomPage(driver);
        supplyShipmentListPage = new SupplyShipmentListPage(driver);
        supplyShipmentDetailsPage = new SupplyShipmentDetailsPage(driver);
        browserHelper = new BrowserHelper(driver);
        waitHelper = PageFactory.initElements(driver, WaitHelper.class);
        genricHelper = PageFactory.initElements(driver, GenericHelper.class);
        //loginAndNavigateToSupplyLandingPage
        logInToApplication();
    }


    @Test(priority = 1)
    public void validateIsSituationRoomCreated() throws InterruptedException {
        String user1 = "MrA";
        leftMenu.clickSupplyMenuOption();
        topMenu.goToSupplyShipmentsTab();
        supplyShipmentListPage.clickOceanIcon();
        supplyShipmentListPage.clickShipmentExceptionHavingData();
        supplyShipmentListPage.clickSupplyShipmentNumber();
        situationRoom.clickSituationRoomIcon();
        situationRoom.clickCreateRoomButton();
        situationRoom.enterSituationRoomName();
        situationRoom.enterRoomDescription("item recall");
        situationRoom.enterIssuetype();
        situationRoom.addUser1();
        situationRoom.enterItemNumber("MAEU597252617");
        Thread.sleep(3000);
        situationRoom.clickCreate();
        Assert.assertTrue(situationRoom.isNewRoomCreated());
    }

    @Test(priority = 2)
    public void validateIsSituationRoomCreatedWithMultipleUsers() throws InterruptedException {
        browserHelper.refresh();
        waitHelper.waitForPageToLoad();
        leftMenu.clickSupplyMenuOption();
        waitHelper.waitForPageToLoad();
        Thread.sleep(3000);
        topMenu.goToSupplyShipmentsTab();
        supplyShipmentListPage.clickOceanIcon();
        supplyShipmentListPage.clickShipmentExceptionHavingData();
        supplyShipmentListPage.clickSupplyShipmentNumber();
        situationRoom.clickSituationRoomIcon();
        situationRoom.clickCreateRoomButton();
        situationRoom.enterSituationRoomName();
        situationRoom.enterRoomDescription("item recall");
        situationRoom.enterIssuetype();
        situationRoom.addUsers2();
        situationRoom.enterItemNumber("MAEU597252617");
        situationRoom.clickCreate();
        Assert.assertTrue(situationRoom.isNewRoomCreated());

    }




}
