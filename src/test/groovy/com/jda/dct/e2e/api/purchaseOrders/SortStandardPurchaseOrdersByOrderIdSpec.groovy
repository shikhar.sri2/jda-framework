package com.jda.dct.e2e.api.purchaseOrders

import com.jda.dct.e2e.api.categories.Smoke
import com.jda.dct.e2e.api.utility.IngestionHelper
import com.jda.dct.e2e.api.utility.QueryObject
import com.jda.dct.e2e.api.utility.QueryResultHandler
import org.junit.experimental.categories.Category
import spock.lang.Specification
import spock.lang.Unroll

class SortStandardPurchaseOrdersByOrderIdSpec extends Specification implements IngestionHelper {

    def setupSpec() {
        init()
    }

    @Category(Smoke)
    @Unroll
    def "retrieve purchase orders by full text search and sort them by their purchaseOrderId"() {
        given: "purchase orders containing \"CO\" in their purchaseOrderId field"

        when: "retrieve the orders sorted by their purchaseOrderId in descending order"
        def queryObject = new QueryObject()
        queryObject.setType("purchaseOrders")
        queryObject.addSortFields("purchaseOrderId", "desc", "string")
/*        queryObject.addFullTextQuery()
                .searchText("CO")
                .searchField("purchaseOrderId")
*/
        queryObject.addQuery("purchaseOrderType", "standardPO")
        def response = QueryResultHandler.getQueryResultHandler().executeWithRetry(queryObject, 20, 3, 5, 1, 1)

        then: "the api should return a success status code and orders containing \"CO\" sorted by their purchaseOrderId in descending order"
        response.rawResponse.status == 200

        (0..response.body.data.size() - 2).each({ i ->
            assert response.body.data[i].purchaseOrderId >= response.body.data[i + 1].purchaseOrderId
        })
    }
}