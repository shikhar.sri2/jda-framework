package com.jda.dct.e2e.api

import com.jda.dct.e2e.api.utility.AuthHelper
import groovyx.net.http.ContentType
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import spock.lang.Shared
import spock.lang.Specification
import groovyx.net.http.RESTClient
import spock.lang.Unroll

// ask haiying/vida/hide what even is this api???
class GetClientSecretsSpec extends Specification implements AuthHelper {
    static clientSecretURL
    @Shared
    def client

    def setupSpec() {
        init()

        def clientSecretPath = properties.getProperty('getClientSecretsPath')
        clientSecretURL = protocol + ip + clientSecretPath
        client = new RESTClient(clientSecretURL)
        client.handler.failure = { resp -> resp }
        header[HttpHeaders.CONTENT_TYPE] = MediaType.APPLICATION_JSON
    }

    // smoke
    @Unroll
    def "Get client secrets API #url"() {
        given: "The API URL is #url "
        when: "HTTP Method is GET"
        def response = client.get(headers: header)
        then: "the response should be 200"
        with(response) {
            status == 200
        }
        where:
        url << [clientSecretURL]
    }

    // regression
    @Unroll
    def "Create client secret and delete client secret with API #url"() {
        def id
        given: "Client secret is created with description #descriptionTitle using #url"
        def response = client.post(
                requestContentType : ContentType.JSON,
                headers : header,
                body    : [ 'description' : description,
                            'expirationDate' : ''])
        and: "Client ID from the newly created client secret"
        id = response.data.data[0].clientId
        and: "Response is 200 OK"
        with(response) {
            status == 200
        }
        and: "Client secret description is #description"
        with(response.data) {
            data[0].description == description
        }
        when: "I delete the client secret"
        def deleteResponse = client.delete(
                path: id,
                headers : header)
        then: "Response is 204"
        with(deleteResponse) {
            status == 204
        }
        where:
        url << [clientSecretURL]
        description = "e2e-test-post"
    }
}
