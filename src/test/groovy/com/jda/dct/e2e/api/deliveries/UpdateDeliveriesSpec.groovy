package com.jda.dct.e2e.api.deliveries


import com.jda.dct.e2e.api.categories.Regression
import com.jda.dct.e2e.api.categories.Smoke
import com.jda.dct.e2e.api.sites.GetSiteShowcase
import com.jda.dct.e2e.api.utility.IngestionHelper
import groovy.json.JsonSlurper
import org.junit.experimental.categories.Category
import org.springframework.http.HttpMethod
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Stepwise
import spock.util.concurrent.PollingConditions

/**
 * End-to-end tests for {@code Delivery} ingestion.
 *
 * @author Vida Bandara
 * @version 1.0-SNAPSHOT
 * @since 24-Apr-2018
 */

// positive tests are OBE because loadshowcase
@Stepwise
class UpdateDeliveriesSpec extends Specification implements IngestionHelper {

    @Shared Map<String, String> modifiedFields = new HashMap<>()
    @Shared DeliveryShowcaseData sample = new DeliveryShowcaseData()
    @Shared Random random = new Random()

    def setupSpec() {
        init()
        def sitePath = properties.getProperty('ingestSitesPath')
        def deliveryPath = properties.getProperty('ingestDeliveriesPath')
        def getPath = properties.getProperty('getDeliveryPath')
        def searchDeliveryPath = properties.get("searchDeliveryPath")
        def searchSitePath = properties.get("searchSitePath")
        siteIngestionUrl = protocol + ip + sitePath
        ingestionURL = protocol + ip + deliveryPath
        getURLPrefix = protocol + ip + getPath
        searchDeliveryUrlPrefix = protocol + ip + searchDeliveryPath
        searchSiteUrlPrefix = protocol + ip + searchSitePath
    }

    @Category(Smoke)
    def "Add a Note to a Delivery and get it back"() {
        given:
        def conditions = new PollingConditions(timeout: 120, initialDelay: 1)

        when: 'A Note is added to the Delivery'
        def res
        conditions.eventually {
            res = executeHttpCall(header, getURLPrefix, null, HttpMethod.GET)
            assert res != null
        }
        def deliveryId = res['data'][0]['id']
        def url = getURLPrefix + 'notes/' + deliveryId
        println(url)
        def sampleNote = '{ "note": "Delivery Note added"}'
        def response = getHttpClient(header, url, sampleNote, HttpMethod.POST)
        assert response.status == 200

        and: "Get the Delivery "
        url = getURLPrefix + deliveryId
        def deliveryText = getHttpClient(header, url, null, HttpMethod.GET).body
        def deliveryRetrieved = new JsonSlurper().parseText(deliveryText).get('data').get(0)

        then: 'Get the Delivery  back and verify the note'
        assert deliveryRetrieved.getAt('notes').toString().contains("Delivery Note added")
    }

    @Category(Regression)
    def "field in process model updates upon a new ingest"() {
        given: 'original delivery'
        String carrier = "carrier" + random.nextInt(100).toString()
        modifiedFields.clear()
        modifiedFields.put("carrier", carrier)
        def updatedDeliveryList = sample.modifyDeliveryMultiFields(false, true, "outboundDelivery", modifiedFields, "temp-delivery.json")
        String deliveryId = sample.getDeliveryId()

        when: 'ingest updated delivery'
        ingestData(updatedDeliveryList, "deliveries?processType=demand&deliveryType=outboundDelivery")
        def delivery = sample.getDeliveryFromShowcase(true, false, deliveryId)

        then: 'the process model field updated'
        delivery['carrier'] == carrier
    }

//    def "Delivery with no site description gets filled in based on site names"() {
//        given: 'A site with a description is ingested'
//        ingestData(sampleSiteWithDesc, siteIngestionUrl)
//
//        when: "A delivery is ingested that does not contain a description"
//        ingestData(sampleDeliveryNoDesc)
//
//        def deliveryIds = getIds(sampleDeliveryNoDesc)
//        def url = getURLPrefix + deliveryIds[0]
//        def response = getHttpClient(header, url, null, HttpMethod.GET)
//        def deliveryText = response.body
//        println 'delivery text: ' + deliveryText
//        def deliveryRetrieved = new JsonSlurper().parseText(deliveryText).get('data').get(0)
//
//        def mapper = new ObjectMapper()
//        def json = JsonOutput.toJson(deliveryRetrieved)
//        println 'json: ' + json
//        def received = mapper.readValue(json, Delivery)
//        println 'received: ' + received
//
//        then:
//        received.getShipFromSiteDescription() == "Description to be copied"
//        received.getShipToSiteDescription() == "Description to be copied"
//    }

    /* @Category(Regression)
   not sure if this test case is valid or not
   def "field not in process model not updated upon a new ingest"() {
       given: 'original delivery ingested'
       DeliveryShowcaseData sample = new DeliveryShowcaseData()
       def originalDelivery = sample.modifyDelivery("OD0003_SU", "flexFloat1", "123.45", "temp-delivery.json")
       def updatedDelivery = sample.modifyDelivery("OD0003_SU", "flexFloat1", "999.45", "temp-delivery.json")

       when: 'ingest updated delivery'
       ingestData(originalDelivery, "deliveries?processType=demand&deliveryType=outboundDelivery")
       def delivery1 = sample.getDeliveryFromShowcase("OD0003_SU")
       ingestData(updatedDelivery, "deliveries?processType=demand&deliveryType=outboundDelivery")
       def delivery2 = sample.getDeliveryFromShowcase("OD0003_SU")

       then: 'non-process model field not updated'
       delivery1["flexFloat1"] ==  delivery2["flexFloat1"]
   }*/
}
