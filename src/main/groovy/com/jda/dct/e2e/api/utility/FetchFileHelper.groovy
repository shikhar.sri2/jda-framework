package com.jda.dct.e2e.api.utility

import org.json.simple.JSONArray
import org.json.simple.parser.JSONParser
import org.junit.platform.commons.util.StringUtils

trait FetchFileHelper extends IngestionHelper{

    /**
     * Get master-data, testing-data, ums-data from tenants/show-case/
    * */
    List<List<File>> getShowcaseData(String tid) {
        List<File> allCSVFiles = new ArrayList<>()
        List<File> allControlFiles = new ArrayList<>()
        List<File> allJSONFiles = new ArrayList<>()
        getFilesFromDir(allCSVFiles, allControlFiles, allJSONFiles, "show-case/master-data")
        getFilesFromDir(allCSVFiles, allControlFiles, allJSONFiles, "show-case/testing-data")

        List<File> allUmsFiles = new ArrayList<>()
        getFilesFromDir("show-case/ums-data/" + tid, allUmsFiles)

        List<List<File>> res = new ArrayList<>()
        res.add(allCSVFiles)
        res.add(allControlFiles)
        res.add(allUmsFiles)
        res.add(allJSONFiles)
        res
    }

    /**
     * Divide all files of a given folder into CSVFilesList, ControlFilesList, JSONFilesList
    * */
    void getFilesFromDir(List<File> allCSVFiles, List<File> allControlFiles, List<File> allJSONFiles, String folderPath) {
        File[] listOfFiles = new File("build/" + folderPath).listFiles()
        if (listOfFiles != null && listOfFiles.length > 0) {
            for (File file : listOfFiles) {
                if (file.isFile() && (file.getName().contains(".csv"))) {
                    allCSVFiles.add(file)
                } else if (file.isFile() && (file.getName().contains(".ctl"))) {
                    allControlFiles.add(file)
                }else if (file.isFile() && (file.getName().contains(".json"))){
                    allJSONFiles.add(file)
                }
            }
        }
    }

    void getFilesFromDir(String folderPath, List<File> res) {
        File[] listOfFiles = new File("build/" + folderPath).listFiles()
        if (listOfFiles != null && listOfFiles.length > 0) {
            for (File file : listOfFiles) {
                if (file != null) {
                    if (file.isFile()) {
                        res.add(file)
                    } else if (file.isDirectory()) {
                        getFilesFromDir(folderPath + "/" + file.getName(), res)
                    }
                }
            }
        }
        res
    }
    /**
     * Get the check file(.ctl) of a csv file
     * */
    File getCheckFile(File dataFile, List<File> allControlFiles) {
        if (allControlFiles == null || allControlFiles.isEmpty()) {
            return null
        }
        for (File f : allControlFiles) {
            String dataFileName = dataFile.getName().substring(0, dataFile.getName().lastIndexOf("."))
            String controlFileName = f.getName().substring(0, f.getName().lastIndexOf("."))
            if (dataFileName.equals(controlFileName)) {
                return f
            }
        }
        return null
    }

    File getProcessModel(String tenantName) {
        File processModel = null
        File[] subFiles = new File("build/" + tenantName).listFiles()
        if (subFiles != null && subFiles.length > 0) {
            for (File file : subFiles) {
                if (file.isFile() && file.getName().contains("model")) {
                    processModel = file
                    break
                }
            }
        }
        processModel
    }

    /**
     * Get all csv files and process model of given tenant name,
     * input can be show-case, Electrolux or other tenants
     *
    */
    List<File> getAllFiles(String dataName) {
        List<File> allFiles = new ArrayList<>()
        File rootFile = new File("build/" + dataName)
        Queue queue = new LinkedList<File>()
        queue.offer(rootFile)

        while (!queue.isEmpty()) {
            int counter = 0
            File[] listOfFiles = queue.peek().listFiles()
            while (listOfFiles == null && counter < 50) {
                counter++
            }
            if (listOfFiles != null && listOfFiles.length > 0) {
                for (File file : listOfFiles) {
                    if (file.isFile() && (file.getName().contains(".csv") || file.getName().contains("model"))) {
                        allFiles.add(file)
                    } else if (file.isDirectory()) {
                        queue.offer(file)
                    }
                }
            } else {
                println "No file found in " + queue.peek().getName()
            }
            queue.poll()
        }
        allFiles
    }

    List<File> selectedFile(String boNames, allCSVFiles) {
        List<File> selected = new ArrayList<>()
        String[] bos = boNames.trim().split("\\s*,\\s*")
        for (File file : allCSVFiles) {
            if (file != null) {
                String postUrl = getPostUrl(file.getName())
                for (String boName : bos) {
                    if (postUrl.contains(boName)) {
                        selected.add(file)
                    }
                }
            }
        }
        return selected
    }

    void selectByKeyword(String keyWords, List<File> input, List<File> output, boolean keywordIsFileName) {
        String[] keyWordsArray = keyWords.trim().split(",")
        if (keyWordsArray == null || keyWordsArray.length == 0 || input == null || input.isEmpty()) {
            return
        }
        for (String keyword : keyWordsArray) {
            if (StringUtils.isBlank(keyword)) {
                continue
            }
            for (File file : input) {
                if (file != null && file.isFile()) {
                    String fileName = file.getName().substring(0, file.getName().indexOf(".json"))
                    if (keywordIsFileName && fileName.equals(keyword)) {
                        output.add(file)
                    } else if (!keywordIsFileName && fileName.contains(keyword)) {
                        output.add(file)
                    }
                } else if (file != null && file.isDirectory()) {
                    selectByKeyword(keyword, Arrays.asList(file.listFiles()), output, keywordIsFileName)
                }
            }
        }
    }

    JSONArray selectJsonArrayByKeyword(String keyWords, List<File> input, List<File> output, boolean keywordIsFileName) {
        selectByKeyword(keyWords, input, output, keywordIsFileName)
        JSONArray  jsonArray = new JSONArray()
        JSONParser parser = new JSONParser()
        for (File f : output) {
            Object obj = parser.parse(f.text)
            jsonArray.addAll((JSONArray) obj)
        }
        jsonArray
    }

    /**
    reorder a list of csv file with given loading order, return a ordered list of csv files
     */
    List<File> reorderCSV(List<File> csvFiles) {
        List<File> orderedCSV = new ArrayList<File>()
        //this is important, since objects should be loaded in certain order, for example, nodes need to be loaded
        //before measures are loaded
        String[] orders = ["sites", "items", "sourcingLanes", "participants", "batches", "calendars", "boms", "nodes", "measures",
                           "salesOrders", "purchaseOrders", "deliveries", "shipments"]
        Map<String, List<File>> linkedHashMap = new LinkedHashMap<>()
        for (int i = 0; i < orders.length; i++) {
            linkedHashMap.put(orders[i], new ArrayList<File>())
        }
        for (File file : csvFiles) {
            if (!file.getName().contains(".csv")) {
                continue
            }
            String key = getResourceName(file.getName())
            List<File> files = linkedHashMap.get(key)
            if (files == null) {
                //this model is not in the predefined list, append to the end.
                files = new ArrayList<>()
                linkedHashMap.put(key, files)
            }
            files.add(file)
        }
        for (Map.Entry<String, List<File>> entry : linkedHashMap.entrySet()) {
            for (File file : entry.getValue()) {
                orderedCSV.add(file)
            }
        }
        orderedCSV
    }
}