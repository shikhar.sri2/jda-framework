package com.jda.dct.e2e.api.alerts
//package com.jda.dct.e2e.api
// look into whether we need to cover this in ReadyAPI

//class com.jda.dct.e2e.api.alerts.AlertAPISpec extends Specification implements IngestionHelper {
//
//    @Shared
//    def salesOrderGet
//
//    @Shared
//    def salesOrderIngestionURL
//
//    @Shared
//    def purchaseOrderPath
//
//    @Shared
//    def unReadAlertUrl
//
//    @Shared
//        samplePOLate
//
//    @Shared searchAlertPath
//
//    def setupSpec() {
//        init()
//        purchaseOrderPath = properties.getProperty('ingestPurchaseOrdersPath')
//        def getPath = properties.getProperty('getPurchaseOrderPath')
//        searchAlertPath = properties.get("searchAlertPath")
//        def unReadAlertPath = properties.get("unReadAlertPath")
//        searchAlertUrlPrefix = protocol + ip + searchAlertPath
//        purchaseOrderIngestionUrl = protocol + ip + purchaseOrderPath
//        ingestionURL = protocol + ip + purchaseOrderPath
//        getURLPrefix = protocol + ip + getPath
//        salesOrderIngestionURL = protocol + ip + properties.getProperty('ingestSalesOrdersPath')
//        searchAlertPath = properties.get("searchAlertPath")
//        searchAlertUrlPrefix = protocol + ip + searchAlertPath
//        unReadAlertUrl = protocol +ip + unReadAlertPath + '?query=state=Open&aggregationField=objectType'
//    }
//    def "Update alert readby users when alert is marked as read"() {
//        given: 'ingest sample purchase order'
//        samplePOLate = this.getClass().getClassLoader().getResource('sample-alert-update-readby-1.json').text
//
//        when: 'Ingesting data'
//        ingestionURL = protocol + ip + purchaseOrderPath
//        ingestData(samplePOLate)
//        def purchaseOrderIds = getPurchaseOrderIds(samplePOLate)
//        sleep(10000)
//        final Map<String, String> searchCriteria = new HashMap<>()
//        searchCriteria.put("limit", "1")
//        searchCriteria.put("query", "objectId=" + purchaseOrderIds[0])
//        def responseDef =
//                searchResponseJson(searchCriteria, searchAlertUrlPrefix, "Open", 60)
//        def alertDef = responseDef.get("data").getAt(0)
//        def alertId = alertDef.get("id")
//        def resourceUrl = searchAlertUrlPrefix + "/" + alertId
//        ResponseEntity response = getHttpClient(header, resourceUrl, null, HttpMethod.PUT)
//
//        then:
//        assert responseDef != null
//        assert alertDef.get("objectId") != null
//        assert alertDef.get("objectId") == purchaseOrderIds[0]
//        assert alertDef.get("objectType") != null
//        assert alertDef.get("objectType") == 'PurchaseOrder'
//        assert response.statusCodeValue == 200
//
//    }
//
//    def "Get the unread alert count"() {
//        given: 'ingest sample  purchase order'
//        samplePOLate = this.getClass().getClassLoader().getResource('sample-alert-unreadcount-1.json').text
//        when: 'Ingesting data'
//        ingestionURL = protocol + ip + purchaseOrderPath
//        ingestData(samplePOLate)
//        sleep(10000)
//        def response = executeHttpCall(header, unReadAlertUrl, null, HttpMethod.GET)
//        def unread = response.getAt("totalCount")
//        def typeCount = response.getAt("typeCounts")
//
//        then:
//        assert response != null
//        assert unread > 0
//        assert typeCount.size > 0
//    }
//
//    def "Get the getMostRecent alert"() {
//
//        given: "The API URL"
//
//        def getMostRecentURL = protocol + ip + searchAlertPath + "/recent"
//
//        when: 'Fetch an alert'
//        def response = executeHttpCall(header, getMostRecentURL, null, HttpMethod.GET)
//        def count = response.getAt("count")
//        def alertStatusLmd = response.getAt("data").getAt("alertStatusLmd")
//
//        then:
//        assert response != null
//        assert count == 1
//        assert alertStatusLmd != null
//    }
//}
