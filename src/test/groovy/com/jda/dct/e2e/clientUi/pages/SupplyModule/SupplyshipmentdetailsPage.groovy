package com.jda.dct.e2e.clientUi.pages.SupplyModule

import geb.Page
import org.openqa.selenium.By


class SupplyshipmentdetailsPage extends Page {
    static url = "/tempraturecontrolled/shipment"

    static atCheckWaiting = true

    static at = {

        $("#shipment-detail-title").isDisplayed()
    }

    static content = {

        shipmentprogress(wait: true) {
            $(By.cssSelector("//text()[.='Containers']/ancestor::div[1]]"))
        }


        trailerNo(wait:true) {
            $(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Trailer No'])[1]/preceding::a[1]"))
        }


        deliverylisttitle(wait:true){

            $("#tempraturecontrolled-page-tab-deliveries")
        }

        navigateback(wait: true) {

            $("a", text: "Shipment")

        }
    }



    def  selectshipmentcontainer(){

      waitFor {
             trailerNo.displayed
      }
        Thread.sleep(3000)
        trailerNo.click()

        println("Shipment trailer no is clicked; navigating to Deliverylist")


        waitFor {

            deliverylisttitle.displayed

        }
    }


    def gobacktosupply() {

        navigateback.click()

        println(" welcome to shipment details page ")
    }
}