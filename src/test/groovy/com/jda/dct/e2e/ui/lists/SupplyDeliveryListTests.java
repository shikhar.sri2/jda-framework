package com.jda.dct.e2e.ui.lists;

import com.jda.dct.e2e.ui.utility.action.CommonActions;
import com.jda.dct.e2e.ui.utility.common.BaseTest;
import com.jda.dct.e2e.ui.utility.common.LeftMenu;
import com.jda.dct.e2e.ui.utility.helper.AssertHelper.AssertHelper;
import com.jda.dct.e2e.ui.utility.pageobjects.SupplyDeliveryListPage;
import com.jda.dct.e2e.ui.utility.pageobjects.SupplyShipmentListPage;
import com.jda.dct.e2e.ui.utility.pageobjects.TopMenuPage;
import com.jda.dct.e2e.ui.utility.pageobjects.TopSubMenuPage;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class SupplyDeliveryListTests extends BaseTest {
    private LeftMenu leftMenu;
    private TopMenuPage topMenu;
    private TopSubMenuPage topSubMenu;
    private SupplyDeliveryListPage supplyDeliveryPage;
    private SupplyShipmentListPage supplyShipmentListPage;
    private AssertHelper assertHelper;
    private CommonActions commonActions;
    private WebDriver driver;


    @BeforeClass
    public void setUp() throws InterruptedException {
        System.out.println("Starting tests for :" + this.getClass().getName());
        //Initialize required objects
        driver = getDriver();
        leftMenu = new LeftMenu(driver);
        topMenu = new TopMenuPage(driver);
        topSubMenu = new TopSubMenuPage(driver);
        supplyDeliveryPage = new SupplyDeliveryListPage(driver);
        assertHelper = new AssertHelper(driver);
        commonActions = new CommonActions(driver);
        //loginAndNavigateToSupplyLandingPage
        logInToApplication();
    }

    @BeforeMethod
    public void commonMethods(){
        leftMenu.clickSupplyMenuOption();
    }

    @Test(priority = 1,retryAnalyzer = BaseTest.class)
    public void validateSupplyDeliveryListPageIsDisplayed() throws InterruptedException {
        topMenu.goToSupplyDeliveriesTab();
        Thread.sleep(2000);
        supplyDeliveryPage.clickOceanIcon();
        commonActions.disableAllExceptionFilter();
        assertHelper.assertTrue(supplyDeliveryPage.isSupplyDeliveryListPageDisplayed(), "Supply Delivery list page is displayed successfully");

    }
}