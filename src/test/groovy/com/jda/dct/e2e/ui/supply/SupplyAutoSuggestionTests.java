//package com.jda.dct.e2e.ui.supply;
//
//import BaseTest;
//import LeftMenu;
//import com.jda.dct.e2e.utility.ui.pageobjects.*;
//import org.openqa.selenium.WebDriver;
//import org.testng.Assert;
//import org.testng.annotations.BeforeClass;
//import org.testng.annotations.BeforeMethod;
//import org.testng.annotations.Test;
//
//public class SupplyAutoSuggestionTests extends BaseTest {
//
//    private LeftMenu leftMenu;
//    private TopMenuPage topMenu;
//    private TopSubMenuPage topSubMenu;
//    private SupplyShipmentListPage supplyShipmentListPage;
//    private SupplyOrderListPage supplyOrderListPage;
//    //private SupplyInventoryListPage supplyInventoryListPage;
//    private SupplyDeliveryListPage supplyDeliveryListPage;
//    private SupplyForecastListPage supplyForecastListPage;
//    private WebDriver driver;
//
//    @BeforeClass
//    public void setUp() throws InterruptedException {
//        System.out.println("Starting tests for :" + this.getClass().getName());
////Initialize required objects
//        driver = getDriver();
//        leftMenu = new LeftMenu(driver);
//        topMenu = new TopMenuPage(driver);
//        topSubMenu = new TopSubMenuPage(driver);
//        supplyOrderListPage = new SupplyOrderListPage(driver);
//        //supplyInventoryListPage = new SupplyInventoryListPage(driver);
//        supplyDeliveryListPage = new SupplyDeliveryListPage(driver);
//        supplyShipmentListPage = new SupplyShipmentListPage(driver);
//        supplyForecastListPage = new SupplyForecastListPage(driver);
//    }
//
//    @BeforeMethod
//    public void commonFunction() throws InterruptedException{
//        leftMenu.clickSupplyMenuOption();
//    }
//
//    @Test(priority = 1)
//    public void validateAutoSuggestionInSupplyShipmentlistPage() throws InterruptedException {
//        topMenu.goToSupplyShipmentsTab();
//        topSubMenu.clickSearchButton();
//        //enter letters in search box
//        topSubMenu.getAutoSuggestionForAnySearchDataEntered("is0");
//        Thread.sleep(3000);
////verify user sees the first text on the list
//        Assert.assertTrue(topSubMenu.isAutoSuggestionListDisplayed());
//
//    }
//
//
//    @Test(priority = 2)
//    public void validateAutoSuggestionInSupplyDeliveryListPage() throws InterruptedException {
//
//        topMenu.goToSupplyDeliveriesTab();
//        topSubMenu.clickSearchButton();
//        //enter letters in search box
//        topSubMenu.getAutoSuggestionForAnySearchDataEntered("is0");
//        Thread.sleep(3000);
//        Assert.assertTrue(topSubMenu.isAutoSuggestionListDisplayed());
//
//    }
//
//
//    @Test(priority = 3)
//    public void validateAutoSuggestionInSupplyOrdersListPage() throws InterruptedException {
//        topMenu.goToSupplyOrdersTab();
//        topSubMenu.clickSearchButton();
//        //enter letters in search box
//        topSubMenu.getAutoSuggestionForAnySearchDataEntered("is0");
////verify user sees the first text on the list
//        Assert.assertTrue(topSubMenu.isAutoSuggestionListDisplayed());
//    }
//
////
////    @Test(priority = 4)
////    public void validateAutoSuggestionInSupplyInventoryListPage() throws InterruptedException {
////        topMenu.goToSupplyInventoryTab();
////        topSubMenu.clickSearchButton();
////        //enter letters in search box
////        topSubMenu.getAutoSuggestionForAnySearchDataEntered("is0");
//////verify user sees the first text on the list
////        Assert.assertTrue(topSubMenu.isAutoSuggestionListDisplayed());
////    }
//
//
//    @Test(priority = 5)
//    public void validateAutoSuggestionInSupplyForecastListPage() throws InterruptedException {
//        topMenu.goToSupplyForecastCommitTab();
//        topSubMenu.clickSearchButton();
//        //enter letters in search box
//        topSubMenu.getAutoSuggestionForAnySearchDataEntered("is0");
//        Thread.sleep(3000);
////verify user sees the first text on the list
//        Assert.assertTrue(topSubMenu.isAutoSuggestionListDisplayed());
//    }
//
//
//}