//package com.jda.dct.e2e.clientUi.pages.watchTower
//
//import geb.Page
//
//class WatchTowerPage extends Page{
//    static url = "/"
//
//    static atCheckWaiting = true
//
//    static at = {
//        title == "Luminate Control Tower - Luminate Control Tower"
//        $("button[aria-label=Menu]").displayed
//    }
//
//
//
//    static content = {
//        menuButton(wait: true) {
//            $("button[aria-label=Menu]")
//        }
//        slidRightButton(wait: true) {
//            $("#icon_next")
//        }
//        slidLeftButton(wait: true) {
//            $("#icon_previous")
//        }
//        demand(wait: true) {
//            $("a[href*='demand']").children()
//        }
//        tempraturecontrolled(wait: true) {
//            $("a[href*='tempraturecontrolled']").children()
//        }
//        analytics(wait: true) {
//            $("a[href*='analytics']").children()
//        }
//        insights(wait: true) {
//            $("a[href*='insights']").children()
//        }
//
//    }
//
//
//
//
//
//    def navigateToDemand() {
//        println "\nFrom watch tower page, user can navigate to demand module"
//        println "User click menu button"
//        menuButton.click()
//        println "Current page url: " + getExceptionPage().getCurrentUrl()
//
//        println "User click demand button"
//        demand.click()
//        println "Current page url: " + getExceptionPage().getCurrentUrl()
//    }
//
//    def navigateToSupply() {
//        println "\nFrom watch tower page, user can navigate to tempraturecontrolled module"
//        println "User click menu button"
//        menuButton.click()
//        println "Current page url: " + getExceptionPage().getCurrentUrl()
//
//        println "User click tempraturecontrolled button"
//        tempraturecontrolled.click()
//        println "Current page url: " + getExceptionPage().getCurrentUrl()
//    }
//
//    def navigateToAnalytics() {
//        println "\nFrom watch tower page, user can navigate to analytics module"
//        println "User click menu button"
//        menuButton.click()
//        println "Current page url: " + getExceptionPage().getCurrentUrl()
//
//        println "User click tempraturecontrolled button"
//        analytics.click()
//        println "Current page url: " + getExceptionPage().getCurrentUrl()
//    }
//
//    def navigateToInsight() {
//        println "\nFrom watch tower page, user can navigate to insights module"
//        println "User click menu button"
//        menuButton.click()
//        println "Current page url: " + getExceptionPage().getCurrentUrl()
//
//        println "User click insight button"
//        insights.click()
//        println "Current page url: " + getExceptionPage().getCurrentUrl()
//    }
//
//    def rightSlideNavigation() {
//        println "\nFrom watch tower page, user can click on right slider"
//        println "User click right slide"
//        sleep(3000)
//        slidRightButton.click()
//        sleep(5000)
//        println "Current page url: " + getExceptionPage().getCurrentUrl()
//    }
//
//    def leftSlideNavigation() {
//        println "\nFrom watch tower page, user can click on left slider"
//        println "User click left slide"
//        sleep(3000)
//        slidLeftButton.click()
//        sleep(5000)
//        println "Current page url: " + getExceptionPage().getCurrentUrl()
//    }
//}
