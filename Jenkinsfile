#! /usr/bin/env groovy

@Library('jenkins-lib@master-ci') _

// Parameter passed to gradle and ReadyAPI that dictates the tests that should be run ["Smoke", "Regression", "Standard Demo"]
def testSuite = "Regression"

pipeline {
    agent {
        node {
            label "microservice"
        }
    }
    triggers {
        pollSCM "H 6,7 * * 1-5"
        // Nightly regression test run; 9:00 PM US Central Time
        cron "H 22 * * *"
    }
    parameters {
        string(name: 'resourceGroup',
                defaultValue: 'lct-cicd-itest-01',
                description: 'The cluster resource group')
        string(name: 'namespace',
                defaultValue: 'lct-cicd-itest-01',
                description: 'The cluster namespace')
        string([name: 'isFinal',
            defaultValue: 'false',
            description: 'If true on release branch will run final build'])
        choice(name: 'testSuite',
                choices: "Smoke\nRegression\nStandard Demo",
                description: 'Select test suite to run.')
        booleanParam(name: 'skipTest',
                defaultValue: false,
                description: 'Skip Test')
        booleanParam(name: 'skipClean',
                defaultValue: true,
                description: 'Skip Clean')
        string([name        : 'version',
                defaultValue: '2019.1.3',
                description : 'Version for build artifacts'])
        string([name        : 'depVersion',
                defaultValue: '2019.1.3',
                description : 'Version for build artifacts'])
        string([name        : 'platformVersion',
                defaultValue: '2019.1.3',
                description : 'Version for platform build artifacts'])
        string([name        : 'previousReleaseVersion',
                defaultValue: '2019.1.0',
                description : 'Version for build artifacts'])
        string([name        : 'previousReleaseFoundationVersion',
                defaultValue: '2019.1.0',
                description : 'Version for build artifacts'])
        string([name        : 'previousReleaseplatformVersion',
                defaultValue: '2019.1.0',
                description : 'Version for platform build artifacts'])
        string([name        : 'latestReleaseVersion',
                defaultValue: '2019.1.2',
                description : 'Version for build artifacts'])
        string([name        : 'latestReleaseFoundationVersion',
                defaultValue: '2019.1.2',
                description : 'Version for build artifacts'])
        string([name        : 'latestReleaseplatformVersion',
                defaultValue: '2019.1.2',
                description : 'Version for platform build artifacts'])
    }
    environment {
        ARTIFACTORY_USER = credentials('artifactoryUser')
        ARTIFACTORY_PASSWORD = credentials('artifactoryPassword')
        AZURE_USER = credentials('azureServicePrincipalUser')
        AZURE_PASSWORD = credentials('azureServicePrincipalPassword')
        AZURE_TENANT = credentials('azureServicePrincipalTenant')
        DEV_CI_WEB_RESOURCE_GROUP = "dctui-intergration"
        ES_PUBLIC_IP_NAME = "es-external-lb-ip"
        GRADLE_USER_HOME = "/var/lib/jenkins/e2e-testing-cache"
        RESOURCE_GROUP = "$params.resourceGroup"
        DEPLOY_STACK = "$params.resourceGroup"
        VERSION = "$params.version"
        DEP_VERSION = "$params.depVersion"
        PLATFORM_VERSION = "$params.platformVersion"
        PREVIOUS_RELEASE_VERSION = "$params.previousReleaseVersion"
        PREVIOUS_RELEASE_FOUNDATION_VERSION = "$params.previousReleaseFoundationVersion"
        PREVIOUS_RELEASE_PLATFORM_VERSION = "$params.previousReleaseplatformVersion"
        LATEST_RELEASE_VERSION = "$params.latestReleaseVersion"
        LATEST_RELEASE_FOUNDATION_VERSION = "$params.latestReleaseFoundationVersion"
        LATEST_RELEASE_PLATFORM_VERSION = "$params.latestReleaseplatformVersion"
        NAME_SPACE = "$params.namespace"
        ACCESS_TOKEN = credentials('accessToken3')
        UI_CREDENTIALS = credentials("36337fff-1e4c-4be9-8117-3feb36b29a75")
    }
    options {
        skipDefaultCheckout()
        disableConcurrentBuilds()
        lock resource: "${params.resourceGroup}"
    }
    stages {
        stage('Checkout') {
            options {
                skipDefaultCheckout()
            }
            steps {
                checkout scm
                node(label: "readyapi") {
                    checkout scm
                }
                script {
                    env.GIT_COMMIT_EMAIL = sh(script: 'git --no-pager show -s --format=\'%ae\'', returnStdout: true).trim()
                    env.GIT_COMMIT_HASH = sh(returnStdout: true, script: 'git rev-parse HEAD').trim()
                    println "GIT_COMMIT_EMAIL: $env.GIT_COMMIT_EMAIL"
                    if (env.GIT_COMMIT_EMAIL == "nobody@nowhere") {
                        env.GIT_COMMIT_EMAIL = "ravi.nannuri@jda.com"
                    }
                    println "GIT_COMMIT_EMAIL: $env.GIT_COMMIT_EMAIL"
                    if (env.BRANCH_NAME == "master-ci") {
                        env.RECIPIENTS = "Syed.Kazmi@jda.com,Haidong.Li@jda.com,Jack.Yen@jda.com,Haiying.Wang@jda.com,Xiaofeng.Shen@jda.com,Vida.Bandara@jda.com,Ganesh.Kadayam@jda.com,Ravi.Nannuri@jda.com,Max.Brito@jda.com,Nhi.Dang@jda.com,Yolanda.Jia@jda.com,Jay.Liu@jda.com,Jing.Yang@jda.com,Anu.Panyam@jda.com,Hanzhou.Tang@jda.com,Hide.Yamauchi@jda.com,eric.schwarz@jda.com,Abhinay.Madasu@jda.com,Aditi.Jain1@jda.com,Aniket.Patole@jda.com,Arun.Venkataramanan@jda.com,BishmaReddy.Mantralu@jda.com,Deepak.Kashtikar@jda.com,Grishma.Marathi@jda.com,Kaushik.SV@jda.com,Kavitha.Puranik@jda.com,Kirti.Jalan@jda.com,Kishore.Hanumansetty@jda.com,Mahendra.Reddy@jda.com,Naval.Tharun@jda.com,Raj.Tiwary@jda.com,Rakesh.Balguri@jda.com,Ritu.Bhandari@jda.com,Sahil.Sheikh@jda.com,Santosh.Bhima@jda.com,Santosh.Gayam@jda.com,Shreshthha.Jain@jda.com,Venkatababu.Chekoddi@jda.com"
                    } else {
                        env.RECIPIENTS = env.GIT_COMMIT_EMAIL
                    }
                }
                sh 'env'
            }
        }
        stage('Environment') {
            options {
                skipDefaultCheckout()
            }
            steps {
                script {
                    // If the job is not triggered by the nightly regression tests, replace testSuite with the testSuite
                    // parameter passed from the jenkins job.
                    if (currentBuild.getBuildCauses("hudson.triggers.TimerTrigger\$TimerTriggerCause").size() == 0) {
                        testSuite = params.testSuite
                    }
                }
                sh 'echo version: $version'
                sh 'echo "artifactoryContextUrl=https://jdasoftware.jfrog.io/jdasoftware" > gradle.properties'
                sh 'echo "artifactoryUsername=$ARTIFACTORY_USER" >> gradle.properties'
                sh 'echo "artifactoryPassword=$ARTIFACTORY_PASSWORD" >> gradle.properties'
                sh 'cat gradle.properties'

                //set subscription based on DEPLOY_STACK and login to Azure
                setBuildTask()
                setSubscription()
                setCredentials()
                setGatewayandWebURL()
                checkPods()
            }
        }
        stage('Drop schema and create data model') {
            options {
                skipDefaultCheckout()
            }
            when {
                allOf {
                    expression { params.skipClean == false }
                    expression {
                        BRANCH_NAME ==~ /(master-ci|master|release.*)/
                    }
                }
            }
            steps {
                build job: "LUMINATE-PLATFORM/luminate-schema/master-ci",
                        parameters: [
                                string(name: 'resourceGroup', value: "${env.RESOURCE_GROUP}")
                        ],
                        wait: true

                build job: "data-model/${env.JOB_BASE_NAME}",
                        parameters: [
                                string(name: 'resourceGroup', value: "${env.RESOURCE_GROUP}")
                        ],
                        wait: true
            }
        }
        // Not deleting Ingestion API and Deletion Stages yet in case we decide to use them in some way
//        stage('Run End-to-End Ingestion API Tests') {
//            options {
//                skipDefaultCheckout()
//            }
//            when {
//                allOf {
//                    expression { params.skipTest == false }
//                    expression {
//                        BRANCH_NAME ==~ /(master-ci|master|release.*)/
//                    }
//                }
//            }
//            steps {
//                script {
//                    try {
//                        sh "./gradlew clean ingestionApiTests --max-workers 10 -Pversion=${env.BUILD_VERSION} " +
//                                "-PdepVersion=${env.DEP_VERSION_LOOKUP} -PplatformVersion=${env.PLATFORM_VERSION_LOOKUP} -Dserver.protocol=${env.GATEWAY_PROTOCOL} " +
//                                "-Dserver.url=${env.GATEWAY} -Dclient.url=$webURL "
//                    } catch (e) {
//                        echo " Run End-to-End Ingestion API Tests failed!"
//                        currentBuild.result = 'FAILURE'
//                    }
//                }
//            }
//            post {
//                always {
//                    script {
//                        sh 'chown -R 995.992 build .gradle'
//                        junit "build/test-results/ingestionApiTests/*.xml"
//                        stash includes: "build/test-results/ingestionApiTests/TEST-*.xml", name: "ingestionApiResults"
//                        publishHTML target: [
//                                allowMissing         : false,
//                                alwaysLinkToLastBuild: false,
//                                keepAll              : true,
//                                reportDir            : 'build/reports/tests/ingestionApiTests',
//                                reportFiles          : 'index.html',
//                                reportName           : "Ingestion API Test Report"
//                        ]
//                    }
//                }
//            }
//        }
//        stage('Delete All Data') {
//            options {
//                skipDefaultCheckout()
//            }
//            when {
//                allOf {
//                    expression { params.skipClean == false }
//                    expression {
//                        BRANCH_NAME ==~ /(master-ci|master|release.*)/
//                    }
//                }
//            }
//            steps {
//                script {
//                    sh "./gradlew clean test -Dtest.single=CleanupSpec -Pversion=${env.BUILD_VERSION} " +
//                            "-PdepVersion=${env.DEP_VERSION_LOOKUP} -PplatformVersion=${env.PLATFORM_VERSION_LOOKUP} -Dserver.protocol=${env.GATEWAY_PROTOCOL} " +
//                            "-Dserver.url=${env.GATEWAY}"
//                }
//            }
//            post {
//                always {
//                    script {
//                        sh 'chown -R 995.992 build .gradle'
//                        publishHTML target: [
//                                allowMissing         : false,
//                                alwaysLinkToLastBuild: false,
//                                keepAll              : true,
//                                reportDir            : 'build/reports/tests/test',
//                                reportFiles          : 'index.html',
//                                reportName           : "Delete All Data"
//                        ]
//                    }
//                }
//            }
//        }
        stage('Load Showcase Model and Data') {
            options {
                skipDefaultCheckout()
            }
            when {
                expression {
                    BRANCH_NAME ==~ /(master-ci|master|release.*)/
                }
            }
            //            steps {
            //                script {
            //                    build job: "Deployment Projects/Load Showcase Data",
            //                    parameters: [
            //                            string(name: 'stackName', value: "${env.STACK_NAME}"),
            //                            booleanParam(name: 'needValidation', value: true)
            //                    ],
            //                    wait: true
            //                }
            //            }
            steps {
                script {
                    sh "./gradlew clean unzip -Pversion=${env.BUILD_VERSION} -PdepVersion=${env.DEP_VERSION_LOOKUP} " +
                            "-PplatformVersion=${env.PLATFORM_VERSION_LOOKUP} --refresh-dependencies"
                    sh "./gradlew test --tests 'com.jda.dct.e2e.api.loadData.*' -Pversion=${env.BUILD_VERSION} " +
                            "-PdepVersion=${env.DEP_VERSION_LOOKUP} -PplatformVersion=${env.PLATFORM_VERSION_LOOKUP} " +
                            "-Dserver.protocol=${env.GATEWAY_PROTOCOL} -Dserver.url=${env.GATEWAY} -Dclient.url=$webURL"
                }
            }
            post {
                always {
                    script {
                        sh 'chown -R 995.992 build .gradle'
                        publishHTML target: [
                                allowMissing         : false,
                                alwaysLinkToLastBuild: false,
                                keepAll              : true,
                                reportDir            : 'build/reports/tests/test',
                                reportFiles          : 'index.html',
                                reportName           : "Load Showcase to Tenants"
                        ]
                    }
                }
            }
        }
        stage('Run End-to-End Functional Tests') {
            options {
                skipDefaultCheckout()
            }
            when {
                allOf {
                    expression { params.skipTest == false }
                    expression {
                        BRANCH_NAME ==~ /(master-ci|master|release.*)/
                    }
                }
            }
            steps {
                script {
                    try {
                        sh "./gradlew spockTests --max-workers 10 -Pversion=${env.BUILD_VERSION} " +
                                "-PdepVersion=${env.DEP_VERSION_LOOKUP} -PplatformVersion=${env.PLATFORM_VERSION_LOOKUP} " +
                                "-Dserver.protocol=${env.GATEWAY_PROTOCOL} -Dserver.url=${env.GATEWAY} " +
                                "-Dclient.url=$webURL \"-DtestSuite=${testSuite}\""
                    } catch (e) {
                        echo "Run End-to-End API Tests failed!"
                    }
                    if (fileExists("build/test-results/spockTests")) {
                        junit "build/test-results/spockTests/*.xml"
                    }

                    try {
                        // Start Docker container for Selenium remote driver
                        sh 'curl -L --fail https://github.com/docker/compose/releases/download/1.21.2/run.sh -o docker-compose'
                        sh 'chmod +x ./docker-compose'
                        sh './docker-compose up -d'

                        sh "./gradlew testNGTests -Pversion=${env.BUILD_VERSION} " +
                                "-PdepVersion=${env.DEP_VERSION_LOOKUP} -PplatformVersion=${env.PLATFORM_VERSION_LOOKUP}" +
                                "-Dserver.protocol=${env.GATEWAY_PROTOCOL} -Dserver.url=${env.GATEWAY} " +
                                "-Dclient.url=$webURL -Ddriver=remote-chrome " +
                                "-Dclient.user=$UI_CREDENTIALS_USR -Dclient.password=$UI_CREDENTIALS_PSW " +
                                "\"-DtestSuite=${testSuite}\""
                    } catch (e) {
                        echo "Run End-to-End UI Tests failed!"
                    }
                    if (fileExists("build/test-results/testNGTests")) {
                        junit "build/test-results/testNGTests/*.xml"
                    }
                }
            }
            post {
                always {
                    script {
                        sh './docker-compose down -v'
                        sh 'chown -R 995.992 build .gradle'
                    }
                }
            }
        }
        stage("Run ReadyAPI Tests") {
            agent {
                node {
                    label "readyapi"
                }
            }
            options {
                skipDefaultCheckout()
            }
            when {
                allOf {
                    expression { params.skipTest == false }
                    expression {
                        BRANCH_NAME ==~ /(master-ci|release.*)/
                    }
                }
            }
            steps {
                script {
                    def script = "/opt/SmartBear/ReadyAPI-2.6.0/bin/testrunner.sh -j -g " +
                            "-f${env.WORKSPACE}/build/test-results/readyApiTests -E${env.DEPLOY_STACK} " +
                            "\"-TTestCase $testSuite\" -PjenkinsRun=true " +
                            "src/test/readyapi"
                    def statusCode = sh(script: script, returnStatus: true)
                    echo statusCode.toString()
                    // Only fail build if testrunner returns error code other than 0 (success) and 255 (test case failure)
                    if ([0, 255].contains(statusCode) && fileExists("build/test-results/readyApiTests")) {
                        stash includes: "build/test-results/readyApiTests/TEST-*.xml", name: "readyApiResults"
                        junit "build/test-results/readyApiTests/TEST-*.xml"
                    } else if ([0, 255].contains(statusCode)) {
                        echo "No ReadyAPI tests were run."
                    } else {
                        currentBuild.result = "FAILURE"
                        error("Run ReadyAPI Tests failed!")
                    }
                }
            }
        }
        stage("Create Aggregate Report") {
            options {
                skipDefaultCheckout()
            }
            when {
                allOf {
                    expression { params.skipTest == false }
                    expression {
                        BRANCH_NAME ==~ /(master-ci|master|release.*)/
                    }
                }
            }
            steps {
                dir("build/test-results/readyApiTests") {
                    unstash "readyApiResults"
                }
                sh "./gradlew aggregateReport -Pversion=${env.BUILD_VERSION} -PdepVersion=${env.DEP_VERSION_LOOKUP} " +
                        "-PplatformVersion=${env.PLATFORM_VERSION_LOOKUP}"
                //sh "./gradlew --stop"
                publishHTML target: [
                        allowMissing         : false,
                        alwaysLinkToLastBuild: false,
                        keepAll              : true,
                        reportDir            : 'build/reports/aggregateReport',
                        reportFiles          : 'index.html',
                        reportName           : "Aggregate Report"
                ]
            }
        }
    }
    post {
        success {
            mail to: "${env.RECIPIENTS}",
            subject: "Successul Pipeline: ${currentBuild.fullDisplayName}",
            body: "Build successful! ${env.BUILD_URL}"
        }
        failure {
            mail to: "${env.RECIPIENTS}",
            subject: "Failed Pipeline: ${currentBuild.fullDisplayName}",
            body: "Something is wrong with ${env.BUILD_URL}"
        }
    }
}

def delete(String service) {
    sh "kubectl -n $namespace delete pods -l app=${service}"
    sh 'sleep 10'
}

def isRunning(String service, String[] podsName) {
    for (int i = 0; i < podsName.length; i++){
        if (podsName[i].contains(service)){
            String status = sh(returnStdout: true, script: "kubectl -n $namespace get -o template ${podsName[i]} --template={{.status.phase}}").trim()
            if (status == "Running") {
                return true
            } else {
                return false
            }
        }
    }
    return false
}

def restartAllPods() {
    setSubscription()
    setCredentials()
    sh 'kubectl -n $NAME_SPACE delete pods -l app=ignite'
    script {
        def services = ["computation-backend", "computation-frontend", "node", "transportation", "searchsync", "staticdata", "tracking","order","alert"]
        String[] podsNames = sh(returnStdout: true, script: "kubectl -n $namespace get pods -o=name").toString().trim().split("\\s+")
        int count = 0
        while (!isRunning("ignite", podsNames) && count < 100) {
            sleep(10)
            count++
        }
        if (isRunning("ignite", podsNames)) {
            for (int i = 0; i < services.size(); i++){
                delete(services[i])
            }
        } else {
            error("Ignite is not running after it is deleted")
        }
        String[] updatedPodsName = sh(returnStdout: true, script: "kubectl -n $namespace get pods -o=name").toString().trim().split("\\s+")
        for (int i = 0; i < services.size(); i++) {
            int count1 = 0
            while (!isRunning(services[i], updatedPodsName) && count1 < 100) {
                count1++
                sleep(2)
            }
        }
    }
    sh 'sleep 20'
}

def checkPods() {
    script {
        env.CLUSTER = sh(returnStdout: true, script: "az aks list -o table | grep $namespace | awk '{print \$1}'")
    }
    sh 'az aks get-credentials -g $RESOURCE_GROUP -n $CLUSTER -f aks-config-e2e > /dev/null 2>&1'
    sh 'pwd'
    sh 'kubectl --kubeconfig=aks-config-e2e cluster-info'
    sh 'kubectl --kubeconfig=aks-config-e2e -n $NAME_SPACE get pods'
    script {
        String[] pods = sh(returnStdout: true, script: "kubectl --kubeconfig=aks-config-e2e -n $namespace get pods -o=name").toString().trim().split("\\s+")
        for (int i = 0; i < pods.length; i++){
            String status = sh(returnStdout: true, script: "kubectl --kubeconfig=aks-config-e2e -n $namespace get -o template ${pods[i]} --template={{.status.phase}}").trim()
            if (status != "Running") {
                echo "${pods[i]} is not running! Please check and restart failed ones!"
                currentBuild.result = 'FAILURE'
            } else {
                echo "${pods[i]} is running properly!"
            }
        }
    }
}
