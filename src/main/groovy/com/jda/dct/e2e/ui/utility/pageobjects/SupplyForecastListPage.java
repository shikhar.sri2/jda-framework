package com.jda.dct.e2e.ui.utility.pageobjects;


import com.jda.dct.e2e.ui.utility.action.CommonActions;
import com.jda.dct.e2e.ui.utility.common.BasePage;
import com.jda.dct.e2e.ui.utility.helper.Wait.WaitHelper;
import com.jda.dct.e2e.ui.utility.helper.genericHelper.GenericHelper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class SupplyForecastListPage extends BasePage {
    WebDriver driver;
    WebDriverWait webDriverWait;
    WaitHelper objWaitHelper;
    GenericHelper objGenericHelper;
    CommonActions commonActions;

    public SupplyForecastListPage(WebDriver webdriver) {

        this.driver = webdriver;
        PageFactory.initElements(driver, this);
        webDriverWait = new WebDriverWait(driver, 50);
        objGenericHelper = new GenericHelper(driver);
        objWaitHelper = new WaitHelper(driver);
        commonActions = new CommonActions(driver);
    }


    @FindBy(xpath = "//*[@id='PanelSummary-RenderedItem-Item-1-LinkItem']")
    //@FindBy(id = "PanelSummary-RenderedItem-Item")
    private WebElement supplyForecastListPage;

    @FindBy(xpath = "//*[contains(@id, 'forecast-commits-details')]")
    private WebElement supplyForecastDetailsPage;

    @FindBy(xpath="//*[@id='PanelSummary-RenderedItem-Item-1-LinkItem']")
    private WebElement forecastItemNumber;
    @FindBy(xpath ="//a[@href='/supply/forecast-commits']")
    private WebElement gobackFromDetailPage;

    // @FindBy(xpath = "//span[contains(text(),'Item')]")
    @FindBy(xpath = "//*[text()=\"Item\"]")
    private WebElement item;
    @FindBy(xpath = "//*[text()=\"Supplier\"]")
    //@FindBy(xpath = "//span[contains(text(),'Supplier')]")
    private WebElement supplier;
    @FindBy(xpath = "//*[text()=\"Ship To\"]")
    // @FindBy(xpath = "//span[contains(text(),'Ship To')]")
    private WebElement shipTo;
    // @FindBy(xpath = "//span[contains(text(),'Ship From')]")
    @FindBy(xpath = "//*[text()=\"Ship From\"]")
    private WebElement shipFrom;
    //@FindBy(xpath = "(//p[contains(text(),'Notes')])]")
    @FindBy(xpath = "//*[text()=\"Notes\"]")
    private WebElement notes;

    // @FindBy(xpath = "(//span[contains(text(),'Material Group')])[1]")
    @FindBy(xpath = "//*[text()=\"Material Group\"]")
    private WebElement materialGroup;

    @FindBy(xpath = "//*[contains(@id,'supply-procurement-Filter-List-Card')]//span")
    private List<WebElement> forecastExceptionStatusCount;

    //========Methods=====

    public void clickForecastItemNumber() {
        objWaitHelper.waitAndClickElement(forecastItemNumber, "Item No.");
    }


    //Assertion Methods
    public boolean isitemAtHeaderLevelDisplayed() {
        objWaitHelper.waitElementToBeVisible(item);
        return objGenericHelper.isDisplayed(item, "Item");
    }

    public boolean isSupplierAtHeaderLevelDisplayed() {
        objWaitHelper.waitElementToBeVisible(supplier);
        return objGenericHelper.isDisplayed(supplier, "Supplier");
    }

    public boolean isShipToAtHeaderLevelDisplayed() {
        objWaitHelper.waitElementToBeVisible(shipTo);
        return objGenericHelper.isDisplayed(shipTo, "Ship To");
    }

    public boolean isShipFromAtHeaderLevelDisplayed() {
        objWaitHelper.waitElementToBeVisible(shipFrom);
        return objGenericHelper.isDisplayed(shipFrom, "Ship From");
    }

    public boolean isNotesAtHeaderLevelDisplayed() {
        objWaitHelper.waitElementToBeVisible(notes);
        return objGenericHelper.isDisplayed(notes, "Notes");
    }

    public boolean isMaterialGroupAtHeaderLevelDisplayed() {
        objWaitHelper.waitElementToBeVisible(materialGroup);
        return objGenericHelper.isDisplayed(materialGroup, "Material Group");
    }


    public boolean isSupplyForecastListPageDisplayed() {
        objWaitHelper.waitElementToBeVisible(supplyForecastListPage);
        return objGenericHelper.isDisplayed(supplyForecastListPage, "Forecast list page");
    }

    public boolean isSupplyForecastDetailsPageDisplayed() {
        objWaitHelper.waitElementToBeVisible(supplyForecastDetailsPage);
        return objGenericHelper.isDisplayed(supplyForecastDetailsPage, "Forecast details page");
    }

    public void clickForecastExceptionsHavingData() {
        commonActions.clickExceptionStatusHavingData(forecastExceptionStatusCount);
        Reporter("Exception status having data is clicked successfully", "Pass", log);
    }

}





