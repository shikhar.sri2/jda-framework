package com.jda.dct.e2e.ui.utility.pageobjects;

import com.jda.dct.e2e.ui.utility.common.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class LoginPage extends BasePage {

    private WebDriver driver;
    private WebDriverWait wait;

    @FindBy(className="login-button-wrapper")
    private WebElement loginButton;
    @FindBy(id ="i0116")
    private WebElement userField;
    @FindBy(id ="displayName")
    private WebElement displayName;
    @FindBy(id ="i0118")
    private WebElement passwordField;
    @FindBy(id = "idSIButton9")
    private WebElement contextSensitiveSignInButton;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, getTimeOutInSeconds());
        PageFactory.initElements(driver, this);
    }

    public void clickSignin() {
        loginButton.click();
    }

    public void enterEmailAddress(String emailAddress) {
        userField.sendKeys(emailAddress);
    }

    public void clickNext() {
        contextSensitiveSignInButton.click();
    }

    public void enterPassword(String password) {
        passwordField.sendKeys(password);
    }

    public void clickLogin() {
        contextSensitiveSignInButton.click();
    }

    public void clickYes() {
        contextSensitiveSignInButton.click();
    }

    public void login(String username, String password) {
        wait.until(ExpectedConditions.elementToBeClickable(loginButton));
        loginButton.click();
        // Wait for user field to be actionable, enter username, and click "Next"
        wait.until(ExpectedConditions.elementToBeClickable(userField));
        userField.sendKeys(username);
        wait.until(ExpectedConditions.elementToBeClickable(contextSensitiveSignInButton));
        contextSensitiveSignInButton.click();
        // Wait for the password frame to load
        wait.until(ExpectedConditions.visibilityOf(displayName));
        // Wait for password field to be actionable, enter username, and click "Sign in"
        wait.until(ExpectedConditions.elementToBeClickable(passwordField));
        passwordField.sendKeys(password);
        wait.until(ExpectedConditions.elementToBeClickable(contextSensitiveSignInButton));
        contextSensitiveSignInButton.click();
        // Wait for confirm to stay signed in prompt and click "Yes" (choice doesn't matter this state isn't saved between runs)
        wait.until(ExpectedConditions.elementToBeClickable(contextSensitiveSignInButton));
        contextSensitiveSignInButton.click();

        //return new LeftMenu(driver);
    }
}
