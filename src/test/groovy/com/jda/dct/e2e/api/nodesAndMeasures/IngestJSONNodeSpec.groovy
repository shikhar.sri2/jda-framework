package com.jda.dct.e2e.api.nodesAndMeasures

import com.jda.dct.e2e.api.categories.Regression
import com.jda.dct.e2e.api.utility.IngestionHelper
import groovy.json.JsonSlurper
import org.junit.experimental.categories.Category
import org.springframework.core.io.FileSystemResource
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.util.LinkedMultiValueMap
import org.springframework.web.client.RestTemplate
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll
import spock.util.concurrent.PollingConditions

/**
 * Ingest node measure file in JSON format.
 *
 */

// is JSON ingestion testing necessary
@Category(Regression)
class IngestJSONNodeSpec extends Specification implements IngestionHelper {

    @Shared
            httpHeaders
    @Shared
            nodeURL
    @Shared
            measureURL

    @Shared
            sampleNodeForAlert,
            sampleItemForAlert,
            sampleSiteForAlert

    @Shared
            getSitesUrl

    def setupSpec() {
        init()

        def sitePath = properties.getProperty('ingestSitesPath')
        def nodeIngestPath = properties.getProperty('ingestNodesPath')
        def getPath = properties.getProperty('getNodePath')
        def itemPath = properties.getProperty('ingestItemsPath')

        itemIngestionUrl = protocol + ip + itemPath
        siteIngestionUrl = protocol + ip + sitePath
        nodeIngestionUrl = protocol + ip + nodeIngestPath
        sampleSiteWithDesc = this.getClass().getClassLoader().getResource('sample-site-with-desc.json').text

        sampleNodeNoDesc = this.getClass().getClassLoader().getResource('sample-node-no-desc.json').text

        getURLPrefix = protocol + ip + getPath

        httpHeaders = new HttpHeaders()
        httpHeaders.setContentType(MediaType.MULTIPART_FORM_DATA)
        httpHeaders.add('Authorization', header.get('Authorization'))

        def nodePath = properties.getProperty('nodePostPath')
        def measurePath = properties.getProperty('measurePath')
        nodeURL = protocol + ip + nodePath
        measureURL = protocol + ip + measurePath

        def searchAlertPath = properties.get("searchAlertPath")
        searchAlertUrlPrefix = protocol + ip + searchAlertPath

        sampleNodeForAlert = this.getClass().getClassLoader().getResource('sample-node-stockout-exception.json').text
        sampleItemForAlert = this.getClass().getClassLoader().getResource('sample-item-for-alert-testing-1.json').text
        sampleSiteForAlert = this.getClass().getClassLoader().getResource('sample-site-for-alert-testing-1.json').text
    }

//    @Unroll
//    def "ingest a small good json file: #file"() {
//        given:
//        def filePath = this.getClass().getClassLoader().getResource(file).path
//
//        when:
//        def response = postFile(filePath, postUrl)
//        def ingestionId = getIngestionId(response)
//        def status = waitUntilComplete(ingestionId)
//
//        then:
//        with(status) {
//            contains('COMPLETED')
//            !contains('COMPLETED_WITH_ERROR')
//        }
//
//        where:
//        file                    | postUrl
//        'sample-node-1.json'    | nodeURL
//        'sample-measure-1.json' | measureURL
//    }

    // --------------------
    //    Helper Methods
    // --------------------

    def postFile(def path, def url) {
        def map = new LinkedMultiValueMap<>()
        map.add('file', new FileSystemResource(path))
        def httpEntity = new HttpEntity<>(map, httpHeaders)

        def restTemplate = new RestTemplate()
        def response = restTemplate.exchange(url, HttpMethod.POST, httpEntity, String)

        response
    }


    def getIngestionId(ResponseEntity<Object> response) {
        getJson(response.body).ingestionId
    }

//    def "Node with no site description gets filled in based on site names"() {
//        given: 'A site with a description is ingested and can be retrieved from search'
//
//        def nodeId = getNodeIds(sampleNodeNoDesc)[0]
//        deleteObject(getURLPrefix, nodeId)
//
//        ingestData(sampleSiteWithDesc, siteIngestionUrl)
//
//        when: "A node is ingested that does not contain a description"
//
//        ingestData(sampleNodeNoDesc, nodeIngestionUrl)
//
//        def url = getURLPrefix + nodeId
//        def nodeText = getHttpClient(header, url, null, HttpMethod.GET).body
//        def nodeRetrieved = new JsonSlurper().parseText(nodeText).get('data').get(0)
//
//        def mapper = new ObjectMapper()
//        def received = mapper.readValue(groovy.json.JsonOutput.toJson(nodeRetrieved), Node.class)
//
//        then:
//        received.getValue("shipToSiteDescription") == "Description to be copied"
//        received.getValue("shipFromSiteDescription") == "Description to be copied"
//    }
//
//    def "Ingest a stockout node which triggers alert service to generate alerts"() {
//        given: 'Node with Stockout data'
//
//        ingestData(sampleSiteForAlert, siteIngestionUrl)
//        ingestData(sampleItemForAlert, siteIngestionUrl)
//        sleep(60000)
//        when: 'Ingesting data'
//        def nodeId = getNodeIds(sampleNode)[0]
//        deleteObject(getURLPrefix, nodeId)
//
//        ingestData(sampleNode)
//
//        final Map<String, String> searchCriteria = new HashMap<>()
//        searchCriteria.put("limit", "1")
//        searchCriteria.put("query", "objectId=" + nodeId)
//
//        def responseDef = searchResponseJson(searchCriteria, searchAlertUrlPrefix, state, 10)
//
//        then:
//        assert responseDef != null
//        def alertDef = responseDef.get("data").getAt(0)
//        assert alertDef.get("objectId") != null
//        assert alertDef.get("objectType") == 'Node'
//        assert alertDef.get("state") == state
//        assert alertDef.get("severity") == 4
//        assert alertDef.get("type") == 'projectedInventory'
//        assert alertDef.get("title") == 'Stockout at IT06'
//        assert alertDef.get("firstOccurrenceDate") != null
//        assert alertDef.get("objectNumber") == "950010868"
//        if (alertDef.get("state") == 'Open') {
//            assert alertDef.get("closedDate") == null
//        }
//        assert alertDef.get("alertStatusLmd") != null
//
//        def references = alertDef.get("refObjects")
//        assert references != null
//
//        assert references.get("shipToSites") != null
//        def shipToSite = references.get("shipToSites").get(0)
//        assert shipToSite.get("participantName").equals("Electrolux AB")
//        assert shipToSite.get("siteName").equals("IT06")
//        assert shipToSite.get("siteOwnerName").equals("Electrolux AB IT06 Owner")
//
//        assert references.get("shipFromSites") != null
//        def shipFromSite = references.get("shipFromSites").get(0)
//        assert shipFromSite.get("participantName").equals("Samsung")
//        assert shipFromSite.get("siteName").equals("IT07")
//        assert shipFromSite.get("siteOwnerName").equals("Samsung IT07 Owner")
//
//        assert references.get("customerItems") != null
//        def customerItem = references.get("customerItems").get(0)
//        assert customerItem.get("itemName").equals("950010868")
//        assert customerItem.get("itemOwnerName").equals("Electrolux AB")
//
//        assert references.get("supplierItems") != null
//        def supplierItem = references.get("supplierItems").get(0)
//        supplierItem.get("itemName").equals("950010899")
//        supplierItem.get("itemOwnerName").equals("Samsung")
//
//        where:
//
//        sampleNode                                                                                    | state
//        sampleNodeForAlert.replaceAll("\"time\": 1535112000000", "\"time\": " + new Date().getTime()) | 'Open'
//        sampleNodeForAlert.replaceAll("\"status\": \"Active\"", "\"status\": \"Inactive\"")           | 'Closed'
//        sampleNodeForAlert.replaceAll("\"status\": \"Inactive\"", "\"status\": \"Active\"")           | 'Open'
//    }

    def "Add a Note to a node and get it back"() {
        given:
        def conditions = new PollingConditions(timeout: 120, initialDelay: 1)
        def noteNode = this.getClass().getClassLoader().getResource('sample-node-1.json').path

        when: 'A Note is added to the node'

        def response = postFile(noteNode, nodeURL)
        def ingestionId = getIngestionId(response)
        def status = waitUntilComplete(ingestionId)

        then:
        with(status) {
            contains('COMPLETED')
            !contains('COMPLETED_WITH_ERROR')
        }

        def nodeIds = getNodeIds(this.getClass().getClassLoader().getResource('sample-node-1.json').text)
        def url = getURLPrefix + 'notes/' + nodeIds[0]
        def sampleNote = '{ "note": "node Note added"}'
        def response1 = getHttpClient(header, url, sampleNote, HttpMethod.POST)
        assert response1.status == 200

        and: "Get the Node"

        def url1 = getURLPrefix + nodeIds[0]
        def nodeText = getHttpClient(header, url1, null, HttpMethod.GET).body
        def nodeRetrieved = new JsonSlurper().parseText(nodeText).get('data').get(0)

        then: 'Get the node  back and verify the note'
        assert nodeRetrieved.getAt('notes').toString().contains("node Note added") == true
    }
}
