package com.jda.dct.e2e.clientUi.pages.SupplyModule

import geb.Page


class SupplyInventorylistPage extends Page{


    static url = "/tempraturecontrolled/inventory"

    static atCheckWaiting = true

    static at = {

        $("div", role:"button", 'aria-expanded':"false").first().isDisplayed()


    }

    static content = {

        InventoryitemNo(wait: true) {

            $("div", role: "button", 'aria-expanded': "false").first().find("a", "href": contains("tempraturecontrolled/inventory"))
        }

        inventoryorders(wait: true) {

            $("#NodeDetail-Typography-item-name")
        }
    }

    def gotoSupplyInventory() {

        waitFor {
            InventoryitemNo.displayed
         }
        InventoryitemNo.click()
        inventoryorders.displayed
        println("welcome to inventory list page")
    }

}
























