package com.jda.dct.e2e.api.utility

import com.jda.dct.domain.util.NumberUtil
import com.jda.luminate.common.base.TypeCountObject
import org.apache.commons.lang.StringUtils
import org.springframework.http.HttpMethod
import spock.util.concurrent.PollingConditions

class QueryResultHandler implements IngestionHelper {
    static String URLPrefix
    static String baseUiUrl
    static handler

    def setup() {
        init()
        URLPrefix = protocol + ip + "/api/v1/"
        baseUiUrl = protocol + webUrl + "/api/v1/"
    }

    boolean executeQueryObjects(QueryObjects queryObjects) {
        if (queryObjects == null || queryObjects.getQueryObjects() == null ||
                queryObjects.getQueryObjects().isEmpty()) {
            return true
        }
        for (QueryObject queryObject : queryObjects.getQueryObjects()) {
//            if (!refreshIndex(queryObject)) {
//                return false
//            }
            if (!executeAndCheck(queryObject)) {
                return false
            }
        }
        return true
    }

    boolean executeAndCheck(QueryObject queryObject) {
        def res = false
        def url = queryObject.getURL(URLPrefix)
        println "Check url: " + url
        def conditions = createCondition(queryObject)
        conditions.eventually {
            def response = execute(queryObject)
            if (response != null && queryObject.getCount() != null && !queryObject.getCount().isEmpty()) {
                res = checkCount(response.body, queryObject.getCount())
                assert res
            }
        }
        return res
    }

    boolean refreshIndex(QueryObject queryObject) {
        def res = false
        def conditions = new PollingConditions(timeout: 30, initialDelay: 2)
        conditions.setDelay(4)
        conditions.eventually {
            res = executeRefresh(queryObject)
            assert res
        }
        return res
    }

    private boolean executeRefresh(QueryObject queryObject) {
        final Map<String, String> mapping = new HashMap<>()
        mapping.put('nodes','node')
        mapping.put('deliveries','delivery')
        mapping.put('shipments','shipment')
        mapping.put('sites','site')
        mapping.put('purchaseOrders','purchaseorder')
        mapping.put('salesOrders','salesorder')
        mapping.put('Alert','alert')
        mapping.put('items','item')
        mapping.put('sourcingLanes','sourcinglane')
        mapping.put('participants','participant')
        mapping.put('batches','batch')
        mapping.put('calendars','calendar')

        def url = null
        def response = null
        if (queryObject != null && StringUtils.isNotBlank(queryObject.getType()) &&
                mapping.containsKey(queryObject.getType())) {
            url = URLPrefix + "elasticsearch/a4b7a825f67930965747445709011120-" + mapping.get(queryObject.getType()) + "/_refresh"
            println("Execute refreshing index: " + url)
            response = sendHttpRequest(header, url, null, HttpMethod.GET)
        }
        int countOfSuccessful = (url == null || response == null ) ? 1 : response.body["_shards"]["successful"]
        print("The number of shard copies the index operation succeeded on: " + countOfSuccessful)
        return countOfSuccessful > 0
    }

    private createCondition(QueryObject queryObject) {
        def timeout = Math.max(800, queryObject.getTimeout())
        def initialDelay = Math.max(2, queryObject.getInitialDelay())
        def delay = Math.max(20, queryObject.getDelay())
        def conditions = new PollingConditions(timeout: timeout, initialDelay: initialDelay)
        conditions.setDelay(delay)
        return conditions
    }

    def execute(QueryObject queryObject) {
        if (queryObject == null) {
            return null
        }
        def url = queryObject.getURL(URLPrefix)
        return sendHttpRequest(header, url, null, HttpMethod.GET)
    }

    def executeWithRetry(QueryObject queryObject, double timeout , double initialDelay, double delay, int expectedTotalCount, int expectedCount) {
        def response = null
        if (queryObject == null) {
            return null
        }
        def url = queryObject.getURL(URLPrefix)
        println "Check url: " + url
        def conditions = new PollingConditions(timeout: timeout, initialDelay: initialDelay)
        conditions.setDelay(delay)
        conditions.eventually {
            response = sendHttpRequest(header, url, null, HttpMethod.GET)
            assert response != null && response.body["totalCount"] >= expectedTotalCount && response.body["count"] >= expectedCount
        }
        response
    }

    boolean checkCount(response, Map<String, Long> count) {
        if (count == null || count.isEmpty()) {
            return true
        }
        if (response == null) {
            return false
        }
        List<TypeCountObject> typeCounts = response["typeCounts"]
        if (typeCounts == null || typeCounts.isEmpty()) {
            println("typeCount object is blank, checking again")
            return false
        }
        int match = count.size()
        println(match + " type(s) below should be matched")
        for (String key : count.keySet()) {
            if (StringUtils.isBlank(key)) {
                continue
            }
            for (TypeCountObject tc : typeCounts) {
                if (tc == null) {
                    continue
                }
                if (key.equals(tc.getType())) {
                    if (NumberUtil.compareTo(count.get(key), tc.getCount()) <= 0) {
                        match--
                    }
                    println "Type: " + key + ", Expected Count: " + count.get(key) + ", Actual Count: " + tc.getCount()
                }
            }
        }
        return match == 0
    }

    static QueryResultHandler getQueryResultHandler() {
        if (handler == null) {
            handler = new QueryResultHandler()
            handler.setup()
            return handler
        }
        return handler
    }
}
