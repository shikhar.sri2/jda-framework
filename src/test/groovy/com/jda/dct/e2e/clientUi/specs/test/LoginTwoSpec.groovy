/*
package com.jda.dct.e2e.clientUi.specs.test

import com.jda.dct.e2e.clientUi.pages.Start.HomeMenuPage
import com.jda.dct.e2e.clientUi.pages.Start.HomePage
import com.jda.dct.e2e.clientUi.pages.Start.InitialLoginPage
import com.jda.dct.e2e.clientUi.pages.Start.SecondPage
import geb.driver.CachingDriverFactory

import geb.spock.GebSpec
import spock.lang.Stepwise

@Stepwise
class LoginTwoSpec extends GebSpec {

    def setupSpec() {
        CachingDriverFactory.clearCache()
    }


    def "User should get signin page before they are directed to Luminate tower loginPage"() {

        given: " user have the right url"
        to InitialLoginPage

        when: "User should see the  Microsoft online login page"
        at(InitialLoginPage).signin()

        then: "user should be directed to directed to Luminate tower loginPage"
        waitFor {
            driver.getTitle() == "Sign in to your account"

        }
    }

    def "Users can login to DCT website"() {

        when: "User is navigated to Microsoft online login page, login with given credential"
        at(SecondPage).login()

        then: "User is navigated to DCT watch tower page"
        waitFor {
            driver.getTitle() == "Luminate Control Tower - Luminate Control Tower"

        }

    }


    def " User can click on  Slide navigation bar available on the homepage" () {
        given: "User are at Dashboard"
        to HomePage

        when: "User  can click next on the  navigation button on Dashboard"
        at(HomePage).useNavigatorbuttons()

        then: "User  is navigated to left navigation bar on the Dashboard"
        at HomeMenuPage


    }



    def "Navigate from HomeMenuPage to Supply" () {
        given: "User is at HomePage"
        to HomeMenuPage

        when: "User clicks Supply button from menu button"
        at(HomeMenuPage).moduleSupply()

        then: "user is at tempraturecontrolled/shipment Module"
        println("Welcome to Supply Module")

    }



    def cleanupSpec() {
        CachingDriverFactory.clearCacheAndQuitDriver()
    }

}







*/
