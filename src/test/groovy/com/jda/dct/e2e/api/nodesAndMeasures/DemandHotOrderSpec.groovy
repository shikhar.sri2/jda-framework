package com.jda.dct.e2e.api.nodesAndMeasures
/*
package com.jda.dct.e2e.api.api-nodesAndMeasures

import IngestionHelper
import QueryObject
import QueryResultHandler
import spock.lang.Specification
import spock.lang.Unroll

class DemandHotOrderSpec extends Specification implements IngestionHelper {

    @Unroll
    def "verify item  counts in a shipment"(String shipmentId, String itemType, String label) {
        given:
        "shipment ID : #shipmentId"

        when:
        "search #label counts"
        def queryObject = new QueryObject()
        queryObject.setType("salesOrders")
        queryObject.setShipmentSalesOrderCounts(true)

        queryObject.addQuery("shipmentId", shipmentId)
        queryObject.addQuery("shipmentType", "outboundShipment")
        queryObject.addQuery("criticalities", itemType)


        def response = QueryResultHandler.getQueryResultHandler().execute(queryObject)

        def countSize = response.body.typeCounts[0].typeCounts.size()

        def totalItems = 0
        (0..countSize - 1).each({ v ->

            totalItems = totalItems + response.body.typeCounts[0].typeCounts[v].count
        })

        println totalItems

        then: "Header and Line counts should be equal"

        assert response.body.typeCounts[0].count == totalItems

        where:
        shipmentId     | itemType   | label
        "OS0038_SD_S1" | "HotOrder" | "Hot_Order"
        "OS0038_SD_S1" | ""         | "All_Order"
        "OS0017_SO_S1" | "HotOrder" | "Hot_Order"
        "OS0017_SO_S1" | ""         | "All_Order"
    }
}
*/
