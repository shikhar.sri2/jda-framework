package com.jda.dct.e2e.api.purchaseOrders

import com.jda.dct.e2e.api.categories.Smoke
import com.jda.dct.e2e.api.utility.IngestionHelper
import com.jda.dct.e2e.api.utility.QueryObject
import com.jda.dct.e2e.api.utility.QueryResultHandler
import org.junit.experimental.categories.Category
import spock.lang.Specification

class RetrieveAPurchaseOrderByIdQuerySpec extends Specification implements IngestionHelper {

    def setupSpec() {
        init()
    }

    @Category(Smoke)
    def "retrieve a purchase order by query"() {
        given: "Purchase order PO0024_CO"
        def purchaseOrderId = "PO0024_CO"

        when: "retrieve the order by querying the purchaseOrderId field"
        def queryObject = new QueryObject()
        queryObject.setType("purchaseOrders")
        queryObject.addQuery("purchaseOrderId", purchaseOrderId)
        def response = QueryResultHandler.getQueryResultHandler().executeWithRetry(queryObject, 20, 2, 5, 1, 1)

        then: "the api should return a success status code and the response should contain only PO0024_CO"
        response.rawResponse.status == 200
        response.body.data.size() == 1
        response.body.data[0].purchaseOrderId == purchaseOrderId
    }
}
