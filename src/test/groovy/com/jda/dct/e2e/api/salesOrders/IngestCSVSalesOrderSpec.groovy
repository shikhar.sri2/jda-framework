package com.jda.dct.e2e.api.salesOrders

import com.jda.dct.e2e.api.categories.Regression
import com.jda.dct.e2e.api.utility.IngestionHelper
import org.junit.experimental.categories.Category
import spock.lang.Shared
import spock.lang.Specification

/**
 * Ingest sales order in CSV format.
 *
 * @author Kishore H
 * @version 1.0-SNAPSHOT
 * @since 13-Jul-2018
 */

class IngestCSVSalesOrderSpec extends Specification implements IngestionHelper {

    @Shared salesOrderURL

    void setup() {
	
        init()

        def salesOrderPath = properties.getProperty('ingestSalesOrdersPath')
        salesOrderURL = protocol + ip + salesOrderPath + '?processType=demand&salesOrderType=standardOrder'

        def getPath = properties.getProperty('getSalesOrderPath')
        getURLPrefix = protocol + ip + getPath
    }

    @Category(Regression)
	def "create and ingest a faulty salesOrder csv"() {
        given: "a faulty sample salesOrder, quantity is not a valid number"
/*        def filePath = getTmpDir() + 'sample_so.csv'
        new File(filePath).newWriter().withWriter {
            it << 'Order Type,Process Type,Order No,Customer,Customer Name,Supplier,Supplier Name,Bill To,Sold To,Ship To,Ship To Site,Creation Date,Line No,Item Type,Purchase Order Id,PO Line No,Customer Item,Item,Need By Date,Order Quantity,Schedule No,Confirmed Delivery Date,Scheduled Quantity\n' +
                    'standardOrder,demand,2221237,SOCUS001,Best Buy,WH001,BD Maryland Warehouse,,,,,7/9/2018,2,,PObeforecsv4,12345,P700,CA1,7/21/2018,100,1,7/10/2018,111\n' +
                    'standardOrder,demand,2221238,SOCUS001,Best Buy,WH001,BD Maryland Warehouse,,,,,7-9-2018,1,,PObeforecsv4,12346,P100,CA1,7/21/2018,200,2,7/11/2018,222'
                   
        }

        when: "ingest into DCT"
        def response = postFile(filePath, salesOrderURL, null)
        def ingestionId = getIngestionId(response)
        def status = waitUntilComplete(ingestionId)

        then: "should have status - completed_with_error"
        status.contains('COMPLETED_WITH_ERROR')

 */
//********************************************************************************************************

        SampleSalesOrderShowcaseData sample = new SampleSalesOrderShowcaseData()
        Map<String, String> modifiedFields = new HashMap<>()
        //   modifiedFields.put("salesOrderId", "saleTest01")
        modifiedFields.put("amount", "abc")
        def newSalesOrderId = sample.duplicateSalesOrder("SO0065_OO")
        def newSalesOrder = sample.modifySalesOrderMultiFields(newSalesOrderId, modifiedFields, "temp-sales.json")
        def ingestionId = ingestData(newSalesOrder, "salesOrders?processType=demand&salesOrderType=standardOrder")
        println("ingestionId : " + ingestionId)
        when: "Fetch status after ingesting"
        def status = waitUntilComplete(ingestionId)
        then: "Check status. Should fail due to invalid qty"
        println status
        def testSalesOrder = sample.getSalesOrderFromShowcase(newSalesOrderId)
        assert testSalesOrder['amount']!="abc"

    }

    // regression and redundant
/*
    @Category(Regression)
    def "ingest a large faulty salesOrder CSV file"() {
        given:
        def numSalesOrders = 500
        def filePath = getTmpDir() + 'sample_so.csv'
        new File(filePath).newWriter().withWriter { writer ->
            writer << 'Order Type,Process Type,Order No,Customer Name,Customer Description,Supplier Name,Supplier Description,Bill To,Sold To,Ship To Site,Ship To Site Description,Creation Date,Line No,Item Type,Purchase Order Id,PO Line No,Customer Item Name,Supplier Item Name,Need By Date,Order Quantity,SO Delivery Schedule No,Confirmed Delivery Date,Confirmed Quantity\n'
            def templateLine = 'standardOrder,demand,<salesOrder>,SOCUS001,Best Buy,WH001,BD Maryland Warehouse,,,,,7-9-2018,1,,PObeforecsv4,12346,P100,CA1,	7/21/2018,200,2,7/11/2018,222\n'
            numSalesOrders.times { salesOrder ->
                writer << templateLine.replaceAll('<salesOrder>', ('1231235' + salesOrder))
            }
        }

        when:
        def response = postFile(filePath, salesOrderURL, null)
        def ingestionId = getIngestionId(response)
        def status = waitUntilComplete(ingestionId, "ingesting:$ingestionId", 60)

        then:
        with(status) {
            !contains('COMPLETED')
            contains('COMPLETED_WITH_ERROR')
        }
        getStatusObjects(ingestionId).find { it.get('status') == 'COMPLETED_WITH_ERROR' }
                .get('numProcessed') == 0

    }
*/
//    def "Ingest a sales order which triggers computation on the sales order itself and header status as Late"() {
//
//        given: 'original sales order ingested'
//        def filePath = this.getClass().getClassLoader().getResource('csv/so_data_computation_2.csv').path
//
//        when:
//        def response = postFile(filePath, salesOrderURL, null)
//        print response
//        def ingestionId = getIngestionId(response)
//        def status = waitUntilComplete(ingestionId)
//
//        then:
//        with(status) {
//            contains('COMPLETED')
//            !contains('COMPLETED_WITH_ERROR')
//        }
//
//        SalesOrder salesOrder = new SalesOrder()
//
//        salesOrder.setTid("a4b7a825f67930965747445709011120")
//        salesOrder.setSalesOrderId("S199")
//        salesOrder.setSalesOrderType("standardOrder")
//        salesOrder.setSupplierName("WH001")
//        salesOrder.setCustomerName("SOCUS001")
//
//        def iidFor = IidUtil.getIidFor(salesOrder)
//
//        def url = getURLPrefix + iidFor
//        sleep(60000)
//        def salesOrderText = getHttpClient(header, url, null, HttpMethod.GET).body
//        def salesOrderRetrieved = new JsonSlurper().parseText(salesOrderText)
//
//        then: 'verify that sales order exception status is updated'
//
//        salesOrderRetrieved.getAt('data').getAt('exceptionStatus').toString() == '[Late]'
//        salesOrderRetrieved.getAt('data').getAt('salesOrderLines').getAt('SL911').getAt('exceptionStatus').toString() == '[Late]'
//        salesOrderRetrieved.getAt('data').getAt('salesOrderLines').getAt('SL912').getAt('exceptionStatus').toString() == '[On Time]'
//        salesOrderRetrieved.getAt('data').getAt('salesOrderLines').getAt('SL913').getAt('exceptionStatus').toString() =='[Late]'
//    }

//    def "Ingest a sales order which triggers computation on the sales order itself and header status as On Time"() {
//
//        given: 'original sales order ingested'
//        def filePath = this.getClass().getClassLoader().getResource('csv/so_data_computation_3.csv').path
//
//        when:
//        def response = postFile(filePath, salesOrderURL, null)
//        print response
//        def ingestionId = getIngestionId(response)
//        def status = waitUntilComplete(ingestionId)
//
//        then:
//        with(status) {
//            contains('COMPLETED')
//            !contains('COMPLETED_WITH_ERROR')
//        }
//
//        SalesOrder salesOrder = new SalesOrder()
//
//        salesOrder.setTid("a4b7a825f67930965747445709011120")
//        salesOrder.setSalesOrderId("S200")
//        salesOrder.setSalesOrderType("standardOrder")
//        salesOrder.setSupplierName("WH001")
//        salesOrder.setCustomerName("SOCUS001")
//
//        def iidFor = IidUtil.getIidFor(salesOrder)
//
//        def url = getURLPrefix + iidFor
//        sleep(60000)
//        def salesOrderText = getHttpClient(header, url, null, HttpMethod.GET).body
//        def salesOrderRetrieved = new JsonSlurper().parseText(salesOrderText)
//
//        then: 'verify that sales order exception status is updated'
//
//        salesOrderRetrieved.getAt('data').getAt('exceptionStatus').toString() == '[Late]'
//        salesOrderRetrieved.getAt('data').getAt('salesOrderLines').getAt('SL2001').getAt('exceptionStatus').toString() == '[On Time]'
//        salesOrderRetrieved.getAt('data').getAt('salesOrderLines').getAt('SL2002').getAt('exceptionStatus').toString() == '[On Time]'
//        salesOrderRetrieved.getAt('data').getAt('salesOrderLines').getAt('SL2003').getAt('exceptionStatus').toString() =='[Late]'
//    }
//    def "Ingest a sales order and verify ingested data"() {
//
//        given: 'original sales order ingested'
//        def filePath = this.getClass().getClassLoader().getResource('csv/so_data_verification.csv').path
//
//        when:
//        def response = postFile(filePath, salesOrderURL, null)
//        print response
//        def ingestionId = getIngestionId(response)
//        def status = waitUntilComplete(ingestionId)
//
//        then:
//        with(status) {
//            contains('COMPLETED')
//            !contains('COMPLETED_WITH_ERROR')
//        }
//
//        SalesOrder salesOrder = new SalesOrder()
//
//        salesOrder.setTid("a4b7a825f67930965747445709011120")
//        salesOrder.setSalesOrderId("testSoData")
//        salesOrder.setSalesOrderType("standardOrder")
//        salesOrder.setCustomerName("SOCUS001")
//        salesOrder.setSupplierName("WH001")
//
//        def iidFor = IidUtil.getIidFor(salesOrder)
//
//        def url = getURLPrefix + iidFor
//        sleep(30000)
//        def salesOrderText = getHttpClient(header, url, null, HttpMethod.GET).body
//        def salesOrderRetrieved = new JsonSlurper().parseText(salesOrderText)
//
//        then: 'verify that sales order is ingested properly'
//        assert salesOrderRetrieved != null
//        def shipToAddress = salesOrderRetrieved.getAt("data").getAt("shipToAddress")
//        assert shipToAddress != null
//        assert shipToAddress.getAt("addressLine1").getAt(0).toString().equals("no not my address")
//        assert shipToAddress.getAt("addressLine2").getAt(0).toString().equals("its boring")
//        assert shipToAddress.getAt("city").getAt(0).toString().equals("Yunfu Shi")
//        assert shipToAddress.getAt("state").getAt(0).toString().equals("Ajka1")
//        assert shipToAddress.getAt("postalCode").getAt(0).toString().equals("518000")
//        assert shipToAddress.getAt("country").getAt(0).toString().equals("China")
//        assert shipToAddress.getAt("latitude").getAt(0).toString().equals("0.2")
//        assert shipToAddress.getAt("longitude").getAt(0).toString().equals("0.5")
//    }
}
