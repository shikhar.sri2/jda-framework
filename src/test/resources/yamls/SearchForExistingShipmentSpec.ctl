queryObjects:
  - type: shipments
    processType: supply
    subType: inboundShipment
    fullTextQuery:
    - searchText:
      - "IS0053_SO_S2"
    initialDelay: 1
    delay: 5
    timeout: 60