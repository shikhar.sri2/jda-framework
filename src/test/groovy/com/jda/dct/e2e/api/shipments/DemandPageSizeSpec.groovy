package com.jda.dct.e2e.api.shipments

import com.jda.dct.e2e.api.categories.Regression
import com.jda.dct.e2e.api.utility.IngestionHelper
import com.jda.dct.e2e.api.utility.QueryObject
import com.jda.dct.e2e.api.utility.QueryResultHandler
import org.junit.experimental.categories.Category
import spock.lang.Specification
import spock.lang.Unroll


// regression
class DemandPageSizeSpec extends Specification implements IngestionHelper {

    @Category(Regression)
    @Unroll
    def "verify page size"(String shipmentType, Integer pageSize) {

        given: "all outboundShipments"
        // verify page size[2] and verify page size[4] fails in itest env but passes in stable
        when: "searchText is #shipmentType and limit per page is #pageSize "
        def queryObject = new QueryObject()
        queryObject.setType("shipments")
        queryObject.addSortFields("shipmentId","desc","string")
        queryObject.setLimit(pageSize)
        queryObject.setOffset(0)
        queryObject.addQuery("shipmentType", "outboundShipment")
        queryObject.addQuery("shipmentMode", shipmentType)
        def response = QueryResultHandler.getQueryResultHandler().executeWithRetry(queryObject, 30, 2, 2, pageSize, pageSize)

        then: "return object count should equal to #pageSize"
        //  response.totalCount == response.count
        assert   response.body.count == pageSize

        where:
        shipmentType | pageSize
        "Ocean"      | 0
        "Road"       | 5
        "Ocean"      | 10
        "Road"       | 15
        "Ocean"      | 20
    }
}
