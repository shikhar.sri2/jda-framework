/**
 *
 */
package com.jda.dct.e2e.ui.utility.helper.Utility;

import com.jda.dct.e2e.ui.utility.helper.Resource.ResourceHelper;
import com.jda.dct.e2e.ui.utility.common.BaseTest;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

public class UtilityHelper extends BaseTest {

    /**
     * This method create a directory if it does not exists
     * @param DirectoryName
     */
    private String defaultDownloadPath = ResourceHelper.getResourcePath(UtilityHelper.getProperty("testFramework","default.download.path"));
    public static void CreateADirectory(String DirectoryName) {

        String workingDirectory = ResourceHelper.getBaseResourcePath();
        String dir = workingDirectory + File.separator + DirectoryName;
        File file = new File(dir);
        if (!file.exists()) {
            file.mkdir();
        }
    }

    /**
     * This method create a directory if it does not exists
     * @param DirectoryName
     */
    public static void CreateOrCleanADirectory(String DirectoryName) {
        try {
            String workingDirectory = ResourceHelper.getBaseResourcePath();
            String dir = workingDirectory + File.separator + DirectoryName;
            File file = new File(dir);
            if (!file.exists()) {
                file.mkdir();
            } else {
                FileUtils.cleanDirectory(file);
            }
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    /**
     * This file returns value from Property file
     * @param filename
     * @param key
     * @return String
     */
    public static String getProperty(String filename, String key) {
        Properties prop = new Properties();
        try {
            prop.load(ResourceHelper.getResourcePathInputStream("/src/main/resources/" + filename + ".properties"));
            return prop.getProperty(key);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }
    public void deleteFileInFileSystem()
    {
        File folder=new File(defaultDownloadPath);
        //List the files on that folder
        File[] listOfFiles = folder.listFiles();
        for (File listOfFile : listOfFiles)
        {
            if (listOfFile.isFile()) {
                String fileName = listOfFile.getName();
                Reporter("File " + listOfFile.getName() + " is deleted from System", "Info");
                listOfFile.deleteOnExit();
            }
        }
    }

    private static int retryCheck = 0 ;
    public boolean verifyFileInFileSystem(String expectedFileName) {
        try {
            File folder = new File(defaultDownloadPath);
            //List the files on that folder
            if (folder.listFiles().length > 0) {
                File[] listOfFiles = folder.listFiles();
                for (File listOfFile : listOfFiles) {
                   if (listOfFile.isFile()) {
                        String fileNameBeforeExtension = listOfFile.getName().substring(0, listOfFile.getName().indexOf(".") - 1);
                        String expectedFileNameBeforeExtension = expectedFileName.substring(0, expectedFileName.indexOf(".") - 1);
                        if (fileNameBeforeExtension.contains(expectedFileNameBeforeExtension)) {
                            Reporter("File has been downloaded and present is File system", "Pass");
                            return true;
                            //listOfFile.deleteOnExit();//if file is present, delete the file
                        }
                    }
                }
            }else if (retryCheck < 5){
                retryCheck ++;
                Thread.sleep(1000);
                return verifyFileInFileSystem(expectedFileName);
            }else {
                Reporter("File Not found in file System", "Fail");
            }
            return false;
        } catch (Exception e) {
            return false;
        }finally {
            retryCheck = 0;
        }
    }

    /**
     * This method verifies if file is present at default downloaded path and return file name
     * @param expectedFileName
     * @return
     */
    public String verifyAndReturnFileNameIfFileFoundInFileSystem(String expectedFileName) {
        try {
            File folder = new File(defaultDownloadPath);
            //List the files on that folder
            if (folder.listFiles().length > 0) {
                File[] listOfFiles = folder.listFiles();
                for (File listOfFile : listOfFiles) {
                    if (listOfFile.isFile()) {
                        String fileNameBeforeExtension = listOfFile.getName().substring(0, listOfFile.getName().indexOf(".") - 1);
                        String expectedFileNameBeforeExtension = expectedFileName.substring(0, expectedFileName.indexOf(".") - 1);
                        if (fileNameBeforeExtension.contains(expectedFileNameBeforeExtension)) {
                            Reporter("File has been downloaded and present is File system", "Pass");
                            return listOfFile.getName();
                            //listOfFile.deleteOnExit();//if file is present, delete the file
                        }
                    }
                }
            }else if (retryCheck < 15){
                retryCheck ++;
                Thread.sleep(1000);
                return verifyAndReturnFileNameIfFileFoundInFileSystem(expectedFileName);
            }else {
                Reporter(expectedFileName + " file Not found in file System", "Fail");
            }
            Reporter(expectedFileName + " file Not found in file System", "Fail");
            throw new RuntimeException(expectedFileName + " file Not found in file System");
        } catch (Exception e) {
            Reporter(expectedFileName + " file Not found in file System", "Fail");
            throw new RuntimeException(expectedFileName + " file Not found in file System:"+ e);
        }finally {
            retryCheck = 0;
        }
    }
}
