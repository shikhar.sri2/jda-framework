package com.jda.dct.e2e.readyapi.utility

import org.apache.commons.csv.CSVFormat
import org.apache.commons.csv.CSVParser
import org.apache.commons.csv.CSVPrinter
import org.apache.commons.csv.CSVRecord

import java.nio.charset.StandardCharsets
import java.nio.file.Files
import java.nio.file.Path

class CsvData {

    static List<String> getFields(String fileRecord) {
        List<String> fields = new ArrayList<>()
        def parser = CSVParser.parse(fileRecord, CSVFormat.EXCEL)
            for (def record : parser) {
                for (def field : record) {
                    fields.add(field)
                }
            }
        return fields
    }

    static void write(Path csvFilePath, List<List<String>> records) {
        Files.newBufferedWriter(csvFilePath, StandardCharsets.UTF_8).withWriter() { writer ->
            new CSVPrinter(writer, CSVFormat.EXCEL).with() { printer ->
                printer.printRecords(records)
                writer.close()
                printer.close()
            }
        }
    }
}
