package com.jda.dct.e2e.clientUi.pages.Start


import geb.Page

import org.openqa.selenium.By


class HomeMenuPage extends Page {
    static url = ""

    static at = {
        $(".icon").isDisplayed()
    }


    static content = {
        menuButton(wait: true) {
            $("button[aria-label=Menu]")
        }
        slidRightButton(wait: true) {
            $("#icon_next")
        }
        slidLeftButton(wait: true) {
            $("#icon_previous")
        }
        demand(wait: true) {
            $("a[href*='demand']").children()
        }
        supply(wait: true) {
            $("a[href*='tempraturecontrolled']").children()
        }
        analytics(wait: true) {
            $("a[href*='analytics']").children()
        }
        insights(wait: true) {
            $("a[href*='insights']").children()
        }

        logoutmenu (wait: true){
            $(By.xpath("//span[text()='LOGOUT']"))

     }


    }


    def moduleSupply() {
        println "\nFrom HomeMenuPage, user can navigate to tempraturecontrolled module"
        println "User click menu button"
        menuButton.click()
        println "User click tempraturecontrolled button"
        supply.click()


    }

    def moduleDemand() {
        println "\nFrom HomePage, user can navigate to demand module"
        println "User click menu button"
        menuButton.click()
        println "Current page url: " + getDriver().getCurrentUrl()

        println "User click demand button"
        demand.click()
        println "Current page url: " + getDriver().getCurrentUrl()
    }



    def logout(){
             menuButton.click()
            logoutmenu.click()
     }

}



// def moduleAnalytics() {
//        println "\nFrom watch tower page, user can navigate to analytics module"
//        println "User click menu button"
//        menuButton.click()
//        println "Current page url: " + getExceptionPage().getCurrentUrl()
//
//        println "User click tempraturecontrolled button"
//        analytics.click()
//        println "Current page url: " + getExceptionPage().getCurrentUrl()
//    }
//
//    def moduleInsights() {
//        println "\nFrom HomePage, user can navigate to insights module"
//        println "User click menu button"
//        menuButton.click()
//        println "Current page url: " + getExceptionPage().getCurrentUrl()
//
//        println "User click insight button"
//        insights.click()
//        println "Current page url: " + getExceptionPage().getCurrentUrl()
//    }








