/*
package com.jda.dct.e2e.clientUi.specs.SupplyModule

import com.jda.dct.e2e.clientUi.pages.Start.HomeMenuPage
import com.jda.dct.e2e.clientUi.pages.Start.HomePage
import com.jda.dct.e2e.clientUi.pages.Start.InitialLoginPage
import com.jda.dct.e2e.clientUi.pages.Start.SecondPage
import com.jda.dct.e2e.clientUi.pages.SupplyModule.SupplyDeliverydetailsPage
import com.jda.dct.e2e.clientUi.pages.SupplyModule.SupplyDeliverylistPage
import com.jda.dct.e2e.clientUi.pages.SupplyModule.SupplyInventorydetailsPage
import com.jda.dct.e2e.clientUi.pages.SupplyModule.SupplyInventorylistPage
import com.jda.dct.e2e.clientUi.pages.SupplyModule.SupplyOrderlistPage
import com.jda.dct.e2e.clientUi.pages.SupplyModule.SupplyPurchaseOrderdetailsPage
import com.jda.dct.e2e.clientUi.pages.SupplyModule.SupplyShipmentlistPage
import com.jda.dct.e2e.clientUi.pages.SupplyModule.SupplyshipmentdetailsPage

import geb.driver.CachingDriverFactory
import geb.spock.GebSpec
import spock.lang.Stepwise


@Stepwise
class SupplyModuleSpec extends GebSpec {

    def setupSpec() {
        CachingDriverFactory.clearCache()
        to InitialLoginPage
        at(InitialLoginPage).signin()
        at(SecondPage).login()
        to HomePage
    }


    def "Navigating to Supply Module"() {
        given: "User is at HomePage"
        to HomeMenuPage

        when: "user is at tempraturecontrolled Module: Shipment"
        at(HomeMenuPage).moduleSupply()

        then: "User is at Supply Shipment list Page"
        at(SupplyShipmentlistPage).searchForItem()

        at (SupplyShipmentlistPage).gotoSupplyShipment()

        then: "User is at Supply shipment details Page"
        at(SupplyshipmentdetailsPage).gobacktosupply()
    }


    def "Navigating to Supply Module:Delivery"() {
        given: "User is at SupplyModule"
        to SupplyDeliverylistPage

        when: "User is navigated to Supply Delivery List Page"
        at(SupplyDeliverylistPage).gotoSupplyDelivery()

        then: "User is navigated to Supply Delivery Details Page"
        at SupplyDeliverydetailsPage

        at(SupplyDeliverydetailsPage).gobacktosupplydelivery()


    }


    def "Navigating to Supply Module:Order"() {
        when: "User is at Supply/order Page"
        to SupplyOrderlistPage

        then: "User is navigated to Supply Order List Page"
        at(SupplyOrderlistPage).gotoSupplyOrder()

        then: "User is at Supply Purchase Order Details Page"
        at(SupplyPurchaseOrderdetailsPage).gobacktosupplyOrder()
    }


    def "Navigating to Supply Module:Inventory"() {
        given: "User is navigated to Supply/Inventory Page"
        to SupplyInventorylistPage

        when: "User is navigated to Supply inventory List page"
        at(SupplyInventorylistPage).gotoSupplyInventory()

        then: "User is navigated to Supply Inventory Details Page"
        at(SupplyInventorydetailsPage).gotoSupplyInventorydetails()

        println(" SUPPLY MODULE COMPLETED")
    }


    def cleanupSpec() {
        to HomeMenuPage
        at(HomeMenuPage).logout()
        CachingDriverFactory.clearCacheAndQuitDriver()
    }

}
*/
