package com.jda.dct.e2e.ui.supply;

import com.jda.dct.e2e.ui.utility.common.BaseTest;
import com.jda.dct.e2e.ui.utility.common.LeftMenu;
import com.jda.dct.e2e.ui.utility.pageobjects.*;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
        import org.testng.annotations.Test;

public class SupplySortByTests extends BaseTest {

    private LeftMenu leftMenu;
    private TopMenuPage topMenu;
    private SortByPage sortBy;
    private SupplyOrderListPage supplyOrderListPage;
    private SupplyDeliveryListPage supplyDeliveryListPage;
    private SupplyShipmentListPage supplyShipmentListPage;
    private SupplyForecastListPage supplyForecastListPage;
    private WebDriver driver;

    @BeforeClass
    public void setUp() throws InterruptedException {
        System.out.println("Starting tests for :" + this.getClass().getName());
//Initialize required objects
        driver = getDriver();
        leftMenu = new LeftMenu(driver);
        topMenu = new TopMenuPage(driver);
        sortBy = new SortByPage(driver);
        supplyOrderListPage = new SupplyOrderListPage(driver);
        supplyDeliveryListPage = new SupplyDeliveryListPage(driver);
        supplyShipmentListPage = new SupplyShipmentListPage(driver);
        supplyForecastListPage = new SupplyForecastListPage(driver);

        //loginAndNavigateToSupplyShipmentLandingPage
        logInToApplication();
    }

    @Test(priority = 1)
    public void validateIsAscendingSupplyShipmentListDisplayed() throws InterruptedException {
        leftMenu.clickSupplyMenuOption();
        topMenu.goToSupplyShipmentsTab();
        topMenu.clickExceptionsTab();
        topMenu.clickSupplyShipmentExceptionsOnTime();
        sortBy.clickAscendingShipment();//click on bol
        System.out.println("arranged in ASC sorting order...");
        sortBy.isAscendingShipmentListDisplayed();
    }
    @Test(priority = 2)
    public void validateIsDescendingSupplyShipmentListDisplayed() throws InterruptedException {
        leftMenu.clickSupplyMenuOption();
        topMenu.goToSupplyShipmentsTab();
        topMenu.clickExceptionsTab();
        topMenu.clickSupplyShipmentExceptionsDelayed();
        sortBy.clickDescendingShipment();
        System.out.println("arranged in DSC sorting order...");
       sortBy.isDescendingShipmentListDisplayed();


    }


}