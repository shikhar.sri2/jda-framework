package com.jda.dct.e2e.api.salesOrders

import com.jda.dct.domain.stateful.SalesOrder
import com.jda.dct.e2e.api.utility.IngestionHelper
import groovy.json.JsonBuilder
import groovy.json.JsonSlurper
import org.apache.groovy.json.internal.LazyMap
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.HttpMethod
import spock.util.concurrent.PollingConditions

class SampleSalesOrderShowcaseData implements IngestionHelper{

    private final Logger LOGGER = LoggerFactory.getLogger(SampleSalesOrderShowcaseData.class)
    static Random random = new Random()

    SampleSalesOrderShowcaseData() {}

    def getSalesOrderFromShowcase(String salesOrderId) {
        init()
        def getPath = properties.getProperty('getSalesOrderPath')
        def getURLPrefix = protocol + ip + getPath
//      println "Url prefix is: " + getURLPrefix
        def conditions = new PollingConditions(timeout: 15, initialDelay: 1)
        def res = null
        conditions.eventually {
            res = executeHttpCall(header, getURLPrefix + "?query=salesOrderId=" + salesOrderId, null, HttpMethod.GET)
            println "Executing at URL: " + getURLPrefix + "?query=salesOrderId=" + salesOrderId
            assert res != null
        }
        if (res['data'] == null || res['data'].size() == 0) {
            LOGGER.warn("getSalesOrderFromShowcase with salesOrderid: {}, data return null", salesOrderId)
        }
        def salesOrder = res['data'][0]
        return salesOrder
    }
    def modifySalesOrder(String salesOrderId, String key, String value, String filename) {
        List<Object> salesOrderList = new ArrayList<>()
        def salesOrder = getSalesOrderFromShowcase(salesOrderId)
        ((LazyMap)salesOrder).put(key, value)
        new File(filename).write(new JsonBuilder(salesOrder).toPrettyString())
        def updatedSalesOrder =  new JsonSlurper().parseText(new File(filename).text)
        new File(filename).delete()
        salesOrderList.add(updatedSalesOrder)
        return salesOrderList
    }
    def modifySalesOrderMultiFields(String salesOrderId, Map<String, String> fields, String filename) {
        List<Object> salesOrderList = new ArrayList<>()
        def salesOrder = getSalesOrderFromShowcase(salesOrderId)
        if (fields.isEmpty()) {
            return null
        }
        for (String field : fields.keySet()) {
            ((LazyMap)salesOrder).put(field, fields.get(field))
        }
        new File(filename).write(new JsonBuilder(salesOrder).toPrettyString())
        def updatedSalesOrder =  new JsonSlurper().parseText(new File(filename).text)
        new File(filename).delete()
        salesOrderList.add(updatedSalesOrder)
        return salesOrderList
    }
    def duplicateSalesOrder(String salesOrderId) {
        List<Object> salesOrderList = new ArrayList<>()
        def salesOrder = getSalesOrderFromShowcase(salesOrderId)
        def newSalesOrderId = salesOrderId + "-" + (random.nextInt()%1000+1)
        println "New Sales Order Id is: " + newSalesOrderId
        ((Map)salesOrder).put("salesOrderId", newSalesOrderId)
        new File("temp-new.json").write(new JsonBuilder(salesOrder).toPrettyString())
        def newSalesOrder =  new JsonSlurper().parseText(new File("temp-new.json").text)
        new File("temp-new.json").delete()
        salesOrderList.add(newSalesOrder)
        def ingestId = ingestData(salesOrderList, "salesOrders?processType=demand&salesOrderType=standardOrder")
        def status = waitUntilComplete(ingestId)
        println "Duplication status log: " + status
        return newSalesOrderId
    }
}
