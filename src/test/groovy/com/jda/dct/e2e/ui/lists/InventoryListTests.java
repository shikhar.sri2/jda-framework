package com.jda.dct.e2e.ui.lists;

import com.jda.dct.e2e.ui.utility.action.CommonActions;
import com.jda.dct.e2e.ui.utility.common.BaseTest;
import com.jda.dct.e2e.ui.utility.common.LeftMenu;
import com.jda.dct.e2e.ui.utility.helper.AssertHelper.AssertHelper;
import com.jda.dct.e2e.ui.utility.pageobjects.*;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class InventoryListTests extends BaseTest {

    private LeftMenu leftMenu;
    private TopMenuPage topMenu;
    private TopSubMenuPage topSubMenu;
    private SupplyShipmentListPage supplyShipmentListPage;
    private SupplyShipmentDetailsPage supplyShipmentDetailsPage;
    private InventoryListPage inventoryListPage;
    private AssertHelper assertHelper;
    private CommonActions commonActions;
    private WebDriver driver;


    @BeforeClass
    public void setUp() throws InterruptedException {
        System.out.println("Starting tests for :" + this.getClass().getName());
        //Initialize required objects
        driver = getDriver();
        assertHelper = new AssertHelper(driver);
        leftMenu = new LeftMenu(driver);
        topMenu = new TopMenuPage(driver);
        topSubMenu = new TopSubMenuPage(driver);
        supplyShipmentListPage = new SupplyShipmentListPage(driver);
        supplyShipmentDetailsPage = new SupplyShipmentDetailsPage(driver);
        inventoryListPage = new InventoryListPage(driver);
        commonActions = new CommonActions(driver);
        //loginAndNavigateToSupplyLandingPage
        logInToApplication();
    }


    @Test(priority = 1, retryAnalyzer = BaseTest.class)
    public void validateInventoryListPageIsDisplayed() throws InterruptedException {
        leftMenu.clickInventoryMenuOption();
        commonActions.disableAllExceptionFilter();
        assertHelper.assertTrue(inventoryListPage.isInventoryListPageDisplayed(), "Inventory list page is displayed");
        System.out.println(" Welcome to Inventory List Page");
    }



}

