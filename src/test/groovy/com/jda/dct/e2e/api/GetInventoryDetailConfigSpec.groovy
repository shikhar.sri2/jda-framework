package com.jda.dct.e2e.api

import com.jda.dct.e2e.api.utility.AuthHelper
import groovyx.net.http.RESTClient
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll

/**
 * GET method to retrieve demand inventory detail configuration
 *
 */

// may be redundant, check and delete on confirmation
class GetInventoryDetailConfigSpec extends Specification implements AuthHelper {
    static inventoryDetailConfigURL
    @Shared
    def client

    def setupSpec() {
        init()

        def inventoryDetailConfigPath = properties.getProperty('getInventoryDetailConfigPath')
        inventoryDetailConfigURL = protocol + ip + inventoryDetailConfigPath + "?"
        client = new RESTClient(inventoryDetailConfigURL)
        client.handler.failure = { resp -> resp }
        header[HttpHeaders.CONTENT_TYPE] = MediaType.APPLICATION_JSON
    }

    @Unroll
    def "Get inventory detail configuration API #url"() {
        given: "The API URL is #url "
        when: "HTTP Method is GET: should return demand inventory list configuration"
        def response = client.get(headers: header)
        then: "the response should be 200"
        with(response) {
            status == 200
        }
        where:
        url << [inventoryDetailConfigURL]

    }
}
