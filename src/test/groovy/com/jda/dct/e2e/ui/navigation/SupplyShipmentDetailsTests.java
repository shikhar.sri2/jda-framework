package com.jda.dct.e2e.ui.navigation;

import com.jda.dct.e2e.ui.utility.action.CommonActions;
import com.jda.dct.e2e.ui.utility.common.BaseTest;
import com.jda.dct.e2e.ui.utility.common.LeftMenu;
import com.jda.dct.e2e.ui.utility.helper.Browser.BrowserHelper;
import com.jda.dct.e2e.ui.utility.pageobjects.SupplyShipmentDetailsPage;
import com.jda.dct.e2e.ui.utility.pageobjects.SupplyShipmentListPage;
import com.jda.dct.e2e.ui.utility.pageobjects.TopMenuPage;
import com.jda.dct.e2e.ui.utility.pageobjects.TopSubMenuPage;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class SupplyShipmentDetailsTests extends BaseTest {

    private LeftMenu leftMenu;
    private TopMenuPage topMenu;
    private TopSubMenuPage topSubMenu;
    private SupplyShipmentListPage supplyShipmentListPage;
    private SupplyShipmentDetailsPage supplyShipmentDetailsPage;
    private BrowserHelper browserHelper;
    private CommonActions commonActions;
    private WebDriver driver;


    @BeforeClass
    public void setUp() throws InterruptedException {
        //Initialize required objects
        driver = getDriver();
        leftMenu = new LeftMenu(driver);
        topMenu = new TopMenuPage(driver);
        topSubMenu = new TopSubMenuPage(driver);
        supplyShipmentListPage = new SupplyShipmentListPage(driver);
        supplyShipmentDetailsPage = new SupplyShipmentDetailsPage(driver);
        browserHelper = new BrowserHelper(driver);
        commonActions = new CommonActions(driver);
        //loginAndNavigateToSupplyShipmentsLandingPage
        logInToApplication();
    }


    @Test(priority = 1,retryAnalyzer = BaseTest.class)
    public void validateSupplyShipmentDetailsPageIsDisplayed() throws InterruptedException {
        leftMenu.clickSupplyMenuOption();
        topMenu.goToSupplyShipmentsTab();
        topMenu.clearFilterIfVisible();
        supplyShipmentListPage.clickOceanIcon();
        commonActions.disableAllExceptionFilter();
        supplyShipmentListPage.clickSupplyShipmentNumber();
        //verify that user lands on Supply Details page
        Assert.assertTrue(supplyShipmentListPage.isSupplyShipmentDetailsPageDisplayed());
        browserHelper.refresh();
    }

    //testcases for  object navigation inside a ShipmentNo

    //Shipment Notes
    @Test(priority = 2,retryAnalyzer = BaseTest.class)
    public void validateNotesTabIsDisplayedWhenShipmentNotesIsClicked() throws InterruptedException {
        leftMenu.clickSupplyMenuOption();
        topMenu.goToSupplyShipmentsTab();
        supplyShipmentListPage.clickOceanIcon();
        commonActions.disableAllExceptionFilter();
        supplyShipmentListPage.clickSupplyShipmentNumber();
//click the Notes available in a Shipment No
        supplyShipmentDetailsPage.clickNotes();
//verify user lands at Notes page
        Assert.assertTrue(supplyShipmentDetailsPage.isNotesPageDisplayed());
        browserHelper.refresh();
    }


    //click ShipmentallItems
    @Test(priority = 3,retryAnalyzer = BaseTest.class)
    public void validateSupplyInventoryListPageIsDisplayedWhenShipmentAllitemsIsClicked() throws InterruptedException {
        leftMenu.clickSupplyMenuOption();
        topMenu.goToSupplyShipmentsTab();
        supplyShipmentListPage.clickOceanIcon();
        commonActions.disableAllExceptionFilter();
        supplyShipmentListPage.clickSupplyShipmentNumber();
        //verify that user lands on Supply Details page
        Assert.assertTrue(supplyShipmentListPage.isSupplyShipmentDetailsPageDisplayed());
        //click the Allitems available in a Shipment No
        supplyShipmentDetailsPage.clickShipmentAllitems();
        //verify that user lands on SupplyInventory page
        Assert.assertTrue(supplyShipmentDetailsPage.isInventoryPageDisplayed());
        //return to Supply shipment tab
        browserHelper.refresh();

    }


    //shipmentHotitems
    @Test(priority = 4,retryAnalyzer = BaseTest.class)
    public void validateSupplyInventoryListPageIsDisplayedWhenShipmentHotItemsIsClicked() throws InterruptedException {
        leftMenu.clickSupplyMenuOption();
        topMenu.goToSupplyShipmentsTab();
        supplyShipmentListPage.clickOceanIcon();
        commonActions.disableAllExceptionFilter();
        supplyShipmentListPage.clickSupplyShipmentNumber();
        //verify that user lands on Supply Details page
        Assert.assertTrue(supplyShipmentListPage.isSupplyShipmentDetailsPageDisplayed());
        //click the hotItems available in a Shipment No
        supplyShipmentDetailsPage.clickShipmentHotitems();
        //verify that user lands on SupplyInventory page
        Assert.assertTrue(supplyShipmentDetailsPage.isInventoryPageDisplayed());
        browserHelper.refresh();
    }

    //Shipment all Orders
    @Test(priority = 5,retryAnalyzer = BaseTest.class)
    public void validateSupplyOrderListPageIsDisplayedWhenAllordersIsClicked() throws InterruptedException {
        leftMenu.clickSupplyMenuOption();
        topMenu.goToSupplyShipmentsTab();
        supplyShipmentListPage.clickOceanIcon();
        commonActions.disableAllExceptionFilter();
        supplyShipmentListPage.clickSupplyShipmentNumber();
//verify that user lands on Supply Details page
        Assert.assertTrue(supplyShipmentListPage.isSupplyShipmentDetailsPageDisplayed());
        //click the Allorders available in a Shipment No
        supplyShipmentDetailsPage.clickContainerAllorders();
//verify that user lands on SupplyOrderListPage
        Assert.assertTrue(supplyShipmentDetailsPage.isSupplyOrderPageDisplayed());
        browserHelper.refresh();
    }

    //  Shipment Hot Orders
    @Test(priority = 6,retryAnalyzer = BaseTest.class)
    public void validateSupplyOrderListPageIsDisplayedWhenShipmentHotordersIsClicked() throws InterruptedException {
        leftMenu.clickSupplyMenuOption();
        topMenu.goToSupplyShipmentsTab();
        supplyShipmentListPage.clickOceanIcon();
        commonActions.disableAllExceptionFilter();
//        supplyShipmentListPage.clickShipmentExceptionHavingData();
        supplyShipmentListPage.clickSupplyShipmentNumber();
//verify that user lands on Supply Details page
        Assert.assertTrue(supplyShipmentListPage.isSupplyShipmentDetailsPageDisplayed());
//click HotOrders available in a Shipment No
        supplyShipmentDetailsPage.clickShipmentHotorders();
        //verify that user lands on Supply OrderList page
        Assert.assertTrue(supplyShipmentDetailsPage.isSupplyOrderPageDisplayed());
        browserHelper.refresh();
    }


    // testcases to validate object navigation inside Supplyshipment Details page

    //container Trailer no
    @Test(priority = 7,retryAnalyzer = BaseTest.class)
    public void validateSupplyDeliveryListPageIsDisplayedWhenContainerNoIsClicked() throws InterruptedException {
        String shipmentNo = "IS001_SD_S1";
        leftMenu.clickSupplyMenuOption();
        topMenu.goToSupplyShipmentsTab();
        supplyShipmentListPage.clickOceanIcon();
        commonActions.disableAllExceptionFilter();
        topSubMenu.goToSearchFeature(shipmentNo);
        //Assert.assertTrue(supplyShipmentDetailsPage.isSearchShipmentNumberDisplayed(shipmentNo));
        supplyShipmentDetailsPage.clickSearchedShipmentNumber(shipmentNo);
//click the trailer number available in a containerNo
        supplyShipmentDetailsPage.clickContainerNumber();
//verify that user lands on SupplyDeliveryDetails page
        Assert.assertTrue(supplyShipmentDetailsPage.isSupplyDeliveryPageDisplayed());
        topMenu.goToSupplyShipmentsTab();
        topMenu.clearFilter();
        browserHelper.refresh();
    }

    @Test(priority = 8,retryAnalyzer = BaseTest.class)
    public void validateSupplyInventoryListPageIsDisplayedWhenShipmentLineAllitemsIsClicked() throws InterruptedException {
        leftMenu.clickSupplyMenuOption();
        topMenu.goToSupplyShipmentsTab();
        topMenu.clearFilterIfVisible();
        supplyShipmentListPage.clickOceanIcon();
        commonActions.disableAllExceptionFilter();
        supplyShipmentListPage.clickSupplyShipmentNumber();
        //verify that user lands on Supply Details page
        Assert.assertTrue(supplyShipmentListPage.isSupplyShipmentDetailsPageDisplayed());
        //click Allitems available in a containerNo
        supplyShipmentDetailsPage.clickContainerAllitems();

        //verify that user lands on  Inventory List page
        Assert.assertTrue(supplyShipmentDetailsPage.isInventoryPageDisplayed());
        browserHelper.refresh();
    }

    //Container Hot items
    @Test(priority = 9,retryAnalyzer = BaseTest.class)
    public void validateSupplyInventoryListPageIsDisplayedWhenShipmentLineHotitemsIsClicked() throws InterruptedException {
        leftMenu.clickSupplyMenuOption();
        topMenu.goToSupplyShipmentsTab();
        topMenu.clearFilterIfVisible();
        supplyShipmentListPage.clickOceanIcon();
        commonActions.disableAllExceptionFilter();
        supplyShipmentListPage.clickSupplyShipmentNumber();
        //verify that user lands on Supply Details page
        Assert.assertTrue(supplyShipmentListPage.isSupplyShipmentDetailsPageDisplayed());
//click Hotitems available in a containerNo
        supplyShipmentDetailsPage.clickContainerHotitems();

        //verify that user lands on Supply Inventory List page
        Assert.assertTrue(supplyShipmentDetailsPage.isInventoryPageDisplayed());
        browserHelper.refresh();
    }


    //Container All Orders
    @Test(priority = 10,retryAnalyzer = BaseTest.class)
    public void validateSupplyOrderListPageIsDisplayedWhenShipmentLineAllordersIsClicked() throws InterruptedException {
        leftMenu.clickSupplyMenuOption();
        topMenu.goToSupplyShipmentsTab();
        topMenu.clearFilterIfVisible();
        supplyShipmentListPage.clickOceanIcon();
        commonActions.disableAllExceptionFilter();
        supplyShipmentListPage.clickSupplyShipmentNumber();
        //verify that user lands on Supply Details page
        Assert.assertTrue(supplyShipmentListPage.isSupplyShipmentDetailsPageDisplayed());
//click AllOrders available in a containerNo
        supplyShipmentDetailsPage.clickContainerAllorders();

        //verify that user lands on Supply OrderList page
        Assert.assertTrue(supplyShipmentDetailsPage.isSupplyOrderPageDisplayed());
        browserHelper.refresh();
    }

    @Test(priority = 11,retryAnalyzer = BaseTest.class)
    public void validateSupplyOrderListPageIsDisplayedWhenShipmentLineHotordersIsClicked() throws InterruptedException {
        leftMenu.clickSupplyMenuOption();
        topMenu.goToSupplyShipmentsTab();
        topMenu.clearFilterIfVisible();
        supplyShipmentListPage.clickOceanIcon();
//        supplyShipmentListPage.clickShipmentExceptionHavingData();
        supplyShipmentListPage.clickSupplyShipmentNumber();
        //verify that user lands on Supply Details page
        Assert.assertTrue(supplyShipmentListPage.isSupplyShipmentDetailsPageDisplayed());
//click HotOrders available inside a containerNo
        supplyShipmentDetailsPage.clickContainerHotorders();
        //verify that user lands on Supply OrderList page
        Assert.assertTrue(supplyShipmentDetailsPage.isSupplyOrderPageDisplayed());
        browserHelper.refresh();
    }

    //validating click on trailer button on Shipment list page
/*    @Test(priority = 12,retryAnalyzer = BaseTest.class)
    public void validateSupplyDeliveryListPageIsDisplayedWhenTrailerNumberIsClicked() throws InterruptedException {
        String shipmentNo = "IS001_DL_S1";
        topMenu.goToSupplyShipmentsTab();
        topMenu.clickExceptionsTab();
        topMenu.clickSupplyShipmentExceptionsLate();
        supplyShipmentListPage.clickMaterialGroupDropdown();
        supplyShipmentListPage.clickFinishedGoodsMaterialType();
        supplyShipmentListPage.clickRoadIcon();
        topSubMenu.goToSearchFeature(shipmentNo);

        supplyShipmentListPage.clickShipmentExceptionHavingData();
        supplyShipmentListPage.clickExpandButton();
        //verify tempraturecontrolled trailer number
        supplyShipmentListPage.clickSupplyTrailerNumber();
//verify that user lands on SupplyDeliveryDetails page
        Assert.assertTrue(supplyShipmentDetailsPage.isSupplyDeliveryPageDisplayed());
        supplyShipmentDetailsPage.clickBackToSupplyShipmentTab();
        topMenu.clearFilter();
        browserHelper.refresh();
    }*/

}
