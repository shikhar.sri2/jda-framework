package com.jda.dct.e2e.ui.lots;


import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;
import com.jda.dct.e2e.ui.utility.common.BaseTest;
import com.jda.dct.e2e.ui.utility.common.LeftMenu;
import com.jda.dct.e2e.ui.utility.helper.AssertHelper.AssertHelper;
import com.jda.dct.e2e.ui.utility.helper.Browser.BrowserHelper;
import com.jda.dct.e2e.ui.utility.helper.Wait.WaitHelper;
import com.jda.dct.e2e.ui.utility.pageobjects.*;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

public class SupplyItemLotSummaryTests extends BaseTest {
    private LeftMenu leftMenu;
    private TopMenuPage topMenu;
    private SupplyDeliveryListPage supplyDeliveryPage;
    private SupplyShipmentListPage supplyShipmentListPage;
    private SupplyShipmentDetailsPage supplyShipmentDetailsPage;
    private TopSubMenuPage topSubMenu;
    private BrowserHelper browserHelper;
    private AssertHelper assertHelper;
    private WebDriver driver;
    private WaitHelper waitHelper;

    @BeforeClass
    public void setUp() throws InterruptedException {
        System.out.println("Starting tests for :" + this.getClass().getName());
//Initialize required objects
        driver = getDriver();
        leftMenu = new LeftMenu(driver);
        topMenu = new TopMenuPage(driver);
        topSubMenu = new TopSubMenuPage(driver);
        supplyDeliveryPage = new SupplyDeliveryListPage(this.driver);
        supplyShipmentListPage = new SupplyShipmentListPage(this.driver);
        supplyShipmentDetailsPage = new SupplyShipmentDetailsPage(this.driver);
        browserHelper = new BrowserHelper(driver);
        assertHelper = new AssertHelper(driver);
        waitHelper = PageFactory.initElements(driver, WaitHelper.class);
        //loginAndNavigateToSupplyShipmentLandingPage
        logInToApplication();
    }

    @BeforeMethod
    public void commonMethods() {
        //navigates to Supply shipment tab
        leftMenu.clickSupplyMenuOption();

    }

    @Test(priority =1 )
    //Validate Supply Delivery  page - change method name and assertion
    public void validateIsSupplyLotSummaryPageDisplayed() throws InterruptedException {
        String shipmentNo = "IS003_CO";
        topMenu.goToSupplyShipmentsTab();
        topMenu.clickStatesTab();
        topMenu.clickSupplyShipmentStatesPlannedShipment();
        topSubMenu.clickMaterialGroupDropdown();
        topSubMenu.clickAllTypes();
        supplyShipmentListPage.clickRoadIcon();
        topSubMenu.goToSearchFeature(shipmentNo);
        topMenu.clearFilterIfVisible();
        waitHelper.waitForPageToLoad();
        supplyShipmentDetailsPage.clickSearchedShipmentNumber(shipmentNo);
        supplyShipmentListPage.clickLotCount();
        supplyShipmentListPage.isLotValueDisplayed();
        assertHelper.assertTrue(supplyShipmentListPage.isLotSummaryPaneDisplayed(),"Items with Lots are displayed");
        assertHelper.assertTrue(supplyShipmentListPage.islotIdsDisplayed(), "Lot Ids l019, L015 are displayed");
    }
}
