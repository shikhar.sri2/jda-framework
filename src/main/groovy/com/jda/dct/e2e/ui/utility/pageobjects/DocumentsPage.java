package com.jda.dct.e2e.ui.utility.pageobjects;

import com.jda.dct.e2e.ui.utility.action.CommonActions;
import com.jda.dct.e2e.ui.utility.common.BasePage;
import com.jda.dct.e2e.ui.utility.helper.Wait.WaitHelper;
import com.jda.dct.e2e.ui.utility.helper.genericHelper.GenericHelper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;


public class DocumentsPage extends BasePage {
    WebDriver driver;
    WaitHelper objWaitHelper;
    GenericHelper objGenericHelper;
    CommonActions commonActions;

    public DocumentsPage(WebDriver webdriver) {

        this.driver = webdriver;
        PageFactory.initElements(driver, this);
        objWaitHelper = new WaitHelper(driver);
        objGenericHelper = new GenericHelper(driver);
        commonActions = new CommonActions(driver);
    }

    @FindBy(xpath = "//*[@id='select-option']/..")
    private WebElement allFilesFilter;

    @FindBy(xpath = "//*[@role='option'][@data-value='Downloaded Files']")
    private WebElement downloadedFilesFilter;

    @FindBy(xpath = "//*[@role='option'][@data-value='Uploaded Files']")
    private WebElement uploadedFilesFilter;

    @FindBy(xpath = "//*[@id='PanelSummary-RenderedItem-Type-0-OperationType']/a[1]")
    private List<WebElement> firstDownloadLink;

    @FindBy(xpath = "//*[@id='PanelSummary-RenderedItem-Completed-on-0-LinkItem']")
    private List<WebElement> completedOnColData;

    @FindBy(xpath = "//*[@id='PanelSummary-RenderedItem-Details-0-DetailsItem']/a/span")
    private WebElement failureDetails;

    //========Methods=====

    public boolean clickAllFilesFilter(){
        return objGenericHelper.elementClick(allFilesFilter, "All files filter");
    }

    public boolean clickDownloadFilesFilterOption(){
        return objGenericHelper.elementClick(downloadedFilesFilter, "Download Files option");
    }

    public boolean clickUploadFilesFilterOption(){
        return objGenericHelper.elementClick(uploadedFilesFilter, "Upload Files option");
    }

    public boolean clickOnFirstDownloadLink(){
        try {
            objWaitHelper.waitForElement(20, firstDownloadLink.get(0));
            return objWaitHelper.waitAndClickElement(firstDownloadLink.get(0), "Download link");
        }catch (Exception e){
            return false;
        }
    }

    public boolean checkUploadFileStatus(){
        try {
            String uploadStatus = objGenericHelper.getTextFromElement(completedOnColData.get(0), "Completed On detail");
            if(uploadStatus.contains("Failed")){
                Reporter("Upload Failed", "Fail");
                return false;
            }else if (uploadStatus.contains("Processing")){
                Thread.sleep(1000);
                checkUploadFileStatus();
            }else {
                Reporter("Upload Successful", "Pass");
                return true;
            }
            return false;
        }catch (Exception e){
            Reporter("Exception while getting upload status:" + e, "Fail");
            return false;
        }
    }
}
