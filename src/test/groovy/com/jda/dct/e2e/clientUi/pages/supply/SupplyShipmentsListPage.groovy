//package com.jda.dct.e2e.clientUi.pages.tempraturecontrolled
//
//import geb.Page
//
//class SupplyShipmentsListPage extends Page{
//    static url = "/tempraturecontrolled/shipment"
//
//    static atCheckWaiting = true
//
//    static at = {
//        waitFor{
//            $("div", role:"button", 'aria-expanded':"false").first().isDisplayed()
//        }
//    }
//
//    static content = {
//        IS001_SO(wait: true){
//            $("a", href:contains("supplyo/shipment"),"text": "IS001_SO")
//        }
//        firstShipment(wait: true) {
//            $("#shipment-list-link-item-1").first().children()
//        }
//    }
//
//    def clickFirstShipment(){
//        waitFor {
//            firstShipment.displayed
//        }
//        firstShipment.click()
//    }
//
//}
