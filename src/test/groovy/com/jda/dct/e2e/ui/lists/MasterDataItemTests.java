package com.jda.dct.e2e.ui.lists;


import com.jda.dct.e2e.ui.utility.common.BaseTest;
import com.jda.dct.e2e.ui.utility.common.LeftMenu;
import com.jda.dct.e2e.ui.utility.pageobjects.MasterDataItemPage;
import com.jda.dct.e2e.ui.utility.pageobjects.TopMenuPage;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class MasterDataItemTests extends BaseTest {

    private LeftMenu leftMenu;
    private TopMenuPage topMenu;
    private WebDriver driver;
    private MasterDataItemPage masterDataItems;
    @BeforeClass
    public void setUp() throws InterruptedException {
        //Initialize required objects
        driver = getDriver();
        leftMenu = new LeftMenu(driver);
        topMenu = new TopMenuPage(driver);
    masterDataItems = new MasterDataItemPage(driver);

        //loginAndNavigateToSupplyLandingPage
        logInToApplication();

    }

    @BeforeMethod
    public void commonMethods() throws InterruptedException {
        //navigates to Demand Page
        leftMenu.clickMasterDataMenuOption();
    }

    @Test(priority = 1,retryAnalyzer = BaseTest.class)
    public void validateMasterDataItemListPageIsDisplayed() {
        //navigates to Master-Data Item Tab
        topMenu.goToMasterDataItemTab();
        //verify user is at MasterData Item List Page
        Assert.assertTrue(masterDataItems.isMasterDataItemlistPageDisplayed());
    }

}








