package com.jda.dct.e2e.api.nodesAndMeasures

import com.jda.dct.e2e.api.categories.Smoke
import com.jda.dct.e2e.api.utility.IngestionHelper
import com.jda.dct.e2e.api.utility.QueryObject
import com.jda.dct.e2e.api.utility.QueryResultHandler
import org.junit.experimental.categories.Category
import spock.lang.Specification
import spock.lang.Unroll

class ShipmentAllItemsHotItemsSpec extends Specification implements IngestionHelper {

    @Category(Smoke)
    @Unroll
    def "verify shipment #shipmentId is existed"() {
        given: "shipment ID : #shipmentId"

        when: "search for the shipment"
        def queryObject = new QueryObject()
        queryObject.setType("shipments")
        queryObject.addQuery("shipmentId", shipmentId)
        def response = QueryResultHandler.getQueryResultHandler().executeWithRetry(queryObject, 20, 2, 5,1,1)

        then: "the shipment exists"
        println(response.body.data.size())
        response.body.data.size() == 1

        where:
        shipmentId = "IS001_SD_S1"
    }
/*  Duplicate test case exist in ready API
    @Category(Smoke)
    @Unroll
    def "verify #shipmentId has #label counts"(String shipmentId, String itemType, String label) {
        given: "shipment ID : #shipmentId"

        when: "search for All items counts for the shipment "
        def queryObject = new QueryObject()
        queryObject.setType("nodes")
        queryObject.setShipmentNodeCounts(true)
        queryObject.addQuery("shipmentId", shipmentId)
        queryObject.addQuery("shipmentType", "inboundShipment")
        queryObject.addQuery("criticalities", itemType)

        def response = QueryResultHandler.getQueryResultHandler().executeWithRetry(queryObject, 200, 2, 10, 4,0)

        def countSize = response.body.typeCounts[0].typeCounts.size()

        def totalItems = 0
        (0..countSize - 1).each({ v ->
            totalItems = totalItems + response.body.typeCounts[0].typeCounts[v].count
        })

        then: "Header and Line counts should be equal"
        println(label + ": " + totalItems)
        assert response.body.typeCounts[0].count == totalItems

        where:
        shipmentId    | itemType  | label
        "IS001_SD_S1" | "HotItem" | "Hot_Item"
        "IS001_SD_S1" | ""        | "All_Item"
    }
    
 */
}