package com.jda.dct.e2e.api.utility

import java.text.SimpleDateFormat

import static java.util.concurrent.TimeUnit.*
import com.github.javafaker.*
import com.jda.dct.domain.Node
import com.jda.dct.domain.Tsd

trait UsesMockNodeData {
    abstract List<Node> getMockNodeData()
    def random
    def faker
    def boundingBoxes = [:]
    String DATE_FORMAT = "MM-dd-yyyy'T'HH:mm:ss"

    def DEFAULTS = [
        seed: 0L,
        numNodes: 1,
        numAttrPerNode: 5,
        tids: ['0'],
        earliestCreationDate: new SimpleDateFormat(DATE_FORMAT).parse('01-01-2000T00:00:00'),
        latestCreationDate: new SimpleDateFormat(DATE_FORMAT).parse('01-01-2018T00:00:00'),
        measures: ['Commits', 'Forecast'],
        measureLengthDays: 5,
        numAttrPerTsd: 2,
        maxQuantity: 1000000,
        boundingBoxes: [:]
    ]

    def someMockNodeData(Map props) {
        mergeWithDefaults(props)
        def seed = props.seed ?: System.currentTimeMillis()
        random = new Random(seed)
        faker = new Faker(random)

        props.boundingBoxes.each {
            entry ->
                initBoundingBox([which: entry.key, boxSize: entry.value])
        }

        mockNodeData = (1..props.numNodes).collect {
            def node = new Node(
                //attributes: mockAttributes(props.numAttrPerNode),
                tid: mockTid(props),
                creationDate: mockCreationDate(props),
                customerName: mockCustomerName(props),
                nodeType: mockNodeType(props),
                supplierName: mockSupplierName(props),
                customerItemName: mockCustItemName(props),
                customerItemOwnerName: mockCustItemOwnerName(props),
                supplierItemName: mockSuppItemName(props),
                supplierItemOwnerName: mockSuppItemOwnerName(props),
                shipToSiteName: mockShipToSiteName(props),
                shipToSiteOwnerName: mockShipToSiteOwnerName(props),
                shipFromSiteName: mockShipFromSiteName(props),
                shipFromSiteOwnerName: mockShipFromSiteOwnerName(props),
                status: mockStatus(props),
                lob: mockLob(props),
                processType: 'demand')
            node.lmd = mockLmd(node.creationDate, props)
            mockMeasures(node, props)

            node
        }
    }

    private def pickAny(a) {
        a[random.nextInt(a.size())]
    }
    private def mockSiteName() {
        def address = faker.address()
        [address.streetAddress(true), address.city(), address.country()].join(' ')
    }
    private def mockAttributes(Integer numAttr) {
        def ATTR_TYPES_TO_GEN = [
            {'color'}: {faker.color().name()},
            {faker.hacker().noun()}: {"${faker.hacker().ingverb()} ${faker.hacker().adjective()}"},
            {'cctype'}: {pickAny(CreditCardType.values())},
            {'code'}: {faker.code().isbn10()},
            {'superpower'}: {faker.superhero().power()}
        ]

        def attr = [:]
        (1..numAttr).each {
            def attrTypeToGen = pickAny(ATTR_TYPES_TO_GEN.entrySet())
            attr[attrTypeToGen.key.call()] = attrTypeToGen.value.call().toString()
        }
        attr
    }


    private def initBoundingBox(def bb) {
        def generator
        switch (bb.which) {
            case 'customerName':
                generator = faker.company().&name
                break
            case 'custItemName':
                generator = faker.commerce().&productName
                break
        }
        (1..bb.boxSize).each {
            boundingBoxes.get(bb.which, []) << generator()
        }
    }

    private def mockTid(props) {
        pickAny(props.tids)
    }
    private def mockCreationDate(props) {
        def earliestTime = props.earliestCreationDate.time
        def latestTime = props.latestCreationDate.time
        new Date(earliestTime + (long)(random.nextDouble() * (latestTime - earliestTime)))
    }
    private def mockLmd(Date creationDate, props) {
        def earliestTime = creationDate.time
        def latestTime = props.latestCreationDate.time
        new Date(earliestTime + (long)(random.nextDouble() * (latestTime - earliestTime)))
    }
    private def mockCustomerName(props) {
        if (boundingBoxes.customerName) {
            pickAny(boundingBoxes.customerName)
        } else {
            faker.company().name()
        }
    }
    private def mockNodeType(props) {
        pickAny(['fulfillment'])
    }
    private def mockSupplierName(props) {
        faker.company().name()
    }
    private def mockCustItemName(props) {
        if (boundingBoxes.custItemName) {
            pickAny(boundingBoxes.custItemName)
        } else {
            faker.commerce().productName()
        }
    }
    private def mockCustItemOwnerName(props) {
        faker.company().name()
    }
    private def mockSuppItemName(props) {
        faker.commerce().productName()
    }
    private def mockSuppItemOwnerName(props) {
        faker.company().name()
    }
    private def mockShipToSiteName(props) {
        mockSiteName()
    }
    private def mockShipToSiteOwnerName(props) {
        faker.company().name()
    }
    private def mockShipFromSiteName(props) {
        mockSiteName()
    }
    private def mockShipFromSiteOwnerName(props) {
        faker.company().name()
    }
    private def mockStatus(props) {
        pickAny(['lost', 'found', 'in-limbo'])
    }
    private def mockLob(props) {
        faker.company().industry()
    }
    private def mockSolutionType(props) {
        faker.lorem().words(2).join(' ')
    }
    private void mockMeasures(def node, props) {
        def creationDate = node.creationDate
        props.measures.each {
            measure ->
                node.measures[measure] = (1..props.measureLengthDays).collect {
                    dayNo ->
                        def tsd = new Tsd()
                        tsd.time = new Date(creationDate.time + DAYS.toMillis(dayNo as Long))
                        def quantity = random.nextDouble() * props.maxQuantity
                        tsd.quantity = Math.round(quantity * 100.0) / 100.0
                        //tsd.attributes = mockAttributes(props.numAttrPerTsd)
                        tsd
                }
        }
    }

    private void mergeWithDefaults(Map props) {
        DEFAULTS.entrySet().each {
            entry ->
                if (!(entry.key in props)) {
                    props[entry.key] = entry.value
                }
        }
    }
}
