package com.jda.dct.e2e.api.items

import com.fasterxml.jackson.databind.ObjectMapper
import com.jda.dct.domain.Item
import com.jda.dct.e2e.api.categories.Regression
import com.jda.dct.e2e.api.categories.Smoke
import com.jda.dct.e2e.api.utility.IngestionHelper
import groovy.json.JsonOutput
import groovy.json.JsonSlurper
import org.junit.experimental.categories.Category
import org.springframework.http.HttpMethod
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Stepwise
import spock.util.concurrent.PollingConditions

/**
 * End-to-end tests for {@code Item} ingestion.
 *
 * @version 1.0-SNAPSHOT
 * @since 01-June-2018
 */
@Stepwise
class UpdateItemsSpec extends Specification implements IngestionHelper {

    @Shared ItemShowcaseData sample = new ItemShowcaseData()
    @Shared Map<String, String> modifiedFields = new HashMap<>()
    @Shared Random random = new Random()

    def setupSpec() {
        init()

        def itemPath = properties.getProperty('ingestItemsPath')
        def getPath = properties.getProperty('getItemsPath')

        ingestionURL = protocol + ip + itemPath
        getURLPrefix = protocol + ip + getPath

        sampleItem = this.getClass().getClassLoader().getResource('sample-item-1.json').text
        sampleItem = sampleItem.replace('ItemName1','ItemName1' + (new Date()).getTime())
    }


    @Category(Smoke)
    def "Add a Note to a Item and get it back"() {
        given:
        def conditions = new PollingConditions(timeout: 120, initialDelay: 1)
        conditions.setDelay(10)

        when: 'A Note is added to the Item'
        def res
        conditions.eventually {
            res = executeHttpCall(header, getURLPrefix, null, HttpMethod.GET)
            assert res != null
        }
        def itemId = res['data'][0]['id']
        def url = getURLPrefix + 'notes/' +  itemId
        def sampleNote = '{ "note": "Item note added"}'
        def response = getHttpClient(header, url, sampleNote, HttpMethod.POST)
        assert response.status == 200

        and: "Get the Item "
        url = getURLPrefix + itemId
        def itemText = getHttpClient(header, url, null, HttpMethod.GET).body
        def itemRetrieved = new JsonSlurper().parseText(itemText).get('data').get(0)

        then: 'Get the Item  back and verify the note'
        assert itemRetrieved.getAt('notes').toString().contains("Item note added")
    }

    // evaluate function and rename for clarity
    @Category(Regression)
    def "field in process model updates upon a new ingest"() {
        given: 'original item from showcase'
        String itemDescription = "temp item description" + random.nextInt(100).toString()
        modifiedFields.clear()
        modifiedFields.put("itemDescription", itemDescription)
        def updatedItemList = sample.modifyItemMultiFields("AC-FG-001", modifiedFields, "temp-item.json")

        when: 'ingest updated item'
        ingestData(updatedItemList, "items?itemType=Sold")
        def item = sample.getItemFromShowcase("AC-FG-001")

        then: 'the process model field updated'
        item['itemDescription'] == itemDescription
    }
    // evaluate what this means and rename?? or be able to explain it
    //not sure if this test case is valid or not
    /*@Category(Regression)
    def "field not in process model not updated upon a new ingest"() {
        given: 'original item ingested'
        ingestData(sampleItem)
        def itemIds = getItemIds(sampleItem)
        def url = getURLPrefix + itemIds[0]

        when: 'ingest updated item'
        def modifiedSampleItem = sampleItem.replaceAll("\"flexFloat1\": 123.45", "\"flexFloat1\": 987.65")
        def expectedItem = getJsonAt(sampleItem, 0)

        ingestData(modifiedSampleItem)

        def itemText = getHttpClient(header, url, null, HttpMethod.GET).body
        def itemRetrieved = new JsonSlurper().parseText(itemText).get('data').get(0)
        ['creationDate', 'id', 'lmd'].each { itemRetrieved.remove(it) }

        def mapper = new ObjectMapper()
        def expected = mapper.readValue(JsonOutput.toJson(expectedItem), Item)
        def received = mapper.readValue(JsonOutput.toJson(itemRetrieved), Item)

        then: 'non-process model field not updated'

        System.out.println("Expected: " + expected.toString())
        System.out.println("Actual: " + received.toString())
        received == expected
    }*/

}
