package com.jda.dct.e2e.ui.utility.pageobjects;

import com.jda.dct.e2e.ui.utility.common.BasePage;
import com.jda.dct.e2e.ui.utility.helper.Wait.WaitHelper;
import com.jda.dct.e2e.ui.utility.helper.genericHelper.GenericHelper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.util.List;


public class DashboardPage extends BasePage {

    WebDriver driver;
    WaitHelper objWaitHelper;
    GenericHelper objGenericHelper;


    public DashboardPage(WebDriver webdriver) {
        this.driver = webdriver;
        PageFactory.initElements(driver, this);
        objWaitHelper = PageFactory.initElements(driver, WaitHelper.class);
        objGenericHelper = PageFactory.initElements(driver, GenericHelper.class);
    }

    //pageobjects
    @FindBy(xpath = "//button[@aria-label='Menu']")
    private WebElement dashboardMap;
    @FindBy(xpath = "//button[@aria-label='Menu']") // Change to leftmenue Jan
    private WebElement menuViewIcon;
    @FindBy(xpath = "//span[text()='DASHBOARD']")
    private WebElement selectDashBoardTabFromDisplayedMenu; // leftmenu
    @FindBy(xpath = "//div[contains(@class,'AppFrame')]/div/div/div/div/div/div/button")
    private WebElement globalFilterIcon;//global filter
    //div[@style='border-radius: 0px 4px 4px 0px; background-color: rgb(216, 228, 235);'] // Gridview
    //@FindBy(xpath = "//button[@tabindex='0' and @style='width: 40px; height: 40px;']/parent::div/following-sibling::div")
    @FindBy(xpath = "(//*[@role=\"presentation\"])[9]")
    private WebElement gridViewIcon; //grid view icon
    @FindBy(xpath = "//input[@placeholder='Search' and @type='text']")
    private WebElement dashBoardSearchBox; // dash boardsearchbox
    //*[@style='color: rgb(216, 228, 235);' and @viewBox='0 0 24 24'] // map view
    @FindBy(xpath = "//button[contains(@class,'jss')]/parent::div/following-sibling::div/following-sibling::div/div/div[1]")
    private WebElement mapViewSwitch; // Map ICON
    @FindBy(xpath = "//div/p[text()='Filters']")
    private WebElement dashBoardFilterLabel;
    @FindBy(xpath = "//*[@aria-hidden='true']/*[@d='M3 17v2h6v-2H3zM3 5v2h10V5H3zm10 16v-2h8v-2h-8v-2h-2v6h2zM7 9v2H3v2h4v2h2V9H7zm14 4v-2H11v2h10zm-6-4h2V7h4V5h-4V3h-2v6z']")
    private WebElement filterIcon;
    @FindBy(xpath = "(//div[@id='MuiSelect_control']/div/div/div/input)")
    private WebElement itemSearchBox;
    // @FindBy(xpath ="(.//*[normalize-space(text()) and normalize-space(.)='Search'])[1]/following::div[14]")
    @FindBy(xpath = "(//div[@id='MuiSelect_control']/div/div/div/input)[2]")
    private WebElement siteSearchBox;
    @FindBy(xpath = "//div[@style='display: block; height: 24px;']") //evaluate to delete
    private WebElement dashBoardLabel;//evaluate to delete
    @FindBy(xpath = "//div/p[text()='Filters']")//evaluate to delete
    private WebElement filterLabel;//evaluate to delete
    // uzma
    @FindBy(xpath = "//div[@style='display: block; height: 24px;']")
    private WebElement footerDisplayed; //enterItemToSearch
    @FindBy(xpath = "//h1/following-sibling::h1")
    private WebElement inboundShipmentTotal;
    @FindBy(xpath = "//div[@aria-hidden='false']")
    private WebElement enterItemToSearch; //use one enterItemToSearch
    @FindBy(xpath = "//div[@aria-hidden='false']//h1[contains(text(),'Shipments')]")
    private WebElement shipment; // use from left men
//    @FindBy(css = "#app > div > div.App__AppWrapper-jqrDvk.dTKlbn > div.AppFrame-jkyAsh.jqOhkH > div > div:nth-child(3) > div > div > div > div.slick-slide.slick-active.slick-current > div > div > div > h1")
    @FindBy(xpath = "//*[@id='app']//div[@class = 'slick-track']//h5[contains(text(),'Inventory ')]")
    private WebElement inventoryException;
    //@FindBy(xpath = "//*[@id=\"app\"]/div/div[2]/div[3]/div/div[1]/div/div[1]/div/div[2]")
    @FindBy(xpath = "//*[@id='app']//input[@placeholder='Search'][@type='text']")
    private WebElement itemInputField;
    @FindBy(xpath = "//*[@id='undefined-Card-On Time']")
    private WebElement onTimeShipment;
    // NEW
    //@FindBy(xpath = "//div[@data-index='0' and @aria-hidden='false']// aside[contains(text(),'PTA ')]")
    @FindBy(xpath = "//*[@id='undefined-Card-PTA Unknown']")
    private WebElement PTAUnknown;
    //    @FindBy(xpath = "//div[@style='padding: 0px 0px 10px; border: none; background-color: rgb(25, 48, 57); border-radius: 3px; position: relative;' and @class='jss45 jss67 jss51 jss53 jss64']/parent::div/parent::div/parent::div/parent::div/parent::div/parent::div/following-sibling::div/following-sibling::div/following-sibling::div/following-sibling::div/div/div/div/div/div/div/div[3]")
    @FindBy(xpath = "//*[@id='undefined-Card-Overstock']")
    private WebElement overStockShipment;
    //@FindBy(xpath = "//h1[@style='color: rgb(219, 114, 224); width: inherit; height: inherit;']/div/following-sibling::span")
    @FindBy(xpath = "//div[@style='padding: 0px 0px 10px; border: none; background-color: rgb(25, 48, 57); border-radius: 3px; position: relative;' and @class='jss45 jss67 jss51 jss53 jss64']/parent::div/parent::div/parent::div/parent::div/parent::div/parent::div/following-sibling::div/following-sibling::div/following-sibling::div/following-sibling::div/div/div/div/div/div/div/div[3]/div/h1/div/aside[text()='Overstock']")
    private WebElement overStockShipmentValue;
    @FindBy(xpath = "//*[@id='undefined-Card-Early']")
    private WebElement earlyShipment;
    @FindBy(xpath = "//*[@class='slick-arrow slick-next']")
    private WebElement footSlidingArrow;
    @FindBy(xpath = "//*[@id='undefined-Card-Low Stock']")
    private WebElement lowStock;

    //@FindBy(xpath = "//aside[contains(text(),'Low Stock')]")
    //h1/div/aside[text()='Low Stock']
    @FindBy(xpath = "//aside[text()='Delayed']/parent::div/following-sibling::span")
    private WebElement gotoLowStockShipmentValue;
    @FindBy(xpath = "//h1[@class='jss463 jss468 jss481 jss477' and text()='Locations with Inventory Exceptions']")
    private WebElement locationsWithInventoryExceptions;
    //StockedOut
    @FindBy(xpath = "//*[@id='undefined-Card-Stockout']")
    private WebElement stockedOut;
    @FindBy(xpath = "//aside[text()='Delayed']/parent::div/following-sibling::span")
    private WebElement gotoStockedOutShipmentValue;

    @FindBy(xpath = "//div[contains(@class,'exanding-search-box-select__menu')]")
    private WebElement autoSuggestionList;
    @FindBy(xpath = "//li[@role=\"option\"]")
    public List<WebElement> autoSuggestionListDisplay;

    @FindBy(xpath = "//a[@href='/inventory?materialType=Finished Goods&status=Low Stock']")
    //@FindBy(xpath = "//*[contains (text(),\"Low Stock\")]")
    private WebElement dashboardInventoryLowStock;
    @FindBy(xpath = "//a[@href='/inventory?materialType=Finished Goods&status=Overstock']")
    private WebElement dashboardInventoryOverStock;
    @FindBy(xpath ="//a[@href='/inventory?materialType=Finished Goods&status=Stockout']")
    private WebElement dashBoardInventoryStockOut;
    @FindBy(xpath = "(//*[@text-anchor='middle'])[12]")
    private WebElement exceptionValue;
    @FindBy(xpath = "//*[@id=\"demandSupply-inventory-Filter-List-Card-Low Stock\"]//span")
    private WebElement listlowstockValue;
    @FindBy(xpath = "//*[@id=\"demandSupply-inventory-Filter-List-Card-Overstock\"]//span")
    private WebElement listOverStockValue;
    @FindBy(xpath = "//*[@id=\"demandSupply-inventory-Filter-List-Card-Stockout\"]//span")
    private WebElement listStockOutValue;
    @FindBy(xpath = "//*[@href='/inventory?materialType=Finished Goods&status=Low Stock']/div/h6")
    private WebElement lowstockValue;
    @FindBy(xpath = "//*[@href=\"/inventory?materialType=Finished Goods&status=Overstock\"]/div/h6")
    private WebElement overstockValue;
    @FindBy(xpath = "//*[@href=\"/inventory?materialType=Finished Goods&status=Stockout\"]/div/h6")
    private WebElement stockoutValue;
    //@FindBy(xpath = "//*[@class='react-grid-item widgetStyles react-draggable cssTransforms react-resizable'][3]")
    @FindBy(css="div[class='css-1dj54gm']")

    private WebElement inventoryWidget;



    public boolean isInventoryWidgetDisplayed() {
        return objGenericHelper.isDisplayed(inventoryWidget, "InventoryWidgetisdisplayed");
    }


    public boolean isfilterLabelDisplayed() {
        return objGenericHelper.isDisplayed(filterLabel, "Filter Label");
    }

    public boolean isFooterDisplayed() {
        return objGenericHelper.isDisplayed(enterItemToSearch, "Enter Item to Search");
    }

    public boolean isShipmentDisplayed() {
        return objGenericHelper.isDisplayed(shipment, "Shipment");
    }

    public boolean isPTAUnknownDisplayed() {
        return objGenericHelper.isDisplayed(PTAUnknown, "PTA Unknown");
    }


    public boolean isInventoryExceptionDisplayed() {
        objWaitHelper.waitElementToBeVisible(inventoryException);
        return objGenericHelper.isDisplayed(inventoryException, "Inventory Exception");
    }


    public boolean isSearchBarDisplayed() {
        return objGenericHelper.isDisplayed(itemInputField, "Item Input Field");
    }


    public boolean isStockedOutShipmentDisplayed() {
        return objGenericHelper.isDisplayed(stockedOut, "Stocked Out");
    }

    public boolean isOnTimeShipmentDisplayed() {
        return objGenericHelper.isDisplayed(onTimeShipment, "On Time Shipment");
    }

    public boolean isOverStockShipmentDisplayed() {
        return objGenericHelper.isDisplayed(overStockShipment, "OverStock Shipment");
    }


    public boolean isLowStockShipmentDisplayed() {
        return objGenericHelper.isDisplayed(lowStock, "Low Stock");
    }


    public boolean isEarlyShipmentDisplayed() {
        return objGenericHelper.isDisplayed(earlyShipment, "Early shipment");
    }


    public boolean iSDashBoardLabelDisplayed() {
        return objGenericHelper.isDisplayed(dashBoardLabel, "Dashboard Label");
    }

    public boolean iSIsPTAUnknownDisplayed() {
        return objGenericHelper.isDisplayed(PTAUnknown, "PTA Unknown");
    }


    public void goToFootSlidingArrow() {
        objWaitHelper.waitElementToBeClickable(menuViewIcon);
        objWaitHelper.waitForPageToLoad();
        objGenericHelper.elementClick(footSlidingArrow, "Foot Sliding Arrow");
    }
//    public void goToIsPTAUnknownDisplayed() {
//        webDriverWait.until(ExpectedConditions.elementToBeClickable(menuViewIcon));
//        PTAUnknown.click();
//    }


    public void goToMenuViewIcon() {
        objWaitHelper.waitForPageToLoad();
        objWaitHelper.waitElementToBeClickable(menuViewIcon);
        objGenericHelper.elementClick(menuViewIcon, "Menu view Icon");
    }

    public void goToSelectTabFromDisplayedMenu() {
        objWaitHelper.waitForPageToLoad();
        objWaitHelper.waitElementToBeClickable(selectDashBoardTabFromDisplayedMenu);
        objGenericHelper.elementClick(selectDashBoardTabFromDisplayedMenu, "Select Dashboard Tab from displayed menu");
    }

    public void goToGlobalFilterIcon() {
        objWaitHelper.waitElementToBeClickable(globalFilterIcon);
        objWaitHelper.waitForPageToLoad();
        objGenericHelper.elementClick(globalFilterIcon, "Gobal Filter Icon");
    }

    public void clickGridViewIcon() {
        objWaitHelper.waitElementToBeClickable(gridViewIcon);
        objWaitHelper.waitForPageToLoad();
        objGenericHelper.elementClick(gridViewIcon, "Grid View Icon");
    }

    public void goToSearchBoxField() {
        objWaitHelper.waitElementToBeClickable(dashBoardSearchBox);
        objGenericHelper.setElementText(dashBoardSearchBox, "Dashboard Search box","JDA" );
    }

    public void goToMapView() {
        objWaitHelper.waitElementToBeClickable(mapViewSwitch);
        objWaitHelper.waitForPageToLoad();
        objGenericHelper.elementClick(mapViewSwitch, "Map view switch");
    }

//    public void goToOverStockShipmentValue() {
//        webDriverWait.until(ExpectedConditions.elementToBeClickable(overStockShipment));
//        overStockShipmentValue.click();
//    }

    @FindBy(xpath = "//div[contains(@class,'exanding-search-box-select__menu')]")
    private WebElement autoSuggestionDisplayBox;


    public void gotoDashBoardFilterLabel() {
        objWaitHelper.waitAndClickElement(dashBoardFilterLabel, "Dashboard filter label");
    }

    public void assertLowStock() {
        String exp = lowstockValue.getText();
        objWaitHelper.waitAndClickElement(dashboardInventoryLowStock, "Inventorylowstockvaluematches");
    //navigate to list page
        String act = listlowstockValue.getText();
        //validation low stock validation page
        Assert.assertEquals(act,exp);
    }

    public void assertOverStock() {
        String actual = overstockValue.getText();
        objWaitHelper.waitAndClickElement(dashboardInventoryOverStock, "Inventoryoverstockvaluematches");
        //navigate to list page
        String exp = listOverStockValue.getText();
        //validation low stock validation page
        Assert.assertEquals(actual,exp);
    }

    public void assertStockOut() {
        String actual = stockoutValue.getText();
        objWaitHelper.waitAndClickElement(dashBoardInventoryStockOut, "InventoryStockoutValuematches");
        //navigate to list page
        String exp = listStockOutValue.getText();
        //validation low stock validation page
        Assert.assertEquals(actual,exp);
    }

    public void clickLowStock() {
        objWaitHelper.waitAndClickElement(dashboardInventoryLowStock, "inventorylowstockisclicked");
    }

    public boolean isLowStockDisplayed() {
        return objGenericHelper.isDisplayed(dashboardInventoryLowStock, "InventoryLOwStockisDisplayed");
    }
    public void clickOverStock() {
        objWaitHelper.waitAndClickElement(dashboardInventoryOverStock, "InventoryOverStockisclicked");
    }
    public boolean isOverStockDisplayed() {
        return objGenericHelper.isDisplayed(dashboardInventoryOverStock, "inventoryOverStockisdisplayed");
    }

    public void clickStockOut() {
        objWaitHelper.waitAndClickElement(dashboardInventoryOverStock,"inventoryStockOutisclciked");
    }

    public boolean isStockOutDisplayed() {
        return objGenericHelper.isDisplayed(dashBoardInventoryStockOut, "InventoryStockOutisdisplayed");
    }


public void gotoLowStockValue(){
objWaitHelper.waitElementToBeVisible(lowstockValue);
}
public void OverStockCount()
{
objWaitHelper.waitElementToBeVisible(overstockValue);

}

public void StockoutValue (){
objWaitHelper.waitElementToBeVisible(stockoutValue);
}


    public void exceptionValue (){
        objWaitHelper.waitElementToBeVisible(exceptionValue);
    }

    public boolean isExceptionCountDisplayed() {
        return objGenericHelper.isDisplayed(exceptionValue, "exceptionvalueisdisplayed");
    }

        
    public boolean isLowStockValueDisplayed() {
        return objGenericHelper.isDisplayed(lowstockValue, "Lowstock is displayed");
    }
    public boolean isOverStockValueDisplayed() {
        return objGenericHelper.isDisplayed(overstockValue, "value");
    }
    public boolean isStockOutValueDisplayed() {
        return objGenericHelper.isDisplayed(stockoutValue, "value");
    }

    public boolean goToItemAutoSuggestionFeature(String item) throws InterruptedException {

        objWaitHelper.waitElementToBeVisible(itemSearchBox);
        objGenericHelper.setElementText(itemSearchBox, "Item search box", item);
        Thread.sleep(3000);
        objGenericHelper.elementClick(itemSearchBox, "Item search box");

        List<WebElement> autoList = autoSuggestionListDisplay;
        System.out.println("Size is" + autoList.size());
        if (autoList.size() == 0) {
            objGenericHelper.elementClick(itemSearchBox, "Item search box");

        }
        if (autoList.size() > 0) {
            for (WebElement auto : autoList) {
                if (!auto.getText().toLowerCase().contains(item.toLowerCase())) {
                   Reporter("Search String '" + item + "' is not found in displayed auto suggested results '" + auto.getText() + "'","Fail",log);
                    return false;
                }
            }
        }
        return true;
    }





    public boolean goToSiteAutoSuggestionFeature(String site) throws  InterruptedException {
        objWaitHelper.waitElementToBeVisible(siteSearchBox);
            objGenericHelper.setElementText(siteSearchBox, "Site search box", site);

            Thread.sleep(3000);
            objGenericHelper.elementClick(siteSearchBox,"Site Search box");
//get all the list stored in suto suggestion list
            List<WebElement> autoList = autoSuggestionListDisplay;
            System.out.println("Size is" + autoList.size()); //print the size of the list
 //if the size equals to zero then click on the entered item  and return true
        if (autoList.size() == 0) {
                siteSearchBox.click();
            }
//if the size is >0, then enter the for loop and search the list
           if (autoList.size() > 0) {
                for (WebElement auto : autoList) {
//if the autolist doesnot conatin the item entered display the msg and return false
                    if (!auto.getText().toLowerCase().contains(site.toLowerCase())) {
                      Reporter("Search String '" + site + "' is not found in displayed auto suggested results '" + auto.getText() + "'","Fail", log);
                        return false;
                    }
                }
            }
            return true;
        }


        public boolean isAutoSuggestionListDisplayed () {
            objWaitHelper.waitElementToBeVisible(autoSuggestionListDisplay.get(0));
            //get the item stored under index 0 to display
            return objGenericHelper.isDisplayed(autoSuggestionListDisplay.get(0),autoSuggestionListDisplay.get(0).toString()) ;

        }


     public void
     validateSum()   {
      int a = Integer.parseInt(lowstockValue.getText());
      int b = Integer.parseInt(overstockValue.getText());
      int c = Integer.parseInt(stockoutValue.getText());
      String d = exceptionValue.getText();//305/97%
 //split the string d to get 305
        String e[] = d.split("/");
         System.out.println(e[0]);
         //convert 305 to integer
         int act = Integer.parseInt(e[0].trim());

         int sum = a+b+c;

      Assert.assertEquals(act,sum);

     }



    }
