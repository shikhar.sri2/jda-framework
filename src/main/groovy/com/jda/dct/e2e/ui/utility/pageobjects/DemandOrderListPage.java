package com.jda.dct.e2e.ui.utility.pageobjects;

import com.jda.dct.e2e.ui.utility.common.BasePage;
import com.jda.dct.e2e.ui.utility.helper.Wait.WaitHelper;
import com.jda.dct.e2e.ui.utility.helper.genericHelper.GenericHelper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DemandOrderListPage extends BasePage {
    WebDriver driver;
    WebDriverWait webDriverWait;
    WaitHelper objWaitHelper;
    GenericHelper objGenericHelper;

    public DemandOrderListPage(WebDriver webdriver) {

        this.driver = webdriver;
        PageFactory.initElements(driver, this);
        webDriverWait = new WebDriverWait(driver, getTimeOutInSeconds());
        objWaitHelper = PageFactory.initElements(driver, WaitHelper.class);
        objGenericHelper = PageFactory.initElements(driver, GenericHelper.class);
    }
    //Deliveries tab related data elements

    @FindBy(xpath = "//*[@id='PanelSummary-RenderedItem-Order-No-1-LinkItem']")
    private WebElement demandOrderListPage;
    @FindBy(xpath = "//*[contains(@id , 'sales-order details')]")
    private WebElement demandSalesOrderDetailsPage;

    @FindBy(xpath = "//*[@id='PanelSummary-RenderedItem-Order-No-1-LinkItem']")
    private WebElement demandOrderNumber;
    @FindBy(xpath = "//a[@href = '/demand/sales-orders']")
    private WebElement backToDemandOrdersTab;
    @FindBy(xpath="//*[@id='PanelSummary-RenderedItem-Notes-1-NotesItem']/a")
    private WebElement orderNotes;
    @FindBy(xpath = "//*[@id='notesDrawer']//span[text()='Notes']")
    private WebElement notesTitle;
    @FindBy(id = "ExpansionPanel-ExpandMoreIcon")
    private WebElement expandButton;


    //================ Methods========================


    //Assertion methods

    public boolean isDemandOrderListPageDisplayed() {
        objWaitHelper.waitElementToBeVisible(demandOrderListPage);
        return objGenericHelper.isDisplayed(demandOrderListPage, "Order List page");
    }

    public boolean isDemandSalesOrderDetailsPageDisplayed() {
        webDriverWait.until(ExpectedConditions.visibilityOf(demandSalesOrderDetailsPage));
        return demandSalesOrderDetailsPage.isDisplayed();
    }

    public void clickOnOrderNumber() {
        objWaitHelper.waitAndClickElement(demandOrderNumber, "Demand Order number");
    }


    public void clickBackToDemandOrdersTab() {
        objWaitHelper.waitElementToBeVisible(backToDemandOrdersTab);
        objGenericHelper.elementClick(backToDemandOrdersTab, "Demand orders link");
    }

    public void clickOrderNotes() {
        objWaitHelper.waitAndClickElement(orderNotes, "Notes");
    }

    public void clickExpandButton() {
        objWaitHelper.waitElementToBeClickable(expandButton);
        objWaitHelper.waitAndClickElement(expandButton, "Expand button");
    }

    public boolean isNotesPageDisplayed() {
        objWaitHelper.waitElementToBeVisible(notesTitle);
        return objGenericHelper.isDisplayed(notesTitle, "Notes Title");
    }
}


