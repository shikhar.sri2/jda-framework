package com.jda.dct.e2e.ui.navigation;

import com.jda.dct.e2e.ui.utility.action.CommonActions;
import com.jda.dct.e2e.ui.utility.common.BaseTest;
import com.jda.dct.e2e.ui.utility.common.LeftMenu;
import com.jda.dct.e2e.ui.utility.helper.AssertHelper.AssertHelper;
import com.jda.dct.e2e.ui.utility.helper.Browser.BrowserHelper;
import com.jda.dct.e2e.ui.utility.pageobjects.*;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class SupplyOrderDetailsTests extends BaseTest {

    private LeftMenu leftMenu;
    private TopMenuPage topMenu;
    private TopSubMenuPage topSubMenu;
    private SupplyShipmentListPage supplyShipmentListPage;
    private SupplyShipmentDetailsPage supplyShipmentDetailsPage;
    private SupplyOrderListPage supplyOrderlistPage;
    private AssertHelper assertHelper;
    private BrowserHelper browserHelper;
    private InventoryListPage inventoryListPage;
    private CommonActions commonActions;
    private WebDriver driver;


    @BeforeClass
    public void setUp() throws InterruptedException {
        System.out.println("Starting tests for :" + this.getClass().getName());
        //Initialize required objects
        driver = getDriver();
        leftMenu = new LeftMenu(driver);
        topMenu = new TopMenuPage(driver);
        topSubMenu = new TopSubMenuPage(driver);
        supplyShipmentListPage = new SupplyShipmentListPage(driver);
        supplyShipmentDetailsPage = new SupplyShipmentDetailsPage(driver);
        supplyOrderlistPage = new SupplyOrderListPage(driver);
        inventoryListPage = new InventoryListPage(driver);
        assertHelper = new AssertHelper(driver);
        browserHelper = new BrowserHelper(driver);
        commonActions = new CommonActions(driver);
        //loginAndNavigateToSupplyLandingPage
        logInToApplication();
    }

    @Test(priority = 1,retryAnalyzer = BaseTest.class)
    public void purchaseOrderDetailPageIsDisplayed() {
        leftMenu.clickSupplyMenuOption();
        topMenu.goToSupplyOrdersTab();
        topMenu.clearFilterIfVisible();
        commonActions.disableAllExceptionFilter();
        supplyOrderlistPage.clickOrderNumber();
        assertHelper.assertTrue(supplyOrderlistPage.isSupplyPurchaseOrderDetailsPageDisplayed(), "Supply Purchase Order is displayed");
        supplyOrderlistPage.clickBackToSupplyOrdersTab();
        browserHelper.refresh();
    }


    @Test(priority = 2,retryAnalyzer = BaseTest.class)
    public void inventoryDetailsPageIsDisplayedWhenOrderItemIsClicked() {
        leftMenu.clickSupplyMenuOption();
        topMenu.goToSupplyOrdersTab();
        commonActions.disableAllExceptionFilter();
        supplyOrderlistPage.clickExpandButton();
        supplyOrderlistPage.clickOnOrderItem();
        assertHelper.assertTrue(supplyOrderlistPage.inventoryDetailPageIsDisplayed(), "Inventory details page is displayed successfully");
        /*        inventoryListPage.clickBackToInventoryPage();*/
        browserHelper.refresh();
    }

    @Test(priority = 3,retryAnalyzer = BaseTest.class)
    public void validateSupplyPurchaseOrderDetailsPageIsDisplayedWhenNotesIsClicked(){
        leftMenu.clickSupplyMenuOption();
        topMenu.goToSupplyOrdersTab();
        supplyOrderlistPage.clickExpandButton();
        supplyOrderlistPage.clickNotes();
        assertHelper.assertTrue(supplyOrderlistPage.isNotesPageDisplayed(), "Notes is displayed successfully");
        //supplyShipmentDetailsPage.clickBackToNotes();
        browserHelper.refresh();
        assertHelper.assertTrue(supplyOrderlistPage.isSupplyOrderListPageDisplayed(), "Supply Purchase Order page is displayed");

    }

}

