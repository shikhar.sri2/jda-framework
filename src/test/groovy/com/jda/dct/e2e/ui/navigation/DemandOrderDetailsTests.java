package com.jda.dct.e2e.ui.navigation;


import com.jda.dct.e2e.ui.utility.action.CommonActions;
import com.jda.dct.e2e.ui.utility.common.BaseTest;
import com.jda.dct.e2e.ui.utility.common.LeftMenu;
import com.jda.dct.e2e.ui.utility.helper.AssertHelper.AssertHelper;
import com.jda.dct.e2e.ui.utility.pageobjects.*;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


public class DemandOrderDetailsTests extends BaseTest {

    private LeftMenu leftMenu;
    private TopMenuPage topMenu;
    private TopSubMenuPage topSubMenu;
    private SupplyOrderListPage supplyOrderlistPage;
    private DemandShipmentListPage demandShipmentListPage;
    private DemandShipmentDetailsPage demandShipmentDetailsPage;
    private DemandOrderListPage demandOrderListPage;
    private AssertHelper assertHelper;
    private CommonActions commonActions;
    private WebDriver driver;


    @BeforeClass
    public void setUp() throws InterruptedException {
        System.out.println("Starting tests for :" + this.getClass().getName());
        //Initialize required objects
        driver = getDriver();
        leftMenu = new LeftMenu(driver);
        topMenu = new TopMenuPage(driver);
        topSubMenu = new TopSubMenuPage(driver);
        supplyOrderlistPage = new SupplyOrderListPage(driver);
        demandOrderListPage = new DemandOrderListPage(driver);
        assertHelper = new AssertHelper(driver);
        commonActions = new CommonActions(driver);
        //loginAndNavigateToDemandLandingPage
        logInToApplication();
    }


    @Test(priority = 1,retryAnalyzer = BaseTest.class)
    public void validateDemandOrderDetailPageIsDisplayed() throws InterruptedException {
        leftMenu.clickDemandMenuOption();
        topMenu.goToDemandOrdersTab();
        topMenu.clearFilterIfVisible();
        commonActions.disableAllExceptionFilter();
        demandOrderListPage.clickOnOrderNumber();
        assertHelper.assertTrue(demandOrderListPage.isDemandSalesOrderDetailsPageDisplayed(), "Demand Sales Order Page is displayed successfully");
        demandOrderListPage.clickBackToDemandOrdersTab();
    }


    @Test(priority = 2,retryAnalyzer = BaseTest.class)
    public void validateNoteWindowDisplayed() throws InterruptedException {
        leftMenu.clickDemandMenuOption();
        topMenu.goToDemandOrdersTab();
        commonActions.disableAllExceptionFilter();
        demandOrderListPage.clickExpandButton();
        demandOrderListPage.clickOrderNotes();
        Assert.assertTrue(demandOrderListPage.isNotesPageDisplayed());
        //demandShipmentDetailsPage.clickBackToNotes();
    }
}


