package com.jda.dct.e2e.api


import com.jda.dct.e2e.api.utility.IngestionHelper
import groovy.json.JsonSlurper
import org.springframework.http.HttpMethod
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll
import spock.util.concurrent.PollingConditions

class CleanupSpec extends Specification implements IngestionHelper{

    @Shared deliveriesUrl
    @Shared shipmentsUrl
    @Shared nodesUrl
    @Shared sitesUrl
    @Shared purchaseOrdersUrl
    @Shared salesOrdersUrl
    @Shared participantsUrl
    @Shared itemsUrl
    @Shared alertsUrl
    @Shared receiptsUrl
    @Shared sourcingLaneUrl
    @Shared batchesUrl
    @Shared calendarsUrl
    @Shared bomsUrl
    @Shared indexUrl
    @Shared tenantIdDash

    @Shared def client

    def setupSpec() {
        init()

        tenantIdDash = "a4b7a825f67930965747445709011120-"

        def deliveryPath = properties.getProperty('getDeliveryPath')
        deliveriesUrl = protocol + ip + deliveryPath.substring(0, deliveryPath.lastIndexOf("/"))

        def shipmentPath = properties.getProperty('getShipmentsPath')
        shipmentsUrl = protocol + ip + shipmentPath.substring(0, shipmentPath.lastIndexOf("/"))

        def nodePath = properties.getProperty('nodeGetPath')
        nodesUrl = protocol + ip + nodePath.substring(0, nodePath.lastIndexOf("/"))

        def sitePath = properties.getProperty('getSitesPath')
        sitesUrl = protocol + ip + sitePath.substring(0, sitePath.lastIndexOf("/"))

        def poPath = properties.getProperty('getPurchaseOrderPath')
        purchaseOrdersUrl = protocol + ip + poPath.substring(0, poPath.lastIndexOf("/"))

        def soPath = properties.getProperty('getSalesOrderPath')
        salesOrdersUrl = protocol + ip + soPath.substring(0, soPath.lastIndexOf("/"))

        def participantsPath = properties.getProperty("getParticipantsPath")
        participantsUrl = protocol + ip + participantsPath.substring(0, participantsPath.lastIndexOf("/"))

        def batchesPath = properties.getProperty("getBatchesPath")
        batchesUrl = protocol + ip + batchesPath.substring(0, batchesPath.lastIndexOf("/"))

        def calendarsPath = properties.getProperty("getCalendarsPath")
        calendarsUrl = protocol + ip + calendarsPath.substring(0, calendarsPath.lastIndexOf("/"))

        def itemsPath = properties.getProperty("getItemsPath")
        itemsUrl = protocol + ip + itemsPath.substring(0, itemsPath.lastIndexOf("/"))

        def sourcingLanesPath = properties.getProperty("getSourcingLanesPath")
        sourcingLaneUrl = protocol + ip + sourcingLanesPath.substring(0, sourcingLanesPath.lastIndexOf("/"))

        def bomsPath = properties.getProperty("getBomsPath")
        bomsUrl = protocol + ip + bomsPath.substring(0, bomsPath.lastIndexOf("/"))

        def alertsPath = properties.getProperty('getAlertsPath')
        alertsUrl = protocol + ip + alertsPath.substring(0, alertsPath.lastIndexOf("/"))

        def receiptsPath = properties.getProperty('getReceiptsPath')
        receiptsUrl = protocol + ip + receiptsPath.substring(0, receiptsPath.lastIndexOf("/"))

        indexUrl = protocol + ip + "/api/v1/elasticsearch/_cat/indices?v"
    }

    @Unroll
    def "Deleting #type from ignite cache"() {
        given: 'provided searchCriteria'
        final Map<String,String> searchCriteria = new HashMap<>()
        searchCriteria.put("limit", "0")

        when: "Delete data from ignite cache"
        println "\n" + type.toUpperCase() +"\nDelete data from ignite cache"
        String afterDeleteResponse
        String indexes = getHttpClient(header, indexUrl, null, HttpMethod.GET).body
        if (indexes.contains(tenantIdDash + object)) {
            executeHttpCall(header, url, null, HttpMethod.DELETE)
            afterDeleteResponse =
                    searchUntil(searchCriteria, url, "count=", 0,120)
        } else {
            println(type + " data not founded")
            afterDeleteResponse = "count=0"
        }
        println(afterDeleteResponse)

        then: 'After delete, count=0'
        afterDeleteResponse.contains("count=0")

        where:
        type                 | url                  | object
        "deliveries"         | deliveriesUrl        | "delivery"
        "shipments"          | shipmentsUrl         | "shipment"
        "nodes and measures" | nodesUrl             | "node"
        "sites"              | sitesUrl             | "site"
        "purchase orders"    | purchaseOrdersUrl    | "purchaseorder"
        "sales order"        | salesOrdersUrl       | "salesorder"
        "items"              | itemsUrl             | "item"
        "participants"       | participantsUrl      | "participant"
        "batches"            | batchesUrl           | "batch"
        "calendars"          | calendarsUrl         | "calendar"
        "alerts"             | alertsUrl            | "alert"
        "sourcingLanes"      | sourcingLaneUrl      | "sourcinglane"
        "boms"               | bomsUrl              | "bom"
        "receipts"           | receiptsUrl          | "receipt"
    }

    @Unroll
    def "Deleting #type docs"() {
        given: "provided query json in post call body"
        def jsonSlurper = new JsonSlurper()
        def data = jsonSlurper.parseText('{\n' +
                '  "query": {\n' +
                '    "match_all": {}\n' +
                '  }\n' +
                '}')
        def conditions = new PollingConditions(timeout: 10, initialDelay: 1)
        String indexes = getHttpClient(header, indexUrl, null, HttpMethod.GET).body
        boolean indexIsExist = indexes.contains(tenantIdDash + object)
        String url1 = protocol + ip + "/api/v1/elasticsearch/" + tenantIdDash + object + "/_delete_by_query?conflicts=proceed"

        when: "Delete docs in every index"
        println "\n" + type.toUpperCase()
        if (indexIsExist) {
            executeHttpCall(header, url1, data, HttpMethod.POST)
        } else {
            println(object + " index not founded")
        }

        then: 'docs.count == 0'
        if (indexIsExist) {
            String[][] curRes = null
            conditions.eventually {
                curRes = getCurrentIdxDetails(indexUrl, tenantIdDash, object)
                println(curRes[0].toString() + "\n" + curRes[1].toString())
                assert curRes[1][6] == '0'
            }
        } else {
            assert true
        }

        where:
        type                 | url                  | object
        "deliveries"         | deliveriesUrl        | "delivery"
        "shipments"          | shipmentsUrl         | "shipment"
        "nodes and measures" | nodesUrl             | "node"
        "sites"              | sitesUrl             | "site"
        "purchase orders"    | purchaseOrdersUrl    | "purchaseorder"
        "sales order"        | salesOrdersUrl       | "salesorder"
        "items"              | itemsUrl             | "item"
        "participants"       | participantsUrl      | "participant"
        "batches"            | batchesUrl           | "batch"
        "calendars"          | calendarsUrl         | "calendar"
        "alerts"             | alertsUrl            | "alert"
        "sourcingLanes"      | sourcingLaneUrl      | "sourcinglane"
        "boms"               | bomsUrl              | "bom"
        "receipts"           | receiptsUrl          | "receipt"
    }
}
