//package com.jda.dct.e2e.clientUi.pages.tempraturecontrolled
//
//import geb.Page
//
//class SupplyShipmentDetailsPage extends Page{
//    static atCheckWaiting = true
//
//    static at = {
//        $("#shipment-detail-title").text() != ""
//    }
//
//    static content = {
//        container1101AllItems(wait: true) {
//            $("#Shipments-Panel-Cell").find( role:"button", 'a[href$="shipmentLineId=10"]')
//        }
//
//        firstContainer(wait: true) {
//            $("a", href:contains("tempraturecontrolled/delivery?containerNumber")).first()
//        }
//        allItems(wait: true) {
//            $("a", href:contains("tempraturecontrolled/inventory?shipmentId")).first()
//        }
//    }
//
//    def clickFirstContainer(){
//        waitFor {
//            firstContainer.displayed
//        }
//        firstContainer.click()
//    }
//
//    def clickAllItemsIn1101() {
//        waitFor {
//            container1101AllItems.displayed
//        }
//        container1101AllItems.click()
//    }
//
//    def clickAllItemsOnTop() {
//        waitFor {
//            allItems.displayed
//        }
//        allItems.click()
//    }
//}
