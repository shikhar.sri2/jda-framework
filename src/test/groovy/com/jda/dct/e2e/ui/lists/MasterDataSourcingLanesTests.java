package com.jda.dct.e2e.ui.lists;


import com.jda.dct.e2e.ui.utility.common.BaseTest;
import com.jda.dct.e2e.ui.utility.common.LeftMenu;
import com.jda.dct.e2e.ui.utility.pageobjects.MasterDataSourcingLanesPage;
import com.jda.dct.e2e.ui.utility.pageobjects.TopMenuPage;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class MasterDataSourcingLanesTests extends BaseTest {

    private LeftMenu leftMenu;
    private TopMenuPage topMenu;
    private WebDriver driver;
    private MasterDataSourcingLanesPage sourcingLanesListPage;


    @BeforeClass
    public void setUp() throws InterruptedException {
        System.out.println("Starting tests for :" + this.getClass().getName());
        //Initialize required objects
        driver = getDriver();
        leftMenu = new LeftMenu(driver);
        topMenu = new TopMenuPage(driver);
        sourcingLanesListPage = new MasterDataSourcingLanesPage(driver);
        //loginAndNavigateToMasterDataLandingPage
        logInToApplication();
    }

    @BeforeMethod
    public void commonMethods() throws InterruptedException {
        leftMenu.clickMasterDataMenuOption();
    }

    @Test(priority = 1,retryAnalyzer = BaseTest.class)
    public void validateMasterDataSourcingLanesListPageIsDisplayed() {
        //navigates to Master-Data Item Tab
        topMenu.goToMasterDataSourcingLanesTab();
        //verify user is at MasterData Sourcing Lanes List Page
       Assert.assertTrue(sourcingLanesListPage.isSourcingLanesListPageDisplayed());
    }

@Test (priority =2,retryAnalyzer = BaseTest.class)
    public void validateMasterDataSourcingLanesISDisplayedWhenGridButtonIsClicked(){
       topMenu.goToMasterDataSourcingLanesTab();
       sourcingLanesListPage.clickSourcingGridButton();
       sourcingLanesListPage.clicksourcingmasterDataButton();
       Assert.assertTrue(sourcingLanesListPage.isSourcingLanesSummaryPaneDisplayed());
    }

}