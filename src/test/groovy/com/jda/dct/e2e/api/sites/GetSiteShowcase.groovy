package com.jda.dct.e2e.api.sites

import com.jda.dct.e2e.api.utility.IngestionHelper
import net.sf.json.JSONArray
import net.sf.json.JSONObject
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.HttpMethod
import spock.util.concurrent.PollingConditions

class GetSiteShowcase implements IngestionHelper {

    private final Logger LOGGER = LoggerFactory.getLogger(GetSiteShowcase.class)

    GetSiteShowcase() {

    }

    def getSitesFromShowcase(String siteName) {
        init()
        def getPath = properties.getProperty('getSitesPath')
        def getURLPrefix = protocol + ip + getPath
        def conditions = new PollingConditions(timeout: 15, initialDelay: 1)
        def res = null
        conditions.eventually {
            res = executeHttpCall(header, getURLPrefix + "?query=siteName=" + siteName, null, HttpMethod.GET)
            assert res != null
        }
        if (res['data'] == null || res['data'].size() == 0) {
            LOGGER.warn("getDeliveryFromShowcase with siteName: {}, data return null", siteName)
        }
        /*def delivery = res['data'][0]*/
        return res['data']
    }
}
