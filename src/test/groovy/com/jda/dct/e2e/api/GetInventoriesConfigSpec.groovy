package com.jda.dct.e2e.api

import com.jda.dct.e2e.api.utility.AuthHelper
import groovyx.net.http.RESTClient
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll

/**
 * GET method to retrieve demand inventory list configuration
 *
 */

// may be redundant, check and delete on confirmation
class GetInventoriesConfigSpec extends Specification implements AuthHelper {
    static inventoriesConfigURL
    @Shared
    def client

    def setupSpec() {
        init()

        def inventoriesConfigPath = properties.getProperty('getInventoriesConfigPath')
        inventoriesConfigURL = protocol + ip + inventoriesConfigPath
        client = new RESTClient(inventoriesConfigURL)
        client.handler.failure = { resp -> resp }
        header[HttpHeaders.CONTENT_TYPE] = MediaType.APPLICATION_JSON
    }

    @Unroll
    def "Get inventories configuration API #url"() {
        given: "The API URL is #url "
        when: "HTTP Method is GET: should return demand inventory list configuration"
        def response = client.get(headers: header)
        then: "the response should be 200"
        with(response) {
            status == 200
        }
        where:
        url << [inventoriesConfigURL]

    }
}
