package com.jda.dct.e2e.ui.utility.pageobjects;

import com.jda.dct.e2e.ui.utility.helper.Action.ActionHelper;
import com.jda.dct.e2e.ui.utility.helper.Wait.WaitHelper;
import com.jda.dct.e2e.ui.utility.helper.genericHelper.GenericHelper;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;


public class SituationRoomPage {
    WebDriver driver;
    WebDriverWait webDriverWait;

    WaitHelper objWaitHelper;
    GenericHelper objGenericHelper;
    ActionHelper actionHelper;

    public SituationRoomPage(WebDriver webdriver) {

        this.driver = webdriver;
        PageFactory.initElements(driver, this);
    webDriverWait = new WebDriverWait(driver, 30);
    objWaitHelper = PageFactory.initElements(driver, WaitHelper.class);
    objGenericHelper = PageFactory.initElements(driver, GenericHelper.class);
    actionHelper = PageFactory.initElements(driver, ActionHelper.class);
}

    //@FindBy(id = "situation_room_navigation")
    @FindBy(xpath ="(//*[@role=\"presentation\"])[2]")
    private WebElement situationRoomIcon;
    @FindBy(id = "view_situation_room_create_button")
    private WebElement createButton;
    @FindBy(id = "create_situation_room_name")
    private WebElement situationRoomName;
    @FindBy(id = "create_situation_room_description")
    private WebElement roomDescription;
    @FindBy(id = "create_situation_room_issue_type")
    private WebElement issueType;
    @FindBy(xpath = "//input[@value='shipment_delayed']")
    private WebElement ship;
    @FindBy(id = "create_situation_room_add_users")
    private WebElement addUser1;
    @FindBy(id = "create_situation_room_add_users")
    private WebElement addUser2;
    @FindBy(id = "create_situation_room_obj_no")
    private WebElement itemNumber;
   @FindBy(xpath = "//div[text()='MrA']")
   private WebElement userOne;
    @FindBy(xpath = "//div[text()='MrA']")
    private WebElement userTwo;
    @FindBy(xpath = "//div[text()='Shipment Delayed']")
    private WebElement shipment;
    @FindBy(xpath = "//span[text()='Create']")
    private WebElement create;
    @FindBy(id = "situation_room_detail_back_button")
    private WebElement allroom;
    @FindBy(xpath="(//*[@id='PanelSummary-RenderedItem-Monetary-Impact-0-drawer-link']/div)[2]")
    private WebElement situationMonetaryImpact;
    @FindBy(xpath="//*[@id=\"PanelSummary-RenderedItem-Sales-Order No-0-LinkItem\"]")
    private WebElement monetaryImpactSalesSummary;
    @FindBy(xpath="//span[text()=\"Demand\"]")
    private WebElement monetaryImpactDemand;
    @FindBy(id="PanelSummary-RenderedItem-Week-0-LinkItem")
    private WebElement monetaryImpactWeeklySummary;
    public void clickSituationRoomIcon() {
        objWaitHelper.waitElementToBeClickable(situationRoomIcon);
        objGenericHelper.elementClick(situationRoomIcon,"SituationRoomIcon");

    }

    public void clickCreateRoomButton() {
        objWaitHelper.waitElementToBeClickable(createButton);
        objGenericHelper.elementClick(createButton,"roomButton");

    }

    public void enterSituationRoomName() {
        //Use below code to get current timestamps and convert to string for room
        Timestamp ts = new Timestamp(System.currentTimeMillis());
        String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        String roomName = "Test_" + timeStamp;
        System.out.println("roomName: " + roomName);
        objWaitHelper.waitAndClickElement(situationRoomName, "CreateNewRoom");
        objGenericHelper.setElementText(situationRoomName, "Room Name", roomName);
    }

    public void enterRoomDescription(String roomdesc) {
        objWaitHelper.waitElementToBeClickable(roomDescription);
        objGenericHelper.elementClick(roomDescription, "Room description");
        objGenericHelper.setElementText(roomDescription, "Room Description", roomdesc);
        roomDescription.sendKeys(Keys.ENTER);
    }

    public void enterIssuetype() {
        objWaitHelper.waitElementToBeClickable(issueType);
        objGenericHelper.elementClick(issueType, "IssueType");
        objWaitHelper.waitElementToBeClickable(shipment);
        objGenericHelper.elementClick(shipment, "Shipment");
    }


    public void addUser1() {
        String user = "MrA";
        objWaitHelper.waitElementToBeClickable(addUser1);
        objGenericHelper.elementClick(addUser1,"Add User");
        objWaitHelper.waitElementToBeClickable(userOne);
        objGenericHelper.elementClick(userOne,"User One");
    }

    public void addUsers2() {
        objWaitHelper.waitElementToBeClickable(addUser1);
        objGenericHelper.elementClick(addUser1,"Add User");
        objWaitHelper.waitElementToBeClickable(userOne);
        objGenericHelper.elementClick(userOne,"User One");

        objWaitHelper.waitElementToBeClickable(addUser1);
        objGenericHelper.elementClick(addUser1,"Add User");
        objWaitHelper.waitElementToBeClickable(userTwo);
        objGenericHelper.elementClick(userTwo,"User Two");

    }


    public void enterItemNumber(String itemnumber) {
        objWaitHelper.waitAndClickElement(itemNumber, "Item numbeer");
        objGenericHelper.setElementText(itemNumber, "Item number", itemnumber);
        itemNumber.sendKeys(Keys.ENTER);
    }

    public void clickCreate() {
        objWaitHelper.waitElementToBeClickable(create);
        objGenericHelper.elementClick(create, "Create");
    }


    public boolean isNewRoomCreated() {
        objWaitHelper.waitElementToBeVisible(allroom);
        return objGenericHelper.isDisplayed(allroom, "all Room");
    }

    public boolean isSituationMonetaryImpactDisplayed() {
        objWaitHelper.waitElementToBeVisible(situationMonetaryImpact);
        return objGenericHelper.isDisplayed(situationMonetaryImpact, "Monetary Impact");
    }
    public void clickmonetaryImpact() {
        objWaitHelper.waitElementToBeClickable(situationMonetaryImpact);
        objGenericHelper.elementClick(situationMonetaryImpact,"MonetaryImpactisclicked");

    }

    public void clickmonetaryImpactDemand() {
        objWaitHelper.waitElementToBeClickable(monetaryImpactDemand);
        objGenericHelper.elementClick(monetaryImpactDemand,"MonetaryImpactDemandisclicked");
    }

    public boolean isMonetaryImpactSalesSummaryDisplayed()throws InterruptedException {
    objWaitHelper.waitElementToBeVisible(monetaryImpactSalesSummary);
           Thread.sleep(8000);
        return objGenericHelper.isDisplayed(monetaryImpactSalesSummary,"monetaryImpactSalesSummaryisDisplayed");
       }


    public boolean isMonetaryImpactWeeklySummaryDisplayed()throws InterruptedException {
        objWaitHelper.waitElementToBeVisible(monetaryImpactWeeklySummary);
        return objGenericHelper.isDisplayed(monetaryImpactWeeklySummary,"monetaryImpactWeeklySummaryisDisplayed");
    }

}


