package com.jda.dct.e2e.qa.pageobjects;


import com.jda.dct.e2e.ui.utility.common.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class SupplyInventoryListPage extends BasePage {
    WebDriver driver;
    WebDriverWait webDriverWait;
    //span[text()='INVENTORY']")
    @FindBy(id = "PanelSummary-RenderedItem-Item")
    private WebElement supplyInventoryListPage;
    @FindBy(xpath = "//span[contains(text(),'   Inventory Details   ')]")
    private WebElement supplyInventoryDetailsPage;
    //@FindBy(id = "PanelSummary-RenderedItem-Item")
    @FindBy(xpath = "(//a[@id=\"PanelSummary-RenderedItem-Item-LinkItem\"])[1]")
    private WebElement inventoryItemNumber;
    // @FindBy(xpath = "//a[@href='/supply/inventory']")
    @FindBy(xpath = "//a[@href=\"/supply/inventory\"] ")
    private WebElement backToInventoryPage;


    public SupplyInventoryListPage(WebDriver webdriver) {

        this.driver = webdriver;
        PageFactory.initElements(driver, this);
        webDriverWait = new WebDriverWait(driver, getTimeOutInSeconds());
    }


    //========Methods=====

    public boolean isSupplyInventoryListPageDisplayed() {
        webDriverWait.until(ExpectedConditions.visibilityOf(supplyInventoryListPage));
        return supplyInventoryListPage.isDisplayed();
    }


    public boolean isSupplyInventoryDetailsPageDisplayed() {
        webDriverWait.until(ExpectedConditions.visibilityOf(supplyInventoryDetailsPage));
        return supplyInventoryDetailsPage.isDisplayed();
    }

    public void clickInventoryItemNumber() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(inventoryItemNumber));
        inventoryItemNumber.click();
    }

    public void clickBackToInventoryPage() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(backToInventoryPage));
        backToInventoryPage.click();
    }


}


