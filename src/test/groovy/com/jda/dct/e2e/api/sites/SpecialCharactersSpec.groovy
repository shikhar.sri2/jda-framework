package com.jda.dct.e2e.api.sites

import com.jda.dct.e2e.api.categories.Smoke
import com.jda.dct.e2e.api.utility.DateUtils
import com.jda.dct.e2e.api.utility.IngestionHelper
import org.junit.experimental.categories.Category
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll

class SpecialCharactersSpec extends Specification implements IngestionHelper {
    @Shared sitesURL

    def setupSpec() {
        init()

        def sitesPath = properties.getProperty('ingestSitesPath')
        sitesURL = protocol + ip + sitesPath + '?siteType=DC'
    }

    // smoke
    @Category(Smoke)
    @Unroll
    def "Ingest default csv files: #file"() {
        given:
        def filePath = this.getClass().getClassLoader().getResource(file).path
        DateUtils.convertDateInFile(new File(filePath))

        when:
        def response = postFile(filePath, postUrl, null)
        def ingestionId = getIngestionId(response)
        def status = waitUntilComplete(ingestionId)

        then:
        with(status) {
            contains('COMPLETED')
        }

        where:
        file                                  | postUrl
        'csv/special_characters/site.csv'     | sitesURL
    }
}
