package com.jda.dct.e2e.ui.lists;

import com.jda.dct.e2e.ui.utility.action.CommonActions;
import com.jda.dct.e2e.ui.utility.common.BaseTest;
import com.jda.dct.e2e.ui.utility.common.LeftMenu;
import com.jda.dct.e2e.ui.utility.helper.AssertHelper.AssertHelper;
import com.jda.dct.e2e.ui.utility.pageobjects.SupplyForecastListPage;
import com.jda.dct.e2e.ui.utility.pageobjects.TopMenuPage;
import com.jda.dct.e2e.ui.utility.pageobjects.TopSubMenuPage;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class SupplyForecastListTests extends BaseTest {

    private LeftMenu leftMenu;
    private TopMenuPage topMenu;
    private TopSubMenuPage topSubMenu;
    private SupplyForecastListPage supplyForecastListPage;
    private AssertHelper assertHelper;
    private WebDriver driver;
    private CommonActions commonActions;

    @BeforeClass
    public void setUp() throws InterruptedException {
        //Initialize required objects
        driver = getDriver();
        leftMenu = new LeftMenu(driver);
        topMenu = new TopMenuPage(driver);
        topSubMenu = new TopSubMenuPage(driver);
        supplyForecastListPage = new SupplyForecastListPage(driver);
        assertHelper = new AssertHelper(driver);
        commonActions = new CommonActions(driver);
        //loginAndNavigateToSupplyLandingPage
        logInToApplication();
    }

    @Test(priority =1,retryAnalyzer = BaseTest.class)
    public void validateSupplyForecastListPageIsDisplayed() throws InterruptedException{
        leftMenu.clickSupplyMenuOption();
 //verify user is at Supply Forecast-commit tab
        topMenu.goToSupplyForecastCommitTab();
        //supplyForecastListPage.clickForecastExceptionsHavingData();
//verify user is at Supply Forecast List page
        commonActions.disableAllExceptionFilter();
        assertHelper.assertTrue(supplyForecastListPage.isSupplyForecastListPageDisplayed(), "Forecast list is displayed successfully");

    }


    @Test(priority = 2,retryAnalyzer = BaseTest.class)
    public void validateFieldsDisplayAtHeaderLevel()throws InterruptedException{
        leftMenu.clickSupplyMenuOption();
 //verify user is at Supply Forecast-commit tab
        topMenu.goToSupplyForecastCommitTab();
//verify all Headerlevel fields are displayed
        //supplyForecastListPage.clickForecastExceptionsHavingData();
        commonActions.disableAllExceptionFilter();
        assertHelper.assertTrue(supplyForecastListPage.isitemAtHeaderLevelDisplayed(), "Item at Header level is displayed successfully");
        assertHelper.assertTrue(supplyForecastListPage.isSupplierAtHeaderLevelDisplayed(), "Supplier at Header level is displayed successfully");
        assertHelper.assertTrue(supplyForecastListPage.isShipToAtHeaderLevelDisplayed(), "Ship To at Header level is displayed successfully");
        assertHelper.assertTrue(supplyForecastListPage.isShipFromAtHeaderLevelDisplayed(), "Ship From at Header level is displayed successfully");
        assertHelper.assertTrue(supplyForecastListPage.isNotesAtHeaderLevelDisplayed(), "Notes at Header level is displayed successfully");
        assertHelper.assertTrue(supplyForecastListPage.isMaterialGroupAtHeaderLevelDisplayed(), "Material Group at Header level is displayed successfully");

    }


    @Test(priority = 3,retryAnalyzer = BaseTest.class)
    public void validateSupplyForecastDetailsPageIsDisplayedWhenItemNumberIsClicked() throws InterruptedException {
        leftMenu.clickSupplyMenuOption();
        topMenu.goToSupplyForecastCommitTab();
        topMenu.clickSupplyForecastCommitsForecastCommitMismatch();
        supplyForecastListPage.clickForecastItemNumber();
 //verify user is at Supply Forecast Details Page
        assertHelper.assertTrue(supplyForecastListPage.isSupplyForecastDetailsPageDisplayed(), "Forecast details page is displayed successfully");
    }
}


