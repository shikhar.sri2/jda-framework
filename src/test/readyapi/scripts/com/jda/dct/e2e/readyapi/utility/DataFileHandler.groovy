package com.jda.dct.e2e.readyapi.utility

import java.nio.file.Files
import java.nio.file.Path
import java.time.LocalDate
import java.time.format.DateTimeFormatter

class DataFileHandler {

    static List<String> read(Path filePath) {
        return Files.readAllLines(filePath)
    }

    static String calculateDate(String delta, String datePattern) {
        return LocalDate.now().plusDays(delta.toInteger()).atStartOfDay().format(DateTimeFormatter.ofPattern(datePattern)).toString()
    }

    static List<String> processLines(List<String> lines) {
        List<String> processedLines = new ArrayList<>()

        for (def line : lines) {
            line = line.replaceAll(/\{#currentDate\((-?[0-9]+)\)}/) {String all, String delta ->
                calculateDate(delta, "MM/dd/yyyy HH:mm")
            }

            line = line.replaceAll(/\{#currentIsoDate\((-?[0-9]+)\)}/) {String all, String delta ->
                calculateDate(delta, "yyyy-MM-dd")
            }
            processedLines.add(line)
        }

        return processedLines
    }

    static void write(Path filePath, List<String> records) {
        Files.write(filePath, records)
    }

    static List<String> processAndMove(Path sourceFilePath, Path destinationFilePath) {
        List<String> sourceFileLines = read(sourceFilePath)
        List<String> processedFileLines = processLines(sourceFileLines)
        write(destinationFilePath, processedFileLines)

        return processedFileLines
    }
}
