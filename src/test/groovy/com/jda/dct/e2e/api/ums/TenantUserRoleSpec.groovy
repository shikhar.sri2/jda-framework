package com.jda.dct.e2e.api.ums

import com.jda.dct.e2e.api.utility.IngestionHelper
import org.json.simple.JSONArray
import org.json.simple.parser.JSONParser
import org.springframework.http.HttpMethod
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll

@Unroll
class TenantUserRoleSpec extends Specification implements IngestionHelper {

    @Shared umsUrl
    @Shared resetUserRoleUrl

    @Shared sampleTenantUser
    @Shared sampleTenantRole
    @Shared sampleTenantRole1

    @Shared String tenantId
    @Shared String userName
    @Shared String roleName
    @Shared String roleName1

    def setupSpec() {
        init()
        umsUrl = protocol + ip + umsPath
        resetUserRoleUrl = umsUrl + 'tenantUsers/resetTenantUser'
        JSONParser parser = new JSONParser()

        sampleTenantUser = this.getClass().getClassLoader().getResource('jsons/sample-tenant-user.json').text
        Object user = parser.parse(sampleTenantUser)
        tenantId = ((JSONArray) user).get(0)["tenantId"]
        userName = ((JSONArray) user).get(0)["userName"]

        sampleTenantRole = this.getClass().getClassLoader().getResource('jsons/sample-tenant-role.json').text
        Object role = parser.parse(sampleTenantRole)
        roleName = ((JSONArray) role).get(0)["roleName"]

        sampleTenantRole1 = this.getClass().getClassLoader().getResource('jsons/sample-tenant-role1.json').text
        Object role1 = parser.parse(sampleTenantRole1)
        roleName1 = ((JSONArray) role1).get(0)["roleName"]
    }
    // smoke
    def "Assign tenantUser role"() {
        given: "Provided a tenant user and role"
        executeHttpCall(header, umsUrl + 'tenantUsers', sampleTenantUser, HttpMethod.POST)
        executeHttpCall(header, umsUrl + 'tenantRoles', sampleTenantRole, HttpMethod.POST)

        when: "Assign the user with the role"
        def sampleTenantUserRole = this.getClass().getClassLoader().getResource('jsons/sample-tenant-assign-user-role.json').text
        def response = executeHttpCall(header, resetUserRoleUrl, sampleTenantUserRole, HttpMethod.POST)

        then: "Assign successfully"
        response != null && response.data.size > 0 &&
                response.data[0].user["userName"].contains(userName) && response.data[0].tenantRoles["roleName"].any {
            it.contains(roleName)
        }
    }
    // smoke
    def "Get tenant user role"() {
        when: "Get tenant user role"
        def url = umsUrl + "tenantUsers/" + userName + "?detail=true"
        def response = executeHttpCall(header, url, null, HttpMethod.GET)

        then: "Get user role association"
        response != null && response.data.size > 0 && response.data[0].tenantRoles["roleName"].any {
            it.contains(roleName)
        }
    }
    // rest are regression
    def "Edit tenant user role assignment"() {
        given: "Add role1 to tenant"
        executeHttpCall(header, umsUrl + 'tenantRoles', sampleTenantRole1, HttpMethod.POST)

        when: "Assign user with role1"
        def sampleTenantUserRole1 = this.getClass().getClassLoader().getResource('jsons/sample-tenant-assign-user-role1.json').text
        def response = executeHttpCall(header, resetUserRoleUrl, sampleTenantUserRole1, HttpMethod.POST)

        then: "Assigned successfully"
        response != null && response.data.size > 0 &&
                response.data[0].user["userName"].contains(userName) && response.data[0].tenantRoles["roleName"].any{
            it.contains(roleName1)
        }
    }

    /*def "Delete tenant user role association"() {
        given: "user is assigned to some roles"
        List<String> roles1 = getRolesOfAUser(userName)

        when: "Delete the tenant user roles association"
        for (String role : roles1) {
            println role
            def url = umsUrl + "tenantUsers/removeTenantRole" + "?tid=" + tenantId + "&userName=" + userName + "&roleName=" + role
            executeHttpCall(header, url, null, HttpMethod.POST)
        }

        then: "user is not assigned to any role"
        List<String> roles2 = getRolesOfAUser(userName)
        println roles1.size()
        println roles2.size()
        roles1.size() > 0
        roles2.size() == 0
    }*/

    def "Verify error when role doesn't exist while associating tenantUserRole" () {
        when: "Assign user to a non-exist role"
        def sampleTenantUserRoleFalse = this.getClass().getClassLoader().getResource('jsons/sample-tenant-assign-user-rolefalse.json').text
        int status = getHttpStatusVal(header, resetUserRoleUrl, sampleTenantUserRoleFalse, HttpMethod.POST)

        then: "Expect association failed"
        status != 200
    }

    def "Verify error when tenant doesn't exist while associating tenantUserRole" () {
        when: "Assign user role with temp tenant"
        def sampleTenantUserRoleFalse = this.getClass().getClassLoader().getResource('jsons/sample-tenant-assign-user-role-invalidTenantId.json').text
        int status = getHttpStatusVal(header, resetUserRoleUrl, sampleTenantUserRoleFalse, HttpMethod.POST)

        then: "Expect association failed"
        status != 200
    }

    def "Verify multiple roles can be associated to same user"() {
        when: "Assign the user with the roles"
        def sampleTenantUserRole = this.getClass().getClassLoader().getResource('jsons/sample-tenant-assign-user-role-multiRoles.json').text
        def response = executeHttpCall(header, resetUserRoleUrl, sampleTenantUserRole, HttpMethod.POST)

        then: "Assign multi-roles to the user successfully"
        response != null && response.data.size > 0 &&
                response.data[0].user["userName"].contains(userName) && response.data[0].tenantRoles.size() > 0

    }

//Helper Function

    List<String> getRolesOfAUser(String userName) {
        def url = umsUrl + "tenantRoles/userName/" + userName
        def response = executeHttpCall(header, url, null, HttpMethod.GET)
        List<String> roles = new ArrayList<>()
        if (response != null) {
            response.data.every{
                roles.add(it["roleName"].toString())
            }
        }
        return roles
    }
}
