//package com.jda.dct.e2e.ui.inventory;
//
//
//import BaseTest;
//import LeftMenu;
//import com.jda.dct.e2e.utility.ui.pageobjects.*;
//
//import TopMenuPage;
//import TopSubMenuPage;
//import org.openqa.selenium.WebDriver;
//import org.testng.Assert;
//import org.testng.annotations.BeforeClass;
//import org.testng.annotations.Test;
//
//
//public class DemandInventoryListTests extends BaseTest {
//
//    private LeftMenu leftMenu;
//    private TopMenuPage topMenu;
//    private TopSubMenuPage topSubMenu;
//    private DemandInventoryListPage demandInventoryListPage;
//    private DemandShipmentListPage demandShipmentListPage;
//    private DemandShipmentDetailsPage demandShipmentDetailsPage;
//    private WebDriver driver;
//
//
//    @BeforeClass
//    public void setUp() throws InterruptedException {
//        System.out.println("Starting tests for :" + this.getClass().getName());
//        //Initialize required objects
//        driver = getDriver();
//        leftMenu = new LeftMenu(driver);
//        topMenu = new TopMenuPage(driver);
//        topSubMenu = new TopSubMenuPage(driver);
//        demandShipmentListPage = new DemandShipmentListPage(driver);
//        demandInventoryListPage = new DemandInventoryListPage(driver);
//        demandShipmentDetailsPage = new DemandShipmentDetailsPage(driver);
//        //loginAndNavigateToDemandLandingPage
//        logInToApplication();
//        leftMenu.clickDemandMenuOption();
//    }
//
//    @Test(priority = 1)
//    public void validateDemandInventoryListPageIsDisplayed() {
//        // navigate to Inventory tab
//        topMenu.goToInventorySideBarTab();
//
//        //verify demand Inventory List page is displayed
//        Assert.assertTrue(demandInventoryListPage.isDemandInventoryListPageDisplayed());
//    }
//
////    @Test(priority = 2)
////    public void validateDemandInventoryDetailPageIsDisplayed() {
////        topMenu.goToInventorySideBarTab();
////        demandInventoryListPage.clickInvnetoryItemNumber();
////        //driver.navigate().refresh();
////        Assert.assertTrue(demandInventoryListPage.isDemandInventoryDetailsPageDisplayed());
////        demandInventoryListPage.backToDemandInventoryPage();
////
////    }
////
////
////
////    }
////}