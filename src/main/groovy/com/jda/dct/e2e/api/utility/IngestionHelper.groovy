package com.jda.dct.e2e.api.utility

import com.fasterxml.jackson.databind.ObjectMapper
//import com.jda.dct.domain.Participant
import com.jda.dct.domain.Item
import com.jda.dct.domain.Node
import com.jda.dct.domain.Site
import com.jda.dct.domain.SourcingLane
import com.jda.dct.domain.stateful.Delivery
import com.jda.dct.domain.stateful.PurchaseOrder
import com.jda.dct.domain.stateful.SalesOrder
import com.jda.dct.domain.stateful.Shipment
import groovy.json.JsonOutput
import groovy.json.JsonSlurper
import org.codehaus.groovy.reflection.ReflectionUtils
import org.junit.platform.commons.util.StringUtils
import org.springframework.core.io.FileSystemResource
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.util.LinkedMultiValueMap
import org.springframework.web.client.RestTemplate

import java.util.logging.Logger

/**
 * utility helper methods for ingestion activities.
 *
 * @author Vida Bandara
 * @version 1.0-SNAPSHOT
 * @since 11-May-2018
 */

trait IngestionHelper extends TestHelper {
    static sampleNode
    static sampleNodeNoDesc
    static sampleSite
    static sampleItem
    static sampleSourcingLane
    static sampleParticipant
    static sampleSiteWithDesc
    static sampleDelivery
    static sampleDeliveryNoDesc
    static sampleShipment
    static sampleShipmentNoDesc
    static sampleShipmentMissingReqExtField
    static samplePurchaseOrder
    static samplePurchaseOrderNoDesc
    static sampleSalesOrder
    static sampleSalesOrderNoDesc
    static sampleSiteSourcingLaneShipment
    static header
    static itemIngestionUrl
    static siteIngestionUrl
    static sourcingLaneIngestionUrl
    static participantsIngestionUrl
    static batchesIngestionUrl
    static calendarsIngestionUrl
    static nodeIngestionUrl
    static purchaseOrderIngestionUrl
    static salesOrderIngestionUrl
    static ingestionURL
    static getURLPrefix
    static searchAlertUrlPrefix
    static searchDeliveryUrlPrefix
    static searchSiteUrlPrefix
    static statusURLPrefix

    static protocol
    static ip
    static webUrl
    Properties properties

    static processModelPostPath
    static dctIngestionPath
    static staticIngestionPath
    static umsPath

    def init() {
        def init = new Initialize()
        init.initConfig()

        def token = init.getAccessToken()
        header = ['Authorization': "Bearer $token"]

        protocol = init.getProtocol()
        ip = init.getIP()
        webUrl = init.getWebURL()
        fetchEndPoints()
    }

    def initAndGetTokenMultiTenant(def tid) {
        if (StringUtils.isBlank(tid)) {
            throw new IllegalArgumentException("Missing tid when multiTenantInit(tid)")
        }
        def init = new Initialize()
        init.initConfig()
        def token = init.getAccessTokenWithTid(tid, null)
        def headerOfTid = ['Authorization': "Bearer $token"]
        protocol = init.getProtocol()
        ip = init.getIP()
        webUrl = init.getWebURL()
        fetchEndPoints()
        return headerOfTid
    }

    def fetchEndPoints() {
        def stream = this.getClass().getClassLoader().getResourceAsStream('endpoint.properties')
        properties = new Properties()
        properties.load(stream)

        def statusPath = properties.getProperty('ingestionStatusPath')
        statusURLPrefix = protocol + ip + statusPath

        processModelPostPath = properties.getProperty('processModelPath')
        dctIngestionPath = properties.getProperty('dctIngestionPath')
        staticIngestionPath = properties.getProperty('staticIngestionPath')
        umsPath = properties.getProperty('umsPath')
    }

    def getStatus(id) {
        def statusObjs = getStatusObjects(id)
        def status = statusObjs?.collect { it.get('status') }.findAll { !it.isEmpty() }

        status
    }

    def getStatusObjects(id) {
        def statusURL = statusURLPrefix + id
        def jsonStatus = getHttpClient(header, statusURL, null, HttpMethod.GET).body
        // println JsonOutput.prettyPrint(jsonStatus)
        def statusEvents = getJson(jsonStatus)
        def status = statusEvents.events.findAll {
            !it.isEmpty() && it.get('component') == 'dct'
        }
        status
    }

    def getErrors(id) {
        def statusURL = statusURLPrefix + id
        def jsonStatus = getHttpClient(header, statusURL, null, HttpMethod.GET).body
        def statusEvents = getJson(jsonStatus)
        def errorStatus = statusEvents.events.find { it.get('status') == 'COMPLETED_WITH_ERROR' }

        errorStatus.get('errors')
    }

    def ingestData(def data, def endpoint) {
        def url = protocol + ip + dctIngestionPath + endpoint
        System.out.println("ingestionURL: $url")
        def response = executeHttpCall(header, url, data, HttpMethod.POST)
        def ingestionId = response.ingestionId
        def message = "*** Could not ingest: $data"
        waitUntilComplete(ingestionId, message)

        return ingestionId
    }

    def waitUntilComplete(ingestionId, def message = "could not ingest $ingestionId", int retries = 10) {
        def status = getStatus(ingestionId)
        def counter = 0
        while (!(status.contains('COMPLETED') || status.contains('COMPLETED_WITH_ERROR'))) {
            status = getStatus(ingestionId)
            if (++counter > retries) {
                System.err.println(message)
                break
            }
            sleep(1_000L)
        }
        status
    }

    def searchUntil(Map<String, String> searchCriteriaMap,
                    def searchUrl, String condition, int expectedResultCount, int timeOutInSeconds) {
        long timeOutInMilliseconds = timeOutInSeconds * 1000
        long startTime = new Date().getTime()
        String results = null

        while (!exceededTimeout(timeOutInMilliseconds, startTime)) {
            results = search(searchCriteriaMap, searchUrl)
            println results
            if (results.contains(condition + expectedResultCount)) {
                return results
            } else {
                //Sleep and Retry
                System.err.println("Response: $results")
                Thread.sleep(2_000L)
            }
        }
        return results
    }

//    def searchResponseJson(Map<String, String> searchCriteriaMap,
//                           def searchUrl, String expectedState, int timeOutInSeconds) {
//        long timeOutInMilliseconds = timeOutInSeconds * 1000
//        long startTime = new Date().getTime()
//        def result
//        while (!exceededTimeout(timeOutInMilliseconds, startTime)) {
//            result = search(searchCriteriaMap, searchUrl)
//            if (result != null && result.get("data") != null && result.get("data").size() > 0 && result.get("data").get(0) != null
//                    && result.get("data").get(0).get("state").toString().equals(expectedState)) {
//                return result
//            } else {
//                Thread.sleep(1_000L)
//            }
//        }
//        return result
//    }
//
//    def searchUntil(Map<String, String> searchCriteriaMap,
//                    def searchUrl, int expectedCount, int expectedTotalCount, int timeOutInSeconds) {
//        long timeOutInMilliseconds = timeOutInSeconds * 1000
//        long startTime = new Date().getTime()
//        String results = null
//
//        while (!exceededTimeout(timeOutInMilliseconds, startTime)) {
//            results = search(searchCriteriaMap, searchUrl)
//            if (results.contains("count=" + expectedCount) && (results.contains("totalCount=" + expectedTotalCount))) {
//                return results
//            } else {
//                //Sleep and Retry
//                System.err.println(results)
//                Thread.sleep(1_000L)
//            }
//        }
//        return results
//    }

    def delete(Map<String, String> searchCriteriaMap, def searchUrl) {
        String parameters = constructSearchParameters(searchCriteriaMap)

        def searchWithParams = searchUrl + parameters
        println "execute DELETE"
        def response = executeHttpCall(header, searchWithParams, null, HttpMethod.DELETE)
        response
    }

    def deleteObject(def url, def id) {
        def searchUrl = makeSearchUrlFromGetUrl(url)
        final Map<String, String> searchCriteria = new HashMap<>()
        searchCriteria.put('query', 'id=' + id)

        delete(searchCriteria, searchUrl)
        sleep(1_000L)
    }

    def search(Map<String, String> searchCriteriaMap, def searchUrl) {
        String parameters = constructSearchParameters(searchCriteriaMap)

        def searchWithParams = searchUrl + parameters
        def response = executeHttpCall(header, searchWithParams, null, HttpMethod.GET)
        response
    }

    private String constructSearchParameters(Map<String, String> searchCriteriaMap) {
        final StringBuilder parameters = new StringBuilder("?")

        final Iterator<Map.Entry<String, String>> parameterIterator = searchCriteriaMap.entrySet().iterator()

        while (parameterIterator.hasNext()) {
            final Map.Entry<String, String> entry = parameterIterator.next()
            if (parameterIterator.hasNext()) {
                parameters.append(entry.getKey()).append("=").append(entry.getValue()).append("&")
            } else {
                parameters.append(entry.getKey()).append("=").append(entry.getValue())
            }
        }
        parameters.toString()
    }

    def exceededTimeout(long timeOutInMilliseconds, long startTime) {
        long currentTime = new Date().getTime()
        long timePassed = currentTime - startTime

        if (timePassed > timeOutInMilliseconds) {
            return true
        } else {
            return false
        }
    }

    List<String> getIds(def delivery) {
        new JsonSlurper().parseText(delivery).collect {
            JsonOutput.toJson(it)
        }.collect {
            new ObjectMapper().readValue(it, Delivery.class)
        }.collect {
            it.getId()
        }
    }

    List<String> getNodeIds(def node) {
        new JsonSlurper().parseText(node).collect {
            JsonOutput.toJson(it)
        }.collect {
            new ObjectMapper().readValue(it, Node.class)
        }.collect {
            it.getId()
        }
    }

    List<String> getShipmentIds(def shipment) {
        new JsonSlurper().parseText(shipment).collect {
            JsonOutput.toJson(it)
        }.collect {
            new ObjectMapper().readValue(it, Shipment.class)
        }.collect {
            it.getId()
        }
    }

    List<String> getSiteIds(def site) {
        new JsonSlurper().parseText(site).collect {
            JsonOutput.toJson(it)
        }.collect {
            new ObjectMapper().readValue(it, Site.class)
        }.collect {
            it.getId()
        }
    }

    List<String> getItemIds(def item) {
        new JsonSlurper().parseText(item).collect {
            JsonOutput.toJson(it)
        }.collect {
            new ObjectMapper().readValue(it, Item.class)
        }.collect {
            it.getId()
        }
    }

    List<String> getSourcingLaneIds(def sourcingLane) {
        new JsonSlurper().parseText(sourcingLane).collect {
            JsonOutput.toJson(it)
        }.collect {
            new ObjectMapper().readValue(it, SourcingLane.class)
        }.collect {
            it.getId()
        }
    }

    List<String> getParticipantIds(def participant) {
        new JsonSlurper().parseText(participant).collect {
            JsonOutput.toJson(it)
        }.collect {
            new ObjectMapper().readValue(it, Participant.class)
        }.collect {
            it.getId()
        }
    }

    List<String> getDeliveryIds(def deliveries) {
        new JsonSlurper().parseText(deliveries).collect {
            JsonOutput.toJson(it)
        }.collect {
            new ObjectMapper().readValue(it, Delivery.class)
        }.collect {
            it.getId()
        }
    }

    List<String> getPurchaseOrderIds(def purchaseOrder) {
        new JsonSlurper().parseText(purchaseOrder).collect {
            JsonOutput.toJson(it)
        }.collect {
            new ObjectMapper().readValue(it, PurchaseOrder.class)
        }.collect {
            it.getId()
        }
    }

    List<String> getSalesOrderIds(def salesOrder) {
        new JsonSlurper().parseText(salesOrder).collect {
            JsonOutput.toJson(it)
        }.collect {
            new ObjectMapper().readValue(it, SalesOrder.class)
        }.collect {
            println(it.getId())
            it.getId()
        }
    }

    def getId(def jsonData, def dataObjectClass) {
        new JsonSlurper().parseText(jsonData).collect {
            JsonOutput.toJson(it)
        }.collect {
            new ObjectMapper().readValue(it, dataObjectClass)
        }.collect {
            it.getId()
        }
    }

    Object getJsonAt(def jsonString, def index) {
        def ingestedText = new JsonSlurper().parseText(jsonString).collect {
            JsonOutput.toJson(it)
        }[index]
        def ingestedJson = new JsonSlurper().parseText(ingestedText)

        ingestedJson
    }

    def postFile(def path, def url, def header2) {
        def httpHeaders = new HttpHeaders()
        httpHeaders.setContentType(MediaType.MULTIPART_FORM_DATA)
        def curHeader = header2 == null ? header : header2
        httpHeaders.add('Authorization', curHeader.get('Authorization'))

        def map = new LinkedMultiValueMap<>()
        map.add('file', new FileSystemResource(path))
        def httpEntity = new HttpEntity<>(map, httpHeaders)

        def restTemplate = new RestTemplate()
        def response = restTemplate.exchange(url, HttpMethod.POST, httpEntity, String)
        if (response == null) {
            println "Could not get response after POST"
        }
        response
    }

    def getIngestionId(ResponseEntity<Object> response) {
        if (getJson(response.body).ingestionId == null) {
            println "Could not get ingestion id"
        }
        getJson(response.body).ingestionId
    }

    /**
     * Get postUrl with given file name
     */
    String getPostUrl(String fileName) {
        String[] words = getFileNameWordsArray(fileName)
        String postUrl
        def resourceName = getResourceName(fileName)
        if (resourceName == 'sites') {
            postUrl = protocol + ip + staticIngestionPath + resourceName + "?siteType=" + words[1]
        } else if (resourceName == 'items') {
            postUrl = protocol + ip + staticIngestionPath + resourceName + "?itemType=" + words[1]
        } else if (resourceName == 'sourcingLanes') {
            postUrl = protocol + ip + staticIngestionPath + resourceName
        } else if (resourceName == 'participants') {
            postUrl = protocol + ip + staticIngestionPath + resourceName + "?participantType=" + words[1]
        } else if (resourceName == 'batches' || resourceName == 'calendars' ||  resourceName == 'boms') {
            postUrl = protocol + ip + staticIngestionPath + resourceName
        } else {
            def model = getModelName(resourceName)
            postUrl = protocol + ip + dctIngestionPath + resourceName +
                    "?processType=" + words[0] + '&' + model + 'Type=' + words[2]
        }
        postUrl
    }

    /**
     * Get resource type with given file name
     */
    String getResourceName(String fileName) {
        String[] words = getFileNameWordsArray(fileName)
        if (words[0] == 'sites' || words[0] == 'items' || words[0] == 'sourcingLanes' || words[0] == 'participants'
                || words[0] == 'batches' || words[0] == 'calendars' || words[0] == 'boms') {
            return words[0]
        }
        return words[1]
    }

    /**
     * Get model name from resource name
     */
    String getModelName(String resourceName) {
        //some special mapping, most case, just remove 's' in the ene
        def modelMap = ['measures': 'node', 'deliveries': 'delivery']
        def model = modelMap.get(resourceName)
        if (model == null) {
            //not special, strip last character (should be 's') that is the model name
            model = resourceName.subSequence(0, resourceName.length() - 1)
        }
        return model
    }

    /**
     * Split file name by "-", and save into an array
     * */
    String[] getFileNameWordsArray(String fileName) {
        String[] nameArray
        if(fileName.contains(".csv")){
            nameArray = fileName.substring(0, fileName.indexOf(".csv")).split("-")
        }else{
            nameArray = fileName.substring(0, fileName.indexOf(".json")).split("-")
        }
        return  nameArray
    }

    def getResponse(ingestionId, header) {
        def statusURL = statusURLPrefix + ingestionId
        def jsonStatus = getHttpClient(header, statusURL, null, HttpMethod.GET).body
        jsonStatus
    }

    /**
     * Keep checking ingestion status of given ingestionId
     * */
    def getFinalStatus(ingestionId, String fileName, def message = "could not ingest $ingestionId", int retries = 350, def header2) {
        long startTime = System.nanoTime()
        Stack stack = new Stack<>()
        def curHeader = header2 == null ? header : header2
        def jsonStatus = getResponse(ingestionId, curHeader)
        stack.push(jsonStatus)
        Set<String> statusSet = getBusinessStatus(getJson(jsonStatus))
        int counter = 0
        while (!(statusSet.contains('COMPLETED') || statusSet.contains('COMPLETED_WITH_ERROR'))) {
            sleep(1_000L)
            jsonStatus = getResponse(ingestionId, curHeader)
            stack.push(jsonStatus)
            statusSet = getBusinessStatus(getJson(jsonStatus))
            if (++counter > retries) {
                System.err.println(message)
                break
            }
        }
        long endTime = System.nanoTime()
        long duration = (endTime - startTime) / 1000000
        println()
        println "Loading " + fileName + " takes "+ duration + " ms"
        def status
        if (statusSet.contains('COMPLETED')) {
            status = 'COMPLETED'
            println ("Status: COMPLETED: " + fileName)
        } else {
            status = 'COMPLETED_WITH_ERROR'
            println ("Status: COMPLETED_WITH_ERROR:" + fileName)
            println JsonOutput.prettyPrint(stack.peek())
        }
        status
    }

    def getBusinessStatus(responseJson) {
        Set<String> statusSet = new HashSet<>()
        responseJson.events.each {
            //do not check harvester which is the service to put into datalake
            if (!it.isEmpty() && it.get('service') != 'harvester') {
                String status = it.get('status')
                statusSet.add(status)
            }
        }
        statusSet
    }

    def getTmpDir() {
        def parent = System.getProperty('java.io.tmpdir')
        def subDirectory = ReflectionUtils.getCallingClass().simpleName
        def directoryName = parent + File.separator + subDirectory + File.separator
        def directory = new File(directoryName)
        if (!directory.exists()) {
            directory.mkdirs()
        }
        directoryName
    }

    /**
     * Get details of an elastic search index, like health, status, docs.count...
     * */
    String[][] getCurrentIdxDetails(indexUrl, tenantIdDash, object) {
        String[][] curRes = new String[2][10]
        List<String> res = Arrays.asList(getHttpClient(header, indexUrl, null, HttpMethod.GET).body.toString().split("\n"))
        String[] indexHeader = res.get(0).split("\\s+")
        curRes[0] = indexHeader
        for (int i = 1; i < res.size(); i++) {
            String[] cur = res.get(i).split("\\s+")
            if (cur.length > 7 && cur[2] == tenantIdDash + object) {
                curRes[1] = cur
                return curRes
            }
        }
        return null
    }
}