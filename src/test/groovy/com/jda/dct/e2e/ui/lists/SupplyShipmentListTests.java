package com.jda.dct.e2e.ui.lists;

import com.jda.dct.e2e.ui.utility.action.CommonActions;
import com.jda.dct.e2e.ui.utility.common.BaseTest;
import com.jda.dct.e2e.ui.utility.common.LeftMenu;
import com.jda.dct.e2e.ui.utility.helper.AssertHelper.AssertHelper;
import com.jda.dct.e2e.ui.utility.helper.Browser.BrowserHelper;
import com.jda.dct.e2e.ui.utility.pageobjects.*;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class SupplyShipmentListTests extends BaseTest {

    private LeftMenu leftMenu;
    private TopMenuPage topMenu;
    private SupplyDeliveryListPage supplyDeliveryPage;
    private SupplyShipmentListPage supplyShipmentListPage;
    private SupplyShipmentDetailsPage supplyShipmentDetailsPage;
    private TopSubMenuPage topSubMenu;
    private BrowserHelper browserHelper;
    private CommonActions commonActions;
    AssertHelper assertHelper;
    private SupplyProcessModelPage processmodel;
    private WebDriver driver;

    @BeforeClass
    public void setUp() throws InterruptedException {
        System.out.println("Starting tests for :" + this.getClass().getName());
//Initialize required objects
        driver = getDriver();
        leftMenu = new LeftMenu(driver);
        topMenu = new TopMenuPage(driver);
        topSubMenu = new TopSubMenuPage(driver);
        supplyDeliveryPage = new SupplyDeliveryListPage(this.driver);
        supplyShipmentListPage = new SupplyShipmentListPage(this.driver);
        supplyShipmentDetailsPage = new SupplyShipmentDetailsPage(this.driver);
        browserHelper = new BrowserHelper(driver);
        assertHelper = new AssertHelper(driver);
        processmodel = new SupplyProcessModelPage(driver);
        commonActions = new CommonActions(driver);
        //loginAndNavigateToSupplyShipmentLandingPage
        logInToApplication();
    }

    @BeforeMethod
    public void commonMethods() throws InterruptedException{
        leftMenu.clickSupplyMenuOption();
        //navigates to Supply shipment tab
    }

    @Test(priority = 1,retryAnalyzer = BaseTest.class)
    public void validateSupplyShipmentListPageIsDisplayed() throws InterruptedException {
//verify user is at tempraturecontrolled shipment page
        topMenu.goToSupplyShipmentsTab();
        topMenu.clearFilterIfVisible();
        supplyShipmentListPage.clickOceanIcon();
        commonActions.disableAllExceptionFilter();
        assertHelper.assertTrue(supplyShipmentListPage.isSupplyShipmentListPageDisplayed(), "Supply Shipment Page is displayed successfully");
        browserHelper.refresh();
    }

    //testcase for DCT 10220
//    @Test(priority = 2,retryAnalyzer = BaseTest.class)
//    public void validateShipmentFieldsperprocessmodel() throws InterruptedException {
//        String Shipment ="ISTEST004_NO_S1";
//        topMenu.goToSupplyShipmentsTab();
//        topMenu.clickStatesTab();
//        topMenu.clickSupplyShipmentStatesShipped();
//        supplyShipmentListPage.clickRoadIcon();
//        topSubMenu.clickMaterialGroupDropdown();
//        topSubMenu.clickAllTypes();
//        topSubMenu.clickSearchButton();
//        topMenu.clearFilterIfVisible();
//        topSubMenu.goToSearchFeature(Shipment);
//        topMenu.clickExpandButton();
//        processmodel.fieldvalidation();
//
//    }
//
//





}