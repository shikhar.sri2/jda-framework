package com.jda.dct.e2e.ui.lots;

import com.jda.dct.e2e.ui.utility.common.BaseTest;
import com.jda.dct.e2e.ui.utility.common.LeftMenu;
import com.jda.dct.e2e.ui.utility.helper.AssertHelper.AssertHelper;
import com.jda.dct.e2e.ui.utility.helper.Browser.BrowserHelper;
import com.jda.dct.e2e.ui.utility.helper.Wait.WaitHelper;
import com.jda.dct.e2e.ui.utility.pageobjects.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


public class DemandItemLotSummaryTests extends BaseTest {

    private LeftMenu leftMenu;
    private TopMenuPage topMenu;
    private TopSubMenuPage topSubMenu;
    private DemandShipmentListPage demandShipmentListPage;
    private DemandShipmentDetailsPage demandShipmentDetailsPage;
    private WebDriver driver;
    private SupplyShipmentListPage supplyShipmentListPage;
    private AssertHelper assertHelper;
    private BrowserHelper browserHelper;
    private WaitHelper waitHelper;

    @BeforeClass
    public void setUp() throws InterruptedException {
        System.out.println("Starting tests for :" + this.getClass().getName());
        //Initialize required objects
        driver = getDriver();
        leftMenu = new LeftMenu(driver);
        topMenu = new TopMenuPage(driver);
        topSubMenu = new TopSubMenuPage(driver);
        supplyShipmentListPage = new SupplyShipmentListPage(driver);
        demandShipmentListPage = new DemandShipmentListPage(driver);
        demandShipmentDetailsPage = new DemandShipmentDetailsPage(driver);
        browserHelper = new BrowserHelper(driver);
        assertHelper = new AssertHelper(driver);
        waitHelper = PageFactory.initElements(driver, WaitHelper.class);
        //loginAndNavigateToDemandLandingPage
        logInToApplication();
    }


    @BeforeMethod
    public void commonMethods() {
        //navigate to the Demand Page
        leftMenu.clickDemandMenuOption();
        topMenu.goToDemandShipmentTab();
    }

    @Test(priority = 1)
    //Validate Supply Delivery  page - change method name and assertion
    public void validateIsLotsItemSummaryPaneDisplayed() throws InterruptedException {
        String Shipment = "OS0002_SO_S2";
        topMenu.clickStatesTab();
        topMenu.clickDemandShipmentStatesShipped();
        supplyShipmentListPage.clickOceanIcon();
        topSubMenu.goToSearchFeature(Shipment);
        topMenu.clearFilterIfVisible();
        waitHelper.waitForPageToLoad();
        demandShipmentDetailsPage.clickSearchedShipmentNumber(Shipment);
        demandShipmentListPage.isLotValueDisplayed();
        supplyShipmentListPage.clickLotCount();
        assertHelper.assertTrue(supplyShipmentListPage.isLotSummaryPaneDisplayed(), "Items with Lots are displayed");
        assertHelper.assertTrue(demandShipmentListPage.islotIdDisplayed(), "Lot Ids L027 is displayed");
        assertHelper.assertTrue(demandShipmentListPage.islotIdsDisplayed(), "Lot Ids l014, L015 are displayed");
    }
}






