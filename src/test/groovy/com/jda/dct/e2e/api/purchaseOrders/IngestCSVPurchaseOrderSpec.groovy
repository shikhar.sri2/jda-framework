package com.jda.dct.e2e.api.purchaseOrders

import com.jda.dct.e2e.api.categories.Regression
import com.jda.dct.e2e.api.utility.IngestionHelper
import org.junit.experimental.categories.Category
import spock.lang.Shared
import spock.lang.Specification

/**
 * Ingest Purchase Order in CSV format.
 *
 * @author Arun V
 * @version 1.0-SNAPSHOT
 * @since 12-Jul-2018
 */

class IngestCSVPurchaseOrderSpec extends Specification implements IngestionHelper {

    @Shared
            purchaseOrderURL

    @Shared
            salesOrderURL

    void setup() {
        init()

        def purchaseOrderPath = properties.getProperty('ingestPurchaseOrdersPath')
        purchaseOrderURL = protocol + ip + purchaseOrderPath + '?processType=tempraturecontrolled&purchaseOrderType=standardPO'

        def salesPrderPath = properties.getProperty('ingestSalesOrdersPath')
        salesOrderURL = protocol + ip + salesPrderPath + '?processType=demand&salesOrderType=standardOrder'
    }

    @Category(Regression)
    def "create and ingest a faulty purchaseOrder csv"() {
        given: "a faulty sample purchaseOrder, quantity is not a valid number"
        def filePath = getTmpDir() + 'sample_po.csv'
        new File(filePath).newWriter().withWriter {
            it << 'Order Type,Process Type,Order No,Customer,Customer Name,Supplier,Supplier Name,Buyer Name,Buyer Code,Header Status,Inco Terms,Freight Terms,Freight Carrier,Creation Date,Last Modified Date,Line No,Item Type,Item,Item Name,Supplier Item,Commondity Code,Requested Ship Date,Need By Date,Order Quantity,Order Uom,Sales Order No,Sales Order Line No,Line Status,To,From,Site Name,Material Group,Unit Price,Schedule No,Scheduled Delivery Date,Scheduled Quantity\n' +
                  'standardPO,tempraturecontrolled,1231234567,WH001,BD Maryland Warehouse,BDNJ,BD Factory,John Doe,jdoe,Created,FOB,FOB Origin,,07/09/2018,07/09/2018,1,FG,P100,Plasma Proteome Stabilisation,M001,COM1,,7/21/2018,100,EA,,,Created,WH001,BD Maryland Warehouse,BDNJ,Mobiles,100,BD Factory,07/09/2018,wrong_number\n' +
                  'standardPO,tempraturecontrolled,1231234567,WH001,BD Maryland Warehouse,BDNJ,BD Factory,John Doe,jdoe,Created,FOB,FOB Origin,,07/09/2018,07/09/2018,2,FG,P700,GLP - 1 Stabilisation,M001,COM1,,7/21/2018,250,EA,,,Created,WH001,BD Maryland Warehouse,BDNJ,Mobiles,100,BD Factory,07/09/2018,10'
                   
        }

        when: "ingest into DCT"
        def response = postFile(filePath, purchaseOrderURL, null)
        def ingestionId = getIngestionId(response)
        def status = waitUntilComplete(ingestionId)

        then: "should have status - completed_with_error"
        status.contains('COMPLETED_WITH_ERROR')
        /* getErrors(ingestionId) == [
                [
                        'rowId'       : 1,
                        'errorCode'   : 'dct.io.castError',
                        'errorMessage': 'Scheduled Quantity:wrong_number cannot be cast to double.'
                ]
        ]*/
    }

    // probably redundant
    @Category(Regression)
    def "ingest a large faulty purchaseOrder CSV file"() {
        given:
        def numPurchaseOrder = 500
        def filePath = getTmpDir() + 'sample_po.csv'
        new File(filePath).newWriter().withWriter { writer ->
            writer << 'Order No,Customer Name,Customer Description,Supplier Name,Supplier Description,Buyer Name,Buyer Code,Header Status,Inco Terms,Freight Terms,Freight Carrier,Creation Date,Last Modified Date,Line No,Item Type,Item Name,Item Description,Material,Commondity Code,Requested Ship Date,Need By Date,Order Quantity,Order Uom,Sales Order No,Sales Order Line No,Line Status,To,From,Site Description,Schedule No,Scheduled Delivery Date,Scheduled Quantity\n'
            def templateLine = '<purchaseOrder>,WH001,BD Maryland Warehouse,BDNJ,BD Factory,John Doe,jdoe,Created,FOB,FOB Origin,,07/09/2018,07/09/2018,,FG,P100,Plasma Proteome Stabilisation,M001,COM1,,7/21/2018,100,EA,,,Created,WH001,BD Maryland Warehouse,BDNJ,BD Factory,07/09/2018,20\n'
            numPurchaseOrder.times { purchaseOrder ->
                writer << templateLine.replaceAll('<purchaseOrder>', ('1231235' + purchaseOrder))

            }

        }

        when:
        def response = postFile(filePath, purchaseOrderURL, null)
        def ingestionId = getIngestionId(response)
        def status = waitUntilComplete(ingestionId, "ingesting:$ingestionId", 60)

        then:
        with(status) {
            !contains('COMPLETED')
            contains('COMPLETED_WITH_ERROR')
        }
        getStatusObjects(ingestionId).find { it.get('status') == 'COMPLETED_WITH_ERROR' }
                .get('numProcessed') == 0

    }

    /*def "ingest a purchaseOrder csv file which triggers the computation and verify the exception status"() {
        given:

        def filePath = this.getClass().getClassLoader().getResource('csv/po_data_poComputation.csv').path

        PurchaseOrder purchaseOrder = new PurchaseOrder()
        purchaseOrder.setTid("a4b7a825f67930965747445709011120")
        purchaseOrder.setPurchaseOrderId("PO3")
        purchaseOrder.setPurchaseOrderType("standardPO")
        purchaseOrder.setCustomerName("WH001")
        purchaseOrder.setSupplierName("BDNJ")

        when:
        def response = postFile(filePath, purchaseOrderURL, null)
        def ingestionId = getIngestionId(response)
        def status = waitUntilComplete(ingestionId)

        def iidFor = IidUtil.getIidFor(purchaseOrder)
        sleep(60000)
        def getPurchaseOrderPath = getURLPrefix = protocol + ip + properties.getProperty('getPurchaseOrderPath') + iidFor

        def purchaseOrderText = getHttpClient(header, getPurchaseOrderPath, null, HttpMethod.GET).body
        def purchaseOrderRetrieved = new JsonSlurper().parseText(purchaseOrderText)

        then:
        with(status) {
            contains('COMPLETED')
            !contains('COMPLETED_WITH_ERROR')
        }

        then: 'verify that purchase order exception status is updated'

        purchaseOrderRetrieved.getAt('data').getAt('exceptionStatus').toString() == '[Late]'
        purchaseOrderRetrieved.getAt('data').getAt('purchaseOrderLines').getAt('PL31').getAt('exceptionStatus').toString() == '[On Time]'
        purchaseOrderRetrieved.getAt('data').getAt('purchaseOrderLines').getAt('PL32').getAt('exceptionStatus').toString() == '[Late]'
        purchaseOrderRetrieved.getAt('data').getAt('purchaseOrderLines').getAt('PL33').getAt('exceptionStatus').toString() == '[Late]'
    }
*/

//    def "ingest a sales order csv and purchaseOrder csv file which triggers the computation and verify the exception status on sales order"() {
//        given:
//        def poFilePath = this.getClass().getClassLoader().getResource('csv/po_data_soComputation.csv').path
//        def soFilePath = this.getClass().getClassLoader().getResource('csv/so_data_computation.csv').path
//
//        SalesOrder salesOrder = new SalesOrder()
//
//        salesOrder.setTid("a4b7a825f67930965747445709011120")
//        salesOrder.setSalesOrderId("SL02")
//        salesOrder.setSalesOrderType("standardOrder")
//        salesOrder.setSupplierName("WH001")
//        salesOrder.setCustomerName("SOCUS001")
//
//        def iidFor = IidUtil.getIidFor(salesOrder)
//
//        def po = new PurchaseOrder()
//        po.tid = 'a4b7a825f67930965747445709011120'
//        po.purchaseOrderId = 'PO4'
//        po.purchaseOrderType = 'standardPO'
//        po.customerName = 'WH001'
//        po.supplierName = 'BDNJ'
//        def poId = po.getId()
//
//        def getPoPath = protocol + ip + properties.getProperty('getPurchaseOrderPath')
//        def getSoPath = protocol + ip + properties.getProperty('getSalesOrderPath')
//        def getSalesOrderPath = getSoPath + iidFor
//
//        deleteObject(getPoPath, poId)
//        deleteObject(getSoPath, iidFor)
//
//
//        when:
//        def soResponse = postFile(soFilePath, salesOrderURL, null)
//        // waiting for the SO to be persisted
//        print soResponse
//        def soingestionId = getIngestionId(soResponse)
//        def soStatus = waitUntilComplete(soingestionId)
//        // expecting the soStatus to be successful and not verifying as this test case is to
//        // verify the computation of PO and identify the exceptions if any
//        // increasing sleep time, so that the SO can be persisted into the elastic. Just a hope.
//        sleep(60000)
//        def response = postFile(poFilePath, purchaseOrderURL, null)
//        def ingestionId = getIngestionId(response)
//        def status = waitUntilComplete(ingestionId)
//        sleep(60000)
//        def salesOrderText = getHttpClient(header, getSalesOrderPath, null, HttpMethod.GET).body
//        def salesOrderRetrieved = new JsonSlurper().parseText(salesOrderText)
//
//        then: 'verify that sales order exception status is updated'
//        with(status) {
//            contains('COMPLETED')
//            !contains('COMPLETED_WITH_ERROR')
//        }
//
//        salesOrderRetrieved.getAt('data').getAt('exceptionStatus').toString() == '[Late]'
//        salesOrderRetrieved.getAt('data').getAt('salesOrderLines').getAt('SL21').getAt('exceptionStatus').toString() == '[On Time]'
//        salesOrderRetrieved.getAt('data').getAt('salesOrderLines').getAt('SL22').getAt('exceptionStatus').toString() == '[Late]'
//        salesOrderRetrieved.getAt('data').getAt('salesOrderLines').getAt('SL23').getAt('exceptionStatus').toString() == '[Late]'
//    }

}