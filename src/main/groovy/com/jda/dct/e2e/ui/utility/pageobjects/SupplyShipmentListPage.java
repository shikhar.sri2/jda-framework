package com.jda.dct.e2e.ui.utility.pageobjects;

import com.jda.dct.e2e.ui.utility.action.CommonActions;
import com.jda.dct.e2e.ui.utility.common.BasePage;
import com.jda.dct.e2e.ui.utility.helper.Action.ActionHelper;
import com.jda.dct.e2e.ui.utility.helper.Wait.WaitHelper;
import com.jda.dct.e2e.ui.utility.helper.genericHelper.GenericHelper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class SupplyShipmentListPage extends BasePage {

    WebDriver driver;
    WebDriverWait webDriverWait;

    WaitHelper objWaitHelper;
    GenericHelper objGenericHelper;
    CommonActions commonActions;
    ActionHelper actionHelper;

    public SupplyShipmentListPage(WebDriver webdriver) {
        this.driver = webdriver;
        PageFactory.initElements(driver, this);
        webDriverWait = new WebDriverWait(driver, 30);
        objWaitHelper = PageFactory.initElements(driver, WaitHelper.class);
        objGenericHelper = PageFactory.initElements(driver, GenericHelper.class);
        commonActions = new CommonActions(driver);
        actionHelper = new ActionHelper(driver);
    }

    @FindBy(id = "PanelSummary-RenderedItem-Shipment-No-1-LinkItem")
    private WebElement supplyShipmentListPage;
    //@FindBy(xpath = "//span[contains(text(),'Shipment Details')]")
    @FindBy(xpath = "//*[contains(@id,'shipment-details')]")
    private WebElement supplyShipmentDetailsPage1;
    @FindBy(id="PanelSummary-RenderedItem-Shipment-No-1-LinkItem")
    private WebElement supplyShipmentNumber;
    @FindBy(xpath = "//*[@id='PanelSummary-RenderedItem-Item-Name-1']/following::*[@id='ExpansionPanel-ExpandMoreIcon'][1]")
    private WebElement expandButton;
    @FindBy(id = "panelDetailSlider")
    private WebElement supplyLinePanelSlider;
    @FindBy(xpath = "//*[@id='PanelSummary-RenderedItem-Container-No-1-LinkItem']")
    private WebElement containerNumber;
    @FindBy(xpath = "//*[@id='PanelSummary-RenderedItem-Trailer-No-1-LinkItem']")
    private WebElement trailerNumber;
    @FindBy (xpath = "(//div[@role='button']/span/following-sibling::*)[1]")
    private WebElement firstFilter;
    @FindBy(xpath = "//button[text()=\"Clear All\"]")
    private WebElement clearAllFilter;
    @FindBy(id="breadcrumb-shipment")
    private WebElement backToShipmentPage;
    @FindBy(xpath = "//button[@value='Road']/span/div")
    private WebElement roadIcon;
    @FindBy(xpath = "//button[@value='Ocean']/span/div")
    private WebElement shipIcon;
    @FindBy(xpath ="//p[@id='PanelSummary-RenderedItem-Lots-0-CustomerItem']/a[1]")
    private WebElement lotCount;
    @FindBy(xpath ="//a[text()='2']")
    private WebElement lotnumber;
    @FindBy(xpath = "//*[starts-with(@id,'AC')]")
    private WebElement lotSummaryPane;
    @FindBy(xpath = "//span[text()='L097, L027']")
    private WebElement lotIds;




    @FindBy(xpath = "//*[contains(@id,'supply-inboundShipment-Filter-List-Card')]//span")
    private List<WebElement> shipmentExceptionStatusCount;
    @FindBy(xpath = "//*[contains(@id,'demand-inboundDelivery-Filter-List-Card')]/span")
    private List<WebElement> deliveryExceptionStatusCount;
    //========Methods=====
    //Assertion methods as the page lands on shipment tab by default

    public boolean isSupplyShipmentListPageDisplayed() {
        objWaitHelper.waitElementToBeVisible(supplyShipmentListPage);
        return objGenericHelper.isDisplayed(supplyShipmentListPage, "Supply Shipment lis page");
    }

    public boolean isSupplyShipmentDetailsPageDisplayed() {
        objWaitHelper.waitElementToBeVisible(supplyShipmentDetailsPage1);
        return objGenericHelper.isDisplayed(supplyShipmentDetailsPage1,"Supply shipment page");
    }

    public boolean displaypanelslider() {
        webDriverWait.until(ExpectedConditions.visibilityOf(supplyLinePanelSlider));
        return supplyLinePanelSlider.isDisplayed();
    }


    //click on first shipment number
    public void clickSupplyShipmentNumber() {
        objWaitHelper.waitAndClickElement(supplyShipmentNumber,"Shipment no.");
    }

    //click expand button
    public void clickExpandButton() {
        objWaitHelper.waitAndClickElement(expandButton, "Expand Button");
    }

    //click on container number
    public void clickSupplyContainerNumber() {

        objWaitHelper.waitAndClickElement(containerNumber, "Container Number");
    }

    //click on trailer number
    public void clickSupplyTrailerNumber() {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        objWaitHelper.waitAndClickElement(trailerNumber, "Trailer Number");
    }

    public void clickRoadIcon() {
        objWaitHelper.waitAndClickElement(roadIcon, "Road Icon");
    }

    public void clickOceanIcon()  {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        objWaitHelper.waitAndClickElement(shipIcon, "Ship icon");
    }

    public void clickFirstFilter() {

        webDriverWait.until(ExpectedConditions.elementToBeClickable(firstFilter));
        firstFilter.click();
    }

    public void clickShipmentExceptionHavingData() throws InterruptedException{
        Thread.sleep(1000);
        commonActions.clickExceptionStatusHavingData(shipmentExceptionStatusCount);
    }

    public void clickDeliveryExceptionsHavingData() {
        commonActions.clickExceptionStatusHavingData(deliveryExceptionStatusCount);
    }


    public void clickLotCount() {
        objWaitHelper.waitAndClickElement(lotCount, "lot count");

    }

    public boolean isLotValueDisplayed() throws InterruptedException {
        objWaitHelper.waitElementToBeVisible(lotnumber);
        return lotnumber.isDisplayed();
    }

    public boolean isLotSummaryPaneDisplayed() throws InterruptedException {
        objWaitHelper.waitElementToBeVisible(lotSummaryPane);
        return lotSummaryPane.isDisplayed();
    }

    public void clickClearAll() {
        objWaitHelper.waitAndClickElement(clearAllFilter, "lot count");

    }

    public boolean islotIdsDisplayed() throws InterruptedException {

        if (lotIds.isDisplayed()) return true;
        else return false;
    }


}