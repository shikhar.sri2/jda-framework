package com.jda.dct.e2e.api.items

import com.jda.dct.e2e.api.categories.Regression
import com.jda.dct.e2e.api.utility.IngestionHelper
import org.junit.experimental.categories.Category
import spock.lang.Shared
import spock.lang.Specification

class IngestFaultyItemsSpec extends Specification implements IngestionHelper {

    @Shared ItemShowcaseData sample = new ItemShowcaseData()
    @Shared Map<String, String> modifiedFields = new HashMap<>()

    @Category(Regression)
    def "Ingestion status of an invalid item becomes complete with error"() {
        given: "a item data, whose itemType is not valid"
        modifiedFields.clear()
        modifiedFields.put("itemType", "wrong-item-type")
        def modifiedItem = sample.modifyItemMultiFields("AC-FG-048", modifiedFields, "tmp-item.json")

        when: 'a item json ingested'
        def ingestionId = ingestData(modifiedItem, "items?itemType=Sold")
        def status = waitUntilComplete(ingestionId)

        then: 'ingestion status eventually becomes COMPLETED_WITH_ERROR'
        status.contains('COMPLETED_WITH_ERROR')
    }
}
