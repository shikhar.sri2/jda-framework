package com.jda.dct.e2e.ui.pagination;


import com.jda.dct.e2e.ui.utility.common.BaseTest;
import com.jda.dct.e2e.ui.utility.common.LeftMenu;
import com.jda.dct.e2e.ui.utility.pageobjects.*;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class MasterDataPaginationTests extends BaseTest {

    private LeftMenu leftMenu;
    private TopMenuPage topMenu;
    private TopSubMenuPage topSubMenu;
    private MasterDataItemPage masterDataItems;
    private MasterDataSitesPage masterDataSites;
    private MasterDataNodesPage masterDataNodes;
    private MasterDataParticipantsPage masterDataParticipant;
    private MasterDataSourcingLanesPage masterDataSourcingLanes;
    private WebDriver driver;

    @BeforeClass
    public void setUp() throws InterruptedException {
        System.out.println("Starting tests for :" + this.getClass().getName());
        //Initialize required objects
        driver = getDriver();
        leftMenu = new LeftMenu(driver);
        topMenu = new TopMenuPage(driver);
        topSubMenu = new TopSubMenuPage(driver);
        masterDataItems = new MasterDataItemPage(driver);
        masterDataSites = new MasterDataSitesPage(driver);
        masterDataNodes = new MasterDataNodesPage(driver);
        masterDataParticipant = new MasterDataParticipantsPage(driver);
        masterDataSourcingLanes = new MasterDataSourcingLanesPage(driver);
        //login to Application
        logInToApplication();
    }

    @Test(priority = 1,retryAnalyzer = BaseTest.class)
    public void validateMasterDataItemsPaginationfeature() throws InterruptedException {
        leftMenu.clickMasterDataMenuOption();
        topMenu.goToMasterDataItemTab();
        System.out.println("Clicked on Items");
        topSubMenu.validatePagination();
    }

    public void validateMasterDataSitesPaginationfeature() throws InterruptedException{
        leftMenu.clickMasterDataMenuOption();
        topMenu.goToMasterDataSitesTab();
        topSubMenu.validatePagination();
        //Assertion is included in method
    }

    @Test(priority = 2,retryAnalyzer = BaseTest.class)
    public void validateMasterDataNodesPaginationfeature() throws InterruptedException{
        leftMenu.clickMasterDataMenuOption();
        topMenu.goToMasterDataNodesTab();
        topSubMenu.validatePagination();
        //Assertion is included in method
    }

    @Test(priority = 3,retryAnalyzer = BaseTest.class)
    public void validateMasterDataParticipantsPaginationfeature()throws InterruptedException {
        leftMenu.clickMasterDataMenuOption();
        topMenu.goToMasterDataParticipantsTab();
        topSubMenu.validatePagination();
        //Assertion is included in method
    }

}