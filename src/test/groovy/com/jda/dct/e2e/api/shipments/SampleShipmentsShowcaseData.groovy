package com.jda.dct.e2e.api.shipments

import com.jda.dct.e2e.api.utility.IngestionHelper
import groovy.json.JsonBuilder
import groovy.json.JsonSlurper
import org.apache.groovy.json.internal.LazyMap
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.HttpMethod
import spock.util.concurrent.PollingConditions

class SampleShipmentsShowcaseData implements IngestionHelper{

    private final Logger LOGGER = LoggerFactory.getLogger(SampleShipmentsShowcaseData.class)

    SampleShipmentsShowcaseData() {}

    def getShipmentsFromShowcase(String shipmentId) {
        init()
        def getPath = properties.getProperty('getShipmentsPath')
        def getURLPrefix = protocol + ip + getPath
        println "Url prefix is: " + getURLPrefix
        def conditions = new PollingConditions(timeout: 15, initialDelay: 1)
        def res = null
        conditions.eventually {
            res = executeHttpCall(header, getURLPrefix + "?query=shipmentId=" + shipmentId, null, HttpMethod.GET)
            println "Executing at URL: " + getURLPrefix + "?query=shipmentId=" + shipmentId
            assert res != null
        }
        if (res['data'] == null || res['data'].size() == 0) {
            LOGGER.warn("getShipmentsFromShowcase with shipmentid: {}, data return null", shipmentId)
        }
        def shipment = res['data'][0]
        return shipment
    }
    def modifyShipmentMultiFields(String shipmentId, Map<String, String> fields, String filename) {
        List<Object> shipmentsList = new ArrayList<>()
        def shipment = getShipmentsFromShowcase(shipmentId)
        if (fields.isEmpty()) {
            return null
        }
        for (String field : fields.keySet()) {
            ((LazyMap)shipment).put(field, fields.get(field))
        }
        new File(filename).write(new JsonBuilder(shipment).toPrettyString())
        def updatedShipment =  new JsonSlurper().parseText(new File(filename).text)
        new File(filename).delete()
        shipmentsList.add(updatedShipment)
        return shipmentsList
    }
}
