package com.jda.dct.e2e.api.shipments

import com.jda.dct.e2e.api.categories.Smoke
import com.jda.dct.e2e.api.utility.IngestionHelper
import com.jda.dct.e2e.api.utility.QueryObject
import com.jda.dct.e2e.api.utility.QueryResultHandler
import org.junit.experimental.categories.Category
import spock.lang.Specification
import spock.lang.Unroll

class SupplyShipmentTextSearchSpec extends Specification implements IngestionHelper {

    @Category(Smoke)
    @Unroll
    def "Search a full text"(String field, String text) {
        given: "all outboundShipments"
        def queryObject = new QueryObject()
        queryObject.setType("shipments")
        queryObject.addFullTextQuery()
                .searchText(text)
                .searchField(field)
        queryObject.addQuery("shipmentType", "inboundShipment")

        when: "searchText is Shipment No "
        def response = QueryResultHandler.getQueryResultHandler().executeWithRetry(queryObject, 20, 5, 5, 1, 1)
        then: "all shipments that include Shipped as text will return"
        response.body.totalCount != null
        and:
        response.body.data.size() > 0

        /*response.body.data[field].each({ v ->
            assert v == text
        })*/

        where:
        field             | text
        ""                | '"ISPTA06"'
        ""                | '"ISPTA07"'
        ""                | '"ISPTA08"'

        //  "state"           | "Shipped"
        //  "processType"     | "demand"
        //  "exceptionStatus" | "On Time"

    }

    @Category(Smoke)
    def "Search an invalid text"() {
        given: "all outboundShipments"
        def queryObject = new QueryObject()
        queryObject.setType("shipments")
        queryObject.addFullTextQuery()
                .searchText("abc123")
                .searchField("state")
        queryObject.addQuery("shipmentType", "inboundShipment")

        when: "searchText is abc123 "
        def response = QueryResultHandler.getQueryResultHandler().execute(queryObject)
        then: "no data should return"

        assert response.body.data.size() == 0

    }
}
