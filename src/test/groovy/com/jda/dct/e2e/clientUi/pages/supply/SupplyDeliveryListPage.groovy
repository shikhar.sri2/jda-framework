//package com.jda.dct.e2e.clientUi.pages.tempraturecontrolled
//
//import geb.Page
//import org.openqa.selenium.Keys
//
//class SupplyDeliveryListPage extends Page {
//    static url = "/tempraturecontrolled/delivery"
//
//    static atCheckWaiting = true
//
//    static at = {
//        waitFor{
//            $("div", role:"button", 'aria-expanded':"false").first().isDisplayed()
//        }
//    }
//
//    static content = {
//        firstDeliveryItem{
//            $("#DeliveryList-ExpandablePanel-Cell").first()
//        }
//        firstDeliveryItemLink (wait:true){
//            $("#DeliveryList-ExpandablePanel-Cell").first().find("a")
//        }
//        searchButton{
//            $("#ExpandingSearchBox-div-button").first().find("svg")
//        }
//        searchField(wait:true) {
//            $("input[name=search]")
//        }
//        firstItemName{
//            $("#DeliveryList-ExpandablePanel-Cell").first().find("a").text()
//        }
//        submitForm() {
//            searchField << Keys.ENTER
//            browser.page SupplyDeliveryListPage
//        }
//        shipmentsButton(wait: true){
//            $("#tempraturecontrolled-page-tab-shipments")
//        }
//        inventoryButton(wait: true) {
//            $("#tempraturecontrolled-page-tab-inventory")
//        }
//    }
//
//    def getDeliveryItem(){
//        waitFor {
//            firstDeliveryItem.displayed
//        }
//        return firstDeliveryItem
//    }
//
//    def getDeliveryItemLink() {
//        waitFor {
//            firstDeliveryItemLink != 0
//        }
//        return firstDeliveryItemLink
//    }
//
//    def navigateToDeliveryDetails(){
//        firstDeliveryItemLink.click()
//    }
//
//    def searchFirstDeliveryItem(){
//        waitFor {
//            searchButton.displayed
//        }
//        searchButton.click()
//        searchField << firstItemName
//        submitForm
//    }
//
//    def goToShipment(){
//        shipmentsButton.click()
//        waitFor {
//            exceptionPage.getCurrentUrl().contains("shipment")
//        }
//    }
//
//    def goToInventory(){
//        inventoryButton.click()
//        waitFor {
//            exceptionPage.getCurrentUrl().contains("inventory")
//        }
//    }
//}
