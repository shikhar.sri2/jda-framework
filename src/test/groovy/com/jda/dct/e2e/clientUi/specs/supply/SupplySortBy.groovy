//package com.jda.dct.e2e.clientUi.specs.tempraturecontrolled
//
//import org.openqa.selenium.By
//import org.openqa.selenium.WebDriver
//import org.openqa.selenium.firefox.FirefoxDriver
//import java.util.concurrent.TimeUnit
//
//class SupplySortBy {
//
//    //Test Login, goto WatchTower main page, click Supply from Left Panel Menu///
//    //When at Homepage: Click OnTime, Early, Delayed, Late and PTA Unknown
//    //Login to the Supply w CH Browser
//
//    static void main(String[] args) throws Exception {
//        //Start FF Browser, Clear Cache and Cookies, Set Browser to Max Dimensions//
//        System.out.println(("Starting FF Browswer window to Login page, clearing cache and cookies setting to max dimensions"));
//        System.setProperty("webdriver.gecko.exceptionPage", "C:\\Users\\1024555\\Desktop\\Env\\drivers\\geckodriver.exe");
//        WebDriver exceptionPage = new FirefoxDriver();
//        exceptionPage.get("https://dctui-intergration.azurewebsites.net/login");
//        exceptionPage.manage().window().maximize();
//
//        //Click Sign-On Button
//        exceptionPage.findElement(By.cssSelector(".login-button-wrapper")).click();
//        //Click in Username text field then enter password
//        exceptionPage.findElement(By.id("i0116")).click();
//        exceptionPage.findElement(By.id("i0116")).sendKeys("mra@dcttestllc.onmicrosoft.com");
//        exceptionPage.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
//        //Click SignIn Button
//        exceptionPage.findElement(By.id("idSIButton9")).click();
//        Thread.sleep(3000)
//        //Send Password to Signin text field
//        exceptionPage.findElement(By.id("i0118")).sendKeys("coco!@#4");
//        Thread.sleep(3000)
//        //Click SignIn Button
//        exceptionPage.findElement(By.id("idSIButton9")).click();
//        //Click SignIn Button
//        exceptionPage.findElement(By.id("idSIButton9")).click();
//        //// exceptionPage.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
//        Thread.sleep(10000)
//
//        //WatchTower: Click Supply icon left panel menu//
//        System.out.println("Navigating to Left Panel Menu, goto Supply Main Page...")
//        //Click Supply Icon Left Panel Menu
//        exceptionPage.findElement(By.xpath("/html/body/div/div/div[2]/div[1]/header/div/div/div[1]/button")).click();
//        Thread.sleep(2000);
//        exceptionPage.findElement(By.xpath("/html/body/div[2]/div[2]/div/div/ul/div/a[2]/div/div/span")).click();
//        Thread.sleep(5000);
//
//
//
//        //Sort By Supplier
//        System.out.println("Sort By: Selecting Each element in the drop down menu...")
//        exceptionPage.findElement(By.xpath("//div[3]/div/div/div/div/div/div/div[3]/div/div/div/div/div/div")).click();
//        exceptionPage.findElement(By.xpath("//div[2]/ul/li[2]")).click();
//        System.out.println("Sort By: Supplier...")
//        Thread.sleep(1000)
//        //Sort By Customer: div > li
//        exceptionPage.findElement(By.xpath("//div[3]/div/div/div/div/div/div/div[3]/div/div/div/div/div/div")).click();
//        exceptionPage.findElement(By.xpath("//div[2]/ul/li[3]")).click();
//        System.out.println("Sort By: Customer...")
//        Thread.sleep(1000)
//        //Sort By Carrier
//        exceptionPage.findElement(By.xpath("//div[3]/div/div/div/div/div/div/div[3]/div/div/div/div/div/div")).click();
//        exceptionPage.findElement(By.xpath("//div[2]/ul/li[4]")).click();
//        System.out.println("Sort By: Carrier...")
//        Thread.sleep(1000)
//        //Sort By Vessel
//        exceptionPage.findElement(By.xpath("//div[3]/div/div/div/div/div/div/div[3]/div/div/div/div/div/div")).click();
//        exceptionPage.findElement(By.xpath("//div[2]/ul/li[5]")).click();
//        System.out.println("Sort By: Vessel...")
//        Thread.sleep(1000)
//        //Sort By BOL No
//        exceptionPage.findElement(By.xpath("//div[3]/div/div/div/div/div/div/div[3]/div/div/div/div/div/div")).click();
//        exceptionPage.findElement(By.xpath("//li[6]")).click();
//        System.out.println("Sort By: BOL No...")
//        Thread.sleep(1000)
//        //Sort By Departure Port
//        exceptionPage.findElement(By.xpath("//div[3]/div/div/div/div/div/div/div[3]/div/div/div/div/div/div")).click();
//        exceptionPage.findElement(By.xpath("//li[7]")).click();
//        System.out.println("Sort By: Departure Port...")
//        Thread.sleep(1000)
//        //Sort By Arrival Port
//        exceptionPage.findElement(By.xpath("//div[3]/div/div/div/div/div/div/div[3]/div/div/div/div/div/div")).click();
//        exceptionPage.findElement(By.xpath("//li[8]")).click();
//        System.out.println("Sort By: Arrival Port...")
//        Thread.sleep(1000)
//        //Sort By ETA
//        exceptionPage.findElement(By.xpath("//div[3]/div/div/div/div/div/div/div[3]/div/div/div/div/div/div")).click();
//        exceptionPage.findElement(By.xpath("//li[9]")).click();
//        System.out.println("Sort By: ETA...")
//        Thread.sleep(1000)
//        //Sort By PTA
//        exceptionPage.findElement(By.xpath("//div[3]/div/div/div/div/div/div/div[3]/div/div/div/div/div/div")).click();
//        exceptionPage.findElement(By.xpath("//li[10]")).click();
//        System.out.println("Sort By: PTA...")
//        Thread.sleep(1000)
//        //Sort By Creation Date
//        exceptionPage.findElement(By.xpath("//div[3]/div/div/div/div/div/div/div[3]/div/div/div/div/div/div")).click();
//        exceptionPage.findElement(By.xpath("//li[11]")).click();
//        System.out.println("Sort By: Createion Date...")
//        Thread.sleep(1000)
//        //Sort By Last Modified
//        exceptionPage.findElement(By.xpath("//div[3]/div/div/div/div/div/div/div[3]/div/div/div/div/div/div")).click();
//        exceptionPage.findElement(By.xpath("//li[12]")).click();
//        System.out.println("Sort By: Last Modified...")
//        Thread.sleep(1000)
//        //Sort By Shipment No
//        exceptionPage.findElement(By.xpath("//div[3]/div/div/div/div/div/div/div[3]/div/div/div/div/div/div")).click();
//        exceptionPage.findElement(By.xpath("//div[2]/ul/li")).click();
//        Thread.sleep(1000)
//        System.out.println("Sort By: Shipment No...")
//        System.out.println("End of Sort By Test...")
//        exceptionPage.close()
//        exceptionPage.quit()
//        //Logic: enter verify statement
//    }
//}