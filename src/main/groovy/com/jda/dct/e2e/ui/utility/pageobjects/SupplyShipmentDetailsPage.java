package com.jda.dct.e2e.ui.utility.pageobjects;

import com.jda.dct.e2e.ui.utility.action.CommonActions;
import com.jda.dct.e2e.ui.utility.common.BasePage;
import com.jda.dct.e2e.ui.utility.helper.Wait.WaitHelper;
import com.jda.dct.e2e.ui.utility.helper.genericHelper.GenericHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SupplyShipmentDetailsPage extends BasePage {

    WebDriver driver;
    WebDriverWait webDriverWait;
    WaitHelper waitHelper;
    GenericHelper genericHelper;
    CommonActions commonActions;


    public SupplyShipmentDetailsPage(WebDriver webdriver) {
        this.driver = webdriver;
        PageFactory.initElements(driver, this);
        webDriverWait = new WebDriverWait(driver, 50);
        waitHelper = new WaitHelper(driver);
        genericHelper = new GenericHelper(driver);
        commonActions = new CommonActions(driver);
    }

    @FindBy(xpath = "//*[@id='PanelSummary-RenderedItem-Notes-0-NotesItem']/a")
    private WebElement notes;
    @FindBy(xpath ="//*[@id='notesDrawer']//span[text()='Notes']")
    private WebElement notesTitle;
    @FindBy(id ="DrawerNav-CloseIcon")
    private WebElement backToNotes;
    @FindBy(xpath = "//textarea[@placeholder='add a note']")
    private WebElement noteSearchbox;
    @FindBy(xpath = "//button[@aria-label='Clear']")
    private WebElement deleteButton;
    @FindBy(xpath = "//button[@aria-label='Create']")
    private WebElement saveButton;
    @FindBy(css = "//aside[contains(.,'Supplytest')]")
    private WebElement saveNotes;


    // @FindBy(xpath = "(//a[@id = \"undefined-LinkItem\"])[3]")
    @FindBy(xpath = "//*[@id='PanelSummary-RenderedItem-All-Items-0']//*[@id='undefined-LinkItem'][1]")
    private WebElement shipmentAllItems;
    @FindBy(xpath = "//*[@id='PanelSummary-RenderedItem-Hot-Items-0']//*[@id='undefined-LinkItem'][1]")
    private WebElement shipmentHotItems;
    @FindBy(xpath = "//*[@id='PanelSummary-RenderedItem-All-Orders-0']//*[@id='undefined-LinkItem'][1]")
    private WebElement shipmentAllorders;
    @FindBy(xpath = "//*[@id='PanelSummary-RenderedItem-Hot-Orders-0']//*[@id='undefined-LinkItem'][1]")
    private WebElement shipmentHotOrders;
    @FindBy(id = "PanelSummary-RenderedItem-Shipment-No-1-LinkItem")
    private WebElement searchedShipmentNumber;

    @FindBy(id = "PanelSummary-RenderedItem-Container-No-0-LinkItem")
    private WebElement containerNumber;

    //"+searchValue+"
    //Webelements inside container panel
    @FindBy(xpath = "//*[@id='Shipments-Panel-Cell']//*[@id='PanelSummary-RenderedItem-All-Items-0']//*[@id='undefined-LinkItem']")
    private WebElement containerAllitems;
    @FindBy(xpath = "//*[@id='Shipments-Panel-Cell']//*[@id='PanelSummary-RenderedItem-Hot-Items-0']//*[@id='undefined-LinkItem']")
    private WebElement containerHotitems;
    // goto Supply Inventory Page
    @FindBy(xpath = "//*[@id='breadcrumb-inventory']")
    private WebElement inventoryListPage1;

    @FindBy(xpath ="//*[@id='Shipments-Panel-Cell']//*[@id='PanelSummary-RenderedItem-All-Orders-0']//*[@id='undefined-LinkItem']")
    private WebElement containerAllOrders;
    @FindBy(xpath ="//*[@id='Shipments-Panel-Cell']//*[@id='PanelSummary-RenderedItem-Hot-Orders-0']//*[@id='undefined-LinkItem']")
    private WebElement containerHotOrders;
    //------------webelements for navigation assertions
    // goto Supply order Page
    @FindBy(xpath = "//*[@href='/supply/purchase-orders']")
    private WebElement supplyOrderListPage1;
    // goto Supply Delivery Page
    //@FindBy(xpath = "//span[text()='   Delivery   ']")
    @FindBy(id ="supply-page-tab-deliveries")
    private WebElement supplyDeliveryListPage1;//1-keeping BCL as asertion as  page is true but itemno is not visible..until fixed
    //@FindBy(xpath="//a[@href=\"/supply/shipment\"]")
    @FindBy(xpath = "//a[@id = \"breadcrumb-shipment\"]")
    private WebElement backToSupplyShipmentTab;


    //methods for navigation from Supply Shipment List to Supply Shipment Details Page


    //-------methods for object navigation inside SupplyShipment NO

    public void clickShipmentAllitems() {
        waitHelper.waitElementToBeClickable(shipmentAllItems);
        genericHelper.elementClick(shipmentAllItems, "shipmentAllItems");
    }


    public void clickShipmentHotitems() {
        waitHelper.waitElementToBeClickable(shipmentHotItems);
        genericHelper.elementClick(shipmentHotItems, "shipmentHotItems");
    }

    public void clickShipmentHotorders() {
        waitHelper.waitAndClickElement(shipmentHotOrders, "Hot Orders");
    }


    public void clickShipmentAllOrders() {
        waitHelper.waitAndClickElement(shipmentAllorders, "All orders");
    }

    public void clickNotes() {
        waitHelper.waitAndClickElement(notes, "Notes ");
    }

    public boolean isNotesPageDisplayed() {
        return waitHelper.waitAndClickElement(notesTitle, "Notes Title");
    }

    public void clickBackToNotes() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(backToNotes));
        backToNotes.click();
    }

//-----------methods to navigate container details

    public void clickContainerNumber() {
        waitHelper.waitAndClickElement(containerNumber, "Container Number");
    }

    public void clickContainerAllitems() {
        waitHelper.waitAndClickElement(containerAllitems, "Container All Items");
    }


    public void clickContainerHotitems() {
        waitHelper.waitAndClickElement(containerHotitems, "Container Hot Items");
    }


    public void clickContainerAllorders() {
        waitHelper.waitAndClickElement(containerAllOrders, "Container All orders");
    }


    public void clickContainerHotorders() {
        waitHelper.waitAndClickElement(containerHotOrders, "Container Hot orders");
    }

    public void clickBackToSupplyShipmentTab() {
        waitHelper.waitAndClickElement(backToSupplyShipmentTab, "Shipment Page link");
    }


    //----------assertion methods--------

    //displays shipment line details

    public boolean isInventoryPageDisplayed() {
        return waitHelper.waitAndClickElement(inventoryListPage1, "Inventory list page");
    }

    public boolean isSupplyOrderPageDisplayed() {
        waitHelper.waitElementToBeVisible(supplyOrderListPage1);
/*        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/
        return genericHelper.isDisplayed(supplyOrderListPage1, "Supply Order list page");
    }


    public boolean isSupplyDeliveryPageDisplayed() {
        waitHelper.waitElementToBeVisible(supplyDeliveryListPage1);
        return genericHelper.isDisplayed(supplyDeliveryListPage1, "Supply delivery list page");
    }

    public boolean isNotesTitleDisplayed() {
        webDriverWait.until(ExpectedConditions.visibilityOf(notesTitle));
        return notesTitle.isDisplayed();
    }

//    public boolean isSearchedShipmentNoDisplayed() {
//        webDriverWait.until(ExpectedConditions.visibilityOf(searchedShipmentNumber));
//
//        return searchedShipmentNumber.isDisplayed();
//    }


    public boolean isSearchShipmentNumberDisplayed(String searchValue) throws InterruptedException {

        WebElement webElement = driver.findElement(By.xpath("//a[contains(text(),'" + searchValue + "')]"));
        Thread.sleep(3000);

        webDriverWait.until(ExpectedConditions.visibilityOf(webElement));
        Thread.sleep(3000);
        return webElement.isDisplayed();
    }


    public void clickSearchedShipmentNumber(String searchedshipmentNumber) {
        waitHelper.waitAndClickElement(searchedShipmentNumber, "Searched Shipment Number");
    }


}