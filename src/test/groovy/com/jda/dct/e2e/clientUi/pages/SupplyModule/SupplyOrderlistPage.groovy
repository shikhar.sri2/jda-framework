package com.jda.dct.e2e.clientUi.pages.SupplyModule

import geb.Page


class SupplyOrderlistPage extends Page{


    static url = "/tempraturecontrolled/purchase-orders"

    static atCheckWaiting = true

    static at = {

            $("#tempraturecontrolled-page-tab-purchase-orders").isDisplayed()
        }


    static content = {


        orderNo(wait:true){

            $("#demandpurchaseorder-list-link-item-1>a").first()

        }

        purchaseordertitle(wait:true){

         $( "#purchase-order-title")

         }

    }



    def gotoSupplyOrder() {

        waitFor {
            orderNo.displayed
        }
        orderNo.click()

        println("welcome to order page")

        waitFor {
            purchaseordertitle.displayed
        }
    }
}









