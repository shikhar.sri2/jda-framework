package com.jda.dct.e2e.ui.utility.pageobjects;


import com.jda.dct.e2e.ui.utility.common.BasePage;
import com.jda.dct.e2e.ui.utility.helper.Wait.WaitHelper;
import com.jda.dct.e2e.ui.utility.helper.genericHelper.GenericHelper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MasterDataSourcingLanesPage extends BasePage {

    WebDriver driver;
    WebDriverWait webDriverWait;
    WaitHelper waitHelper;
    GenericHelper genericHelper;

    public MasterDataSourcingLanesPage(WebDriver webdriver) {

        this.driver = webdriver;
        PageFactory.initElements(driver, this);
        webDriverWait = new WebDriverWait(driver, getTimeOutInSeconds());
        waitHelper = new WaitHelper(driver);
        genericHelper = new GenericHelper(driver);
    }


    @FindBy(id ="PanelSummary-RenderedItem-From-Participant-0-LinkItem")
    private WebElement masterDataSourcingLanesListPage;

    @FindBy(xpath = "//div[@role ='button']/div/span/*")
    private WebElement masterDataSourcingGridButton;

    @FindBy(xpath = "//span[contains(text(),'chevron_right')]")
    private WebElement masterDataSourcingmasterDataButton;

    @FindBy(xpath = "//span[contains(text(),'chevron_right')]")
    private WebElement masterDataSourcingSummaryPane;

    //---Methods--

    public void clickSourcingGridButton() {
        waitHelper.waitAndClickElement(masterDataSourcingGridButton, "MasterDataSourcing grid button");
    }

    public void clicksourcingmasterDataButton() {
        waitHelper.waitAndClickElement(masterDataSourcingmasterDataButton, "MasterDataSourcing master data button");
    }

    public boolean isSourcingLanesSummaryPaneDisplayed() {
        waitHelper.waitElementToBeVisible(masterDataSourcingSummaryPane);
        return genericHelper.isDisplayed(masterDataSourcingSummaryPane, "MasterDataSourcing Summary Pane");
    }

    public boolean isSourcingLanesListPageDisplayed() {
        waitHelper.waitElementToBeVisible(masterDataSourcingLanesListPage);
        return genericHelper.isDisplayed(masterDataSourcingLanesListPage, "MasterDataSourcingList Page");
    }

}