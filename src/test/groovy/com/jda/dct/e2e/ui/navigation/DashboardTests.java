package com.jda.dct.e2e.ui.navigation;

import com.jda.dct.e2e.ui.utility.common.BaseTest;
import com.jda.dct.e2e.ui.utility.common.LeftMenu;
import com.jda.dct.e2e.ui.utility.helper.AssertHelper.AssertHelper;
import com.jda.dct.e2e.ui.utility.helper.Browser.BrowserHelper;
import com.jda.dct.e2e.ui.utility.pageobjects.*;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.Objects;

public class DashboardTests extends BaseTest {
    private DashboardPage dashboard;
    private LeftMenu leftMenu;
    private TopMenuPage topMenu;
    private TopSubMenuPage topSubMenu;
    private WebDriver driver;
    private BrowserHelper browserHelper;
    private AssertHelper assertHelper;
    private InventoryListPage inventoryListPage;

    @BeforeClass
    public void setUp() throws InterruptedException {
        //Initialize required objects
        driver = getDriver();
        dashboard = new DashboardPage(driver);
        leftMenu = new LeftMenu(driver);
        topMenu = new TopMenuPage(driver);
        topSubMenu = new TopSubMenuPage(driver);
        browserHelper = new BrowserHelper(driver);
        assertHelper = new AssertHelper(driver);
        inventoryListPage = new InventoryListPage(driver);
        //loginAndNavigateToGeoMapLandingPage
        logInToApplication();
    }


    @Test(priority = 1,retryAnalyzer = BaseTest.class)
    public void validateToggleBetweenDashboardAndGridViewTest() {
        dashboard.goToMenuViewIcon();
        dashboard.goToSelectTabFromDisplayedMenu();
/*        leftMenu.clickTopLeftMenu();*/
        browserHelper.refresh();
        dashboard.clickGridViewIcon();
        dashboard.goToMapView();
        assertHelper.assertTrue(dashboard.iSDashBoardLabelDisplayed(), "Dashboard Label is displayed successfully");
    }


    @Test(priority = 2,retryAnalyzer = BaseTest.class)
    public void TypeInKeyWordsToValidateSearchBoxFieldFunctionalityTest() {

        dashboard.goToMenuViewIcon();
        dashboard.goToSelectTabFromDisplayedMenu();
        dashboard.goToSearchBoxField();
        driver.navigate().refresh();
        assertHelper.assertTrue(dashboard.iSDashBoardLabelDisplayed(), "Dashboard Label is displayed successfully");
    }

    @Test(priority = 3,retryAnalyzer = BaseTest.class)
    public void validateFooterIsDisplayed() throws InterruptedException {
        dashboard.goToMenuViewIcon();
        dashboard.goToSelectTabFromDisplayedMenu();
        assertHelper.assertTrue(dashboard.isFooterDisplayed(), "Footer is displayed successfully");
    }


/*    @Test(priority = 4,retryAnalyzer = BaseTest.class)
    public void validateInventoryExceptionIsDisplayed() throws InterruptedException {
        dashboard.goToMenuViewIcon();
        dashboard.goToSelectTabFromDisplayedMenu();
        dashboard.goToMapView();
        browserHelper.refresh();
        dashboard.goToFootSlidingArrow();
        assertHelper.assertTrue(dashboard.isInventoryExceptionDisplayed(), "inventory Exception is displayed successfully");
    }*/

    @Test(priority = 5,retryAnalyzer = BaseTest.class)
    public void validateSearchBarIsDisplayed() throws InterruptedException {
        dashboard.goToMenuViewIcon();
        dashboard.goToSelectTabFromDisplayedMenu();
        browserHelper.refresh();
        dashboard.goToMapView();
        assertHelper.assertTrue(dashboard.isSearchBarDisplayed(), "Search bar is displayed successfully");
    }

/*    @Test(priority = 6)
    public void validateIsPTAUnknownDisplayed() {
        dashboardpage.goToMenuViewIcon();
        dashboardpage.goToSelectTabFromDisplayedMenu();
        driver.navigate().refresh();
        assertHelper.assertTrue(dashboardpage.isPTAUnknownDisplayed(), "PTA Unknown filter is displayed");
    }*/

    @Test(priority = 7,retryAnalyzer = BaseTest.class)
    public void validateOnTimeShipmentIsDisplayed() {
        dashboard.goToMenuViewIcon();
        dashboard.goToSelectTabFromDisplayedMenu();
        assertHelper.assertTrue(dashboard.isOnTimeShipmentDisplayed(), "On Time Shipment filter is displayed");
    }

    @Test(priority = 8,retryAnalyzer = BaseTest.class)
    public void validateIsEarlyShipmentDisplayed() {
        dashboard.goToMenuViewIcon();
        dashboard.goToSelectTabFromDisplayedMenu();
        assertHelper.assertTrue(dashboard.isEarlyShipmentDisplayed(), "Early Shipment filter is displayed");
    }

    @Test(priority = 9,retryAnalyzer = BaseTest.class)
    public void validateStockedOutShipmentDisplayed() throws InterruptedException {

        dashboard.goToMenuViewIcon();
        dashboard.goToSelectTabFromDisplayedMenu();
        browserHelper.refresh();
        dashboard.goToMapView();
        browserHelper.refresh();
        dashboard.goToFootSlidingArrow();
        assertHelper.assertTrue(dashboard.isStockedOutShipmentDisplayed(), "Stock out filter is displayed");
    }

    @Test(priority = 10,retryAnalyzer = BaseTest.class)
    public void validateOverStockShipmentIsDisplayed() throws InterruptedException {

        dashboard.goToMenuViewIcon();
        dashboard.goToSelectTabFromDisplayedMenu();
        browserHelper.refresh();
        dashboard.goToFootSlidingArrow();
        assertHelper.assertTrue(dashboard.isOverStockShipmentDisplayed(), "Over Stock filter is displayed");

    }

/* DCT-9732
    @Test(priority = 11)
    public void validateLowStockShipmentIsDisplayed() throws InterruptedException {
        dashboard.goToMenuViewIcon();
        dashboard.goToSelectTabFromDisplayedMenu();
        browserHelper.refresh();
        dashboard.goToFootSlidingArrow();
        assertHelper.assertTrue(dashboard.isLowStockShipmentDisplayed(), "Low stock filter is displayed");
    }

@Test(priority = 12)
public void validateInventoryexceptions() throws InterruptedException {

dashboard.clickGridViewIcon();
Assert.assertTrue(dashboard.isInventoryWidgetDisplayed());
Assert.assertTrue(dashboard.isLowStockDisplayed());
Assert.assertTrue(dashboard.isOverStockDisplayed());
Assert.assertTrue(dashboard.isLowStockDisplayed());
Assert.assertTrue(dashboard.isExceptionCountDisplayed());
Assert.assertTrue(dashboard.isLowStockValueDisplayed());
Assert.assertTrue(dashboard.isOverStockValueDisplayed());
Assert.assertTrue(dashboard.isLowStockValueDisplayed());
System.out.println(" Welcome to Inventory List Page");
}


@Test(priority = 13 )
public void validateIsInventorylowStockCountMatch(){
dashboard.clickGridViewIcon();
dashboard.isInventoryWidgetDisplayed();
dashboard.assertLowStock();
}


@Test(priority = 14 )
public void validateIsInventoryOverStockCountMatch(){
dashboard.clickGridViewIcon();
dashboard.isInventoryWidgetDisplayed();
dashboard.assertOverStock();
}


@Test(priority = 15 )
public void validateIsInventoryStockOutCountMatch(){
dashboard.clickGridViewIcon();
dashboard.isInventoryWidgetDisplayed();
dashboard.assertStockOut();
}

@Test(priority = 16)
public void validateIsInventoryExceptionSum(){
dashboard.clickGridViewIcon();
dashboard.isInventoryWidgetDisplayed();
dashboard.validateSum();
}
*/

}














