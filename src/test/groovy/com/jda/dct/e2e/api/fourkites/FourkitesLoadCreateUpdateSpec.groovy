/*
package com.jda.dct.e2e.api.fourkites

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.jda.dct.domain.stateful.Shipment
import com.jda.dct.e2e.api.categories.Smoke
import com.jda.dct.e2e.api.utility.FourkitesLoad
import com.jda.dct.e2e.api.utility.IngestionHelper
import com.jda.dct.tracking.loads.rest.fourkites.SignatureCalculator
import groovy.json.JsonSlurper
import org.junit.experimental.categories.Category
import org.springframework.http.HttpMethod
import spock.lang.Ignore
import spock.lang.Shared
import spock.lang.Specification
import spock.util.concurrent.PollingConditions

import java.text.SimpleDateFormat

*/
/**
 * TODO: Summary.
 *
 * @author Vida Bandara
 * @version 1.0-SNAPSHOT
 * @since 18-Jun-2018
 *//*


// determine what is being tested here
@Category(Smoke)
@Ignore
class FourkitesLoadCreateUpdateSpec extends Specification implements IngestionHelper {

    @Shared shipmentURL
    @Shared siteURL
    @Shared shipmentGetURLPrefix
    @Shared shipment
    @Shared shipmentUpdated
    @Shared site1
    @Shared site2
    @Shared site3


    def setupSpec() {
        init()

        def shipmentPath = properties.getProperty('ingestShipmentsPath')
        def shipmentGetPath = properties.getProperty('getShipmentsPath')
        shipmentURL = protocol + ip + shipmentPath

        def sitePath = properties.getProperty('ingestSitesPath')
        siteURL = protocol + ip + sitePath
        shipmentGetURLPrefix = protocol + ip + shipmentGetPath
        shipment = this.getClass().getClassLoader().getResource('fourkites/shipment-1.json').text
//        shipmentUpdated = this.getClass().getClassLoader().getResource('fourkites/shipment-1-updated.json').text
        site1 = this.getClass().getClassLoader().getResource('fourkites/site-1.json').text
        site2 = this.getClass().getClassLoader().getResource('fourkites/site-2.json').text
//        site3 = this.getClass().getClassLoader().getResource('fourkites/site-3.json').text
    }

    def "Ingesting a new shipment creates fourkites load(s)"() {
        given: 'existing sites'
        def site1response = executeHttpCall(header, siteURL, site1, HttpMethod.POST)
        waitUntilComplete(site1response.ingestionId)
        def site2response = executeHttpCall(header, siteURL, site2, HttpMethod.POST)
        waitUntilComplete(site2response.ingestionId)
        sleep(1000L)
        def conditions = new PollingConditions(timeout: 60, initialDelay: 10, delay: 10)

        when: 'a shipment json ingested'
        def shipmentResponse = executeHttpCall(header, shipmentURL, shipment, HttpMethod.POST)
        waitUntilComplete(shipmentResponse.ingestionId)

        then: 'fourkites load id(s) is created'
        conditions.eventually {
            def ids = getFourKitesIds()
            assert ids.any {
                it != null
            }
        }
    }

    @Ignore
    def "Update created shipments"(){
        given: 'existing Fourkites id(s)'
        def site1response = executeHttpCall(header, siteURL, site1, HttpMethod.POST)
        waitUntilComplete(site1response.ingestionId)
        def site2response = executeHttpCall(header, siteURL, site2, HttpMethod.POST)
        waitUntilComplete(site2response.ingestionId)
        sleep(1000L)

        def shipmentResponse = executeHttpCall(header, shipmentURL, shipment, HttpMethod.POST)
        waitUntilComplete(shipmentResponse.ingestionId)
        sleep(1000L)

        def ids =  getFourKitesIds()

        when: 'set site2.siteDescription to Ship To Site Updated'
        site2response = executeHttpCall(header, siteURL, site3, HttpMethod.POST)
        waitUntilComplete(site2response.ingestionId)
        sleep(1000L)

        shipmentResponse = executeHttpCall(header, shipmentURL, shipmentUpdated, HttpMethod.POST)
        waitUntilComplete(shipmentResponse.ingestionId)
        sleep(1000L)

        then: 'fourkites load id(s) are the same'
        assert  ids==getFourKitesIds()
        and: 'loads are updated'
        def loads = ids.collect{
            getResponseByForkitesId(it)
        }
        assert loads.get(0).stops.get(1).name=="Ship To Site 002"

    }

    def getResponseByForkitesId(id){
        def ClientID = "54a27be01d0846beb58288689c9ead4a"
        def Secret = 'zu9ibhjtzefdchzqykfrmgv0nkvjbgzmruxsqtdwmud3mkp1rud0q2xlsnveajrqs2xkcep6utltmctwaw9sqnhsnd0='
        def tracking_service_url="https://tracking-api-staging.fourkites.com"
        def dateFormat = new SimpleDateFormat("yyyyMMddHHmmss")
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"))
        def timestamp = dateFormat.format(new Date());
        def path = "/api/v1/tracking/$id?client_id=$ClientID&timestamp=$timestamp"
        def calculator = new SignatureCalculator(Secret)
        def signature = calculator.calculate(path)
        def url = tracking_service_url+path+"&signature=$signature"
        String response = getHttpClient(header,url,null,HttpMethod.GET).body
        ObjectMapper mapper = new ObjectMapper()
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
        mapper.readValue(response, FourkitesLoad.class)
    }

    def getFourKitesIds() {
        def url = shipmentGetURLPrefix + getId(shipment,Shipment.class)
        println "url $url"
        def shipmentText = getHttpClient(header, url, null, HttpMethod.GET).body
        println "response text: $shipmentText "
        def shipmentRetrieved = new JsonSlurper().parseText(shipmentText).get('data').get(0)
        shipmentRetrieved.get('shipmentLines').collect {
            key,value ->
            value.get('fourkitesLoadId')
        }
    }
}*/
