package com.jda.dct.e2e.ui.navigation;

import com.jda.dct.e2e.ui.utility.action.CommonActions;
import com.jda.dct.e2e.ui.utility.common.BaseTest;
import com.jda.dct.e2e.ui.utility.common.LeftMenu;
import com.jda.dct.e2e.ui.utility.pageobjects.InventoryListPage;
import com.jda.dct.e2e.ui.utility.pageobjects.SupplyForecastDetailsPage;
import com.jda.dct.e2e.ui.utility.pageobjects.SupplyForecastListPage;
import com.jda.dct.e2e.ui.utility.pageobjects.TopMenuPage;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class SupplyForecastDetailsTests extends BaseTest {

    private LeftMenu leftMenu;
    private TopMenuPage topMenu;
    private InventoryListPage inventoryListPage;
    private SupplyForecastListPage supplyForecastListPage;
    private SupplyForecastDetailsPage supplyForecastDetailsPage;
    private CommonActions commonActions;
    private WebDriver driver;

    @BeforeClass
    public void setUp() throws InterruptedException {
        System.out.println("Starting tests for :" + this.getClass().getName());
        //Initialize required objects
        driver = getDriver();
        leftMenu = new LeftMenu(driver);
        topMenu = new TopMenuPage(driver);
        inventoryListPage = new InventoryListPage(driver);
        supplyForecastDetailsPage = new SupplyForecastDetailsPage(driver);
        supplyForecastListPage = new SupplyForecastListPage(driver);
        commonActions = new CommonActions(driver);
        //loginAndNavigateToSupplyLandingPage
        logInToApplication();
    }

    @Test(priority = 1,retryAnalyzer = BaseTest.class)
    public void validateSupplyInventoryListPageIsDisplayedWhenForecastItemIsClicked() throws InterruptedException {
        leftMenu.clickSupplyMenuOption();
        topMenu.goToSupplyForecastCommitTab();
        topMenu.clearFilterIfVisible();
        commonActions.disableAllExceptionFilter();
        supplyForecastListPage.clickForecastItemNumber();
//verify that user lands on Supply Forecast Details page
        Assert.assertTrue(supplyForecastListPage.isSupplyForecastDetailsPageDisplayed());
//verify user clicks at details item number
        supplyForecastDetailsPage.clickForecastDetailsItemNumber();
//verify that user lands on SupplyInventoryList page
        Assert.assertTrue(supplyForecastDetailsPage.isInventoryDetailsPageDisplayed());

    }

    @Test(priority = 2,retryAnalyzer = BaseTest.class)
    public void validateWeeklyForecastIsDisplayedWhenForecastItemIsClicked() throws InterruptedException {
        leftMenu.clickSupplyMenuOption();
        topMenu.goToSupplyForecastCommitTab();
        commonActions.disableAllExceptionFilter();
        supplyForecastListPage.clickForecastItemNumber();
//verify that 26 weekly forecast is displayed
      Assert.assertTrue(supplyForecastDetailsPage.isWeeklyForecastDisplayed());

    }

    @Test(priority = 3,retryAnalyzer = BaseTest.class)
    public void validateForecastMeasuresInSupplyForecastDetailsPage() throws InterruptedException {
        leftMenu.clickSupplyMenuOption();
        topMenu.goToSupplyForecastCommitTab();
//        supplyForecastListPage.clickForecastExceptionsHavingData();
        commonActions.disableAllExceptionFilter();
        supplyForecastListPage.clickForecastItemNumber();
        //verify user finds Message at Forecast Details Page
        Assert.assertTrue(supplyForecastDetailsPage.isMeasureForecastDisplayed());
        //verify user finds  Commits at Forecast Details Page
        Assert.assertTrue(supplyForecastDetailsPage.isMeasureCommitDisplayed());
    }


}
