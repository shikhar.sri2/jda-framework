package com.jda.dct.e2e.ui.utility.helper.genericHelper;

import com.jda.dct.e2e.ui.utility.common.BaseTest;
import com.jda.dct.e2e.ui.utility.helper.Wait.WaitHelper;
import org.apache.log4j.Logger;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GenericHelper extends BaseTest {

    private static Logger log = Logger.getLogger(GenericHelper.class);
    WaitHelper objWait;
    private WebDriver driver;

    public GenericHelper(WebDriver driver) {
        this.driver = driver;
        objWait = PageFactory.initElements(driver, WaitHelper.class);
    }

    /**
     * This method is used to insert value in text box
     *
     * @param element
     * @param label
     * @param value
     * @return
     */
    public boolean setElementText(WebElement element, String label, String value) {

        try {
            element.clear( );
            element.sendKeys(value);
            Reporter("In " + label + " textbox parameter inserted is: '" + value + "'", "Pass", log);
            return true;
        } catch (Exception e) {
            e.printStackTrace( );
            Reporter("Exception while inserting text in " + label + " textbox", "Fail", log);
            throw new RuntimeException( );
        }
    }

    /**
     * This method is used to insert value in text box
     *
     * @param element
     * @param label
     * @param num
     * @return
     */
    public boolean setElementText(WebElement element, String label, Integer num) {

        try {
            String value = num.toString( );
            element.clear( );
            element.sendKeys(value);
            Reporter("In " + label + " textbox parameter inserted is: '" + value + "'", "Pass", log);
            return true;
        } catch (Exception e) {
            e.printStackTrace( );
            Reporter("Exception while inserting text in " + label + " textbox", "Fail", log);
            throw new RuntimeException(e.getLocalizedMessage( ));
        }
    }

    public boolean setElementTextinSelection(WebElement element, String label, String value, boolean clear) {
        try {
            if (clear)
                element.clear( );
            element.sendKeys(value);
            Reporter("In " + label + " textbox parameter inserted is: '" + value + "'", "Pass", log);
            return true;
        } catch (Exception e) {
            e.printStackTrace( );
            Reporter("Exception while inserting text in " + label + " textbox", "Fail", log);
            throw new RuntimeException(e.getLocalizedMessage( ));
        }
    }

    public boolean  elementClick(WebElement element, String label) {
        try {
            element.click( );
            Reporter(label + " is clicked successfully", "Pass", log);
            return true;
        } catch (Exception e) {
            Reporter("Exception while clicking " + label +" : "+ e.getMessage(), "Fail", log);
            throw new RuntimeException("Exception while clicking " + label + ": " + e.getMessage( ));
        }
    }

    /**
     * This method enables or disables text based on parameter passed
     *
     * @param element
     * @param label
     * @param toggleStatus
     * @return
     */
    public boolean toggleElementStatus(WebElement element, String toggleStatus, String label) {
        try {
            if (toggleStatus.equalsIgnoreCase("Enable")) {
                if (element.isEnabled( ) == false) {
                    element.click( );
                    Reporter(label + "checkbox is " + toggleStatus + " successfully", "Pass", log);
                    return true;
                } else {
                    return true;
                }
            } else if (toggleStatus.equalsIgnoreCase("Disable")) {
                if (element.isEnabled( ) == true) {
                    element.click( );
                    Reporter(label + "checkbox is " + toggleStatus + " successfully", "Pass", log);
                    return true;
                }
            } else {
                Reporter(label + "checkbox is " + toggleStatus + " successfully", "Pass", log);
                return true;
            }
            Reporter(label + " checkbox is not displayed", "Fail", log);
            return false;
        } catch (Exception e) {
            e.printStackTrace( );
            Reporter("Exception while " + toggleStatus + "ing " + label + " checkbox", "Fail", log);
            throw new RuntimeException(e.getMessage( ));
        }
    }

    public String getRandomNumber(int num) throws Exception {
        Random randomGenerator = new Random( );
        int randomInt = randomGenerator.nextInt(num);
        if (randomInt == 0) {
            randomInt = randomGenerator.nextInt(num);
        }
        return String.valueOf(randomInt);
    }

    /**
     * This method checks visibility of element on page
     *
     * @param element
     * @param text    info text for WebElement
     * @author shikhar
     */
    public boolean checkVisbilityOfElement(WebElement element, String text) {
        try {
           // objWait.waitElementToBeVisible(element);
            if (element.isDisplayed( ) == true) {
                Reporter(text + " element is visible", "Pass", log);
                return true;
            } else {
                Reporter(text + " is not visible on page", "Info", log);
                return false;
            }
        } catch (NoSuchElementException e) {
            Reporter(text + " is not visible on page", "Info", log);
            return false;
        }
    }

    /**
     * This method checks visibility of element on page
     *
     * @param element
     * @param text    info text for WebElement
     * @author shikhar
     */
    public boolean checkInvisbilityOfElement(WebElement element, String text) {
        try {
            if (element.isDisplayed( ) == false) {
                Reporter("Correctly" + text + " is not visible on page", "Pass", log);
                return true;
            } else {
                Reporter(text + " element is visible", "Fail", log);
                throw new RuntimeException(text + " is visible");
            }
        } catch (NoSuchElementException e) {
            Reporter("Correctly" + text + " is not visible on page", "Pass", log);
            return true;
        }

    }


    public boolean selectDropdown(WebElement element, String dropdownText, String msg) {

        try {
            Select drpElement = new Select(element);
            drpElement.selectByVisibleText(dropdownText);
            Reporter("From '" + msg + "' drop down '" + dropdownText + "' is selected", "Pass", log);
            return true;
        } catch (Exception e) {
            e.printStackTrace( );
            Reporter("Exception while selecting text from " + msg + " dropdown", "Fail", log);
            throw new RuntimeException(e.getMessage( ));
        }
    }

    public boolean toggleElement(WebElement element, boolean expected, String label) {

        try {
            boolean actual = element.isSelected( );
            if (actual != expected) {
                element.click( );
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public synchronized void attachFile(String fileName) {

        log.info(" Trying to attach file : " + fileName);

        sleep(1);
        if (fileName.isEmpty( )) {
            log.error(" File name provided is empty ");
            throw new RuntimeException("File name is provided empty for attachments ");
        }

        File file = new File("src/test/resources/testdata/" + fileName);
        String filePath = file.getAbsolutePath( );
        log.info(" Retrieving Absolute file path : " + filePath);

        if (!file.exists( )) {
            log.error("File doesn't exists");
            throw new RuntimeException("File doesn't exists");
        }

        log.info("Set Absolute file path to Clipboard");
        Toolkit.getDefaultToolkit( ).getSystemClipboard( ).setContents(new StringSelection(filePath), null);

        Robot robot = null;

        try {
            log.info("Create Robot Object ");
            robot = new Robot( );
        } catch (Exception e) {
            log.info(" Exception while creating Robot object ");
            log.error(e.getMessage( ));
        } finally {
            if (robot == null)
                throw new RuntimeException("Unable to initialize AWT Robot ");
        }

        boolean linux = System.getProperty("os.name").equalsIgnoreCase("linux");

        if (linux) {

            log.info(" Pressing Ctrl + L to get to file path provider ");
            robot.keyPress(KeyEvent.VK_CONTROL);
            robot.keyPress(KeyEvent.VK_L);
            robot.keyRelease(KeyEvent.VK_L);
            robot.keyRelease(KeyEvent.VK_CONTROL);
        }

        log.info(" Pressing Ctrl + V to paste clipboard content to file path provider ");
        robot.keyPress(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_CONTROL);

        log.info(" Pressing Enter to submit attachment ");
        robot.keyPress(KeyEvent.VK_ENTER);
        robot.keyRelease(KeyEvent.VK_ENTER);
        sleep(1);

    }

    public synchronized void attachFileIfFilePathProvided(String filePath) {

        log.info(" Trying to attach file : " + filePath);

        sleep(1);
        if (filePath.isEmpty( )) {
            log.error(" File name provided is empty ");
            throw new RuntimeException("File name is provided empty for attachments ");
        }

        File file = new File(filePath);
        filePath = file.getAbsolutePath( );
        log.info(" Retrieving Absolute file path : " + filePath);

        if (!file.exists( )) {
            log.error("File doesn't exists");
            throw new RuntimeException("File doesn't exists");
        }

        log.info("Set Absolute file path to Clipboard");
        Toolkit.getDefaultToolkit( ).getSystemClipboard( ).setContents(new StringSelection(filePath), null);

        Robot robot = null;

        try {
            log.info("Create Robot Object ");
            robot = new Robot( );
        } catch (Exception e) {
            log.info(" Exception while creating Robot object ");
            log.error(e.getMessage( ));
        } finally {
            if (robot == null)
                throw new RuntimeException("Unable to initialize AWT Robot ");
        }

        boolean linux = System.getProperty("os.name").equalsIgnoreCase("linux");

        if (linux) {

            log.info(" Pressing Ctrl + L to get to file path provider ");
            robot.keyPress(KeyEvent.VK_CONTROL);
            robot.keyPress(KeyEvent.VK_L);
            robot.keyRelease(KeyEvent.VK_L);
            robot.keyRelease(KeyEvent.VK_CONTROL);
        }

        log.info(" Pressing Ctrl + V to paste clipboard content to file path provider ");
        robot.keyPress(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_CONTROL);

        log.info(" Pressing Enter to submit attachment ");
        robot.keyPress(KeyEvent.VK_ENTER);
        robot.keyRelease(KeyEvent.VK_ENTER);
        sleep(1);

    }


    public void sleep(int sleepInSecs) {
        try {
            Thread.sleep(sleepInSecs * 1000);
        } catch (Exception e) {
        }
    }

    public void navigateTo(String additionURL) {
        try {
            driver.navigate( ).to(additionURL);
            Reporter("Navigate to '" + additionURL, "Pass", log);
        } catch (Exception e) {
            e.printStackTrace( );
            Reporter("Exception while navigating to '" + additionURL + "'", "Fail", log);
        }
    }

    /*    public void clearCookiesAndLoad() {
            try {
                driver.manage( ).deleteAllCookies( );
                gotoHomePage( );
                Reporter(" Done clearing cookies and reload ", "Pass", log);
            } catch (Exception e) {
                e.printStackTrace( );
                Reporter(" Exception while clearing cookies", "Pass", log);
            }
        }*/

    public static synchronized String getElementText(WebElement element) {
        if (null == element) {
            log.info("weblement is null");
            return null;
        }
        String elementText = null;
        try {
            elementText = element.getText( );
        } catch (Exception ex) {
            log.info("Element not found " + ex);
        }
        return elementText;
    }

    public String getTextFromElement(WebElement element, String label) {

        if (null == element) {
            log.info("weblement is null");
            return null;
        }

        boolean displayed = false;
        try {
            displayed = isDisplayed(element, label);
        } catch (Exception e) {
            log.error(e);
            return null;
        }

        if (!displayed)
            return null;
        String text = element.getText( );
        Reporter(label + " value is.. " + text, "Info", log);
        return text;
    }

    public String getValuefromAttribute(WebElement element, String label) {
        if (null == element)
            return null;
        if (!isDisplayed(element, label))
            return null;
        String value = element.getAttribute("value");
        log.info("weblement valus is.." + value);
        return value;
    }

    public String getValueFromAttribute(WebElement element, String tag) {
        String value = element.getAttribute(tag);
        log.info("weblement valus is.." + value);
        return value;
    }

    public boolean isDisplayed(WebElement element, String label) {
        try {
            element.isDisplayed( );
            Reporter("'"+ label + "' is Displayed on page ", "Pass", log);
            return true;
        } catch (NoSuchElementException n) {
            Reporter("'"+ label + "' is not Displayed on page ", "Info", log);
            return false;
        } catch (Exception e) {
            log.info(e);
            Reporter("Exception while loading element " + label + ": " +e.getCause(), "Fail", log);
            throw new RuntimeException(e.getLocalizedMessage( ));
        }
    }

    public List<String> listOfStrings(List<WebElement> elements,String label) {
        List<String> listOfWebElements = new ArrayList<String>();
        try {
        Reporter("'" + label + "' is Displayed on page ", "Info", log);
        for (int i = 0; i < elements.size(); i++) {
            listOfWebElements.add(elements.get(i).getText());
            System.out.println(elements.get(i).getText());
        }
      return  listOfWebElements;
        }
        catch (Exception ex)
        {
            Reporter("'" + label + "' is not Displayed on page ", "Info", log);
        }
        return  null;
    }

    public List<WebElement> listOfWebElements(List<WebElement> elements,String label) {
        List<WebElement> listOfWebElements = new ArrayList<WebElement>();
        try {
            Reporter("'" + label + "' is Displayed on page ", "Info", log);
            for (int i = 0; i < elements.size(); i++) {
                listOfWebElements.add(elements.get(i));
                System.out.println(elements.get(i).getText());
            }
            return  listOfWebElements;
        }
        catch (Exception ex)
        {
            Reporter("'" + label + "' is not Displayed on page ", "Info", log);
        }
        return  null;
    }
}
