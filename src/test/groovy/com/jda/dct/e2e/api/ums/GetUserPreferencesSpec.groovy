package com.jda.dct.e2e.api.ums

import com.jda.dct.e2e.api.utility.AuthHelper
import groovyx.net.http.ContentType
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import spock.lang.Shared
import spock.lang.Specification
import groovyx.net.http.RESTClient
import spock.lang.Unroll

// may be redundant, check and delete on confirmation
class GetUserPreferencesSpec extends Specification implements AuthHelper {
    static userPreferenceURL
    @Shared
    def client

    def setupSpec() {
        init()

        def userPreferencePath = properties.getProperty('getUserPreferencesPath')
        userPreferenceURL = protocol + ip + userPreferencePath
        client = new RESTClient(userPreferenceURL)
        client.handler.failure = { resp -> resp }
        header[HttpHeaders.CONTENT_TYPE] = MediaType.APPLICATION_JSON
    }

    @Unroll
    def "Get user preferences API #url"() {
        given: "The API URL is #url "
        when: "HTTP Method is GET"
        def response = client.get(headers: header)
        then: "the response should be 200"
        with(response) {
            status == 200
        }
        where:
        url << [userPreferenceURL]
    }

    @Unroll
    def "Create user preference with API #url"() {
        def id
        given: "User preference is created with userPrefField001"
        when:
        def response = client.post(
                requestContentType : ContentType.JSON,
                headers : header,
                body    : '{ "preferences" :  { "userPrefField001" : "abcd" } }')
        and: "userPrefField001 value from the newly created user preference"

        then: "Response is 200 OK"
        with(response) {
            status == 200
        }
        and: "User preference userPrefField001 is abcd"
        with(response.data.data.preferences) {
            userPrefField001 == [fieldValue]
        }
        where:
        url << [userPreferenceURL]
        fieldValue = "abcd"
    }
}
