package com.jda.dct.e2e.api.shipments

import com.jda.dct.e2e.api.categories.Smoke
import com.jda.dct.e2e.api.utility.IngestionHelper
import com.jda.dct.e2e.api.utility.QueryObject
import com.jda.dct.e2e.api.utility.QueryResultHandler
import org.junit.experimental.categories.Category
import spock.lang.Specification

class ShipmentSortSpec extends Specification implements IngestionHelper {

    def setupSpec() {
        init()
    }

    @Category(Smoke)
    def "Sorting shipments"() {

        given: "all inboundShipments"

        when: "shipments searched by a valid text and sorted by a field"
        def queryObject = new QueryObject()
        queryObject.setType("shipments")
        queryObject.addQuery("shipmentType", "inboundShipment")
        queryObject.addSortFields("shipperRefNo", "asc", "string")

        and: "page size defined as 10"
        queryObject.setLimit("10")

        def response = QueryResultHandler.getQueryResultHandler().executeWithRetry(queryObject, 50, 2, 2, 10, 9)

        then: "return shipments should be in asc order in the page"
        (0..response.body.data.size() - 2).each({ i ->
            assert response.body.data[i].shipperRefNo <= response.body.data[i + 1].shipperRefNo
        })
    }

    def "Sorting shipments per page"() {

        given: "all inboundShipments"

        when: "shipments searched by a valid text and sorted by a field"
        def queryObject = new QueryObject()
        queryObject.setType("shipments")
        queryObject.addQuery("shipmentType", "inboundShipment")
        queryObject.addSortFields("shipperRefNo", "asc", "string")

        and: "page size defined as 10 and offset is 20"
        queryObject.setOffset("20")
        queryObject.setLimit("10")
        def response = QueryResultHandler.getQueryResultHandler().executeWithRetry(queryObject, 50, 2, 2, 10, 10)

        then: "return shipments should be in asc order in the page"
        (0..response.body.data.size() - 2).each({ i ->
            assert response.body.data[i].shipperRefNo <= response.body.data[i + 1].shipperRefNo
        })
    }

    def "Sorting shipments in reverse order"() {

        given: "all inboundShipments"
//      Fails in itest, passes in stable
        when: "shipments searched by a valid text and sorted by a field"
        def queryObject = new QueryObject()
        queryObject.setType("shipments")
        queryObject.addQuery("shipmentType", "inboundShipment")
        queryObject.addSortFields("shipperRefNo", "desc", "string")

        and: "page size defined as 10 and offset is 30"
        queryObject.setOffset("20")
        queryObject.setLimit("10")
        def response = QueryResultHandler.getQueryResultHandler().executeWithRetry(queryObject, 50, 2, 2, 10, 10)

        then: "return shipments should be in desc order in the page"
        (0..response.body.data.size() - 2).each({ i ->

            assert response.body.data[i].shipperRefNo >= response.body.data[i + 1].shipperRefNo
        })
    }
}
