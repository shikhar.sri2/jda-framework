package com.jda.dct.e2e.ui.utility.pageobjects;

import com.jda.dct.e2e.ui.utility.action.CommonActions;
import com.jda.dct.e2e.ui.utility.common.BasePage;
import com.jda.dct.e2e.ui.utility.helper.Wait.WaitHelper;
import com.jda.dct.e2e.ui.utility.helper.genericHelper.GenericHelper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class DemandDeliveryListPage extends BasePage {
    WebDriver driver;
    WebDriverWait webDriverWait;
    WaitHelper waitHelper;
    GenericHelper genericHelper;
    CommonActions commonActions;
    //Deliveries tab related data elements
//    @FindBy(xpath = "//text()[.='DELIVERIES']/ancestor::span[1]")
//    private WebElement demandDeliveryPageTab;

    //Webelements for navigation
    @FindBy(id ="demand-page-tab-deliveries")
    //@FindBy(xpath = "//div[contains(@id,'DeliveryList-ExpandablePanel-Cell')]//a")
    private WebElement demandDeliveryListPage;

    @FindBy(xpath = "//span[contains(text(),'Delivery Details')]")
    private WebElement demandDeliveryDetailsPage;
    @FindBy(xpath = "//a[@href='/demand/delivery']")
    private WebElement demandDeliveryLink;
    @FindBy(xpath = "//*[@id='toolBar']//button[@value = 'Ocean']")
    private WebElement oceanIcon;
    @FindBy(xpath = "//*[contains(@id,'demand-outboundDelivery-Filter-List-Card')]//span")
    private List<WebElement> deliveryExceptionStatusCount;

    public DemandDeliveryListPage(WebDriver webdriver) {

        this.driver = webdriver;
        PageFactory.initElements(driver, this);
        webDriverWait = new WebDriverWait(driver, getTimeOutInSeconds());
        waitHelper = new WaitHelper(driver);
        genericHelper = new GenericHelper(driver);
        commonActions = new CommonActions(driver);
    }


    //================ Methods========================


    //navigate to Demand Inventory shipment page

//    public void goToDemandDeliveryTab() {
//        webDriverWait.until(ExpectedConditions.elementToBeClickable(demandDeliveryPageTab));
//        demandDeliveryPageTab.click();
//    }


    //Assertion methods

    public boolean isDemandDeliveryListPageDisplayed() {
        webDriverWait.until(ExpectedConditions.visibilityOf(demandDeliveryListPage));
        return demandDeliveryListPage.isDisplayed();
    }

    public boolean isDemandDeliveryDetailsPageDisplayed() {
        webDriverWait.until(ExpectedConditions.visibilityOf(demandDeliveryDetailsPage));
        return demandDeliveryDetailsPage.isDisplayed();

    }

    public boolean clickOceanIcon() {
        waitHelper.waitElementToBeClickable(oceanIcon);
        return genericHelper.elementClick(oceanIcon, "Ocean Icon");
    }

    public void clickDemandDeliveryExceptionsHavingData() {
        try {
            Thread.sleep(1000);
            commonActions.clickExceptionStatusHavingData(deliveryExceptionStatusCount);
        }catch (Exception e){

        }
    }
}