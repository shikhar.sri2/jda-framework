
package com.jda.dct.e2e.ui.navigation;

import com.jda.dct.e2e.ui.utility.action.CommonActions;
import com.jda.dct.e2e.ui.utility.common.BaseTest;
import com.jda.dct.e2e.ui.utility.common.LeftMenu;
import com.jda.dct.e2e.ui.utility.helper.AssertHelper.AssertHelper;
import com.jda.dct.e2e.ui.utility.helper.Browser.BrowserHelper;
import com.jda.dct.e2e.ui.utility.pageobjects.*;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class DemandDeliveryDetailsTests extends BaseTest {


    private LeftMenu leftMenu;
    private TopMenuPage topMenu;
    private TopSubMenuPage topSubMenu;
    private WebDriver driver;
    private DemandDeliveryDetailsPage demanddeliverydetailspage;
    private DemandDeliveryListPage demandDeliveryListPage;
    private DemandShipmentListPage demandShipmentListPage;
    private AssertHelper assertHelper;
    private BrowserHelper browserHelper;
    private CommonActions commonActions;

    @BeforeClass
    public void setUp() throws InterruptedException {
//Initialize required objects
        driver = getDriver();
        leftMenu = new LeftMenu(driver);
        topMenu = new TopMenuPage(driver);
        topSubMenu = new TopSubMenuPage(driver);
        demanddeliverydetailspage = new DemandDeliveryDetailsPage(driver);
        demandDeliveryListPage = new DemandDeliveryListPage(driver);
        demandShipmentListPage = new DemandShipmentListPage(driver);
        browserHelper = new BrowserHelper(driver);
        assertHelper = new AssertHelper(driver);
        commonActions = new CommonActions(driver);
        logInToApplication();
    }


    @Test(priority = 1,retryAnalyzer = BaseTest.class)
    public void validateDeliveryDetailsPageIsDisplayed()  {
        leftMenu.clickDemandMenuOption();
        topMenu.gotoDemandDeliveryTab();
        topMenu.clearFilterIfVisible();
        demandDeliveryListPage.clickOceanIcon();
        commonActions.disableAllExceptionFilter();
        demanddeliverydetailspage.clickOnDeliveryNumber();
        assertHelper.assertTrue(demanddeliverydetailspage.isDeliveryDetailsPageDisplayed(), "Delivery details page is displayed successfully");
        demanddeliverydetailspage.gotoDeliveryPage();
        browserHelper.refresh();
    }

    @Test(priority = 2,retryAnalyzer = BaseTest.class)
    public void validateShipmentPageWhenContainerNumberIsClickedFRomExpandedList() throws InterruptedException {
        leftMenu.clickDemandMenuOption();
        topMenu.gotoDemandDeliveryTab();
        String deliveryNo = "OD0039_SE";
        topMenu.gotoDemandDeliveryTab();
        topSubMenu.goToSearchFeature(deliveryNo);
        demandDeliveryListPage.clickOceanIcon();
        commonActions.disableAllExceptionFilter();
        demanddeliverydetailspage.clickOnContainerNumber();
        demandShipmentListPage.clickOceanIcon();
        commonActions.disableAllExceptionFilter();
        assertHelper.assertTrue(demanddeliverydetailspage. isShipmentNumberDisplayed(), "Shipment No. is displayed successfully");
        demanddeliverydetailspage.gotoDeliveryPage();
        topMenu.clearFilter();
        browserHelper.refresh();
    }

    @Test(priority = 3,retryAnalyzer = BaseTest.class)
    public void validateShipmentPageWhenContainerNumberIsClicked() throws InterruptedException {
        leftMenu.clickDemandMenuOption();
        topMenu.gotoDemandDeliveryTab();
        String deliveryNo = "OD0039_SE";
        topMenu.clearFilterIfVisible();
        topSubMenu.goToSearchFeature(deliveryNo);
        demandDeliveryListPage.clickOceanIcon();
        commonActions.disableAllExceptionFilter();
        demanddeliverydetailspage.expandDropDown();
        demanddeliverydetailspage.clickOnContainerNumber();
        demandShipmentListPage.clickOceanIcon();
        commonActions.disableAllExceptionFilter();
        assertHelper.assertTrue(demanddeliverydetailspage. isShipmentNumberDisplayed(), "Shipment No. is displayed successfully");
        demanddeliverydetailspage.gotoDeliveryPage();
        topMenu.clearFilter();
        browserHelper.refresh();
    }

    @Test(priority = 4,retryAnalyzer = BaseTest.class)
    public void validateInventoryDetailsPageWhenItemNumberIsClicked()  {
        leftMenu.clickDemandMenuOption();
        topMenu.gotoDemandDeliveryTab();
        topMenu.clearFilterIfVisible();
        demandDeliveryListPage.clickOceanIcon();
        commonActions.disableAllExceptionFilter();
        demanddeliverydetailspage.expandDropDown();
        demanddeliverydetailspage.clickOnItemNumber();
        assertHelper.assertTrue(demanddeliverydetailspage.isInventoryDetailsPageDisplayed(), "Inventory details page is displayed successfully");
        browserHelper.refresh();
    }

    @Test(priority = 5,retryAnalyzer = BaseTest.class)
    public void validateSalesOrderDetailsPageWhenSalesOrderNumberIsClicked()  {
        leftMenu.clickDemandMenuOption();
        topMenu.gotoDemandDeliveryTab();
        demandDeliveryListPage.clickOceanIcon();
        commonActions.disableAllExceptionFilter();
        demanddeliverydetailspage.expandDropDown();
        demanddeliverydetailspage.clickOnSaleOrderNumber();
        assertHelper.assertTrue(demanddeliverydetailspage.isSalesOrderDetailsPageDisplayed(), "Sales Order details page is displayed successfully");
        browserHelper.refresh();
    }
}
