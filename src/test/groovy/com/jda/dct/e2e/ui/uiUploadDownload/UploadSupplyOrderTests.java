package com.jda.dct.e2e.ui.uiUploadDownload;

import com.jda.dct.e2e.ui.utility.ExcelHelper.ExcelReader;
import com.jda.dct.e2e.ui.utility.action.CommonActions;
import com.jda.dct.e2e.ui.utility.common.BaseTest;
import com.jda.dct.e2e.ui.utility.common.LeftMenu;
import com.jda.dct.e2e.ui.utility.helper.AssertHelper.AssertHelper;
import com.jda.dct.e2e.ui.utility.helper.Browser.BrowserHelper;
import com.jda.dct.e2e.ui.utility.helper.Resource.ResourceHelper;
import com.jda.dct.e2e.ui.utility.helper.Utility.UtilityHelper;
import com.jda.dct.e2e.ui.utility.helper.genericHelper.GenericHelper;
import com.jda.dct.e2e.ui.utility.pageobjects.*;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;

public class UploadSupplyOrderTests extends BaseTest {

    private LeftMenu leftMenu;
    private TopMenuPage topMenu;
    private TopSubMenuPage topSubMenu;
    private SupplyShipmentListPage supplyShipmentListPage;
    private SupplyShipmentDetailsPage supplyShipmentDetailsPage;
    private SupplyOrderListPage supplyOrderlistPage;
    private AssertHelper assertHelper;
    private BrowserHelper browserHelper;
    private InventoryListPage inventoryListPage;
    private DocumentsPage documentsPage;
    private CommonActions commonActions;
    private UtilityHelper utilityHelper;
    private ExcelReader excelReader;
    private GenericHelper genericHelper;
    private WebDriver driver;


    @BeforeClass
    public void setUp() throws InterruptedException {
        System.out.println("Starting tests for :" + this.getClass().getName());
        //Initialize required objects
        driver = getDriver();
        leftMenu = new LeftMenu(driver);
        topMenu = new TopMenuPage(driver);
        topSubMenu = new TopSubMenuPage(driver);
        supplyShipmentListPage = new SupplyShipmentListPage(driver);
        supplyShipmentDetailsPage = new SupplyShipmentDetailsPage(driver);
        supplyOrderlistPage = new SupplyOrderListPage(driver);
        inventoryListPage = new InventoryListPage(driver);
        documentsPage = new DocumentsPage(driver);
        assertHelper = new AssertHelper(driver);
        browserHelper = new BrowserHelper(driver);
        commonActions = new CommonActions(driver);
        excelReader = new ExcelReader();
        utilityHelper = new UtilityHelper();
        genericHelper = new GenericHelper(driver);
        //loginAndNavigateToSupplyLandingPage
        logInToApplication();
    }

    @Test(priority = 1)
    public void validateUploadDownloadEndToEndOnSupplyOrderListPage() {
        String fileName = "purchaseOrders.xlsx";
        long countOfRecordsInUI = 0;
        String exactFileName = "";
        leftMenu.clickSupplyMenuOption();
        topMenu.goToSupplyOrdersTab();
        commonActions.disableAllExceptionFilter();
        countOfRecordsInUI = topSubMenu.getNoOfRecordsOnUIWithAppliedFilter();
        assertHelper.assertTrue(supplyOrderlistPage.isSupplyOrderListPageDisplayed(), "Supply Order list page is displayed successfully");
        topSubMenu.clickDownloadButton();
        leftMenu.clickDocumentsMenuOption();
        browserHelper.refresh();
        documentsPage.clickAllFilesFilter();
        documentsPage.clickDownloadFilesFilterOption();
        documentsPage.clickOnFirstDownloadLink();
        exactFileName = utilityHelper.verifyAndReturnFileNameIfFileFoundInFileSystem(fileName);
        assertHelper.assertTrue(commonActions.verifyRecordsInDownloadedExcelIsMoreThanOrEqualToTheRecordsInUI(exactFileName,countOfRecordsInUI),
                "Count of records in downloaded excel is more than or equal to UI");
        leftMenu.clickSupplyMenuOption();
        topMenu.goToSupplyOrdersTab();
        topSubMenu.clickUploadButton();
        topSubMenu.clickUploadChooseFileButton();
        genericHelper.attachFileIfFilePathProvided(ResourceHelper.getResourcePath(UtilityHelper.getProperty("testFramework","default.download.path")) + File.separator + exactFileName);
        topSubMenu.clickUploadFileButtonOnUploadFilePopUp();
        leftMenu.clickDocumentsMenuOption();
        documentsPage.clickAllFilesFilter();
        documentsPage.clickUploadFilesFilterOption();
        assertHelper.assertTrue(documentsPage.checkUploadFileStatus(), "File Upload is successful");
    }


    @Test(priority = 2)
    public void verifyNewPurchaseOrderAdditionUsingUploadOnSupplyOrderListPage() throws InterruptedException{
        String fileName = "/UIUploadDownload/purchaseOrdersTestData.xlsx";
        String poNumber = "POA0017_CO";
        leftMenu.clickSupplyMenuOption();
        topMenu.goToSupplyOrdersTab();
        topSubMenu.clickUploadButton();
        topSubMenu.clickUploadChooseFileButton();
        genericHelper.attachFile(fileName);
        topSubMenu.clickUploadFileButtonOnUploadFilePopUp();
        leftMenu.clickDocumentsMenuOption();
        documentsPage.clickAllFilesFilter();
        documentsPage.clickUploadFilesFilterOption();
        browserHelper.refresh();
        assertHelper.assertTrue(documentsPage.checkUploadFileStatus(), "File Upload is successful");
        leftMenu.clickSupplyMenuOption();
        topMenu.goToSupplyOrdersTab();
        topSubMenu.clickSearchButton();
        topSubMenu.goToSearchFeature(poNumber);
        commonActions.disableAllExceptionFilter();
        supplyOrderlistPage.clickOnOrderItem();
        supplyOrderlistPage.clickOrderNumber();
        assertHelper.assertTrue(supplyOrderlistPage.isSupplyPurchaseOrderDetailsPageDisplayed(), "Supply Purchase Order is displayed");
        supplyOrderlistPage.clickBackToSupplyOrdersTab();
        topMenu.clearFilter();
        browserHelper.refresh();

    }

}

