package com.jda.dct.e2e.api.ums

import com.jda.dct.e2e.api.utility.IngestionHelper
import org.springframework.http.HttpMethod
import spock.lang.Shared
import spock.lang.Specification

class TenantUsersSpec extends Specification implements IngestionHelper{

    @Shared umsUrl
    @Shared tenantUserUrl
    @Shared sampleTenantUser

    def setupSpec() {
        init()
        umsUrl = protocol + ip + umsPath
        tenantUserUrl = umsUrl + 'tenantUsers'
    }
    // smoke
    def "Add a tenant user"() {
        given: "Given a tenant user"
        sampleTenantUser = this.getClass().getClassLoader().getResource('jsons/sample-tenant-user.json').text

        when: "Add the user"
        def response = executeHttpCall(header, tenantUserUrl, sampleTenantUser, HttpMethod.POST)

        then: "Get user back"
        response != null && response.data.size() == 1 && response.data[0].userName == 'acme_supply_planner@dcttestllc.onmicrosoft.com'
    }
    // smoke
    def "Get tenant user"() {
        given: "given added user"
        def user = '/acme_supply_planner@dcttestllc.onmicrosoft.com'

        when: "GET tenant user"
        def response = executeHttpCall(header, tenantUserUrl + user, null, HttpMethod.GET)

        then: "get user back"
        response != null && response.count == 1
    }
    // rest are regression
    def "Verify error when tenant user is added without tenant"() {
        given: "Given a tenant user without tenant"
        def invalidTenantUserJson = this.getClass().getClassLoader().getResource('jsons/sample-tenant-user-false.json').text

        when: "Add the user"
        int status = getHttpStatusVal(header, tenantUserUrl, invalidTenantUserJson, HttpMethod.POST)

        then: "Unable to add"
        status != 200
    }
}
