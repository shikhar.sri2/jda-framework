package com.jda.dct.e2e.api.searchandfiltering

import com.jda.dct.e2e.api.utility.IngestionHelper
import spock.lang.Shared
import spock.lang.Specification

/**
 * test searchable fields.
 *
 * @author Vida Bandara
 * @version 1.0-SNAPSHOT
 * @since 17-Sep-2018
 */

class SearchableFieldsSpec extends Specification implements IngestionHelper {

    @Shared elasticsearchURL

    @Shared sampleShipment

    void setupSpec() {
        init()

        def shipmentPath = properties.getProperty('ingestShipmentsPath')
        def elasticsearchPath = properties.getProperty('elasticsearchPath')

        ingestionURL = protocol + ip + shipmentPath
        elasticsearchURL = protocol + ip + elasticsearchPath

        sampleShipment = this.getClass().getClassLoader().
                getResource('jsons/sample-shipment-searchable-fields.json').text
        //each run data is different, so can run multiple times
        sampleShipment = sampleShipment.replace("shipmentForSearchableFieldsSpec",
                "shipmentForSearchableFieldsSpec" + (new Date()).getTime())
    }

    // evaluate test value and migrate
//    def "only searchable fields appear in elastic-search index"() {
//        given: 'a sample shipment'
//        def shipmentId = getShipmentIds(sampleShipment)[0]
//        def indexName = shipmentId.split('-')[0..1].join('-').toLowerCase()
//        def searchUrl = elasticsearchURL + '/' + indexName + '/_search?q=_id:' + shipmentId
//
//        when: 'after ingested into LCT'
//        ingestData(sampleShipment)
//
//        //keep polling for every 5 seconds for max a minute
//        def conditions = new PollingConditions(timeout: 60, initialDelay: 2)
//        conditions.setDelay(10)
//
//        then: 'noOfContainers is not in the elastic search'
//        conditions.eventually {
//            def response = getHttpClient(header, searchUrl, null, HttpMethod.GET)
//            def data = new JsonSlurper().parseText(response.body)
//
//            with(data.get('hits').get('hits')[0].get('_source')) {
//                get('id') == shipmentId
//                get('shipmentType') == 'inboundShipment'
//                !containsKey('noOfContainers')
//            }
//        }
//    }
}