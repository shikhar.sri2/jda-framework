package com.jda.dct.e2e.ui.inventory;

import com.jda.dct.e2e.ui.utility.common.BaseTest;
import com.jda.dct.e2e.ui.utility.common.LeftMenu;
import com.jda.dct.e2e.ui.utility.pageobjects.SortByPage;
import com.jda.dct.e2e.ui.utility.pageobjects.TopMenuPage;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class InventorySortByTests extends BaseTest {

    private LeftMenu leftMenu;
    private TopMenuPage topMenu;
    private SortByPage sortBy;
    private WebDriver driver;

    @BeforeClass
    public void setUp() throws InterruptedException {
        System.out.println("Starting tests for :" + this.getClass().getName());
//Initialize required objects
        driver = getDriver();
        leftMenu = new LeftMenu(driver);
        topMenu = new TopMenuPage(driver);
        sortBy = new SortByPage(driver);
        //inventoryListPage = new InventoryListPage(driver);

        //loginAndNavigateToInventoryListLandingPage
        logInToApplication();
        leftMenu.clickInventoryMenuOption();
    }

        @Test(priority = 1)
    public void validateIsAscendingInventoryListDisplayed() throws InterruptedException {
        leftMenu.clickInventoryMenuOption();
        topMenu.clickInventoryLowStock();
         sortBy.clickAscendingInventory();
        System.out.println("arranged in ASC sorting order...");
        sortBy.isAscendingInventoryListDisplayed();
    }

     @Test(priority = 2)
     public void validateIsDescendingInventoryListDisplayed() throws InterruptedException {
          leftMenu.clickInventoryMenuOption();
          topMenu.clickInventoryStockOut();
         sortBy.clickDescendingInventory();
         sortBy.isDescendingInventoryListDisplayed();

         System.out.println("arranged in Desc sorting order...");







     }









    }





