package com.jda.dct.e2e.clientUi.pages.SupplyModule
import geb.Page
import org.openqa.selenium.By

class SupplyListPage extends Page {


    static url = "/tempraturecontrolled/shipment"

    static atCheckWaiting = true

    static at = {

         $("#tempraturecontrolled-page-tab-shipments").isDisplayed()

    }


    static content = {


        deliverylisttitle(wait: true) {

            $("div", role:"button", 'aria-expanded':"false").first()
        }
        println "You have succesfully arrived  here from Supply Delivery Container  page"

    }



}


