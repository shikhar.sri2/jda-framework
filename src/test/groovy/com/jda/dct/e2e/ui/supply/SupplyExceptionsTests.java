package com.jda.dct.e2e.ui.supply;/*

package com.jda.dct.e2e.ui.supply;

import BaseTest;
import LeftMenu;
import com.jda.dct.e2e.utility.ui.pageobjects.*;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.*;

public class SupplyExceptionsTests extends BaseTest {



private LeftMenu leftMenu;
private TopMenuPage topMenu;
private TopSubMenuPage topSubMenu;
private WebDriver driver;

@BeforeClass
public void setUp() throws InterruptedException {
System.out.println("Starting tests for :" + this.getClass().getName());
//Initialize required objects
driver = getDriver();

leftMenu = new LeftMenu(driver);
topMenu = new TopMenuPage(driver);
topSubMenu = new TopSubMenuPage(driver);

logInToApplication();
leftMenu.clickSupplyMenuOption();

}

@Test(priority = 54, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
public void validatingSupplyExceptionsStatusLateTC54(String shipmentNo54a,String shipmentNo54b) throws InterruptedException {
topMenu.goToSupplyShipmentsTab();
topSubMenu.clickOceanIcon();
topMenu.clickExceptionsTab();
topMenu.clickSupplyShipmentExceptionsLate();
topSubMenu.clickSearchButton();
topSubMenu.goToSearchFeature(shipmentNo54a);
Assert.assertTrue(topMenu.isSupplyShipmentLateVisible());
topMenu.assertActualAndExpectedSupplyShipmentNumberEqual(shipmentNo54b);
topMenu.clearFilter();
driver.navigate().refresh();
}


@Test(priority = 55, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
public void validatingSupplyExceptionsStatusDelayedTC55(String shipmentNo55a,String shipmentNo55b) throws InterruptedException {
topMenu.goToSupplyShipmentsTab();
topSubMenu.clickOceanIcon();
topMenu.clickExceptionsTab();
topMenu.clickSupplyShipmentExceptionsDelayed();
topSubMenu.clickSearchButton();
topSubMenu.goToSearchFeature(shipmentNo55a);
Assert.assertTrue(topMenu.isSupplyShipmentDelayedVisible());
topMenu.assertActualAndExpectedSupplyShipmentNumberEqual(shipmentNo55b);

topMenu.clearFilter();
driver.navigate().refresh();

}

@Test(priority = 58, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
public void validatingSupplyExceptionsStatusDelayedTC58(String shipmentNo58a,String shipmentNo58b) throws InterruptedException {

topMenu.goToSupplyShipmentsTab();
topMenu.clickExceptionsTab();
topMenu.clickSupplyShipmentExceptionsDelayed();
topSubMenu.clickSearchButton();
topSubMenu.goToSearchFeature(shipmentNo58a);
Assert.assertTrue(topMenu.isSupplyShipmentDelayedVisible());
topMenu.assertActualAndExpectedSupplyShipmentNumberEqual(shipmentNo58b);
topMenu.clearFilter();  // webDriverWait is causing the test to fail
driver.navigate().refresh();
}
@Test(priority = 59, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
public void validatingSupplyExceptionsStatusLateTC59(String shipmentNo59a,String shipmentNo59b) throws InterruptedException {

topMenu.goToSupplyShipmentsTab();
topMenu.clickExceptionsTab();
topMenu.clickSupplyShipmentExceptionsLate();

topSubMenu.clickSearchButton();
topSubMenu.goToSearchFeature(shipmentNo59a);
Assert.assertTrue(topMenu.isSupplyShipmentLateVisible());
topMenu.assertActualAndExpectedSupplyShipmentNumberEqual(shipmentNo59b);
topMenu.clearFilter();
driver.navigate().refresh();
}

@Test(priority = 60, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
public void validatingSupplyExceptionsStatusDelayedTC60(String shipmentNo60a,String shipmentNo60b) throws InterruptedException {
topMenu.goToSupplyShipmentsTab();
topMenu.clickExceptionsTab();
topMenu.clickSupplyShipmentExceptionsDelayed();
topSubMenu.clickSearchButton();
topSubMenu.goToSearchFeature(shipmentNo60a);
Assert.assertTrue(topMenu.isSupplyShipmentDelayedVisible());
topMenu.assertActualAndExpectedSupplyShipmentNumberEqual(shipmentNo60b);
topMenu.clearFilter();
driver.navigate().refresh();
}




@Test(priority = 65, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
public void validatingSupplyExceptionsStatusOnTimeTC65(String shipmentNo65a,String shipmentNo65b) throws InterruptedException {
topMenu.goToSupplyDeliveriesTab();
topSubMenu.clickOceanIcon();
topMenu.clickExceptionsTab();
topMenu.clickSupplyDeliveryExceptionsOnTime();
topSubMenu.clickSearchButton();
topSubMenu.goToSearchFeature(shipmentNo65a);
Assert.assertTrue(topMenu.isSupplyDeliveryExceptionsOnTimeVisible());
topMenu.assertActualAndExpectedSupplyDeliveryNumberEqualOcean(shipmentNo65b);
topMenu.clearFilter();
driver.navigate().refresh();
}


//Order Exceptions

@Test(priority = 66, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
public void validatingSupplyExceptionsStatusPastDueTC66(String shipmentNo66a,String shipmentNo66b)throws InterruptedException{
topMenu.goToSupplyOrdersTab();
topMenu.clickExceptionsTab();
topMenu.clickSupplyOrdersExceptionPastDue();
topSubMenu.clickSearchButton();
topSubMenu.goToSearchFeature(shipmentNo66a);
Assert.assertTrue(topMenu.isSupplyOrderPastDuesVisible());
topMenu.assertActualAndExpectedSupplyOdersNumberEqualOcean(shipmentNo66b);
topMenu.clearFilter();
driver.navigate().refresh();
topMenu.goToSupplyOrdersTab();

}

@Test(priority = 67, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
public void validatingSupplyExceptionsStatusLateTC67(String shipmentNo67a,String shipmentNo67b) throws InterruptedException {
topMenu.goToSupplyOrdersTab();
topMenu.clickExceptionsTab();
topMenu.clickSupplyOrdersExceptionsLate();
topSubMenu.clickSearchButton();
topSubMenu.goToSearchFeature(shipmentNo67a);
Assert.assertTrue(topMenu.isSupplyOrderLateVisible());
topMenu.assertActualAndExpectedSupplyOdersNumberEqualOcean(shipmentNo67b);
topMenu.clearFilter();
driver.navigate().refresh();
}
@Test(priority = 68, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
public void validatingSupplyExceptionsStatusUnconfirmedTC68(String shipmentNo68a,String shipmentNo68b) throws InterruptedException {
topMenu.goToSupplyOrdersTab();
topMenu.clickExceptionsTab();
topMenu.clickSupplyOrdersExceptionsUnconfirmed();
topSubMenu.clickSearchButton();
topSubMenu.goToSearchFeature(shipmentNo68a);
Assert.assertTrue(topMenu.isSupplyOrdersUnconfirmedVisible());
topMenu.assertActualAndExpectedSupplyOdersNumberEqualOcean(shipmentNo68b);
topMenu.clearFilter();
driver.navigate().refresh();
}


@Test(priority = 70, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
public void validatingSupplyExceptionsStatusPastDueTC70(String shipmentNo70a,String shipmentNo70b) throws InterruptedException {
topMenu.goToSupplyOrdersTab();
topMenu.clickExceptionsTab();
topMenu.clickSupplyOrdersExceptionPastDue();
topSubMenu.clickSearchButton();
topSubMenu.goToSearchFeature(shipmentNo70a);
Assert.assertTrue(topMenu.isSupplyOrdersExceptionPastDueVisible());
topMenu.assertActualAndExpectedSupplyOdersNumberEqualOcean(shipmentNo70b);
topMenu.clearFilter();
driver.navigate().refresh();
leftMenu.clickInventoryMenuOption();
}

}
package com.jda.dct.e2e.testscripts.uitests.supply;

import BaseTest;
import LeftMenu;
import com.jda.dct.e2e.ui.pageobjects.*;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.*;

public class SupplyExceptionsTests extends BaseTest {


private LeftMenu leftMenu;
private TopMenuPage topMenu;
private TopSubMenuPage topSubMenu;
private WebDriver driver;

@BeforeClass
public void setUp() throws InterruptedException {
System.out.println("Starting tests for :" + this.getClass().getName());
//Initialize required objects
driver = getDriver();

leftMenu = new LeftMenu(driver);
topMenu = new TopMenuPage(driver);
topSubMenu = new TopSubMenuPage(driver);

logInToApplication();
leftMenu.clickSupplyMenuOption();

}

@Test(priority = 54, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
public void validatingSupplyExceptionsStatusLateTC54(String shipmentNo54a,String shipmentNo54b){
topMenu.goToSupplyShipmentsTab();
topSubMenu.clickOceanIcon();
topMenu.clickExceptionsTab();
topMenu.clickSupplyShipmentExceptionsLate();
topSubMenu.clickSearchButton();
topSubMenu.clickSearchButton();
topSubMenu.goToSearchFeature(shipmentNo54a);
Assert.assertTrue(topMenu.isSupplyShipmentLateVisible());
topMenu.assertActualAndExpectedSupplyShipmentNumberEqualOcean(shipmentNo54b);
topMenu.clearFilter();
//driver.navigate().refresh();
}


@Test(priority = 55, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
public void validatingSupplyExceptionsStatusDelayedTC55(String shipmentNo55a,String shipmentNo55b)
{
topMenu.goToSupplyShipmentsTab();
topSubMenu.clickOceanIcon();
topMenu.clickExceptionsTab();
topMenu.clickSupplyShipmentExceptionsDelayed();
topSubMenu.clickSearchButton();
topSubMenu.clickSearchButton();
topSubMenu.goToSearchFeature(shipmentNo55a);
Assert.assertTrue(topMenu.isSupplyShipmentDelayedVisible());
topMenu.assertActualAndExpectedSupplyShipmentNumberEqualOcean(shipmentNo55b);

topMenu.clearFilter();
// driver.navigate().refresh();

}

@Test(priority = 56, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
public void validatingSupplyExceptionsStatusEarlyTC56(String shipmentNo56a,String shipmentNo56b)
{
topMenu.goToSupplyShipmentsTab();
topSubMenu.clickOceanIcon();
topMenu.clickExceptionsTab();
topMenu.clickSupplyShipmentExceptionsEarly();
topSubMenu.clickSearchButton();
topSubMenu.clickSearchButton();
topSubMenu.goToSearchFeature(shipmentNo56a);
Assert.assertTrue(topMenu.isSupplyShipmentEarlyVisible());
//topMenu.assertActualAndExpectedSupplyShipmentNumberEqualOcean(shipmentNo56b);
topMenu.clearFilter();
// driver.navigate().refresh();
}

@Test(priority = 57, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
public void validatingSupplyExceptionsStatusOnTimeTC57(String shipmentNo57a,String shipmentNo57b)
{
topMenu.goToSupplyShipmentsTab();
topMenu.clickExceptionsTab();
topMenu.clickSupplyShipmentExceptionsOnTime();
topSubMenu.clickSearchButton();
topSubMenu.clickSearchButton();
topSubMenu.goToSearchFeature(shipmentNo57a);
Assert.assertTrue(topMenu.isSupplyShipmentOnTimeVisible());
topMenu.assertActualAndExpectedSupplyShipmentNumberEqualOcean(shipmentNo57b);
topMenu.clearFilter();
// driver.navigate().refresh();
}




@Test(priority = 58, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
public void validatingSupplyExceptionsStatusDelayedTC58(String shipmentNo58a,String shipmentNo58b){

topMenu.goToSupplyShipmentsTab();
topMenu.clickExceptionsTab();
topMenu.clickSupplyShipmentExceptionsDelayed();
topSubMenu.clickSearchButton();
topSubMenu.clickSearchButton();
topSubMenu.goToSearchFeature(shipmentNo58a);
Assert.assertTrue(topMenu.isSupplyShipmentDelayedVisible());
topMenu.assertActualAndExpectedSupplyShipmentNumberEqualOcean(shipmentNo58b);
topMenu.clearFilter();  // webDriverWait is causing the test to fail
driver.navigate().refresh();
}
@Test(priority = 59, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
public void validatingSupplyExceptionsStatusLateTC59(String shipmentNo59a,String shipmentNo59b){

topMenu.goToSupplyShipmentsTab();
topMenu.clickExceptionsTab();
topMenu.clickSupplyShipmentExceptionsLate();
topSubMenu.clickSearchButton();

topSubMenu.clickSearchButton();
topSubMenu.goToSearchFeature(shipmentNo59a);
Assert.assertTrue(topMenu.isSupplyShipmentLateVisible());
topMenu.assertActualAndExpectedSupplyShipmentNumberEqualOcean(shipmentNo59b);
topMenu.clearFilter();
//driver.navigate().refresh();
}

@Test(priority = 60, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
public void validatingSupplyExceptionsStatusDelayedTC60(String shipmentNo60a,String shipmentNo60b)
{
topMenu.goToSupplyShipmentsTab();
topMenu.clickExceptionsTab();
topMenu.clickSupplyShipmentExceptionsDelayed();
topSubMenu.clickSearchButton();
topSubMenu.clickSearchButton();
topSubMenu.goToSearchFeature(shipmentNo60a);
Assert.assertTrue(topMenu.isSupplyShipmentDelayedVisible());
topMenu.assertActualAndExpectedSupplyShipmentNumberEqualOcean(shipmentNo60b);
topMenu.clearFilter();
//driver.navigate().refresh();
}



@Test(priority = 61, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
public void validatingSupplyExceptionsStatusEarlyTC61(String shipmentNo61a,String shipmentNo61b) throws InterruptedException {
topMenu.goToSupplyShipmentsTab();
topMenu.clickExceptionsTab();
topMenu.clickSupplyShipmentExceptionsEarly();
topSubMenu.clickSearchButton();
topSubMenu.clickSearchButton();
topSubMenu.goToSearchFeature(shipmentNo61a);
Assert.assertTrue(topMenu.isSupplyShipmentEarlyVisible());
// topMenu.assertActualAndExpectedSupplyShipmentNumberEqualOcean(shipmentNo61b);
topMenu.clearFilter();
//  driver.navigate().refresh();
}


// Delivery exceptions starts


@Test(priority = 62, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
public void validatingSupplyExceptionsStatusLateTC62(String shipmentNo62a,String shipmentNo62b) throws InterruptedException {
topMenu.goToSupplyDeliveriesTab();
topSubMenu.clickOceanIcon();
topMenu.clickExceptionsTab();
topMenu.clickSupplyDeliveryExceptionsLate();
topSubMenu.clickSearchButton();
topSubMenu.clickSearchButton();
topSubMenu.goToSearchFeature(shipmentNo62a);
Assert.assertTrue(topMenu.isSupplyDeliveryLateVisible());
topMenu.assertActualAndExpectedSupplyDeliveryNumberEqualOcean(shipmentNo62b);
topMenu.clearFilter();
// driver.navigate().refresh();
}


@Test(priority = 63, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
public void validatingSupplyExceptionsStatusDelayedTC63(String shipmentNo63a,String shipmentNo63b) throws InterruptedException {
topMenu.goToSupplyDeliveriesTab();
topSubMenu.clickOceanIcon();
topMenu.clickExceptionsTab();
topMenu.clickSupplyDeliveryExceptionsDelayed();
topSubMenu.clickSearchButton();
topSubMenu.clickSearchButton();
topSubMenu.goToSearchFeature(shipmentNo63a);
Assert.assertTrue(topMenu.isSupplyDeliveryDelayedVisible());
topMenu.assertActualAndExpectedSupplyDeliveryNumberEqualOcean(shipmentNo63b);
topMenu.clearFilter();
//driver.navigate().refresh();

}

@Test(priority = 64, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
public void validatingSupplyExceptionsStatusEarlyTC64(String shipmentNo64a,String shipmentNo64b) throws InterruptedException {
topMenu.goToSupplyDeliveriesTab();
topSubMenu.clickOceanIcon();
topMenu.clickExceptionsTab();
topMenu.clickSupplyDeliveryExceptionsEarly();// clickEarly
topSubMenu.clickSearchButton();
topSubMenu.clickSearchButton();
topSubMenu.goToSearchFeature(shipmentNo64a);
Assert.assertTrue(topMenu.isSupplyDeliveryEarlyVisible());
// topMenu.assertActualAndExpectedSupplyDeliveryNumberEqualOcean(shipmentNo64b);
topMenu.clearFilter();
//driver.navigate().refresh();

}

@Test(priority = 65, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
public void validatingSupplyExceptionsStatusOnTimeTC65(String shipmentNo65a,String shipmentNo65b) throws InterruptedException {
topMenu.goToSupplyDeliveriesTab();
topSubMenu.clickOceanIcon();
topMenu.clickExceptionsTab();
topMenu.clickSupplyDeliveryExceptionsOnTime();
topSubMenu.clickSearchButton();
topSubMenu.clickSearchButton();
topSubMenu.goToSearchFeature(shipmentNo65a);
Assert.assertTrue(topMenu.isSupplyDeliveryExceptionsOnTimeVisible());
topMenu.assertActualAndExpectedSupplyDeliveryNumberEqualOcean(shipmentNo65b);
topMenu.clearFilter();
driver.navigate().refresh();
}


//Order Exceptions

@Test(priority = 66, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
public void validatingSupplyExceptionsStatusPastDueTC66(String shipmentNo66a,String shipmentNo66b)
{
topMenu.goToSupplyOrdersTab();
topMenu.clickExceptionsTab();
topMenu.clickSupplyOrdersExceptionPastDue();
topSubMenu.clickSearchButton();
topSubMenu.clickSearchButton();
topSubMenu.goToSearchFeature(shipmentNo66a);
Assert.assertTrue(topMenu.isSupplyOrderPastDuesVisible());
topMenu.assertActualAndExpectedSupplyOdersNumberEqualOcean(shipmentNo66b);
topMenu.clearFilter();
driver.navigate().refresh();
topMenu.goToSupplyOrdersTab();

}

@Test(priority = 67, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
public void validatingSupplyExceptionsStatusLateTC67(String shipmentNo67a,String shipmentNo67b)
{
topMenu.goToSupplyOrdersTab();
topMenu.clickExceptionsTab();
topMenu.clickSupplyOrdersExceptionsLate();
topSubMenu.clickSearchButton();
topSubMenu.clickSearchButton();
topSubMenu.goToSearchFeature(shipmentNo67a);
Assert.assertTrue(topMenu.isSupplyOrderLateVisible());
// topMenu.assertActualAndExpectedSupplyOdersNumberEqualOcean(shipmentNo67b);
topMenu.clearFilter();
// driver.navigate().refresh();
}
@Test(priority = 68, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
public void validatingSupplyExceptionsStatusUnconfirmedTC68(String shipmentNo68a,String shipmentNo68b)
{
topMenu.goToSupplyOrdersTab();
topMenu.clickExceptionsTab();
topMenu.clickSupplyOrdersExceptionsUnconfirmed();
topSubMenu.clickSearchButton();
topSubMenu.clickSearchButton();
topSubMenu.goToSearchFeature(shipmentNo68a);
Assert.assertTrue(topMenu.isSupplyOrdersUnconfirmedVisible());
topMenu.assertActualAndExpectedSupplyOdersNumberEqualOcean(shipmentNo68b);
topMenu.clearFilter();
//driver.navigate().refresh();
}

@Test(priority = 69, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
public void validatingSupplyExceptionsStatusOnTimeTC69(String shipmentNo69a,String shipmentNo69b)
{
topMenu.goToSupplyOrdersTab();
topMenu.clickExceptionsTab();
topMenu.clickSupplyOrdersExceptionsOnTime();
topSubMenu.clickSearchButton();
topSubMenu.clickSearchButton();
topSubMenu.goToSearchFeature(shipmentNo69a);
Assert.assertTrue(topMenu.isSupplyOrdersOnTimeVisible());
topMenu.assertActualAndExpectedSupplyOdersNumberEqualOcean(shipmentNo69b);
topMenu.clearFilter();
//driver.navigate().refresh();
}

@Test(priority = 70, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
public void validatingSupplyExceptionsStatusPastDueTC70(String shipmentNo70a,String shipmentNo70b) throws InterruptedException {
topMenu.goToSupplyOrdersTab();
topMenu.clickExceptionsTab();
topMenu.clickSupplyOrdersExceptionPastDue();
topSubMenu.clickSearchButton();
topSubMenu.clickSearchButton();
topSubMenu.goToSearchFeature(shipmentNo70a);
Assert.assertTrue(topMenu.isSupplyOrdersExceptionPastDueVisible());
topMenu.assertActualAndExpectedSupplyOdersNumberEqualOcean(shipmentNo70b);
topMenu.clearFilter();
driver.navigate().refresh();
leftMenu.clickInventoryMenuOption();

}

//Supply Inventory Exceptions


@Test(priority = 71, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
public void validatingSupplyExceptionsStatusStockOutTC71(String shipmentNo71a,String shipmentNo71b) throws InterruptedException {

topMenu.clickSupplyInventoryStockOut();
topSubMenu.clickSearchButton();
topSubMenu.clickSearchButton();
topSubMenu.goToSearchFeature(shipmentNo71a);

Assert.assertTrue(topMenu.isSupplyInventoryStockedOutVisible());

topMenu.assertActualAndExpectedInventoryNumberEqual(shipmentNo71b);
topMenu.clearFilter();
// driver.navigate().refresh();
}


@Test(priority = 72, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
public void validatingSupplyExceptionsStatusLowStockTC72(String shipmentNo72a,String shipmentNo72b) throws InterruptedException {
topMenu.clickSupplyInventoryLowStock();
topSubMenu.clickSearchButton();
topSubMenu.clickSearchButton();
topSubMenu.goToSearchFeature(shipmentNo72a);
Assert.assertTrue(topMenu.isSupplyInventoryLowStockVisible());

topMenu.assertActualAndExpectedInventoryNumberEqual(shipmentNo72b);
topMenu.clearFilter();
driver.navigate().refresh();
}


@Test(priority = 73, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
public void validatingSupplyStatusOverstockTC73(String shipmentNo73a,String shipmentNo73b) throws InterruptedException {
leftMenu.clickInventoryMenuOption();
topMenu.clickSupplyInventoryOverstock();
topSubMenu.clickSearchButton();
topSubMenu.clickSearchButton();
topSubMenu.goToSearchFeature(shipmentNo73a);
Assert.assertTrue(topMenu.isSupplyInventoryOverstockVisible());
topMenu.assertActualAndExpectedInventoryNumberEqual(shipmentNo73b);
topMenu.clearFilter();
driver.navigate().refresh();
}

@Test(priority = 74, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
public void validatingSupplyStatusOnTrackSETC74(String shipmentNo574,String shipmentNo74b) throws InterruptedException {
// leftMenu.clickInventoryMenuOption();
topMenu.clickSupplyInventoryOnTrack();
topSubMenu.clickSearchButton();
topSubMenu.clickSearchButton();
topSubMenu.goToSearchFeature(shipmentNo574);
Assert.assertTrue(topMenu.isSupplyInventoryOnTrackVisible());
topMenu.assertActualAndExpectedInventoryNumberEqual(shipmentNo74b);
topMenu.clearFilter();
//driver.navigate().refresh();
}


//Supply-->Forecast-Commits



@Test(priority = 75, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
public void validatingSupplyStatusForecastCommitMismatchTC75(String shipmentNo75a,String shipmentNo75b) throws InterruptedException {
leftMenu.clickSupplyMenuOption();
topMenu.goToSupplyForecastCommitTab();

topMenu.clickSupplyForecastCommitsForecastCommitMismatch();
topSubMenu.clickSearchButton();
topSubMenu.clickSearchButton();
topSubMenu.goToSearchFeature(shipmentNo75a);
Assert.assertTrue(topMenu.issupplyForecastCommitsForecastCommitMismatchVisible());
topMenu.assertActualAndExpectedSupplyForecastCommitsNumberEqualOcean(shipmentNo75b);
topMenu.clearFilter();
//driver.navigate().refresh();
}

@Test(priority = 76, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
public void validatingSupplyStatusNewForecastTC76(String shipmentNo76a,String shipmentNo76b)
{
topMenu.goToSupplyForecastCommitTab();
topMenu.clickSupplyForecastCommitsNewForecast();
topSubMenu.clickSearchButton();
topSubMenu.clickSearchButton();
topSubMenu.goToSearchFeature(shipmentNo76a);
Assert.assertTrue(topMenu.issupplyForecastCommitsNewForecastVisible());
topMenu.assertActualAndExpectedSupplyForecastCommitsNumberEqualOcean(shipmentNo76b);
topMenu.clearFilter();
//driver.navigate().refresh();
}


// Note - Test SETC77 & SETC78 tabs seems to be removed from the Application. May uncomment cases in the future.

@Test(priority = 77, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
public void validatingSupplyStatusForecastRevisedTC77(String shipmentNo77a,String shipmentNo77b)
{ //Note that the Revised tab is missing
topMenu.goToSupplyForecastCommitTab();
topMenu.clickSupplyForecastCommitsForecastRevision();
topSubMenu.clickSearchButton();
topSubMenu.clickSearchButton();
topSubMenu.goToSearchFeature(shipmentNo77a);
Assert.assertTrue(topMenu.isSupplyForecastCommitsForecastRevisionVisible());
//  topMenu.assertActualAndExpectedSupplyForecastCommitsNumberEqualOcean(shipmentNo77b);
topMenu.clearFilter();
// driver.navigate().refresh();
}

@Test(priority = 78, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
public void validatingSupplyStatusForecastCommitInterlockTC78(String shipmentNo78a,String shipmentNo78b) throws InterruptedException {
topMenu.goToSupplyForecastCommitTab();
topMenu.clickSupplyForecastCommitsForecastCommitInterlock();
topSubMenu.clickSearchButton();
topSubMenu.clickSearchButton();
topSubMenu.goToSearchFeature(shipmentNo78a);
Assert.assertTrue(topMenu.issupplyForecastCommitsForecastCommitInterlockVisible());
//  topMenu.assertActualAndExpectedSupplyForecastCommitsNumberEqualOcean(shipmentNo78b);
topMenu.clearFilter();
driver.navigate().refresh();
leftMenu.clickSupplyMenuOption();
}



}
*/


