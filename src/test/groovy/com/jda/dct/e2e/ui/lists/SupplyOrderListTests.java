package com.jda.dct.e2e.ui.lists;

import com.jda.dct.e2e.ui.utility.action.CommonActions;
import com.jda.dct.e2e.ui.utility.common.BaseTest;
import com.jda.dct.e2e.ui.utility.common.LeftMenu;
import com.jda.dct.e2e.ui.utility.helper.AssertHelper.AssertHelper;
import com.jda.dct.e2e.ui.utility.helper.Browser.BrowserHelper;
import com.jda.dct.e2e.ui.utility.pageobjects.*;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class SupplyOrderListTests extends BaseTest {

    private LeftMenu leftMenu;
    private TopMenuPage topMenu;
    private TopSubMenuPage topSubMenu;
    private SupplyShipmentListPage supplyShipmentListPage;
    private SupplyShipmentDetailsPage supplyShipmentDetailsPage;
    private SupplyOrderListPage supplyOrderlistPage;
    private AssertHelper assertHelper;
    private BrowserHelper browserHelper;
    private InventoryListPage inventoryListPage;
    private CommonActions commonActions;
    private WebDriver driver;


    @BeforeClass
    public void setUp() throws InterruptedException {
        System.out.println("Starting tests for :" + this.getClass().getName());
        //Initialize required objects
        driver = getDriver();
        leftMenu = new LeftMenu(driver);
        topMenu = new TopMenuPage(driver);
        topSubMenu = new TopSubMenuPage(driver);
        supplyShipmentListPage = new SupplyShipmentListPage(driver);
        supplyShipmentDetailsPage = new SupplyShipmentDetailsPage(driver);
        supplyOrderlistPage = new SupplyOrderListPage(driver);
        inventoryListPage = new InventoryListPage(driver);
        assertHelper = new AssertHelper(driver);
        browserHelper = new BrowserHelper(driver);
        commonActions = new CommonActions(driver);
        //loginAndNavigateToSupplyLandingPage
        logInToApplication();
    }


    @BeforeMethod
    public void commonMethods() {
        leftMenu.clickSupplyMenuOption();
        topMenu.goToSupplyOrdersTab();
    }

    @Test(priority = 1,retryAnalyzer = BaseTest.class)
    public void validateSupplyOrderListPageIsDisplayed() throws InterruptedException {
//verify user is at Supply Order List page
        commonActions.disableAllExceptionFilter();
        assertHelper.assertTrue(supplyOrderlistPage.isSupplyOrderListPageDisplayed(), "Supply Order list page is displayed successfully");
        supplyOrderlistPage.clickBackToSupplyOrdersTab();
        browserHelper.refresh();
    }
}

