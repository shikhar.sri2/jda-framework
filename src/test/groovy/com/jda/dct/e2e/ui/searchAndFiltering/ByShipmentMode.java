package com.jda.dct.e2e.ui.searchAndFiltering;//
//
//package com.jda.dct.e2e.ui.searchAndFiltering;
//
//import BaseTest;
//import LeftMenu;
//import com.jda.dct.e2e.utility.ui.pageobjects.*;
//import TestDataFile;
//import org.openqa.selenium.WebDriver;
//import org.testng.Assert;
//import org.testng.annotations.BeforeClass;
//import org.testng.annotations.Test;
//
//public class ByShipmentMode extends BaseTest {
//
//    private LeftMenu leftMenu;
//    private TopMenuPage topMenu;
//    private SupplyDeliveryListPage supplyDeliveryPage;
//    private SupplyShipmentListPage supplyShipmentListPage;
//    private SupplyShipmentDetailsPage supplyShipmentDetailsPage;
//    private TopSubMenuPage topSubMenu;
//    private WebDriver driver;
//
//    @BeforeClass
//    public void setUp() throws InterruptedException {
//        System.out.println("Starting tests for :" + this.getClass().getName());
////Initialize required objects
//        driver = getDriver();
//        leftMenu = new LeftMenu(driver);
//        topMenu = new TopMenuPage(driver);
//        topSubMenu = new TopSubMenuPage(driver);
//        supplyDeliveryPage = new SupplyDeliveryListPage(this.driver);
//        supplyShipmentListPage = new SupplyShipmentListPage(this.driver);
//        supplyShipmentDetailsPage = new SupplyShipmentDetailsPage(this.driver);
//        //loginAndNavigateToSupplyShipmentLandingPage
//        logInToApplication();
//        leftMenu.clickSupplyMenuOption();
//    }
//
//    @Test(priority = 1)
//    //Validate Supply Delivery  page - change method name and assertion
//    public void validatingFilteringOcean() throws InterruptedException {
//     //  leftMenu.clickSupplyMenuOption();
//        String shipmentNo = "IS0036_SD_S2";
//        topMenu.goToSupplyShipmentsTab();
//        topMenu.clickExceptionsTab();
//        topMenu.clickSupplyShipmentExceptionsDelayed();
//        topSubMenu.clickOceanIcon();
//        topSubMenu.goToSearchFeature(shipmentNo);
//        topMenu.assertActualAndExpectedDemandShipmentNumberEqualOcean1();
//        supplyShipmentListPage.clickExpandButton();
//        supplyShipmentListPage.clickSupplyContainerNumber();
////verify that user lands on SupplyDeliveryList page
//        Assert.assertTrue(supplyShipmentDetailsPage.isSupplyDeliveryPageDisplayed());
//        supplyShipmentDetailsPage.clickBackToSupplyShipmentTab();
//       // topMenu.clearFilter();
//        driver.navigate().refresh();
//    }
//
//
//    @Test(priority = 2)
//    //Validate Supply Delivery  page - change method name and assertion
//    public void validatingFilteringRoad() throws InterruptedException {
//      //  leftMenu.clickSupplyMenuOption();
//        String shipmentNo = "IS0035_SD_S1";
//        topMenu.goToSupplyShipmentsTab();
//        topMenu.clickExceptionsTab();
//        topMenu.clickSupplyShipmentExceptionsDelayed();
//        topSubMenu.clickRoadIcon();
//        topSubMenu.goToSearchFeature(shipmentNo);
//        topMenu.assertActualAndExpectedDemandShipmentNumberEqualRoad();
//
//        supplyShipmentListPage.clickExpandButton();
//        supplyShipmentListPage.clickSupplyTrailerNumber();
//
////verify that user lands on SupplyDeliveryList page
//        Assert.assertTrue(supplyShipmentDetailsPage.isSupplyDeliveryPageDisplayed());
//        supplyShipmentDetailsPage.clickBackToSupplyShipmentTab();
//        //topMenu.clearFilter();
//        driver.navigate().refresh();
//    }
//
//    @Test(priority = 108 , dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
//    public void validateMonetaryImpactOnSalesAndDemandTC108(String shipmentNo108a, String shipmentNo108b, String FG108c)
//    {
//        topMenu.goToSupplyShipmentsTab();
//        topSubMenu.clickOceanIcon();
//        topMenu.clickExceptionsTab();
//        topMenu.clickSupplyShipmentExceptionsDelayed();  //Shipped 81
//        topSubMenu.clickSearchButton();
//        topSubMenu.goToSearchFeature(shipmentNo108a);
//        topMenu.assertActualAndExpectedSupplyShipmentNumberEqual(shipmentNo108b);
//        supplyShipmentListPage.clickSupplyContainerNumber();
//        supplyShipmentListPage.clickSupplyShipmentAllItems();
//       // topMenu.clickfinishedGoods(); //Finished Goods
//       // topMenu.clickfinishedGoods(); //monitor
//        topMenu.clickInventoryStockout();
//        topSubMenu.clickSearchButton();
//        topSubMenu.goToSearchFeature(FG108c);
//        topMenu.clickinventoryItemNumber();
//        topMenu.clickInventoryMonetaryImpact();
//        //click on sales tab
//        topMenu.clickInventoryMonetaryImpactSalesTab();
//        topMenu.IsInventoryMonetaryImpactSalesAmountVisible();
//        topMenu.clickInventoryMonetaryImpactDemandTab();
//        topMenu.IsInventoryMonetaryImpactDemandAmountVisible();
//        //click saleorder
//        topMenu.clickInventoryMonetaryImpactSalesTab();
//        topMenu.clickInventoryMonetaryImpactSalesOrderNumber();
//        topMenu.IsInventoryMonetaryImpactSalesAmountVisible();
//        //topMenu.clearFilter();
//        driver.navigate().refresh();
//    }
//
//
//}
