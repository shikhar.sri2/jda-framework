
package com.jda.dct.e2e.ui.utility.DataProvider;


import com.jda.dct.e2e.ui.utility.ExcelHelper.ExcelReaderForDataDrivenTest;
import com.jda.dct.e2e.ui.utility.common.BaseTest;
import org.testng.annotations.DataProvider;

import java.lang.reflect.Method;
import java.util.*;

public class TestDataProvider {

    private String sTestCaseName;

    @DataProvider(name = "TestRuns")
    public Iterator<Object[]> getEntireSheetData(Method m) {

        ExcelReaderForDataDrivenTest excel = new ExcelReaderForDataDrivenTest();
        sTestCaseName = m.getName();

        List<Map<String, String>> testRecords = excel.getExcelData(sTestCaseName);
        Collection<Object[]> collection = new ArrayList<Object[]>();

        for (Map<String, String> testRecord : testRecords) {
            collection.add(new Map[]{testRecord});
            BaseTest.dataItem.add(testRecord);
        }

        return collection.iterator();
    }

/*    @SuppressWarnings("unused")
	@DataProvider(name = "specifcRow")
    public Iterator<Object[]> getSpecificRowdata() {

        ExcelReaderForDataDrivenTest excel = Bas.ms.reader;

        List<Map<String, String>> testRecords = excel.getExcelData();
        Collection<Object[]> collection = new ArrayList<Object[]>();

        for (Map<String, String> testRecord : testRecords)
            collection.add(new Map[]{testRecord});

        return collection.iterator();
    }*/
}
