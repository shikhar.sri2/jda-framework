//package com.jda.dct.e2e.testscripts.uitests.supply;
//
//import com.jda.dct.e2e.qa.common.BaseTest;
//import com.jda.dct.e2e.qa.common.LeftMenu;
//import com.jda.dct.e2e.qa.pageobjects.*;
//import org.openqa.selenium.WebDriver;
//import org.testng.Assert;
//import org.testng.annotations.BeforeClass;
//import org.testng.annotations.Test;
//
//public class SupplyInventoryListTests extends BaseTest {
//
//    private LeftMenu leftMenu;
//    private TopMenuPage topMenu;
//    private TopSubMenuPage topSubMenu;
//    private SupplyShipmentListPage supplyShipmentListPage;
//    private SupplyShipmentDetailsPage supplyShipmentDetailsPage;
//    private SupplyInventoryListPage supplyInventoryListPage;
//    private WebDriver driver;
//
//
//    @BeforeClass
//    public void setUp() throws InterruptedException {
//        System.out.println("Starting tests for :" + this.getClass().getName());
//        //Initialize required objects
//        driver = getDriver();
//        leftMenu = new LeftMenu(driver);
//        topMenu = new TopMenuPage(driver);
//        topSubMenu = new TopSubMenuPage(driver);
//        supplyShipmentListPage = new SupplyShipmentListPage(driver);
//        supplyShipmentDetailsPage = new SupplyShipmentDetailsPage(driver);
//        supplyInventoryListPage = new SupplyInventoryListPage(driver);
//
//        //loginAndNavigateToSupplyLandingPage
//        logInToApplication();
//        leftMenu.clickSupplyMenuOption();
//    }
//
//    @Test(priority = 1)
//    public void validateSupplyInventoryListPageIsDisplayed() throws InterruptedException {
//        leftMenu.clickSupplyMenuOption();
//        //user is at SupplyInventory tab
//        topMenu.goToSupplyInventoryTab();
////verify user is at Supply Inventory List page
//        Assert.assertTrue(supplyInventoryListPage.isSupplyInventoryListPageDisplayed());
//        System.out.println(" Welcome to Supply Inventory List Page");
//    }
//
//    @Test(priority = 2)
//    public void validateSupplyInventoryDetailPageIsDisplayed() throws InterruptedException{
//        leftMenu.clickSupplyMenuOption();
//        topMenu.goToSupplyInventoryTab();
//        supplyInventoryListPage.clickInventoryItemNumber();
////verify user lands at  Supply Inventory Details Page
//        Assert.assertTrue(supplyInventoryListPage.isSupplyInventoryDetailsPageDisplayed());
//        supplyInventoryListPage.clickBackToInventoryPage();
//
//    }
//
////    @Test(priority = 3)
////    public void validateSupplyInventoryListPageIsDisplayedWhenNotesItemIsClicked ()throws InterruptedException {
////        leftMenu.clickSupplyMenuOption();
////        topMenu.goToSupplyInventoryTab();
////        supplyShipmentListPage.clickExpandButton();
//////user click at inventory notes
////        supplyShipmentDetailsPage.clickNotes();
////        Thread.sleep(3000);
////        Assert.assertTrue(supplyShipmentDetailsPage.isNotesPageDisplayed());
//// //user clicks at Notes button to go back to Supply Inventory List Page
////        supplyShipmentDetailsPage.clickBackToNotes();
//////verify user lands back to SupplyInventoryListPage
////        Assert.assertTrue(supplyInventoryListPage.isSupplyInventoryListPageDisplayed());
////    }
//
//
//}
//
