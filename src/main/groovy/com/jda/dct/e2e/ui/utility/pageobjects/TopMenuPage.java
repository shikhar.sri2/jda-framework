package com.jda.dct.e2e.ui.utility.pageobjects;

import com.jda.dct.e2e.ui.utility.common.BasePage;
import com.jda.dct.e2e.ui.utility.helper.Wait.WaitHelper;
import com.jda.dct.e2e.ui.utility.helper.genericHelper.GenericHelper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class TopMenuPage extends BasePage {

    WebDriver driver;
    WebDriverWait webDriverWait;
    WaitHelper objWaitHelper;
    GenericHelper objGenericHelper;
    private Object pass;

    public TopMenuPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(this.driver, this); //local argument is being initialized to the local driver object?
        webDriverWait = new WebDriverWait(this.driver, 90);
        objWaitHelper = new WaitHelper(driver);
        objGenericHelper = new GenericHelper(driver);
    }


    @FindBy(xpath = "//*[@href='/supply/shipment']")
    public WebElement supplyShipmentLink;

    @FindBy(xpath = "//*[@id=\"icon_previous\"]")
    public WebElement previousIcon;

    @FindBy(xpath = "//*[@id=\"icon_next\"]")
    public WebElement nextIcon;

    // Supply -> DeliveryPage
    @FindBy(xpath = "//*[@href='/supply/delivery']")
    public WebElement supplyDeliveryLink;

    // Supply -> Purchase-OrdersPage
    @FindBy(xpath = "//*[@href='/supply/purchase-orders']")
    //@FindBy(id = "tempraturecontrolled-page-tab-purchase-orders")
    public WebElement supplyOrdersLink;

    // Supply -> Forecast-CommitsPage
    @FindBy(id = "supply-page-tab-forecast-commits")
    public WebElement supplyForecastCommitsLink;


    //demand -> ShipmentPage
    @FindBy(xpath = "//*[@href='/demand/shipment']")
    public WebElement demandShipment;
    // demand -> DeliveryPage
    @FindBy(xpath = "//*[@href='/demand/delivery']")
    public WebElement demandDelivery;
    // demand -> Sales-OrdersPage
    @FindBy(xpath = "//*[@href='/demand/sales-orders']")
    public WebElement demandSalesOrders;
    // demand -> InventoryPage

    @FindBy(xpath = "//div/div[@id='SideBar-Item-Inventory']/span[text()='INVENTORY']")
    //@FindBy(id ="NavPanel-Item-INVENTORY")
    public WebElement inventorySideBar;

    //=================Top Menu navigation Master Data WebElements
    @FindBy(id = "master-page-tab-items")
    public WebElement itemLink;
    @FindBy(id = "master-page-tab-sites")
    public WebElement sitesLink;
    @FindBy(id = "master-page-tab-nodes")
    public WebElement nodesLink;
    @FindBy(id = "master-page-tab-lot")
    public WebElement batchesLink;
    @FindBy(id = "master-page-tab-sourcing-lanes")
    public WebElement sourcingLanesLink;
    @FindBy(id = "master-page-tab-participant")
    public WebElement participantsLink;


    @FindBy(xpath="//span[text()='Exceptions']")
    public WebElement selectExceptionsTab;


    @FindBy(id = "supply-inboundShipment-Filter-List-Card-PTA Unknown")  //
    public WebElement supplyShipmentExceptionsPTA;

//Supply Shipment Exceptions Page Factory

    @FindBy(id = "supply-inboundShipment-Filter-List-Card-Late")
    public WebElement supplyShipmentExceptionsLate;

    //    @FindBy(xpath = "//p[contains(text(),'Delayed')]")
    @FindBy(id = "supply-inboundShipment-Filter-List-Card-Delayed")
    public WebElement supplyShipmentExceptionsDelayed;

    @FindBy(id = "supply-inboundShipment-Filter-List-Card-Early")
    public WebElement supplyShipmentExceptionsEarly;

    @FindBy(id = "supply-inboundShipment-Filter-List-Card-On Time")
    public WebElement supplyShipmentExceptionsOnTime;


    @FindBy(id = "PanelSummary-RenderedItem-Shipment-No-1-LinkItem")

    // @FindBy(id ="PanelSummary-RenderedItem-Shipment No-LinkItem")
    private WebElement supplyShipmentAssertionPointOcean; //feb

    @FindBy(id = "PanelSummary-RenderedItem-Delivery No-LinkItem")
    private WebElement supplyDeliveryAssertionPointOcean; //feb

    @FindBy(id = "PanelSummary-RenderedItem-Order No-LinkItem")
    private WebElement supplyOrdersAssertionPointOcean; //feb


//Supply Shipment States Page Factory
    //Supply-->Shipment-->States
    //Supply Shipment States (New, Planned Shipment, Shipped, InTransit, Delivered, Received, Closed)
    //Supply Shipment Exceptions ( Late, Delayed, Early, OnTime)

    @FindBy(id = "supply-inboundShipment-Filter-List-Card-New")
    public WebElement supplyShipmentStatesNew;

    @FindBy(id = "supply-inboundShipment-Filter-List-Card-Planned Shipment")
    public WebElement supplyShipmentStatesPlannedShipment;

    @FindBy(id = "supply-inboundShipment-Filter-List-Card-Shipped")
    public WebElement supplyShipmentStatesShipped;

    @FindBy(id = "supply-inboundShipment-Filter-List-Card-In Transit")
    public WebElement supplyShipmentStatesInTransit;

    @FindBy(id = "supply-inboundShipment-Filter-List-Card-Delivered")
    public WebElement supplyShipmentStatesDelivered;

    @FindBy(id = "supply-inboundShipment-Filter-List-Card-Received")
    public WebElement supplyShipmentStatesReceived;

    @FindBy(id = "supply-inboundShipment-Filter-List-Card-Closed")
    public WebElement supplyShipmentStatesClosed;


    @FindBy(id = "PanelSummary-RenderedItem-Carrier-LinkItem")
    // @FindBy(id = "PanelSummary-RenderedItem-Shipment No-LinkItem")  //ID Fails
    private WebElement supplyShipmentCarrierAssertionPoint;

    @FindBy(xpath = "//a[contains(@href,'/supply/shipment/a4b7')]")
    // @FindBy(id = "PanelSummary-RenderedItem-Shipment No-LinkItem")  //ID Fails
    private WebElement supplyShipmentAssertionPoint;


    //Supply ->Deliveries

    @FindBy(id = "supply-inboundDelivery-Filter-List-Card-PTA Unknown")  //
    public WebElement supplyDeliveryExceptionsPTA;

    @FindBy(id = "supply-inboundDelivery-Filter-List-Card-Late")
    public WebElement supplyDeliveryExceptionsLate;

    @FindBy(id = "supply-inboundDelivery-Filter-List-Card-Early")
    public WebElement supplyDeliveryExceptionsEarly;


    @FindBy(id = "supply-inboundDelivery-Filter-List-Card-Delayed")
    public WebElement supplyDeliveryExceptionsDelayed;


    @FindBy(id = "supply-inboundDelivery-Filter-List-Card-On Time")
    public WebElement supplyDeliveryExceptionsOnTime;


//Demand Shipment

    @FindBy(id = "demand-outboundShipment-Filter-List-Card-PTA Unknown")
    private WebElement demandShipmentExceptionsPTAUnknown;

    @FindBy(id = "demand-outboundShipment-Filter-List-Card-Late")
    public WebElement demandShipmentExceptionsLate;

    @FindBy(id = "demand-outboundShipment-Filter-List-Card-Delayed")
    public WebElement demandShipmentExceptionsDelayed;

    @FindBy(id = "demand-outboundShipment-Filter-List-Card-Early")
    public WebElement demandShipmentExceptionsEarly;

    @FindBy(id = "demand-outboundShipment-Filter-List-Card-On Time")
    public WebElement demandShipmentExceptionsOnTime;


    @FindBy(id = "PanelSummary-RenderedItem-Shipment No-LinkItem")

    private WebElement demandShipmentAssertionPointOcean;//Ocean

    @FindBy(xpath = "//a[@id='PanelSummary-RenderedItem-Shipment-No-1-LinkItem' and text()='IS0036_SD_S2']")
    private WebElement demandShipmentAssertionPointOceans;//Ocean

    @FindBy(xpath = "//a[@id='PanelSummary-RenderedItem-Shipment No-LinkItem' and text()='IS0035_SD_S1']")
    private WebElement demandShipmentAssertionPointRoad;//Road


    @FindBy(id = "PanelSummary-RenderedItem-Delivery-No-1-LinkItem") // just
    // @FindBy(xpath = "//p/a[@id='PanelSummary-RenderedItem-Delivery-No-1-LinkItem']") // just
    private WebElement demandDeliveryAssertionPointOcean;

    // @FindBy(xpath = "//p/a[@id='PanelSummary-RenderedItem-Order No-LinkItem']") // For All
    @FindBy(id = "PanelSummary-RenderedItem-Order-No-1-LinkItem") // For All
    private WebElement demandOdersAssertionPoint;

    @FindBy(xpath = "//a[@id='PanelSummary-RenderedItem-Item-LinkItem']") // For All
    // @FindBy(xpath ="//a[@id='PanelSummary-RenderedItem-Shipment No-LinkItem' and text()='OS0034_DL_S1']") //
    private WebElement InventoryAssertionPoint;


    @FindBy(id = "ExpansionPanel-ExpandMoreIcon")
    private WebElement expandButton;

//Demand Delivery

    @FindBy(id = "demand-outboundDelivery-Filter-List-Card-Late")
    public WebElement demandDeliveryExceptionsLate;

    @FindBy(id = "demand-outboundDelivery-Filter-List-Card-Delayed")
    public WebElement demandDeliveryExceptionsDelayed;

    @FindBy(id = "demand-outboundDelivery-Filter-List-Card-Early")
    public WebElement demandDeliveryExceptionsEarly;

    @FindBy(id = "demand-outboundDelivery-Filter-List-Card-On Time")
    public WebElement demandDeliveryExceptionsOnTime;

    @FindBy(id = "demand-outboundDelivery-Filter-List-Card-_BLANK")
    public WebElement demandDeliveryExceptionsNoExceptions;


    //Supply-->Orders-->Exceptions Page Factory Starts

    //Past Due
    @FindBy(id = "supply-standardPO-Filter-List-Card-Past Due")
    public WebElement supplyOrdersExceptionsPastDue;
    //Late
    @FindBy(id = "supply-standardPO-Filter-List-Card-Late")
    public WebElement supplyOrdersExceptionsLate;

    //Unconfirmed
    @FindBy(id = "supply-standardPO-Filter-List-Card-Unconfirmed")
    public WebElement supplyOrdersExceptionUnconfirmed;

    //OnTime
    @FindBy(id = "supply-standardPO-Filter-List-Card-On Time")
    public WebElement supplyOrdersExceptionOnTime;

    //No Exceptions
    @FindBy(id = "supply-standardPO-Filter-List-Card-_BLANK")
    public WebElement supplyOrdersNoExceptions;


    //Demand Orders

    @FindBy(id = "demand-standardOrder-Filter-List-Card-Past Due")
    public WebElement demandOrdersExceptionsPastDue;

    @FindBy(id = "demand-standardOrder-Filter-List-Card-Late")
    public WebElement demandOrdersExceptionsLate;

    @FindBy(id = "demand-standardOrder-Filter-List-Card-Unconfirmed")
    public WebElement demandOrdersExceptionsUnconfirmed;

    @FindBy(id = "demand-standardOrder-Filter-List-Card-On Time")
    public WebElement demandOrdersExceptionsOnTime;

    @FindBy(id = "demand-standardOrder-Filter-List-Card-_BLANK")
    public WebElement demandOrdersExceptionsNoExceptions;


    //Demand Inventory

    @FindBy(id = "demandSupply-inventory-Filter-List-Card-Stockout")
    public WebElement InventoryStockout;


    //  @FindBy(xpath ="//button[@value='Finished Goods']")
    @FindBy(xpath = "//p[text()='Finished Goods']")
    public WebElement inventoryFinishedGoods;

    @FindBy(id = "PanelSummary-RenderedItem-Item-1-LinkItem")
    public WebElement inventoryItemNumber;

    @FindBy(id = "PanelSummary-RenderedItem-Monetary-Impact-0-drawer-link")
    public WebElement inventoryMonetaryImpact;


    @FindBy(xpath = "//button[@value='Components']")
    public WebElement components;
    //supply Forecast-Commits PageFactory

    @FindBy(xpath = "//span[text()='Sales']")
    public WebElement inventoryMonetaryImpactSales;

    // @FindBy(id ="PanelSummary-RenderedItem-Order-No-0-LinkItem")
    @FindBy(xpath = "//a[text()='SO0070_NO']")
    public WebElement inventoryMonetaryImpactSalesOrderNumber;


    @FindBy(xpath = "//span[text()='Demand']")
    public WebElement inventoryMonetaryImpactDemand;


    @FindBy(id = "supply-procurement-Filter-List-Card-Forecast-Commit Mismatch")
    public WebElement supplyForecastCommitsForecastCommitMismatch;


    @FindBy(id = "supply-procurement-Filter-List-Card-New Forecast")
    public WebElement supplyForecastCommitsNewForecast;


    @FindBy(id = "supply-procurement-Filter-List-Card-Revised Forecast")
    public WebElement supplyForecastCommitsForecastRevisedForecast;


    @FindBy(id = "supply-procurement-Filter-List-Card-Forecast-Commit Interlock")
    public WebElement supplyForecastCommitsForecastCommitInterlock;

    @FindBy(id = "supply-procurement-Filter-List-Card-New Forecast")
    public WebElement supplyForecastCommitsForecastCommitNewForecast;

    // @FindBy(xpath ="//a[contains(@href,'/supply/forecast-commits/a4b')]")
    @FindBy(id = "PanelSummary-RenderedItem-Item-LinkItem")
    public WebElement supplyForecastCommitsAssertionPoint;

    //Clear Filter After Each Search
    @FindBy(xpath = "//div[@role='button']/../button")
    public WebElement clearFilterSearchResult;


//Supply Delivery States (Delivered, Received, Shipped, Plannned Shipment)
//Supply Delivery Exceptions(Late, Delayed, Early, OnTime)

    @FindBy(id = "supply-inboundDelivery-Filter-List-Card-Delivered")
    public WebElement supplyDeliveryStatesDelivered;


    @FindBy(id = "supply-inboundDelivery-Filter-List-Card-Received")
    public WebElement supplyDeliveryStatesReceived;


    @FindBy(id = "supply-inboundDelivery-Filter-List-Card-Shipped")
    public WebElement supplyDeliveryStatesShipped;


    @FindBy(id = "supply-inboundDelivery-Filter-List-Card-Planned Shipment")
    public WebElement supplyDeliveryStatesPlannedShipment;


    @FindBy(xpath = "//a[contains(@href,'/supply/delivery/a4b')]") //For all
    private WebElement SupplyDeliveryAssertionPoint;


    @FindBy(id = "PanelSummary-RenderedItem-Order No-LinkItem") //great
    private WebElement supplyOrdersAssertionPoint;


    //Order States
// Supply Orders Exceptions( Past Due, Late, Unconfirmed, OnTime, NoExceptions)
// Supply Orders States(Created, Shipped, Planned Shipment, Open, New, Cancelled, ,
//Confirmed, Confirmed with changes, Closed, Received and Rejected
    @FindBy(id = "supply-standardPO-Filter-List-Card-Created")
    public WebElement supplyOrdersStatesCreated;


    @FindBy(id = "supply-standardPO-Filter-List-Card-Shipped")
    public WebElement SupplyOrdersStatesShipped;

    @FindBy(id = "supply-standardPO-Filter-List-Card-Planned Shipment")
    public WebElement supplyOrdersStatesPlannedShipment;

    @FindBy(id = "supply-standardPO-Filter-List-Card-Open")
    public WebElement supplyOrdersStatesOpen;

    @FindBy(id = "supply-standardPO-Filter-List-Card-New")
    public WebElement supplyOrdersStatesNew;

    @FindBy(id = "supply-standardPO-Filter-List-Card-Cancelled")
    public WebElement supplyOrdersStatesCancelled;

    @FindBy(id = "supply-standardPO-Filter-List-Card-Confirmed")
    public WebElement supplyOrdersStatesConfirmed;

    @FindBy(id = "supply-standardPO-Filter-List-Card-Confirmed with Changes")
    public WebElement supplyOrdersStatesConfirmedWithChanges;


    @FindBy(id = "supply-standardPO-Filter-List-Card-Closed")
    public WebElement supplyOrdersStatesClosed;

    @FindBy(id = "supply-standardPO-Filter-List-Card-Received")
    public WebElement supplyOrdersStatesReceived;

    @FindBy(id = "supply-standardPO-Filter-List-Card-Rejected")
    public WebElement supplyOrdersStatesRejected;

// Inventory Page Factory


    // @FindBy(id ="PanelSummary-RenderedItem-Amount-0-LinkItem")
    @FindBy(xpath = "//span[text()='86,330.00']")
    public WebElement InventoryDemandAmountAssertionPoint;


    // @FindBy(id ="PanelSummary-RenderedItem-Amount-0-LinkItem")
    @FindBy(xpath = "//span[text()='12,000.00']")
    public WebElement InventorySalesAmountAssertionPoint;

    @FindBy(id = "demandSupply-inventory-Filter-List-Card-Stockout")
    // @FindBy(xpath ="//*[@id=\"demandSupply-inventory-Filter-List-Card-Stockout\"]")
    public WebElement inventoryStockOut;


    @FindBy(id = "demandSupply-inventory-Filter-List-Card-Stockout")
    public WebElement inventoryLowStock;

    @FindBy(id = "demandSupply-inventory-Filter-List-Card-Overstock")
    public WebElement inventoryOverstock;

    @FindBy(id = "demandSupply-inventory-Filter-List-Card-On-Track")
    public WebElement inventoryOnTrack;

    @FindBy(xpath = "//a[contains(@href,'/supply/inventory/a4b')]")
    private WebElement inventoryAssertionPoint;


//Demand Shipment States

    @FindBy(id = "demand-outboundShipment-Filter-List-Card-New")
    public WebElement demandShipmentStatesNew;

    @FindBy(id = "demand-outboundShipment-Filter-List-Card-Planned Shipment")
    public WebElement demandShipmentStatesPlannedShipment;

    @FindBy(id = "demand-outboundShipment-Filter-List-Card-Shipped")
    public WebElement demandShipmentStatesShipped;


    @FindBy(id = "demand-outboundShipment-Filter-List-Card-In Transit")
    public WebElement demandShipmentStatesInTransit;

    @FindBy(id = "demand-outboundShipment-Filter-List-Card-Delivered")
    public WebElement demandShipmentStatesDelivered;

    @FindBy(id = "demand-outboundShipment-Filter-List-Card-Received")
    public WebElement demandShipmentStatesReceived;

    @FindBy(id = "demand-outboundShipment-Filter-List-Card-Closed")
    public WebElement demandShipmentStatesClosed;


    // @FindBy(xpath = "//button[contains(@id,'tab-states')]")
    @FindBy(xpath= "//span[text()='States']")
    public WebElement clickStatesTab;


    public void clearFilter() {
        objWaitHelper.waitAndClickElement(clearFilterSearchResult, "Clear All filter");
    }

    public void clearFilterIfVisible() {
        if (objGenericHelper.checkVisbilityOfElement(clearFilterSearchResult, "Clear All filter") == true) {
            objWaitHelper.waitAndClickElement(clearFilterSearchResult, "Clear All filter");
        }
    }

    public void clickExceptionsTab()  //Methods
    {
        objWaitHelper.waitAndClickElement(selectExceptionsTab, "Exceptions Tab");
    }


    public void clickStatesTab()  //Methods
    {
        objWaitHelper.waitAndClickElement(clickStatesTab, "States tab");
    }


    public void clickSupplyShipmentExceptionsLate() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(supplyShipmentExceptionsLate));
        supplyShipmentExceptionsLate.click();
    }


    public void clickSupplyShipmentExceptionsPTA() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(supplyShipmentExceptionsPTA));
        supplyShipmentExceptionsPTA.click();
    }


    public void goToSupplyShipmentsTab() throws InterruptedException //Methods
    {
        Thread.sleep(1000);
        objWaitHelper.waitAndClickElement(supplyShipmentLink, "Supply shipment link");
    }


    public void clickSupplyShipmentExceptionsDelayed() {
        String opacity = supplyShipmentExceptionsDelayed.getCssValue("opacity");
        System.out.println(opacity);

        if (opacity.equalsIgnoreCase("0.5")) {
            objWaitHelper.waitElementToBeClickable(supplyShipmentExceptionsDelayed);
            objGenericHelper.elementClick(supplyShipmentExceptionsDelayed, "Delayed filter");
        } else {
            Object pass = this.pass;
        }

    }


    public void clickSupplyShipmentExceptionsOnTime() {
        String opacity = supplyShipmentExceptionsOnTime.getCssValue("opacity");
        System.out.println(opacity);
        if (opacity.equalsIgnoreCase("0.5")) {
            objGenericHelper.checkVisbilityOfElement(supplyShipmentExceptionsOnTime, "supplyShipmentExceptionsOnTime is displayed");
            objWaitHelper.waitAndClickElement(supplyShipmentExceptionsOnTime, "SupplyShipmentExceptionsOnTime clicked");
        }
    }


    public void clickSupplyShipmentExceptionsEarly() {
        //  webDriverWait.until(ExpectedConditions.elementToBeClickable(supplyShipmentExceptionsEarly));
        objWaitHelper.waitElementToBeClickable(supplyShipmentExceptionsEarly);
        objGenericHelper.elementClick(supplyShipmentExceptionsEarly, "Supply shipment Exceptions Early");
        supplyShipmentExceptionsEarly.click();
    }


    public void clickSupplyShipmentStatesNew() {
        //webDriverWait.until(ExpectedConditions.elementToBeClickable(supplyShipmentStatesNew));
        objWaitHelper.waitElementToBeClickable(supplyShipmentStatesNew);
        objGenericHelper.elementClick(supplyShipmentStatesNew, "Supply shipment States New");
        supplyShipmentStatesNew.click();
    }


    public void clickSupplyShipmentStatesPlannedShipment() {
        String opacity = supplyShipmentStatesPlannedShipment.getCssValue("opacity");
        System.out.println(opacity);
        if (opacity.equalsIgnoreCase("0.5")) {
            objWaitHelper.waitElementToBeClickable(supplyShipmentStatesPlannedShipment);
            objGenericHelper.elementClick(supplyShipmentStatesPlannedShipment, "Supply shipment States Planned Shipment");
        }
    }


    public void clickSupplyShipmentStatesShipped() {
        //webDriverWait.until(ExpectedConditions.elementToBeClickable(shippedIsEnabled));
        objWaitHelper.waitElementToBeClickable(supplyShipmentStatesShipped);
        objGenericHelper.elementClick(supplyShipmentStatesShipped, "Supply shipment States Shipped");
        supplyShipmentStatesShipped.click();
    }


    public void clickSupplyShipmentStatesInTransit() {
        //webDriverWait.until(ExpectedConditions.elementToBeClickable(supplyShipmentStatesInTransit));
        objWaitHelper.waitElementToBeClickable(supplyShipmentStatesInTransit);
        objGenericHelper.elementClick(supplyShipmentStatesInTransit, "Supply shipment States In-Transit");
        supplyShipmentStatesInTransit.click();
    }


    public void clickSupplyShipmentStatesReceived() {
        //  webDriverWait.until(ExpectedConditions.elementToBeClickable(supplyShipmentStatesReceived));
        objWaitHelper.waitElementToBeClickable(supplyShipmentStatesReceived);
        objGenericHelper.elementClick(supplyShipmentStatesReceived, "Supply shipment States Received");
        supplyShipmentStatesReceived.click();
    }

    public void clicksupplyShipmentStatesClosed() {
        objWaitHelper.waitElementToBeClickable(supplyShipmentStatesClosed);
        objGenericHelper.elementClick(supplyShipmentStatesClosed, "Supply shipment States Closed");
    }

    public void clickSupplyShipmentStatesDelivered() {
        // webDriverWait.until(ExpectedConditions.elementToBeClickable(supplyShipmentStatesDelivered));
        objWaitHelper.waitElementToBeClickable(supplyShipmentStatesDelivered);
        objGenericHelper.elementClick(supplyShipmentStatesDelivered, "Supply shipment States Delivered");
    }

    public boolean isSupplyShipmentPlannedShipmentVisible() {
        // webDriverWait.until(ExpectedConditions.elementToBeClickable(supplyShipmentStatesPlannedShipment));
        objWaitHelper.waitElementToBeClickable(supplyShipmentStatesPlannedShipment);
        return objGenericHelper.isDisplayed(supplyShipmentStatesPlannedShipment, "Supply shipment States Planned Shipment");

    }


    public boolean isSupplyShipmentShippedVisible() {
        // webDriverWait.until(ExpectedConditions.elementToBeClickable(supplyShipmentStatesShipped));
        objWaitHelper.waitElementToBeClickable(supplyShipmentStatesShipped);
        return objGenericHelper.isDisplayed(supplyShipmentStatesShipped, "Supply shipment States Shipped");

    }

    public boolean isSupplyShipmentInTransitVisible() {
        //  webDriverWait.until(ExpectedConditions.elementToBeClickable(supplyShipmentStatesInTransit));
        objWaitHelper.waitElementToBeClickable(supplyShipmentStatesInTransit);
        return objGenericHelper.isDisplayed(supplyShipmentStatesInTransit, "Supply shipment States In-Transit");

    }


    public void isSupplyShipmentOnTimeVisible() {
        if (!supplyShipmentExceptionsOnTime.isDisplayed()) {
            nextIcon.click();
        } else {
        }
        objGenericHelper.isDisplayed(supplyShipmentExceptionsOnTime, "Supply shipment States OnTime");
    }

    public boolean isSupplyShipmentDelayedVisible() {
        // webDriverWait.until(ExpectedConditions.elementToBeClickable(supplyShipmentExceptionsDelayed));
        objWaitHelper.waitElementToBeClickable(supplyShipmentExceptionsDelayed);
        objGenericHelper.elementClick(supplyShipmentExceptionsDelayed, "Supply shipment Exceptions Delayed");
        return supplyShipmentExceptionsDelayed.isDisplayed();
    }

    public boolean isSupplyShipmentEarlyVisible() {
        objWaitHelper.waitElementToBeClickable(supplyShipmentExceptionsEarly);
        objGenericHelper.elementClick(supplyShipmentExceptionsEarly, "Supply shipment Exceptions Early");
        return supplyShipmentExceptionsEarly.isDisplayed();
    }

    public boolean isSupplyShipmentLateVisible() {
        //webDriverWait.until(ExpectedConditions.elementToBeClickable(supplyShipmentExceptionsLate));
        objWaitHelper.waitElementToBeClickable(supplyShipmentExceptionsLate);
        objGenericHelper.elementClick(supplyShipmentExceptionsLate, "Supply shipment Exceptions Late");
        return supplyShipmentExceptionsLate.isDisplayed();
    }

    public boolean isSupplyShipmentDeliveredVisible() {
        objWaitHelper.waitElementToBeClickable(supplyShipmentStatesDelivered);
        objGenericHelper.elementClick(supplyShipmentStatesDelivered, "Supply shipment States Delivered");
        return supplyShipmentStatesDelivered.isDisplayed();
    }


    public boolean isSupplyShipmentReceivedVisible() {
        //webDriverWait.until(ExpectedConditions.elementToBeClickable(supplyShipmentStatesReceived));
        objWaitHelper.waitElementToBeClickable(supplyShipmentStatesReceived);
        objGenericHelper.elementClick(supplyShipmentStatesReceived, "Supply shipment States Received");
        return supplyShipmentStatesReceived.isDisplayed();

    }

    public void goToSupplyDeliveriesTab() {
        objWaitHelper.waitElementToBeClickable(supplyDeliveryLink);
        objWaitHelper.waitAndClickElement(supplyDeliveryLink, "Supply Delivery link");
    }

    public void clickSupplyDeliveryExceptionsLate() {
        objWaitHelper.waitElementToBeClickable(supplyDeliveryExceptionsLate);
        objWaitHelper.waitAndClickElement(supplyDeliveryExceptionsLate, "Delivery Exceptions Late");
    }


    public void clickSupplyDeliveryExceptionsOnTime() throws InterruptedException {
        objWaitHelper.waitElementToBeClickable(supplyDeliveryExceptionsOnTime);
        objWaitHelper.waitAndClickElement(supplyDeliveryExceptionsOnTime, "Delivery Exceptions OnTime");
    }


    public void clickSupplyDeliveryExceptionsEarly() {
        // webDriverWait.until(ExpectedConditions.elementToBeClickable(supplyDeliveryExceptionsEarly));
        objWaitHelper.waitElementToBeClickable(supplyDeliveryExceptionsEarly);
        objWaitHelper.waitAndClickElement(supplyDeliveryExceptionsEarly, "Delivery Exceptions Early");
    }


    public void clickSupplyDeliveryExceptionsDelayed() {
        objWaitHelper.waitElementToBeClickable(supplyDeliveryExceptionsDelayed);
        objWaitHelper.waitAndClickElement(supplyDeliveryExceptionsDelayed, "Delivery Exceptions Delayed");
        supplyDeliveryExceptionsDelayed.click();
    }


    public boolean isSupplyDeliveryLateVisible() {
        // webDriverWait.until(ExpectedConditions.elementToBeClickable(supplyDeliveryExceptionsLate));
        objWaitHelper.waitElementToBeClickable(supplyDeliveryExceptionsLate);
        objWaitHelper.waitAndClickElement(supplyDeliveryExceptionsLate, "Delivery Exceptions Late");
        return supplyDeliveryExceptionsLate.isDisplayed();
    }


    public boolean isSupplyDeliveryDelayedVisible() {
        objWaitHelper.waitElementToBeClickable(supplyDeliveryExceptionsDelayed);
        objWaitHelper.waitAndClickElement(supplyDeliveryExceptionsDelayed, "Delivery Exceptions Delayed");
        return supplyDeliveryExceptionsDelayed.isDisplayed();
    }

    public boolean isSupplyDeliveryExceptionsOnTimeVisible() {
        objWaitHelper.waitElementToBeClickable(supplyDeliveryExceptionsOnTime);
        objWaitHelper.waitAndClickElement(supplyDeliveryExceptionsOnTime, "Delivery Exceptions On-Time");
        return supplyDeliveryExceptionsOnTime.isDisplayed();
    }


    public boolean isSupplyDeliveryEarlyVisible() {
        objWaitHelper.waitElementToBeClickable(supplyDeliveryExceptionsEarly);
        objWaitHelper.waitAndClickElement(supplyDeliveryExceptionsEarly, "Delivery Exceptions Early");
        return supplyDeliveryExceptionsEarly.isDisplayed();
    }

// Supply Delivery States come

    public void clickSupplyDeliveryStatesDelivered() {
        // webDriverWait.until(ExpectedConditions.elementToBeClickable(deliveredIsEnabled));
        objWaitHelper.waitElementToBeClickable(supplyDeliveryStatesDelivered);
        objWaitHelper.waitAndClickElement(supplyDeliveryStatesDelivered, "Delivery States Delivered");
        supplyDeliveryStatesDelivered.click();
    }

    public void clickSupplyDeliveryStatesReceived() {
        objWaitHelper.waitElementToBeClickable(supplyDeliveryStatesReceived);
        objWaitHelper.waitAndClickElement(supplyDeliveryStatesReceived, "Delivery States Received");
        supplyDeliveryStatesReceived.click();
    }

    public void clickSupplyDeliveryStatesPlannedShipment() {// iot
        objWaitHelper.waitElementToBeClickable(supplyDeliveryStatesPlannedShipment);
        objWaitHelper.waitAndClickElement(supplyDeliveryStatesPlannedShipment, "Delivery States Planned Shipment");
        supplyDeliveryStatesPlannedShipment.click();
    }


    public void clickSupplyDeliveryStatesShipped() {
        objWaitHelper.waitElementToBeClickable(supplyDeliveryStatesShipped);
        objWaitHelper.waitAndClickElement(supplyDeliveryStatesShipped, "Delivery States Shipped");
        supplyDeliveryStatesShipped.click();
    }


    public boolean isSupplyDeliveryDeliveredVisible() {
        objWaitHelper.waitElementToBeClickable(supplyDeliveryStatesDelivered);
        objWaitHelper.waitAndClickElement(supplyDeliveryStatesDelivered, "Delivery States Delivered");
        return supplyDeliveryStatesDelivered.isDisplayed();
    }

    public boolean isSupplyDeliveryReceivedVisible() {
        objWaitHelper.waitElementToBeClickable(supplyDeliveryStatesReceived);
        objWaitHelper.waitAndClickElement(supplyDeliveryStatesReceived, "Delivery States Received");
        return supplyDeliveryStatesReceived.isDisplayed();
    }

    public boolean isSupplyDeliveryShippedVisible() {
        objWaitHelper.waitElementToBeClickable(supplyDeliveryStatesShipped);
        objWaitHelper.waitAndClickElement(supplyDeliveryStatesShipped, "Delivery States Shipped");
        return supplyDeliveryStatesShipped.isDisplayed();
    }


    public void goToSupplyOrdersTab() {
        objWaitHelper.waitAndClickElement(supplyOrdersLink, "Supply Orders link");
    }


    public void clickSupplyOrdersExceptionPastDue() { //
        objWaitHelper.waitElementToBeClickable(supplyOrdersExceptionsPastDue);
        objWaitHelper.waitAndClickElement(supplyOrdersExceptionsPastDue, "Supply Orders Exceptions Past Due");
        supplyOrdersExceptionsPastDue.click();
    }

    public void clickSupplyOrdersExceptionsLate() { //
        objWaitHelper.waitElementToBeClickable(supplyOrdersExceptionsLate);
        objWaitHelper.waitAndClickElement(supplyOrdersExceptionsLate, "Supply Orders Exceptions Late");
        supplyOrdersExceptionsLate.click();
    }

    public void clickSupplyOrdersExceptionsOnTime() {
        objWaitHelper.waitElementToBeClickable(supplyOrdersExceptionOnTime);
        objWaitHelper.waitAndClickElement(supplyOrdersExceptionOnTime, "Supply Orders Exceptions On Time");
        supplyOrdersExceptionOnTime.click();
    }

    public void clickSupplyOrdersExceptionsUnconfirmed() {
        objWaitHelper.waitElementToBeClickable(supplyOrdersExceptionUnconfirmed);
        objWaitHelper.waitAndClickElement(supplyOrdersExceptionUnconfirmed, "Supply Orders Exceptions Unconfirmed");
        supplyOrdersExceptionUnconfirmed.click();
    }


    public boolean isSupplyOrderPastDuesVisible() {
        objWaitHelper.waitElementToBeClickable(supplyOrdersExceptionsPastDue);
        objWaitHelper.waitAndClickElement(supplyOrdersExceptionsPastDue, "Supply Orders Exceptions Past Due");
        return supplyOrdersExceptionsPastDue.isDisplayed();
    }

    public boolean isSupplyOrdersOnTimeVisible() {
        objWaitHelper.waitElementToBeClickable(supplyOrdersExceptionOnTime);
        objWaitHelper.waitAndClickElement(supplyOrdersExceptionOnTime, "Supply Orders Exceptions On-Time");
        return supplyOrdersExceptionOnTime.isDisplayed();
    }


    public boolean isSupplyOrdersUnconfirmedVisible() {
        objWaitHelper.waitElementToBeClickable(supplyOrdersExceptionUnconfirmed);
        objWaitHelper.waitAndClickElement(supplyOrdersExceptionUnconfirmed, "Supply Orders Exceptions Unconfirmed");
        return supplyOrdersExceptionUnconfirmed.isDisplayed();
    }

    public boolean isSupplyOrderLateVisible() {
        objWaitHelper.waitElementToBeClickable(supplyOrdersExceptionsLate);
        objWaitHelper.waitAndClickElement(supplyOrdersExceptionsLate, "Supply Orders Exceptions Late");
        return supplyOrdersExceptionsLate.isDisplayed();
    }


    public boolean isSupplyOrdersExceptionPastDueVisible() { //
        objWaitHelper.waitElementToBeClickable(supplyOrdersExceptionsPastDue);
        objWaitHelper.waitAndClickElement(supplyOrdersExceptionsPastDue, "Supply Orders Exceptions Past Due");
        return supplyOrdersExceptionsPastDue.isDisplayed();
    }

    public void clickSupplyOrdersStatesShipped() {
        objWaitHelper.waitElementToBeClickable(SupplyOrdersStatesShipped);
        objWaitHelper.waitAndClickElement(SupplyOrdersStatesShipped, "Supply Orders States Shipped");
        SupplyOrdersStatesShipped.click();
    }


    public void clickSupplyOrdersStatesPlannedShipment() {
        objWaitHelper.waitElementToBeClickable(supplyOrdersStatesPlannedShipment);
        objWaitHelper.waitAndClickElement(supplyOrdersStatesPlannedShipment, "Supply Orders StatesPlannedShipment");
        supplyOrdersStatesPlannedShipment.click();
    }

    public void clickSupplyOrdersStatesOpen() {
        objWaitHelper.waitElementToBeClickable(supplyOrdersStatesOpen);
        objWaitHelper.waitAndClickElement(supplyOrdersStatesOpen, "Supply Orders States Open");
        supplyOrdersStatesOpen.click();
    }

    // Spring 8 stopped
    public void clickSupplyOrdersStatesNew() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(supplyOrdersStatesNew));
        supplyOrdersStatesNew.click();
    }

    public void clickSupplyOrdersStatesCancelled() {
        //  webDriverWait.until(ExpectedConditions.elementToBeClickable(supplyOrdersStatesCancelled));
        supplyOrdersStatesCancelled.click();
    }

    public void clickSupplyOrdersStatesConfirmed() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(supplyOrdersStatesConfirmed));
        supplyOrdersStatesConfirmed.click();
    }


    public void clickSupplyOrdersStatesConfirmedWithChanges() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(supplyOrdersStatesConfirmedWithChanges));
        supplyOrdersStatesConfirmedWithChanges.click();
    }


    public void clickSupplyOrdersStatesClosed() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(supplyOrdersStatesClosed));
        supplyOrdersStatesClosed.click();
    }


    public void clickSupplyOrdersStatesReceived() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(supplyOrdersStatesReceived));
        supplyOrdersStatesReceived.click();
    }


    public void clickSupplyOrdersStatesRejected() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(supplyOrdersStatesRejected));
        supplyOrdersStatesRejected.click();
    }


    public boolean isSupplyOrdersStatesCreatedVisible() {

        return supplyOrdersStatesCreated.isDisplayed();
    }


    public boolean isSupplyOrdersStatesShippedVisible() {

        return SupplyOrdersStatesShipped.isDisplayed();
    }


    public boolean isSupplyOrdersStatesPlannedShipmentVisible() {

        return supplyOrdersStatesPlannedShipment.isDisplayed();
    }


    public boolean isSupplyOrdersStatesOpenVisible() {

        return supplyOrdersStatesOpen.isDisplayed();
    }


    public boolean isSupplyOrdersStatesNewVisible() {

        return supplyOrdersStatesNew.isDisplayed();
    }

    public boolean isSupplyOrdersStatesCancelledVisible() {

        return supplyOrdersStatesCancelled.isDisplayed();
    }

    public boolean isSupplyOrdersStatesConfirmedVisible() {

        return supplyOrdersStatesConfirmed.isDisplayed();
    }


    public boolean isSupplyOrdersStatesConfirmedWithChangesVisible() {

        return supplyOrdersStatesConfirmedWithChanges.isDisplayed();
    }


    public boolean isSupplyOrdersStatesClosedVisible() {

        return supplyOrdersStatesClosed.isDisplayed();
    }

    public boolean isSupplyOrdersStatesReceivedVisible() {

        return supplyOrdersStatesReceived.isDisplayed();
    }

    public boolean isSupplyOrdersStatesRejectedVisible() {

        return supplyOrdersStatesRejected.isDisplayed();
    }


    public boolean IsInventoryMonetaryImpactDemandAmountVisible() {

        webDriverWait.until(ExpectedConditions.elementToBeClickable(InventoryDemandAmountAssertionPoint));

        return InventoryDemandAmountAssertionPoint.isDisplayed();
    }


    public boolean IsInventoryMonetaryImpactSalesAmountVisible() {

        webDriverWait.until(ExpectedConditions.elementToBeClickable(InventorySalesAmountAssertionPoint));

        return InventorySalesAmountAssertionPoint.isDisplayed();
    }


    public boolean IsInventoryMonetaryImpactSalesOrderNumberVisible() {

        webDriverWait.until(ExpectedConditions.elementToBeClickable(inventoryMonetaryImpactSalesOrderNumber));

        return inventoryMonetaryImpactSalesOrderNumber.isDisplayed();
    }


    public void clickInventoryLowStock() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(inventoryLowStock));
        inventoryLowStock.click();
    }


    public void clickInventoryOverstock() {
        objWaitHelper.waitElementToBeClickable(inventoryOverstock);

    }

    public void clickInventoryStockOut() {
        String opacity = inventoryStockOut.getCssValue("opacity");
        System.out.println(opacity);

        if (opacity.equalsIgnoreCase("0.5")) {
            objGenericHelper.checkVisbilityOfElement(inventoryStockOut, "not selected");
            objWaitHelper.waitAndClickElement(inventoryStockOut, "script clicked");
        } else {
            Object pass = this.pass;
        }
    }




    public void clickInventoryOnTrack() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(inventoryOnTrack));
        inventoryOnTrack.click();
    }

    public boolean isInventoryOnTrackVisible() {
        // webDriverWait.until(ExpectedConditions.elementToBeClickable(inventoryOnTrack));

        return inventoryOnTrack.isDisplayed();
    }


    public boolean isInventoryLowStockVisible() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(inventoryLowStock));

        return inventoryLowStock.isDisplayed();
    }

    public boolean isInventoryOverstockVisible() {
        return inventoryOverstock.isDisplayed();
    }


    public boolean isInventoryStockedOutVisible() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(inventoryStockOut));

        return inventoryStockOut.isDisplayed();
    }

    public boolean isSupplyInventorySearchResultDisplayed() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(inventoryAssertionPoint));
        return inventoryAssertionPoint.isDisplayed();
    }

//======================Supply ForecastCommit


    //click expand button
    public void clickExpandButton() {

        webDriverWait.until(ExpectedConditions.elementToBeClickable(expandButton));
        expandButton.click();
    }

    public void goToSupplyForecastCommitTab() {
/*        try {
            Thread.sleep(2000);
        }catch (InterruptedException e){
            e.printStackTrace();
        }*/
        objWaitHelper.waitElementToBeClickable(supplyForecastCommitsLink);
        objWaitHelper.waitAndClickElement(supplyForecastCommitsLink, "Forecast Commit link");
    }


    public void clickSupplyForecastCommitsForecastCommitMismatch() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(supplyForecastCommitsForecastCommitMismatch));
        supplyForecastCommitsForecastCommitMismatch.click();
    }


    public void clickSupplyForecastCommitsNewForecast() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(supplyForecastCommitsNewForecast));
        supplyForecastCommitsNewForecast.click();
    }

    public void clickSupplyForecastCommitsForecastRevision() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(supplyForecastCommitsForecastRevisedForecast));
        supplyForecastCommitsForecastRevisedForecast.click(); //yes
    }

    public void clickSupplyForecastCommitsForecastCommitInterlock() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(supplyForecastCommitsForecastCommitInterlock));
        supplyForecastCommitsForecastCommitInterlock.click();
    }


    public void clickSupplyForecastCommitsForecastCommitNewForecast() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(supplyForecastCommitsForecastCommitNewForecast));
        supplyForecastCommitsForecastCommitNewForecast.click();
    }


    public boolean issupplyForecastCommitsNewForecastVisible() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(supplyForecastCommitsNewForecast));

        return supplyForecastCommitsNewForecast.isDisplayed();
    }


    public boolean isSupplyForecastCommitsForecastRevisionVisible() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(supplyForecastCommitsForecastRevisedForecast));

        return supplyForecastCommitsForecastRevisedForecast.isDisplayed();
    }


    public boolean issupplyForecastCommitsForecastCommitMismatchVisible() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(supplyForecastCommitsForecastCommitMismatch));

        return supplyForecastCommitsForecastCommitMismatch.isDisplayed();
    }


    public boolean issupplyForecastCommitsForecastCommitInterlockVisible() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(supplyForecastCommitsForecastCommitInterlock));

        return supplyForecastCommitsForecastCommitInterlock.isDisplayed(); //yes
    }

    public boolean isSupplyForecastCommitsForecastCommitNewForecastVisible() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(supplyForecastCommitsForecastCommitNewForecast));

        return supplyForecastCommitsForecastCommitNewForecast.isDisplayed(); //yes
    }


    public void goToDemandShipmentTab() {
        try {
            Thread.sleep(2000);
            objWaitHelper.waitAndClickElement(demandShipment, "Demand Shipment");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    public boolean isDemandShipmentStatesNewVisible() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(demandShipmentStatesNew));

        return demandShipmentStatesNew.isDisplayed();
    }

    public boolean isDemandShipmentStatesPlannedShipmentVisible() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(demandShipmentStatesPlannedShipment));

        return demandShipmentStatesPlannedShipment.isDisplayed();
    }

    public boolean isDemandShipmentStatesShippedVisible() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(demandShipmentStatesShipped));

        return demandShipmentStatesShipped.isDisplayed();
    }

    public boolean isDemandShipmentStatesInTransitVisible() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(demandShipmentStatesInTransit));

        return demandShipmentStatesInTransit.isDisplayed();
    }

    public boolean isDemandShipmentStatesDeliveredVisible() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(demandShipmentStatesDelivered));

        return demandShipmentStatesDelivered.isDisplayed();
    }


    public boolean isDemandShipmentStatesReceivedVisible() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(demandShipmentStatesReceived));

        return demandShipmentStatesReceived.isDisplayed();
    }


    public boolean isDemandShipmentStatesClosedVisible() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(demandShipmentStatesClosed));

        return demandShipmentStatesClosed.isDisplayed();
    }


    public void clickDemandShipmentExceptionsPTAUnknown() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(demandShipmentExceptionsDelayed));
        demandShipmentExceptionsPTAUnknown.click();
    }

    public boolean isDemandShipmentExceptionsPTAUnknownVisible() {
        // webDriverWait.until(ExpectedConditions.elementToBeClickable(demandShipmentExceptionsDelayed));
        return demandShipmentExceptionsPTAUnknown.isDisplayed();
    }


    public void clickDemandShipmentExceptionsLate() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(demandShipmentExceptionsLate));
        demandShipmentExceptionsLate.click();
    }

    public boolean isDemandShipmentExceptionsLateVisible() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(demandShipmentExceptionsLate));
        return demandShipmentExceptionsLate.isDisplayed();
    }


    public void clickDemandShipmentExceptionsDelayed() {
        // webDriverWait.until(ExpectedConditions.elementToBeClickable(demandShipmentExceptionsDelayed));
        objGenericHelper.elementClick(demandShipmentExceptionsDelayed, "Delayed filter");
    }

    public boolean isDemandShipmentExceptionsDelayedVisible() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(demandShipmentExceptionsDelayed));
        return demandShipmentExceptionsDelayed.isDisplayed();
    }

    //==

    public void clickDemandShipmentExceptionsEarly() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(demandShipmentExceptionsEarly));
        demandShipmentExceptionsEarly.click();
    }

    public boolean isDemandShipmentExceptionsEarlyVisible() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(demandShipmentExceptionsEarly));
        return demandShipmentExceptionsEarly.isDisplayed();
    }


    public void clickDemandShipmentExceptionsOnTime() {
        // webDriverWait.until(ExpectedConditions.elementToBeClickable(demandShipmentExceptionsOnTime));
        objGenericHelper.elementClick(demandShipmentExceptionsOnTime, "On Time filter");
    }

    public boolean isDemandShipmentExceptionsOnTimeVisible() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(demandShipmentExceptionsOnTime));
        return demandShipmentExceptionsOnTime.isDisplayed();
    }


    public void clickDemandShipmentStatesNew() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(demandShipmentStatesNew));

        demandShipmentStatesNew.click();
    }


    public void clickDemandShipmentStatesPlannedShipment() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(demandShipmentStatesPlannedShipment));
        demandShipmentStatesPlannedShipment.click();
    }

    public void clickDemandShipmentStatesShipped() {
        String opacity = demandShipmentStatesShipped.getCssValue("opacity");
        System.out.println(opacity);
        if (opacity.equalsIgnoreCase("0.5")) {
            objGenericHelper.checkVisbilityOfElement(demandShipmentStatesShipped, "Shipped is visible");
            objWaitHelper.waitAndClickElement(demandShipmentStatesShipped, "shipped clciked");
        }
    }

    public void clickDemandShipmentStatesInTransit() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(demandShipmentStatesInTransit));
        demandShipmentStatesInTransit.click();
    }

    public void clickDemandShipmentStatesDelivered() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(demandShipmentStatesDelivered));
        demandShipmentStatesDelivered.click();
    }

    public void clickDemandShipmentStatesReceived() {
        //  webDriverWait.until(ExpectedConditions.elementToBeClickable(demandShipmentExceptionsDelayed));
        demandShipmentStatesReceived.click();
    }

    public void clickDemandShipmentStatesClosed() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(demandShipmentExceptionsDelayed));
        demandShipmentStatesClosed.click();
    }

    public void gotoDemandDeliveryTab() {
        objWaitHelper.waitAndClickElement(demandDelivery, "Delivery");
    }


    public void clickDemandDeliveryExceptionsLate() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(demandDeliveryExceptionsLate));
        demandDeliveryExceptionsLate.click();
    }


    public boolean isDemandDeliveryExceptionsLateVisible() {
        // webDriverWait.until(ExpectedConditions.elementToBeClickable(demandDeliveryExceptionsLate));
        return demandDeliveryExceptionsLate.isDisplayed();
    }

//==

    public void clickDemandDeliveryExceptionsDelayed() {
        // webDriverWait.until(ExpectedConditions.elementToBeClickable(demandDeliveryExceptionsDelayed));
        demandDeliveryExceptionsDelayed.click();
    }


    public boolean isDemandDeliveryExceptionsDelayedVisible() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(demandDeliveryExceptionsDelayed));
        return demandDeliveryExceptionsDelayed.isDisplayed();
    }

    public void clickDemandDeliveryExceptionsEarly() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(demandDeliveryExceptionsEarly));
        demandDeliveryExceptionsEarly.click();
    }

    public boolean isDemandDeliveryExceptionsEarlyVisible() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(demandDeliveryExceptionsEarly));
        return demandDeliveryExceptionsEarly.isDisplayed();
    }


//==

    public void clickDemandDeliveryExceptionsOnTime() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(demandDeliveryExceptionsOnTime));
        demandDeliveryExceptionsOnTime.click();
    }


    public boolean isDemandDeliveryExceptionsOnTimeVisible() {
        // webDriverWait.until(ExpectedConditions.elementToBeClickable(demandDeliveryExceptionsOnTime));
        return demandDeliveryExceptionsOnTime.isDisplayed();
    }

//==

    public void clickDemandDeliveryExceptionsNoExceptions() {
        // webDriverWait.until(ExpectedConditions.elementToBeClickable(demandDeliveryExceptionsNoExceptions));
        demandDeliveryExceptionsNoExceptions.click();
    }

    public boolean isDemandDeliveryExceptionsNoExceptionsVisible() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(demandDeliveryExceptionsNoExceptions));
        return demandDeliveryExceptionsNoExceptions.isDisplayed();
    }


    @FindBy(id = "demand-outboundDelivery-Filter-List-Card-Shipped")
    public WebElement demandDeliveryStatesShipped;


    public boolean isdemandDeliveryStatesShippedVisible() {
        return demandDeliveryStatesShipped.isDisplayed();
    }


    public void clickDemandDeliveryStatesShipped() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(demandDeliveryStatesShipped));
        demandDeliveryStatesShipped.click();
    }

    @FindBy(id = "demand-outboundDelivery-Filter-List-Card-Delivered")
    public WebElement demandDeliveryStatesDelivered;


    public boolean isDemandDeliveryStatesDeliveredVisible() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(demandDeliveryStatesDelivered));

        return demandDeliveryStatesDelivered.isDisplayed();
    }


    public void clickDemandDeliveryStatesDelivered() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(demandDeliveryStatesDelivered));
        demandDeliveryStatesDelivered.click();
    }

    @FindBy(id = "demand-outboundDelivery-Filter-List-Card-Closed")
    public WebElement demandDeliveryStatesClosed;


    public boolean isdemandDeliveryStatesClosedVisible() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(demandDeliveryStatesClosed));

        return demandDeliveryStatesClosed.isDisplayed();
    }


    public void clickDemandDeliveryStatesClosed() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(demandDeliveryStatesClosed));
        demandDeliveryStatesClosed.click();
    }

    @FindBy(id = "demand-outboundDelivery-Filter-List-Card-Received")
    public WebElement demandDeliveryStatesReceived;


    public boolean isdemandDeliveryStatesReceivedVisible() {
        //webDriverWait.until(ExpectedConditions.elementToBeClickable(demandDeliveryStatesReceived));

        return demandDeliveryStatesReceived.isDisplayed();
    }


    public void clickDemandDeliveryStatesReceived() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(demandDeliveryStatesReceived));
        demandDeliveryStatesReceived.click();
    }

    @FindBy(id = "demand-outboundDelivery-Filter-List-Card-Planned Shipment")
    public WebElement demandDeliveryStatesPlannedShipment;


    public boolean isDemandDeliveryStatesPlannedShipmentVisible() {
        return demandDeliveryStatesPlannedShipment.isDisplayed();
    }


    public void clickDemandDeliveryStatesPlannedShipment() {
        // webDriverWait.until(ExpectedConditions.elementToBeClickable(demandDeliveryStatesShipped));
        demandDeliveryStatesPlannedShipment.click();
    }


    ////


    //======================Demand    Orders

    public void goToDemandOrdersTab() {
        objWaitHelper.waitAndClickElement(demandSalesOrders, "Orders Tab");
    }


    public void clickDemandOrdersExceptionsPastDue() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(demandOrdersExceptionsPastDue));
        demandOrdersExceptionsPastDue.click();
    }

    public boolean isDemandOrdersExceptionsPastDueVisible() {
        // webDriverWait.until(ExpectedConditions.elementToBeClickable(demandOrdersExceptionsPastDue));
        return demandOrdersExceptionsPastDue.isDisplayed();
    }


//==

    public void clickDemandOrdersExceptionsLate() {
        // webDriverWait.until(ExpectedConditions.elementToBeClickable(demandOrdersExceptionsLate));
        demandOrdersExceptionsLate.click();
    }

    public boolean isDemandOrdersExceptionsLateVisible() {
        // webDriverWait.until(ExpectedConditions.elementToBeClickable(demandOrdersExceptionsLate));
        return demandOrdersExceptionsLate.isDisplayed();
    }


//==

    public void clickDemandOrdersExceptionsUnconfirmed() {
        // webDriverWait.until(ExpectedConditions.elementToBeClickable(demandOrdersExceptionsUnconfirmed));
        demandOrdersExceptionsUnconfirmed.click();
    }

    public boolean isDemandOrdersExceptionsUnconfirmedVisible() {
        // webDriverWait.until(ExpectedConditions.elementToBeClickable(demandOrdersExceptionsUnconfirmed));
        return demandOrdersExceptionsUnconfirmed.isDisplayed();
    }

//==

    public void clickDemandOrdersExceptionsOnTime() {
        // webDriverWait.until(ExpectedConditions.elementToBeClickable(demandOrdersExceptionsOnTime));
        objGenericHelper.elementClick(demandOrdersExceptionsOnTime, "'On Time' filte");
    }

    public boolean isDemandOrdersExceptionsOnTimeVisible() {
        // webDriverWait.until(ExpectedConditions.elementToBeClickable(demandOrdersExceptionsOnTime));
        return demandOrdersExceptionsOnTime.isDisplayed();
    }


    @FindBy(id = "demand-standardOrder-Filter-List-Card-New")
    public WebElement demandOrdersStatesNew;


    public boolean isdemandOrdersStatesNewVisible() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(demandOrdersStatesNew));

        return demandOrdersStatesNew.isDisplayed();
    }


    public void clickDemandOrdersStatesNew() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(demandOrdersStatesNew));
        demandOrdersStatesNew.click();
    }

    @FindBy(id = "demand-standardOrder-Filter-List-Card-Open")
    public WebElement demandOrdersStatesOpen;


    public boolean isDemandOrdersStatesOpenVisible() {
        return demandOrdersStatesNew.isDisplayed();
    }


    public void clickDemandOrdersStatesOpen() {
        // webDriverWait.until(ExpectedConditions.elementToBeClickable(demandDeliveryStatesShipped));
        demandOrdersStatesOpen.click();
    }


////

    @FindBy(id = "demand-standardOrder-Filter-List-Card-Confirmed with Changes")
    public WebElement demandOrdersStatesConfirmedWithChanges;


    public boolean isDemandOrdersStatesConfirmedWithChangesVisible() {
        return demandOrdersStatesNew.isDisplayed();
    }


    public void clickDemandOrdersStatesConfirmedWithChanges() {
        // webDriverWait.until(ExpectedConditions.elementToBeClickable(demandDeliveryStatesShipped));
        demandOrdersStatesConfirmedWithChanges.click();
    }


////

    @FindBy(id = "demand-standardOrder-Filter-List-Card-Confirmed")
    public WebElement demandOrdersStatesConfirmed;


    public boolean isDemandOrdersStatesConfirmedVisible() {
        return demandOrdersStatesConfirmed.isDisplayed();
    }


    public void clickDemandOrdersStatesConfirmed() {
        // webDriverWait.until(ExpectedConditions.elementToBeClickable(demandDeliveryStatesShipped));
        demandOrdersStatesConfirmed.click();
    }


////

    @FindBy(id = "demand-standardOrder-Filter-List-Card-Planned Shipment")
    public WebElement demandOrdersStatesPlannedShipment;


    public boolean isDemandOrdersStatesPlannedShipmentVisible() {
        return demandOrdersStatesPlannedShipment.isDisplayed();
    }


    public void clickDemandOrdersStatesPlannedShipment() {
        // webDriverWait.until(ExpectedConditions.elementToBeClickable(demandOrdersStatesPlannedShipment));
        demandOrdersStatesPlannedShipment.click();
    }

///

    @FindBy(id = "demand-standardOrder-Filter-List-Card-Shipped")
    public WebElement demandOrdersStatesShipped;


    public boolean isDemandOrdersStatesShippedVisible() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(demandOrdersStatesShipped));

        return demandOrdersStatesShipped.isDisplayed();
    }


    public void clickDemandOrdersStatesShipped() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //webDriverWait.until(ExpectedConditions.elementToBeClickable(demandDeliveryStatesShipped));
        demandOrdersStatesShipped.click();
    }


///

    @FindBy(id = "demand-standardOrder-Filter-List-Card-Received")
    public WebElement demandOrdersStatesReceived;


    public boolean isDemandOrdersStatesReceivedVisible() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(demandOrdersStatesReceived));

        return demandOrdersStatesReceived.isDisplayed();
    }


    public void clickDemandOrdersStatesReceived() {
        // webDriverWait.until(ExpectedConditions.elementToBeClickable(demandDeliveryStatesShipped));
        demandOrdersStatesReceived.click();
    }


///

    @FindBy(id = "demand-standardOrder-Filter-List-Card-Rejected")
    public WebElement demandOrdersStatesRejected;


    public boolean isDemandOrdersStatesRejectedVisible() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(demandOrdersStatesRejected));

        return demandOrdersStatesRejected.isDisplayed();
    }


    public void clickDemandOrdersStatesRejected() {
        // webDriverWait.until(ExpectedConditions.elementToBeClickable(demandDeliveryStatesShipped));
        demandOrdersStatesRejected.click();
    }

///

    @FindBy(id = "demand-standardOrder-Filter-List-Card-Cancelled")
    public WebElement demandOrdersStatesCancelled;


    @FindBy(id = "demand-standardOrder-Filter-List-Card-Closed")
    public WebElement demandOrdersStatesClosed;


    public boolean isDemandOrdersStatesClosedVisible() {
        return demandOrdersStatesClosed.isDisplayed();
    }


    public void clickDemandOrdersStatesClosed() {
        // webDriverWait.until(ExpectedConditions.elementToBeClickable(demandDeliveryStatesShipped));
        demandOrdersStatesClosed.click();
    }


    @FindBy(id = "demand-standardOrder-Filter-List-Card-Created")
    public WebElement demandOrdersStatesCreated;


    @FindBy(id = "demand-standardOrder-Filter-List-Card-Delivered")
    public WebElement demandOrdersStatesDelivered;


    @FindBy(id = "PanelSummary-RenderedItem-Order No-LinkItem")
    private WebElement demandOrdersAssertionValue;


    @FindBy(id = "PanelSummary-RenderedItem-Item-LinkItem")
    private WebElement demandInventoryAssertionValue;

    //======================Demand    Inventory


    public void goToInventorySideBarTab() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(inventorySideBar));
        inventorySideBar.click();
    }


    public void clickInventoryStockout() {
        //  webDriverWait.until(ExpectedConditions.elementToBeClickable(InventoryStockout));
        InventoryStockout.click();
    }

    public boolean isInventoryStockoutVisible() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(InventoryStockout));
        return InventoryStockout.isDisplayed();
    }


    public void clickfinishedGoods() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(inventoryFinishedGoods));
        inventoryFinishedGoods.click();
    }

    public void clickinventoryItemNumber() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(inventoryItemNumber));
        inventoryItemNumber.click();
    }

    public void clickInventoryMonetaryImpact() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(inventoryMonetaryImpact));
        inventoryMonetaryImpact.click();
    }


    public void clickInventoryMonetaryImpactDemandTab() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(inventoryMonetaryImpactDemand));
        inventoryMonetaryImpactDemand.click();
    }


    public void clickInventoryMonetaryImpactSalesTab() {
        // webDriverWait.until(ExpectedConditions.elementToBeClickable(inventoryMonetaryImpactSales));
        inventoryMonetaryImpactSales.click();
    }

    public void clickInventoryMonetaryImpactSalesOrderNumber() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(inventoryMonetaryImpactSalesOrderNumber));
        inventoryMonetaryImpactSalesOrderNumber.click();
    }


//-------------master data ,methods----------------

    public void goToMasterDataItemTab() {
        objWaitHelper.waitAndClickElement(itemLink, "Items");
    }


    public void goToMasterDataSitesTab() {
        objWaitHelper.waitAndClickElement(sitesLink, "Sites link");
    }


    public void goToMasterDataNodesTab() {
        objWaitHelper.waitAndClickElement(nodesLink, "Nodes link");
    }

    public void goToMasterDataBatchesTab() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(batchesLink));
        batchesLink.click();
    }

    public void goToMasterDataSourcingLanesTab() {
        objWaitHelper.waitAndClickElement(sourcingLanesLink, "Sourcing lanes link");
    }


    public void goToMasterDataParticipantsTab() {
        objWaitHelper.waitElementToBeClickable(sourcingLanesLink);
        objGenericHelper.elementClick(participantsLink, "Participants link");
    }


    public void assertActualAsterndExpectedDemandShipmentNumberEqualRoad() {  //for all
        webDriverWait.until(ExpectedConditions.elementToBeClickable(demandShipmentAssertionPointRoad));
        try {
            Thread.sleep(8000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        String shipmentNo = demandShipmentAssertionPointRoad.getText();
        Assert.assertEquals(shipmentNo, "IS0035_SD_S1");

        Assert.assertEquals(shipmentNo, "IS0035_SD_S1");
    }


    //DemandStates
    public void assertActualAndExpectedDemandShipmentNumberEqualOcean(String value) {  //JJ
        objWaitHelper.waitElementToBeClickable(demandShipmentAssertionPointOcean);
        String shipmentNo = objGenericHelper.getTextFromElement(demandShipmentAssertionPointOcean, "demandShipmentAssertionPointOcean");
        Assert.assertEquals(shipmentNo, value, "Actual and Expected Ocean Shipment No. matches"); //dallas
    }

    //DemandStatesIS0035_SD_S1
    public void assertActualAndExpectedDemandShipmentNumberEqualOcean1() {  //JJ
        webDriverWait.until(ExpectedConditions.elementToBeClickable(demandShipmentAssertionPointOceans));
        String shipmentNo = demandShipmentAssertionPointOceans.getText();
        Assert.assertEquals(shipmentNo, "IS0036_SD_S2"); //dallas
    }

//
//    public void assertActualAndExpectedDemandDeliveryNumberEqualOcean(String value) {  //for all
//        webDriverWait.until(ExpectedConditions.elementToBeClickable(demandDeliveryAssertionPointOcean));
//        try {
//            Thread.sleep(12000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//        String shipmentNo = demandDeliveryAssertionPointOcean.getText();
//        Assert.assertEquals(shipmentNo, value);
//    }


    public void assertActualAndExpectedDemandDeliveryNumberEqualOcean(String value) {  //for all
        try {
            Thread.sleep(8000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        objWaitHelper.waitElementToBeClickable(demandDeliveryAssertionPointOcean);
        String shipmentNo = objGenericHelper.getTextFromElement(demandDeliveryAssertionPointOcean, "demandDeliveryAssertionPointOcean");
        Assert.assertEquals(shipmentNo, value, "Actual and Expected Ocean Shipment No. matches"); //dallas
    }


    public void assertActualAndExpectedDemandOrdersNumberEqual(String value) {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(demandOdersAssertionPoint));
        try {
            Thread.sleep(9000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String shipmentNo = demandOdersAssertionPoint.getText();
        Assert.assertEquals(shipmentNo, value);
    }

    public void assertActualAndExpectedInventoryNumberEqual(String value) {  //gecko
        webDriverWait.until(ExpectedConditions.elementToBeClickable(InventoryAssertionPoint));

        String shipmentNo = InventoryAssertionPoint.getText();

        Assert.assertEquals(shipmentNo, value);
    }


    //Supply Exceptions

    public void assertActualAndExpectedSupplyShipmentNumberEqual(String value) {  //JJ
        // objWaitHelper.waitElementToBeClickable(supplyShipmentAssertionPointOcean);
        // objGenericHelper.elementClick(supplyShipmentAssertionPointOcean, "Supply shipment Assertion Point Ocean");

        webDriverWait.until(ExpectedConditions.elementToBeClickable(supplyShipmentAssertionPointOcean));

        String shipmentNo = supplyShipmentAssertionPointOcean.getText();
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Assert.assertEquals(shipmentNo, value); //dallas
    }


    public void assertActualAndExpectedSupplyDeliveryNumberEqualOcean(String value) {  //JJ
        webDriverWait.until(ExpectedConditions.elementToBeClickable(supplyDeliveryAssertionPointOcean));
        String shipmentNo = supplyDeliveryAssertionPointOcean.getText();
        Assert.assertEquals(shipmentNo, value); //dallas
    }

    public void assertActualAndExpectedSupplyOdersNumberEqualOcean(String value) {  //JJ
        webDriverWait.until(ExpectedConditions.elementToBeClickable(supplyOrdersAssertionPointOcean));
        String shipmentNo = supplyOrdersAssertionPointOcean.getText();

        Assert.assertEquals(shipmentNo, value); //dallas
    }


    public void assertActualAndExpectedSupplyForecastCommitsNumberEqualOcean(String value) {  //JJ
        webDriverWait.until(ExpectedConditions.elementToBeClickable(supplyForecastCommitsAssertionPoint));
        String shipmentNo = supplyForecastCommitsAssertionPoint.getText();
        Assert.assertEquals(shipmentNo, value); //dallas
    }
}