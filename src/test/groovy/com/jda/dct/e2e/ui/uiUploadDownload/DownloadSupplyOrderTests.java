package com.jda.dct.e2e.ui.uiUploadDownload;

import com.jda.dct.e2e.ui.utility.ExcelHelper.ExcelReader;
import com.jda.dct.e2e.ui.utility.action.CommonActions;
import com.jda.dct.e2e.ui.utility.common.BaseTest;
import com.jda.dct.e2e.ui.utility.common.LeftMenu;
import com.jda.dct.e2e.ui.utility.helper.AssertHelper.AssertHelper;
import com.jda.dct.e2e.ui.utility.helper.Browser.BrowserHelper;
import com.jda.dct.e2e.ui.utility.helper.Utility.UtilityHelper;
import com.jda.dct.e2e.ui.utility.pageobjects.*;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class DownloadSupplyOrderTests extends BaseTest {

    private LeftMenu leftMenu;
    private TopMenuPage topMenu;
    private TopSubMenuPage topSubMenu;
    private SupplyShipmentListPage supplyShipmentListPage;
    private SupplyShipmentDetailsPage supplyShipmentDetailsPage;
    private SupplyOrderListPage supplyOrderlistPage;
    private AssertHelper assertHelper;
    private BrowserHelper browserHelper;
    private InventoryListPage inventoryListPage;
    private DocumentsPage documentsPage;
    private CommonActions commonActions;
    private UtilityHelper utilityHelper;
    private ExcelReader excelReader;
    private WebDriver driver;


    @BeforeClass
    public void setUp() throws InterruptedException {
        System.out.println("Starting tests for :" + this.getClass().getName());
        //Initialize required objects
        driver = getDriver();
        leftMenu = new LeftMenu(driver);
        topMenu = new TopMenuPage(driver);
        topSubMenu = new TopSubMenuPage(driver);
        supplyShipmentListPage = new SupplyShipmentListPage(driver);
        supplyShipmentDetailsPage = new SupplyShipmentDetailsPage(driver);
        supplyOrderlistPage = new SupplyOrderListPage(driver);
        inventoryListPage = new InventoryListPage(driver);
        documentsPage = new DocumentsPage(driver);
        assertHelper = new AssertHelper(driver);
        browserHelper = new BrowserHelper(driver);
        commonActions = new CommonActions(driver);
        excelReader = new ExcelReader();
        utilityHelper = new UtilityHelper();
        //loginAndNavigateToSupplyLandingPage
        logInToApplication();
    }

    @Test(priority = 1)
    public void validateDownloadOnSupplyOrderListPageForStandardPOOrderType() {
        String fileName = "purchaseOrders.xlsx";
        long countOfRecordsInUI = 0;
        String exactFileName = "";
        leftMenu.clickSupplyMenuOption();
        topMenu.goToSupplyOrdersTab();
        supplyOrderlistPage.clickOrderTypeFilter();
        supplyOrderlistPage.clickStandardPOOrderType();
        commonActions.disableAllExceptionFilter();
        countOfRecordsInUI = topSubMenu.getNoOfRecordsOnUIWithAppliedFilter();
        assertHelper.assertTrue(supplyOrderlistPage.isSupplyOrderListPageDisplayed(), "Supply Order list page is displayed successfully");
        topSubMenu.clickDownloadButton();
        leftMenu.clickDocumentsMenuOption();
        documentsPage.clickAllFilesFilter();
        documentsPage.clickDownloadFilesFilterOption();
        browserHelper.refresh();
        documentsPage.clickOnFirstDownloadLink();
        exactFileName = utilityHelper.verifyAndReturnFileNameIfFileFoundInFileSystem(fileName);
        assertHelper.assertTrue(commonActions.verifyRecordsInDownloadedExcelIsMoreThanOrEqualToTheRecordsInUI(exactFileName,countOfRecordsInUI),
                "Count of records in downloaded excel is more than or equal to UI");
    }

    @Test(priority = 2)
    public void validateDownloadOnSupplyOrderListPageForStockTransferPOOrderType() {
        String fileName = "purchaseOrders.xlsx";
        long countOfRecordsInUI = 0;
        String exactFileName = "";
        leftMenu.clickSupplyMenuOption();
        topMenu.goToSupplyOrdersTab();
        supplyOrderlistPage.clickOrderTypeFilter();
        supplyOrderlistPage.clickStockTransferPOOrderType();
        commonActions.disableAllExceptionFilter();
        countOfRecordsInUI = topSubMenu.getNoOfRecordsOnUIWithAppliedFilter();
        assertHelper.assertTrue(supplyOrderlistPage.isSupplyOrderListPageDisplayed(), "Supply Order list page is displayed successfully");
        topSubMenu.clickDownloadButton();
        leftMenu.clickDocumentsMenuOption();
        documentsPage.clickAllFilesFilter();
        documentsPage.clickDownloadFilesFilterOption();
        browserHelper.refresh();
        documentsPage.clickOnFirstDownloadLink();
        exactFileName = utilityHelper.verifyAndReturnFileNameIfFileFoundInFileSystem(fileName);
        assertHelper.assertTrue(commonActions.verifyRecordsInDownloadedExcelIsMoreThanOrEqualToTheRecordsInUI(exactFileName,countOfRecordsInUI),
                "Count of records in downloaded excel is more than or equal to UI");
    }
}

