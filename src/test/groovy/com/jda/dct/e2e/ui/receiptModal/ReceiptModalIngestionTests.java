package com.jda.dct.e2e.ui.receiptModal;

import com.jda.dct.e2e.ui.utility.DataProvider.TestDataProvider;
import com.jda.dct.e2e.ui.utility.ExcelHelper.ExcelReaderForDataDrivenTest;
import com.jda.dct.e2e.ui.utility.action.SupplyOrderActions;
import com.jda.dct.e2e.ui.utility.common.BaseTest;
import com.jda.dct.e2e.ui.utility.common.LeftMenu;
import com.jda.dct.e2e.ui.utility.helper.AssertHelper.AssertHelper;
import com.jda.dct.e2e.ui.utility.helper.Browser.BrowserHelper;
import com.jda.dct.e2e.ui.utility.pageobjects.*;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

public class ReceiptModalIngestionTests extends BaseTest {

    private LeftMenu leftMenu;
    private TopMenuPage topMenu;
    private TopSubMenuPage topSubMenu;
    private SupplyShipmentListPage supplyShipmentListPage;
    private SupplyShipmentDetailsPage supplyShipmentDetailsPage;
    private SupplyOrderListPage supplyOrderlistPage;
    private SupplyOrderDetailsPage supplyOrderDetailsPage;
    private AssertHelper assertHelper;
    private BrowserHelper browserHelper;
    private InventoryListPage inventoryListPage;
    private WebDriver driver;
    private SupplyOrderActions supplyOrderActions;
    ExcelReaderForDataDrivenTest excelReaderForDataDrivenTest;

    @BeforeClass
    public void setUp() throws InterruptedException {
        System.out.println("Starting tests for :" + this.getClass().getName());
        //Initialize required objects
        driver = getDriver();
        leftMenu = new LeftMenu(driver);
        topMenu = new TopMenuPage(driver);
        topSubMenu = new TopSubMenuPage(driver);
        supplyShipmentListPage = new SupplyShipmentListPage(driver);
        supplyShipmentDetailsPage = new SupplyShipmentDetailsPage(driver);
        supplyOrderlistPage = new SupplyOrderListPage(driver);
        inventoryListPage = new InventoryListPage(driver);
        supplyOrderDetailsPage = new SupplyOrderDetailsPage(driver);
        supplyOrderActions = new SupplyOrderActions(driver);
        assertHelper = new AssertHelper(driver);
        browserHelper = new BrowserHelper(driver);
        excelReaderForDataDrivenTest = new ExcelReaderForDataDrivenTest();
        HashMap filedetails = new HashMap<String, String>();
        filedetails.put("FileName", "RecieptModalIngestion/supply-receipts-standardReceipt.xlsx");
        filedetails.put("SheetName", "supply-receipts-standardReceipt");
        excelReaderForDataDrivenTest.setFilenameAndSheetName(filedetails);
        //loginAndNavigateToSupplyLandingPage
        logInToApplication();
    }


    @BeforeMethod
    public void commonMethods() {
        leftMenu.clickSupplyMenuOption();
        topMenu.goToSupplyOrdersTab();
    }

    @Test(dataProvider = "TestRuns", dataProviderClass = TestDataProvider.class, priority = 1)
    public void whenTotalGRQtyAssociatedWithPOLineIsClickedReceiptIsDisplayed(Map<String, String> data) throws InterruptedException {
        String orderNumberHavingReceipt = data.get("PO No.");
        topMenu.clickExceptionsTab();
        topSubMenu.clickMaterialGroupDropdown();
        topSubMenu.clickFinishedGoodsMaterialType();
        topSubMenu.goToSearchFeature(orderNumberHavingReceipt);
        supplyOrderlistPage.clickShipmentExceptionHavingData();
        supplyOrderlistPage.clickSearchedOrderNumber(orderNumberHavingReceipt);
        assertHelper.assertTrue(supplyOrderlistPage.isSupplyPurchaseOrderDetailsPageDisplayed(), "Supply Purchase Order is displayed");
        supplyOrderDetailsPage.clickTotalGRQty();
        supplyOrderDetailsPage.getReceiptNo();
        supplyOrderDetailsPage.getReceiptState();
        supplyOrderDetailsPage.getReceiptDate();
        supplyOrderDetailsPage.getQtyReceived();
        browserHelper.refresh();
        supplyOrderlistPage.clickBackToSupplyOrdersTab();
        topMenu.clearFilter();
    }


    @Test(dataProvider = "TestRuns", dataProviderClass = TestDataProvider.class, priority = 2)
    public void verifyTotalGRQtyIsSumOfReceivedQtyOfAssociatedReceipts(Map<String, String> data) throws InterruptedException {
        String orderNumberHavingReceipt = data.get("PO No.");
        topMenu.clickExceptionsTab();
        topSubMenu.clickMaterialGroupDropdown();
        topSubMenu.clickFinishedGoodsMaterialType();
        topSubMenu.goToSearchFeature(orderNumberHavingReceipt);
        supplyOrderlistPage.clickShipmentExceptionHavingData();
        supplyOrderlistPage.clickSearchedOrderNumber(orderNumberHavingReceipt);
        assertHelper.assertTrue(supplyOrderlistPage.isSupplyPurchaseOrderDetailsPageDisplayed(), "Supply Purchase Order is displayed");
        supplyOrderDetailsPage.clickTotalGRQty();
        assertHelper.assertTrue(supplyOrderActions.verifyTotalGrQtyIsAdditionOfQtyReceivedOfAssociatedReceivedReceipt(),
                "Total GR Qty displayed is addition of Qty received of associated receipt");
        browserHelper.refresh();
        supplyOrderlistPage.clickBackToSupplyOrdersTab();
        topMenu.clearFilter();
    }
}

