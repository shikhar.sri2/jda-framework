//package com.jda.dct.e2e.ui.demand;
//
//import BaseTest;
//import LeftMenu;
//
//import com.jda.dct.e2e.utility.ui.pageobjects.*;
//import org.openqa.selenium.WebDriver;
//import org.testng.Assert;
//import org.testng.annotations.BeforeClass;
//import org.testng.annotations.Test;
//
//public class DemandAutoSuggestionTests extends BaseTest {
//
//    private LeftMenu leftMenu;
//    private TopMenuPage topMenu;
//    private TopSubMenuPage topSubMenu;
//    private DemandShipmentListPage demandShipmentListPage;
//    private DemandOrderListPage demandOrderListPage;
//    private DemandInventoryListPage demandInventoryListPage;
//    private DemandDeliveryListPage demandDeliveryListPage;
//    private WebDriver driver;
//
//    @BeforeClass
//    public void setUp() throws InterruptedException {
//        System.out.println("Starting tests for :" + this.getClass().getName());
////Initialize required objects
//        driver = getDriver();
//        leftMenu = new LeftMenu(driver);
//        topMenu = new TopMenuPage(driver);
//        topSubMenu = new TopSubMenuPage(driver);
//        demandOrderListPage = new DemandOrderListPage(driver);
//        demandInventoryListPage = new DemandInventoryListPage(driver);
//        demandDeliveryListPage = new DemandDeliveryListPage(driver);
//        //loginAndNavigateToDemandShipmentLandingPage
//        logIntoApplication();
//        leftMenu.clickDemandMenuOption();
//
//
//    }
//
//    @Test(priority = 1)
//    public void validateAutoSuggestionInDemandShipmentlistPage() throws InterruptedException {
//        topMenu.goToDemandShipmentLink();
//        topSubMenu.clickSearchButton();
//        //enter letters in search box
//        topSubMenu.getAutoSuggestionForAnySearchDataEntered("is0");
//        Thread.sleep(3000);
//        Assert.assertTrue(topSubMenu.isAutoSuggestionListDisplayed());
//
//    }
//
//
//    @Test(priority = 2)
//    public void validateAutoSuggestionInDemandDeliveryListPage() throws InterruptedException {
//        topMenu.gotoDemandDeliveryLink();
//        topSubMenu.clickSearchButton();
//        //enter letters in search box
//        topSubMenu.getAutoSuggestionForAnySearchDataEntered("is0");
//        Thread.sleep(3000);
//        Assert.assertTrue(topSubMenu.isAutoSuggestionListDisplayed());
//    }
//
//
//    @Test(priority = 3)
//    public void validateAutoSuggestionInDemandOrdersListPage() throws InterruptedException {
//        topMenu.goToDemandInventoryLink();
//        topSubMenu.clickSearchButton();
//        //enter letters in search box
//        topSubMenu.getAutoSuggestionForAnySearchDataEntered("is0");
//        Thread.sleep(3000);
//        Assert.assertTrue(topSubMenu.isAutoSuggestionListDisplayed());
//    }
//
//
//    @Test(priority = 4)
//    public void validateAutoSuggestionInDemandInventoryListPage() throws InterruptedException {
//        topMenu.goToDemandOrdersLink();
//        topSubMenu.clickSearchButton();
//        //enter letters in search box
//        topSubMenu.getAutoSuggestionForAnySearchDataEntered("is0");
//        Thread.sleep(3000);
//        Assert.assertTrue(topSubMenu.isAutoSuggestionListDisplayed());
//    }
//
//
//}