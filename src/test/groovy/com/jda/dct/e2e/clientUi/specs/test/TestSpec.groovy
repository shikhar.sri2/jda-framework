//package com.jda.dct.e2e.clientUi.specs.demand
//
//import com.jda.dct.e2e.clientUi.pages.Insights.InsightPage
//import com.jda.dct.e2e.clientUi.pages.analytics.AnalyticsShipmentsListPage
//import com.jda.dct.e2e.clientUi.pages.demand.DemandInventoryListPage
//import com.jda.dct.e2e.clientUi.pages.login.LoginPage
//import com.jda.dct.e2e.clientUi.pages.demand.DemandDeliveryListPage
//import com.jda.dct.e2e.clientUi.pages.demand.DemandShipmentsListPage
//import com.jda.dct.e2e.clientUi.pages.tempraturecontrolled.SupplyOrderListPage
//import com.jda.dct.e2e.clientUi.pages.tempraturecontrolled.SupplyShipmentDetailsPage
//import com.jda.dct.e2e.clientUi.pages.tempraturecontrolled.SupplyShipmentsListPage
//import com.jda.dct.e2e.clientUi.pages.watchTower.WatchTowerPage
//import geb.exceptionPage.CachingDriverFactory
//import geb.spock.GebSpec
//import spock.lang.Stepwise
//
//
//class TestSpec extends GebSpec{
//
//    def setupSpec() {
//        CachingDriverFactory.clearCache()
//        to LoginPage
//        at(LoginPage).login()
//        println "Successfully Login to Luminate Control Tower. Current page url:"
//        println getExceptionPage().getCurrentUrl()
//    }
//
//    def "From watch tower on the shipment slides Click on Right Slide navigation" () {
//        given: "User is Shipments slide at watch tower page"
//        to WatchTowerPage
//
//        when: "User click right slide navigation button from Shipments slide"
//        at(WatchTowerPage).rightSlideNavigation()
//
//        then: "User is at Locations with Inventory Exceptions slide at the tower page"
//        at WatchTowerPage
//
//        when: "User click left slide navigation button from Locations with Inventory Exceptions slide"
//        at(WatchTowerPage).leftSlideNavigation()
//
//        then: "User is at Locations with Shipment at the tower page"
//        at WatchTowerPage
//    }
//
//
//
//
//
//    def "From demand, User can navigate to shipment list page"(){
//        given: "User is at demand page"
//        to DemandShipmentsListPage
//        println "\nAt demand module, User can navigate to shipment list page"
//        println "User is at demand module"
//        println "Current page url:" + getExceptionPage().getCurrentUrl()
//
//        when: "User click shipments on the top"
//        at(DemandShipmentsListPage).goToShipment()
//        println "User click shipments on the top"
//
//        then: "User is at demand shipment list page"
//        at DemandShipmentsListPage
//        println "User is at demand shipment list page"
//        println "Current page url:" + getExceptionPage().getCurrentUrl()
//
//    }
//
//    def "From demand, User can navigate to inventory list page" (){
//        given: "User is at demand page"
//        to DemandShipmentsListPage
//        println "\nAt demand module, User can navigate to inventory list page"
//        println "User is at demand module"
//        println "Current page url:" + getExceptionPage().getCurrentUrl()
//
//        when: "User click inventory on the top"
//        at(DemandShipmentsListPage).goToInventory()
//        println "User click inventory on the top"
//
//        then: "User is at demand inventory list page"
//        at DemandInventoryListPage
//        println "User is at demand inventory list page"
//        println "Current page url:" + getExceptionPage().getCurrentUrl()
//
//    }
//
//    def "From tempraturecontrolled, User can navigate to shipment list page"() {
//        given: "User is at tempraturecontrolled module"
//        to SupplyOrderListPage
//        println "\nAt tempraturecontrolled module, user can navigate to shipment list page"
//        println "User is at tempraturecontrolled module"
//        println "Current page url:" + getExceptionPage().getCurrentUrl()
//
//    }
//
//    def "Navigate from watch tower to tempraturecontrolled(default: tempraturecontrolled shipments list page)" () {
//        given: "User is at watch tower page"
//        to WatchTowerPage
//
//        when: "User click Analytics button from menu button"
//        at(WatchTowerPage).navigateToAnalytics()
//
//        then: "User is at Analytics Dashboard page by default"
//        at AnalyticsShipmentsListPage
//    }
//
//
//    def "Navigate from watch tower to Insights" () {
//        given: "User is at watch tower  page"
//        to WatchTowerPage
//
//        when: "User click Insights button from menu button"
//        at(WatchTowerPage).navigateToInsight()
//
//        then: "User is at Insight Dashboard by default "
//        at InsightPage
//    }
//
//    def cleanupSpec() {
//        CachingDriverFactory.clearCacheAndQuitDriver()
//    }
//}
