package com.jda.dct.e2e.ui.utility.pageobjects;

import com.jda.dct.e2e.ui.utility.action.CommonActions;
import com.jda.dct.e2e.ui.utility.common.BasePage;
import com.jda.dct.e2e.ui.utility.helper.Wait.WaitHelper;
import com.jda.dct.e2e.ui.utility.helper.genericHelper.GenericHelper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;


public class SupplyDeliveryListPage extends BasePage {
    WebDriver driver;
    WebDriverWait webDriverWait;
    WaitHelper objWaitHelper;
    GenericHelper objGenericHelper;
    CommonActions commonActions;

    @FindBy(id ="PanelSummary-RenderedItem-Delivery-No-1-LinkItem")
    private WebElement deliveryNumberAtSupplyDeliveryListPage;

    @FindBy(xpath = "//span[contains(text(),'Delivery Details')]")
    private WebElement supplyDeliveryDetailsPage;

    @FindBy(xpath = "//*[contains(@id,'supply-inboundDelivery-Filter-List-Card')]//span")
    private List<WebElement> deliveryExceptionStatusCount;

    @FindBy(xpath ="//button[@value='Ocean']/span/div")
    private WebElement shipIcon;

    public SupplyDeliveryListPage(WebDriver webdriver) {

        this.driver = webdriver;
        PageFactory.initElements(driver, this);
        webDriverWait = new WebDriverWait(driver, getTimeOutInSeconds());
        objWaitHelper = new WaitHelper(driver);
        objGenericHelper = new GenericHelper(driver);
        commonActions = new CommonActions(driver);
    }


    //========Methods=====

    public boolean isSupplyDeliveryListPageDisplayed() {
        objWaitHelper.waitElementToBeVisible(deliveryNumberAtSupplyDeliveryListPage);
        return objGenericHelper.isDisplayed(deliveryNumberAtSupplyDeliveryListPage, "Delivery Number At Supply Delivery List Page");
    }

    public boolean isSupplyDeliveryDetailsPageDisplayed() {
        return objGenericHelper.isDisplayed(supplyDeliveryDetailsPage, "Supply Delivery Details Page");
    }

    public void clickSupplyDeliveryExceptionsHavingData() throws InterruptedException{
        Thread.sleep(1000);
        commonActions.clickExceptionStatusHavingData(deliveryExceptionStatusCount);
        Reporter("Exception status having data is clicked successfully", "Pass", log);
    }

    public void clickOceanIcon()  {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        objWaitHelper.waitAndClickElement(shipIcon, "Ship icon");
    }

}
