import org.openqa.selenium.WebDriver
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.chrome.ChromeOptions
import org.openqa.selenium.firefox.FirefoxOptions
import org.openqa.selenium.remote.RemoteWebDriver
import com.aoe.gebspockreports.GebReportingListener

//baseUrl = "http://localhost:3000"
//baseUrl = "https://dctui-intergration.azurewebsites.net"


environments {
    firefox {
        driver = {
            FirefoxOptions firefoxOptions = new FirefoxOptions()
            firefoxOptions.addArguments("--headless")
            firefoxOptions.addArguments("--window-size=1920,1080")
            new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), firefoxOptions)
        }
    }

    chrome {
        driver = {
            ChromeOptions chromeOptions = new ChromeOptions()
            chromeOptions.addArguments("--headless")
            chromeOptions.addArguments("--window-size=1920,1080")
            new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), chromeOptions)
        }
    }
}

def env = System.getenv()
if(env['webURL'].contains("azurewebsites")){
    baseUrl="https://".concat(env['webURL'])
}
else {
    baseUrl = "http://".concat(env['webURL'])
}

reportingListener = new GebReportingListener()
reportsDir = 'build/reports/tests/uiTests'
reportOnTestFailureOnly = false
waiting {timeout = 15}
atCheckWaiting = true

/*System.setProperty("webdriver.chrome.exceptionPage", "src/test/resources/chromedriver.exe")
exceptionPage = { new ChromeDriver() }*/
