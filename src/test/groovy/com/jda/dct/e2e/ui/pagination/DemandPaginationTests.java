package com.jda.dct.e2e.ui.pagination;

import com.jda.dct.e2e.ui.utility.action.CommonActions;
import com.jda.dct.e2e.ui.utility.common.BaseTest;
import com.jda.dct.e2e.ui.utility.common.LeftMenu;
import com.jda.dct.e2e.ui.utility.pageobjects.*;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class DemandPaginationTests extends BaseTest {
    private LeftMenu leftMenu;
    private TopMenuPage topMenu;
    private TopSubMenuPage topSubMenu;
    private DemandShipmentListPage demandShipmentListPage;
    private DemandOrderListPage demandOrderListPage;
    private DemandDeliveryListPage demandDeliveryListPage;
    private CommonActions commonActions;
    private WebDriver driver;

    @BeforeClass
    public void setUp() throws InterruptedException {
        System.out.println("Starting tests for :" + this.getClass().getName());
//Initialize required objects
        driver = getDriver();
        leftMenu = new LeftMenu(driver);
        topMenu = new TopMenuPage(driver);
        topSubMenu = new TopSubMenuPage(driver);
        demandShipmentListPage = new DemandShipmentListPage(driver);
        demandOrderListPage = new DemandOrderListPage(driver);
        demandDeliveryListPage = new DemandDeliveryListPage(driver);
        commonActions = new CommonActions(driver);
        //login Application
       logInToApplication();
    }


    @Test(priority = 1,retryAnalyzer = BaseTest.class)
    public void validateDemandShipmentListPaginationFeature() {
        leftMenu.clickDemandMenuOption();
        topMenu.goToDemandShipmentTab();
        topMenu.clearFilterIfVisible();
        demandShipmentListPage.clickOceanIcon();
        commonActions.disableAllExceptionFilter();
        topSubMenu.validatePagination();
        //Assertion is included in method
    }

    @Test(priority = 2,retryAnalyzer = BaseTest.class)
    public void validateDemandDeliveryListPaginationFeature(){
        leftMenu.clickDemandMenuOption();
        topMenu.gotoDemandDeliveryTab();
        demandDeliveryListPage.clickOceanIcon();
        commonActions.disableAllExceptionFilter();
        topSubMenu.validatePagination();
        //Assertion is included in method
    }


}