package com.jda.dct.e2e.api.utility;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class QueryObject {
    private String type;
    private String processType;
    private String subType;
    private List<SearchQuery> fullTextQuery;
    private List<String> aggregationField;
    private List<String> aggregationFieldValues;
    private List<String> sortFields;
    private String offset;
    private String limit;
    private Map<String, List<String>> query;
    private String shipmentNodeCounts;
    private String shipmentSalesOrderCounts;
    private Map<String, Long> count;
    private double initialDelay;
    private double delay;
    private double timeout;

    public QueryObject() {
        // Instantiate Parameter Classes
        fullTextQuery = new ArrayList<SearchQuery>();
        aggregationField = new ArrayList<String>();
        aggregationFieldValues = new ArrayList<String>();
        sortFields = new ArrayList<String>();
        query = new HashMap<String, List<String>>();

        // Set default values for retry timing
        initialDelay = 1;
        delay = 1;
        timeout = 60;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getProcessType() {
        return processType;
    }

    public void setProcessType(String processType) {
        this.processType = processType;
    }

    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }

    public SearchQuery addFullTextQuery() {
        SearchQuery searchQuery = new SearchQuery();
        this.fullTextQuery.add(searchQuery);
        return searchQuery;
    }

    public List<SearchQuery> getFullTextQuery() {
        return fullTextQuery;
    }

    public List<String> getAggregationField() {
        return aggregationField;
    }

    public void setAggregationField(List<String> aggregationField) {
        this.aggregationField = aggregationField;
    }

    public void addAggregationField(String aggregationField) {
        this.aggregationField.add(aggregationField);
    }

    public List<String> getAggregationFieldValues() {
        return aggregationFieldValues;
    }

    public void setAggregationFieldValues(List<String> aggregationFieldValues) {
        this.aggregationFieldValues = aggregationFieldValues;
    }

    public void addAggregationFieldValues(List<String> aggregationFieldValues) {
        this.aggregationFieldValues.addAll(aggregationFieldValues);
    }

    public void addAggregationFieldValues(String aggregationFieldValue) {
        this.aggregationFieldValues.add(aggregationFieldValue);
    }

    public List<String> getSortFields() {
        return sortFields;
    }

    public void setSortFields(List<String> sortFields) {
        this.sortFields = sortFields;
    }

    public void addSortFields(List<String> sortFields) {
        this.sortFields.addAll(sortFields);
    }

    public void addSortFields(String sortField, String order, String dataType) {
        this.sortFields.add(String.format("%s:%s:%s", sortField, order, dataType));
    }

    public String getOffset() {
        return offset;
    }

    public void setOffset(String offset) {
        this.offset = offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset.toString();
    }

    public String getLimit() {
        return limit;
    }

    public void setLimit(String limit) {
        this.limit = limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit.toString();
    }

    public Map<String, List<String>> getQuery() {
        return query;
    }

    public void setQuery(Map<String, List<String>> query) {
        this.query = query;
    }

    public void addQuery(Map<String, List<String>> query) {
        this.query.putAll(query);
    }

    public void addQuery(String key, List<String> values) {
        query.put(key, values);
    }

    public void addQuery(String key, String value) {
        List<String> values = new ArrayList<String>();
        values.add(value);
        query.put(key, values);
    }

    public String getShipmentNodeCounts() {
        return shipmentNodeCounts;
    }

    public void setShipmentNodeCounts(String shipmentNodeCounts) {
        this.shipmentNodeCounts = shipmentNodeCounts;
    }

    public void setShipmentNodeCounts(Boolean shipmentNodeCounts) {
        this.shipmentNodeCounts = shipmentNodeCounts.toString();
    }

    public String getShipmentSalesOrderCounts() {
        return shipmentSalesOrderCounts;
    }

    public void setShipmentSalesOrderCounts(String shipmentSalesOrderCounts) {
        this.shipmentSalesOrderCounts = shipmentSalesOrderCounts;
    }

    public void setShipmentSalesOrderCounts(Boolean shipmentSalesOrderCounts) {
        this.shipmentSalesOrderCounts = shipmentSalesOrderCounts.toString();
    }

    public Map<String, Long> getCount() {
        return count;
    }

    public void setCount(Map<String, Long> count) {
        this.count = count;
    }

    public double getInitialDelay() {
        return initialDelay;
    }

    public void setInitialDelay(double initialDelay) {
        this.initialDelay = initialDelay;
    }

    public double getDelay() {
        return delay;
    }

    public void setDelay(double delay) {
        this.delay = delay;
    }

    public double getTimeout() {
        return timeout;
    }

    public void setTimeout(double timeout) {
        this.timeout = timeout;
    }

    @JsonIgnore
    public String getURL(String URLPrefix) {
        List<String> parameters = new ArrayList<String>();
        StringBuilder stringBuilder = new StringBuilder();

        String url = URLPrefix + getType() + "?";
        stringBuilder.append(url);

        String fullTextQueryParameter = this.buildFullTextQuery();
        if (!StringUtils.isBlank(fullTextQueryParameter)) {
            parameters.add(String.format("fullTextQuery=%s", fullTextQueryParameter));
        }
        String aggregationFieldParameter = String.join(",", aggregationField);
        if (!StringUtils.isBlank(aggregationFieldParameter)) {
            parameters.add(String.format("aggregationField=%s", aggregationFieldParameter));
        }
        String aggregationFieldValuesParameter = String.join(",", aggregationFieldValues);
        if (!StringUtils.isBlank(aggregationFieldValuesParameter)) {
            parameters.add(String.format("aggregationFieldValues=%s", aggregationFieldValuesParameter));
        }
        String sortFieldsParameter = String.join(",", sortFields);
        if (!StringUtils.isBlank(sortFieldsParameter)) {
            parameters.add(String.format("sortFields=%s", sortFieldsParameter));
        }
        if (!StringUtils.isBlank(offset)) {
            parameters.add(String.format("offset=%s", offset));
        }
        if (!StringUtils.isBlank(limit)) {
            parameters.add(String.format("limit=%s", limit));
        }
        if (query.size() > 0) {
            List<String> queryParameters = new ArrayList<String>();
            for(Map.Entry<String, List<String>> queryParameter: query.entrySet()) {
                String value = String.join(",", queryParameter.getValue());
                queryParameters.add(String.format("%s=%s", queryParameter.getKey(), value));
            }
            parameters.add(String.format("query=%s", String.join(";", queryParameters)));
        }
        if (!StringUtils.isBlank(shipmentNodeCounts)) {
            parameters.add(String.format("shipmentNodeCounts=%s", shipmentNodeCounts));
        }
        if (!StringUtils.isBlank(shipmentSalesOrderCounts)) {
            parameters.add(String.format("shipmentSalesOrderCounts=%s", shipmentSalesOrderCounts));
        }

        stringBuilder.append(String.join("&", parameters));
        return stringBuilder.toString();
    }

    private String buildFullTextQuery() {
        StringBuilder stringBuilder = new StringBuilder();
        if (fullTextQuery.size() > 0) {
            stringBuilder.append(fullTextQuery.get(0).getSearchQuery());
            for (SearchQuery searchQuery : fullTextQuery.subList(1, fullTextQuery.size())) {
                stringBuilder.append(String.format(";%s;%s", searchQuery.getOperator(), searchQuery.getSearchQuery()));
            }
        }
        return stringBuilder.toString();
    }

    private static class SearchQuery {
        private List<String> searchText;
        private List<String> searchField;
        private Boolean isIntersection;

        private SearchQuery() {
            searchText = new ArrayList<String>();
            searchField = new ArrayList<String>();
            isIntersection = true;
        }

        public List<String> getSearchText() {
            return searchText;
        }

        public SearchQuery searchText(List<String> searchText) {
            this.searchText = searchText;
            return this;
        }

        public SearchQuery searchText(String searchText) {
            List<String> newList = new ArrayList<String>();
            newList.add(searchText);
            this.searchText = newList;
            return this;
        }

        public List<String> getSearchField() {
            return searchText;
        }

        public SearchQuery searchField(List<String> searchField) {
            this.searchField = searchField;
            return this;
        }

        public SearchQuery searchField(String searchField) {
            List<String> newList = new ArrayList<String>();
            newList.add(searchField);
            this.searchField = newList;
            return this;
        }

        public Boolean getIsIntersection() {
            return isIntersection;
        }

        public SearchQuery isIntersection(Boolean isIntersection) {
            this.isIntersection = isIntersection;
            return this;
        }

        private String getOperator() {
            String operator;
            if (isIntersection) {
                operator = ":and:";
            } else {
                operator = ":or";
            }
            return operator;
        }

        private String getSearchQuery() {
            return String.format("searchText=%s;searchField=%s",
                    String.join(",", searchText), String.join(",", searchField));
        }
    }

    // How to use QueryObject programmatically:
    public static void main(String[] args) throws Exception {
        QueryObject queryObject = new QueryObject();
        queryObject.setType("shipments");
        queryObject.addFullTextQuery()
                .searchText("foo1")
                .searchField("bar1");
        queryObject.addFullTextQuery()
                .searchText("foo2")
                .isIntersection(true);
        queryObject.addFullTextQuery()
                .searchField("bar3")
                .isIntersection(false);
        queryObject.setLimit(5);
        queryObject.setOffset(5);
        queryObject.addAggregationField("fooagg");
        queryObject.addAggregationFieldValues("baragg");
        queryObject.addQuery("fookey","barvalue");
        queryObject.addQuery("foo2key","bar2value");

        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        System.out.println(mapper.writeValueAsString(queryObject));
    }
}
