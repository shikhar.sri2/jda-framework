package com.jda.dct.e2e.clientUi.pages.SupplyModule

import geb.Page
import org.openqa.selenium.By

class DeliveryListPage extends Page {


    static url = "/tempraturecontrolled/Delivery"

    static atCheckWaiting = true

    static at = {


        $("#tempraturecontrolled-page-tab-deliveries").isDisplayed()

    }


    static content = {


        elementone(wait: true) {

//            $("#app div:nth-child(1) > svg ")
//            $("div")
            //$(   "#app div:nth-child(1)").find("svg")

            $(By.xpath("////div[@id='app']/div/div[2]/div[3]/div/div/div/div/div[2]/div/span"))
        }

        elementtwo(wait: true) {

            //$(   "#app div:nth-child(2)").find("svg")

            $(By.xpath("//div[@id='app']/div/div[2]/div[3]/div/div/div/div/div[2]/div/span"))
        }

        shipmentlisttitle(wait: true) {
            $("#tempraturecontrolled-page-tab-shipments")
        }
        a(wait: true) {
        $("div", role: "button", 'aria-expanded': "false")
    }
        println " You have  successfully arrived from Supply Shipment Container page"
    }


    def validateelements() {

        waitFor {
            elementone.displayed
        }

        elementone.click()

        println "Data displayed"


    }


}























