//package com.jda.dct.e2e.ui.pageobjects;
//
//import BasePage;
//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebElement;
//import org.openqa.selenium.support.FindBy;
//import org.openqa.selenium.support.PageFactory;
//import org.openqa.selenium.support.ui.ExpectedConditions;
//import org.openqa.selenium.support.ui.WebDriverWait;
//
//public class DemandInventoryListPage extends BasePage {
//    WebDriver driver;
//    WebDriverWait webDriverWait;
//
//  @FindBy(id ="PanelSummary-RenderedItem-Item-LinkItem")
//    private WebElement demandInventoryListPage;
//    @FindBy(xpath = "(//a[@id=\"PanelSummary-RenderedItem-Item-LinkItem\"])[1]")
//    private WebElement inventoryItemNumber;
//    @FindBy (xpath = "//span[text()='   Inventory Details   ']")
//
//    private WebElement demandInventoryDetailsPage;
//    @FindBy(xpath = "//a[@href='/demand/inventory']")
//    private WebElement backToDemandInventoryPage;
//    @FindBy(xpath = "(//p[@id =\"PanelSummary-RenderedItem-Notes-NotesItem\"]/a)[1]")
//    private WebElement inventoryNotes;
//
//
//    public DemandInventoryListPage(WebDriver webdriver) {
//
//        this.driver = webdriver;
//        PageFactory.initElements(driver, this);
//        webDriverWait = new WebDriverWait(driver, getTimeOutInSeconds());
//    }
//
//
//    //================ Methods========================
//
//    //Assertion methods
//    public boolean isDemandInventoryListPageDisplayed() {
//        webDriverWait.until(ExpectedConditions.visibilityOf(demandInventoryListPage));
//        return demandInventoryListPage.isDisplayed();
//    }
//
//
//    public void clickInvnetoryItemNumber() {
//        webDriverWait.until(ExpectedConditions.elementToBeClickable(inventoryItemNumber));
//        inventoryItemNumber.click();
//    }
//
//    public boolean isDemandInventoryDetailsPageDisplayed() {
//        webDriverWait.until(ExpectedConditions.visibilityOf(demandInventoryDetailsPage));
//        return demandInventoryDetailsPage.isDisplayed();
//    }
//
//    public void backToDemandInventoryPage() {
//        webDriverWait.until(ExpectedConditions.elementToBeClickable(backToDemandInventoryPage));
//        backToDemandInventoryPage.click();
//    }
//
//
//    public void clickInventoryNotes() {
//        webDriverWait.until(ExpectedConditions.elementToBeClickable(inventoryNotes));
//        inventoryNotes.click();
//    }
//
//
//}
//
//
//
//
//
