package com.jda.dct.e2e.ui.utility.pageobjects;

import com.jda.dct.e2e.ui.utility.common.BasePage;
import com.jda.dct.e2e.ui.utility.helper.Action.ActionHelper;
import com.jda.dct.e2e.ui.utility.helper.Wait.WaitHelper;
import com.jda.dct.e2e.ui.utility.helper.genericHelper.GenericHelper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class DemoWorkFlow extends BasePage {

    WebDriver driver;
    WebDriverWait webDriverWait;

    WaitHelper objWaitHelper;
    GenericHelper objGenericHelper;
    ActionHelper actionHelper;

    public DemoWorkFlow(WebDriver webdriver) {
        this.driver = webdriver;
        PageFactory.initElements(driver, this);
        webDriverWait = new WebDriverWait(driver, 100);
        objWaitHelper = PageFactory.initElements(driver, WaitHelper.class);
        objGenericHelper = PageFactory.initElements(driver, GenericHelper.class);
        actionHelper = PageFactory.initElements(driver, ActionHelper.class);
    }


    // goto Supply Inventory Page
    @FindBy(xpath = "//span[text()='   Inventory   ']")
    private WebElement inventoryListPage1;

    //@FindBy(id ="PanelSummary-RenderedItem-Item-4-LinkItem")
    @FindBy(xpath = "//a[text()='AC-FG-010']")
    private WebElement itemNumber;
    @FindBy(xpath = "//span[text()='Compare']")
    private WebElement compare;
    @FindBy(xpath = "//span[text()='Details']")
    private WebElement details;
    @FindBy(xpath = "(//div[@role='button']/span/following-sibling::*)[2]")
    private WebElement clearFilter;
    @FindBy(xpath = "//button[@value=\"Finished Goods\"]")
    private WebElement finishedGoods;
    @FindBy(xpath = "(//a[@id ='undefined-LinkItem'])[1]")
    private WebElement shipmentAllItems;
    @FindBy(xpath = "(//a[@id='undefined-LinkItem'])[2]")
    private WebElement shipmentHotItems;
    //@FindBy(id = "PanelSummary-RenderedItem-Shipment-No-6-LinkItem")
    @FindBy(xpath = "//a[text()='IS001_SD_S1']")
    private WebElement searchedShipmentNumber;
    @FindBy(xpath = "//div[starts-with(@id,'demandSupply-inventory-Filter-List-Card')]")
    private List<WebElement> inventory;
    @FindBy(id = "PanelSummary-RenderedItem-Item-1-LinkItem")
    //@FindBy(xpath="//a[text()='AC-FG-010']")
    private WebElement items2;

    //@FindBy(id ="PanelSummary-RenderedItem-Item-2-LinkItem")
    @FindBy(xpath = "//a[text()='AC-FG-023']")
    private WebElement items1;

    // @FindBy(id ="PanelSummary-RenderedItem-Item-1-LinkItem")
    @FindBy(xpath = "//a[text()='AC-FG-011']")
    private WebElement items4;
    // @FindBy(id ="PanelSummary-RenderedItem-Item-2-LinkItem")
    @FindBy(xpath = "//a[text()='AC-FG-003']")
    private WebElement items3;

    @FindBy(xpath = "//span[text()='Cancel']")
    private WebElement cancel;

    @FindBy(xpath = "//p[text()='AC-WH05']")
    private WebElement location1;
    @FindBy(xpath = "//p[text()='AC_DC01']")
    private WebElement location2;
    @FindBy(xpath = "//p[text()='AC_DC02']")
    private WebElement location3;
    @FindBy(xpath = "//p[text()='AC_DC04']")
    private WebElement location4;

    @FindBy(xpath = "//a[text()='500']")
    private WebElement planned;
    @FindBy(xpath = "//p[text()='500']")
    private WebElement predicted;
    @FindBy(xpath = "//*[starts-with(@id,'AC')]")
    private WebElement item;
    @FindBy(xpath = "//a[contains(@id,'PanelSummary-RenderedItem-Item')]")
    private List<WebElement> inventoryitem;


    //-------methods for Demo

    public void clickShipmentAllitems() throws InterruptedException {
        objWaitHelper.waitElementToBeClickable(shipmentAllItems);
        Thread.sleep(8000);
        objGenericHelper.elementClick(shipmentAllItems, "shipmentAllItems");
    }


    public void clickShipmentHotitems() throws InterruptedException {
        objWaitHelper.waitElementToBeClickable(shipmentHotItems);
        Thread.sleep(9000);
        objGenericHelper.elementClick(shipmentHotItems, "shipmentHotItems");
        Thread.sleep(8000);
    }


    public void clickSearchedShipmentNumber(String searchedshipmentNumber) {
        objWaitHelper.waitElementToBeClickable(searchedShipmentNumber);
        objGenericHelper.elementClick(searchedShipmentNumber, "searchedShipmentNumber");

    }

    public void clickItemNumber(String shipment) {
        objWaitHelper.waitElementToBeClickable(itemNumber);
        // objGenericHelper.setElementText(itemNumber,"item Number",shipment);
        objGenericHelper.elementClick(itemNumber, "itemNumber");
    }


    public void clickItemone() {
        objWaitHelper.waitElementToBeClickable(items1);

        objGenericHelper.elementClick(items1, "items1");
    }

    public void clickCompareButton() {
        objWaitHelper.waitElementToBeClickable(compare);
        objGenericHelper.elementClick(compare, "compare");
    }

    public void clickDetailsButton() {
        objWaitHelper.waitElementToBeClickable(details);
        objGenericHelper.elementClick(details, "details");
    }

    public void clickCancelButton() {
        objWaitHelper.waitElementToBeClickable(cancel);
        objGenericHelper.elementClick(cancel, "cancel");
    }

    public void clickClearFilter() {
        objWaitHelper.waitElementToBeClickable(clearFilter);
        objGenericHelper.elementClick(clearFilter, "clearFilter");
    }

    public void clickFinishedGoods() {
        objWaitHelper.waitElementToBeClickable(finishedGoods);
        objGenericHelper.elementClick(finishedGoods, "finishedGoods");
    }

    public boolean isItemOneDisplayed() {
        objWaitHelper.waitElementToBeVisible(items1);
        return objGenericHelper.isDisplayed(items1, "items1");
    }

    public boolean isItemTwoDisplayed() {
        objWaitHelper.waitElementToBeVisible(items2);
        return objGenericHelper.isDisplayed(items2, "items2");
    }


    public boolean isItemThreeDisplayed() {
        objWaitHelper.waitElementToBeVisible(items3);
        return objGenericHelper.isDisplayed(items3, "items3");
    }

    public boolean isItemFourDisplayed() {
        objWaitHelper.waitElementToBeVisible(items4);
        return objGenericHelper.isDisplayed(items4, "items4");
    }


    public boolean isLocationOneDisplayed() {
        objWaitHelper.waitElementToBeVisible(location1);
        return objGenericHelper.isDisplayed(location1, "location1");
    }

    public boolean isLocationTwoDisplayed() {
        objWaitHelper.waitElementToBeVisible(location2);
        return objGenericHelper.isDisplayed(location2, "location2");
    }

    public boolean isLocationThreeDisplayed() {
        objWaitHelper.waitElementToBeVisible(location3);
        return objGenericHelper.isDisplayed(location3, "location3");
    }

    public boolean isLocationFourDisplayed() {
        objWaitHelper.waitElementToBeVisible(location4);
        return objGenericHelper.isDisplayed(location4, "location4");
    }

    public boolean isPlannedArrivalDisplayed() {
        objWaitHelper.waitElementToBeVisible(planned);
        return objGenericHelper.isDisplayed(planned, "planned");
    }

    public boolean isPredictedArrivalDisplayed() {
        objWaitHelper.waitElementToBeVisible(predicted);
        return objGenericHelper.isDisplayed(predicted, "predicted");
    }

    public boolean isHotItemsDisplayed() {
        objWaitHelper.waitElementToBeVisible(shipmentHotItems);
        return objGenericHelper.isDisplayed(shipmentHotItems, "shipmentHotItems");
    }

}



