package com.jda.dct.e2e.api.shipments

import com.jda.dct.e2e.api.categories.Smoke
import com.jda.dct.e2e.api.utility.IngestionHelper
import com.jda.dct.e2e.api.utility.QueryHandler
import com.jda.dct.e2e.api.utility.QueryObjects
import com.jda.dct.e2e.api.utility.QueryResultHandler
import org.junit.experimental.categories.Category
import spock.lang.Specification

class SearchForExistingShipmentSpec extends Specification implements IngestionHelper{

    def setupSpec() {
        init()
    }
/*
    @Category(Smoke)
    def "Search a shipment by typing text in search box"() {
        when: "Call Full Text Search"
        def filePath = this.getClass().getClassLoader().getResource('yamls/SearchForExistingShipmentSpec.ctl').path
        QueryObjects queryObjects = new QueryHandler().process(new File(filePath))
        def response = QueryResultHandler.getQueryResultHandler().executeWithRetry(queryObjects.getQueryObjects().get(0), 20, 2, 2, 1, 1)

        then: "A valid shipment is returned if it exists"
        response.rawResponse.status == 200
        response.body.data != null
        and:
        response.body.data.size() > 0
    }
    
 */
}
