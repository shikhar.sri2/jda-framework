package com.jda.dct.e2e.ui.utility.pageobjects;

import com.jda.dct.e2e.ui.utility.common.BasePage;
import com.jda.dct.e2e.ui.utility.helper.Action.ActionHelper;
import com.jda.dct.e2e.ui.utility.helper.Browser.BrowserHelper;
import com.jda.dct.e2e.ui.utility.helper.Javascript.JavaScriptHelper;
import com.jda.dct.e2e.ui.utility.helper.Wait.WaitHelper;
import com.jda.dct.e2e.ui.utility.helper.genericHelper.GenericHelper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AdvancedFilterMenuPage extends BasePage {

    WebDriverWait webDriverWait;
    WebDriver driver;

    WaitHelper objWaitHelper;
    GenericHelper objGenericHelper;
    ActionHelper actionHelper;
    JavaScriptHelper javaScriptHelper;
    BrowserHelper browserHelper;

    //@FindBy()

    public AdvancedFilterMenuPage(WebDriver webdriver) {
        this.driver = webdriver;
        PageFactory.initElements(driver, this);
        webDriverWait = new WebDriverWait(driver, 60);
        objWaitHelper = new WaitHelper(driver);
        objGenericHelper = new GenericHelper(driver);
        actionHelper = new ActionHelper(driver);
        javaScriptHelper = new JavaScriptHelper(driver);
        browserHelper = new BrowserHelper(driver);
    }
}
