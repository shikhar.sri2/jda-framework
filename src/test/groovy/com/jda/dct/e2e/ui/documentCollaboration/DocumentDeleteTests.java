package com.jda.dct.e2e.ui.documentCollaboration;

import com.jda.dct.e2e.ui.utility.action.CommonActions;
import com.jda.dct.e2e.ui.utility.common.BaseTest;
import com.jda.dct.e2e.ui.utility.common.LeftMenu;
import com.jda.dct.e2e.ui.utility.helper.AssertHelper.AssertHelper;
import com.jda.dct.e2e.ui.utility.helper.Browser.BrowserHelper;
import com.jda.dct.e2e.ui.utility.helper.Utility.UtilityHelper;
import com.jda.dct.e2e.ui.utility.pageobjects.*;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.List;

public class DocumentDeleteTests extends BaseTest {
    private WebDriver driver;
    private LeftMenu leftMenu;
    private TopMenuPage topMenu;
    private TopSubMenuPage topSubMenu;
    private SupplyShipmentListPage supplyShipmentListPage;
    private SupplyShipmentDetailsPage supplyShipmentDetailsPage;
    private DemandShipmentListPage demandShipmentListPage;
    private DemandShipmentDetailsPage demandShipmentDetailsPage;
    private BrowserHelper browserHelper;
    private UtilityHelper utilityHelper;
    private AssertHelper assertHelper;
    private CommonActions commonActions;


    @BeforeClass
    public void setUp() throws InterruptedException {
        //Initialize required objects
        driver = getDriver();
        leftMenu = new LeftMenu(driver);
        topMenu = new TopMenuPage(driver);
        topSubMenu = new TopSubMenuPage(driver);
        supplyShipmentListPage = new SupplyShipmentListPage(driver);
        supplyShipmentDetailsPage = new SupplyShipmentDetailsPage(driver);
        demandShipmentListPage = new DemandShipmentListPage(driver);
        demandShipmentDetailsPage = new DemandShipmentDetailsPage(driver);
        browserHelper = new BrowserHelper(driver);
        assertHelper = new AssertHelper(driver);
        commonActions = new CommonActions(driver);
        utilityHelper = new UtilityHelper();
        //loginAndNavigateToSupplyShipmentsLandingPage
        logInToApplication();
    }




    @Test
    public void verifyDocumentDeleteInDemandShipment() throws InterruptedException {
        leftMenu.clickDemandMenuOption();
        topMenu.goToDemandShipmentTab();
        demandShipmentListPage.clickOceanIcon();
        commonActions.disableAllExceptionFilter();
        demandShipmentListPage.clickDemandShipmentNumber();
        demandShipmentDetailsPage.clickNotes();
        demandShipmentDetailsPage.clickAttachmentsTab();
        List<String> listOfAttachments_BeforeDelete = demandShipmentDetailsPage.listOfAttachements();
        System.out.println("listOfAttachments_BeforeDelete: " + listOfAttachments_BeforeDelete.size());
        demandShipmentDetailsPage.clickNotesTab();
        Assert.assertTrue(demandShipmentDetailsPage.isNotesPageDisplayed());
        //demandShipmentDetailsPage.setNotesTextArea();
        demandShipmentDetailsPage.clickAttachmentsLinkInNotes();
        String fileName = "testdata/DocumentCollaboration/csv_file.csv";
        demandShipmentDetailsPage.uploadAttachment(fileName);
        demandShipmentDetailsPage.submitAttachment();
        demandShipmentDetailsPage.downloadAttachment();
        utilityHelper.verifyFileInFileSystem(fileName);
        demandShipmentDetailsPage.deleteAttachment();
        demandShipmentDetailsPage.confirmDeleteAttachment();
        //demandShipmentDetailsPage.deleteAllAttachments();
        demandShipmentDetailsPage.isAttachmentsTabDisplayed();
        Thread.sleep(5000L);
        demandShipmentDetailsPage.clickAttachmentsTab();
        List<String> listOfAttachments = demandShipmentDetailsPage.listOfAttachements();
        System.out.println("listOfAttachments_AfterDelete: " + listOfAttachments.size());
        Assert.assertTrue(listOfAttachments.size() <= listOfAttachments_BeforeDelete.size());
        browserHelper.refresh();
        demandShipmentListPage.clickBackToDemandShipmentPage();
        utilityHelper.deleteFileInFileSystem();
        browserHelper.refresh();


    }


}
