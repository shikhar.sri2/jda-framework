package com.jda.dct.e2e.readyapi.utility


import java.nio.file.Path
import java.nio.file.Paths

class IngestionConfig {
    private callingContext
    private commonContext
    private Path projectRoot
    private Path tempDirectory
    private Path testDataDirectory

    IngestionConfig(def context, String callingTestSuite, String callingTestCase, String testDataDirectory) {
        commonContext = context.getTestCase()
        callingContext = context.testCase.testSuite.project.getTestSuiteByName(callingTestSuite).getTestCaseByName(callingTestCase)
        projectRoot = Paths.get(context.expand('${#Project#projectRoot}'))
        tempDirectory = Paths.get(context.expand('${#Project#tempDirectory}'))
        this.testDataDirectory = Paths.get(testDataDirectory)
    }

    void configureIngestionRequestTestStep(Path attachmentFilePath) {
        def ingestionRequestStep = commonContext.getTestStepByName("Ingest Data")
        // Remove existing request attachments and add new test data file
        def ingestionRequest = ingestionRequestStep.getTestRequest()
        for (def attachment: ingestionRequest.getAttachments()) {
            ingestionRequest.removeAttachment(attachment)
        }
        ingestionRequest.attachFile(attachmentFilePath.toFile(), false)
    }

    def fileIngestionConfig(String dataFileName, String dataSourceStep = null) {
        def testDataAbsolutePath = projectRoot.resolve(testDataDirectory).resolve(dataFileName)
        def tempDataAbsolutePath = projectRoot.resolve(tempDirectory).resolve(dataFileName)
        // Get data from requested test data file and calculate any relative dates found
        List<String> lines = DataFileHandler.processAndMove(testDataAbsolutePath, tempDataAbsolutePath)
        configureIngestionRequestTestStep(projectRoot.relativize(tempDataAbsolutePath))
        if (!dataSourceStep.isEmpty()) {
            def dataSourceConfig = new DataSourceConfig(callingContext)
            List<String> headers = new ArrayList<>()
            if (dataFileName.substring(dataFileName.lastIndexOf(".")).toLowerCase() == ".csv") {
                headers = CsvData.getFields(lines[0])
            }
            dataSourceConfig.configureFileDataSourceStep(dataSourceStep, projectRoot.relativize(tempDataAbsolutePath), headers)
        }
    }
}
