//
//package com.jda.dct.e2e.ui.demand;
//
//import BaseTest;
//import LeftMenu;
//import BrowserHelper;
//import TopMenuPage;
//import TopSubMenuPage;
//import TestDataFile;
//import org.openqa.selenium.WebDriver;
//import org.testng.Assert;
//import org.testng.annotations.BeforeClass;
//import org.testng.annotations.Test;
//
//    public class DemandExceptionsTests extends BaseTest {
//
//        private LeftMenu leftMenu;
//        private TopMenuPage topMenu;
//        private TopSubMenuPage topSubMenu;
//        private WebDriver driver;
//        private BrowserHelper browserHelper;
//
//        @BeforeClass
//        public void setUp() throws InterruptedException {
//
//            System.out.println("Starting tests for :" + this.getClass().getName());
////Initialize required objects
//            driver = getDriver();
//
//            leftMenu = new LeftMenu(driver);
//            topMenu = new TopMenuPage(driver);
//            topSubMenu = new TopSubMenuPage(driver);
//            browserHelper = new BrowserHelper(driver);
//
//            //loginAndNavigateToDemandShipmentLandingPage
//            logInToApplication();
//            leftMenu.clickDemandMenuOption();
//
//
//        }
//
//        @Test(priority = 2, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
//        public void validatingExceptionStatusLateTC2 (String shipmentNo2a,String shipmentNo2b){
//
//            topMenu.goToDemandShipmentTab();
//            topMenu.clickExceptionsTab();
//            topSubMenu.clickOceanIcon();
//            topMenu.clickDemandShipmentExceptionsLate();
//
//            topSubMenu.clickSearchButton();
//            topSubMenu.goToSearchFeature(shipmentNo2a);
//
//            Assert.assertTrue(topMenu.isDemandShipmentExceptionsLateVisible());
//            topMenu.assertActualAndExpectedDemandShipmentNumberEqualOcean(shipmentNo2b);
//           // topMenu.clearFilter();
//            browserHelper.refresh();
//        }
//
//
//        @Test(priority = 3, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
//        public void validatingExceptionStatusDelayedTC3(String shipmentNo3a,String shipmentNo3b)
//        {
//            topMenu.goToDemandShipmentTab();
//            topMenu.clickExceptionsTab();
//            topSubMenu.clickOceanIcon();
//            topMenu.clickDemandShipmentExceptionsDelayed();
//            topSubMenu.clickSearchButton();
//            topSubMenu.goToSearchFeature(shipmentNo3a);
//            Assert.assertTrue(topMenu.isDemandShipmentExceptionsDelayedVisible());
//            topMenu.assertActualAndExpectedDemandShipmentNumberEqualOcean(shipmentNo3b);
//            topMenu.clearFilter();
//            browserHelper.refresh();
//
//        }
//
//        @Test(priority = 4, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
//        public void validatingExceptionStatusEarlyTC4(String shipmentNo4a,String shipmentNo4b)
//        {
//            topMenu.goToDemandShipmentTab();
//            topSubMenu.clickOceanIcon();
//            topMenu.clickExceptionsTab();
//            topMenu.clickDemandShipmentExceptionsEarly();
//            topSubMenu.clickSearchButton();
//            topSubMenu.goToSearchFeature(shipmentNo4a);
//            Assert.assertTrue(topMenu.isDemandShipmentExceptionsEarlyVisible());
//            topMenu.assertActualAndExpectedDemandShipmentNumberEqualOcean(shipmentNo4b);
//            topMenu.clearFilter();
//            browserHelper.refresh();
//        }
//
//        @Test(priority = 5, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
//        public void validatingExceptionStatusOnTimeTC5(String shipmentNo5a,String shipmentNo5b)
//        {
//            topMenu.goToDemandShipmentTab();
//            topSubMenu.clickOceanIcon();
//            topMenu.clickExceptionsTab();
//            topMenu.clickDemandShipmentExceptionsOnTime();
//            topSubMenu.clickSearchButton();
//            topSubMenu.goToSearchFeature(shipmentNo5a);
//            Assert.assertTrue(topMenu.isDemandShipmentExceptionsOnTimeVisible());
//            topMenu.assertActualAndExpectedDemandShipmentNumberEqualOcean(shipmentNo5b);
//            topMenu.clearFilter();
//            driver.navigate().refresh();
//        }
//
//
//        @Test(priority = 6, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
//        public void validatingExceptionStatusDelayedTC6 (String shipmentNo6a,String shipmentNo6b){  //  VSL_0045, VSL_0663, VSL_0939
//            topMenu.goToDemandShipmentTab();
//            topSubMenu.clickOceanIcon();
//            topMenu.clickExceptionsTab();
//            topMenu.clickDemandShipmentExceptionsDelayed();
//            topSubMenu.clickSearchButton();
//            topSubMenu.goToSearchFeature(shipmentNo6a);
//            Assert.assertTrue(topMenu.isDemandShipmentExceptionsDelayedVisible());
//            topMenu.assertActualAndExpectedDemandShipmentNumberEqualOcean(shipmentNo6b);
//            topMenu.clearFilter();
//            driver.navigate().refresh();
//        }
//        @Test(priority = 7, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
//        public void validatingExceptionStatusLateTC7 (String shipmentNo7a,String shipmentNo7b){
//            topMenu.goToDemandShipmentTab();
//            topSubMenu.clickOceanIcon();
//            topMenu.clickExceptionsTab();
//            topMenu.clickDemandShipmentExceptionsLate();
//            topSubMenu.clickSearchButton();
//            topSubMenu.goToSearchFeature(shipmentNo7a);
//            Assert.assertTrue(topMenu.isDemandShipmentExceptionsLateVisible());
//            topMenu.assertActualAndExpectedDemandShipmentNumberEqualOcean(shipmentNo7b);
//            topMenu.clearFilter();
//            driver.navigate().refresh();
//        }
//
//
//        @Test(priority = 8, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
//        public void validatingExceptionStatusDelayedTC8(String shipmentNo8a,String shipmentNo8b)
//        {
//            topMenu.goToDemandShipmentTab();
//            topSubMenu.clickOceanIcon();
//            topMenu.clickExceptionsTab();
//            topMenu.clickDemandShipmentExceptionsDelayed();
//            topSubMenu.clickSearchButton();
//            topSubMenu.goToSearchFeature(shipmentNo8a);
//            Assert.assertTrue(topMenu.isDemandShipmentExceptionsDelayedVisible());
//            topMenu.assertActualAndExpectedDemandShipmentNumberEqualOcean(shipmentNo8b);
//            topMenu.clearFilter();
//            driver.navigate().refresh();
//
//        }
//
//        @Test(priority = 9, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
//        public void validatingExceptionStatusEarlyTC9(String shipmentNo9a,String shipmentNo9b)
//        {
//            topMenu.goToDemandShipmentTab();
//            topSubMenu.clickOceanIcon();
//            topMenu.clickExceptionsTab();
//            topMenu.clickDemandShipmentExceptionsEarly();
//            topSubMenu.clickSearchButton();
//            topSubMenu.goToSearchFeature(shipmentNo9a);
//            Assert.assertTrue(topMenu.isDemandShipmentExceptionsEarlyVisible());
//            topMenu.assertActualAndExpectedDemandShipmentNumberEqualOcean(shipmentNo9b);
//            topMenu.clearFilter();
//            driver.navigate().refresh();
//        }
//
//        @Test(priority = 10, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
//        public void validatingExceptionStatusOnTimeTC10(String shipmentNo10a,String shipmentNo10b)
//        {
//            topMenu.goToDemandShipmentTab();
//            topSubMenu.clickOceanIcon();
//            topMenu.clickExceptionsTab();
//            topMenu.clickDemandShipmentExceptionsOnTime();
//            topSubMenu.clickSearchButton();
//            topSubMenu.goToSearchFeature(shipmentNo10a);
//            Assert.assertTrue(topMenu.isDemandShipmentExceptionsOnTimeVisible());
//            topMenu.assertActualAndExpectedDemandShipmentNumberEqualOcean(shipmentNo10b);
//            topMenu.clearFilter();
//            driver.navigate().refresh();
//        }
//
//
//
//// Delivery exceptions starts          5605034048
//
//        @Test(priority = 11, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
//        public void validatingExceptionStatusLateTC11 (String shipmentNo11a,String shipmentNo11b){
//            topMenu.gotoDemandDeliveryTab();
//            topMenu.clickExceptionsTab();
//            topSubMenu.clickOceanIcon();
//            topMenu.clickDemandDeliveryExceptionsLate();
//            topSubMenu.clickSearchButton();
//            topSubMenu.goToSearchFeature(shipmentNo11a);
//            Assert.assertTrue(topMenu.isDemandDeliveryExceptionsLateVisible());
//           // topMenu.assertActualAndExpectedDemandDeliveryNumberEqualOcean(shipmentNo11b);
//            topMenu.clearFilter();
//            driver.navigate().refresh();
//        }
//
//
//        @Test(priority = 12, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
//        public void validatingExceptionStatusDelayedTC12(String shipmentNo12a,String shipmentNo12b)
//        {
//            topMenu.gotoDemandDeliveryTab();
//            topMenu.clickExceptionsTab();
//            topSubMenu.clickOceanIcon();
//            topMenu.clickDemandDeliveryExceptionsDelayed();
//            topSubMenu.clickSearchButton();
//            topSubMenu.goToSearchFeature(shipmentNo12a);
//            Assert.assertTrue(topMenu.isDemandDeliveryExceptionsDelayedVisible());
//            topMenu.assertActualAndExpectedDemandDeliveryNumberEqualOcean(shipmentNo12b);
//            topMenu.clearFilter();
//            driver.navigate().refresh();
//
//        }
//
//        @Test(priority = 13, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
//        public void validatingExceptionStatusEarlyTC13 (String shipmentNo13a,String shipmentNo13b)
//        {
//            topMenu.gotoDemandDeliveryTab();
//            topMenu.clickExceptionsTab();
//            topSubMenu.clickOceanIcon();
//            topMenu.clickDemandDeliveryExceptionsEarly();
//            topSubMenu.clickSearchButton();
//            topSubMenu.goToSearchFeature(shipmentNo13a);
//            Assert.assertTrue(topMenu.isDemandDeliveryExceptionsEarlyVisible());
//            topMenu.assertActualAndExpectedDemandDeliveryNumberEqualOcean(shipmentNo13b);
//            topMenu.clearFilter();
//            driver.navigate().refresh();
//        }
//
//        @Test(priority = 14, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
//        public void validatingExceptionStatusOnTimeTC14(String shipmentNo14a,String shipmentNo14b)
//        {
//            topMenu.gotoDemandDeliveryTab();
//            topMenu.clickExceptionsTab();
//            topSubMenu.clickOceanIcon();
//            topMenu.clickDemandDeliveryExceptionsOnTime();
//            topSubMenu.clickSearchButton();
//            topSubMenu.goToSearchFeature(shipmentNo14a);
//            Assert.assertTrue(topMenu.isDemandDeliveryExceptionsOnTimeVisible());
//            topMenu.assertActualAndExpectedDemandDeliveryNumberEqualOcean(shipmentNo14b);
//            topMenu.clearFilter();
//            driver.navigate().refresh();
//        }
//
//
//
//        @Test(priority = 15, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
//        public void validatingExceptionStatusNoExceptionsTC15(String shipmentNo15a,String shipmentNo15b)
//        {
//            topMenu.gotoDemandDeliveryTab();
//            topSubMenu.clickOceanIcon();
//            topMenu.clickExceptionsTab();
//            topSubMenu.clickDemandDeliverySliderIcon();
//            topMenu.clickDemandDeliveryExceptionsNoExceptions();
//            topSubMenu.clickSearchButton();
//            topSubMenu.goToSearchFeature(shipmentNo15a);
//            Assert.assertTrue(topMenu.isDemandDeliveryExceptionsNoExceptionsVisible());
//            topMenu.assertActualAndExpectedDemandDeliveryNumberEqualOcean(shipmentNo15b);
//            topMenu.clearFilter();
//            driver.navigate().refresh();
//        }
//
//
//
//        //Order Exceptions
//
//        @Test(priority = 16, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
//        public void validatingExceptionStatusPastDuesTC16(String shipmentNo16a,String shipmentNo16b)
//        {
//            topMenu.goToDemandOrdersTab();
//            topMenu.clickExceptionsTab();
//            topMenu.clickDemandOrdersExceptionsPastDue();
//            topSubMenu.clickSearchButton();
//            topSubMenu.goToSearchFeature(shipmentNo16a);
//            Assert.assertTrue(topMenu.isDemandOrdersExceptionsPastDueVisible());
//            topMenu.assertActualAndExpectedDemandOrdersNumberEqual(shipmentNo16b);
//            topMenu.clearFilter();
//            driver.navigate().refresh();
//
//        }
//
//
//
//        @Test(priority = 17, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
//        public void validatingExceptionStatusLateTC17(String shipmentNo17a,String shipmentNo17b)
//        {
//            topMenu.goToDemandOrdersTab();
//            topMenu.clickExceptionsTab();
//           topMenu.clickDemandOrdersExceptionsLate();
//            topSubMenu.clickSearchButton();
//            topSubMenu.goToSearchFeature(shipmentNo17a);
//            Assert.assertTrue(topMenu.isDemandOrdersExceptionsLateVisible());
//            topMenu.assertActualAndExpectedDemandOrdersNumberEqual(shipmentNo17b);
//            topMenu.clearFilter();
//            driver.navigate().refresh();
//
//        }
//
//        @Test(priority = 18, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
//        public void validatingExceptionStatusUnconfirmedTC18(String shipmentNo18a,String shipmentNo18b)
//        {
//            topMenu.goToDemandOrdersTab();
//            topMenu.clickExceptionsTab();
//            topMenu.clickDemandOrdersExceptionsUnconfirmed();
//            topSubMenu.clickSearchButton();
//            topSubMenu.goToSearchFeature(shipmentNo18a);
//            Assert.assertTrue(topMenu.isDemandOrdersExceptionsUnconfirmedVisible());
//            topMenu.assertActualAndExpectedDemandOrdersNumberEqual(shipmentNo18b);
//            topMenu.clearFilter();
//            driver.navigate().refresh();
//        }
//
//        @Test(priority = 19, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
//        public void validatingExceptionStatusOnTimeTC19(String shipmentNo19a,String shipmentNo19b) throws InterruptedException {
//            topMenu.goToDemandOrdersTab();
//            topMenu.clickExceptionsTab();
//            topMenu.clickDemandOrdersExceptionsOnTime();
//            topSubMenu.clickSearchButton();
//            topSubMenu.goToSearchFeature(shipmentNo19a);
//            Assert.assertTrue(topMenu.isDemandOrdersExceptionsOnTimeVisible());
//            topMenu.assertActualAndExpectedDemandOrdersNumberEqual(shipmentNo19b);
//            topMenu.clearFilter();
//            driver.navigate().refresh();
//            leftMenu.clickInventoryMenuOption();
//
//        }
//
//
////package com.jda.dct.e2e.testscripts.uitests.demand;
////
////import BaseTest;
////import LeftMenu;
////import BrowserHelper;
////import TopMenuPage;
////import TopSubMenuPage;
////import TestDataFile;
////import org.openqa.selenium.WebDriver;
////import org.testng.Assert;
////import org.testng.annotations.BeforeClass;
////import org.testng.annotations.Test;
////
////    public class DemandExceptionsTests extends BaseTest {
////
////        private LeftMenu leftMenu;
////        private TopMenuPage topMenu;
////        private TopSubMenuPage topSubMenu;
////        private WebDriver driver;
////        private BrowserHelper browserHelper;
////
////        @BeforeClass
////        public void setUp() throws InterruptedException {
////
////            System.out.println("Starting tests for :" + this.getClass().getName());
//////Initialize required objects
////            driver = getDriver();
////
////            leftMenu = new LeftMenu(driver);
////            topMenu = new TopMenuPage(driver);
////            topSubMenu = new TopSubMenuPage(driver);
////            browserHelper = new BrowserHelper(driver);
////
////            //loginAndNavigateToDemandShipmentLandingPage
////            logInToApplication();
////            leftMenu.clickDemandMenuOption();
////
////
////        }
////
////        @Test(priority = 2, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////        public void validatingExceptionStatusLateTC2 (String shipmentNo2a,String shipmentNo2b){
////
////            topMenu.goToDemandShipmentTab();
////            topMenu.clickExceptionsTab();
////            topSubMenu.clickOceanIcon();
////            topMenu.clickDemandShipmentExceptionsLate();
////
////            topSubMenu.clickSearchButton();
////            topSubMenu.goToSearchFeature(shipmentNo2a);
////
////            Assert.assertTrue(topMenu.isDemandShipmentExceptionsLateVisible());
////            topMenu.assertActualAndExpectedDemandShipmentNumberEqualOcean(shipmentNo2b);
////           // topMenu.clearFilter();
////            browserHelper.refresh();
////        }
////
////
////        @Test(priority = 3, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////        public void validatingExceptionStatusDelayedTC3(String shipmentNo3a,String shipmentNo3b)
////        {
////            topMenu.goToDemandShipmentTab();
////            topMenu.clickExceptionsTab();
////            topSubMenu.clickOceanIcon();
////            topMenu.clickDemandShipmentExceptionsDelayed();
////            topSubMenu.clickSearchButton();
////            topSubMenu.goToSearchFeature(shipmentNo3a);
////            Assert.assertTrue(topMenu.isDemandShipmentExceptionsDelayedVisible());
////            topMenu.assertActualAndExpectedDemandShipmentNumberEqualOcean(shipmentNo3b);
////            topMenu.clearFilter();
////            browserHelper.refresh();
////
////        }
////
////        @Test(priority = 4, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////        public void validatingExceptionStatusEarlyTC4(String shipmentNo4a,String shipmentNo4b)
////        {
////            topMenu.goToDemandShipmentTab();
////            topSubMenu.clickOceanIcon();
////            topMenu.clickExceptionsTab();
////            topMenu.clickDemandShipmentExceptionsEarly();
////            topSubMenu.clickSearchButton();
////            topSubMenu.goToSearchFeature(shipmentNo4a);
////            Assert.assertTrue(topMenu.isDemandShipmentExceptionsEarlyVisible());
////            topMenu.assertActualAndExpectedDemandShipmentNumberEqualOcean(shipmentNo4b);
////            topMenu.clearFilter();
////            browserHelper.refresh();
////        }
////
////        @Test(priority = 5, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////        public void validatingExceptionStatusOnTimeTC5(String shipmentNo5a,String shipmentNo5b)
////        {
////            topMenu.goToDemandShipmentTab();
////            topSubMenu.clickOceanIcon();
////            topMenu.clickExceptionsTab();
////            topMenu.clickDemandShipmentExceptionsOnTime();
////            topSubMenu.clickSearchButton();
////            topSubMenu.goToSearchFeature(shipmentNo5a);
////            Assert.assertTrue(topMenu.isDemandShipmentExceptionsOnTimeVisible());
////            topMenu.assertActualAndExpectedDemandShipmentNumberEqualOcean(shipmentNo5b);
////            topMenu.clearFilter();
////            driver.navigate().refresh();
////        }
////
////
////        @Test(priority = 6, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////        public void validatingExceptionStatusDelayedTC6 (String shipmentNo6a,String shipmentNo6b){  //  VSL_0045, VSL_0663, VSL_0939
////            topMenu.goToDemandShipmentTab();
////            topSubMenu.clickOceanIcon();
////            topMenu.clickExceptionsTab();
////            topMenu.clickDemandShipmentExceptionsDelayed();
////            topSubMenu.clickSearchButton();
////            topSubMenu.goToSearchFeature(shipmentNo6a);
////            Assert.assertTrue(topMenu.isDemandShipmentExceptionsDelayedVisible());
////            topMenu.assertActualAndExpectedDemandShipmentNumberEqualOcean(shipmentNo6b);
////            topMenu.clearFilter();
////            driver.navigate().refresh();
////        }
////        @Test(priority = 7, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////        public void validatingExceptionStatusLateTC7 (String shipmentNo7a,String shipmentNo7b){
////            topMenu.goToDemandShipmentTab();
////            topSubMenu.clickOceanIcon();
////            topMenu.clickExceptionsTab();
////            topMenu.clickDemandShipmentExceptionsLate();
////            topSubMenu.clickSearchButton();
////            topSubMenu.goToSearchFeature(shipmentNo7a);
////            Assert.assertTrue(topMenu.isDemandShipmentExceptionsLateVisible());
////            topMenu.assertActualAndExpectedDemandShipmentNumberEqualOcean(shipmentNo7b);
////            topMenu.clearFilter();
////            driver.navigate().refresh();
////        }
////
////
////        @Test(priority = 8, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////        public void validatingExceptionStatusDelayedTC8(String shipmentNo8a,String shipmentNo8b)
////        {
////            topMenu.goToDemandShipmentTab();
////            topSubMenu.clickOceanIcon();
////            topMenu.clickExceptionsTab();
////            topMenu.clickDemandShipmentExceptionsDelayed();
////            topSubMenu.clickSearchButton();
////            topSubMenu.goToSearchFeature(shipmentNo8a);
////            Assert.assertTrue(topMenu.isDemandShipmentExceptionsDelayedVisible());
////            topMenu.assertActualAndExpectedDemandShipmentNumberEqualOcean(shipmentNo8b);
////            topMenu.clearFilter();
////            driver.navigate().refresh();
////
////        }
////
////        @Test(priority = 9, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////        public void validatingExceptionStatusEarlyTC9(String shipmentNo9a,String shipmentNo9b)
////        {
////            topMenu.goToDemandShipmentTab();
////            topSubMenu.clickOceanIcon();
////            topMenu.clickExceptionsTab();
////            topMenu.clickDemandShipmentExceptionsEarly();
////            topSubMenu.clickSearchButton();
////            topSubMenu.goToSearchFeature(shipmentNo9a);
////            Assert.assertTrue(topMenu.isDemandShipmentExceptionsEarlyVisible());
////            topMenu.assertActualAndExpectedDemandShipmentNumberEqualOcean(shipmentNo9b);
////            topMenu.clearFilter();
////            driver.navigate().refresh();
////        }
////
////        @Test(priority = 10, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////        public void validatingExceptionStatusOnTimeTC10(String shipmentNo10a,String shipmentNo10b)
////        {
////            topMenu.goToDemandShipmentTab();
////            topSubMenu.clickOceanIcon();
////            topMenu.clickExceptionsTab();
////            topMenu.clickDemandShipmentExceptionsOnTime();
////            topSubMenu.clickSearchButton();
////            topSubMenu.goToSearchFeature(shipmentNo10a);
////            Assert.assertTrue(topMenu.isDemandShipmentExceptionsOnTimeVisible());
////            topMenu.assertActualAndExpectedDemandShipmentNumberEqualOcean(shipmentNo10b);
////            topMenu.clearFilter();
////            driver.navigate().refresh();
////        }
////
////
////
////// Delivery exceptions starts          5605034048
////
////        @Test(priority = 11, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////        public void validatingExceptionStatusLateTC11 (String shipmentNo11a,String shipmentNo11b){
////            topMenu.gotoDemandDeliveryTab();
////            topMenu.clickExceptionsTab();
////            topSubMenu.clickOceanIcon();
////            topMenu.clickDemandDeliveryExceptionsLate();
////            topSubMenu.clickSearchButton();
////            topSubMenu.goToSearchFeature(shipmentNo11a);
////            Assert.assertTrue(topMenu.isDemandDeliveryExceptionsLateVisible());
////           // topMenu.assertActualAndExpectedDemandDeliveryNumberEqualOcean(shipmentNo11b);
////            topMenu.clearFilter();
////            driver.navigate().refresh();
////        }
////
////
////        @Test(priority = 12, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////        public void validatingExceptionStatusDelayedTC12(String shipmentNo12a,String shipmentNo12b)
////        {
////            topMenu.gotoDemandDeliveryTab();
////            topMenu.clickExceptionsTab();
////            topSubMenu.clickOceanIcon();
////            topMenu.clickDemandDeliveryExceptionsDelayed();
////            topSubMenu.clickSearchButton();
////            topSubMenu.goToSearchFeature(shipmentNo12a);
////            Assert.assertTrue(topMenu.isDemandDeliveryExceptionsDelayedVisible());
////            topMenu.assertActualAndExpectedDemandDeliveryNumberEqualOcean(shipmentNo12b);
////            topMenu.clearFilter();
////            driver.navigate().refresh();
////
////        }
////
////        @Test(priority = 13, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////        public void validatingExceptionStatusEarlyTC13 (String shipmentNo13a,String shipmentNo13b)
////        {
////            topMenu.gotoDemandDeliveryTab();
////            topMenu.clickExceptionsTab();
////            topSubMenu.clickOceanIcon();
////            topMenu.clickDemandDeliveryExceptionsEarly();
////            topSubMenu.clickSearchButton();
////            topSubMenu.goToSearchFeature(shipmentNo13a);
////            Assert.assertTrue(topMenu.isDemandDeliveryExceptionsEarlyVisible());
////            topMenu.assertActualAndExpectedDemandDeliveryNumberEqualOcean(shipmentNo13b);
////            topMenu.clearFilter();
////            driver.navigate().refresh();
////        }
////
////        @Test(priority = 14, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////        public void validatingExceptionStatusOnTimeTC14(String shipmentNo14a,String shipmentNo14b)
////        {
////            topMenu.gotoDemandDeliveryTab();
////            topMenu.clickExceptionsTab();
////            topSubMenu.clickOceanIcon();
////            topMenu.clickDemandDeliveryExceptionsOnTime();
////            topSubMenu.clickSearchButton();
////            topSubMenu.goToSearchFeature(shipmentNo14a);
////            Assert.assertTrue(topMenu.isDemandDeliveryExceptionsOnTimeVisible());
////            topMenu.assertActualAndExpectedDemandDeliveryNumberEqualOcean(shipmentNo14b);
////            topMenu.clearFilter();
////            driver.navigate().refresh();
////        }
////
////
////
////        @Test(priority = 15, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////        public void validatingExceptionStatusNoExceptionsTC15(String shipmentNo15a,String shipmentNo15b)
////        {
////            topMenu.gotoDemandDeliveryTab();
////            topSubMenu.clickOceanIcon();
////            topMenu.clickExceptionsTab();
////            topSubMenu.clickDemandDeliverySliderIcon();
////            topMenu.clickDemandDeliveryExceptionsNoExceptions();
////            topSubMenu.clickSearchButton();
////            topSubMenu.goToSearchFeature(shipmentNo15a);
////            Assert.assertTrue(topMenu.isDemandDeliveryExceptionsNoExceptionsVisible());
////            topMenu.assertActualAndExpectedDemandDeliveryNumberEqualOcean(shipmentNo15b);
////            topMenu.clearFilter();
////            driver.navigate().refresh();
////        }
////
////
////
////        //Order Exceptions
////
////        @Test(priority = 16, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////        public void validatingExceptionStatusPastDuesTC16(String shipmentNo16a,String shipmentNo16b)
////        {
////            topMenu.goToDemandOrdersTab();
////            topMenu.clickExceptionsTab();
////            topMenu.clickDemandOrdersExceptionsPastDue();
////            topSubMenu.clickSearchButton();
////            topSubMenu.goToSearchFeature(shipmentNo16a);
////            Assert.assertTrue(topMenu.isDemandOrdersExceptionsPastDueVisible());
////            topMenu.assertActualAndExpectedDemandOrdersNumberEqual(shipmentNo16b);
////            topMenu.clearFilter();
////            driver.navigate().refresh();
////
////        }
////
////
////
////        @Test(priority = 17, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////        public void validatingExceptionStatusLateTC17(String shipmentNo17a,String shipmentNo17b)
////        {
////            topMenu.goToDemandOrdersTab();
////            topMenu.clickExceptionsTab();
////           topMenu.clickDemandOrdersExceptionsLate();
////            topSubMenu.clickSearchButton();
////            topSubMenu.goToSearchFeature(shipmentNo17a);
////            Assert.assertTrue(topMenu.isDemandOrdersExceptionsLateVisible());
////            topMenu.assertActualAndExpectedDemandOrdersNumberEqual(shipmentNo17b);
////            topMenu.clearFilter();
////            driver.navigate().refresh();
////
////        }
////
////        @Test(priority = 18, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////        public void validatingExceptionStatusUnconfirmedTC18(String shipmentNo18a,String shipmentNo18b)
////        {
////            topMenu.goToDemandOrdersTab();
////            topMenu.clickExceptionsTab();
////            topMenu.clickDemandOrdersExceptionsUnconfirmed();
////            topSubMenu.clickSearchButton();
////            topSubMenu.goToSearchFeature(shipmentNo18a);
////            Assert.assertTrue(topMenu.isDemandOrdersExceptionsUnconfirmedVisible());
////            topMenu.assertActualAndExpectedDemandOrdersNumberEqual(shipmentNo18b);
////            topMenu.clearFilter();
////            driver.navigate().refresh();
////        }
////
////        @Test(priority = 19, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////        public void validatingExceptionStatusOnTimeTC19(String shipmentNo19a,String shipmentNo19b) throws InterruptedException {
////            topMenu.goToDemandOrdersTab();
////            topMenu.clickExceptionsTab();
////            topMenu.clickDemandOrdersExceptionsOnTime();
////            topSubMenu.clickSearchButton();
////            topSubMenu.goToSearchFeature(shipmentNo19a);
////            Assert.assertTrue(topMenu.isDemandOrdersExceptionsOnTimeVisible());
////            topMenu.assertActualAndExpectedDemandOrdersNumberEqual(shipmentNo19b);
////            topMenu.clearFilter();
////            driver.navigate().refresh();
////            leftMenu.clickInventoryMenuOption();
////
////        }
////
//
////
////        //Demand Inventory Exceptions
////
////        @Test(priority = 20, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////        public void validatingExceptionStatusStockOutTC20(String shipmentNo20a,String shipmentNo20b) throws InterruptedException {
////            leftMenu.clickInventoryMenuOption();
////
//
////            topMenu.clickInventoryStockout();
//
////            topMenu.clickInventoryStockOut();
//
////            topSubMenu.clickSearchButton();
////            topSubMenu.goToSearchFeature(shipmentNo20a);
////            Assert.assertTrue(topMenu.isInventoryStockoutVisible());
////            topMenu.assertActualAndExpectedInventoryNumberEqual(shipmentNo20b);
////            topMenu.clearFilter();
////            driver.navigate().refresh();
////        }
////
////
////        @Test(priority = 21, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////        public void validatingLowStockStatusTC21(String shipmentNo21a,String shipmentNo21b)
////        {
////            topMenu.clickDemandInventorySLowStock();
////            topSubMenu.clickSearchButton();
////            topSubMenu.goToSearchFeature(shipmentNo21a);
////            Assert.assertTrue(topMenu.isDemandInventoryExceptionsLowStockVisible());
////            topMenu.assertActualAndExpectedInventoryNumberEqual(shipmentNo21b);
////            topMenu.clearFilter();
////            driver.navigate().refresh();
////
////        }
////
////
////        @Test(priority = 22, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////        public void validatingSOverstockStatusTC22(String shipmentNo22a,String shipmentNo22b)
////        {
////            topMenu.clickDemandInventoryOverstock();
////            topSubMenu.clickSearchButton();
////            topSubMenu.goToSearchFeature(shipmentNo22a);
////            Assert.assertTrue(topMenu.isDemandInventoryExceptionsOverstockVisible());
////            topMenu.assertActualAndExpectedInventoryNumberEqual(shipmentNo22b);
////            topMenu.clearFilter();
////           driver.navigate().refresh();
////
////        }
////
////        @Test(priority = 23, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////        public void validatingOnTrackStatusTC23(String shipmentNo23a,String shipmentNo23b)
////        {
////            topMenu.clickDemandInventoryOnTrack();
////            topSubMenu.clickSearchButton();
////            topSubMenu.goToSearchFeature(shipmentNo23a);
////            Assert.assertTrue(topMenu.isDemandInventoryExceptionsOnTrackVisible());
////           // topMenu.assertActualAndExpectedInventoryNumberEqual(shipmentNo23b);
////            topMenu.clearFilter();
////            driver.navigate().refresh();
////
////        }
//
//
//
//
//    }
//
//
//
//
//
//
//
////
////
////
////    }
////
////
////
////
////
////
//
