package com.jda.dct.e2e.api.salesOrders

import com.jda.dct.e2e.api.categories.Regression
import com.jda.dct.e2e.api.categories.Smoke
import com.jda.dct.e2e.api.utility.IngestionHelper
import org.junit.experimental.categories.Category
import org.springframework.http.HttpMethod
import spock.lang.Shared
import spock.lang.Specification

/**
 * End-to-end tests for {@code Sales order} ingestion.
 *
 * @author Kishore Hanumansetty
 * @version 1.0-SNAPSHOT
 * @since 13-Jul-2018
 */

// evaluate test cases for migration to ReadyAPI and deletion
class IngestSalesOrderSpec extends Specification implements IngestionHelper {

    @Shared
            sampleSOLate,
            sampleSOOnTime,
            sampleSOUnknownStatus,
            sampleSOReceived,
            sampleSiteForAlert,
            sampleItemForAlert,
            sampleSalesOrderPreActionAdd,
            sampleSalesOrderPreActionUpdate

    def setupSpec() {
        init()

        def sitePath = properties.getProperty('ingestSitesPath')
        def itemPath = properties.getProperty('ingestItemsPath')

        siteIngestionUrl = protocol + ip + sitePath
        itemIngestionUrl = protocol + ip + itemPath

        sampleSiteWithDesc = this.getClass().getClassLoader().getResource('sample-site-with-desc.json').text
        sampleSalesOrderNoDesc = this.getClass().getClassLoader().getResource('sample-sales-order-no-desc.json').text

        def salesOrderPath = properties.getProperty('ingestSalesOrdersPath')
        def getPath = properties.getProperty('getSalesOrderPath')

        def searchAlertPath = properties.get("searchAlertPath")
        searchAlertUrlPrefix = protocol + ip + searchAlertPath

        salesOrderIngestionUrl = protocol + ip + salesOrderPath


        ingestionURL = protocol + ip + salesOrderPath
        getURLPrefix = protocol + ip + getPath

        sampleSalesOrder = this.getClass().getClassLoader().getResource('sample-sales-order-1.json').text
        sampleSOLate = this.getClass().getClassLoader().getResource('sample-sales-order-alert-late-1.json').text
        sampleSOOnTime = this.getClass().getClassLoader().getResource('sample-sales-order-alert-on-time-1.json').text
        sampleSOUnknownStatus = this.getClass().getClassLoader().getResource('sample-sales-order-alert-unknown-status-1.json').text
        sampleSOReceived = this.getClass().getClassLoader().getResource('sample-sales-order-alert-late-received-1.json').text
        sampleSiteForAlert = this.getClass().getClassLoader().getResource('sample-site-for-alert-testing-1.json').text
        sampleItemForAlert = this.getClass().getClassLoader().getResource('sample-item-for-alert-testing-1.json').text
        sampleSalesOrderPreActionAdd = this.getClass().getClassLoader().getResource('sample-sales-order-preaction-computation-add.json').text
        sampleSalesOrderPreActionUpdate = this.getClass().getClassLoader().getResource('sample-sales-order-preaction-computation-update.json').text

    }
    @Category(Regression)
    def "validating sales order information are updated on sales orders"() {
        given: 'get ingested sales order from showcase and duplicate it as a testing data, ingest the testing sales order'
        SampleSalesOrderShowcaseData sample = new SampleSalesOrderShowcaseData()
        Map<String, String> modifiedFields = new HashMap<>()
     //   modifiedFields.put("salesOrderId", "saleTest01")
        modifiedFields.put("shipToSiteName", "destinationTest01")
        def originalSales = sample.modifySalesOrderMultiFields("SO0012_SD", modifiedFields, "temp-sales.json")
        ingestData(originalSales, "salesOrders?processType=demand&salesOrderType=standardOrder")

        when: 'ingest an update to the same delivery - update shipment and line ids'
        modifiedFields.clear()
    //    modifiedFields.put("salesOrderId", "saleTest02")
        modifiedFields.put("shipToSiteName", "destinationTest03")
        def salesWithUpdatedShipmentInfo = sample.modifySalesOrderMultiFields("SO0012_SD", modifiedFields, "temp-sales.json")
        ingestData(salesWithUpdatedShipmentInfo, "salesOrders?processType=supply&deliveryType=inboundDelivery")

        then: 'ids are updated'
        def updatedSales = sample.getSalesOrderFromShowcase("SO0012_SD")
        assert updatedSales["shipToSiteName"] == "destinationTest03"
    }
//    def "Ingesting a sales order object gets a ingestion id"() {
//        when: 'a sales order json ingested'
//        def response = executeHttpCall(header, ingestionURL, sampleSalesOrder, HttpMethod.POST)
//
//        then: 'a valid ingestion id is returned'
//        response.ingestionId.split('-')[0..1].join('-') == 'a4b7a825f67930965747445709011120-dctsalesorders'
//    }
//
//    def "Ingestion status of a valid sales order becomes complete"() {
//        given:
//        def conditions = new PollingConditions(timeout: 120, initialDelay: 1)
//
//        when: 'a sales order json ingested'
//        def response = executeHttpCall(header, ingestionURL, sampleSalesOrder, HttpMethod.POST)
//        def ingestionId = response.ingestionId
//
//        then: 'ingestion status eventually becomes COMPLETED'
//        conditions.eventually {
//            def status = getStatus(ingestionId)
//            assert status.contains('COMPLETED')
//        }
//    }
//
//    def "Add a Note to a Sales Order and get it back"() {
//        given:
//        def conditions = new PollingConditions(timeout: 120, initialDelay: 1)
//
//        when: 'A Note is added to the sales order'
//
//        def salesOrderIds = getSalesOrderIds(sampleSalesOrder)
//        def url = getURLPrefix + 'notes/' + salesOrderIds[0]
//        def sampleNote = '{ "note": "Sales Order Note added"}'
//        def response = getHttpClient(header, url, sampleNote, HttpMethod.POST)
//        assert response.status == 200
//
//        and: "Get the sales order"
//
//        url = getURLPrefix + salesOrderIds[0]
//        def salesOrderText = getHttpClient(header, url, null, HttpMethod.GET).body
//        def salesOrderRetrieved = new JsonSlurper().parseText(salesOrderText).get('data').get(0)
//
//        then: 'Get the Sales Order back and verify the note'
//        assert salesOrderRetrieved.getAt('notes').toString().contains("Sales Order Note added") == true
//    }
//
//    def "Ingestion status of an invalid sales order becomes complete with error"() {
//        given:
//        def conditions = new PollingConditions(timeout: 120, initialDelay: 1)
//        def modifiedSalesOrder = sampleSalesOrder.replaceAll("\"salesOrderType\": \"standardOrder\"", "\"salesOrderType\": \"wrong-order-type\"")
//
//        when: 'a sales order json ingested'
//        def response = executeHttpCall(header, ingestionURL, modifiedSalesOrder, HttpMethod.POST)
//        def ingestionId = response.ingestionId
//
//        then: 'ingestion status eventually becomes COMPLETED WITH ERROR'
//        conditions.eventually {
//            def status = getStatus(ingestionId)
//            assert status.contains('COMPLETED_WITH_ERROR')
//        }
//    }
//
//    def "Ingestion status of an invalid attributes in sales order becomes complete with error"() {
//        given:
//        def conditions = new PollingConditions(timeout: 120, initialDelay: 1)
//        def modifiedSalesOrder = sampleSalesOrder.replaceAll("\"salesOrderId\": \"SO986\"", "\"\": \"\"")
//
//        when: 'a sales order json ingested'
//        def response = executeHttpCall(header, ingestionURL, modifiedSalesOrder, HttpMethod.POST)
//        def ingestionId = response.ingestionId
//
//        then: 'ingestion status eventually becomes COMPLETED WITH ERROR'
//        conditions.eventually {
//            def status = getStatus(ingestionId)
//            assert status.contains('COMPLETED_WITH_ERROR')
//        }
//    }
//
//    def "Get ingested sales order back"() {
//        given: 'Sales Orders ingested completely'
//        def salesOrderSent = getJsonAt(sampleSalesOrder, 0)
//
//        ingestData(sampleSalesOrder)
//        def salesOrderIds = getSalesOrderIds(sampleSalesOrder)
//        def url = getURLPrefix + salesOrderIds[0]
//        def salesOrderText = getHttpClient(header, url, null, HttpMethod.GET).body
//        def salesOrderRetrieved = new JsonSlurper().parseText(salesOrderText).get('data').get(0)
//        ['attributes', 'creationDate', 'id', 'lmd', 'maxDelayInHours', 'exceptionStatus', 'segment'].each {
//            salesOrderRetrieved.remove(it)
//        }
//        ['creationDate', 'lmd', 'delayHours', 'lineExceptionStatus', 'salesOrderLines', 'segment'].each { key ->
//            salesOrderRetrieved.get('salesOrderLines').values().each { it.remove(key) }
//        }
//
//        def mapper = new ObjectMapper()
//        def expected = mapper.readValue(groovy.json.JsonOutput.toJson(salesOrderSent), SalesOrder)
//        def received = mapper.readValue(groovy.json.JsonOutput.toJson(salesOrderRetrieved), SalesOrder)
//
//        expect:
//        received == expected
//    }
//
//    def "Get ingested sales order back and verify its data"() {
//        given: 'Sales Orders ingested completely'
//        def salesOrderSent = getJsonAt(sampleSalesOrder, 0)
//
//        when:
//        ingestData(sampleSalesOrder)
//        def salesOrderIds = getSalesOrderIds(sampleSalesOrder)
//        def url = getURLPrefix + salesOrderIds[0]
//
//        def salesOrderText = getHttpClient(header, url, null, HttpMethod.GET).body
//        def salesOrderRetrieved = new JsonSlurper().parseText(salesOrderText).get('data').get(0)
//
//        def mapper = new ObjectMapper()
//        def responseDef = mapper.readValue(groovy.json.JsonOutput.toJson(salesOrderRetrieved), SalesOrder)
//
//        then:
//        assert responseDef != null
//        def shipToAddress = responseDef.getExtField("shipToAddress")
//        assert shipToAddress != null
//        assert shipToAddress.getAt("addressLine1").toString() == "no not my address"
//        assert shipToAddress.getAt("addressLine2").toString() == "its boring"
//        assert shipToAddress.getAt("city").toString() == "Yunfu Shi"
//        assert shipToAddress.getAt("state").toString() == "Guangdong Sheng"
//        assert shipToAddress.getAt("postalCode").toString() == "518000"
//        assert shipToAddress.getAt("country").toString() == "China"
//        assert shipToAddress.getAt("latitude").toString() == "0.2"
//        assert shipToAddress.getAt("longitude").toString() == "0.5"
//
//
//    }
//
//    def "Sales Order with no site description gets filled in based on site names"() {
//        given: 'A site with a description is ingested'
//
//        ingestData(sampleSiteWithDesc, siteIngestionUrl)
//
//        when: "A Sales Order is ingested that does not contain a description"
//
//        ingestData(sampleSalesOrderNoDesc, salesOrderIngestionUrl)
//
//        def salesOrderIds = getSalesOrderIds(sampleSalesOrderNoDesc)
//        def url = getURLPrefix + salesOrderIds[0]
//        def salesOrderText = getHttpClient(header, url, null, HttpMethod.GET).body
//        def salesOrderRetrieved = new JsonSlurper().parseText(salesOrderText).get('data').get(0)
//
//        def mapper = new ObjectMapper()
//        def received = mapper.readValue(groovy.json.JsonOutput.toJson(salesOrderRetrieved), SalesOrder.class)
//
//        def salesOrderLineList = received.getSalesOrderLines().values()
//        def salesOrderLine1 = salesOrderLineList.getAt(0)
//        def salesOrderLine2 = salesOrderLineList.getAt(1)
//
//        then:
//        salesOrderLine1.getExtFields().get("shipToSiteDescription") == "Description to be copied"
//        salesOrderLine1.getExtFields().get("shipFromSiteDescription") == "Description to be copied"
//        salesOrderLine2.getExtFields().get("shipToSiteDescription") == "Description to be copied"
//        salesOrderLine2.getExtFields().get("shipFromSiteDescription") == "Description to be copied"
//    }
//
//    def "field in process model updates upon a new ingest"() {
//        given: 'original sales order ingested'
//        ingestData(sampleSalesOrder)
//        def salesOrderIds = getSalesOrderIds(sampleSalesOrder)
//        def url = getURLPrefix + salesOrderIds[0]
//
//        when: 'ingest updated sales order'
//        def modifiedSampleSalesOrder = sampleSalesOrder.replaceAll("\"customerName\": \"IBM\"", "\"customerName\": \"Google\"")
//        def expectedSalesOrder = getJsonAt(modifiedSampleSalesOrder, 0)
//
//        ingestData(modifiedSampleSalesOrder)
//
//        def salesOrderText = getHttpClient(header, url, null, HttpMethod.GET).body
//        def salesOrderRetrieved = new JsonSlurper().parseText(salesOrderText).get('data').get(0)
//        ['attributes', 'creationDate', 'id', 'lmd', 'maxDelayInHours', 'exceptionStatus', 'segment'].each {
//            salesOrderRetrieved.remove(it)
//        }
//        ['creationDate', 'lmd', 'delayHours', 'lineExceptionStatus', 'salesOrderLines', 'segment'].each { key ->
//            salesOrderRetrieved.get('salesOrderLines').values().each { it.remove(key) }
//        }
//
//        def mapper = new ObjectMapper()
//        def expected = mapper.readValue(groovy.json.JsonOutput.toJson(expectedSalesOrder), SalesOrder)
//        def received = mapper.readValue(groovy.json.JsonOutput.toJson(salesOrderRetrieved), SalesOrder)
//
//        then: 'the process model field updated'
//        received == expected
//    }
//
//    def "field not in process model not updated upon a new ingest"() {
//        given: 'original sales order ingested'
//        ingestData(sampleSalesOrder)
//        def salesOrderIds = getSalesOrderIds(sampleSalesOrder)
//        def url = getURLPrefix + salesOrderIds[0]
//
//        when: 'ingest updated sales order'
//        def modifiedSamplePO = sampleSalesOrder.replaceAll("\"invalidField\": invalidField", "\"invalidField\": invalidField")
//        def expectedPO = getJsonAt(sampleSalesOrder, 0)
//
//        ingestData(modifiedSamplePO)
//
//        def salesOrderText = getHttpClient(header, url, null, HttpMethod.GET).body
//        def salesOrderRetrieved = new JsonSlurper().parseText(salesOrderText).get('data').get(0)
//        ['attributes', 'creationDate', 'id', 'lmd', 'maxDelayInHours', 'exceptionStatus', 'segment'].each {
//            salesOrderRetrieved.remove(it)
//        }
//        ['creationDate', 'lmd', 'delayHours', 'lineExceptionStatus', 'salesOrderLines', 'segment'].each { key ->
//            salesOrderRetrieved.get('salesOrderLines').values().each { it.remove(key) }
//        }
//
//        def mapper = new ObjectMapper()
//        def expected = mapper.readValue(groovy.json.JsonOutput.toJson(expectedPO), SalesOrder)
//        def received = mapper.readValue(groovy.json.JsonOutput.toJson(salesOrderRetrieved), SalesOrder)
//
//        then: 'non-process model field not updated'
//
//        System.out.println("Expected:" + expected.toString())
//        System.out.println("Actual:" + received.toString())
//        received == expected
//    }
//
//    def "Ingest a late sales order which triggers alert service to generate alerts"() {
//        given: 'Sales Order with exception status'
//
//        ingestData(sampleSiteForAlert, siteIngestionUrl)
//        ingestData(sampleItemForAlert, itemIngestionUrl)
//
//        when: 'Ingesting data'
//        ingestData(salesOrderToIngest)
//
//        def salesOrderIds = getSalesOrderIds(salesOrderToIngest)
//
//        final Map<String, String> searchCriteria = new HashMap<>()
//        searchCriteria.put("limit", "1")
//        searchCriteria.put("query", "objectId=" + salesOrderIds[0])
//
//        sleep(10000)
//
//        def responseDef =
//                searchResponseJson(searchCriteria, searchAlertUrlPrefix, state, 60)
//        then:
//        assert responseDef != null
//        def alertDef = responseDef.get("data").getAt(0)
//        assert alertDef.get("objectId") != null
//        assert alertDef.get("objectId") == salesOrderIds[0]
//        assert alertDef.get("objectType") != null
//        assert alertDef.get("objectType") == 'SalesOrder'
//        assert alertDef.get("state") == state
//        assert alertDef.get("severity") == severity
//        assert alertDef.get("type") == 'Exception Status'
//        assert alertDef.get("objectState") == objectState
//        if (state == 'Open') {
//            assert alertDef.get("closedDate") == null
//        }
//        assert alertDef.get("alertStatusLmd") != null
//        def references = alertDef.get("refObjects")
//        assert references != null
//
//        assert references.get("shipToSites") != null
//        def shipToSite = references.get("shipToSites").get(0)
//        assert shipToSite.get("participantName").equals("Samsung")
//        assert shipToSite.get("siteName").equals("Korea")
//        assert shipToSite.get("siteOwnerName").equals("Samsung Korea Owner")
//
//        assert references.get("shipFromSites") != null
//        def shipFromSite = references.get("shipFromSites").get(0)
//        assert shipFromSite.get("participantName").equals("LG")
//        assert shipFromSite.get("siteName").equals("China")
//        assert shipFromSite.get("siteOwnerName").equals("LG China Owner")
//
//        assert references.get("customerItems") != null
//        def customerItem = references.get("customerItems").get(0)
//        customerItem.get("itemName").equals("Ship to item name")
//        customerItem.get("itemOwnerName").equals("Samsung")
//
//        assert references.get("supplierItems") != null
//        def supplierItem = references.get("supplierItems").get(0)
//        assert supplierItem.get("itemName").equals("Ship from item name")
//        assert supplierItem.get("itemOwnerName").equals("LG")
//
//        where:
//        salesOrderToIngest    | state    | severity | exceptionStatus | objectState
//        sampleSOLate          | 'Open'   | 4        | 'Late'          | 'Open'
//        sampleSOUnknownStatus | 'Open'   | 4        | 'Late'          | 'Confirmed'
//        sampleSOReceived      | 'Closed' | 4        | 'Late'          | 'Received'
//        sampleSOLate          | 'Open'   | 4        | 'Late'          | 'Open'
//        sampleSOOnTime        | 'Closed' | 1        | 'On Time'       | 'Delivered'
//    }
//
//    def "Ingest a sales order which triggers computation on the sales order itself and header status as Late"() {
//
//        given: 'original sales order ingested'
//        def compuationSO = this.getClass().getClassLoader().getResource('sample-sales-order-computation-2.json').text
//
//        when: 'ingest sales order'
//
//        ingestData(compuationSO)
//        def salesOrderIds = getSalesOrderIds(compuationSO)
//        def url = getURLPrefix + salesOrderIds[0]
//        //sleep(60000)
//        def salesOrderText = getHttpClient(header, url, null, HttpMethod.GET).body
//        def salesOrderRetrieved = new JsonSlurper().parseText(salesOrderText)
//
//        then: 'verify that sales order exception status is updated'
//
//        salesOrderRetrieved.getAt('data').getAt('exceptionStatus').toString() == '[Late]'
//        salesOrderRetrieved.getAt('data').getAt('salesOrderLines').getAt('SL41').getAt('exceptionStatus').toString() == '[Late]'
//        salesOrderRetrieved.getAt('data').getAt('salesOrderLines').getAt('SL42').getAt('exceptionStatus').toString() == '[On Time]'
//        salesOrderRetrieved.getAt('data').getAt('salesOrderLines').getAt('SL43').getAt('exceptionStatus').toString() == '[Late]'
//    }
//
//    def "Ingest a sales order which triggers computation on the sales order itself  and header status as On Time"() {
//
//        given: 'original sales order ingested'
//        def compuationSO = this.getClass().getClassLoader().getResource('sample-sales-order-computation-3.json').text
//
//        when: 'ingest sales order'
//
//        ingestData(compuationSO)
//        def salesOrderIds = getSalesOrderIds(compuationSO)
//        def url = getURLPrefix + salesOrderIds[0]
////        sleep(60000)
//        def salesOrderText = getHttpClient(header, url, null, HttpMethod.GET).body
//        def salesOrderRetrieved = new JsonSlurper().parseText(salesOrderText)
//
//        then: 'verify that sales order exception status is updated'
//
//        salesOrderRetrieved.getAt('data').getAt('exceptionStatus').toString() == '[Late]'
//        salesOrderRetrieved.getAt('data').getAt('salesOrderLines').getAt('SL41').getAt('exceptionStatus').toString() == '[On Time]'
//        salesOrderRetrieved.getAt('data').getAt('salesOrderLines').getAt('SL42').getAt('exceptionStatus').toString() == '[On Time]'
//        salesOrderRetrieved.getAt('data').getAt('salesOrderLines').getAt('SL43').getAt('exceptionStatus').toString() == '[Late]'
//    }
//
//    def "Ingest a sales order with line status and get the aggregated header status"() {
//
//        given: 'sales order with line status ingested'
//        def salesOrderAggreg = this.getClass().getClassLoader().getResource('sample-sales-order-state-aggregate-1.json').text
//
//        when: 'ingest sales order'
//
//        ingestData(salesOrderAggreg)
//        def salesOrderIds = getSalesOrderIds(salesOrderAggreg)
//        def url = getURLPrefix + salesOrderIds[0]
//        def salesOrderText = getHttpClient(header, url, null, HttpMethod.GET).body
//        def salesOrderRetrieved = new JsonSlurper().parseText(salesOrderText).get('data').get(0)
//        def mapper = new ObjectMapper()
//        def retrieved = mapper.readValue(groovy.json.JsonOutput.toJson(salesOrderRetrieved), SalesOrder)
//
//        then: 'verify that sales order header status is aggregated'
//        retrieved.getState() == "Open"
//    }
//
//    def "Should calculate tot quantities in preaction given a new or updated Sales order"() {
//        given: 'Sales Order ingested completely'
//        def mapper = new ObjectMapper()
//
//        when: 'Ingesting data'
//        ingestData(salesOrderToIngest)
//
//        def salesOrderIds = getSalesOrderIds(salesOrderToIngest)
//        def url = getURLPrefix + salesOrderIds[0]
//        def salesOrderText = getHttpClient(header, url, null, HttpMethod.GET).body
//        def salesOrderRetrieved = new JsonSlurper().parseText(salesOrderText).get('data').get(0)
//        def received = mapper.readValue(groovy.json.JsonOutput.toJson(salesOrderRetrieved), SalesOrder)
//
//        def expectedQuantity = mapper.readTree(expectedQuantityJson)
//
//        then:
//        assert received != null
//        assert received.getSalesOrderLines() != null
//        assert received.getSalesOrderLines().size() == 2
//        received.getSalesOrderLines().values().each {
//            assert it.getExtField("totalConfirmedQuantity") == expectedQuantity.get(it.getSoLineId()).get("confirmed").doubleValue()
//            assert it.getExtField("totalShippedQuantity") == expectedQuantity.get(it.getSoLineId()).get("shipped").doubleValue()
//            assert it.getExtField("totalGrQuantity") == expectedQuantity.get(it.getSoLineId()).get("gr").doubleValue()
//            assert it.getExtField("totalOpenQuantity") == expectedQuantity.get(it.getSoLineId()).get("open").doubleValue()
//        }
//
//        where:
//        expectedQuantityJson                                                                                                                   | salesOrderToIngest
//        '{"SL41":{"gr":30.0,"open":300.0,"shipped":200.0,"confirmed":240.0},"SL42":{"gr":22.0,"open":100.0,"shipped":100.0,"confirmed":90.0}}' | sampleSalesOrderPreActionAdd
//        '{"SL41":{"gr":20.0,"open":300.0,"shipped":200.0,"confirmed":140.0},"SL42":{"gr":21.0,"open":100.0,"shipped":100.0,"confirmed":75.0}}' | sampleSalesOrderPreActionUpdate
//    }
}
