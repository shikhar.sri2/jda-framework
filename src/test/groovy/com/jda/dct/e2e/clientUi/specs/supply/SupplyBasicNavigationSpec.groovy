//<<<<<<< HEAD
//package com.jda.dct.e2e.clientUi.specs.tempraturecontrolled
//
//
//import com.jda.dct.e2e.clientUi.pages.tempraturecontrolled.SupplyDeliveryListPage
//import com.jda.dct.e2e.clientUi.pages.tempraturecontrolled.SupplyInventoryListPage
//import com.jda.dct.e2e.clientUi.pages.tempraturecontrolled.SupplyShipmentsListPage
//import com.jda.dct.e2e.clientUi.pages.tempraturecontrolled.SupplyOrdersListPage
//import geb.driver.CachingDriverFactory
//import geb.spock.GebSpec
//import spock.lang.Stepwise
//
//@Stepwise
//class SupplyBasicNavigationSpec extends GebSpec{
//
//    def setupSpec() {
//        CachingDriverFactory.clearCache()
//        to LoginFF
//        at(LoginFF).login()
//        println "Successfully Login to Luminate Control Tower. Current page url:"
//        println getDriver().getCurrentUrl()
//    }
//
//    def "From watch tower page, user can navigate to tempraturecontrolled" () {
//        given: "User is at watch tower page"
//        to WatchTowerPage
//
//        when: "User click tempraturecontrolled button from menu panel"
//        at(WatchTowerPage).navigateToSupply()
//
//        then: "User is at tempraturecontrolled orders list page by default"
//        at SupplyOrdersListPage
//    }
//
//    def "From tempraturecontrolled, User can navigate to shipment list page"(){
//        given: "User is at tempraturecontrolled module"
//        to SupplyOrdersListPage
//        println "\nAt tempraturecontrolled module, user can navigate to shipment list page"
//        println "User is at tempraturecontrolled module"
//        println "Current page url:" + getDriver().getCurrentUrl()
//
//        when: "User click shipments on the top"
//        at(SupplyOrdersListPage).goToShipment()
//        println "User click shipments on the top"
//
//        then: "User is at tempraturecontrolled shipment list page"
//        at SupplyShipmentsListPage
//        println "User is at tempraturecontrolled shipment list page"
//        println "Current page url:" + getDriver().getCurrentUrl()
//    }
//
//    def "From tempraturecontrolled, User can navigate to inventory list page" (){
//        given: "User is at tempraturecontrolled module"
//        to SupplyOrdersListPage
//        println "\nAt tempraturecontrolled module, User can navigate to inventory list page"
//        println "User is at tempraturecontrolled module"
//        println "Current page url:" + getDriver().getCurrentUrl()
//
//        when: "User click inventory on the top"
//        at(SupplyOrdersListPage).goToInventory()
//        println "User click inventory on the top"
//
//        then: "User is at tempraturecontrolled inventory list page"
//        at SupplyInventoryListPage
//        println "User is at tempraturecontrolled inventory list page"
//        println "Current page url:" + getDriver().getCurrentUrl()
//    }
//
//    def "From tempraturecontrolled, User can navigate to delivery list page" (){
//        given: "User is at tempraturecontrolled module"
//        println "\nAt tempraturecontrolled module, User can navigate to delivery list page"
//        to SupplyOrdersListPage
//        println "User is at tempraturecontrolled module"
//        println "Current page url:" + getDriver().getCurrentUrl()
//
//        when: "User click delivery on the top"
//        at(SupplyOrdersListPage).goToDelivery()
//        println "User click delivery on the top"
//
//        then: "User is at tempraturecontrolled delivery list page"
//        at SupplyDeliveryListPage
//        println "User is at tempraturecontrolled delivery list page"
//        println "Current page url:" + getDriver().getCurrentUrl()
//    }
//
//    def cleanupSpec() {
//        CachingDriverFactory.clearCacheAndQuitDriver()
//    }
//}
//=======
////package com.jda.dct.e2e.clientUi.specs.tempraturecontrolled
////
////import com.jda.dct.e2e.clientUi.pages.tempraturecontrolled.SupplyDeliveryListPage
////import com.jda.dct.e2e.clientUi.pages.tempraturecontrolled.SupplyInventoryListPage
////import com.jda.dct.e2e.clientUi.pages.tempraturecontrolled.SupplyShipmentsListPage
////import com.jda.dct.e2e.clientUi.pages.tempraturecontrolled.SupplyOrdersListPage
////import com.jda.dct.e2e.clientUi.pages.login.LoginPage
////import com.jda.dct.e2e.clientUi.pages.watchTower.WatchTowerPage
////import geb.driver.CachingDriverFactory
////import geb.spock.GebSpec
////import spock.lang.Stepwise
////
////@Stepwise
////class SupplyBasicNavigationSpec extends GebSpec{
////
////    def setupSpec() {
////        CachingDriverFactory.clearCache()
////        to LoginPage
////        at(LoginPage).login()
////        println "Successfully Login to Luminate Control Tower. Current page url:"
////        println getDriver().getCurrentUrl()
////    }
////
////    def "From watch tower page, user can navigate to tempraturecontrolled" () {
////        given: "User is at watch tower page"
////        to WatchTowerPage
////
////        when: "User click tempraturecontrolled button from menu panel"
////        at(WatchTowerPage).navigateToSupply()
////
////        then: "User is at tempraturecontrolled orders list page by default"
////        at SupplyOrdersListPage
////    }
////
////    def "From tempraturecontrolled, User can navigate to shipment list page"(){
////        given: "User is at tempraturecontrolled module"
////        to SupplyOrdersListPage
////        println "\nAt tempraturecontrolled module, user can navigate to shipment list page"
////        println "User is at tempraturecontrolled module"
////        println "Current page url:" + getDriver().getCurrentUrl()
////
////        when: "User click shipments on the top"
////        at(SupplyOrdersListPage).goToShipment()
////        println "User click shipments on the top"
////
////        then: "User is at tempraturecontrolled shipment list page"
////        at SupplyShipmentsListPage
////        println "User is at tempraturecontrolled shipment list page"
////        println "Current page url:" + getDriver().getCurrentUrl()
////    }
////
////    def "From tempraturecontrolled, User can navigate to inventory list page" (){
////        given: "User is at tempraturecontrolled module"
////        to SupplyOrdersListPage
////        println "\nAt tempraturecontrolled module, User can navigate to inventory list page"
////        println "User is at tempraturecontrolled module"
////        println "Current page url:" + getDriver().getCurrentUrl()
////
////        when: "User click inventory on the top"
////        at(SupplyOrdersListPage).goToInventory()
////        println "User click inventory on the top"
////
////        then: "User is at tempraturecontrolled inventory list page"
////        at SupplyInventoryListPage
////        println "User is at tempraturecontrolled inventory list page"
////        println "Current page url:" + getDriver().getCurrentUrl()
////    }
////
////    def "From tempraturecontrolled, User can navigate to delivery list page" (){
////        given: "User is at tempraturecontrolled module"
////        println "\nAt tempraturecontrolled module, User can navigate to delivery list page"
////        to SupplyOrdersListPage
////        println "User is at tempraturecontrolled module"
////        println "Current page url:" + getDriver().getCurrentUrl()
////
////        when: "User click delivery on the top"
////        at(SupplyOrdersListPage).goToDelivery()
////        println "User click delivery on the top"
////
////        then: "User is at tempraturecontrolled delivery list page"
////        at SupplyDeliveryListPage
////        println "User is at tempraturecontrolled delivery list page"
////        println "Current page url:" + getDriver().getCurrentUrl()
////    }
////
////    def cleanupSpec() {
////        CachingDriverFactory.clearCacheAndQuitDriver()
////    }
////}
//>>>>>>> d3314fef01710ac9cb28148dd1e359524510cf94
