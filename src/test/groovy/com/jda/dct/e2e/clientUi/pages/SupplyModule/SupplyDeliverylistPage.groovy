package com.jda.dct.e2e.clientUi.pages.SupplyModule

import geb.Page
import org.elasticsearch.join.aggregations.Children
import org.openqa.selenium.By
import org.openqa.selenium.WebElement


class SupplyDeliverylistPage extends Page {

        static url = "/tempraturecontrolled/delivery"

        static atCheckWaiting = true

        static at = {

            $("div", role:"button", 'aria-expanded':"false").first().isDisplayed()

        }

        static content = {


            deliveryNo(wait: true){

                $("#DeliveryList-ExpandablePanel-Cell").first().find("a")

            }

            containerNo(wait: true){

             $("#DeliveryList-ExpandablePanel-Cell").first().children()

            }




            deliveryItem(wait: true) {

                $("div.css-xlrzzc > h1")
            }

            shipmentlisttab(wait:true){

                $("#tempraturecontrolled-page-tab-shipments")
            }

    }


        def gotoSupplyDelivery() {


            deliveryNo.click()

            waitFor {
                deliveryItem.displayed
            }


        }



       def selectcontainer(){

             containerNo.click()
           Thread.sleep(2000)

           println("containerno is clicked ; please check shipment list page")

        waitFor {

            shipmentlisttab.displayed
        }
            println("welcometo shipmentlist list page from delivery container page")


        }

    }

