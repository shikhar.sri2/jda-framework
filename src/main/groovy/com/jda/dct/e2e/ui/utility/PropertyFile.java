package com.jda.dct.e2e.ui.utility;

import java.io.FileInputStream;
import java.util.Properties;

public class PropertyFile {
    private String propertyFilePath = "src/main/resources/data.properties";

    public Properties getProperties() {
        Properties properties = new Properties();
        try {
            // TODO: Add logic to throw error if property is invalid?
            properties.load(new FileInputStream(propertyFilePath));
            properties.putAll(System.getProperties());
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
        return properties;
    }
}
