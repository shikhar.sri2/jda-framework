package com.jda.dct.e2e.api.ums

import com.jda.dct.e2e.api.utility.IngestionHelper
import org.springframework.http.HttpMethod
import spock.lang.Shared
import spock.lang.Specification



class TenantRolesSpec extends Specification implements IngestionHelper{

    @Shared umsUrl
    @Shared tenantRoleUrl

    def setupSpec() {
        init()
        umsUrl = protocol + ip + umsPath
        tenantRoleUrl = umsUrl + 'tenantRoles'
    }
    // smoke
    def "Add a new tenant role"() {
        given: "Given a tenant role"
        def sampleTenantRole = this.getClass().getClassLoader().getResource('jsons/sample-tenant-role.json').text

        when: "Add the new role"
        def response = executeHttpCall(header, tenantRoleUrl, sampleTenantRole, HttpMethod.POST)

        then: "Get tenant role back"
        response != null && response.data.size() == 1 && response.data[0].roleName == 'Enterprise Supplier'
    }
    // smoke
    def "Get tenant role through API"() {
        when: "Get tenant role"
        def response = executeHttpCall(header, tenantRoleUrl, null, HttpMethod.GET)

        then: "Get role back"
        response != null && response.count > 0
    }
    // rest should be regression
    def "Edit tenant role"() {
        given: "Provided roleName is /Enterprise Supplier"
        def roleName = "/Enterprise Supplier"

        when: "Get this tenant role"
        def response = executeHttpCall(header, tenantRoleUrl + roleName, null, HttpMethod.GET)

        then: "The original role description of this role is Enterprise Fake Role"
        response != null && response.data[0].roleDescription == "Enterprise Supplier"

        and:
        when: "Get modified role json file, Update this tenant role, get this tenant role back"
        def modifiedJson = this.getClass().getClassLoader().getResource('jsons/sample-tenant-role-edit.json').text
        def response1 = executeHttpCall(header, tenantRoleUrl, modifiedJson, HttpMethod.POST)
        def response2 = executeHttpCall(header, tenantRoleUrl + roleName, null, HttpMethod.GET)

        then: "Role description of this role get updated"
        response2 != null && response2.data[0].roleDescription == "Enterprise Supplier1"
    }

    def "Delete tenant role through API"() {
        given: "With roleName == Enterprise Supplier, Tenant role exist"
        def roleName = "/Enterprise Supplier"

        when: "Execute GET call of this tenant role"
        def response = executeHttpCall(header, tenantRoleUrl + roleName, null, HttpMethod.GET)

        then: "Get this role back"
        response != null && response.count == 1

        and:
        when: "Delete this role with roleName is Enterprise Supplier"
        def response1 = executeHttpCall(header, tenantRoleUrl + roleName, null, HttpMethod.DELETE)
        def response2 = executeHttpCall(header, tenantRoleUrl + roleName, null, HttpMethod.GET)

        then: "Delete successfully, this role not exist"
        response2 != null && response2.count == 0
    }

    def "Verify error when tenant ID doesn't exist during tenant role association"() {
        given: "Given a tenant role with a not existed tenant id"
        def invalidTenantRoleJson = this.getClass().getClassLoader().getResource('jsons/sample-tenant-role-false.json').text

        when: "Associate the role with an not existed tenant id"
        int status = getHttpStatusVal(header, tenantRoleUrl, invalidTenantRoleJson, HttpMethod.POST)

        then: "Unable to associate"
        status != 200
    }
}
