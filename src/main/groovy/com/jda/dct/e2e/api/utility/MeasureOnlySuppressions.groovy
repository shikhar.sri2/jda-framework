package com.jda.dct.e2e.api.utility

import com.fasterxml.jackson.annotation.JsonIgnore

class MeasureOnlySuppressions {
    @JsonIgnore String tid
    @JsonIgnore String id
    @JsonIgnore String status
    @JsonIgnore Map<String, Object> attributes
    @JsonIgnore Date creationDate
    @JsonIgnore Date lmd
    @JsonIgnore String solutionType
    @JsonIgnore String shipFromSiteOwnerName
    @JsonIgnore String shipFromSiteName
    @JsonIgnore String suppItemOwnerName


}
