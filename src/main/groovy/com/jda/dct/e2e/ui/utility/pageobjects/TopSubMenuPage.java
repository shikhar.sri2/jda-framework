package com.jda.dct.e2e.ui.utility.pageobjects;

import com.jda.dct.e2e.ui.utility.action.CommonActions;
import com.jda.dct.e2e.ui.utility.common.BasePage;
import com.jda.dct.e2e.ui.utility.helper.Wait.WaitHelper;
import com.jda.dct.e2e.ui.utility.helper.Action.ActionHelper;
import com.jda.dct.e2e.ui.utility.helper.genericHelper.GenericHelper;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;

import java.util.Arrays;
import java.util.List;



public class TopSubMenuPage extends BasePage {
    WebDriver driver;
    WebDriverWait webDriverWait;
    WaitHelper waitHelper;
    GenericHelper genericHelper;
    WaitHelper objWaitHelper;
    GenericHelper objGenericHelper;
    CommonActions commonActions;
    ActionHelper actionHelper;

    public TopSubMenuPage(WebDriver webdriver) {

        this.driver = webdriver;
        PageFactory.initElements(driver, this);
        webDriverWait = new WebDriverWait(driver, 90);
        waitHelper = new WaitHelper(driver);
        genericHelper = new GenericHelper(driver);
        objWaitHelper = PageFactory.initElements(driver, WaitHelper.class);
        objGenericHelper = PageFactory.initElements(driver, GenericHelper.class);
        commonActions = new CommonActions(driver);
        actionHelper = new ActionHelper(driver);
    }

    //webelements for search
   //@FindBy(xpath = "//button[contains(@style,'height: 32px; width: 32px;')]")
    @FindBy(xpath = "//button[@id=\"search-icon\"]")
    public WebElement searchButton;

    @FindBy(xpath = "//input[starts-with(@id, 'react')]")
    public WebElement searchInputField;

    @FindBy(xpath = "//*[@id=\"toolBar\"]/div/div[1]/div/div[1]/button")
    public WebElement advancedFilterButton;

    //    @FindBy(xpath = "//div[contains(@class,'exanding-search-box-select__menu')]")
//    private WebElement autoSuggestionList;
    @FindBy(xpath = "//li[@role=\"option\"]")
    public List<WebElement> autoSuggestionListDisplay;
    @FindBy(xpath = "//div[@class='slick-arrow slick-next']/*/*")
    private WebElement demandDeliverynextIcon;
    //div[@class='slick-arrow slick-next']/*
    @FindBy(xpath = "//div[@class='slick-arrow slick-next']/*/*")
    private WebElement demandOrdersnextIcon;
    @FindBy(xpath = "//button[@value='Road']/span/div")
    WebElement road;
    @FindBy(xpath = "//button[@value='Ocean']/span/div")
    WebElement ocean;
    @FindBy(id ="Rectangle")
    WebElement hotItem;
    @FindBy(id="Shape")
    WebElement temprature;
    @FindBy(xpath = "//*[@id='toolBar']//*[@id='select-option']/../div")
    private WebElement materialTypeDropdown;
    //@FindBy (xpath = "//*[@id='menu-']//li[@role='option'][@data-value='Finished Goods']")
    @FindBy (xpath ="//li[@data-value ='Finished Goods']/p")
    private WebElement finishedGoodsMaterialType;
    @FindBy (xpath = "//ul[@role='listbox']//li[@data-value='All Types']")
    private WebElement allTypes;

    //Webelements for Pagination feature
    @FindBy(xpath ="//*[@id=\"toolBar\"]")
    //@FindBy(className = "css-1t5gla3")
    private WebElement pagination;
   @FindBy(xpath = "(//button[@class='css-1nhbps'])[3]")
    //@FindBy(xpath = "//*[@id='toolBar']/div/div[2]/div/div/button[3]")
    private WebElement nextPageButton;
    @FindBy(xpath = "(//button[@class='css-1nhbps'])[4]")
    // @FindBy(xpath = "//*[@id='toolBar']/div/div[2]/div/div/button[4]")
    private WebElement nextPageDisabledButton;
     @FindBy(xpath = "//div[@class='css-s7xf50']")
    //@FindBy(className = "css-s7xf50")
    private WebElement currentPage;
    @FindBy(xpath = "(//button[@class='css-1nhbps'])[1]")
    //@FindBy(xpath ="//*[@id=\"toolBar\"]/div/div[2]/div/div/button[1]")
    private WebElement previousPageButton;
    @FindBy(xpath = "(//button[@class='css-1nhbps'])[2]")
    //@FindBy(xpath = "(//button[@class='css-1nhbps'])[2]")
    private WebElement previousPageDisabledButton;
    //get the total no of pages
    @FindBy(xpath ="//div[contains(@id,'PanelSummary-RenderedItem-Participant')]")
    private List<WebElement> totalPagesCount;
    @FindBy(xpath = "//*[@id=\"toolBar\"]/div/div[2]/div/div/div")
    private WebElement totalRecords;
    @FindBy(xpath = "//*[@id=\"toolBar\"]/div/div[2]/div/div/button[4]")
    private WebElement lastPage;
    @FindBy(xpath = "(//div[contains(@class,'headerSummaryContainer')])[1]")
    private WebElement itemNumber;
    @FindBy(xpath = "//*[@id='Download-View']/../..")
    private WebElement downloadButton;
    @FindBy(xpath = "//*[@id='Upload-View']/../..")
    private WebElement uploadButton;
    @FindBy(xpath = "//*[@id='upload-dialog']//button/span[text()='Choose File']")
    private WebElement uploadChooseFileButton;
    @FindBy(xpath = "//*[@id='pagination-page-info']")
    private WebElement paginationInfo;
    @FindBy(xpath = "//*[@id='upload-dialog']//div[@id='dropzone']/following::button/span[1]")
    private WebElement uploadFileButtonOnUploadFilePopup;


    public int getTotalRecords() {
        System.out.println("total records:"+ totalRecords.getText());
        String[] rec = totalRecords.getText().split("of");
        //converting string to int to get last page number
        int rec1 = Integer.parseInt(rec[1].trim());
        return rec1;
    }

    //click last page
    public void gotoLastPage() {
        //this.getLastPage().click();
        waitHelper.waitAndClickElement(lastPage, "Last PagePagination");
    }


    //validation method
    public void validatePagination() {
        System.out.println("Inside pagination method");
        int pagecount = getTotalRecords();

        System.out.println("Pagination count: " + pagecount);
        if (pagecount >= 75) {
            System.out.println("Validating first page");
            waitHelper.waitElementToBeVisible(previousPageDisabledButton);
            Assert.assertTrue(!previousPageDisabledButton.isEnabled());
            waitHelper.waitElementToBeVisible(nextPageButton);
            Assert.assertTrue(nextPageButton.isEnabled());
            waitHelper.waitElementToBeVisible(totalRecords);
            Assert.assertTrue(totalRecords.isDisplayed());
  //verify that a page is displayed with shipment/item numbers
            waitHelper.waitElementToBeVisible(itemNumber);
            Assert.assertTrue(itemNumber.isDisplayed());

  //click next page
            waitHelper.waitElementToBeClickable(nextPageButton);
            nextPageButton.click();

            System.out.println("Validating second page");
            waitHelper.waitElementToBeVisible(previousPageButton);
            try {
                Assert.assertTrue(previousPageButton.isEnabled());
            } catch (StaleElementReferenceException e) {//checks second time --xpath
                Assert.assertTrue(previousPageButton.isEnabled());
            }
            waitHelper.waitElementToBeVisible(nextPageButton);
            Assert.assertTrue(nextPageButton.isEnabled());
            waitHelper.waitElementToBeVisible(currentPage);
            Assert.assertTrue(currentPage.isDisplayed());
//verify that a page is displayed with item numbers
            webDriverWait.until(ExpectedConditions.visibilityOf(itemNumber));
            //Assert.assertTrue(itemContainer.isDisplayed());

//go to last page and click
            this.gotoLastPage();
            System.out.println("Validating last page");
            waitHelper.waitElementToBeVisible(previousPageButton);
            Assert.assertTrue(previousPageButton.isEnabled());
            waitHelper.waitElementToBeVisible(nextPageDisabledButton);
            Assert.assertTrue(nextPageDisabledButton.isDisplayed());//button should not be clickable
            waitHelper.waitElementToBeVisible(totalRecords);
            Assert.assertTrue(totalRecords.isDisplayed());
//verify that a page is displayed with item numbers
            waitHelper.waitElementToBeVisible(itemNumber);
            //Assert.assertTrue(itemContainer.isDisplayed());

        } else if (pagecount <= 25) {
            waitHelper.waitElementToBeVisible(previousPageDisabledButton);
            Assert.assertTrue(!previousPageDisabledButton.isEnabled());
            waitHelper.waitElementToBeVisible(nextPageDisabledButton);
            Assert.assertTrue(!nextPageDisabledButton.isEnabled());
            waitHelper.waitElementToBeVisible(totalRecords);
            Assert.assertTrue(totalRecords.isDisplayed());
 //verify that a page is displayed with item numbers
            waitHelper.waitElementToBeVisible(itemNumber);
            Assert.assertTrue(itemNumber.isDisplayed());

        } else if (pagecount >=50 && pagecount<75) {
            waitHelper.waitElementToBeVisible(previousPageDisabledButton);
            Assert.assertTrue(!previousPageDisabledButton.isEnabled());
            waitHelper.waitElementToBeVisible(nextPageButton);
            Assert.assertTrue(nextPageButton.isEnabled());
            waitHelper.waitElementToBeVisible(totalRecords);
            Assert.assertTrue(totalRecords.isDisplayed());
//verify that a page is displayed with item numbers
            waitHelper.waitElementToBeVisible(itemNumber);
            Assert.assertTrue(itemNumber.isDisplayed());
//move to last page and click
            this.gotoLastPage();
            waitHelper.waitElementToBeVisible(previousPageButton);
            Assert.assertTrue(previousPageButton.isEnabled());
            waitHelper.waitElementToBeVisible(nextPageDisabledButton);
            Assert.assertTrue(!nextPageDisabledButton.isEnabled());
            waitHelper.waitElementToBeVisible(totalRecords);
            Assert.assertTrue(totalRecords.isDisplayed());
            //verify that a page is displayed with item number
            waitHelper.waitElementToBeVisible(itemNumber);
            Assert.assertTrue(itemNumber.isDisplayed());

        }
    }

//search methods


    public void clickSearchButton() {
        waitHelper.waitAndClickElement(searchButton, "Search button");
    }

    //enter shipment number in search field
    public void goToSearchFeature(String shipmentNo)throws InterruptedException {
        waitHelper.waitElementToBeClickable(searchInputField);
        genericHelper.setElementText(searchInputField, "Search Input Field", shipmentNo);
        waitHelper.waitElementToBeClickable(searchInputField);
        searchInputField.sendKeys(Keys.ENTER);
        Thread.sleep(8000);
    }

    //////get auto-suggestion when text is entered
    public boolean getAutoSuggestionForAnySearchDataEntered(String searchData) throws InterruptedException {
        webDriverWait.until(ExpectedConditions.visibilityOf(searchInputField));
        searchInputField.sendKeys(searchData);

        Thread.sleep(3000);
        searchInputField.click();

        List<WebElement> autoList = autoSuggestionListDisplay;
        System.out.println("Size is" + autoList.size());
        if (autoList.size() == 0) {
            searchInputField.click();
        }
        if (autoList.size() > 0) {
            for (WebElement auto : autoList) {

                if (!auto.getText().toLowerCase().contains(searchData.toLowerCase())) {
                    System.out.println("Search String '" + searchData + "' is not found in displayed auto suggested results '" + auto.getText() + "'");
                    return false;
                }
            }
        }
        return true;
    }


    public boolean isAutoSuggestionListDisplayed() {
        webDriverWait.until(ExpectedConditions.visibilityOf(autoSuggestionListDisplay.get(0)));
        return autoSuggestionListDisplay.get(0).isDisplayed();
    }

    public void clickDemandOrderSliderIcon() throws InterruptedException {
        Thread.sleep(5000);
        waitHelper.waitAndClickElement(demandOrdersnextIcon, "Next Icon");
    }

    public void clickRoadIcon() {
        waitHelper.waitAndClickElement(road, "Road");
    }

    public void clickOceanIcon() {
        objWaitHelper.waitAndClickElement(ocean, "Ocean");
    }

    public void clickHotItem() {
        String fill = hotItem.getCssValue("fill");
        System.out.println(fill);
        if (fill.equalsIgnoreCase("rgb(255, 255, 255)")) {
            objWaitHelper.waitAndClickElement(hotItem, "clicked hot item");
        }
        }


    public void clickTempratureControl() {
        String fill = hotItem.getCssValue("fill");
        System.out.println(fill);
        if (fill.equalsIgnoreCase("rgb(255, 255, 255)")) {
            objWaitHelper.waitAndClickElement(temprature, "clickedtemprature");
        }
    }

    public void clickMaterialGroupDropdown(){
        waitHelper.waitAndClickElement(materialTypeDropdown, "Material Group dropdown");
    }

    public void clickFinishedGoodsMaterialType() {
        waitHelper.waitAndClickElement(finishedGoodsMaterialType, "Finished Goods Material Goods");
    }

    public void clickAllTypes() {
        waitHelper.waitElementToBeClickable(allTypes);
        genericHelper.elementClick(allTypes, "All Types Option");
    }

    public AdvancedFilterMenuPage openAdvancedFilterMenu() {
        waitHelper.waitAndClickElement(advancedFilterButton, "Advanced Filter Button");
        return new AdvancedFilterMenuPage(driver);
    }

    public boolean clickDownloadButton(){
        return objGenericHelper.elementClick(downloadButton, "Download Button");
    }

    public boolean clickUploadButton(){
        return objGenericHelper.elementClick(uploadButton, "Upload Button");
    }

    public boolean clickUploadChooseFileButton(){
        return objGenericHelper.elementClick(uploadChooseFileButton, "Choose file button");
    }

    public long getNoOfRecordsOnUIWithAppliedFilter(){
        String paginationInfoText = paginationInfo.getText();
        List paginationInfoTextList = Arrays.asList(paginationInfoText.split(" "));
        long countOfRecords = Long.valueOf(paginationInfoTextList.get(paginationInfoTextList.size()-1).toString());
        Reporter("Count of records in UI is " + countOfRecords, "Pass");
        return countOfRecords;
    }

    public void clickUploadFileButtonOnUploadFilePopUp(){
        objGenericHelper.elementClick(uploadFileButtonOnUploadFilePopup, "Upload File button");
    }
}
