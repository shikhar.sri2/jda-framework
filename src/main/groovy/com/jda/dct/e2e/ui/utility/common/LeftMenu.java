package com.jda.dct.e2e.ui.utility.common;

import com.jda.dct.e2e.ui.utility.helper.Action.ActionHelper;
import com.jda.dct.e2e.ui.utility.helper.Browser.BrowserHelper;
import com.jda.dct.e2e.ui.utility.helper.Javascript.JavaScriptHelper;
import com.jda.dct.e2e.ui.utility.helper.Wait.WaitHelper;
import com.jda.dct.e2e.ui.utility.helper.genericHelper.GenericHelper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/*
This lists actions corresponding to left menu
 */
public class LeftMenu extends BasePage {


    WebDriverWait webDriverWait;
    WebDriver driver;

    WaitHelper objWaitHelper;
    GenericHelper objGenericHelper;
    ActionHelper actionHelper;
    JavaScriptHelper javaScriptHelper;
    BrowserHelper browserHelper;

    @FindBy(id = "menu-navigation-icon")
    //@FindBy(xpath ="//*[@id=\"app\"]/div/div[2]/div[1]/div/header/div/div/div[1]/button/span[1]")
    private WebElement topLeftMenu;
    @FindBy(id = "NavPanel-Item-SUPPLY")
    private WebElement supplyMenuOption;
    //@FindBy(xpath = "//span[text()='DEMAND']")
    @FindBy(id = "NavPanel-Item-DEMAND")
    private WebElement demandMenuOption;
    @FindBy(id ="NavPanel-Item-INVENTORY")
    private WebElement Inventory;
    @FindBy(id ="NavPanel-Item-MASTER DATA")
    private WebElement masterDataMenuOption;
    @FindBy(id = "NavPanel-Item-ANALYTICS")
    private WebElement analyticsMenuOption;
    //@FindBy(xpath = "//span[text()='INSIGHTS']")
    @FindBy(id = "SideBar-Item-Insights")
    private WebElement insightMenuOption;
    //@FindBy(xpath = "//span[text()='LOGOUT']")
    @FindBy(id = "NavPanel-Action-Logout")
    private WebElement logoutMenuOption;
    @FindBy(id = "NavPanel-Item-INVENTORY")
    private WebElement inventoryMenuOption;
    @FindBy(xpath = "//*[@id='NavPanel-Item-DOCUMENTS']")
    private WebElement documentsMenuOption;
    @FindBy(xpath = "//img[@alt='Watch Tower Icon']/../div[contains(text(), 'JDA')]")
    private WebElement jdaSoftwareIcon;

    public LeftMenu(WebDriver webdriver) {

        this.driver = webdriver;
        PageFactory.initElements(driver, this);
        webDriverWait = new WebDriverWait(driver, 60);
        objWaitHelper = new WaitHelper(driver);
        objGenericHelper = new GenericHelper(driver);
        actionHelper = new ActionHelper(driver);
        javaScriptHelper = new JavaScriptHelper(driver);
        browserHelper = new BrowserHelper(driver);
    }

    ////Methods to click on left menu options
    public void clickTopLeftMenu() {
        objWaitHelper.waitElementToBeClickable(topLeftMenu);
        objGenericHelper.elementClick(topLeftMenu, "Top Left Menu");
    }

    public void clickSupplyMenuOption() {
        clickTopLeftMenu();
        objWaitHelper.waitElementToBeClickable(supplyMenuOption);
        objGenericHelper.elementClick(supplyMenuOption, "Supply Menu");
        // Thread.sleep(3000);
    }

    public boolean verifyInvisibiltyOfSupplyMenuOptionWebElement() {
        if (objGenericHelper.checkInvisbilityOfElement(supplyMenuOption, "supply Menu option") == true) {
            return true;
        } else {
            driver.navigate().refresh();
            return objGenericHelper.checkInvisbilityOfElement(supplyMenuOption, "supply Menu option");
        }
    }

    public void clickDemandMenuOption() {
        clickTopLeftMenu();
        objWaitHelper.waitAndClickElement(demandMenuOption, "Demand Option");
    }

    public void clickInventoryMenuOption() throws InterruptedException { //ua
        clickTopLeftMenu();
        objWaitHelper.waitAndClickElement(inventoryMenuOption, "Inventory Page Selected");
    }


    public void clickDocumentsMenuOption() {
        clickTopLeftMenu();
        objWaitHelper.waitElementToBeClickable(documentsMenuOption);
        objGenericHelper.elementClick(documentsMenuOption, "Documents Menu");
        // Thread.sleep(3000);
    }


    public void clickMasterDataMenuOption() throws InterruptedException {
        clickTopLeftMenu();
/*        actionHelper.moveToElement(masterDataMenuOption, "Master Data");
        Thread.sleep(3000);
        objWaitHelper.waitAndClickElement(masterDataMenuOption, "Master Data");*/
        //Temporary Code
        browserHelper.navigateTo("https://" + properties.getProperty("client.url")+ "/master-data/items");
    }

    public void clickAnalyticsMenuOption() throws InterruptedException {
        clickTopLeftMenu();
        webDriverWait.until(ExpectedConditions.elementToBeClickable(analyticsMenuOption));
        analyticsMenuOption.click();
        Thread.sleep(3000);
    }

    public void clickInsightsMenuOption() throws InterruptedException {
        clickTopLeftMenu();
        webDriverWait.until(ExpectedConditions.elementToBeClickable(insightMenuOption));
        insightMenuOption.click();
        Thread.sleep(3000);
    }

    public void clickLogoutMenuOption() throws InterruptedException {
        clickTopLeftMenu();
        webDriverWait.until(ExpectedConditions.elementToBeClickable(logoutMenuOption));
        logoutMenuOption.click();
        Thread.sleep(3000);
    }


    //Methods to verify if left menu options are displayed
    public boolean isSupplyMenuOptionDisplayed() {
        webDriverWait.until(ExpectedConditions.visibilityOf(supplyMenuOption));
        return supplyMenuOption.isDisplayed();
    }

    public boolean isDemandMenuOptionDisplayed() {
        webDriverWait.until(ExpectedConditions.visibilityOf(demandMenuOption));
        return demandMenuOption.isDisplayed();
    }


    public boolean isMasterDataMenuOptionDisplayed() {
        webDriverWait.until(ExpectedConditions.visibilityOf(masterDataMenuOption));
        return masterDataMenuOption.isDisplayed();
    }

    public boolean isaAnalyticsMenuOptionDisplayed() {
        webDriverWait.until(ExpectedConditions.visibilityOf(analyticsMenuOption));
        return analyticsMenuOption.isDisplayed();
    }

    public boolean isInsightMenuOptionDisplayed() {
        webDriverWait.until(ExpectedConditions.visibilityOf(insightMenuOption));
        return insightMenuOption.isDisplayed();
    }

    public boolean isLogoutMenuOptionDisplayed() {
        webDriverWait.until(ExpectedConditions.visibilityOf(logoutMenuOption));
        return logoutMenuOption.isDisplayed();
    }

}