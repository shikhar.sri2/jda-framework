package com.jda.dct.e2e.api.purchaseOrders

import com.jda.dct.e2e.api.categories.Smoke
import com.jda.dct.e2e.api.utility.IngestionHelper
import com.jda.dct.e2e.api.utility.QueryObject
import com.jda.dct.e2e.api.utility.QueryResultHandler
import org.junit.experimental.categories.Category
import spock.lang.Specification

class GetPObyFullTextSearchwithId extends Specification implements IngestionHelper {

    def setupSpec() {
        init()
    }

    @Category(Smoke)
    def "retrieve a purchase order by text search of the purchaseOrderId field"() {
        given: "Purchase order PO0024_CO"
        def purchaseOrderId = '"PO0024_CO"'

        when: "retrieve the purchase order by text search in the purchaseOrderId field"
        def queryObject = new QueryObject()
        queryObject.setType("purchaseOrders")
        queryObject.addFullTextQuery()
                .searchText(purchaseOrderId)
                .searchField("purchaseOrderId")
        queryObject.addQuery("purchaseOrderType", "standardPO")
        def response = QueryResultHandler.getQueryResultHandler().executeWithRetry(queryObject, 20, 2, 5, 1, 1)

        then: "the api should return a success status code and the response should contain only PO0024_CO"
        response.rawResponse.status == 200
        response.body.data.size() == 1
        purchaseOrderId.contains(response.body.data[0].purchaseOrderId)
    }
}
