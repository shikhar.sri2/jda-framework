package com.jda.dct.e2e.ui.demand;

import com.jda.dct.e2e.ui.utility.common.BaseTest;
import com.jda.dct.e2e.ui.utility.common.LeftMenu;
import com.jda.dct.e2e.ui.utility.pageobjects.*;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class DemandSortByTests extends BaseTest {

    private LeftMenu leftMenu;
    private TopMenuPage topMenu;
    private SortByPage sortBy;
    private DemandShipmentListPage demandShipmentListPage;
    private DemandOrderListPage demandOrderListPage;
    private DemandDeliveryListPage demandDeliveryListPage;
    private WebDriver driver;

    @BeforeClass
    public void setUp() throws InterruptedException {
        System.out.println("Starting tests for :" + this.getClass().getName());
//Initialize required objects
        driver = getDriver();
        leftMenu = new LeftMenu(driver);
        topMenu = new TopMenuPage(driver);
        sortBy = new SortByPage(driver);
        demandShipmentListPage = new DemandShipmentListPage(driver);
        demandOrderListPage = new DemandOrderListPage(driver);
        demandDeliveryListPage = new DemandDeliveryListPage(driver);
        //loginAndNavigateToSupplyShipmentLandingPage
       logInToApplication();
        leftMenu.clickDemandMenuOption();
    }


    @Test(priority = 1)
    public void validateIsAscendingDemandShipmentListDisplayed() throws InterruptedException {
        leftMenu.clickDemandMenuOption();
        topMenu.goToDemandShipmentTab();
        topMenu.clickExceptionsTab();
        topMenu.clickDemandShipmentExceptionsDelayed();
        sortBy.clickAscendingShipment();
        System.out.println("arranged in ASC sorting order...");
        sortBy.isAscendingShipmentListDisplayed();
    }
    @Test(priority = 2, retryAnalyzer = BaseTest.class)
    public void validateIsDescendingDemandShipmentListDisplayed() throws InterruptedException {
        leftMenu.clickDemandMenuOption();
        topMenu.goToDemandShipmentTab();
        topMenu.clickExceptionsTab();
        topMenu.clickDemandShipmentExceptionsDelayed();
        sortBy.clickDescendingShipment();
        System.out.println("arranged in DSC sorting order...");
        sortBy.isDescendingShipmentListDisplayed();


    }



}

