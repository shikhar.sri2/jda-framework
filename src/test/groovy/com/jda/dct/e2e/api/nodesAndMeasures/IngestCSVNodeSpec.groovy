package com.jda.dct.e2e.api.nodesAndMeasures

import com.jda.dct.e2e.api.categories.Regression
import com.jda.dct.e2e.api.utility.IngestionHelper
import org.junit.experimental.categories.Category
import spock.lang.Shared
import spock.lang.Specification

/**
 * Ingest node data in CSV format.
 *
 * @author Vida Bandara
 * @version 1.0-SNAPSHOT
 * @since 11-May-2018
 */

class IngestCSVNodeSpec extends Specification implements IngestionHelper {

    @Shared nodeURL
    @Shared measureURL

    def setupSpec() {
        init()

        def nodePath = properties.getProperty('nodePostPath')
        def measurePath = properties.getProperty('measurePath')
        nodeURL = protocol + ip + nodePath + '?processType=demand&nodeType=fulfillment'
        measureURL = protocol + ip + measurePath + '?processType=demand&nodeType=fulfillment'
    }
/*
    @Category(Regression)
    def "create and ingest a faulty measure csv"() {
        given:
        def filePath = getTmpDir() + 'sample_measure.csv'
        new File(filePath).newWriter().withWriter {
            it << 'Process Type,LOB,Customer,Supplier,Ship To,Ship From,Customer Item,Item,Item Name,Material Group,Measure,Geo,Date,Quantity\n' +
                    'demand,Telecommunications,DEMOCUST,DEMOSUPP,USD1,CH01,MB_PH_BK_64,MBPHB964BK,Mb phone B9 64GB -Black,Mobile Phones,Forecast,Central,4/30/18,782\n' +
                    'demand,Telecommunications,DEMOCUST,DEMOSUPP,USD1,CH01,MB_PH_BK_64,MBPHB964BK,Mb phone B9 64GB -Black,Mobile Phones,Forecast,Central,5/7/18,996\n' +
                    'demand,Telecommunications,DEMOCUST,DEMOSUPP,USD1,CH01,MB_PH_BK_64,MBPHB964BK,Mb phone B9 64GB -Black,Mobile Phones,Forecast,Central,xyz,996\n' +
                    'demand,Telecommunications,DEMOCUST,DEMOSUPP,USD1,CH01,MB_PH_BK_64,MBPHB964BK,Mb phone B9 64GB -Black,Mobile Phones,Forecast,Central,5/7/18,996\n'
        }

        when:
        def response = postFile(filePath, measureURL, null)
        def ingestionId = getIngestionId(response)
        println "Response post ingestion is: " + response
        def status = waitUntilComplete(ingestionId)

        then:
        println "Status msg: " + status
        status.contains('COMPLETED_WITH_ERROR')
        println "Errors returned: " + getErrors(ingestionId)
        getErrors(ingestionId) == [
                [
                        'rowId': 3,
                        'errorCode': 'dct.io.castError',
                        'errorMessage': 'Date:xyz cannot be cast to date.'
                ]
        ]
    }
*/
    /*def "ingest a large faulty measure CSV file"() {
        given:
        def numCustomers = 20
        def numItems = 10
        def nodeFile = getTmpDir() + 'sample_node.csv'
        new File(nodeFile).newWriter().withWriter { writer ->
            writer << 'Process Type,LOB,Customer,Supplier,Status,Ship To,Ship From,Customer Item,Item,Item Name,Material Group\n'
            def templateLine = 'demand,Telecommunications,<customer>,DEMOSUPP,Active,USD1,CH01,<item>,MBPHB964BK,Mb phone B9 64GB -Black,Mobile Phones\n'
            numCustomers.times { customer ->
                numItems.times { item ->
                    writer << templateLine
                            .replaceAll('<customer>', 'CUST' + (customer + 1))
                            .replaceAll('<item>', 'ITEM' + (item + 1))
                }
            }
        }

        def measureFile = getTmpDir() + 'sample_measure.csv'
        new File(measureFile).newWriter().withWriter { writer ->
            writer << 'Process Type,LOB,Customer,Supplier,Ship To,Ship From,Customer Item,Item,Item Name,Material Group,Measure,Geo,Date,Quantity\n'
            def templateLine = 'demand,Telecommunications,<customer>,DEMOSUPP,USD1,CH01,<item>,MBPHB964BK,Mb phone B9 64GB -Black,Mobile Phones,Forecast,Central,4/30/18,782\n'
            (numCustomers-1).times { customer ->
                numItems.times { item ->
                    writer << templateLine
                            .replaceAll('<customer>', 'CUST' + (customer + 1))
                            .replaceAll('<item>', 'ITEM' + (item + 1))
                }
            }
            //faulty line
            writer << templateLine
                    .replaceAll('<customer>', 'INVALID_CUSTOMER')
                    .replaceAll('<item>', 'INVALID_ITEM')
        }

        when:
        def nodeResponse = postFile(nodeFile, nodeURL, null)
        def nodeIngestionId = getIngestionId(nodeResponse)
        def nodeStatus = waitUntilComplete(nodeIngestionId,"ingesting:$nodeIngestionId", 30)

        def measureResponse = postFile(measureFile, measureURL, null)
        def measureIngestionId = getIngestionId(measureResponse)
        def measureStatus = waitUntilComplete(measureIngestionId,"ingesting:$measureIngestionId",30)

        then:
        with(nodeStatus) {
            contains('COMPLETED')
            !contains('COMPLETED_WITH_ERROR')
        }
        with(measureStatus) {
            contains('COMPLETED_WITH_ERROR')
        }

        getStatusObjects(nodeIngestionId).find { it.get('status') == 'COMPLETED' }
                .get('numProcessed') == 200
        //only last batch will fail
        getStatusObjects(measureIngestionId).find { it.get('status') == 'COMPLETED_WITH_ERROR' }
                .get('numProcessed') == 190
    }*/
}
