package com.jda.dct.e2e.api.utility

import com.jda.dct.domain.Node
import com.jda.dct.e2e.ui.utility.PropertyFile
import org.apache.commons.lang3.StringUtils
import org.springframework.http.HttpMethod
import org.springframework.util.CollectionUtils

import java.nio.charset.Charset
import java.security.InvalidParameterException
import java.text.MessageFormat

class Initialize extends PropertyFile implements UsesMockNodeData, TestHelper {

    def PROTOCOL
    def IP
    def WEBURL
    def oauthUrl
    def nodeGetUrl
    def nodePostUrl
    def ingestionStatusUrl
    def measurePostUrl

    def tid
    def tenantAdminClientId
    def tenantAdminClientSecret
    def integratorClientId
    def integratorClientSecret

    def tid_t2
    def tenantAdminClientId_t2
    def tenantAdminClientSecret_t2
    def integratorClientId_t2
    def integratorClientSecret_t2
    List<Node> mockNodeData

    /**
     * Reads properties from the config anf application files from the resource folder
     * @return
     */
    def initConfig() {
        Properties properties = getProperties()
        def endpointFile = new File('src/test/resources/endpoint.properties')

        PROTOCOL = properties.get('server.protocol')
        IP = properties.get('server.url')
        WEBURL =  properties.getProperty('client.url')

        tid = properties.get('tid')
        tenantAdminClientId = properties.get('tenantAdminClientId')
        tenantAdminClientSecret = properties.get('tenantAdminClientSecret')
        integratorClientId = properties.get('integratorClientId')
        integratorClientSecret = properties.get('integratorClientSecret')

        tid_t2 = properties.get('tid_t2')
        tenantAdminClientId_t2 = properties.get('tenantAdminClientId_t2')
        tenantAdminClientSecret_t2 = properties.get('tenantAdminClientSecret_t2')
        integratorClientId_t2 = properties.get('integratorClientId_t2')
        integratorClientSecret_t2 = properties.get('integratorClientSecret_t2')

        properties.load(endpointFile.newDataInputStream())
        oauthUrl = properties.get('oauthPath')
        nodeGetUrl = properties.get('nodeGetPath')
        nodePostUrl = properties.get('nodePostPath')
        ingestionStatusUrl = properties.get('ingestionStatusPath')
        measurePostUrl = properties.get('measurePath')
    }

    /**
     * Gets the access token in order to access any of the resources.
     * @return
     */
    def getAccessToken(asIntegrator) {
        def url = (PROTOCOL + IP + oauthUrl + tid).toString()
        def bytes = (asIntegrator ? "$integratorClientId:$integratorClientSecret" :
                "$tenantAdminClientId:$tenantAdminClientSecret").getBytes(Charset.forName('US-ASCII'))
        def encoder = Base64.encoder
        def credential = encoder.encodeToString(bytes)
        def auth = "Basic $credential"
        def headers = getHeader('Authorization', auth)
        def responseJson = executeHttpCall(headers, url, null, HttpMethod.GET)
        def uploadToken = responseJson.access_token

        uploadToken
    }

    /**
     * Gets the access token in order to access any of the resources with tenant id provided.
     * @return
     */
    def getAccessTokenWithTid(tid, asIntegrator) {
        Map<String, Map<String, String>> tenantsDetails = multiTenantInitConfig()
        Map<String, String> curTenantDetail = null
        for (String tenantId : tenantsDetails.keySet()) {
            if (!StringUtils.isBlank(tenantId) && tenantId.equals(tid)) {
                curTenantDetail = tenantsDetails.get(tenantId)
            }
        }
        if (curTenantDetail == null || curTenantDetail.size() == 0) {
            throw new InvalidParameterException(MessageFormat.format("Missing client id, client secret for tenant {0}!", tid))
        }
        def integratorClientId = curTenantDetail.get("integratorClientId")
        def integratorClientSecret = curTenantDetail.get("integratorClientSecret")
        def tenantAdminClientId = curTenantDetail.get("tenantAdminClientId")
        def tenantAdminClientSecret = curTenantDetail.get("tenantAdminClientSecret")

        def url = (PROTOCOL + IP + oauthUrl + tid).toString()
        def bytes = (asIntegrator ? "$integratorClientId:$integratorClientSecret" :
                "$tenantAdminClientId:$tenantAdminClientSecret").getBytes(Charset.forName('US-ASCII'))
        def encoder = Base64.encoder
        def credential = encoder.encodeToString(bytes)
        def auth = "Basic $credential"
        def headers = getHeader('Authorization', auth)
        def responseJson = executeHttpCall(headers, url, null, HttpMethod.GET)
        def uploadToken = responseJson.access_token

        uploadToken
    }

    Map<String, Map<String, String>> multiTenantInitConfig(){
        Map<String, Map<String, String>> tenantsDetails = new HashMap<>()

        Map<String, String> tenantDetail = new HashMap<>()
        tenantDetail.put("tid", tid)
        tenantDetail.put("tenantAdminClientId", tenantAdminClientId)
        tenantDetail.put("tenantAdminClientSecret", tenantAdminClientSecret)
        tenantDetail.put("integratorClientId", integratorClientId)
        tenantDetail.put("integratorClientSecret", integratorClientSecret)

        Map<String, String> tenant2Detail = new HashMap<>()
        tenant2Detail.put("tid", tid_t2)
        tenant2Detail.put("tenantAdminClientId", tenantAdminClientId_t2)
        tenant2Detail.put("tenantAdminClientSecret", tenantAdminClientSecret_t2)
        tenant2Detail.put("integratorClientId", integratorClientId_t2)
        tenant2Detail.put("integratorClientSecret", integratorClientSecret_t2)

        tenantsDetails.put(tid, tenantDetail)
        tenantsDetails.put(tid_t2, tenant2Detail)
        return tenantsDetails
    }

    /**
     * Generates mock data
     * @return
     */
    def generateData() {
        someMockNodeData(
                seed: System.currentTimeMillis(),
                numNodes: 1,
                tids: [tid]
        )
    }

    def getMeasurePostUrl() {
        return measurePostUrl
    }

    void setMeasurePostUrl(measurePostUrl) {
        this.measurePostUrl = measurePostUrl
    }

    def getClientId() {
        return clientId
    }

    void setClientId(clientId) {
        this.clientId = clientId
    }

    def getClientSecret() {
        return clientSecret
    }

    void setClientSecret(clientSecret) {
        this.clientSecret = clientSecret
    }

    List<Node> getMockNodeData() {
        return mockNodeData
    }

    void setMockNodeData(List<Node> mockNodeData) {
        this.mockNodeData = mockNodeData
    }

    def getProtocol() {
        return PROTOCOL
    }

    void setPROTOCOL(PROTOCOL) {
        this.PROTOCOL = PROTOCOL
    }

    def getIP() {
        return IP
    }

    void setIP(IP) {
        this.IP = IP
    }

    def getWebURL() {
        return WEBURL
    }

    void setWebURL(webURL) {
        this.WEBURL = webURL
    }

    def getOauthUrl() {
        return oauthUrl
    }

    void setOauthUrl(oauthUrl) {
        this.oauthUrl = oauthUrl
    }

    def getNodeGetUrl() {
        return nodeGetUrl
    }

    void setNodeGetUrl(nodeGetUrl) {
        this.nodeGetUrl = nodeGetUrl
    }

    def getNodePostUrl() {
        return nodePostUrl
    }

    void setNodePostUrl(nodePostUrl) {
        this.nodePostUrl = nodePostUrl
    }

    def getIngestionStatusUrl() {
        return ingestionStatusUrl
    }

    void setIngestionStatusUrl(ingestionStatusUrl) {
        this.ingestionStatusUrl = ingestionStatusUrl
    }

    def getTid() {
        return tid
    }

    void setTid(tid) {
        this.tid = tid
    }

    static void main(String[] args) {
        new Initialize().getAccessToken(false)
    }
}
