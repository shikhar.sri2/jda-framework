package com.jda.dct.e2e.api.shipments

import com.fasterxml.jackson.databind.ObjectMapper
import com.jda.dct.domain.stateful.Shipment
import com.jda.dct.e2e.api.categories.Regression
import com.jda.dct.e2e.api.utility.IngestionHelper
import groovy.json.JsonOutput
import groovy.json.JsonSlurper
import org.junit.experimental.categories.Category
import org.springframework.http.HttpMethod
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Stepwise
import spock.util.concurrent.PollingConditions

/**
 * End-to-end tests for {@code Shipment} ingestion.
 *
 * @author Vida Bandara
 * @version 1.0-SNAPSHOT
 * @since 24-Apr-2018
 */
@Stepwise
class IngestShipmentsSpec extends Specification implements IngestionHelper {

    @Shared
            sampleSiteForAlert,
            sampleShipmentDelayed,
            sampleShipmentOnTime,
            sampleShipmentLate,
            sampleShipmentReceived,
            sampleShipmentEarly,
            sampleShipmetnPtaUnknown

    def setupSpec() {
        init()

        def sitePath = properties.getProperty('ingestSitesPath')
        def shipmentPath = properties.getProperty('ingestShipmentsPath')
        def getPath = properties.getProperty('getShipmentsPath')
        def searchAlertPath = properties.get("searchAlertPath")

        siteIngestionUrl = protocol + ip + sitePath

        ingestionURL = protocol + ip + shipmentPath
        getURLPrefix = protocol + ip + getPath
        searchAlertUrlPrefix = protocol + ip + searchAlertPath

        sampleShipment = this.getClass().getClassLoader().getResource('sample-shipment-1.json').text
        sampleSiteWithDesc = this.getClass().getClassLoader().getResource('sample-site-with-desc.json').text
        sampleShipmentNoDesc = this.getClass().getClassLoader().getResource('sample-shipment-no-desc.json').text
        sampleShipmentMissingReqExtField = this.getClass().getClassLoader().getResource('sample-shipment-missing-required-ext-field.json').text
        sampleSiteForAlert = this.getClass().getClassLoader().getResource('sample-site-for-alert-testing-1.json').text
        sampleShipmentDelayed = this.getClass().getClassLoader().getResource('sample-shipment-alert-delayed.json').text
        sampleShipmentOnTime = this.getClass().getClassLoader().getResource('sample-shipment-alert-on-time.json').text
        sampleShipmentLate = this.getClass().getClassLoader().getResource('sample-shipment-alert-late.json').text
        sampleShipmentReceived = this.getClass().getClassLoader().getResource('sample-shipment-alert-late-received.json').text
        sampleShipmentEarly = this.getClass().getClassLoader().getResource('sample-shipment-alert-early.json').text
        sampleShipmetnPtaUnknown = this.getClass().getClassLoader().getResource('sample-shipment-alert-pta-unknown.json').text
    }

    @Category(Regression)
    def "Ingestion status of an invalid shipment becomes complete with error"() {
        given:
        def conditions = new PollingConditions(timeout: 120, initialDelay: 1)
        def modifiedSampleShipment = sampleShipment.replaceAll("\"shipmentType\": \"inboundShipment\"", "\"shipmentType\": \"wrong-shipment-type\"")

        when: 'a shipment json ingested'
        def response = executeHttpCall(header, ingestionURL, modifiedSampleShipment, HttpMethod.POST)
        def ingestionId = response.ingestionId

        then: 'ingestion status eventually becomes COMPLETED'
        conditions.eventually {
            def status = getStatus(ingestionId)
            assert status.contains('COMPLETED_WITH_ERROR')
        }
    }

//    def "Shipment with missing mandatory freightCost ext field becomes complete with error"() {
//        given:
//        def conditions = new PollingConditions(timeout: 120, initialDelay: 1)
//
//        when: 'the shipment json with missing mandatory freightCost field is ingested'
//        def response = executeHttpCall(header, ingestionURL, sampleShipmentMissingReqExtField, HttpMethod.POST)
//        def ingestionId = response.ingestionId
//
//        then: 'ingestion status eventually becomes COMPLETED'
//        conditions.eventually {
//            def status = getStatus(ingestionId)
//            assert status.contains('COMPLETED_WITH_ERROR')
//            getErrors(ingestionId) == [
//                    [
//                            'rowId'       : 0,
//                            'errorMessage': 'Failed streaming objects. message:Mandatory field:freightCost value is missing..'
//                    ]
//            ]
//        }
//    }

//    def "Shipment with no site description gets filled in based on site names"() {
//        given: 'A site with a description is ingested'
//
//        ingestData(sampleSiteWithDesc, siteIngestionUrl)
//
//        when: "A shipment is ingested that does not contain a description"
//
//        ingestData(sampleShipmentNoDesc)
//
//        def shipmentIds = getShipmentIds(sampleShipmentNoDesc)
//        def url = getURLPrefix + shipmentIds[0]
//        def shipmentText = getHttpClient(header, url, null, HttpMethod.GET).body
//        def shipmentRetrieved = new JsonSlurper().parseText(shipmentText).get('data').get(0)
//
//        def mapper = new ObjectMapper()
//        def received = mapper.readValue(groovy.json.JsonOutput.toJson(shipmentRetrieved), Shipment)
//
//        def shipmentLine1 = received.getShipmentLines().get("line 1")
//        def shipmentLine2 = received.getShipmentLines().get("line 2")
//
//        then:
//        shipmentLine1.getExtFields().get("shipToSiteDescription") == "Description to be copied"
//        shipmentLine1.getExtFields().get("shipFromSiteDescription") == "Description to be copied"
//        shipmentLine2.getExtFields().get("shipToSiteDescription") == "Description to be copied"
//        shipmentLine2.getExtFields().get("shipFromSiteDescription") == "Description to be copied"
//        received.getValue("shipToSiteDescription") == "Description to be copied"
//        received.getValue("shipFromSiteDescription") == "Description to be copied"
//        received.getValue("arrPortDesc") == "Arr Port Description to be copied"
//        received.getValue("depPortDesc") == "Dep Port Description to be copied"
//    }

    // evaluate and rename if necessary
    @Category(Regression)
    def "field in process model updates upon a new ingest"() {
        given: 'original shipment fetched'
        SampleShipmentsShowcaseData sample = new SampleShipmentsShowcaseData()
        Map<String, String> modifiedFields = new HashMap<>()

        when:
        modifiedFields.put("arrivalPort", "Dallas")
        def modifiedShipment = sample.modifyShipmentMultiFields("DSOS0100_SO_S2", modifiedFields, "temp-shipments.json")
        ingestData(modifiedShipment, "shipments")

        then: 'shipment arrival port are updated'
        def updatedShipment = sample.getShipmentsFromShowcase("DSOS0100_SO_S2")
        assert updatedShipment["arrivalPort"] == "Dallas"
    }

    // evaluate and rename if necessary
/*    @Category(Regression)
    def "field not in process model not updated upon a new ingest"() {
        given: 'original shipment fetched'
/*        ingestData(sampleShipment, "shipments")
        def shipmentIds = getShipmentIds(sampleShipment)
        def url = getURLPrefix + shipmentIds[0]

        when: 'ingest updated shipment'
        def modifiedSampleShipment = sampleShipment.replaceAll("\"flexFloat1\": 123.45", "\"flexFloat1\": 987.65")
        def expectedShipment = getJsonAt(sampleShipment, 0)

        ingestData(modifiedSampleShipment)

        def shipmentText = getHttpClient(header, url, null, HttpMethod.GET).body
        def shipmentRetrieved = new JsonSlurper().parseText(shipmentText).get('data').get(0)
        ['attributes', 'creationDate', 'id', 'lmd', 'maxDelayInHours', 'exceptionStatus', 'segment'].each {
            shipmentRetrieved.remove(it)
        }
        ['creationDate', 'lmd', 'delayHours', 'lineExceptionStatus', 'shipmentLineMilestones', 'segment'].each { key ->
            shipmentRetrieved.get('shipmentLines').values().each { it.remove(key) }
        }

        def mapper = new ObjectMapper()
        def expected = mapper.readValue(groovy.json.JsonOutput.toJson(expectedShipment), Shipment)
        def received = mapper.readValue(groovy.json.JsonOutput.toJson(shipmentRetrieved), Shipment)

        then: 'non-process model field not updated'

        System.out.println("Expected:" + expected.toString())
        System.out.println("Actual:" + received.toString())
        received == expected

        SampleShipmentsShowcaseData sample = new SampleShipmentsShowcaseData()
        Map<String, String> modifiedFields = new HashMap<>()

        when:
        modifiedFields.put("dummyField", "dummyData")
        def modifiedShipment = sample.modifyShipmentMultiFields("DSOS0100_SO_S2", modifiedFields, "temp-shipments.json")
        ingestData(modifiedShipment, "shipments")

        then: 'shipment arrival port are updated'
        def updatedShipment = sample.getShipmentsFromShowcase("DSOS0100_SO_S2")
        assert updatedShipment["dummyField"] != "dummyData"
    }
*/

/*
    @Category(Regression)
    def "exception status and max delay hours updated upon a new ingest"() {
        given: 'original shipment ingested'
        def shipmentId = getShipmentIds(sampleShipment)[0]
        def url = getURLPrefix + shipmentId
        deleteObject(getURLPrefix, shipmentId)
        ingestData(sampleShipment)

        when: 'ingest updated shipment'
        ingestData(sampleShipment)
        def shipmentText = getHttpClient(header, url, null, HttpMethod.GET).body
        def shipmentRetrieved = new JsonSlurper().parseText(shipmentText).get('data').get(0)

        then: 'exception status and max delay hours got updated'

        System.out.println("Actual exception status: " + shipmentRetrieved.get('exceptionStatus'))
        System.out.println("Actual delay hours: " + shipmentRetrieved.get('maxDelayInHours'))
        shipmentRetrieved.get('exceptionStatus') == 'delayed' || 'Delayed'
        shipmentRetrieved.get('maxDelayInHours') == 96.0
    }
*/
//    @Unroll
//    def "Ingest a late shipment which triggers alert service to generate alerts"() {
//        given: 'Shipment with exception status'
//        ingestData(sampleSiteForAlert, siteIngestionUrl)
//
//        when: 'Ingesting data'
//        ingestData(shipmentToIngest)
//        def shipmentIds = getShipmentIds(shipmentToIngest)
//
//        final Map<String, String> searchCriteria = new HashMap<>()
//        searchCriteria.put("limit", "1")
//        searchCriteria.put("query", "objectId=" + shipmentIds[0])
//
//        def responseDef =
//                searchResponseJson(searchCriteria, searchAlertUrlPrefix, state, 60)
//        then:
//        assert responseDef != null
//        def alertDef = responseDef.get("data").getAt(0)
//        assert alertDef.get("objectId") != null
//        assert alertDef.get("objectType") != null
//        assert alertDef.get("state") == state
//        assert alertDef.get("severity") == severity
//        assert alertDef.get("processType") == 'tempraturecontrolled'
//        assert alertDef.get("type") == 'Exception Status'
//        assert alertDef.get("exceptionStatus") == exceptionStatus
//        assert alertDef.get("objectState") == objectState
//        if (state == 'Open') {
//            assert alertDef.get("closedDate") == null
//        }
//        assert alertDef.get("alertStatusLmd") != null
//
//        def references = alertDef.get("refObjects")
//        if (!exceptionStatus.equalsIgnoreCase("PTA Unknown")) {
//
//            assert references != null
//
//            assert references.get("shipToSites") != null
//            references.get("shipToSites").each {
//                ref ->
//                    def participantName = ref.get("participantName")
//                    def siteName = ref.get("siteName")
//                    def siteOwnerName = ref.get("siteOwnerName")
//                    assert participantName.equals("Best Buys")
//                    assert siteName.equals("IT06") || siteName.equals("New York")
//                    assert siteOwnerName.equals("Best Buys IT06 Owner") || siteOwnerName.equals("Best Buys New York Owner")
//            }
//
//            assert references.get("shipFromSites") != null
//            references.get("shipFromSites").each {
//                ref ->
//                    def participantName = ref.get("participantName")
//                    def siteName = ref.get("siteName")
//                    def siteOwnerName = ref.get("siteOwnerName")
//                    assert participantName.equals("Samsung")
//                    assert siteName.equals("IT07") || siteName.equals("Cairo")
//                    assert siteOwnerName.equals("Samsung IT07 Owner") || siteOwnerName.equals("Samsung Cairo Owner")
//            }
//        }
//
//        where:
//        shipmentToIngest         | state    | severity | exceptionStatus | objectState
//        sampleShipmentLate       | 'Open'   | 4        | 'Late'          | 'Shipped'
//        sampleShipmentOnTime     | 'Closed' | 1        | 'On Time'       | 'Planned Shipment'
//        sampleShipmentLate       | 'Open'   | 4        | 'Late'          | 'Shipped'
//        sampleShipmentReceived   | 'Closed' | 4        | 'Late'          | 'Received'
//        sampleShipmentDelayed    | 'Open'   | 3        | 'Delayed'       | 'New'
//        sampleShipmentEarly      | 'Open'   | 2        | 'Early'         | 'Delivered'
//        sampleShipmetnPtaUnknown | 'Open'   | 5        | 'PTA Unknown'   | 'In Transit'
//    }

//    def "Should get ingested shipment back with scac code updated as part of preaction"() {
//        given: 'shipments ingested with carrier names'
//        def shipmentWithCarrier = this.getClass().getClassLoader().getResource('sample-shipment-preaction-missing-scac-update.json').text
//        def shipmentSent = getJsonAt(shipmentWithCarrier, 0)
//        ingestData(shipmentWithCarrier)
//        def shipmentIds = getShipmentIds(shipmentWithCarrier)
//        def url = getURLPrefix + shipmentIds[0]
//        def response = getHttpClient(header, url, null, HttpMethod.GET)
//        println 'response: ' + response
//        def shipmentText = response.body
//        def shipmentRetrieved = new JsonSlurper().parseText(shipmentText).get('data').get(0)
//        def mapper = new ObjectMapper()
//        def expected = mapper.readValue(groovy.json.JsonOutput.toJson(shipmentSent), Shipment)
//        def received = mapper.readValue(groovy.json.JsonOutput.toJson(shipmentRetrieved), Shipment)
//        def scacCode = (String) received.getExtField("scacCode")
//        println 'scac: ' + scacCode
//
//        expect:
//        expected == received
//    }
}
