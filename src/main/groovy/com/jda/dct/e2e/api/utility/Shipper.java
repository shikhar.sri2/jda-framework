package com.jda.dct.e2e.api.utility;
import java.util.List;
import java.util.Objects;

public class Shipper {
    private String id;
    private String name;
    private List subdomainConfigurations;
    private String stopLevelView;
    private String showLatestLocationInStopLevel;

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final Shipper that = (Shipper) o;
        return Objects.equals(id, that.id)
                && Objects.equals(name, that.name)
                && Objects.equals(subdomainConfigurations, that.subdomainConfigurations)
                && Objects.equals(stopLevelView, that.stopLevelView)
                && Objects.equals(showLatestLocationInStopLevel, that.showLatestLocationInStopLevel);
    }
    @Override
    public int hashCode() {
        return Objects.hash(id, name, subdomainConfigurations, stopLevelView, showLatestLocationInStopLevel);
    }

    @Override
    public String toString() {
        return "=Shipper{"
                + "id='" + id+ '\''
                + ", name='" + name + '\''
                + ", subdomainConfigurations='" + subdomainConfigurations + '\''
                + ", stopLevelView='" + stopLevelView + '\''
                + ", showLatestLocationInStopLevel='" + showLatestLocationInStopLevel + '\''
                + '}';
    }


}
