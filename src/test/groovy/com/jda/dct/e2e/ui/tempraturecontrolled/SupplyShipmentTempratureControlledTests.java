/* DCT-9570
package com.jda.dct.e2e.testscripts.uitests;

import com.jda.dct.e2e.qa.common.BaseTest;
import com.jda.dct.e2e.qa.common.LeftMenu;
import com.jda.dct.e2e.qa.helper.AssertHelper.AssertHelper;
import com.jda.dct.e2e.qa.helper.Browser.BrowserHelper;
import com.jda.dct.e2e.qa.pageobjects.*;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class SupplyShipmentTempratureControlledTests extends BaseTest {

private LeftMenu leftMenu;
private TopMenuPage topMenu;
private SupplyDeliveryListPage supplyDeliveryPage;
private SupplyShipmentListPage supplyShipmentListPage;
private SupplyShipmentDetailsPage supplyShipmentDetailsPage;
private TopSubMenuPage topSubMenu;
private BrowserHelper browserHelper;
AssertHelper assertHelper;
private WebDriver driver;
private DemoWorkFlow demoPage;

@BeforeClass
public void setUp() throws InterruptedException {
System.out.println("Starting tests for :" + this.getClass().getName());
//Initialize required objects
driver = getDriver();
leftMenu = new LeftMenu(driver);
topMenu = new TopMenuPage(driver);
topSubMenu = new TopSubMenuPage(driver);
supplyDeliveryPage = new SupplyDeliveryListPage(this.driver);
supplyShipmentListPage = new SupplyShipmentListPage(this.driver);
supplyShipmentDetailsPage = new SupplyShipmentDetailsPage(this.driver);
browserHelper = new BrowserHelper(driver);
assertHelper = new AssertHelper(driver);
demoPage = new DemoWorkFlow(this.driver);
logInToApplication();
}

@BeforeMethod
public void commonMethods() throws InterruptedException {
//navigates to Supply shipment tab
leftMenu.clickSupplyMenuOption();

}

@Test(priority = 1)
public void validateSupplyListPageIsFilteredByHotItems() throws InterruptedException {
String Shipment = "IS0042_SO_S2";
//leftMenu.clickSupplyMenuOption();
topMenu.goToSupplyShipmentsTab();
supplyShipmentListPage.clickOceanIcon();
topSubMenu.clickHotItem();
topSubMenu.clickMaterialGroupDropdown();
topSubMenu.clickAllTypes();
topSubMenu.clickSearchButton();
topMenu.clearFilterIfVisible();
topSubMenu.goToSearchFeature(Shipment);
supplyShipmentDetailsPage.clickSearchedShipmentNumber(Shipment);
assertHelper.assertTrue(demoPage.isHotItemsDisplayed(), "isHotItemDisplayed");
browserHelper.refresh();
}


@Test(priority = 2)
public void validateSupplyListPageIsFilteredByTempratureControlled() throws InterruptedException {
String Shipment = "IS0036_SD_S2";
//leftMenu.clickSupplyMenuOption();
topMenu.goToSupplyShipmentsTab();
supplyShipmentListPage.clickRoadIcon();
topSubMenu.clickTempratureControl();
topSubMenu.clickMaterialGroupDropdown();
topSubMenu.clickAllTypes();
topSubMenu.clickSearchButton();
topMenu.clearFilterIfVisible();
topSubMenu.goToSearchFeature(Shipment);
supplyShipmentDetailsPage.clickSearchedShipmentNumber(Shipment);
assertHelper.assertTrue(supplyShipmentListPage.isSupplyShipmentListPageDisplayed(),
"tempraturecontrolledItemDisplayed");
browserHelper.refresh();
}




}
*/
