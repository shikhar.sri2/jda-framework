package com.jda.dct.e2e.api.shipments

import com.jda.dct.e2e.api.categories.Smoke
import com.jda.dct.e2e.api.utility.IngestionHelper
import com.jda.dct.e2e.api.utility.QueryObject
import com.jda.dct.e2e.api.utility.QueryResultHandler
import org.junit.experimental.categories.Category
import spock.lang.Specification

class DemandSortingShipmentsSpec extends Specification implements IngestionHelper {

    @Category(Smoke)
    def "Sorting shipments"() {

        given: "all outboundShipments"

        when: "shipments searched by a valid text and sorted by a field"
        def queryObject = new QueryObject()
        queryObject.setType("shipments")
        queryObject.addQuery("shipmentType", "outboundShipment")
        queryObject.addSortFields("carrierName", "asc", "string")

        and: "page size defined as 10"
        queryObject.setLimit("10")

        def response = QueryResultHandler.getQueryResultHandler().executeWithRetry(queryObject, 20, 2, 2, 1, 1)

        then: "return shipments should be in asc order in the page"
        (0..response.body.data.size() - 2).each({ i ->
            println  response.body.data[i].carrierName
            assert response.body.data[i].carrierName <= response.body.data[i + 1].carrierName
        })
    }

    @Category(Smoke)
    def "Sorting shipments per page"() {

        given: "all outboundShipments"

        when: "shipments searched by a valid text and sorted by a field"
        def queryObject = new QueryObject()
        queryObject.setType("shipments")
        queryObject.addQuery("shipmentType", "outboundShipment")
        queryObject.addQuery("shipmentMode", "Ocean")
        queryObject.addSortFields("carrierName", "asc", "string")

        and: "page size defined as 15 and offset is 10"
        queryObject.setOffset("10")
        queryObject.setLimit("15")
        def response = QueryResultHandler.getQueryResultHandler().executeWithRetry(queryObject, 20, 2, 2, 1, 1)

        then: "return shipments should be in asc order in the page"
        (0..response.body.data.size() - 2).each({ i ->
            println  response.body.data[i].carrierName
            assert response.body.data[i].carrierName <= response.body.data[i + 1].carrierName
        })
    }

    @Category(Smoke)
    def "Sorting shipments in reverse order"() {

        given: "all outboundShipments"

        when: "shipments searched by a valid text and sorted by a field"
        def queryObject = new QueryObject()
        queryObject.setType("shipments")
        queryObject.addQuery("shipmentType", "outboundShipment")
        queryObject.addQuery("shipmentMode", "Ocean")
        queryObject.addSortFields("carrierName", "desc", "string")

        and: "page size defined as 15 and offset is 10"
        queryObject.setOffset("10")
        queryObject.setLimit("15")
        def response = QueryResultHandler.getQueryResultHandler().executeWithRetry(queryObject, 20, 2, 2, 1, 1)

        then: "return shipments should be in desc order in the page"
        (0..response.body.data.size() - 2).each({ i ->
            println  response.body.data[i].carrierName
            assert response.body.data[i].carrierName >= response.body.data[i + 1].carrierName
        })
    }
}
