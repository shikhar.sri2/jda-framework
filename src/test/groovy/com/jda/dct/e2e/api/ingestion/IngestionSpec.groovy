package com.jda.dct.e2e.api.ingestion

import com.jda.dct.e2e.api.utility.IngestionHelper
import spock.lang.Shared
import spock.lang.Specification


/**
 * test cases related to ingestion.
 *
 * @author Vida Bandara
 * @version 1.0-SNAPSHOT
 * @since 19-Sep-2018
 */

// figure out if needs to be in ReadyAPI
class IngestionSpec extends Specification implements IngestionHelper {

    @Shared elasticsearchURL
    @Shared sampleSite

    void setupSpec() {
        init()

        def sitePath = properties.getProperty('ingestSitesPath')
        def elasticsearchPath = properties.getProperty('elasticsearchPath')

        ingestionURL = protocol + ip + sitePath
        elasticsearchURL = protocol + ip + elasticsearchPath

        sampleSite = this.getClass().getClassLoader().getResource('sample-site-1.json').text
    }

//    def "multiple ingestion of the same object has no impact"() {
//        given:
//        def siteId = getSiteIds(sampleSite)[0]
//        def indexName = siteId.split('-')[0..1].join('-').toLowerCase()
//        def searchUrl = elasticsearchURL + '/' + indexName + '/doc/' + siteId
//
//        when:
//        ingestData(sampleSite)
//        def source1 = getResponseBodyWithRetry(searchUrl)
//        def version1 = source1.get('_version')
//
//        ingestData(sampleSite)
//        def source2 = getResponseBodyWithRetry(searchUrl)
//        def version2 = source2.get('_version')
//
//        then:
//        version1 == version2
//    }
//
//    private Object getResponseBodyWithRetry(searchUrl) {
//        def response
//        def retries = 5
//        while (retries-- > 0) {
//            response = getHttpClient(header, searchUrl, null, HttpMethod.GET)
//            if (response != null) {
//                break
//            }
//            sleep(5000L)
//        }
//        println 'reponse: ' + response
//        def body = response.body
//        def source = new JsonSlurper().parseText(body)
//
//        source
//    }
}