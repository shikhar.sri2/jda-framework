package com.jda.dct.e2e.ui.navigation;

import com.jda.dct.e2e.ui.utility.action.CommonActions;
import com.jda.dct.e2e.ui.utility.common.BaseTest;
import com.jda.dct.e2e.ui.utility.common.LeftMenu;
import com.jda.dct.e2e.ui.utility.helper.Browser.BrowserHelper;
import com.jda.dct.e2e.ui.utility.helper.Wait.WaitHelper;
import com.jda.dct.e2e.ui.utility.pageobjects.*;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class SupplyDeliveryDetailsTests extends BaseTest {

    private LeftMenu leftMenu;
    private SupplyDeliveryDetailsPage supplyDeliveryDetailsPage;
    private TopMenuPage topMenu;
    private TopSubMenuPage topSubMenu;
    private SupplyDeliveryListPage supplyDeliveryPage;
    private SupplyShipmentListPage supplyShipmentListPage;
    private BrowserHelper browserHelper;
    private CommonActions commonActions;
    WaitHelper objWaitHelper;

    private WebDriver driver;


    @BeforeClass
    public void setUp() throws InterruptedException {
        System.out.println("Starting tests for :" + this.getClass().getName());
        //Initialize required objects
        driver = getDriver();
        leftMenu = new LeftMenu(driver);
        topMenu = new TopMenuPage(driver);
        supplyDeliveryDetailsPage = new SupplyDeliveryDetailsPage(driver);
        supplyDeliveryPage = new SupplyDeliveryListPage(driver);
        supplyShipmentListPage = new SupplyShipmentListPage(driver);
        topSubMenu = new TopSubMenuPage(driver);
        objWaitHelper = new WaitHelper(driver);
        browserHelper = new BrowserHelper(driver);
        commonActions = new CommonActions(driver);
        logInToApplication();
        //loginAndNavigateToSupplyLandingPage

    }


    @Test(priority = 1,retryAnalyzer = BaseTest.class)
    public void validateDeliveryDetailsPageWhenDeliveryNumberIsClicked() throws InterruptedException {
        leftMenu.clickSupplyMenuOption();
        topMenu.goToSupplyDeliveriesTab();
        supplyDeliveryPage.clickOceanIcon();
        commonActions.disableAllExceptionFilter();
        supplyDeliveryDetailsPage.clickOnDeliveryNumber();
        Assert.assertTrue(supplyDeliveryDetailsPage.isShipmentNumberVisibleInDeliveryDetailsPage());
        supplyDeliveryDetailsPage.returnToDeliveryListPage();
    }

    /**
     * Container no. data is not present because of that it is failing. Needs to be checked if for all container data should be present?
     *
     * @throws InterruptedException
     */
    @Test(priority = 2,retryAnalyzer = BaseTest.class)
    public void validateShipmentPageWhenContainerNumberIsClicked() throws InterruptedException {
        leftMenu.clickSupplyMenuOption();
        topMenu.goToSupplyDeliveriesTab();
        String deliveryNo = "IDPO0003_CO";
        topMenu.clearFilterIfVisible();
        topSubMenu.goToSearchFeature(deliveryNo);
        supplyDeliveryPage.clickOceanIcon();
        commonActions.disableAllExceptionFilter();
        supplyDeliveryDetailsPage.expandDropDown();
        supplyDeliveryDetailsPage.gotoContainerNumber();
        supplyShipmentListPage.clickOceanIcon();
        commonActions.disableAllExceptionFilter();
        Assert.assertTrue(supplyDeliveryDetailsPage.isShipmentNumberInThePageDisplayed());
        topMenu.goToSupplyDeliveriesTab();
        topMenu.clearFilter();
    }

    @Test(priority = 3,retryAnalyzer = BaseTest.class)
    public void validateSupplyInventoryDetailsPageWhenItemNumberIsClicked() throws InterruptedException {
        leftMenu.clickSupplyMenuOption();
        topMenu.goToSupplyDeliveriesTab();
        topMenu.clearFilterIfVisible();
        supplyDeliveryPage.clickOceanIcon();
        commonActions.disableAllExceptionFilter();
        supplyDeliveryDetailsPage.expandDropDown();
        supplyDeliveryDetailsPage.clickOnItemNumber();
        //driver.navigate().refresh();
        Assert.assertTrue(supplyDeliveryDetailsPage.isInventoryDetailsPageDisplayed());
        supplyDeliveryDetailsPage.returnToInventoryPage();
    }


    @Test(priority = 4,retryAnalyzer = BaseTest.class)
    public void validatePurchaseOrderDetailsPageWhenPurchaseOrderIsClicked() throws InterruptedException {
        leftMenu.clickSupplyMenuOption();
        topMenu.goToSupplyDeliveriesTab();
        topMenu.clearFilterIfVisible();
        supplyDeliveryPage.clickOceanIcon();
        commonActions.disableAllExceptionFilter();
        supplyDeliveryDetailsPage.expandDropDown();
        supplyDeliveryDetailsPage.gotoPurchaseOrderNumber();
        Assert.assertTrue(supplyDeliveryDetailsPage.isOrderNumberDisplayed());
        supplyDeliveryDetailsPage.gotoOrderTab();
        //driver.navigate().refresh();
    }

    @Test(priority = 5,retryAnalyzer = BaseTest.class) //
    public void validateShipmentDetailsPageWhenShipmentNumberIsClicked() throws InterruptedException {
        leftMenu.clickSupplyMenuOption();
        topMenu.goToSupplyDeliveriesTab();
        supplyDeliveryPage.clickOceanIcon();
        commonActions.disableAllExceptionFilter();
        supplyDeliveryDetailsPage.expandDropDown();
        supplyDeliveryDetailsPage.clickOnDeliveryNumber();
        supplyDeliveryDetailsPage.clickOnShipmentNumber();
        supplyDeliveryDetailsPage.returnToShipmentPage();
    }

    @Test(priority = 6,retryAnalyzer = BaseTest.class) //
    public void validateSupplyInventoryDetailsPageaWhenItemNumberIsClicked() throws InterruptedException {
        leftMenu.clickSupplyMenuOption();
        topMenu.goToSupplyDeliveriesTab();
        supplyDeliveryPage.clickOceanIcon();
        commonActions.disableAllExceptionFilter();
        supplyDeliveryDetailsPage.expandDropDown();
        supplyDeliveryDetailsPage.clickOnDeliveryNumber();
        //driver.navigate().refresh();
        supplyDeliveryDetailsPage.gotoItemNumberFromDetailsPage();
        //driver.navigate().refresh();
        Assert.assertTrue(supplyDeliveryDetailsPage.isInventoryDetailsPageDisplayed());
        supplyDeliveryDetailsPage.returnToInventoryPage();
    }

    @Test(priority = 7,retryAnalyzer = BaseTest.class) //
    public void validatePurchaseOrderDetailsPageWhenPurchaseOrderNumberIsClicked() throws InterruptedException {
        leftMenu.clickSupplyMenuOption();
        topMenu.goToSupplyDeliveriesTab();
        supplyDeliveryPage.clickOceanIcon();
        commonActions.disableAllExceptionFilter();
        supplyDeliveryDetailsPage.expandDropDown();
        supplyDeliveryDetailsPage.clickOnDeliveryNumber();
        supplyDeliveryDetailsPage.clickPurchaseOrderNumberAtDeliveryDetailsPage();
        Assert.assertTrue(supplyDeliveryDetailsPage.isOrderNumberDisplayed());
        supplyDeliveryDetailsPage.gotoOrderTab();
        browserHelper.refresh();
    }
}



