//package com.jda.dct.e2e.api.computations
//package com.jda.dct.e2e.api
//   look into whether we need to cover this in ReadyAPI

//package com.jda.dct.e2e.ingestionApi
//
//import IngestionHelper
//import spock.lang.Shared
//import spock.lang.Specification
//import spock.lang.Stepwise
//
//@Stepwise
//class com.jda.dct.e2e.api.com.jda.dct.e2e.api.computations.DeliveryToOrderComputationSpec extends Specification implements IngestionHelper {
//
//    @Shared
//    def deliveryIngestionURL
//
//    @Shared
//    def poIngestionURL
//
//    @Shared
//    def poGetURL
//
//    @Shared
//    def soIngestionURL
//
//    @Shared
//    def soGetURL
//
//    @Shared getDeliveriesPath
//
//    def setupSpec() {
//        init()
//
//        def purchaseOrderPath = properties.getProperty('ingestPurchaseOrdersPath')
//        poIngestionURL = protocol + ip + purchaseOrderPath
//
//        def getPath = properties.getProperty('getPurchaseOrderPath')
//        poGetURL =  protocol + ip + getPath
//
//        def salesOrderPath = properties.getProperty('ingestSalesOrdersPath')
//        soIngestionURL = protocol + ip + salesOrderPath
//
//        def getSalesOrderPath = properties.getProperty('getSalesOrderPath')
//        soGetURL = protocol + ip + getSalesOrderPath
//
//        def deliveryPath = properties.getProperty('ingestDeliveriesPath')
//        deliveryIngestionURL = protocol + ip + deliveryPath
//
//        getDeliveriesPath = protocol + ip + properties.getProperty('getDeliveryPath')
//    }
//
//    def "Ingest a outboundDelivery with PO reference which triggers computation on the purchase order"() {
//
//        given: 'The purchase object and delivery object'
//
//        def sampleOutBoundPurchaseOrder = this.getClass().getClassLoader().getResource('sample-outbound-po-computation.json').text
//        def sampleOutBoundDeliveryWithPO = this.getClass().getClassLoader().getResource('sample-outbound-delivery-with-PO-reference.json').text
//
//        def purchaseOrderId = getPurchaseOrderIds(sampleOutBoundPurchaseOrder)[0]
//        def deliveryId = getDeliveryIds(sampleOutBoundDeliveryWithPO)[0]
//        deleteObject(poGetURL, purchaseOrderId)
//        deleteObject(getDeliveriesPath, deliveryId)
//
//        when: 'ingest a purchase order  and delivery and verify if its ingested'
//
//        ingestionURL = poIngestionURL
//        ingestPurchaseOrder(sampleOutBoundPurchaseOrder)
//        sleep(30000)
//
//        ingestionURL = deliveryIngestionURL
//        ingestDelivery(sampleOutBoundDeliveryWithPO)
//        sleep(60000)
//
//        def url = poGetURL + purchaseOrderId
//
//        def purchaseOrderText = getHttpClient(header, url, null, HttpMethod.GET).body
//        def purchaseOrderRetrieved = new JsonSlurper().parseText(purchaseOrderText)
//
//        then: 'verify that purchase order exception status is updated'
//
//        purchaseOrderRetrieved.getAt('data').getAt('exceptionStatus').toString() == '[Late]'
//        purchaseOrderRetrieved.getAt('data').getAt('purchaseOrderLines').getAt('POL101').getAt('exceptionStatus').toString() == '[Late]'
//        purchaseOrderRetrieved.getAt('data').getAt('purchaseOrderLines').getAt('POL102').getAt('exceptionStatus').toString() == '[On Time]'
//    }
//
//    def "Ingest a outboundDelivery with SO reference which triggers computation on the sales order"() {
//
//        given: 'Sales Order object and delivery object'
//
//        def sampleOutboundSalesOrder = this.getClass().getClassLoader().getResource('sample-outbound-so-computation-2.json').text
//        def sampleOutBoundDeliveryWithSO = this.getClass().getClassLoader().getResource('sample-outbound-delivery-with-SO-reference.json').text
//
//        def salesOrderId = getSalesOrderIds(sampleOutboundSalesOrder)[0]
//        def deliveryId = getDeliveryIds(sampleOutBoundDeliveryWithSO)[0]
//        deleteObject(soGetURL, salesOrderId)
//        deleteObject(getDeliveriesPath, deliveryId)
//
//
//        when: 'ingest a sales order  and delivery and verify if its ingested'
//
//        ingestionURL = soIngestionURL
//        ingestSalesOrder(sampleOutboundSalesOrder)
//        sleep(30000)
//
//        ingestionURL = deliveryIngestionURL
//        ingestData(sampleOutBoundDeliveryWithSO)
//        sleep(60000)
//
//        def url = soGetURL + salesOrderId
//        def salesOrderText = getHttpClient(header, url, null, HttpMethod.GET).body
//        def salesOrderRetrieved = new JsonSlurper().parseText(salesOrderText)
//
//        then: 'verify that sales order exception status is updated'
//
//        salesOrderRetrieved.getAt('data').getAt('exceptionStatus').toString() == '[Late]'
//        salesOrderRetrieved.getAt('data').getAt('salesOrderLines').getAt('SL91').getAt('exceptionStatus').toString() == '[On Time]'
//        salesOrderRetrieved.getAt('data').getAt('salesOrderLines').getAt('SL92').getAt('exceptionStatus').toString() == '[Late]'
//    }
//
//    def "Ingest a inboundDelivery with PO reference which triggers computation on the purchase order"() {
//
//        given: 'Purchase order object and inbound delivery'
//
//        def sampleInBoundPurchaseOrder = this.getClass().getClassLoader().getResource('sample-inbound-po-computation.json').text
//        def sampleInBoundDeliveryWithPO = this.getClass().getClassLoader().getResource('sample-inbound-delivery-with-PO-reference.json').text
//
//        def purchaseOrderId = getPurchaseOrderIds(sampleInBoundPurchaseOrder)[0]
//        def deliveryId = getDeliveryIds(sampleInBoundDeliveryWithPO)[0]
//        deleteObject(poGetURL, purchaseOrderId)
//        deleteObject(getDeliveriesPath, deliveryId)
//
//
//        when: 'ingest a purchase order  and delivery and verify if its ingested'
//
//        ingestionURL = poIngestionURL
//        ingestPurchaseOrder(sampleInBoundPurchaseOrder)
//        sleep(30000)
//
//        ingestionURL = deliveryIngestionURL
//        ingestData(sampleInBoundDeliveryWithPO)
//        sleep(30000)
//
//        def url = poGetURL + purchaseOrderId
//
//        def purchaseOrderText = getHttpClient(header, url, null, HttpMethod.GET).body
//        def purchaseOrderRetrieved = new JsonSlurper().parseText(purchaseOrderText)
//
//        then: 'verify that purchase order exception status is updated'
//
//        purchaseOrderRetrieved.getAt('data').getAt('exceptionStatus').toString() == '[Late]'
//        purchaseOrderRetrieved.getAt('data').getAt('purchaseOrderLines').getAt('POL101').getAt('exceptionStatus').toString() == '[Late]'
//        purchaseOrderRetrieved.getAt('data').getAt('purchaseOrderLines').getAt('POL102').getAt('exceptionStatus').toString() == '[On Time]'
//    }
//
//    def "Ingest a inboundDelivery with SO reference which triggers computation on the sales order"() {
//
//        given: 'Sales Order object and delivery object'
//
//        def sampleInBoundDeliveryWithSO = this.getClass().getClassLoader().getResource('sample-inbound-delivery-with-SO-reference.json').text
//        def sampleInboundSalesOrder = this.getClass().getClassLoader().getResource('sample-inbound-so-computation.json').text
//
//        def deliveryId = getDeliveryIds(sampleInBoundDeliveryWithSO)[0]
//        def salesOrderId = getSalesOrderIds(sampleInboundSalesOrder)[0]
//        deleteObject(getDeliveriesPath, deliveryId)
//        deleteObject(soGetURL, salesOrderId)
//
//
//        when: 'ingest a sales order  and delivery and verify if its ingested'
//
//        ingestionURL = soIngestionURL
//        ingestSalesOrder(sampleInboundSalesOrder)
//        sleep(30000)
//
//        ingestionURL = deliveryIngestionURL
//        ingestData(sampleInBoundDeliveryWithSO)
//        sleep(30000)
//
//        def url = soGetURL + salesOrderId
//        def salesOrderText = getHttpClient(header, url, null, HttpMethod.GET).body
//        def salesOrderRetrieved = new JsonSlurper().parseText(salesOrderText)
//
//        then: 'verify that sales order exception status is updated'
//
//        salesOrderRetrieved.getAt('data').getAt('exceptionStatus').toString() == '[Late]'
//        salesOrderRetrieved.getAt('data').getAt('salesOrderLines').getAt('SL91').getAt('exceptionStatus').toString() == '[On Time]'
//        salesOrderRetrieved.getAt('data').getAt('salesOrderLines').getAt('SL92').getAt('exceptionStatus').toString() == '[Late]'
//    }
//
//    def "Ingest a outboundDelivery with different PO's reference which triggers computation on the purchase orders"() {
//
//        given: 'The purchase objects and delivery object'
//
//        def sampleOutBoundPurchaseOrder = this.getClass().getClassLoader().getResource('sample-outbound-po-computation.json').text
//        def sampleOutBoundPurchaseOrder1 = sampleOutBoundPurchaseOrder.replaceAll("purchaseOrderId\": \"PO10", "purchaseOrderId\": \"PO202")
//
//        sampleOutBoundPurchaseOrder = this.getClass().getClassLoader().getResource('sample-outbound-po-computation.json').text
//        def sampleOutBoundPurchaseOrder2 = sampleOutBoundPurchaseOrder.replaceAll("purchaseOrderId\": \"PO10", "purchaseOrderId\": \"PO303")
//
//        def sampleOutBoundDeliveryWithMultiplePO = this.getClass().getClassLoader().getResource('sample-outbound-delivery-with-multiple-PO-reference.json').text
//
//
//        def purchaseOrderId1 = getPurchaseOrderIds(sampleOutBoundPurchaseOrder1)[0]
//        def purchaseOrderId2 = getPurchaseOrderIds(sampleOutBoundPurchaseOrder2)[0]
//        def deliveryId = getDeliveryIds(sampleOutBoundDeliveryWithMultiplePO)[0]
//        deleteObject(poGetURL, purchaseOrderId1)
//        deleteObject(poGetURL, purchaseOrderId2)
//        deleteObject(getDeliveriesPath, deliveryId)
//
//        when: 'ingest a purchase order  and delivery and verify if its ingested'
//
//        ingestionURL = poIngestionURL
//        ingestPurchaseOrder(sampleOutBoundPurchaseOrder1)
//        sleep(30000)
//
//        ingestionURL = poIngestionURL
//        ingestPurchaseOrder(sampleOutBoundPurchaseOrder2)
//        sleep(30000)
//
//        ingestionURL = deliveryIngestionURL
//        ingestData(sampleOutBoundDeliveryWithMultiplePO)
//        sleep(30000)
//
//        def url = poGetURL + purchaseOrderId1
//
//        def purchaseOrderText = getHttpClient(header, url, null, HttpMethod.GET).body
//        def purchaseOrderRetrieved = new JsonSlurper().parseText(purchaseOrderText)
//
//        def url1 = poGetURL + purchaseOrderId2
//
//        def purchaseOrderText1 = getHttpClient(header, url1, null, HttpMethod.GET).body
//        def purchaseOrderRetrieved1 = new JsonSlurper().parseText(purchaseOrderText1)
//
//        then: 'verify that purchase order exception status is updated'
//
//        purchaseOrderRetrieved1.getAt('data').getAt('exceptionStatus').toString() == '[On Time]'
//        purchaseOrderRetrieved1.getAt('data').getAt('purchaseOrderLines').getAt('POL101').getAt('exceptionStatus').toString() == '[On Time]'
//        purchaseOrderRetrieved1.getAt('data').getAt('purchaseOrderLines').getAt('POL102').getAt('exceptionStatus').toString() == '[On Time]'
//
//        purchaseOrderRetrieved.getAt('data').getAt('exceptionStatus').toString() == '[On Time]'
//        purchaseOrderRetrieved.getAt('data').getAt('purchaseOrderLines').getAt('POL101').getAt('exceptionStatus').toString() == '[On Time]'
//        purchaseOrderRetrieved.getAt('data').getAt('purchaseOrderLines').getAt('POL102').getAt('exceptionStatus').toString() == '[On Time]'
//    }
//
//    def "Ingest a outboundDelivery with different SO's reference which triggers computation on the sales order"() {
//
//        given: 'Sales Order object and delivery object'
//
//        def sampleOutboundSalesOrder = this.getClass().getClassLoader().getResource('sample-outbound-so-computation.json').text
//
//        def sampleOutboundSalesOrder1 = sampleOutboundSalesOrder.replaceAll("\"salesOrderId\": \"SOD9\",", "\"salesOrderId\": \"SO92\",")
//
//        sampleOutboundSalesOrder = this.getClass().getClassLoader().getResource('sample-outbound-so-computation.json').text
//        def sampleOutboundSalesOrder2  =sampleOutboundSalesOrder.replaceAll("\"salesOrderId\": \"SOD9\",", "\"salesOrderId\": \"SO93\",")
//
//        def sampleOutBoundDeliveryWithMultipleSO = this.getClass().getClassLoader().getResource('sample-outbound-delivery-with-multiple-SO-reference.json').text
//
//        def salesOrderId1 = getSalesOrderIds(sampleOutboundSalesOrder1)[0]
//        def salesOrderId2 = getSalesOrderIds(sampleOutboundSalesOrder2)[0]
//        def deliveryId = getDeliveryIds(sampleOutBoundDeliveryWithMultipleSO)[0]
//        deleteObject(soGetURL, salesOrderId1)
//        deleteObject(soGetURL, salesOrderId2)
//        deleteObject(getDeliveriesPath, deliveryId)
//
//
//        when: 'ingest a sales order  and delivery and verify if its ingested'
//
//        ingestionURL = soIngestionURL
//        ingestSalesOrder(sampleOutboundSalesOrder1)
//        sleep(30000)
//
//        ingestionURL = soIngestionURL
//        ingestSalesOrder(sampleOutboundSalesOrder2)
//        sleep(30000)
//
//        ingestionURL = deliveryIngestionURL
//        ingestData(sampleOutBoundDeliveryWithMultipleSO)
//        sleep(30000)
//
//        def url = soGetURL + salesOrderId1
//        def salesOrderText = getHttpClient(header, url, null, HttpMethod.GET).body
//        def salesOrderRetrieved = new JsonSlurper().parseText(salesOrderText)
//
//        def url1 = soGetURL + salesOrderId2
//        def salesOrderText1 = getHttpClient(header, url1, null, HttpMethod.GET).body
//        def salesOrderRetrieved1 = new JsonSlurper().parseText(salesOrderText1)
//
//        then: 'verify that sales order exception status is updated'
//
//        salesOrderRetrieved.getAt('data').getAt('exceptionStatus').toString() == '[On Time]'
//        salesOrderRetrieved.getAt('data').getAt('salesOrderLines').getAt('SL91').getAt('exceptionStatus').toString() == '[On Time]'
//        salesOrderRetrieved.getAt('data').getAt('salesOrderLines').getAt('SL92').getAt('exceptionStatus').toString() == '[On Time]'
//
//        salesOrderRetrieved1.getAt('data').getAt('exceptionStatus').toString() == '[Late]'
//        salesOrderRetrieved1.getAt('data').getAt('salesOrderLines').getAt('SL91').getAt('exceptionStatus').toString() == '[On Time]'
//        salesOrderRetrieved1.getAt('data').getAt('salesOrderLines').getAt('SL92').getAt('exceptionStatus').toString() == '[Late]'
//    }
//
//
//    def "Ingest a outboundDelivery with PO with contains SO reference which triggers computation on the sales order"() {
//
//        given: 'Sales Order object and delivery object'
//
//        def sampleOutboundPurchaseOrder = this.getClass().getClassLoader().getResource('sample-purchase-order-sales-computation-3.json').text
////
////        def sampleOutboundPurchaseOrder1 = sampleOutboundPurchaseOrder.replaceAll("\"purchaseOrderId\": \"PO2\",", "\"purchaseOrderId\": \"PO248\",")
////
////        sampleOutboundPurchaseOrder1.replaceAll("\"salesOrderNo\" : \"SO1\",", "\"salesOrderNo\" : \"SO95\",")
////        sampleOutboundPurchaseOrder1.replaceAll("\"salesOrderLineNo\": \"SL11\",", "\"salesOrderLineNo\": \"SL91\",")
////        sampleOutboundPurchaseOrder1.replaceAll("\"salesOrderLineNo\": \"SL12\",", "\"salesOrderLineNo\": \"SL92\",")
//
//        def sampleOutboundSalesOrder = this.getClass().getClassLoader().getResource('sample-outbound-so-computation-1.json').text
////        def sampleOutboundSalesOrder1  =sampleOutboundSalesOrder.replaceAll("\"salesOrderId\": \"SO9\",", "\"salesOrderId\": \"SO950\",")
////        sampleOutboundSalesOrder1.replace("\"needByDate\": \"2018-08-22T11:31:40.002+0000\"", "\"needByDate\": \"2018-11-22T11:31:40.002+0000\"")
//
//
//        def sampleOutBoundDeliveryWithMultipleSO = this.getClass().getClassLoader().getResource('sample-outbound-delivery-with-PO-reference-1.json').text
//        sampleOutBoundDeliveryWithMultipleSO.replaceAll("\"deliveryId\": \"delivery110\",", "\"deliveryId\": \"delivery190\",")
//
//        def purchaseOrderId = getPurchaseOrderIds(sampleOutboundPurchaseOrder)[0]
//        def salesOrderId = getSalesOrderIds(sampleOutboundSalesOrder)[0]
//        def deliveryId = getDeliveryIds(sampleOutBoundDeliveryWithMultipleSO)[0]
//        deleteObject(poGetURL, purchaseOrderId)
//        deleteObject(soGetURL, salesOrderId)
//        deleteObject(getDeliveriesPath, deliveryId)
//
//
//        when: 'ingest a sales order  and delivery and verify if its ingested'
//
//        ingestionURL = soIngestionURL
//        ingestSalesOrder(sampleOutboundSalesOrder)
//        sleep(30000)
//
//        ingestionURL = poIngestionURL
//        ingestPurchaseOrder(sampleOutboundPurchaseOrder)
//        sleep(30000)
//
//        ingestionURL = deliveryIngestionURL
//        ingestData(sampleOutBoundDeliveryWithMultipleSO)
//        sleep(30000)
//
//        def url = soGetURL + salesOrderId
//        def salesOrderText = getHttpClient(header, url, null, HttpMethod.GET).body
//        def salesOrderRetrieved = new JsonSlurper().parseText(salesOrderText)
//
//        url = poGetURL + purchaseOrderId
//
//        def purchaseOrderText = getHttpClient(header, url, null, HttpMethod.GET).body
//        def purchaseOrderRetrieved = new JsonSlurper().parseText(purchaseOrderText)
//
//
//        then: 'verify that sales order exception status is updated'
//
//        salesOrderRetrieved.getAt('data').getAt('exceptionStatus').toString() == '[Late]'
//        salesOrderRetrieved.getAt('data').getAt('salesOrderLines').getAt('SL91').getAt('exceptionStatus').toString() == '[Late]'
//        salesOrderRetrieved.getAt('data').getAt('salesOrderLines').getAt('SL92').getAt('exceptionStatus').toString() == '[On Time]'
//
//        purchaseOrderRetrieved.getAt('data').getAt('exceptionStatus').toString() == '[Late]'
//        purchaseOrderRetrieved.getAt('data').getAt('purchaseOrderLines').getAt('POL101').getAt('exceptionStatus').toString() == '[Late]'
//        purchaseOrderRetrieved.getAt('data').getAt('purchaseOrderLines').getAt('POL102').getAt('exceptionStatus').toString() == '[On Time]'
//    }
//}
