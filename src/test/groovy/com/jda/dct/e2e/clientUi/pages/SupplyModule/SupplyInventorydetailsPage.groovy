package com.jda.dct.e2e.clientUi.pages.SupplyModule

import geb.Page


class SupplyInventorydetailsPage extends Page{

    static url = "/tempraturecontrolled/inventory"

    static atCheckWaiting = true

    static at = {

        $("#NodeDetail-Typography-item-name").isDisplayed()
    }

    static content = {

        inventoryview(wait:true) {

            $("#app > div > div.App__AppWrapper-jqrDvk.dTKlbn div:nth-child(1) > h2 > b")

        }
        navigateback(wait: true) {

            $("a", text: "Inventory")
        }
    }


    def gotoSupplyInventorydetails() {

        waitFor {
            inventoryview.displayed
        }
        println("welcome to inventory details page")
        navigateback.click()
    }

}
