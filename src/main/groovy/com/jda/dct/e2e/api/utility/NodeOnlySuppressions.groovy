package com.jda.dct.e2e.api.utility

import com.fasterxml.jackson.annotation.JsonIgnore
import com.jda.dct.domain.*

class NodeOnlySuppressions {
    @JsonIgnore String tid
    @JsonIgnore String id
    @JsonIgnore Date creationDate
    @JsonIgnore Date lmd
    @JsonIgnore Map<String, List<Tsd>> measures
    @JsonIgnore String solutionType
    @JsonIgnore String suppItemOwnerName

}
