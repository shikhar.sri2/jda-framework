package com.jda.dct.e2e.ui.utility.pageobjects;

import com.jda.dct.e2e.ui.utility.common.BasePage;
import com.jda.dct.e2e.ui.utility.helper.Action.ActionHelper;
import com.jda.dct.e2e.ui.utility.helper.Wait.WaitHelper;
import com.jda.dct.e2e.ui.utility.helper.genericHelper.GenericHelper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class SupplyDeliveryDetailsPage extends BasePage {
    WebDriver driver;
    WebDriverWait webDriverWait;
    WaitHelper objWaitHelper;
    GenericHelper objGenericHelper;
    ActionHelper objActionHelper;

    public SupplyDeliveryDetailsPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(this.driver, this); //local argument is being initialized to the local driver object?
        webDriverWait = new WebDriverWait(this.driver, 90);
        objWaitHelper = new WaitHelper(driver);
        objGenericHelper = new GenericHelper(driver);
        objActionHelper = new ActionHelper(driver);
    }


    @FindBy(id ="PanelSummary-RenderedItem-Item-1-LinkItem") //just
    private WebElement itemNumber;

    @FindBy(id ="PanelSummary-RenderedItem-Item-0-LinkItem") //just
    private WebElement itemNumberOnDetailsPage;

    @FindBy(id ="PanelSummary-RenderedItem-Container-No-1-LinkItem")
    private WebElement containerNumber;

    @FindBy(xpath = "//*[@id='PanelSummary-RenderedItem-Delivery-No-1-LinkItem']") //delivery no
    private WebElement  deliveryNumber;

    // @FindBy(id ="PanelSummary-RenderedItem-Shipment No-LinkItem")
    @FindBy(xpath ="//*[@id='PanelSummary-RenderedItem-Shipment-No-0-LinkItem']")
    private WebElement shipmentNumber;

    @FindBy(xpath ="//*[@id='PanelSummary-RenderedItem-Shipment-No-1-LinkItem']")
    private WebElement shipmentNumberOnShipmentPage;

    @FindBy(id = "PanelSummary-RenderedItem-PO-No-0-LinkItem")
    private WebElement PurchaseOrderNumberAtDeliveryDetailsPage;

    @FindBy(xpath = "//*[@id='PanelSummary-RenderedItem-PO-No-1-LinkItem']")
    // @FindBy(id ="PanelSummary-RenderedItem-SO No-LinkItem")
    private List<WebElement> pONumber;


    @FindBy(xpath = "//div[@aria-expanded='false']/div/following-sibling::div/span/*")
    private WebElement dropDown;

//
//    @FindBy(xpath = "//span[contains(text(),'   Inventory Details   ')]") //modifif
//    private WebElement inventoryDetailsPage;


    @FindBy(xpath = "//*[@id='breadcrumb-inventory-details']")
    private WebElement inventoryDetailsPage;

    @FindBy(xpath = "//*[contains(@id,'PanelSummary-RenderedItem-Order-No')][contains(@href,'/supply/purchase-orders')]")
    private WebElement isOrderNumberPresent;

    @FindBy(xpath = "//a[@href='/supply/delivery']")
    private WebElement supplyDeliveryTab;

    @FindBy(id ="PanelSummary-RenderedItem-Container No-LinkItem")
    private WebElement isContainerNumberVisible;





    @FindBy(xpath = "//a[@href='/supply/inventory']")
    private WebElement inventory;


    @FindBy(xpath = "//a[@href='/supply/purchase-orders']")
    private WebElement order;


    @FindBy(xpath = "//a[@href='/supply/shipment']")
    private WebElement shipment;

    @FindBy(xpath ="//*[contains(@id,'delivery')]")
    private WebElement returnToDeliveryPageList;

    @FindBy(xpath ="//*[@href='/inventory']")
    private WebElement returnToInventoryPage;

    @FindBy(xpath ="//a[@href='/supply/shipment']")
    private WebElement returnToShipmentPage;



    // Methods  Starts



    public void gotoSupplyDeliveryTab() {
        objWaitHelper.waitElementToBeClickable(supplyDeliveryTab);
        objGenericHelper.elementClick(supplyDeliveryTab, "Delivery Tab");
    }

    public void clickOnDeliveryNumber() { //for delivery
        objWaitHelper.waitAndClickElement(deliveryNumber, "Delivery Number");
    }

    public void gotoToDeliveryNumber() { //for delivery
        objWaitHelper.waitAndClickElement(deliveryNumber, "Delivery Number");
    }



    public void returnToDeliveryListPage() {
        // webDriverWait.until(ExpectedConditions.elementToBeClickable(deliveryNumber));
        objGenericHelper.elementClick(returnToDeliveryPageList, "Delivery page link");
    }

    public void returnToInventoryPage() {
        // webDriverWait.until(ExpectedConditions.elementToBeClickable(deliveryNumber));
        objGenericHelper.elementClick(returnToInventoryPage, "Inventory page link");
    }


    public void returnToShipmentPage() {
        objWaitHelper.waitElementToBeClickable(returnToShipmentPage);
        objGenericHelper.elementClick(returnToShipmentPage, "Shipment page link");
    }


    public void clickOnItemNumber() {
        objWaitHelper.waitAndClickElement(itemNumber, "Item Number");
    }

    public void gotoItemNumber() {
        objWaitHelper.waitAndClickElement(itemNumber, "Item Number");
    }

    public void gotoItemNumberFromDetailsPage() {
        objWaitHelper.waitAndClickElement(itemNumberOnDetailsPage, "Item Number");
    }

    public void clickPurchaseOrderNumberAtDeliveryDetailsPage() {
        objWaitHelper.waitAndClickElement(PurchaseOrderNumberAtDeliveryDetailsPage, "Purchase Order Number");
    }

    public void clickOnShipmentNumber() {
        objWaitHelper.waitAndClickElement(shipmentNumber, "Shipment Number");
    }

    public void gotoContainerNumber() {
        objWaitHelper.waitAndClickElement(containerNumber, "Container Number");
    }

    public void gotoPurchaseOrderNumber() {
        objActionHelper.moveToElement(pONumber.get(0), "PO Number");
        objWaitHelper.waitAndClickElement(pONumber.get(0), "PO Number");
    }

    public void expandDropDown() {
        objWaitHelper.waitAndClickElement(dropDown, "Details dropdown");
    }

    public boolean isOrderNumberDisplayed() {
        objWaitHelper.waitElementToBeClickable(isOrderNumberPresent);
        return objGenericHelper.isDisplayed(isOrderNumberPresent, "Order Number");
    }

    public void gotoInventoryTab() {
        objWaitHelper.waitElementToBeClickable(inventory);
        objGenericHelper.elementClick(inventory, "Inventory");
    }

    public void gotoOrderTab() {
        objWaitHelper.waitElementToBeClickable(order);
        objGenericHelper.elementClick(order, "Order");
    }

    public void gotoshipment() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(shipment));
        shipment.click();
    }

//Assertion Points //

    public boolean isShipmentNumberVisibleInDeliveryDetailsPage() {
        objWaitHelper.waitElementToBeVisible(shipmentNumber);
        return objGenericHelper.isDisplayed(shipmentNumber, "Shipment Number");
    }

    public boolean isItemNumberVisibleOnThePage() {
        objWaitHelper.waitElementToBeVisible(itemNumber);
        return objGenericHelper.isDisplayed(itemNumber, "Item Number");
    }


    public boolean isContainerNumberDisplayed() {
        objWaitHelper.waitElementToBeVisible(containerNumber);
        return objGenericHelper.isDisplayed(containerNumber, "Container Number");
    }

    public boolean isShipmentNumberInThePageDisplayed() {
        objWaitHelper.waitElementToBeVisible(shipmentNumberOnShipmentPage);
        return objGenericHelper.isDisplayed(shipmentNumberOnShipmentPage, "Shipment Number");
    }

    public boolean isInventoryDetailsPageDisplayed() {
        objWaitHelper.waitElementToBeVisible(inventoryDetailsPage);
        return objGenericHelper.isDisplayed(inventoryDetailsPage, "Inventory Details Page");
    }

    public void atInventoryLink() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(inventory));
        inventory.click();
    }

    public void atorderLink() {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(order));
        order.click();
    }
}