package com.jda.dct.e2e.ui.utility.action;

import com.jda.dct.e2e.ui.utility.ExcelHelper.ExcelReader;
import com.jda.dct.e2e.ui.utility.common.BasePage;
import com.jda.dct.e2e.ui.utility.helper.Wait.WaitHelper;
import com.jda.dct.e2e.ui.utility.helper.genericHelper.GenericHelper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class CommonActions extends BasePage {

    WebDriver driver;
    WaitHelper objWaitHelper;
    GenericHelper objGenericHelper;
    ExcelReader excelReader;
    int reCheckCount = 0;

    public CommonActions(WebDriver webdriver) {

        this.driver = webdriver;
        PageFactory.initElements(driver, this);
        objWaitHelper = PageFactory.initElements(driver, WaitHelper.class);
        objGenericHelper = PageFactory.initElements(driver, GenericHelper.class);
        excelReader = new ExcelReader();
    }

    @FindBy(xpath = "//*[@id='toolBar']//button[@value='isHotShipment']")
    private WebElement isHotItemFilter;

    @FindBy(xpath = "//*[@id='toolBar']//button[@value='isTempControlled']")
    private WebElement isTempcontrolledFilter;

    @FindBy(xpath = "//*[@class='sk-spinner ball-spin-fade-loader']")
    private WebElement loadingSpinner;

    @FindBy(xpath = "//*[contains(@id,'Filter-List-Card')]")
    private List<WebElement> filterList;


    /**
     * This method will click on filter status having data greater than 0
     *
     * @param listElement
     * @return
     */
    public boolean clickExceptionStatusHavingData(List<WebElement> listElement) {
        int i = 0;
        objWaitHelper.waitForInvisibiltyOfElement(loadingSpinner);
        while (i < listElement.size()) {
            objWaitHelper.waitElementToBeVisible(listElement.get(i));
            //objWaitHelper.waitForElement(1000, listElement.get(i));
            if (listElement.get(i).getText().equals("0")) {
                i++;
            } else {
                objWaitHelper.waitElementToBeClickable(listElement.get(i));
                objGenericHelper.elementClick(listElement.get(i), "Exception status having data");
                return true;
            }
            if (i == listElement.size()) {
                if (reCheckCount == 0) {
                    reCheckCount++;
                    if (objGenericHelper.checkVisbilityOfElement(isHotItemFilter, "Hot Item filter") == true) {
                        objWaitHelper.waitAndClickElement(isHotItemFilter, "Hot Item filter");
                        objWaitHelper.waitAndClickElement(isTempcontrolledFilter, "Temp Controlled filter");
                        clickExceptionStatusHavingData(listElement);
                    }
                }
                Reporter("No Exception filter is having count greater than 0, may be data is not present", "Fail", log);
                throw new RuntimeException("No Exception filter is having count greater than 0, may be data is not present");
            }
        }
        return false;
    }

    public Double convertQtyFromStringToDouble(String stringValue) {
        double returnValue;
        if (stringValue.contains("K")) {
            returnValue = Double.valueOf(stringValue.substring(0, stringValue.indexOf("K")));
            return returnValue * 1000;
        } else {
            return Double.valueOf(stringValue);
        }
    }

    public void disableAllExceptionFilter() {
        try {
            String value = objGenericHelper.getValueFromAttribute(filterList.get(0), "style");
            if (value.contains("opacity: 1")) {
                filterList.get(0).click();
            } else {
                filterList.get(0).click();
                filterList.get(0).click();
            }
            Reporter("Disabled all Exception filter on the page", "Pass");
        } catch (Exception e) {
            Reporter("Exception while disabling all Exception filter on the page", "Fail");
        }
    }

    public boolean verifyRecordsInDownloadedExcelIsMoreThanOrEqualToTheRecordsInUI(String fileName, long countOfRecordsInUI){
        try{
            long rowCount = excelReader.getNoOfRows(fileName,0);
            if(rowCount>= countOfRecordsInUI){
                Reporter("Downloaded records '"+ rowCount +"' is more than or equal to records in UI '" + countOfRecordsInUI +"'","Pass");
                return true;
            }else {
                Reporter("Downloaded records '"+ rowCount +"' is less than records in UI '" + countOfRecordsInUI +"'","Fail");
                return false;
            }
        }catch (Exception e){
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
}