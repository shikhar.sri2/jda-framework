package com.jda.dct.e2e.clientUi.pages.SupplyModule

import geb.Page


class SupplyShipmentlistPage extends Page {


    static url = "/tempraturecontrolled/shipment"

    static atCheckWaiting = true

    static at = {
        $("#tempraturecontrolled-page-tab-shipments").isDisplayed()

    }

    static content = {

        shipmentno(wait: true) {
            $("#shipment-list-link-item-1").first().children()
        }

        shipmentdetails(wait: true) {
            $("#shipment-detail-title")
        }

//        searchIcon(wait:true){
//            $("div.css-1xa488i > button > span > svg").firstElement()
//        }
        searchInput(wait:true){
            $("div",class:"exanding-search-box-select__value-container")
        }




    }

    def gotoSupplyShipment() {

        waitFor {
            shipmentno.displayed

        }
        shipmentno.click()
        println("welcome to shipment list page")

        shipmentdetails.displayed
    }

    def searchForItem(){
        waitFor {
            searchIcon.displayed
        }
//        searchIcon.click()
//        searchInput << "IS002_SD_S1"
//        searchInput << "${KEY_ENTER}"

    }


}