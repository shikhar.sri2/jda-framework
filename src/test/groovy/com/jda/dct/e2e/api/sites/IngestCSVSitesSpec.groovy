package com.jda.dct.e2e.api.sites

import com.jda.dct.e2e.api.categories.Regression
import com.jda.dct.e2e.api.utility.IngestionHelper
import org.junit.experimental.categories.Category
import spock.lang.Specification
import spock.lang.Unroll

class IngestCSVSitesSpec extends Specification implements IngestionHelper {

    static sitesURL

    def setupSpec() {
        init()

        def sitesPath = properties.getProperty('ingestSitesPath')
        sitesURL = protocol + ip + sitesPath + '?siteType=DC'
    }

    @Category(Regression)
    @Unroll
    def "create and ingest a faulty sites csv: #url"() {
        given: "a faulty sample site, site latitude is not a valid number"
        def filePath = getTmpDir() + 'sites_invalid.csv'
        new File(filePath).newWriter().withWriter {
            it << 'Participant,Site Type,Site,Site Name,Site Owner Name,Longitude,Latitude,Status,Creation Date,Last Modified Date,Site Owner Type\n' +
                    'sears,DC,electrolux dc 333,electrolux dc in ga,e2e test,Below Equator,33.549631,Active,,,DC\n' +
                    'sears,Factory,electrolux dc 222,electrolux dc in ga,e2e test,-84.564924,33.549631,Active,,,DC\n' +
                    'sears,Warehouse,electrolux dc 111,electrolux dc in ga,e2e test,-84.564924,Above Equator,Active,,,DC\n'
        }

        when: "ingestion into DCT"
        def response = postFile(filePath, sitesURL, null)
        def ingestionId = getIngestionId(response)
        def status = waitUntilComplete(ingestionId)

        then: "should have status - completed_with_error"
        status.contains('COMPLETED_WITH_ERROR')

        where:
        url << [sitesURL]
    }
}