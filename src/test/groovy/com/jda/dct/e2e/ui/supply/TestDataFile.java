package com.jda.dct.e2e.ui.supply;

import org.testng.annotations.DataProvider;

import java.lang.reflect.Method;

public class TestDataFile {

    //  extends BaseTest
    @DataProvider(name="UseShipmentId")

    public static Object[][] testData(Method m){
        Object[][] obj=null;

        obj = new Object[1][3]; //making changes to 3 parameters

//Demand Exceptions
        if(m.getName().equals("validatingSearchFeaturePTAUnknownDETC1"))
        {
            obj[0][0] = "OS0024_DL_S3";
            obj[0][1] = "OS0024_DL_S3";
        } if(m.getName().equals("validatingExceptionStatusLateTC2"))
        {
            obj[0][0] = "OS0024_DL_S3"; //showcase  //OS0034_DL_S1
            obj[0][1] = "OS0024_DL_S3"; //showcase  //OS0034_DL_S1

        }if(m.getName().equals("validatingExceptionStatusDelayedTC3"))
        {
            obj[0][0] = "OS0024_DL_S3";//showcase
            obj[0][1] = "OS0024_DL_S3";//showcase

        } if(m.getName().equals("validatingExceptionStatusEarlyTC4"))
        {
            obj[0][0] = "OS0033_SE_S2";//showcase
            obj[0][1] = "OS0033_SE_S2";//showcase

        }if(m.getName().equals("validatingExceptionStatusOnTimeTC5"))
        {
            obj[0][0] = "OS0030_SO_S2";//showcase
            obj[0][1] = "OS0030_SO_S2";//showcase

        }
        if(m.getName().equals("validatingExceptionStatusDelayedTC6"))
        {
            obj[0][0] = "OS0032_SD_S2";//showcase
            obj[0][1] = "OS0032_SD_S2";//showcase

        }if(m.getName().equals("validatingExceptionStatusLateTC7"))
        {
            obj[0][0] = "OS0024_DL_S3";//showcase
            obj[0][1] = "OS0024_DL_S3";//showcase

        } if(m.getName().equals("validatingExceptionStatusDelayedTC8"))
        {
            obj[0][0] = "OS0032_SD_S2";//showcase
            obj[0][1] = "OS0032_SD_S2";//showcase

        }if(m.getName().equals("validatingExceptionStatusEarlyTC9"))
        {
            obj[0][0] = "OS0033_SE_S2";//showcase
            obj[0][1] = "OS0033_SE_S2";//showcase

        }

        if(m.getName().equals("validatingExceptionStatusOnTimeTC10"))
        {
            obj[0][0] = "OS0030_SO_S2";//showcase
            obj[0][1] = "OS0030_SO_S2";//showcase

        }
        if(m.getName().equals("validatingExceptionStatusLateTC11"))
        {
            obj[0][0] = "OS0024_DL_S3";//showcase
            obj[0][1] = "OS0024_DL_S3";//showcase

        }if(m.getName().equals("validatingExceptionStatusDelayedTC12"))
        {
            obj[0][0] = "OD0038_SL";//showcase
            obj[0][1] = "OD0038_SL";//showcase

        } if(m.getName().equals("validatingExceptionStatusEarlyTC13"))
        {
            obj[0][0] = "OD0032_SE_ML";//showcase
            obj[0][1] = "OD0032_SE_ML";//showcase

        }


        if(m.getName().equals("validatingExceptionStatusOnTimeTC14"))
        {
            obj[0][0] = "OD0036_SO";//showcase
            obj[0][1] = "OD0036_SO";//showcase

        }

        if(m.getName().equals("validatingExceptionStatusNoExceptionsTC15"))
        {
            obj[0][0] = "OD0036_SO";
            obj[0][1] = "OD0036_SO";

        }if(m.getName().equals("validatingExceptionStatusPastDuesTC16")) // zero
        {
            obj[0][0] = "SO0017_SO_ML";//showcase
            obj[0][1] = "SO0017_SO_ML";//showcase
        }

        if(m.getName().equals("validatingExceptionStatusLateTC17"))
        {
            obj[0][0] = "SO0007_CL";//showcase
            obj[0][1] = "SO0007_CL";//showcase

        }

        if(m.getName().equals("validatingExceptionStatusUnconfirmedTC18")) //zero
        {
            obj[0][0] = "SO0071_NO";//showcase
            obj[0][1] = "SO0071_NO";//showcase

        }


        if(m.getName().equals("validatingExceptionStatusOnTimeTC19"))//zero
        {
            obj[0][0] = "SO0014_CO_ML";//showcase
            obj[0][1] = "SO0014_CO_ML";//showcase


        }if(m.getName().equals("validatingExceptionStatusStockOutTC20"))//zero
        {
            obj[0][0] = "AC-FG-023";//showcase
            obj[0][1] = "AC-FG-023";//showcase

        }


        if(m.getName().equals("validatingLowStockStatusTC21"))
        {
            obj[0][0] = "AC-FG-032";
            obj[0][1] = "AC-FG-032";

        }

        if(m.getName().equals("validatingSOverstockStatusTC22"))
        {
            obj[0][0] = "AC-FG-028";
            obj[0][1] = "AC-FG-028";

        }

        if(m.getName().equals("validatingOnTrackStatusTC23"))  //stop
        {
            obj[0][0] = "AC-FG-008";
            obj[0][1] = "AC-FG-008";

        }

        if(m.getName().equals("validatingStatesStatusNewTC24"))  //stop
        {
            obj[0][0] = "OS0032_SD_S2"; //showcase zero ocean
            obj[0][1] = "OS0032_SD_S2"; //showcase zero ocean

        }



        if(m.getName().equals("validatingStatesStatusPlannedShipmentTC25"))
        {
            obj[0][0] = "OS0024_DL_S3";//showcase zero ocean
            obj[0][1] = "OS0024_DL_S3";//showcase zero ocean

        }if(m.getName().equals("validatingStatesStatusShippedTC26"))
        {
            obj[0][0] = "OS0033_SE_S2"; //showcase ocean
            obj[0][1] = "OS0033_SE_S2"; //showcase ocean

        } if(m.getName().equals("validatingStatesStatusInTransitTC27"))
        {
            obj[0][0] = "OS0024_DL_S3";//showcase zero ocean
            obj[0][1] = "OS0024_DL_S3";//showcase zero ocean

        }if(m.getName().equals("validatingStatesStatusDeliveredTC28"))
        {
            obj[0][0] = "OS0024_DL_S3"; //ocean
            obj[0][1] = "OS0024_DL_S3"; //ocean

        }if(m.getName().equals("validatingStatesStatusReceivedTC29"))
        {
            obj[0][0] = "OS0024_DL_S3"; //ocean zero
            obj[0][1] = "OS0024_DL_S3"; //ocean zero

        } if(m.getName().equals("validatingStatesStatusPlannedShipmentTC30"))
        {
            obj[0][0] = "OS0024_DL_S3";//showcase zero ocean
            obj[0][1] = "OS0024_DL_S3";//showcase zero ocean

        }

        if(m.getName().equals("validatingStatesStatusShippedTC31"))
        {
            obj[0][0] = "OS0033_SE_S2";//showcase ocean
            obj[0][1] = "OS0033_SE_S2";//showcase ocean

        }if(m.getName().equals("validatingStatesStatusInTransitTC32"))
        {
            obj[0][0] = "OS0024_DL_S3";//showcase zero ocean
            obj[0][1] = "OS0024_DL_S3";//showcase zero ocean

        } if(m.getName().equals("validatingStatesStatusDeliveredTC33"))
        {
            obj[0][0] = "OS0024_DL_S3";//showcase ocean
            obj[0][1] = "OS0024_DL_S3";//showcase ocean

        }if(m.getName().equals("validatingStatesStatusShippedTC34"))
        {
            obj[0][0] = "OD0039_SE";//showcase ocean
            obj[0][1] = "OD0039_SE";//showcase ocean

        }if(m.getName().equals("validatingStatesStatusDeliveredTC35"))
        {
            obj[0][0] = "OD0039_SE";//showcase ocean zero
            obj[0][1] = "OD0039_SE";//showcase ocean zero

        } if(m.getName().equals("validatingStateStatusClosedTC36"))
        {
            obj[0][0] = "OD0039_SE";//showcase zero ocean
            obj[0][1] = "OD0039_SE";//showcase zero ocean

        }


        if(m.getName().equals("validatingStatesStatusReceivedTC37"))
        {
            obj[0][0] = "0182223995";//showcase zero
            obj[0][1] = "0182223995";//showcase zero

        }if(m.getName().equals("validatingStatesStatusShippedTC38"))
        {
            obj[0][0] = "OD0039_SE";//showcase ocean
            obj[0][1] = "OD0039_SE";//showcase ocean

        } if(m.getName().equals("validatingStatesStatusNewTC39"))
        {
            obj[0][0] = "SO0072_NO";//showcase zero
            obj[0][1] = "SO0072_NO";//showcase zero

        }if(m.getName().equals("validatingStatesStatusOpenTC40"))
        {
            obj[0][0] = "SO0073_OC";//showcase zero
            obj[0][1] = "SO0073_OC";//showcase zero

        }if(m.getName().equals("validatingStatesStatusConfirmedWithChangesTC41"))
        {
            obj[0][0] = "SO0074_CC";//showcase zero
            obj[0][1] = "SO0074_CC";//showcase zero

        } if(m.getName().equals("validatingStatesStatusConfirmedTC42"))
        {
            obj[0][0] = "SO0051_CL";//showcase zero
            obj[0][1] = "SO0051_CL";//showcase zero

        }

        if(m.getName().equals("validatingStatesStatusPlannedShipmentTC43"))
        {
            obj[0][0] = "SO0012_SL";//showcase zero
            obj[0][1] = "SO0012_SL";//showcase zero

        }if(m.getName().equals("validatingStatesStatusShippedTC44"))
        {
            obj[0][0] = "SO88883_SL";//showcase
            obj[0][1] = "SO88883_SL";//showcase

        } if(m.getName().equals("validatingStatesStatusReceivedTC45"))
        {
            obj[0][0] = "SO0031_DR";//showcase zero
            obj[0][1] = "SO0031_DR";//showcase zero

        }if(m.getName().equals("validatingStatesStatusRejectedTC46"))
        {
            obj[0][0] = "SO0046_DR";//showcase zero
            obj[0][1] = "SO0046_DR";//showcase zero

        }if(m.getName().equals("validatingStatesStatusClosedTC47"))
        {
            obj[0][0] = "SO0012_SL";//showcase zero
            obj[0][1] = "SO0012_SL";//showcase zero

        } if(m.getName().equals("validatingSearchFeatureCreatedDSTC48")) //Case commented out
        {
            obj[0][0] = "SO0012_SL";
            obj[0][1] = "SO0012_SL";

        }

        if(m.getName().equals("validatingSearchFeatureDeliveredDSTC49"))//Case commented out
        {
            obj[0][0] = "SO0012_SL";
            obj[0][1] = "SO0012_SL";

        }
        if(m.getName().equals("validatingStatesStatusStockOutTC50"))
        {
            obj[0][0] = "AC-FG-023";
            obj[0][1] = "AC-FG-023";

        }

        if(m.getName().equals("validatingStatesStatusLowStockTC51"))
        {
            obj[0][0] = "AC-FG-032";
            obj[0][1] = "AC-FG-032";

        }

        if(m.getName().equals("validatingStatesStatusOverstockTC52"))
        {
            obj[0][0] = "AC-FG-034";
            obj[0][1] = "AC-FG-034";

        }

        if(m.getName().equals("validatingStatesStatusOnTrackTC53"))
        {
            obj[0][0] = "AC-FG-008";
            obj[0][1] = "AC-FG-008";

        }




//Supply Exceptions

        if(m.getName().equals("validatingSupplyExceptionsStatusLateTC54"))
        {
            obj[0][0] = "IS0036_DL_S2";
            obj[0][1] = "IS0036_DL_S2";

        }if(m.getName().equals("validatingSupplyExceptionsStatusDelayedTC55"))
        {
            obj[0][0] = "IS0034_SD_S2";
            obj[0][1] = "IS0034_SD_S2";

        }

        if(m.getName().equals("validatingSupplyExceptionsStatusEarlyTC56")) //zero value
        {
            obj[0][0] = "IS0036_SD_S2";
            obj[0][1] = "IS0036_SD_S2";

        }

        if(m.getName().equals("validatingSupplyExceptionsStatusOnTimeTC57"))
        {
            obj[0][0] = "IS0043_SO_S2";
            obj[0][1] = "IS0043_SO_S2";

        }if(m.getName().equals("validatingSupplyExceptionsStatusDelayedTC58"))
        {
            obj[0][0] = "IS0034_SD_S2";
            obj[0][1] = "IS0034_SD_S2";

        } if(m.getName().equals("validatingSupplyExceptionsStatusLateTC59"))
        {
            obj[0][0] = "IS0034_DL_S2";
            obj[0][1] = "IS0034_DL_S2";

        }if(m.getName().equals("validatingSupplyExceptionsStatusDelayedTC60"))
        {
            obj[0][0] = "IS0034_SD_S2";
            obj[0][1] = "IS0034_SD_S2";
        }
        if(m.getName().equals("validatingSupplyExceptionsStatusEarlyTC61")) //zero data
        {
            obj[0][0] = "IS0032_DL_S2";
            obj[0][1] = "IS0032_DL_S2";

        }if(m.getName().equals("validatingSupplyExceptionsStatusLateTC62"))
        {
            obj[0][0] = "ID0036_DL";
            obj[0][1] = "ID0036_DL";

        } if(m.getName().equals("validatingSupplyExceptionsStatusDelayedTC63"))
        {
            obj[0][0] = "ISPO0003_CO";
            obj[0][1] = "ISPO0003_CO";

        }

        if(m.getName().equals("validatingSupplyExceptionsStatusEarlyTC64"))//Zero data
        {
            obj[0][0] = "CFRQ815124";
            obj[0][1] = "CFRQ815124";

        }
        if(m.getName().equals("validatingSupplyExceptionsStatusOnTimeTC65"))//Zero data
        {
            obj[0][0] = "ID0058_SO";
            obj[0][1] = "ID0058_SO";

        }if(m.getName().equals("validatingSupplyExceptionsStatusPastDueTC66"))
        {
            obj[0][0] = "PO0016_SO"; //OrderNo
            obj[0][1] = "PO0016_SO"; //OrderNo


        }

        if(m.getName().equals("validatingSupplyExceptionsStatusLateTC67")) //zero data
        {
            obj[0][0] = "PO0024_CO";
            obj[0][1] = "PO0024_CO";

        }

        if(m.getName().equals("validatingSupplyExceptionsStatusUnconfirmedTC68"))
        {
            obj[0][0] = "PO0053_NO";
            obj[0][1] = "PO0053_NO";

        }

        if(m.getName().equals("validatingSupplyExceptionsStatusOnTimeTC69"))
        {
            obj[0][0] = "PO0005_CO";
            obj[0][1] = "PO0005_CO";

        }

        if(m.getName().equals("validatingSupplyExceptionsStatusPastDueTC70")) //zero data
        {
            obj[0][0] = "PO0059_CC";
            obj[0][1] = "PO0059_CC";

        }

        if(m.getName().equals("validatingSupplyExceptionsStatusStockOutTC71"))
        {
            obj[0][0] = "AC-FG-023";
            obj[0][1] = "AC-FG-023";


        } if(m.getName().equals("validatingSupplyExceptionsStatusLowStockTC72"))
        {
            obj[0][0] = "AC-FG-032";
            obj[0][1] = "AC-FG-032";

        }if(m.getName().equals("validatingSupplyStatusOverstockTC73"))
        {
            obj[0][0] = "AC-FG-034";
            obj[0][1] = "AC-FG-034";

        } if(m.getName().equals("validatingSupplyStatusOnTrackSETC74"))
        {
            obj[0][0] = "AC-FG-008";
            obj[0][1] = "AC-FG-008";

        }if(m.getName().equals("validatingSupplyStatusForecastCommitMismatchTC75"))
        {
            obj[0][0] = "AC-CP-002";
            obj[0][1] = "AC-CP-002";

        }if(m.getName().equals("validatingSupplyStatusNewForecastTC76"))//zero data
        {
            obj[0][0] = "AC-CP-001";
            obj[0][1] = "AC-CP-001";

        } if(m.getName().equals("validatingSupplyStatusForecastRevisedTC77"))//zero data
        {
            obj[0][0] = "1231235453";
            obj[0][1] = "1231235453";

        }

        if(m.getName().equals("validatingSupplyStatusForecastCommitInterlockTC78"))//zero data
        {
            obj[0][0] = "ID002_DL";
            obj[0][1] = "ID002_DL";

        }

        //Supply States Start


        if(m.getName().equals("validateStatesStatusNewTC79"))//zero value
        {
            obj[0][0] = "ISPO0003_CO";
            obj[0][1] = "ISPO0003_CO";

        }if(m.getName().equals("validateStatesStatusPlannedShipmentTC80"))
        {
            obj[0][0] = "IS003_CO";
            obj[0][1] = "IS003_CO";

        } if(m.getName().equals("validateStatesStatusShippedTC81"))
        {
            obj[0][0] = "IS009_SO_S1"; //Road data
            obj[0][1] = "IS009_SO_S1";

        }if(m.getName().equals("validateStatesStatusInTransitTC82"))
        {
            obj[0][0] = "IS002_DL_S1";
            obj[0][1] = "IS002_DL_S1";

        }if(m.getName().equals("validateStatesStatusDeliveredTC83"))
        {
            obj[0][0] = "IS001_DL_S1"; //Road data  ITEST
            obj[0][1] = "IS001_DL_S1";

        } if(m.getName().equals("validateStatesStatusReceivedTC84"))
        {
            obj[0][0] = "IS002_RO_S1"; //Road data itest
            obj[0][1] = "IS002_RO_S1";

        }

        if(m.getName().equals("validateStatesStatusPlannedShipmentTC85"))
        {
            obj[0][0] = "IS003_CO"; //Road Data
            obj[0][1] = "IS003_CO";

        }if(m.getName().equals("validateStatesStatusShippedTC86"))
        {
            obj[0][0] = "IS003_SD_S1"; // Road Data
            obj[0][1] = "IS003_SD_S1";

        } if(m.getName().equals("validateStatesStatusInTransitTC87"))
        {
            obj[0][0] = "IS001_SD_S1";
            obj[0][1] = "IS001_SD_S1";

        }if(m.getName().equals("validateStatesStatusDeliveredTC88"))
        {
            obj[0][0] = "IS001_DL_S1"; //Road Data
            obj[0][1] = "IS001_DL_S1";

        }if(m.getName().equals("validateStatesStatusInTransitTC89"))
        {
            obj[0][0] = "ID00110_SD";
            obj[0][1] = "ID00110_SD";

        } if(m.getName().equals("validateStatesStatusReceivedTC90"))
        {
            obj[0][0] = "ID003_RO";//Road
            obj[0][1] = "ID003_RO";

        }

        if(m.getName().equals("validateStatesStatusPlannedShipmentTC91")) //zero data
        {
            obj[0][0] = "ID001_CO"; //Road
            obj[0][1] = "ID001_CO";

        }if(m.getName().equals("validateStatesStatusReceivedTC92"))
        {
            obj[0][0] = "ID002_RO";
            obj[0][1] = "ID002_RO";

        } if(m.getName().equals(""))
        {
            obj[0][0] = "PO0030_SO_ML";
            obj[0][1] = "PO0030_SO_ML";

        }if(m.getName().equals("validateStatesStatusShippedTC94")) //zero value
        {
            obj[0][0] = "PO0003_SO";
            obj[0][1] = "PO0003_SO";

        }if(m.getName().equals("validateStatesStatusPlannedShipmentTC95"))
        {
            obj[0][0] = "PO0027_CO";
            obj[0][1] = "PO0027_CO";

        } if(m.getName().equals("validateStatesStatusOpenTC96"))
        {
            obj[0][0] = "PO0058_OC";
            obj[0][1] = "PO0058_OC";

        }

        if(m.getName().equals("validateStatesStatusNewTC97"))
        {
            obj[0][0] = "PO0053_NO";
            obj[0][1] = "PO0053_NO";

        }if(m.getName().equals("validateStatesStatusCancelledTC98"))
        {
            obj[0][0] = "PO0057_CA";
            obj[0][1] = "PO0057_CA";

        } if(m.getName().equals("validateStatesStatusConfirmedTC99"))
        {
            obj[0][0] = "PO0059_CC";
            obj[0][1] = "PO0059_CC";

        }if(m.getName().equals("validateStatesStatusConfirmedWithChangesTC100"))
        {
            obj[0][0] = "ANDE686799";
            obj[0][1] = "ANDE686799";

        }if(m.getName().equals("validateStatesStatusClosedTC101"))
        {
            obj[0][0] = "MSMSCN3829172";
            obj[0][1] = "MSMSCN3829172";

        } if(m.getName().equals("validateStatesStatusReceivedTC102"))
        {
            obj[0][0] = "1231235453";
            obj[0][1] = "1231235453";

        }

        if(m.getName().equals("validateStatesStatusRejectedTC103"))
        {
            obj[0][0] = "ID0031_DL";
            obj[0][1] = "ID0031_DL";

        }if(m.getName().equals("validateStatesStatusStockOutTC104"))
        {
            obj[0][0] = "AC-FG-032";
            obj[0][1] = "AC-FG-032";

        } if(m.getName().equals("validateStatesStatusLowStockTC105"))
        {
            obj[0][0] = "AC-FG-032";
            obj[0][1] = "AC-FG-032";

        }if(m.getName().equals("validateStatesStatusOverstockTC106"))
        {
            obj[0][0] = "AC-FG-034";
            obj[0][1] = "AC-FG-034";

        }if(m.getName().equals("validateStatesStatusOnTrackTC107"))
        {
            obj[0][0] = "AC-FG-008";
            obj[0][1] = "AC-FG-008";

        }

        if(m.getName().equals("validateMonetaryImpactOnSalesAndDemandTC108"))
        {
            obj[0][0] = "IS001_SD_S1";
            obj[0][1] = "IS001_SD_S1";
            obj[0][2] = "AC-FG-010";

        }

        return obj;
    }
}
