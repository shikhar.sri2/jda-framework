package com.jda.dct.e2e.api

import com.jda.dct.e2e.api.utility.AuthHelper
import groovyx.net.http.RESTClient
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll

/**
 * GET method to retrieve demand delivery detail configuration
 *
 */

// may be redundant, check and delete on confirmation
class GetDeliveryDetailConfigSpec extends Specification implements AuthHelper {
    static deliveryDetailConfigURL
    @Shared
    def client

    def setupSpec() {
        init()

        def deliveryDetailConfigPath = properties.getProperty('getDeliveryDetailConfigPath')
        deliveryDetailConfigURL = protocol + ip + deliveryDetailConfigPath
        client = new RESTClient(deliveryDetailConfigURL)
        client.handler.failure = { resp -> resp }
        header[HttpHeaders.CONTENT_TYPE] = MediaType.APPLICATION_JSON
    }

    @Unroll
    def "Get deliveries configuration API #url"() {
        given: "The API URL is #url "
        when: "HTTP Method is GET: should return demand delivery detail configuration"
        def response = client.get(headers: header)
        then: "the response should be 200"
        with(response) {
            status == 200
        }
        where:
        url << [deliveryDetailConfigURL]

    }
}
