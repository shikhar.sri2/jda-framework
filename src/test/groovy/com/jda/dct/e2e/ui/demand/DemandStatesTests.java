//package com.jda.dct.e2e.ui.demand;
//
//import BaseTest;
//import LeftMenu;
//import TopMenuPage;
//import TopSubMenuPage;
//import TestDataFile;
//import org.openqa.selenium.WebDriver;
//import org.testng.Assert;
//import org.testng.annotations.BeforeClass;
//import org.testng.annotations.Test;
//
//public class DemandStatesTests extends BaseTest {
//
//
//    private LeftMenu leftMenu;
//    private TopMenuPage topMenu;
//    private TopSubMenuPage topSubMenu;
//    private WebDriver driver;
//
//    @BeforeClass
//    public void setUp() throws InterruptedException {
//        System.out.println("Starting tests for :" + this.getClass().getName());
////Initialize required objects
//        driver = getDriver();
//        leftMenu = new LeftMenu(driver);
//        topMenu = new TopMenuPage(driver);
//        topSubMenu = new TopSubMenuPage(driver);
//
//        logInToApplication();
//        //leftMenu.clickInventoryMenuOption();
//        leftMenu.clickDemandMenuOption();
//
//    }
//
//
//    //shipped, delivered, Received
//    @Test(priority = 24, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
//    public void validatingStatesStatusNewTC24(String shipmentNo24a,String shipmentNo24b)
//    {
//        //driver.navigate().refresh();
//
//        topMenu.goToDemandShipmentTab();
//        topMenu.clickStatesTab(); //zero
//        topSubMenu.clickOceanIcon();
//        topMenu.clickDemandShipmentStatesNew();
//        topSubMenu.clickSearchButton();
//        topSubMenu.goToSearchFeature(shipmentNo24a);
//        Assert.assertTrue(topMenu.isDemandShipmentStatesNewVisible());
//        topMenu.assertActualAndExpectedDemandShipmentNumberEqualOcean(shipmentNo24b);
//         topMenu.clearFilter();
//        driver.navigate().refresh();
//    }
//
//    @Test(priority = 25, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
//    public void validatingStatesStatusPlannedShipmentTC25(String shipmentNo25a,String shipmentNo25b)
//    {
//        topMenu.goToDemandShipmentTab();//zero
//        topSubMenu.clickOceanIcon();
//        topMenu.clickStatesTab();
//        topMenu.clickDemandShipmentStatesPlannedShipment(); // webDriverWait affects execution
//        topSubMenu.clickSearchButton();
//        topSubMenu.goToSearchFeature(shipmentNo25a);
//        Assert.assertTrue(topMenu.isDemandShipmentStatesPlannedShipmentVisible());
//         topMenu.assertActualAndExpectedDemandShipmentNumberEqualOcean(shipmentNo25b);
//        topMenu.clearFilter();
//        driver.navigate().refresh();
//    }
//
//    @Test(priority = 26, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
//    public void validatingStatesStatusShippedTC26(String shipmentNo26a,String shipmentNo26b) throws InterruptedException {
//        topMenu.goToDemandShipmentTab();
//        topMenu.clickStatesTab();
//        topSubMenu.clickOceanIcon();
//        topMenu.clickDemandShipmentStatesShipped();
//        topSubMenu.clickSearchButton();
//        topSubMenu.goToSearchFeature(shipmentNo26a);
//        Assert.assertTrue(topMenu.isDemandShipmentStatesShippedVisible());
//        topMenu.assertActualAndExpectedDemandShipmentNumberEqualOcean(shipmentNo26b);
//        topMenu.clearFilter();
//        driver.navigate().refresh();
//    }
//
//
//    @Test(priority = 28, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
//    public void validatingStatesStatusDeliveredTC28(String shipmentNo28a,String shipmentNo28b) throws InterruptedException {
//        topMenu.goToDemandShipmentTab();
//        topSubMenu.clickOceanIcon();
//        topMenu.clickStatesTab();
//        topMenu.clickDemandShipmentStatesDelivered();
//        topSubMenu.clickSearchButton();
//        topSubMenu.goToSearchFeature(shipmentNo28a);
//        Assert.assertTrue(topMenu.isDemandShipmentStatesDeliveredVisible());
//        topMenu.assertActualAndExpectedDemandShipmentNumberEqualOcean(shipmentNo28b);
//         topMenu.clearFilter();
//        driver.navigate().refresh();
//    }
//
//    @Test(priority = 29, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
//    public void validatingStatesStatusReceivedTC29(String shipmentNo29a, String shipmentNo29b) throws InterruptedException
//    {
//        topMenu.goToDemandShipmentTab();
//        topSubMenu.clickOceanIcon();
//        topMenu.clickStatesTab();
//        topSubMenu.clickDemandDeliverySliderIcon();
//        topMenu.clickDemandShipmentStatesReceived();
//        topSubMenu.clickSearchButton();
//        topSubMenu.goToSearchFeature(shipmentNo29a);
//        Assert.assertTrue(topMenu.isDemandShipmentStatesReceivedVisible());
//         topMenu.assertActualAndExpectedDemandShipmentNumberEqualOcean(shipmentNo29b);
//         topMenu.clearFilter();
//        driver.navigate().refresh();
//    }
//
//    @Test(priority = 30, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
//    public void validatingStatesStatusPlannedShipmentTC30(String shipmentNo30a, String shipmentNo30b)
//    {
//        topMenu.goToDemandShipmentTab();
//        topSubMenu.clickOceanIcon();
//        topMenu.clickStatesTab();
//        topMenu.clickDemandShipmentStatesPlannedShipment();
//        topSubMenu.clickSearchButton();
//        topSubMenu.goToSearchFeature(shipmentNo30a);
//        Assert.assertTrue(topMenu.isDemandShipmentStatesPlannedShipmentVisible());
//        //  topMenu.assertActualAndExpectedDemandShipmentNumberEqualOcean(shipmentNo30b);
//        topMenu.clearFilter();
//        driver.navigate().refresh();
//    }
//
//    @Test(priority = 31, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
//    public void validatingStatesStatusShippedTC31(String shipmentNo31a, String shipmentNo31b) throws InterruptedException {
//        topMenu.goToDemandShipmentTab();
//        topSubMenu.clickOceanIcon();
//        topMenu.clickStatesTab();
//        topMenu.clickDemandShipmentStatesShipped();
//        topSubMenu.clickSearchButton();
//        topSubMenu.goToSearchFeature(shipmentNo31a);
//        Assert.assertTrue(topMenu.isDemandShipmentStatesShippedVisible());
//        topMenu.assertActualAndExpectedDemandShipmentNumberEqualOcean(shipmentNo31b);
//        topMenu.clearFilter();
//        driver.navigate().refresh();
//
//    }
//
//    @Test(priority = 32, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
//    public void validatingStatesStatusInTransitTC32(String shipmentNo32a, String shipmentNo32b)
//    {
//        topMenu.goToDemandShipmentTab();
//        topSubMenu.clickOceanIcon();
//        topMenu.clickStatesTab();
//        topMenu.clickDemandShipmentStatesInTransit();
//        topSubMenu.clickSearchButton();
//        topSubMenu.goToSearchFeature(shipmentNo32a);
//        Assert.assertTrue(topMenu.isDemandShipmentStatesInTransitVisible());
//        //topMenu.assertActualAndExpectedDemandShipmentNumberEqualOcean(shipmentNo32b);
//        topMenu.clearFilter();
//        driver.navigate().refresh();
//
//
//    }   //stopped in two minutes
//
//    @Test(priority = 33, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
//    public void validatingStatesStatusDeliveredTC33(String shipmentNo33a, String shipmentNo33b) throws InterruptedException {
//        topMenu.goToDemandShipmentTab();
//        topSubMenu.clickOceanIcon();
//        topMenu.clickStatesTab();
//        topMenu.clickDemandShipmentStatesDelivered();
//        topSubMenu.clickSearchButton();
//        topSubMenu.goToSearchFeature(shipmentNo33a);
//        Assert.assertTrue(topMenu.isDemandShipmentStatesDeliveredVisible());
//        topMenu.assertActualAndExpectedDemandShipmentNumberEqualOcean(shipmentNo33b);
//        topMenu.clearFilter();
//        driver.navigate().refresh();
//        topMenu.gotoDemandDeliveryTab();//
//
//    }
//
//
//    @Test(priority = 34, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
//    public void validatingStatesStatusShippedTC34(String shipmentNo34a, String shipmentNo34b)
//    {
//        // topMenu.gotoDemandDeliveryTab();
//        topSubMenu.clickOceanIcon();
//        topMenu.clickStatesTab();
//        topMenu.clickDemandDeliveryStatesShipped();
//        topSubMenu.clickSearchButton();
//        topSubMenu.goToSearchFeature(shipmentNo34a);
//        Assert.assertTrue(topMenu.isdemandDeliveryStatesShippedVisible());
//         topMenu.assertActualAndExpectedDemandDeliveryNumberEqualOcean(shipmentNo34b);
//        topMenu.clearFilter();
//        driver.navigate().refresh();
//    }
//
//    @Test(priority = 35, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
//    public void validatingStatesStatusDeliveredTC35(String shipmentNo35a, String shipmentNo35b)
//    {
//        topMenu.gotoDemandDeliveryTab();
//        topSubMenu.clickOceanIcon();
//        topMenu.clickStatesTab();
//        topMenu.clickDemandDeliveryStatesDelivered();
//        topSubMenu.clickSearchButton();
//        topSubMenu.goToSearchFeature(shipmentNo35a);
//        Assert.assertTrue(topMenu.isDemandDeliveryStatesDeliveredVisible());
//          topMenu.assertActualAndExpectedDemandDeliveryNumberEqualOcean(shipmentNo35b);
//
//        topMenu.clearFilter();
//        driver.navigate().refresh();
//    }
//
//    @Test(priority = 36, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
//    public void validatingStateStatusClosedTC36(String shipmentNo36a, String shipmentNo36b)
//    //States and Exceptions status are interchanges causing cases to fail
//    {
//        topMenu.gotoDemandDeliveryTab();
//        topSubMenu.clickOceanIcon();
//        topMenu.clickStatesTab();
//        topMenu.clickDemandDeliveryStatesClosed();
//        topSubMenu.clickSearchButton();
//        topSubMenu.goToSearchFeature(shipmentNo36a);
//        Assert.assertTrue(topMenu.isdemandDeliveryStatesClosedVisible());
//         topMenu.assertActualAndExpectedDemandDeliveryNumberEqualOcean(shipmentNo36b);
//        topMenu.clearFilter();
//        driver.navigate().refresh();
//    }
//
//    @Test(priority = 37, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
//    public void validatingStatesStatusReceivedTC37(String shipmentNo37a, String shipmentNo37b)
//    {
//        topMenu.gotoDemandDeliveryTab();
//        topSubMenu.clickOceanIcon();
//        topMenu.clickStatesTab();
//        topMenu.clickDemandDeliveryStatesReceived();
//        topSubMenu.clickSearchButton();
//        topSubMenu.goToSearchFeature(shipmentNo37a);
//        Assert.assertTrue(topMenu.isdemandDeliveryStatesReceivedVisible());
//        topMenu.assertActualAndExpectedDemandDeliveryNumberEqualOcean(shipmentNo37b);
//        topMenu.clearFilter();
//        driver.navigate().refresh();
//    }
//
//
//    @Test(priority = 38, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
//    public void validatingStatesStatusShippedTC38(String shipmentNo38a, String shipmentNo38b)
//    {
//        topMenu.gotoDemandDeliveryTab();
//        topSubMenu.clickOceanIcon();
//        topMenu.clickStatesTab();
//        topMenu.clickDemandDeliveryStatesShipped();
//        topSubMenu.clickSearchButton();
//        topSubMenu.goToSearchFeature(shipmentNo38a);
//        topMenu.assertActualAndExpectedDemandDeliveryNumberEqualOcean(shipmentNo38b);
//        Assert.assertTrue(topMenu.isdemandDeliveryStatesShippedVisible());
//        topMenu.clearFilter();
//        driver.navigate().refresh();
//    }
//
////    //State Order
//
//
//    @Test(priority = 39, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
//    public void validatingStatesStatusNewTC39(String shipmentNo39a, String shipmentNo39b)
//    {
//        topMenu.goToDemandOrdersTab();
//        topMenu.clickStatesTab();
//        topMenu.clickDemandOrdersStatesNew();
//        topSubMenu.clickSearchButton();
//        topSubMenu.goToSearchFeature(shipmentNo39a);
//        Assert.assertTrue(topMenu.isdemandOrdersStatesNewVisible());
//        topMenu.assertActualAndExpectedDemandOrdersNumberEqual(shipmentNo39b);
//        topMenu.clearFilter();
//        driver.navigate().refresh();
//    }
//
//    @Test(priority = 40, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
//    public void validatingStatesStatusOpenTC40(String shipmentNo40a, String shipmentNo40b)
//    {
//        topMenu.goToDemandOrdersTab();
//        topMenu.clickStatesTab();
//        topMenu.clickDemandOrdersStatesOpen();
//        topSubMenu.clickSearchButton();
//        topSubMenu.goToSearchFeature(shipmentNo40a);
//        Assert.assertTrue(topMenu.isDemandOrdersStatesOpenVisible());
//        topMenu.assertActualAndExpectedDemandOrdersNumberEqual(shipmentNo40a);
//        topMenu.clearFilter();
//        driver.navigate().refresh();
//    }
//    //
//    @Test(priority = 41, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
//    public void validatingStatesStatusConfirmedWithChangesTC41(String shipmentNo41a, String shipmentNo41b)
//    {
//        topMenu.goToDemandOrdersTab();
//        topMenu.clickStatesTab();
//        topMenu.clickDemandOrdersStatesConfirmedWithChanges();
//        topSubMenu.clickSearchButton();
//        topSubMenu.goToSearchFeature(shipmentNo41a);
//        Assert.assertTrue(topMenu.isDemandOrdersStatesConfirmedWithChangesVisible());
//        topMenu.assertActualAndExpectedDemandOrdersNumberEqual(shipmentNo41b);
//        topMenu.clearFilter();
//        driver.navigate().refresh();
//    }
//    //
//    @Test(priority = 42, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
//    public void validatingStatesStatusConfirmedTC42(String shipmentNo42a, String shipmentNo42b)
//    {
//        topMenu.goToDemandOrdersTab();
//        topMenu.clickStatesTab();
//        topMenu.clickDemandOrdersStatesConfirmed();
//        topSubMenu.clickSearchButton();
//        topSubMenu.goToSearchFeature(shipmentNo42a);
//        Assert.assertTrue(topMenu.isDemandOrdersStatesConfirmedVisible());
//        topMenu.assertActualAndExpectedDemandOrdersNumberEqual(shipmentNo42b);
//        topMenu.clearFilter();
//        driver.navigate().refresh();
//    }
//
//    //
//    @Test(priority = 43, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
//    public void validatingStatesStatusPlannedShipmentTC43(String shipmentNo43a, String shipmentNo43b)
//    {
//        topMenu.goToDemandOrdersTab();
//        topMenu.clickStatesTab();
//        topMenu.clickDemandOrdersStatesPlannedShipment();
//        topSubMenu.clickSearchButton();
//        topSubMenu.goToSearchFeature(shipmentNo43a);
//        Assert.assertTrue(topMenu.isDemandOrdersStatesPlannedShipmentVisible());
//        topMenu.assertActualAndExpectedDemandOrdersNumberEqual(shipmentNo43b);
//        topMenu.clearFilter();
//        driver.navigate().refresh();
//    }
//
//    //
//    @Test(priority = 44, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
//    public void validatingStatesStatusShippedTC44(String shipmentNo44a, String shipmentNo44b) throws InterruptedException
//    {
//        topMenu.goToDemandOrdersTab();
//        topMenu.clickStatesTab();
//        topSubMenu.clickDemandOrderSliderIcon();
//        topMenu.clickDemandOrdersStatesShipped();
//        topSubMenu.clickSearchButton();
//        topSubMenu.goToSearchFeature(shipmentNo44a);
//        Assert.assertTrue(topMenu.isDemandOrdersStatesShippedVisible());
//        topMenu.assertActualAndExpectedDemandOrdersNumberEqual(shipmentNo44b);
//        topMenu.clearFilter();
//        driver.navigate().refresh();
//    }
//
//    //
//    @Test(priority = 45, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
//    public void validatingStatesStatusReceivedTC45(String shipmentNo45a, String shipmentNo45b) throws InterruptedException {
//        topMenu.goToDemandOrdersTab();
//        topMenu.clickStatesTab();
//        topSubMenu.clickDemandOrderSliderIcon();
//        topSubMenu.clickDemandOrderSliderIcon();
//        topMenu.clickDemandOrdersStatesReceived();
//        topSubMenu.clickSearchButton();
//        topSubMenu.goToSearchFeature(shipmentNo45a);
//        Assert.assertTrue(topMenu.isDemandOrdersStatesReceivedVisible());
//        topMenu.assertActualAndExpectedDemandOrdersNumberEqual(shipmentNo45b);
//        topMenu.clearFilter();
//        driver.navigate().refresh();
//    }
//
//
//    //
//    @Test(priority = 46, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
//    public void validatingStatesStatusRejectedTC46(String shipmentNo46a, String shipmentNo46b) throws InterruptedException {
//        topMenu.goToDemandOrdersTab();
//        topMenu.clickStatesTab();
//        topSubMenu.clickDemandOrderSliderIcon();
//        topSubMenu.clickDemandOrderSliderIcon();
//        topSubMenu.clickDemandOrderSliderIcon();
//        topMenu.clickDemandOrdersStatesRejected();
//        topSubMenu.clickSearchButton();
//        topSubMenu.goToSearchFeature(shipmentNo46a);
//        Assert.assertTrue(topMenu.isDemandOrdersStatesRejectedVisible());
//        topMenu.assertActualAndExpectedDemandOrdersNumberEqual(shipmentNo46b);
//        topMenu.clearFilter();
//        driver.navigate().refresh();
//    }
//    //
//    @Test(priority = 47, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
//    public void validatingStatesStatusClosedTC47(String shipmentNo47a, String shipmentNo47b) throws InterruptedException {
//        topMenu.goToDemandOrdersTab();
//        topMenu.clickStatesTab();
//        topSubMenu.clickDemandOrderSliderIcon();
//        topSubMenu.clickDemandOrderSliderIcon();
//        topSubMenu.clickDemandOrderSliderIcon();
//        topSubMenu.clickDemandOrderSliderIcon();
//        topSubMenu.clickDemandOrderSliderIcon();
//        topMenu.clickDemandOrdersStatesClosed(); //closed
//        topSubMenu.clickSearchButton();
//        topSubMenu.goToSearchFeature(shipmentNo47a);
//        Assert.assertTrue(topMenu.isDemandOrdersStatesClosedVisible());
//        // topMenu.assertActualAndExpectedDemandOrdersNumberEqual(shipmentNo47b);
//        topMenu.clearFilter();
//        driver.navigate().refresh();
//        leftMenu.clickInventoryMenuOption();
//
//    }
//
//
//
//    @Test(priority = 50, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
//    public void validatingStatesStatusStockOutTC50(String shipmentNo50a, String shipmentNo50b)
//    {
//        topMenu.clickInventoryStockout();
//        topSubMenu.clickSearchButton();
//        topSubMenu.goToSearchFeature(shipmentNo50a);
//        Assert.assertTrue(topMenu.isInventoryStockoutVisible());
//        topMenu.assertActualAndExpectedInventoryNumberEqual(shipmentNo50b);
//        topMenu.clearFilter();
//        driver.navigate().refresh();
//    }
//
////
////    @Test(priority = 51, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////    public void validatingStatesStatusLowStockTC51(String shipmentNo51a, String shipmentNo51b)
////    {
////
////        topMenu.clickDemandInventorySLowStock();
////        topSubMenu.clickSearchButton();
////        topSubMenu.goToSearchFeature(shipmentNo51a);
////        Assert.assertTrue(topMenu.isDemandInventoryExceptionsLowStockVisible());
////        topMenu.assertActualAndExpectedInventoryNumberEqual(shipmentNo51b);
//
////package com.jda.dct.e2e.testscripts.uitests.demand;
////
////import BaseTest;
////import LeftMenu;
////import TopMenuPage;
////import TopSubMenuPage;
////import TestDataFile;
////import org.openqa.selenium.WebDriver;
////import org.testng.Assert;
////import org.testng.annotations.BeforeClass;
////import org.testng.annotations.Test;
////
////public class DemandStatesTests extends BaseTest {
////
////
////    private LeftMenu leftMenu;
////    private TopMenuPage topMenu;
////    private TopSubMenuPage topSubMenu;
////    private WebDriver driver;
////
////    @BeforeClass
////    public void setUp() throws InterruptedException {
////        System.out.println("Starting tests for :" + this.getClass().getName());
//////Initialize required objects
////        driver = getDriver();
////        leftMenu = new LeftMenu(driver);
////        topMenu = new TopMenuPage(driver);
////        topSubMenu = new TopSubMenuPage(driver);
////
////        logInToApplication();
////        //leftMenu.clickInventoryMenuOption();
////        leftMenu.clickDemandMenuOption();
////
////    }
////
////
//////shipped, delivered, Received
////    @Test(priority = 24, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////    public void validatingStatesStatusNewTC24(String shipmentNo24a,String shipmentNo24b)
////    {
////        //driver.navigate().refresh();
////
////        topMenu.goToDemandShipmentTab();
////        topMenu.clickExceptionsTab();// remove after words
////        topMenu.clickStatesTab(); //zero
////        topSubMenu.clickOceanIcon();
////        topMenu.clickDemandShipmentStatesNew();
////        topSubMenu.clickSearchButton();
////        topSubMenu.goToSearchFeature(shipmentNo24a);
////        Assert.assertTrue(topMenu.isDemandShipmentStatesNewVisible());
////        //topMenu.assertActualAndExpectedDemandShipmentNumberEqualOcean(shipmentNo24b);
////       // topMenu.clearFilter();
////        driver.navigate().refresh();
////    }
////
////    @Test(priority = 25, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////    public void validatingStatesStatusPlannedShipmentTC25(String shipmentNo25a,String shipmentNo25b)
////    {
////        topMenu.goToDemandShipmentTab();//zero
////        topSubMenu.clickOceanIcon();
////        topMenu.clickExceptionsTab();// remove after words
////        topMenu.clickStatesTab();
////        topMenu.clickDemandShipmentStatesPlannedShipment(); // webDriverWait affects execution
////        topSubMenu.clickSearchButton();
////        topSubMenu.goToSearchFeature(shipmentNo25a);
////        Assert.assertTrue(topMenu.isDemandShipmentStatesPlannedShipmentVisible());
////       // topMenu.assertActualAndExpectedDemandShipmentNumberEqualOcean(shipmentNo25b);
////        topMenu.clearFilter();
////        driver.navigate().refresh();
////    }
////
////    @Test(priority = 26, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////    public void validatingStatesStatusShippedTC26(String shipmentNo26a,String shipmentNo26b)
////    {
////        topMenu.goToDemandShipmentTab();
////        topMenu.clickExceptionsTab();// remove after words
////        topMenu.clickStatesTab();
////        topSubMenu.clickOceanIcon();
////        topMenu.clickDemandShipmentStatesShipped();
////        topSubMenu.clickSearchButton();
////        topSubMenu.goToSearchFeature(shipmentNo26a);
////        Assert.assertTrue(topMenu.isDemandShipmentStatesShippedVisible());
////        topMenu.assertActualAndExpectedDemandShipmentNumberEqualOcean(shipmentNo26b);
////        topMenu.clearFilter();
////        driver.navigate().refresh();
////    }
////
////    @Test(priority = 27, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////    public void validatingStatesStatusInTransitTC27(String shipmentNo27a,String shipmentNo27b)
////    {
////        topMenu.goToDemandShipmentTab();
////        topSubMenu.clickOceanIcon();
////        topMenu.clickExceptionsTab();// remove after words
////        topMenu.clickStatesTab();
////        topMenu.clickDemandShipmentStatesInTransit();
////        topSubMenu.clickSearchButton();
////        topSubMenu.goToSearchFeature(shipmentNo27a);
////        Assert.assertTrue(topMenu.isDemandShipmentStatesInTransitVisible());
////       // topMenu.assertActualAndExpectedDemandShipmentNumberEqualOcean(shipmentNo27b);
////        topMenu.clearFilter();
////        driver.navigate().refresh();
////    }
////
////    @Test(priority = 28, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////    public void validatingStatesStatusDeliveredTC28(String shipmentNo28a,String shipmentNo28b)
////    {
////        topMenu.goToDemandShipmentTab();
////        topSubMenu.clickOceanIcon();
////        topMenu.clickExceptionsTab();// remove after words
////        topMenu.clickStatesTab();
////        topMenu.clickDemandShipmentStatesDelivered();
////        topSubMenu.clickSearchButton();
////        topSubMenu.goToSearchFeature(shipmentNo28a);
////        Assert.assertTrue(topMenu.isDemandShipmentStatesDeliveredVisible());
////        topMenu.assertActualAndExpectedDemandShipmentNumberEqualOcean(shipmentNo28b);
////       // topMenu.clearFilter();
////        driver.navigate().refresh();
////    }
////
////    @Test(priority = 29, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////    public void validatingStatesStatusReceivedTC29(String shipmentNo29a, String shipmentNo29b) throws InterruptedException
////    {
////        topMenu.goToDemandShipmentTab();
////        topSubMenu.clickOceanIcon();
////        topMenu.clickExceptionsTab();// remove after words
////        topMenu.clickStatesTab();
////        topSubMenu.clickDemandDeliverySliderIcon();
////        topMenu.clickDemandShipmentStatesReceived();
////        topSubMenu.clickSearchButton();
////        topSubMenu.goToSearchFeature(shipmentNo29a);
////        Assert.assertTrue(topMenu.isDemandShipmentStatesReceivedVisible());
////       // topMenu.assertActualAndExpectedDemandShipmentNumberEqualOcean(shipmentNo29b);
////        topMenu.clearFilter();
////        driver.navigate().refresh();
////    }
////
////    @Test(priority = 30, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////    public void validatingStatesStatusPlannedShipmentTC30(String shipmentNo30a, String shipmentNo30b)
////    {
////        topMenu.goToDemandShipmentTab();
////        topSubMenu.clickOceanIcon();
////        topMenu.clickExceptionsTab();// remove after words
////        topMenu.clickStatesTab();
////        topMenu.clickDemandShipmentStatesPlannedShipment();
////        topSubMenu.clickSearchButton();
////        topSubMenu.goToSearchFeature(shipmentNo30a);
////        Assert.assertTrue(topMenu.isDemandShipmentStatesPlannedShipmentVisible());
////      //  topMenu.assertActualAndExpectedDemandShipmentNumberEqualOcean(shipmentNo30b);
////        topMenu.clearFilter();
////        driver.navigate().refresh();
////    }
////
////    @Test(priority = 31, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////    public void validatingStatesStatusShippedTC31(String shipmentNo31a, String shipmentNo31b)
////    {
////        topMenu.goToDemandShipmentTab();
////        topSubMenu.clickOceanIcon();
////        topMenu.clickExceptionsTab();// remove afterwards
////        topMenu.clickStatesTab();
////        topMenu.clickDemandShipmentStatesShipped();
////        topSubMenu.clickSearchButton();
////        topSubMenu.goToSearchFeature(shipmentNo31a);
////        Assert.assertTrue(topMenu.isDemandShipmentStatesShippedVisible());
////        topMenu.assertActualAndExpectedDemandShipmentNumberEqualOcean(shipmentNo31b);
////        topMenu.clearFilter();
////        driver.navigate().refresh();
////
////    }
////
////    @Test(priority = 32, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////    public void validatingStatesStatusInTransitTC32(String shipmentNo32a, String shipmentNo32b)
////    {
////        topMenu.goToDemandShipmentTab();
////        topSubMenu.clickOceanIcon();
////        topMenu.clickExceptionsTab();// remove after words
////        topMenu.clickStatesTab();
////        topMenu.clickDemandShipmentStatesInTransit();
////        topSubMenu.clickSearchButton();
////        topSubMenu.goToSearchFeature(shipmentNo32a);
////        Assert.assertTrue(topMenu.isDemandShipmentStatesInTransitVisible());
////        //topMenu.assertActualAndExpectedDemandShipmentNumberEqualOcean(shipmentNo32b);
////        topMenu.clearFilter();
////        driver.navigate().refresh();
////
////
////    }   //stopped in two minutes
////
////    @Test(priority = 33, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////    public void validatingStatesStatusDeliveredTC33(String shipmentNo33a, String shipmentNo33b)
////    {
////        topMenu.goToDemandShipmentTab();
////        topSubMenu.clickOceanIcon();
////        topMenu.clickExceptionsTab();// remove afterwards
////        topMenu.clickStatesTab();
////        topMenu.clickDemandShipmentStatesDelivered();
////        topSubMenu.clickSearchButton();
////        topSubMenu.goToSearchFeature(shipmentNo33a);
////        Assert.assertTrue(topMenu.isDemandShipmentStatesDeliveredVisible());
////        topMenu.assertActualAndExpectedDemandShipmentNumberEqualOcean(shipmentNo33b);
////        topMenu.clearFilter();
////        driver.navigate().refresh();
////        topMenu.gotoDemandDeliveryTab();//
////
////    }
////
////
////    @Test(priority = 34, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////    public void validatingStatesStatusShippedTC34(String shipmentNo34a, String shipmentNo34b)
////    {
////       // topMenu.gotoDemandDeliveryTab();
////        topSubMenu.clickOceanIcon();
////        topMenu.clickExceptionsTab();// remove afterwards
////        topMenu.clickStatesTab();
////        topMenu.clickDemandDeliveryStatesShipped();
////        topSubMenu.clickSearchButton();
////        topSubMenu.goToSearchFeature(shipmentNo34a);
////        Assert.assertTrue(topMenu.isdemandDeliveryStatesShippedVisible());
////       // topMenu.assertActualAndExpectedDemandDeliveryNumberEqualOcean(shipmentNo34b);
////        topMenu.clearFilter();
////        driver.navigate().refresh();
////    }
////
////    @Test(priority = 35, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////    public void validatingStatesStatusDeliveredTC35(String shipmentNo35a, String shipmentNo35b)
////    {
////        topMenu.gotoDemandDeliveryTab();
////        topSubMenu.clickOceanIcon();
////        topMenu.clickExceptionsTab();// remove afterwards
////        topMenu.clickStatesTab();
////        topMenu.clickDemandDeliveryStatesDelivered();
////        topSubMenu.clickSearchButton();
////        topSubMenu.goToSearchFeature(shipmentNo35a);
////        Assert.assertTrue(topMenu.isDemandDeliveryStatesDeliveredVisible());
////      //  topMenu.assertActualAndExpectedDemandDeliveryNumberEqualOcean(shipmentNo35b);
////
////        topMenu.clearFilter();
////        driver.navigate().refresh();
////    }
////
////    @Test(priority = 36, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////    public void validatingStateStatusClosedTC36(String shipmentNo36a, String shipmentNo36b)
////    //States and Exceptions status are interchanges causing cases to fail
////    {
////        topMenu.gotoDemandDeliveryTab();
////        topSubMenu.clickOceanIcon();
////        topMenu.clickExceptionsTab();// remove afterwards
////        topMenu.clickStatesTab();
////        topMenu.clickDemandDeliveryStatesClosed();
////        topSubMenu.clickSearchButton();
////        topSubMenu.goToSearchFeature(shipmentNo36a);
////        Assert.assertTrue(topMenu.isdemandDeliveryStatesClosedVisible());
////       // topMenu.assertActualAndExpectedDemandDeliveryNumberEqualOcean(shipmentNo36b);
////        topMenu.clearFilter();
////        driver.navigate().refresh();
////    }
////
////    @Test(priority = 37, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////    public void validatingStatesStatusReceivedTC37(String shipmentNo37a, String shipmentNo37b)
////    {
////        topMenu.gotoDemandDeliveryTab();
////        topSubMenu.clickOceanIcon();
////        topMenu.clickExceptionsTab();// remove afterwards
////        topMenu.clickStatesTab();
////        topMenu.clickDemandDeliveryStatesReceived();
////        topSubMenu.clickSearchButton();
////        topSubMenu.goToSearchFeature(shipmentNo37a);
////        Assert.assertTrue(topMenu.isdemandDeliveryStatesReceivedVisible());
////        topMenu.assertActualAndExpectedDemandDeliveryNumberEqualOcean(shipmentNo37b);
//
////        topMenu.clearFilter();
////        driver.navigate().refresh();
////    }
////
//
////
////    @Test(priority = 52, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////    //Filtering is Failing
////    public void validatingStatesStatusOverstockTC52(String shipmentNo52a, String shipmentNo52b)
////    {
////        topMenu.clickDemandInventoryOverstock();
////        topSubMenu.clickSearchButton();
////        topSubMenu.goToSearchFeature(shipmentNo52a);
////        Assert.assertTrue(topMenu.isDemandInventoryExceptionsOverstockVisible());
////        topMenu.assertActualAndExpectedInventoryNumberEqual(shipmentNo52a);
////        topMenu.clearFilter();
////        driver.navigate().refresh();
////    }
////
////    @Test(priority = 53, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////    public void validatingStatesStatusOnTrackTC53 (String shipmentNo53a, String shipmentNo53b)
////    {
////        topMenu.clickDemandInventoryOnTrack();
////        topSubMenu.clickSearchButton();
////        topSubMenu.goToSearchFeature(shipmentNo53a);
////        Assert.assertTrue(topMenu.isDemandInventoryExceptionsOnTrackVisible());
////         topMenu.assertActualAndExpectedInventoryNumberEqual(shipmentNo53a);
////        topMenu.clearFilter();
////        driver.navigate().refresh();
////    }
//
//}
//
////    @Test(priority = 38, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////    public void validatingStatesStatusShippedTC38(String shipmentNo38a, String shipmentNo38b)
////    {
////        topMenu.gotoDemandDeliveryTab();
////        topSubMenu.clickOceanIcon();
////        topMenu.clickExceptionsTab();// remove afterwards
////        topMenu.clickStatesTab();
////        topMenu.clickDemandDeliveryStatesShipped();
////        topSubMenu.clickSearchButton();
////        topSubMenu.goToSearchFeature(shipmentNo38a);
////        topMenu.assertActualAndExpectedDemandDeliveryNumberEqualOcean(shipmentNo38b);
////        Assert.assertTrue(topMenu.isdemandDeliveryStatesShippedVisible());
////        topMenu.clearFilter();
////        driver.navigate().refresh();
////    }
////
//////    //State Order
////
////
////    @Test(priority = 39, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////    public void validatingStatesStatusNewTC39(String shipmentNo39a, String shipmentNo39b)
////    {
////        topMenu.goToDemandOrdersTab();
////        topMenu.clickExceptionsTab();// remove afterwards
////        topMenu.clickStatesTab();
////        topMenu.clickDemandOrdersStatesNew();
////        topSubMenu.clickSearchButton();
////        topSubMenu.goToSearchFeature(shipmentNo39a);
////        Assert.assertTrue(topMenu.isdemandOrdersStatesNewVisible());
////        topMenu.assertActualAndExpectedDemandOrdersNumberEqual(shipmentNo39b);
////        topMenu.clearFilter();
////        driver.navigate().refresh();
////    }
////
////    @Test(priority = 40, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////    public void validatingStatesStatusOpenTC40(String shipmentNo40a, String shipmentNo40b)
////    {
////        topMenu.goToDemandOrdersTab();
////        topMenu.clickExceptionsTab();// remove afterwards
////        topMenu.clickStatesTab();
////        topMenu.clickDemandOrdersStatesOpen();
////        topSubMenu.clickSearchButton();
////        topSubMenu.goToSearchFeature(shipmentNo40a);
////        Assert.assertTrue(topMenu.isDemandOrdersStatesOpenVisible());
////        topMenu.assertActualAndExpectedDemandOrdersNumberEqual(shipmentNo40a);
////        topMenu.clearFilter();
////        driver.navigate().refresh();
////    }
////    //
////    @Test(priority = 41, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////    public void validatingStatesStatusConfirmedWithChangesTC41(String shipmentNo41a, String shipmentNo41b)
////    {
////        topMenu.goToDemandOrdersTab();
////        topMenu.clickExceptionsTab();// remove afterwards
////        topMenu.clickStatesTab();
////        topMenu.clickDemandOrdersStatesConfirmedWithChanges();
////        topSubMenu.clickSearchButton();
////        topSubMenu.goToSearchFeature(shipmentNo41a);
////        Assert.assertTrue(topMenu.isDemandOrdersStatesConfirmedWithChangesVisible());
////        topMenu.assertActualAndExpectedDemandOrdersNumberEqual(shipmentNo41b);
////        topMenu.clearFilter();
////        driver.navigate().refresh();
////    }
////    //
////    @Test(priority = 42, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////    public void validatingStatesStatusConfirmedTC42(String shipmentNo42a, String shipmentNo42b)
////    {
////        topMenu.goToDemandOrdersTab();
////        topMenu.clickExceptionsTab();// remove afterwards
////        topMenu.clickStatesTab();
////        topMenu.clickDemandOrdersStatesConfirmed();
////        topSubMenu.clickSearchButton();
////        topSubMenu.goToSearchFeature(shipmentNo42a);
////        Assert.assertTrue(topMenu.isDemandOrdersStatesConfirmedVisible());
////        topMenu.assertActualAndExpectedDemandOrdersNumberEqual(shipmentNo42b);
////        topMenu.clearFilter();
////        driver.navigate().refresh();
////    }
////
////    //
////    @Test(priority = 43, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////    public void validatingStatesStatusPlannedShipmentTC43(String shipmentNo43a, String shipmentNo43b)
////    {
////        topMenu.goToDemandOrdersTab();
////        topMenu.clickExceptionsTab();// remove afterwards
////        topMenu.clickStatesTab();
////        topMenu.clickDemandOrdersStatesPlannedShipment();
////        topSubMenu.clickSearchButton();
////        topSubMenu.goToSearchFeature(shipmentNo43a);
////        Assert.assertTrue(topMenu.isDemandOrdersStatesPlannedShipmentVisible());
////        //topMenu.assertActualAndExpectedDemandOrdersNumberEqual(shipmentNo43b);
////        topMenu.clearFilter();
////        driver.navigate().refresh();
////    }
////
////    //
////    @Test(priority = 44, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////    public void validatingStatesStatusShippedTC44(String shipmentNo44a, String shipmentNo44b) throws InterruptedException
////    {
////        topMenu.goToDemandOrdersTab();
////        topMenu.clickExceptionsTab();// remove afterwards
////        topMenu.clickStatesTab();
////        topSubMenu.clickDemandOrderSliderIcon();
////        topMenu.clickDemandOrdersStatesShipped();
////        topSubMenu.clickSearchButton();
////        topSubMenu.goToSearchFeature(shipmentNo44a);
////        Assert.assertTrue(topMenu.isDemandOrdersStatesShippedVisible());
////        topMenu.assertActualAndExpectedDemandOrdersNumberEqual(shipmentNo44b);
////        topMenu.clearFilter();
////        driver.navigate().refresh();
////    }
////
////    //
////    @Test(priority = 45, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////    public void validatingStatesStatusReceivedTC45(String shipmentNo45a, String shipmentNo45b) throws InterruptedException {
////        topMenu.goToDemandOrdersTab();
////        topMenu.clickExceptionsTab();// remove afterwards
////        topMenu.clickStatesTab();
////        topSubMenu.clickDemandOrderSliderIcon();
////        topSubMenu.clickDemandOrderSliderIcon();
////        topMenu.clickDemandOrdersStatesReceived();
////        topSubMenu.clickSearchButton();
////        topSubMenu.goToSearchFeature(shipmentNo45a);
////        Assert.assertTrue(topMenu.isDemandOrdersStatesReceivedVisible());
////        topMenu.assertActualAndExpectedDemandOrdersNumberEqual(shipmentNo45b);
////        topMenu.clearFilter();
////        driver.navigate().refresh();
////    }
////
////
////    //
////    @Test(priority = 46, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////    public void validatingStatesStatusRejectedTC46(String shipmentNo46a, String shipmentNo46b) throws InterruptedException {
////        topMenu.goToDemandOrdersTab();
////        topMenu.clickExceptionsTab();// remove afterwards
////        topMenu.clickStatesTab();
////        topSubMenu.clickDemandOrderSliderIcon();
////        topSubMenu.clickDemandOrderSliderIcon();
////        topSubMenu.clickDemandOrderSliderIcon();
////        topMenu.clickDemandOrdersStatesRejected();
////        topSubMenu.clickSearchButton();
////        topSubMenu.goToSearchFeature(shipmentNo46a);
////        Assert.assertTrue(topMenu.isDemandOrdersStatesRejectedVisible());
////        topMenu.assertActualAndExpectedDemandOrdersNumberEqual(shipmentNo46b);
////        topMenu.clearFilter();
////        driver.navigate().refresh();
////    }
////    //
////    @Test(priority = 47, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////    public void validatingStatesStatusClosedTC47(String shipmentNo47a, String shipmentNo47b) throws InterruptedException {
////        topMenu.goToDemandOrdersTab();
////        topMenu.clickExceptionsTab();// remove afterwards
////        topMenu.clickStatesTab();
////        topSubMenu.clickDemandOrderSliderIcon();
////        topSubMenu.clickDemandOrderSliderIcon();
////        topSubMenu.clickDemandOrderSliderIcon();
////        topSubMenu.clickDemandOrderSliderIcon();
////        topSubMenu.clickDemandOrderSliderIcon();
////        topMenu.clickDemandOrdersStatesClosed(); //closed
////        topSubMenu.clickSearchButton();
////        topSubMenu.goToSearchFeature(shipmentNo47a);
////        Assert.assertTrue(topMenu.isDemandOrdersStatesClosedVisible());
////       // topMenu.assertActualAndExpectedDemandOrdersNumberEqual(shipmentNo47b);
////        topMenu.clearFilter();
////        driver.navigate().refresh();
////        leftMenu.clickInventoryMenuOption();
////
////    }
////
////
//////    @Test(priority = 48, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
//////    public void validatingSearchFeatureCreatedDSTC48(String DSTC48)
//////    {
//////        topMenu.goToDemandOrdersTab();
//////        topMenu.clickStatesTab();
//////        topSubMenu.clickDemandOrderSliderIcon();
//////
//////        topSubMenu.clickDemandOrderSliderIcon();
//////
//////        topSubMenu.clickDemandOrderSliderIcon();
//////
//////        topSubMenu.clickDemandOrderSliderIcon();
//////
//////        topSubMenu.clickDemandOrderSliderIcon();
//////
//////        topSubMenu.clickDemandOrderSliderIcon();
//////
//////
//////        topMenu.clickDemandOrdersStatesCreated(); //Created
//////
//////        topSubMenu.clickSearchButton();
//////        topSubMenu.goToSearchFeature(DSTC48);
//////
//////        Assert.assertTrue(topMenu.isDemandOrdersStatesCreatedVisible());
//// //           topMenu.assertDemandShipmentSearchResultIsEqualDSTC48();
////
//////       // Assert.assertTrue(topMenu.isDemandOrdersSearchedResultDisplayed());
//////
//////        topMenu.clearFilter();
//////        driver.navigate().refresh();
//////    }
////
//////    @Test(priority = 49, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
//////    public void validatingSearchFeatureDeliveredDSTC49(String DSTC49) throws InterruptedException {
//////        topMenu.goToDemandOrdersTab();  // Delivered no longer visible here.
//////        topMenu.clickExceptionsTab();
//////
//////        topMenu.clickStatesTab();
//////        topSubMenu.clickDemandOrderSliderIcon();
//////
//////        topSubMenu.clickDemandOrderSliderIcon();
//////        topSubMenu.clickDemandOrderSliderIcon();
//////
//////        topSubMenu.clickDemandOrderSliderIcon();
//////        topSubMenu.clickDemandOrderSliderIcon();
//////        topSubMenu.clickDemandOrderSliderIcon();
//////        topMenu.clickDemandOrdersStatesDelivered(); //delivered
//////
//////        topSubMenu.clickSearchButton();
//////        topSubMenu.goToSearchFeature(DSTC49);
//////        Assert.assertTrue(topMenu.isDemandOrdersStatesDeliveredVisible());
//////        Assert.assertTrue(topMenu.isDemandOrdersSearchedResultDisplayed());
//////        topMenu.clearFilter();
//////        driver.navigate().refresh();
//////    }
////// Inventory
////
////    @Test(priority = 50, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////    public void validatingStatesStatusStockOutTC50(String shipmentNo50a, String shipmentNo50b)
////    {
////        topMenu.clickInventoryStockout();
////        topSubMenu.clickSearchButton();
////        topSubMenu.goToSearchFeature(shipmentNo50a);
////        Assert.assertTrue(topMenu.isInventoryStockoutVisible());
////       topMenu.assertActualAndExpectedInventoryNumberEqual(shipmentNo50b);
////        topMenu.clearFilter();
////        driver.navigate().refresh();
////    }
////
////
////    @Test(priority = 51, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////    public void validatingStatesStatusLowStockTC51(String shipmentNo51a, String shipmentNo51b)
////    {
////
////        topMenu.clickDemandInventorySLowStock();
////        topSubMenu.clickSearchButton();
////        topSubMenu.goToSearchFeature(shipmentNo51a);
////        Assert.assertTrue(topMenu.isDemandInventoryExceptionsLowStockVisible());
////        topMenu.assertActualAndExpectedInventoryNumberEqual(shipmentNo51b);
////        topMenu.clearFilter();
////        driver.navigate().refresh();
////    }
////
////
////    @Test(priority = 52, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////    //Filtering is Failing
////    public void validatingStatesStatusOverstockTC52(String shipmentNo52a, String shipmentNo52b)
////    {
////        topMenu.clickDemandInventoryOverstock();
////        topSubMenu.clickSearchButton();
////        topSubMenu.goToSearchFeature(shipmentNo52a);
////        Assert.assertTrue(topMenu.isDemandInventoryExceptionsOverstockVisible());
////        topMenu.assertActualAndExpectedInventoryNumberEqual(shipmentNo52a);
////        topMenu.clearFilter();
////        driver.navigate().refresh();
////    }
////
////    @Test(priority = 53, dataProviderClass = TestDataFile.class, dataProvider="UseShipmentId")
////    public void validatingStatesStatusOnTrackTC53 (String shipmentNo53a, String shipmentNo53b)
////        {
////        topMenu.clickDemandInventoryOnTrack();
////        topSubMenu.clickSearchButton();
////        topSubMenu.goToSearchFeature(shipmentNo53a);
////        Assert.assertTrue(topMenu.isDemandInventoryExceptionsOnTrackVisible());
////       // topMenu.assertActualAndExpectedInventoryNumberEqual(shipmentNo53a);
////        topMenu.clearFilter();
////        driver.navigate().refresh();
////    }
////
////}
////
