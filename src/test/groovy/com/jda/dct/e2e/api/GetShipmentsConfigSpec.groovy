package com.jda.dct.e2e.api

import com.jda.dct.e2e.api.utility.AuthHelper
import groovyx.net.http.RESTClient
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll

/**
 * GET method to retrieve demand shipments list configuration
 *
 */

// may be redundant, check and delete on confirmation
class GetShipmentsConfigSpec extends Specification implements AuthHelper {
    static shipmentsConfigURL
    @Shared
    def client

    def setupSpec() {
        init()

        def shipmentsConfigPath = properties.getProperty('getShipmentsConfigPath')
        shipmentsConfigURL = protocol + ip + shipmentsConfigPath
        client = new RESTClient(shipmentsConfigURL)
        client.handler.failure = { resp -> resp }
        header[HttpHeaders.CONTENT_TYPE] = MediaType.APPLICATION_JSON
    }

    @Unroll
    def "Get shipments configuration API #url"() {
        given: "The API URL is #url "
        when: "HTTP Method is GET: should return demand shipments list configuration"
        def response = client.get(headers: header)
        then: "the response should be 200"
        with(response) {
            status == 200
        }
        where:
        url << [shipmentsConfigURL]

    }
}
