/*
package com.jda.dct.e2e.api.utility;

import com.fasterxml.jackson.annotation.JsonProperty;

import com.jda.dct.tracking.loads.objects.Load;

import java.util.Objects;


public class FourkitesLoad  extends Load{
    private Shipper fourkitesShipper;
    private Object fourkitesCarrier;
    @JsonProperty("carrier")
    public Object getCarrier(){
        return fourkitesCarrier;
    }
    @JsonProperty("carrier")
    public void setCarrier(Object carrier){
        fourkitesCarrier=carrier;
    }
    @JsonProperty("shipper")
    public Shipper getShipper(){
        return fourkitesShipper;
    }
    @JsonProperty("shipper")
    public void setShipper(Shipper shipper){
        fourkitesShipper=shipper;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final FourkitesLoad load = (FourkitesLoad) o;
        return relayLoad == load.relayLoad
                && Objects.equals(loadNumber, load.loadNumber)
                && Objects.equals(displayLoadNumber, load.displayLoadNumber)
                && Objects.equals(proNumber, load.proNumber)
                && Objects.equals(trackingNumber, load.trackingNumber)
                && Objects.equals(priority, load.priority)
                && Objects.equals(tags, load.tags)
                && Objects.equals(referenceNumbers, load.referenceNumbers)
                && Objects.equals(stops, load.stops)
                && Objects.equals(trackingInfo, load.trackingInfo)
                && Objects.equals(truckRoutingParams, load.truckRoutingParams)
                && Objects.equals(products, load.products)
                && Objects.equals(fourkitesCarrier,load.fourkitesCarrier)
                && Objects.equals(fourkitesShipper,load.fourkitesShipper);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
                loadNumber, displayLoadNumber, proNumber,trackingNumber, priority,fourkitesCarrier,
                tags, referenceNumbers, relayLoad, stops, trackingInfo, truckRoutingParams, products,fourkitesShipper
        );
    }

    @Override
    public String toString() {
        return "Load{"
                + "loadNumber='" + loadNumber + '\''
                + ", displayLoadNumber='" + displayLoadNumber + '\''
                + ", carrier='" + fourkitesCarrier + '\''
                + ", shipper='" + fourkitesShipper + '\''
                + ", proNumber='" + proNumber + '\''
                + ", trackingNumber='" + trackingNumber + '\''
                + ", priority='" + priority + '\''
                + ", tags=" + tags
                + ", referenceNumbers=" + referenceNumbers
                + ", relayLoad=" + relayLoad
                + ", stops=" + stops
                + ", trackingInfo=" + trackingInfo
                + ", truckRoutingParams=" + truckRoutingParams
                + ", products=" + products
                + '}';
    }

}
*/
