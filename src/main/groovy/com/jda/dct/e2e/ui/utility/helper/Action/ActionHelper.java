package com.jda.dct.e2e.ui.utility.helper.Action;

import com.jda.dct.e2e.ui.utility.common.BaseTest;
import org.apache.log4j.Logger;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class ActionHelper extends BaseTest {

    private WebDriver driver;
    private final Logger oLog = Logger.getLogger(ActionHelper.class);

    public ActionHelper(WebDriver driver) {
        this.driver = driver;
        oLog.debug("AlertHelper : " + this.driver.hashCode());
    }

    /**
     * This method clicks using action class
     *
     * @param element
     * @param text
     * @return
     */
    public boolean actionClick(WebElement element, String text) {
        try {
            Actions action = new Actions(driver);
            action.moveToElement(element).build().perform();
            action.click(element).build().perform();
            Reporter("Clicked on " + text + " successfully", "Pass");
            return true;
        } catch (Exception e) {
            Reporter("Exception while clicking on " + text, "Error");
            throw new RuntimeException("Exception while clicking on " + text + ":" + e.getMessage());
        }
    }

    /**
     * This method clicks using action class
     *
     * @param element
     * @param text
     * @return
     */
    public boolean moveToElement(WebElement element, String text) {
        try {
            Actions action = new Actions(driver);
            action.moveToElement(element).build().perform();
            Reporter("Moved to " + text + " successfully", "Pass");
            return true;
        } catch (Exception e) {
            Reporter("Exception while clicking on " + text, "Error");
            throw new RuntimeException("Exception while clicking on " + text + ":" + e.getMessage());
        }
    }

    /**
     * Move to top of page by pressing CNTRL+HOME button
     *
     * @param element
     * @param text
     * @return boolean
     */
    public boolean moveToTop(WebElement element, String text) {
        try {
            Actions action = new Actions(driver);
            action.keyDown(Keys.CONTROL).sendKeys(Keys.HOME).build().perform();
            action.keyDown(Keys.CONTROL).release().perform();
            Reporter("Moved to top successfully", "Pass");
            return true;
        } catch (Exception e) {
            Reporter("Exception while clicking on " + text, "Error");
            throw new RuntimeException("Exception while clicking on " + text + ":" + e.getMessage());
        }
    }

    /**
     * Move to top of page by pressing CNTRL+HOME button
     *
     * @return boolean
     */
    public boolean pause(long duration) {
        try {
            Actions action = new Actions(driver);
            action.pause(duration);
            Reporter("Paused successfully", "Pass");
            return true;
        } catch (Exception e) {
            Reporter("Exception while performing action pause", "Error");
            throw new RuntimeException("Exception while performing action pause" + ":" + e.getMessage());
        }
    }

    /**
     * Move to top of page by pressing CNTRL+HOME button
     *
     * @return boolean
     */
    public boolean pressEnterKey() {
        try {
            Actions action = new Actions(driver);
            action.keyDown(Keys.CONTROL).sendKeys(Keys.ENTER).build().perform();
            action.keyDown(Keys.CONTROL).release().perform();
            return true;
        } catch (Exception e) {
            Reporter("Exception while clicking on Enter key", "Error");
            e.printStackTrace();
            throw new RuntimeException();
        }
    }

}
