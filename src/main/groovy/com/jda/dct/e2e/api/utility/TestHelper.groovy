package com.jda.dct.e2e.api.utility

import com.fasterxml.jackson.databind.ObjectMapper
import com.jda.dct.domain.Node
import groovy.json.JsonSlurper
import org.apache.http.HttpStatus
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.web.client.HttpStatusCodeException
import org.springframework.web.client.RestTemplate

trait TestHelper {
    /*
       Helper method to setup up a rest call to the specified URL and returns the response
    */
    def getHttpClient(Map head, def url, def bodyContent, def httpMethod) {
        def rest = new RestTemplate()
        def headers = new HttpHeaders()
        def body = ""
        headers.contentType = MediaType.APPLICATION_JSON
        head.each {
            name, value ->
                headers.add(name, value)
        }

        if (bodyContent != null)
            body = bodyContent
        def entity = new HttpEntity<String>(body, headers)

        def response = null
        try {
            response = rest.exchange(url, httpMethod, entity, String)
        } catch(HttpStatusCodeException e) {
            System.err.println("Http Request Failure: ")
            System.err.println("\t${e.getStatusCode()}: ${e.getResponseBodyAsString()}")
            System.err.println("\tResponse Headers: ${e.getResponseHeaders()}")
        }

        response
    }

    int getHttpStatusVal(Map head, def url, def bodyContent, def httpMethod) {
        def rest = new RestTemplate()
        def headers = new HttpHeaders()
        def body = ""
        headers.contentType = MediaType.APPLICATION_JSON
        head.each {
            name, value ->
                headers.add(name, value)
        }

        if (bodyContent != null)
            body = bodyContent
        def entity = new HttpEntity<String>(body, headers)
        int status = HttpStatus.SC_OK
        try {
            rest.exchange(url, httpMethod, entity, String)
        } catch(HttpStatusCodeException e) {
            status = e.getStatusCode().value()
        }
        status
    }

    def getJson(def json) {
        def responseJson = new JsonSlurper().parseText(json)

        responseJson
    }

    def getHeader(def name, def value) {
        def header = [:]
        header.put(name, value)

        header
    }

    def jsonFromObject(def mockData, def className) {
        def mapper = new ObjectMapper()
        mapper.addMixIn(Node, className.class)
        def jsonData = mapper.writeValueAsString(mockData)

        jsonData
    }

    def executeHttpCall(def headers, def URL, def data, def HttpMethod) {
        def responseJson = getHttpClient(headers, URL, data, HttpMethod).body
        def response = getJson(responseJson)
        response
    }

    def sendHttpRequest(def headers, def URL, def data, def HttpMethod) {
        def response = getHttpClient(headers, URL, data, HttpMethod)
        return new JdaApiResponse(response)
    }

    def makeSearchUrlFromGetUrl(def getUrl) {
        return getUrl[-1] == '/' ? getUrl[0..-2] : getUrl
    }
}
