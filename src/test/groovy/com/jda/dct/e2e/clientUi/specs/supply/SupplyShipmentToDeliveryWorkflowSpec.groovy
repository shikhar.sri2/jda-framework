//package com.jda.dct.e2e.clientUi.specs.tempraturecontrolled
//
//import com.jda.dct.e2e.clientUi.pages.tempraturecontrolled.SupplyShipmentDetailsPage
//<<<<<< HEAD
//import com.jda.dct.e2e.clientUi.pages.tempraturecontrolled.SupplyShipmentsListPage
//
//import geb.exceptionPage.CachingDriverFactory
//import geb.spock.GebSpec
//
//=======
//import com.jda.dct.e2e.clientUi.pages.tempraturecontrolled.SupplyDeliveryListPage
//import com.jda.dct.e2e.clientUi.pages.tempraturecontrolled.SupplyShipmentsListPage
//import com.jda.dct.e2e.clientUi.pages.login.LoginPage
//
//import geb.exceptionPage.CachingDriverFactory
//import geb.spock.GebSpec
//import spock.lang.Stepwise
//
//@Stepwise
//>>>>>>> d3314fef01710ac9cb28148dd1e359524510cf94
//class SupplyShipmentToDeliveryWorkflowSpec extends GebSpec{
//
//
//    def setupSpec() {
//        CachingDriverFactory.clearCache()
//        to LoginPage
//        at(LoginPage).login()
//        println "Successfully Login to Luminate Control Tower. Current page url:"
//        println getExceptionPage().getCurrentUrl()
//    }
//
//    def "Shipment-list to shipment-details to deliveries workflow" () {
//        given: "User is at tempraturecontrolled shipment list page"
//        to SupplyShipmentsListPage
//        println "\nUser is at Supply Shipment List page with url: "
//        println getExceptionPage().getCurrentUrl()
//
//        when: "User click a shipment No"
//        at(SupplyShipmentsListPage).clickFirstShipment()
//        println "\nUser clicked first shipment"
//
//        then: "User is at shipment details page of the shipment"
//        at SupplyShipmentDetailsPage
//        println "\nUser is at Shipment Details page with url: "
//        println getExceptionPage().getCurrentUrl()
//
//        when:"User click container in shipment details page"
//        at(SupplyShipmentDetailsPage).clickFirstContainer()
//        println "\nUser clicked container in shipment details page"
//
//        then:"Display all deliveries in the container"
//        waitFor {
//            browser.find("div", role:"button", 'aria-expanded':"false").first().isDisplayed()
//        }
//        println "\nNavigated to delivery list page.\nCurrent page url:" + getExceptionPage().getCurrentUrl()
//        int noDeliveries = browser.find("div", role:"button", 'aria-expanded':"false").size()
//        println "\nNumber of Deliveries: " + noDeliveries
//        if (noDeliveries == 0) {
//            println "Oops! There is no outbound delivery inside the container"
//        } else {
//            println "The container has $noDeliveries outbound deliveries"
//        }
//    }
//
//
//    def cleanupSpec() {
//        CachingDriverFactory.clearCacheAndQuitDriver()
//    }
//}
