package com.jda.dct.e2e.api.purchaseOrders

import com.fasterxml.jackson.databind.ObjectMapper
import com.jda.dct.domain.stateful.PurchaseOrder
import com.jda.dct.e2e.api.categories.Regression
import com.jda.dct.e2e.api.categories.Smoke
import com.jda.dct.e2e.api.utility.IngestionHelper
import groovy.json.JsonSlurper
import org.junit.experimental.categories.Category
import org.springframework.http.HttpMethod
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Stepwise
import spock.util.concurrent.PollingConditions

/**
 * End-to-end tests for {@code Purchase order} ingestion.
 *
 * @author Arun Venkataramanan
 * @version 1.0-SNAPSHOT
 * @since 13-Jul-2018
 */


@Stepwise
class IngestPurchaseOrderSpec extends Specification implements IngestionHelper {

    @Shared
    static def previousStatusLmd

    @Shared
    def salesOrderGet

    @Shared
    def salesOrderIngestionURL

    @Shared
    def purchaseOrderPath

    @Shared
            samplePurchaseOrder2,
            samplePurchaseOrder3,
            samplePurchaseOrder4,
            samplePurchaseOrder5,
            samplePOPastDueShipment,
            samplePOLate,
            samplePOOnTime,
            samplePOUnknownStatus,
            samplePOReceived,
            sampleSiteForAlert,
            sampleItemForAlert

    def setupSpec() {
        init()

        def sitePath = properties.getProperty('ingestSitesPath')
        def itemPath = properties.getProperty('ingestItemsPath')

        siteIngestionUrl = protocol + ip + sitePath
        itemIngestionUrl = protocol + ip + itemPath

        sampleSiteWithDesc = this.getClass().getClassLoader().getResource('sample-site-with-desc.json').text
        samplePurchaseOrderNoDesc = this.getClass().getClassLoader().getResource('sample-purchase-order-no-desc.json').text

        purchaseOrderPath = properties.getProperty('ingestPurchaseOrdersPath')
        def getPath = properties.getProperty('getPurchaseOrderPath')
        def getSalesOrderPath = properties.getProperty('getSalesOrderPath')

        def searchAlertPath = properties.get("searchAlertPath")
        searchAlertUrlPrefix = protocol + ip + searchAlertPath

        purchaseOrderIngestionUrl = protocol + ip + purchaseOrderPath


        ingestionURL = protocol + ip + purchaseOrderPath
        getURLPrefix = protocol + ip + getPath

        salesOrderIngestionURL = protocol + ip + properties.getProperty('ingestSalesOrdersPath')
        salesOrderGet = protocol + ip + getSalesOrderPath

        samplePurchaseOrder = this.getClass().getClassLoader().getResource('sample-purchase-order-1.json').text
        samplePurchaseOrder2 = this.getClass().getClassLoader().getResource('sample-po-computation-1-verify.json').text
        samplePurchaseOrder3 = this.getClass().getClassLoader().getResource('sample-po-computation-2-update-values.json').text
        samplePurchaseOrder4 = this.getClass().getClassLoader().getResource('sample-po-computation-3-remove-update-values.json').text
        samplePurchaseOrder5 = this.getClass().getClassLoader().getResource('sample-po-computation-4-remove-update-values.json').text
        samplePOLate = this.getClass().getClassLoader().getResource('sample-purchase-order-alert-late-1.json').text
        samplePOOnTime = this.getClass().getClassLoader().getResource('sample-purchase-order-alert-on-time-1.json').text
        samplePOUnknownStatus = this.getClass().getClassLoader().getResource('sample-purchase-order-alert-unknown-status-1.json').text
        samplePOPastDueShipment = this.getClass().getClassLoader().getResource('sample-purchase-order-alert-past-due-shipment-1.json').text
        samplePOReceived = this.getClass().getClassLoader().getResource('sample-purchase-order-alert-late-received-1.json').text
        sampleSiteForAlert = this.getClass().getClassLoader().getResource('sample-site-for-alert-testing-1.json').text
        sampleItemForAlert = this.getClass().getClassLoader().getResource('sample-item-for-alert-testing-1.json').text
    }

    @Category(Smoke)
    def "Add a Note to a Purchase Order and get it back"() {
        given:
        def conditions = new PollingConditions(timeout: 10, initialDelay: 1)

        when: 'A Note is added to the purchase order'
        println getURLPrefix
        def purchaseOrderIdNew = executeHttpCall(header, getURLPrefix, null, HttpMethod.GET)["data"][0]["id"]

        println purchaseOrderIdNew
    //    def purchaseOrderIds = getPurchaseOrderIds(samplePurchaseOrder)
        def url = getURLPrefix + 'notes/' + purchaseOrderIdNew
        //        purchaseOrderIds[0]
        println url
        def sampleNote = '{ "note": "Purchase Order Note added"}'
        def response = getHttpClient(header, url, sampleNote, HttpMethod.POST)
        assert response.status == 200

        and: "Get a purchase order"
        url = getURLPrefix + purchaseOrderIdNew
        println url
        def purchaseOrderText = getHttpClient(header, url, null, HttpMethod.GET).body
        def purchaseOrderRetrieved = new JsonSlurper().parseText(purchaseOrderText).get('data').get(0)

        then: 'Get the Purchase Order back and verify the note'
        assert purchaseOrderRetrieved.getAt('notes').toString().contains("Purchase Order Note added") == true
    }

    @Category(Regression)
    def "Ingestion status of an invalid purchase order becomes complete with error"() {
        given:
        def conditions = new PollingConditions(timeout: 10, initialDelay: 1)
        def modifiedPurchaseOrder = samplePurchaseOrder.replaceAll("\"purchaseOrderType\": \"standardPO\"", "\"purchaseOrderType\": \"wrong-order-type\"")

        when: 'a purchase order json ingested'
        def response = executeHttpCall(header, ingestionURL, modifiedPurchaseOrder, HttpMethod.POST)
        def ingestionId = response.ingestionId

        then: 'ingestion status eventually becomes COMPLETED WITH ERROR'
        conditions.eventually {
            def status = getStatus(ingestionId)
            assert status.contains('COMPLETED_WITH_ERROR')
        }
    }

    @Category(Regression)
    def "Ingestion status of an invalid attributes in purchase order becomes complete with error"() {
        given:
        def conditions = new PollingConditions(timeout: 10, initialDelay: 1)
        def modifiedPurchaseOrder = samplePurchaseOrder.replaceAll("\"purchaseOrderId\": \"TestOrder\"", "\"\": \"\"")

        when: 'a purchase order json ingested'
        def response = executeHttpCall(header, ingestionURL, modifiedPurchaseOrder, HttpMethod.POST)
        def ingestionId = response.ingestionId

        then: 'ingestion status eventually becomes COMPLETED WITH ERROR'
        conditions.eventually {
            def status = getStatus(ingestionId)
            assert status.contains('COMPLETED_WITH_ERROR')
        }
    }

    @Category(Regression)
    def "Purchase Order with no site description gets filled in based on site names"() {
        given: 'A site with a description is ingested'

        ingestData(sampleSiteWithDesc, siteIngestionUrl)

        when: "A Purchase Order is ingested that does not contain a description"

        ingestData(samplePurchaseOrderNoDesc, purchaseOrderIngestionUrl)

        def purchaseOrderIds = getPurchaseOrderIds(samplePurchaseOrderNoDesc)
        def url = getURLPrefix + purchaseOrderIds[0]
        def purchaseOrderText = getHttpClient(header, url, null, HttpMethod.GET).body
        def purchaseOrderRetrieved = new JsonSlurper().parseText(purchaseOrderText).get('data').get(0)

        def mapper = new ObjectMapper()
        def received = mapper.readValue(groovy.json.JsonOutput.toJson(purchaseOrderRetrieved), PurchaseOrder.class)

        def purchaseOrderLineList = received.getPurchaseOrderLines().values()
        def purchaseOrderLine1 = purchaseOrderLineList.getAt(0)
        def purchaseOrderLine2 = purchaseOrderLineList.getAt(1)

        then:
        purchaseOrderLine1.getExtFields().get("shipToSiteDescription") == "Description to be copied"
        purchaseOrderLine1.getExtFields().get("shipFromSiteDescription") == "Description to be copied"
        purchaseOrderLine2.getExtFields().get("shipToSiteDescription") == "Description to be copied"
        purchaseOrderLine2.getExtFields().get("shipFromSiteDescription") == "Description to be copied"
    }

    // evaluate function and rename if necessary
    def "field in process model updates upon a new ingest"() {
        given: 'original purchase order ingested'
        ingestData(samplePurchaseOrder)
        def purchaseOrderIds = getPurchaseOrderIds(samplePurchaseOrder)
        def url = getURLPrefix + purchaseOrderIds[0]

        when: 'ingest updated purchase order'
        def modifiedSamplePurchaseOrder = samplePurchaseOrder.replaceAll("\"customerName\": \"IBM\"", "\"customerName\": \"Google\"")
        def expectedPurchaseOrder = getJsonAt(modifiedSamplePurchaseOrder, 0)

        ingestData(modifiedSamplePurchaseOrder)

        def purchaseOrderText = getHttpClient(header, url, null, HttpMethod.GET).body
        def purchaseOrderRetrieved = new JsonSlurper().parseText(purchaseOrderText).get('data').get(0)
        ['attributes', 'creationDate', 'id', 'lmd', 'maxDelayInHours', 'exceptionStatus', 'segment'].each {
            purchaseOrderRetrieved.remove(it)
        }
        ['creationDate', 'lmd', 'delayHours', 'lineExceptionStatus', 'purchaseOrderLines', 'segment'].each { key ->
            purchaseOrderRetrieved.get('purchaseOrderLines').values().each { it.remove(key) }
        }

        def mapper = new ObjectMapper()
        def expected = mapper.readValue(groovy.json.JsonOutput.toJson(expectedPurchaseOrder), PurchaseOrder)
        def received = mapper.readValue(groovy.json.JsonOutput.toJson(purchaseOrderRetrieved), PurchaseOrder)

        then: 'the process model field updated'
        received == expected
    }

    // evaluate function and rename if necessary
    def "field not in process model not updated upon a new ingest"() {
        given: 'original purchase order ingested'
        ingestData(samplePurchaseOrder)
        def purchaseOrderIds = getPurchaseOrderIds(samplePurchaseOrder)
        def url = getURLPrefix + purchaseOrderIds[0]

        when: 'ingest updated purchase order'
        def modifiedSamplePO = samplePurchaseOrder.replaceAll("\"invalidField\": invalidField", "\"invalidField\": invalidField")
        def expectedPO = getJsonAt(samplePurchaseOrder, 0)

        ingestData(modifiedSamplePO)

        def purchaseOrderText = getHttpClient(header, url, null, HttpMethod.GET).body
        def purchaseOrderRetrieved = new JsonSlurper().parseText(purchaseOrderText).get('data').get(0)
        ['attributes', 'creationDate', 'id', 'lmd', 'maxDelayInHours', 'exceptionStatus', 'segment'].each {
            purchaseOrderRetrieved.remove(it)
        }
        ['creationDate', 'lmd', 'delayHours', 'lineExceptionStatus', 'purchaseOrderLines', 'segment'].each { key ->
            purchaseOrderRetrieved.get('purchaseOrderLines').values().each { it.remove(key) }
        }

        def mapper = new ObjectMapper()
        def expected = mapper.readValue(groovy.json.JsonOutput.toJson(expectedPO), PurchaseOrder)
        def received = mapper.readValue(groovy.json.JsonOutput.toJson(purchaseOrderRetrieved), PurchaseOrder)

        then: 'non-process model field not updated'

        System.out.println("Expected:" + expected.toString())
        System.out.println("Actual:" + received.toString())
        received == expected
    }

//    def "Test purchase order computation"() {
//        given: 'Purchase Orders ingested completely'
//        def mapper = new ObjectMapper()
//
//        when: 'Ingesting data'
//        ingestData(purchaseOrderToIngest)
//
//        def purchaseOrderIds = getPurchaseOrderIds(purchaseOrderToIngest)
//        def url = getURLPrefix + purchaseOrderIds[0]
//        def purchaseOrderText = getHttpClient(header, url, null, HttpMethod.GET).body
//        def purchaseOrderRetrieved = new JsonSlurper().parseText(purchaseOrderText).get('data').get(0)
//        def received = mapper.readValue(groovy.json.JsonOutput.toJson(purchaseOrderRetrieved), PurchaseOrder)
//
//        def expectedQuantity = mapper.readTree(expectedQuantityJson)
//
//        then:
//        assert received != null
//        assert received.getPurchaseOrderLines() != null
//        assert received.getPurchaseOrderLines().size() == 2
//        received.getPurchaseOrderLines().values().each {
//            assert it.getExtField("totalConfirmedQuantity").toString() == expectedQuantity.get(it.getPoLineId()).get("confirmed").toString()
//            assert it.getExtField("totalShippedQuantity").toString() == expectedQuantity.get(it.getPoLineId()).get("shipped").toString()
//            assert it.getExtField("totalGrQuantity").toString() == expectedQuantity.get(it.getPoLineId()).get("gr").toString()
//            assert it.getExtField("totalOpenQuantity").toString() == expectedQuantity.get(it.getPoLineId()).get("open").toString()
//        }
//
//        where:
//        expectedQuantityJson                                                                                                                    | purchaseOrderToIngest
//        '{"TestPO1":{"gr":9.0,"open":20.0,"shipped":null,"confirmed":20.0},"TestPO2":{"gr":10.0,"open":12.0,"shipped":18.0,"confirmed":25.0}}'  | samplePurchaseOrder2
//        '{"TestPO1":{"gr":10.0,"open":20.0,"shipped":null,"confirmed":20.0},"TestPO2":{"gr":15.0,"open":10.0,"shipped":20.0,"confirmed":30.0}}' | samplePurchaseOrder3
//        '{"TestPO1":{"gr":13.0,"open":20.0,"shipped":null,"confirmed":15.0},"TestPO2":{"gr":15.0,"open":10.0,"shipped":20.0,"confirmed":30.0}}' | samplePurchaseOrder4
//        '{"TestPO1":{"gr":17.0,"open":20.0,"shipped":null,"confirmed":27.0},"TestPO2":{"gr":15.0,"open":10.0,"shipped":20.0,"confirmed":30.0}}' | samplePurchaseOrder5
//    }
//
//    def "Ingest a purchase order which triggers computation on the purchase order itself"() {
//
//        given: 'original purchase order ingested'
//        def compuationPO = this.getClass().getClassLoader().getResource('sample-purchase-order-computation-1.json').text
//
//        def purchaseOrderId = getPurchaseOrderIds(compuationPO)[0]
//        deleteObject(getURLPrefix, purchaseOrderId)
//
//        when: 'ingest purchase order'
//
//        ingestData(compuationPO)
//        def url = getURLPrefix + purchaseOrderId
//
//        def purchaseOrderText = getHttpClient(header, url, null, HttpMethod.GET).body
//        def purchaseOrderRetrieved = new JsonSlurper().parseText(purchaseOrderText)
//
//        then: 'verify that purchase order exception status is updated'
//
//        purchaseOrderRetrieved.getAt('data').getAt('exceptionStatus').toString() == '[Past Due]'
//        purchaseOrderRetrieved.getAt('data').getAt('purchaseOrderLines').getAt('00030').getAt('exceptionStatus').toString() == '[Past Due]'
//        purchaseOrderRetrieved.getAt('data').getAt('purchaseOrderLines').getAt('00020').getAt('exceptionStatus').toString() == '[Late]'
//        purchaseOrderRetrieved.getAt('data').getAt('purchaseOrderLines').getAt('00010').getAt('exceptionStatus').toString() == '[On Time]'
//    }
//
//    def "Ingest a purchase order which triggers computation on the sales order "() {
//
//        given: 'original salesorder and  purchase order ingested'
//        def compuationPO = this.getClass().getClassLoader().getResource('sample-purchase-order-sales-computation-2.json').text
//        def compuationSO = this.getClass().getClassLoader().getResource('sample-sales-order-computation-1.json').text
//
//        def purchaseOrderId = getPurchaseOrderIds(compuationPO)[0]
//        def salesOrderId = getSalesOrderIds(compuationSO)[0]
//        deleteObject(getURLPrefix, purchaseOrderId)
//        deleteObject(salesOrderGet, salesOrderId)
//
//        when: 'ingest sales and purchase order'
//
//        ingestionURL = salesOrderIngestionURL
//        ingestData(compuationSO)
//        ingestionURL = protocol + ip + purchaseOrderPath
//        ingestData(compuationPO)
//
//        def url = getURLPrefix + purchaseOrderId
//        def purchaseOrderText = getHttpClient(header, url, null, HttpMethod.GET).body
//        def purchaseOrderRetrieved = new JsonSlurper().parseText(purchaseOrderText)
//
//        url = salesOrderGet + salesOrderId
//        def salesOrderText = getHttpClient(header, url, null, HttpMethod.GET).body
//        def salesOrderRetrieved = new JsonSlurper().parseText(salesOrderText)
//
//        then: 'verify that purchase order and sales order exception status is updated'
//
//        salesOrderRetrieved.getAt('data').getAt('exceptionStatus').toString() == '[Late]'
//        salesOrderRetrieved.getAt('data').getAt('salesOrderLines').getAt('SL11').getAt('exceptionStatus').toString() == '[On Time]'
//        salesOrderRetrieved.getAt('data').getAt('salesOrderLines').getAt('SL12').getAt('exceptionStatus').toString() == '[Late]'
//        salesOrderRetrieved.getAt('data').getAt('salesOrderLines').getAt('SL13').getAt('exceptionStatus').toString() == '[Late]'
//
//        purchaseOrderRetrieved.getAt('data').getAt('exceptionStatus').toString() == '[Late]'
//        purchaseOrderRetrieved.getAt('data').getAt('purchaseOrderLines').getAt('POL21').getAt('exceptionStatus').toString() == '[On Time]'
//        purchaseOrderRetrieved.getAt('data').getAt('purchaseOrderLines').getAt('POL22').getAt('exceptionStatus').toString() == '[Late]'
//        purchaseOrderRetrieved.getAt('data').getAt('purchaseOrderLines').getAt('POL23').getAt('exceptionStatus').toString() == '[Late]'
//    }
//
//    def "Ingest a late purchase order which triggers alert service to generate alerts"() {
//        given: 'Purchase Order with exception status and site data'
//
//        ingestData(sampleSiteForAlert, siteIngestionUrl)
//        ingestData(sampleItemForAlert, itemIngestionUrl)
//
//        when: 'Ingesting data'
//        ingestData(purchaseOrderToIngest)
//
//        def purchaseOrderIds = getPurchaseOrderIds(purchaseOrderToIngest)
//
//        final Map<String, String> searchCriteria = new HashMap<>()
//        searchCriteria.put("limit", "1")
//        searchCriteria.put("query", "objectId=" + purchaseOrderIds[0])
//
//        def responseDef =
//                searchResponseJson(searchCriteria, searchAlertUrlPrefix, state, 60)
//        then:
//        assert responseDef != null
//        def alertDef = responseDef.get("data").getAt(0)
//        assert alertDef.get("objectId") != null
//        assert alertDef.get("objectId") == purchaseOrderIds[0]
//        assert alertDef.get("objectType") != null
//        assert alertDef.get("objectType") == 'PurchaseOrder'
//        assert alertDef.get("state") == state
//        assert alertDef.get("severity") == severity
//        assert alertDef.get("type") == 'Exception Status'
//        assert alertDef.get("objectState") == objectState
//        if (state == 'Open') {
//            assert alertDef.get("closedDate") == null
//        }
//        assert alertDef.get("alertStatusLmd") != null
//        if (checkForGreater) {
//            assert alertDef.get("alertStatusLmd") > previousStatusLmd
//        }
//        updatePreviousStatusLmd(alertDef.get("alertStatusLmd"))
//
//        def references = alertDef.get("refObjects")
//        assert references != null
//
//        assert references.get("shipToSites") != null
//        def shipToSite = references.get("shipToSites").get(0)
//        assert shipToSite.get("participantName").equals("Best Buys")
//        assert shipToSite.get("siteName").equals("New York")
//        assert shipToSite.get("siteOwnerName").equals("Best Buys New York Owner")
//
//        assert references.get("shipFromSites") != null
//        def shipFromSite = references.get("shipFromSites").get(0)
//        assert shipFromSite.get("participantName").equals("Samsung")
//        assert shipFromSite.get("siteName").equals("Cairo")
//        assert shipFromSite.get("siteOwnerName").equals("Samsung Cairo Owner")
//
//        assert references.get("customerItems") != null
//        def customerItem = references.get("customerItems").get(0)
//        assert customerItem.get("itemName").equals("Ship to item name")
//        assert customerItem.get("itemOwnerName").equals("Best Buys")
//
//        assert references.get("supplierItems") != null
//        def supplierItem = references.get("supplierItems").get(0)
//        supplierItem.get("itemName").equals("Ship from item name")
//        supplierItem.get("itemOwnerName").equals("Samsung")
//
//        where:
//        purchaseOrderToIngest   | state    | severity | exceptionStatus | checkForGreater | objectState
//        samplePOLate            | 'Open'   | 4        | 'Late'          | true            | 'Delivered'
//        samplePOUnknownStatus   | 'Open'   | 4        | 'Late'          | false           | 'Confirmed'
//        samplePOReceived        | 'Closed' | 4        | 'Late'          | true            | 'Received'
//        samplePOLate            | 'Open'   | 4        | 'Late'          | true            | 'Delivered'
//        samplePOOnTime          | 'Closed' | 1        | 'On Time'       | true            | 'Open'
//        samplePOPastDueShipment | 'Open'   | 5        | 'Past Due'      | true            | 'Planned Shipment'
//    }
}
