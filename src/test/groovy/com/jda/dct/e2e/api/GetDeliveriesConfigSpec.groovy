package com.jda.dct.e2e.api

import com.jda.dct.e2e.api.utility.AuthHelper
import groovyx.net.http.RESTClient
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll

/**
 * GET method to retrieve demand delivery list configuration
 *
 */

// this may be redundant we should check that it's being covered and then delete
class GetDeliveriesConfigSpec extends Specification implements AuthHelper {
    static deliveriesConfigURL
    @Shared
    def client

    def setupSpec() {
        init()

        def deliveriesConfigPath = properties.getProperty('getDeliveriesConfigPath')
        deliveriesConfigURL = protocol + ip + deliveriesConfigPath
        client = new RESTClient(deliveriesConfigURL)
        client.handler.failure = { resp -> resp }
        header[HttpHeaders.CONTENT_TYPE] = MediaType.APPLICATION_JSON
    }

    @Unroll
    def "Get deliveries configuration API #url"() {
        given: "The API URL is #url "
        when: "HTTP Method is GET: should return demand delivery list configuration"
        def response = client.get(headers: header)
        then: "the response should be 200"
        with(response) {
            status == 200
        }
        where:
        url << [deliveriesConfigURL]

    }
}