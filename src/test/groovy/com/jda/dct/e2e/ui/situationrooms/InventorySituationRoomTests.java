package com.jda.dct.e2e.ui.situationrooms;

import com.jda.dct.e2e.ui.utility.common.BaseTest;
import com.jda.dct.e2e.ui.utility.common.LeftMenu;
import com.jda.dct.e2e.ui.utility.helper.Browser.BrowserHelper;
import com.jda.dct.e2e.ui.utility.helper.Wait.WaitHelper;
import com.jda.dct.e2e.ui.utility.helper.genericHelper.GenericHelper;
import com.jda.dct.e2e.ui.utility.pageobjects.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


public class InventorySituationRoomTests extends BaseTest {

    private LeftMenu leftMenu;
    private TopMenuPage topMenu;
    private TopSubMenuPage topSubMenu;
    private SituationRoomPage situationRoom;
    private SupplyShipmentListPage supplyShipmentListPage;
    private SupplyShipmentDetailsPage supplyShipmentDetailsPage;
    private InventoryListPage inventoryListPage;
    private WaitHelper waitHelper;
    private GenericHelper genericHelper;
    BrowserHelper browserHelper;
    private DemandShipmentDetailsPage demandShipmentDetailsPage;
    private WebDriver driver;


    @BeforeClass
    public void setUp() throws InterruptedException {
        //Initialize required objects
        driver = getDriver();
        leftMenu = new LeftMenu(driver);
        topMenu = new TopMenuPage(driver);
        topSubMenu = new TopSubMenuPage(driver);
        situationRoom = new SituationRoomPage(driver);
        supplyShipmentListPage = new SupplyShipmentListPage(driver);
        supplyShipmentDetailsPage = new SupplyShipmentDetailsPage(driver);
        inventoryListPage = new InventoryListPage(driver);
        browserHelper = new BrowserHelper(driver);
        waitHelper = PageFactory.initElements(driver, WaitHelper.class);
        genericHelper = PageFactory.initElements(driver, GenericHelper.class);
        //loginAndNavigateToInventoryLandingPage
        logInToApplication();
    }

    @BeforeMethod
    public void commonMethods() throws InterruptedException {
        leftMenu.clickInventoryMenuOption();

    }

    @Test(priority = 1)
    public void validateIsSituationRoomMonetaryImpactisdisplayed() throws InterruptedException {
      String item ="AC-FG-033";
      topMenu.clickInventoryStockOut();
        waitHelper.waitForPageToLoad();
        topMenu.clearFilterIfVisible();
        topSubMenu.goToSearchFeature(item);
        waitHelper.waitForPageToLoad();
        inventoryListPage.clickSearchInventoryItemNumber(item);
        situationRoom.clickSituationRoomIcon();
        situationRoom.clickCreateRoomButton();
        situationRoom.enterSituationRoomName();
        situationRoom.enterRoomDescription("item recall");
        situationRoom.enterIssuetype();
        situationRoom.addUser1();
        situationRoom.enterItemNumber("MAEU597252617");
        situationRoom.clickCreate();
        Assert.assertTrue(situationRoom.isNewRoomCreated());
        Assert.assertTrue(situationRoom.isSituationMonetaryImpactDisplayed());
        situationRoom.clickmonetaryImpact();
        Thread.sleep(8000);
        Assert.assertTrue(situationRoom.isMonetaryImpactSalesSummaryDisplayed());
        situationRoom.clickmonetaryImpactDemand();
        Assert.assertTrue(situationRoom.isMonetaryImpactWeeklySummaryDisplayed());

    }
}

