package com.jda.dct.e2e.ui.pagination;

import com.jda.dct.e2e.ui.utility.common.BaseTest;
import com.jda.dct.e2e.ui.utility.common.LeftMenu;
import com.jda.dct.e2e.ui.utility.pageobjects.*;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class SupplyPaginationTests extends BaseTest {

    private LeftMenu leftMenu;
    private TopMenuPage topMenu;
    private TopSubMenuPage topSubMenu;
    private SupplyOrderListPage supplyOrderListPage;
    private SupplyDeliveryListPage supplyDeliveryListPage;
    private SupplyShipmentListPage supplyShipmentListPage;
    private SupplyForecastListPage supplyForecastListPage;
    private SupplyDeliveryListPage supplyDeliveryPage;
    private WebDriver driver;

    @BeforeClass
    public void setUp() throws InterruptedException {
        System.out.println("Starting tests for :" + this.getClass().getName());
//Initialize required objects
        driver = getDriver();
        leftMenu = new LeftMenu(driver);
        topMenu = new TopMenuPage(driver);
        topSubMenu = new TopSubMenuPage(driver);
        supplyOrderListPage = new SupplyOrderListPage(driver);
        supplyDeliveryListPage = new SupplyDeliveryListPage(driver);
        supplyShipmentListPage = new SupplyShipmentListPage(driver);
        supplyForecastListPage = new SupplyForecastListPage(driver);
        supplyDeliveryPage = new SupplyDeliveryListPage(driver);
        //loginAndNavigateToSupplyLandingPage
        logInToApplication();

    }

    @Test(priority = 1,retryAnalyzer = BaseTest.class)
    public void validateSupplyShipmentlistPaginationfeature() throws InterruptedException{
        leftMenu.clickSupplyMenuOption();
        topMenu.goToSupplyShipmentsTab();
        topMenu.clickExceptionsTab();
        topSubMenu.clickMaterialGroupDropdown();
        topSubMenu.clickFinishedGoodsMaterialType();
        supplyShipmentListPage.clickOceanIcon();
        supplyShipmentListPage.clickShipmentExceptionHavingData();
        topSubMenu.validatePagination();
        //Assertion is included in method

    }

    @Test(priority = 2,retryAnalyzer = BaseTest.class)
    public void validateSupplyDeliveryListPaginationFeature() throws InterruptedException{
        leftMenu.clickSupplyMenuOption();
        topMenu.goToSupplyDeliveriesTab();
        topMenu.clickExceptionsTab();
        supplyDeliveryPage.clickOceanIcon();
        topSubMenu.clickMaterialGroupDropdown();
        topSubMenu.clickFinishedGoodsMaterialType();
        supplyDeliveryPage.clickSupplyDeliveryExceptionsHavingData();
        topSubMenu.validatePagination();
        //Assertion is included in method
    }
}

