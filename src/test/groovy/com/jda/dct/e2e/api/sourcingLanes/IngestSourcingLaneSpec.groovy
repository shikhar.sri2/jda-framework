package com.jda.dct.e2e.api.sourcingLanes

import com.jda.dct.e2e.api.categories.Smoke
import com.jda.dct.e2e.api.utility.IngestionHelper
import groovy.json.JsonSlurper
import org.junit.experimental.categories.Category
import org.springframework.http.HttpMethod
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Stepwise
import spock.util.concurrent.PollingConditions

/**
 * End-to-end tests for {@code Site} ingestion.
 *
 * @version 1.0-SNAPSHOT
 * @since 01-June-2018
 */
@Stepwise
class IngestSourcingLaneSpec extends Specification implements IngestionHelper {

    @Shared sampleSourcingLane

    def setupSpec() {
        init()

        def sourcingLanePath = properties.getProperty('ingestSourcingLanesPath')
        def getPath = properties.getProperty('getSourcingLanesPath')

        ingestionURL = protocol + ip + sourcingLanePath
        getURLPrefix = protocol + ip + getPath

        sampleSourcingLane = this.getClass().getClassLoader().getResource('jsons/sample-sourcing-lane-ingest.json').text
    }

    @Category(Smoke)
    def "Get ingested sourcing lane back"() {
        given: 'sourcing lane ingested completely'
//        def sourcingLaneSent = getJsonAt(sampleSourcingLane, 1)
//        println sourcingLaneSent
        def sourcingLaneSent = executeHttpCall(header, getURLPrefix, null, HttpMethod.GET)["data"][0]
        println sourcingLaneSent
        println ingestionURL
        def response = executeHttpCall(header, ingestionURL, sourcingLaneSent, HttpMethod.POST)
//        ingestData(sampleSourcingLane,"sourcingLanes")
//        sleep(10_000L)
//        def sourcingLaneIds = getSourcingLaneIds(sampleSourcingLane)
        def sourcingLaneIds = sourcingLaneSent["id"]

        def url = getURLPrefix + sourcingLaneIds
        println url
        def sourcingLaneText = getHttpClient(header, url, null, HttpMethod.GET).body
        def sourcingLaneRetrieved = new JsonSlurper().parseText(sourcingLaneText).get('data').get(0)
//        ['creationDate', 'id', 'lmd','refObjects'].each { sourcingLaneRetrieved.remove(it) }

        expect:
        System.out.println("============= sourcingLaneSent: " + sourcingLaneSent.toString());
        System.out.println("============= sourcingLaneRetrieved: " + sourcingLaneRetrieved.toString());
        sourcingLaneRetrieved == sourcingLaneSent
    }

    // confirm that it no longer exists and delete
    def "Add a Note to a Sourcing Lane and get it back"() {
        given:
        def conditions = new PollingConditions(timeout: 120, initialDelay: 1)

        when: 'A Note is added to the Sourcing Lane'

        def sourcingLaneIds = getSourcingLaneIds(sampleSourcingLane)
        def url = getURLPrefix + 'notes/' + sourcingLaneIds[0]
        println url
        def sampleNote = '{ "note": "Sourcing Lane Note added"}'
        def response = getHttpClient(header, url, sampleNote, HttpMethod.POST)
        assert response.status == 200

        and: "Get the Sourcing Lane "

        url = getURLPrefix + sourcingLaneIds[0]
        def sourcingLaneText = getHttpClient(header, url, null, HttpMethod.GET).body
        def sourcingLaneRetrieved = new JsonSlurper().parseText(sourcingLaneText).get('data').get(0)

        then: 'Get the Sourcing Lane  back and verify the note'
        assert sourcingLaneRetrieved.getAt('notes').toString().contains("Sourcing Lane Note added") == true
    }
}
