package com.jda.dct.e2e.api.shipments

import com.jda.dct.e2e.api.categories.Regression
import com.jda.dct.e2e.api.utility.IngestionHelper
import org.junit.experimental.categories.Category
import spock.lang.Shared
import spock.lang.Specification


/**
 * Ingest shipments in CSV format.
 *
 * @author Vida Bandara
 * @version 1.0-SNAPSHOT
 * @since 20-May-2018
 */

class IngestCSVShipmentSpec extends Specification implements IngestionHelper {

    @Shared shipmentsURL

    void setup() {
        init()

        def shipmentPath = properties.getProperty('ingestShipmentsPath')
        shipmentsURL = protocol + ip + shipmentPath + '?processType=temperatureControlled&shipmentType=inboundShipment'
        println "Shipment URL: " + shipmentsURL
    }

    @Category(Regression)
    def "create and ingest a faulty shipment csv"() {
        given: "a faulty sample shipment, No of Containers is not a valid number"
        def filePath = getTmpDir() + 'sample_shipment.csv'
        new File(filePath).newWriter().withWriter {
            it << 'Shipment No,Process Type,Shipment Type,Ship Mode,No of Containers,Country of Origin,BOL No,Ship From,Ship To,Departure Port,Arrival Port,Carrier,Consignee Name,Consolidator Name,Consolidation Location,MBOL No,HAWB No,Booking No,Importer of Record No,Shipper Ref No,Vessel Name,Consignee Ref No,Freight Cost,Freight Terms,Creation Date,Planned Ship Date,Actual Ship Date,Last Known Milestone,Last Known Milestone Arrival Time,Last Known Milestone Departure Time,Next Known Milestone,Next Known Milestone PTA,Ship State,Exception Status,Ship Sub Status,ETA,Delivery Date,Last Modified Date,Line No,Line Mode,Container No,Container Type,Tracking No,No of Deliveries,Pallet No,Seal No,Trailer No,From,To,Line Exception Status,State,HAZMAT Code,Line Sub Status,Line Last Known Milestone,Line Next Known Milestone,PTA,Line ETA,Line Delivery Date,Demurrage test Date,Demurrage End Date,Total Packed Qty,Cargo Gross Wt,Creation-Date,Ship Date\n' +
                    'SHIP_OCN_001,temperatureControlled,inboundShipment,Ocean,20,China,121490123,CH01,US12,Guangzhou,Savannah,Crowley,Electrolux Vacuum Services,,,,,BKNG001,,SHP123,VS_001,,210150.45,EXW,4/29/18,4/30/18,4/30/18,Red Sea,5/30/18 10:05,5/30/18 13:00,Gulf of Suez,,In Transit,On Time,In Transit,6/27/18,,5/31/18,10,Ocean,CNTR123,40\',,10,,SL10H102,,CH01,US12,On Time,In Transit,,In Transit,Red Sea,Gulf of Suez,,6/27/18,,,,4320,43200,4/29/18,4/30/18\n' +
                    'SHIP_OCN_001,temperatureControlled,inboundShipment,Ocean,wrong_number,China,121490123,CH01,US12,Guangzhou,Savannah,Crowley,Electrolux Vacuum Services,,,,,BKNG001,,SHP123,VS_001,,210150.45,EXW,4/29/18,4/30/18,4/30/18,Red Sea,5/30/18 10:05,5/30/18 13:00,Gulf of Suez,,In Transit,On Time,In Transit,6/27/18,,5/31/18,20,Ocean,CNTR124,40\',,10,,SL10H103,,CH01,US12,On Time,In Transit,,In Transit,Red Sea,Gulf of Suez,,6/27/18,,,,4100,41000,4/29/18,4/30/18\n' +
                    'SHIP_OCN_001,temperatureControlled,inboundShipment,Ocean,20,China,121490123,CH01,US12,Guangzhou,Savannah,Crowley,Electrolux Vacuum Services,,,,,BKNG001,,SHP123,VS_001,,210150.45,EXW,4/29/18,4/30/18,4/30/18,Red Sea,5/30/18 11:00,5/30/18 13:00,Gulf of Suez,,In Transit,On Time,In Transit,6/27/18,,5/31/18,30,Ocean,CNTR125,40\',,10,,SL10H104,,CH01,US12,On Time,In Transit,,In Transit,Red Sea,Gulf of Suez,,6/27/18,,,,4210,42100,4/29/18,4/30/18\n'
        }

        when: "ingestion into DCT"
        def response = postFile(filePath, shipmentsURL, null)
        def ingestionId = getIngestionId(response)
        def status = waitUntilComplete(ingestionId)

        then: "should have status - completed_with_error"
        status.contains('COMPLETED_WITH_ERROR')
        /*getErrors(ingestionId) == [
                [
                        'rowId'       : 2,
                        'errorCode'   : 'dct.io.castError',
                        'errorMessage': 'No of Containers:wrong_number cannot be cast to long.'
                ]
        ]*/
    }

    @Category(Regression)
    def "duplicated header columns"() {
        given: "a faulty sample shipment with duplicated header columns"
        def filePath = getTmpDir() + 'sample_shipment.csv'
        new File(filePath).newWriter().withWriter {
            it << 'Shipment No,Process Type,Shipment Type,Ship Mode,No of Containers,Country of Origin,BOL No,Ship From,Ship To,Departure Port,Arrival Port,Carrier,Consignee Name,Consolidator Name,Consolidation Location,MBOL No,HAWB No,Booking No,Importer of Record No,Shipper Ref No,Vessel Name,Consignee Ref No,Freight Cost,Freight Terms,Creation Date,Planned Ship Date,Actual Ship Date,Last Known Milestone,Last Known Milestone Arrival Time,Last Known Milestone Departure Time,Next Known Milestone,Next Known Milestone PTA,Ship State,Exception Status,Ship Sub Status,ETA,Delivery Date,Last Modified Date,Line No,Line Mode,Container No,Container Type,Tracking No,No of Deliveries,Pallet No,Seal No,Trailer No,From,To,Line Exception Status,State,HAZMAT Code,Line Sub Status,Line Last Known Milestone,Line Next Known Milestone,PTA,Line ETA,Line Delivery Date,Demurrage test Date,Demurrage End Date,Total Packed Qty,Cargo Gross Wt,Ship Date,Ship Date\n' +
                    'SHIP_OCN_001,tempraturecontrolled,inboundShipment,Ocean,20,China,121490123,CH01,US12,Guangzhou,Savannah,Crowley,Electrolux Vacuum Services,,,,,BKNG001,,SHP123,VS_001,,210150.45,EXW,4/29/18,4/30/18,4/30/18,Red Sea,5/30/18 10:05,5/30/18 13:00,Gulf of Suez,,In Transit,On Time,In Transit,6/27/18,,5/31/18,10,Ocean,CNTR123,40\',,10,,SL10H102,,CH01,US12,On Time,In Transit,,In Transit,Red Sea,Gulf of Suez,,6/27/18,,,,4320,43200,4/29/18,4/30/18\n' +
                    'SHIP_OCN_001,tempraturecontrolled,inboundShipment,Ocean,20,China,121490123,CH01,US12,Guangzhou,Savannah,Crowley,Electrolux Vacuum Services,,,,,BKNG001,,SHP123,VS_001,,210150.45,EXW,4/29/18,4/30/18,4/30/18,Red Sea,5/30/18 10:05,5/30/18 13:00,Gulf of Suez,,In Transit,On Time,In Transit,6/27/18,,5/31/18,20,Ocean,CNTR124,40\',,10,,SL10H103,,CH01,US12,On Time,In Transit,,In Transit,Red Sea,Gulf of Suez,,6/27/18,,,,4100,41000,4/29/18,4/30/18\n' +
                    'SHIP_OCN_001,tempraturecontrolled,inboundShipment,Ocean,20,China,121490123,CH01,US12,Guangzhou,Savannah,Crowley,Electrolux Vacuum Services,,,,,BKNG001,,SHP123,VS_001,,210150.45,EXW,4/29/18,4/30/18,4/30/18,Red Sea,5/30/18 11:00,5/30/18 13:00,Gulf of Suez,,In Transit,On Time,In Transit,6/27/18,,5/31/18,30,Ocean,CNTR125,40\',,10,,SL10H104,,CH01,US12,On Time,In Transit,,In Transit,Red Sea,Gulf of Suez,,6/27/18,,,,4210,42100,4/29/18,4/30/18\n'
        }

        when: "ingestion into DCT"
        def response = postFile(filePath, shipmentsURL, null)
        def ingestionId = getIngestionId(response)
        def status = waitUntilComplete(ingestionId)

        then: "should have status - completed_with_error"
        status.contains('COMPLETED_WITH_ERROR')
        /*getErrors(ingestionId) == [
                [
                        rowId: 0,
                        errorCode: 'dct.io.internalError',
                        errorMessage: 'internal error.;The header contains a duplicate name: "Ship Date" in [Shipment No, Process Type, Shipment Type, Ship Mode, No of Containers, Country of Origin, BOL No, Ship From, Ship To, Departure Port, Arrival Port, Carrier, Consignee Name, Consolidator Name, Consolidation Location, MBOL No, HAWB No, Booking No, Importer of Record No, Shipper Ref No, Vessel Name, Consignee Ref No, Freight Cost, Freight Terms, Creation Date, Planned Ship Date, Actual Ship Date, Last Known Milestone, Last Known Milestone Arrival Time, Last Known Milestone Departure Time, Next Known Milestone, Next Known Milestone PTA, Ship State, Exception Status, Ship Sub Status, ETA, Delivery Date, Last Modified Date, Line No, Line Mode, Container No, Container Type, Tracking No, No of Deliveries, Pallet No, Seal No, Trailer No, From, To, Line Exception Status, State, HAZMAT Code, Line Sub Status, Line Last Known Milestone, Line Next Known Milestone, PTA, Line ETA, Line Delivery Date, Demurrage test Date, Demurrage End Date, Total Packed Qty, Cargo Gross Wt, Ship Date, Ship Date]'
                ],
                [
                        rowId:0,
                        errorMessage: 'The header contains a duplicate name: "Ship Date" in [Shipment No, Process Type, Shipment Type, Ship Mode, No of Containers, Country of Origin, BOL No, Ship From, Ship To, Departure Port, Arrival Port, Carrier, Consignee Name, Consolidator Name, Consolidation Location, MBOL No, HAWB No, Booking No, Importer of Record No, Shipper Ref No, Vessel Name, Consignee Ref No, Freight Cost, Freight Terms, Creation Date, Planned Ship Date, Actual Ship Date, Last Known Milestone, Last Known Milestone Arrival Time, Last Known Milestone Departure Time, Next Known Milestone, Next Known Milestone PTA, Ship State, Exception Status, Ship Sub Status, ETA, Delivery Date, Last Modified Date, Line No, Line Mode, Container No, Container Type, Tracking No, No of Deliveries, Pallet No, Seal No, Trailer No, From, To, Line Exception Status, State, HAZMAT Code, Line Sub Status, Line Last Known Milestone, Line Next Known Milestone, PTA, Line ETA, Line Delivery Date, Demurrage test Date, Demurrage End Date, Total Packed Qty, Cargo Gross Wt, Ship Date, Ship Date]'
                ]
        ]*/
    }

//    def "missing mandatory fields detected before persisting"() {
//        given: "a faulty sample shipment with duplicated header columns"
//        def filePath = getTmpDir() + 'sample_shipment.csv'
//        new File(filePath).newWriter().withWriter {
//            it << 'Shipment Number,Process Type,Shipment Type,Ship Mode,No of Containers,Country of Origin,BOL No,Ship From,Ship To,Departure Port,Arrival Port,Carrier,Consignee Name,Consolidator Name,Consolidation Location,MBOL No,HAWB No,Booking No,Importer of Record No,Shipper Ref No,Vessel Name,Consignee Ref No,Freight Cost,Freight Terms,Creation Date,Planned Ship Date,Actual Ship Date,Last Known Milestone,Last Known Milestone Arrival Time,Last Known Milestone Departure Time,Next Known Milestone,Next Known Milestone PTA,Ship State,Exception Status,Ship Sub Status,ETA,Delivery Date,Last Modified Date,Line No,Line Mode,Container No,Container Type,Tracking No,No of Deliveries,Pallet No,Seal No,Trailer No,From,To,Line Exception Status,State,HAZMAT Code,Line Sub Status,Line Last Known Milestone,Line Next Known Milestone,PTA,Line ETA,Line Delivery Date,Demurrage test Date,Demurrage End Date,Total Packed Qty,Cargo Gross Wt,Creation-Date,Ship Date\n' +
//                    'SHIP_OCN_001,tempraturecontrolled,inboundShipment,Ocean,20,China,121490123,CH01,US12,Guangzhou,Savannah,Crowley,Electrolux Vacuum Services,,,,,BKNG001,,SHP123,VS_001,,210150.45,EXW,4/29/18,4/30/18,4/30/18,Red Sea,5/30/18 10:05,5/30/18 13:00,Gulf of Suez,,In Transit,On Time,In Transit,6/27/18,,5/31/18,10,Ocean,CNTR123,40\',,10,,SL10H102,,CH01,US12,On Time,In Transit,,In Transit,Red Sea,Gulf of Suez,,6/27/18,,,,4320,43200,4/29/18,4/30/18\n' +
//                    'SHIP_OCN_001,tempraturecontrolled,inboundShipment,Ocean,20,China,121490123,CH01,US12,Guangzhou,Savannah,Crowley,Electrolux Vacuum Services,,,,,BKNG001,,SHP123,VS_001,,210150.45,EXW,4/29/18,4/30/18,4/30/18,Red Sea,5/30/18 10:05,5/30/18 13:00,Gulf of Suez,,In Transit,On Time,In Transit,6/27/18,,5/31/18,20,Ocean,CNTR124,40\',,10,,SL10H103,,CH01,US12,On Time,In Transit,,In Transit,Red Sea,Gulf of Suez,,6/27/18,,,,4100,41000,4/29/18,4/30/18\n' +
//                    'SHIP_OCN_001,tempraturecontrolled,inboundShipment,Ocean,20,China,121490123,CH01,US12,Guangzhou,Savannah,Crowley,Electrolux Vacuum Services,,,,,BKNG001,,SHP123,VS_001,,210150.45,EXW,4/29/18,4/30/18,4/30/18,Red Sea,5/30/18 11:00,5/30/18 13:00,Gulf of Suez,,In Transit,On Time,In Transit,6/27/18,,5/31/18,30,Ocean,CNTR125,40\',,10,,SL10H104,,CH01,US12,On Time,In Transit,,In Transit,Red Sea,Gulf of Suez,,6/27/18,,,,4210,42100,4/29/18,4/30/18\n'
//        }
//
//        when: "ingestion into DCT"
//        def response = postFile(filePath, shipmentsURL, null)
//        def ingestionId = getIngestionId(response)
//        def status = waitUntilComplete(ingestionId)
//
//        then: "should have status - completed_with_error"
//        status.contains('COMPLETED_WITH_ERROR')
//        getErrors(ingestionId) == [
//                [
//                        rowId:0,
//                        errorCode: 'dct.io.missingMandatoryFieldValue',
//                        errorMessage: 'Mandatory field:shipmentId value is missing.'
//                ]
//        ]
//    }

    // probably redundant
    @Category(Regression)
    def "ingest a large faulty shipment CSV file"() {
        given:
        def numShipments = 500
        def filePath = getTmpDir() + 'sample_shipment.csv'
        new File(filePath).newWriter().withWriter { writer ->
            writer << 'Shipment No,Process Type,Shipment Type,Ship Mode,No of Containers,Country of Origin,BOL No,Ship From,Ship To,Departure Port,Arrival Port,Carrier,Consignee Name,Consolidator Name,Consolidation Location,MBOL No,HAWB No,Booking No,Importer of Record No,Shipper Ref No,Vessel Name,Consignee Ref No,Freight Cost,Freight Terms,Creation Date,Planned Ship Date,Actual Ship Date,Last Known Milestone,Last Known Milestone Arrival Time,Last Known Milestone Departure Time,Next Known Milestone,Next Known Milestone PTA,Ship Status,Exception Status,Ship Sub Status,ETA,Delivery Date,Last Modified Date,Line No,Line Ship Mode,Container No,Container Type,Tracking No,No of Deliveries,Pallet No,Seal No,Trailer No,Line Ship From,Line Ship To,Line Exception Status,Status,HAZMAT Code,Line Sub Status,Line Last Known Milestone,Line Next Known Milestone,PTA,Line ETA,Line Delivery Date,Demurrage test Date,Demurrage End Date,Total Packed Qty,Cargo Gross Wt,Line Creation Date,Ship Date\n'
            def templateLine = '<shipment>,tempraturecontrolled,inboundShipment,Ocean,wrong_number,China,121490123,CH01,US12,Guangzhou,Savannah,Crowley,Electrolux Vacuum Services,,,,,BKNG001,,SHP123,VS_001,,210150.45,EXW,4/29/18,4/30/18,4/30/18,Red Sea,5/30/18 10:05,5/30/18 13:00,Gulf of Suez,,In Transit,On Time,In Transit,6/27/18,,5/31/18,10,Ocean,CNTR123,40\',,10,,SL10H102,,CH01,US12,On Time,In Transit,,In Transit,Red Sea,Gulf of Suez,,6/27/18,,,,4320,43200,4/29/18,4/30/18\n'
            numShipments.times { shipment ->
                writer << templateLine.replaceAll('<shipment>', ( 'SHIP_OCN_001' + shipment))
            }
        }

        when:
        def response = postFile(filePath, shipmentsURL, null)
        def ingestionId = getIngestionId(response)
        def status = waitUntilComplete(ingestionId, "ingesting:$ingestionId", 60)

        then:
        with(status) {
            !contains('COMPLETED')
            contains('COMPLETED_WITH_ERROR')
        }
        getStatusObjects(ingestionId).find { it.get('status') == 'COMPLETED_WITH_ERROR' }
                .get('numProcessed') == 0

    }

}