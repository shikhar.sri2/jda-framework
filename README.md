How to run e2e-testing locally:

**Prerequisite:**

Clone Foundation Project:

git clone https://"$userId"@stash.jda.com/scm/dct/foundation.git

Install Foundation Project

gradle clean install

Clone Tenants Project:

git clone https://"$userId"@stash.jda.com/scm/dct/tenants.git

Publish Tenants Artifact:

gradle clean

gradle zip artifactoryPublish


**How to Loading All Showcase Data:**

gradle --refresh-dependencies clean

gradle --refresh-dependencies unzip

gradle test -Dtest.single=LoadShowCaseSpec -DGATEWAY=${GATEWAY_IP}

For example: Loading showcase data into dev-ci environment:

gradle test -Dtest.single=LoadShowCaseSpec -DGATEWAY=104.211.60.65


**How to Run E2E API Testing:**

gradle clean apiTests -DGATEWAY=${GATEWAY_IP}

For example: Run e2e api tests in dev-ci environment:

gradle clean apiTests -DGATEWAY=104.211.60.65


**How to Run E2E UI Testing:**

*Run tests on docker image*:

docker-compose up

gradle clean uiTests -Dgeb.env=chrome -DwebURL=${webURL}

*Run tests on local web browser*:

goto GebConfig.groovy, uncomment line 46-47, which enable local chromedriver to be used

gradle clean uiTests -DwebURL=${webURL}

*For example: Run e2e ui tests in dev-ci environment*:

gradle clean uiTests -DwebURL=dctui-intergration.azurewebsites.net


**For more information about different environment:**

please click https://confluence.jda.com/display/DCT/CI-CD+Environments