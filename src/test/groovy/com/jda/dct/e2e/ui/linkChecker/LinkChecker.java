package com.jda.dct.e2e.ui.linkChecker;

import com.jda.dct.e2e.ui.utility.ExcelHelper.ExcelReaderForDataDrivenTest;
import com.jda.dct.e2e.ui.utility.action.LinkCheckerActions;
import com.jda.dct.e2e.ui.utility.common.BaseTest;
import com.jda.dct.e2e.ui.utility.helper.Wait.WaitHelper;
import com.jda.dct.e2e.ui.utility.DataProvider.TestDataProvider;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.*;

public class LinkChecker extends BaseTest {

    private WebDriver driver;
    private WaitHelper waitHelper;
    private LinkCheckerActions linkCheckerActions;
    ExcelReaderForDataDrivenTest excelReaderForDataDrivenTest;

    @BeforeClass
    public void setUp() throws InterruptedException {
        System.out.println("Starting tests for :" + this.getClass().getName());
        driver = getDriver();
        excelReaderForDataDrivenTest = new ExcelReaderForDataDrivenTest();
        HashMap filedetails = new HashMap<String, String>();
        filedetails.put("FileName", "LinkChecker/LinkChecker.xlsx");
        filedetails.put("SheetName", "Links");
        excelReaderForDataDrivenTest.setFilenameAndSheetName(filedetails);
        waitHelper = new WaitHelper(driver);
        linkCheckerActions = new LinkCheckerActions(driver);
        logInToApplication();
    }


    @Test(dataProvider = "TestRuns", dataProviderClass = TestDataProvider.class, retryAnalyzer = BaseTest.class)
    public void linkChecker(Map<String, String> data) {
        linkCheckerActions.navigateTo(data.get("Link"));
        Assert.assertTrue(linkCheckerActions.checkForLuminateIcon(), "Luminate Icon is displayed successfully");
        Assert.assertTrue(linkCheckerActions.ExtractJSLogs("SEVERE", removeLinks()),"No SEVERE logs found");
    }

    public List<String> removeLinks () {
        ArrayList<String> removeAPIs = new ArrayList<String>();
        //removeAPIs.add("/main");
        return removeAPIs;
    }

}
