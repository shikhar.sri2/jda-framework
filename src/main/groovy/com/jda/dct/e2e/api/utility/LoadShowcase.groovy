package com.jda.dct.e2e.api.utility

import org.apache.commons.lang.StringUtils

import java.util.concurrent.Callable
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.Future
import java.util.concurrent.TimeUnit


class LoadShowcase implements Callable, IngestionHelper, FetchFileHelper {

    File file
    List<File> allControlFiles
    boolean isDefaultTenant
    def header2

    def setup() {
        init()
    }

    LoadShowcase() {
    }

    LoadShowcase(boolean isDefaultTenant, def header2, File file, allControlFiles) {
        this.file = file
        this.allControlFiles = allControlFiles
        this.isDefaultTenant = isDefaultTenant
        this.header2 = isDefaultTenant ? null : header2
        this.setup()
    }

    List<Boolean> loadParallel(boolean isDefaultTenant, def header2, int numberOfThread, List<File> filesToIngest, List<File> allControlFiles) {
        this.allControlFiles = allControlFiles
        ExecutorService executor = Executors.newFixedThreadPool(numberOfThread)
        List<Future<Boolean>> futures = new ArrayList<>()
        for (File file : filesToIngest) {
            if (file == null) {
                continue
            }
            Future<Boolean> future = executor.submit(new LoadShowcase(isDefaultTenant, header2, file, allControlFiles))
            futures.add(future)
        }
        List<Boolean> results = new ArrayList<>()
        for (Future<Boolean> f: futures) {
            results.add(f.get())
        }
        executor.shutdown()
        executor.awaitTermination(1, TimeUnit.SECONDS)
        return results
    }

    List<Boolean> loadSequential(List<File> filesToIngest, String[] sequence,
                                 List<File> allControlFiles, int retry = 50) {
        this.allControlFiles = allControlFiles
        List<Boolean> ress = new ArrayList<>()
        Map<String, List<File>> linkedHashMap = getOrderedFiles(filesToIngest, sequence)
        for (String bo : linkedHashMap.keySet()) {
            List<File> filesOfTheBo = linkedHashMap.get(bo)
            List<Boolean> results = loadParallel(10, filesOfTheBo, allControlFiles)
            for (Boolean result : results) {
                int count = 0;
                while (!result && count < retry) {
                    sleep(1)
                    if (!result && ++count > retry) {
                        System.out.println("**************Attention!!! Files of " + bo + "is not finished.")
                    }
                }
                ress.add(result)
            }
        }
        return ress
    }

    private Map<String, List<File>> getOrderedFiles(List<File> filesToIngest, String[] sequence) {
        Map<String, List<File>> linkedHashMap = new LinkedHashMap<>()
        for (String s : sequence) {
            linkedHashMap.put(s, new ArrayList<>())
        }
        for (File file : filesToIngest) {
            if (file == null) {
                continue
            }
            Set<String> nameSet = new HashSet<>(Arrays.asList(getFileNameWordsArray(file.getName())))
            for (String key : linkedHashMap.keySet()) {
                if (StringUtils.isBlank(key)) {
                    continue
                }
                if (nameSet.contains(key)) {
                    linkedHashMap.get(key).add(file)
                }
            }
        }
        return linkedHashMap
    }

    @Override
    Boolean call() {
        def postUrl = getPostUrl(file.getName())
        def response = postFile(file, postUrl, header2)
        def ingestionId = getIngestionId(response)

        def status = getFinalStatus(ingestionId, file.getName(), header2)

        Boolean result = false
        if (status == 'COMPLETED') {
            File checkFile = getCheckFile(file, allControlFiles)
            QueryObjects queryObjects = new QueryHandler().process(checkFile)
            result = QueryResultHandler.getQueryResultHandler().executeQueryObjects(queryObjects)
        }
        return result
    }
}
