package com.jda.dct.e2e.api.deliveries


import com.jda.dct.e2e.api.utility.IngestionHelper
import groovy.json.JsonBuilder
import groovy.json.JsonSlurper
import net.sf.json.JSONObject
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.HttpMethod
import spock.util.concurrent.PollingConditions

class DeliveryShowcaseData implements IngestionHelper {

    private final Logger LOGGER = LoggerFactory.getLogger(DeliveryShowcaseData.class)

    String deliveryId

    String getDeliveryId() {
        return deliveryId
    }

    void setDeliveryId(String deliveryId) {
        this.deliveryId = deliveryId
    }

    DeliveryShowcaseData() {

    }

    def getDeliveryFromShowcase(boolean deliveryId, boolean deliveryType, value) {
        init()
        def getPath = properties.getProperty('getDeliveryPath')
        def getURLPrefix = protocol + ip + getPath
        String url = null
        if (deliveryId) {
            url = getURLPrefix + "?query=deliveryId=" + value
        } else if (deliveryType) {
            url = getURLPrefix + "?query=deliveryType=" + value
        }

        def conditions = new PollingConditions(timeout: 15, initialDelay: 1)
        def res = null
        conditions.eventually {
            res = executeHttpCall(header,url, null, HttpMethod.GET)
            assert res != null
        }
        if (res['data'] == null || res['data'].size() == 0) {
            LOGGER.warn("getDeliveryFromShowcase data return null")
        }
        String deliId = deliveryId ? value : (deliveryType ? res['data'][0]['deliveryId'] : null)
        setDeliveryId(deliId)
        return (JSONObject) (res['data'][0])
    }

    def modifyDeliveryMultiFields(boolean deliveryId, boolean deliveryType, value, Map<String, String> fields, String filename) {
        List<Object> deliveryList = new ArrayList<>()
        JSONObject delivery = getDeliveryFromShowcase(deliveryId, deliveryType, value)
        if (fields.isEmpty()) {
            return null
        }
        for (String field : fields.keySet()) {
            JSONObject jsonObject = delivery
            setValue(field, (String) (fields.get(field)), jsonObject)
        }
        new File(filename).write(new JsonBuilder(delivery).toPrettyString())
        def updatedDelivery =  new JsonSlurper().parseText(new File(filename).text)
        new File(filename).delete()
        deliveryList.add(updatedDelivery)
        return deliveryList
    }

    void setValue(String keys, String value, JSONObject jsonObject) {
        println("set key: " + keys + " with value " + value)
        String[] keyMain = keys.split("\\.")
        println("keyMain.length: " + keyMain.length)
        int level = 0
        while (level < keyMain.length) {
            println("level: " + level)
            String key = keyMain[level]
            if (level == keyMain.length - 1) {
                if (jsonObject.get(key) != null) {
                    println("set key value: " + key + ": " + value)
                    jsonObject.put(key, value)
                } else {
                    println("add additional field" + key + " with value " + value)
                    jsonObject.put(key, value)
                }
                return
            }
            if (jsonObject.optJSONObject(key) != null) {
                jsonObject = jsonObject.getJSONObject(key)
                level++
            } else {
                println("can not find nested json object for key " + key)
                return
            }
        }
    }

}
