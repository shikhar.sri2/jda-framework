package com.jda.dct.e2e.api.items

import com.jda.dct.e2e.api.utility.IngestionHelper
import groovy.json.JsonBuilder
import groovy.json.JsonSlurper
import net.sf.json.JSONObject
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.HttpMethod
import spock.util.concurrent.PollingConditions

class ItemShowcaseData implements IngestionHelper {

    private final Logger LOGGER = LoggerFactory.getLogger(ItemShowcaseData.class)

    ItemShowcaseData() {

    }

    def getItemFromShowcase(String itemName) {
        init()
        def getPath = properties.getProperty('getItemsPath')
        def getURLPrefix = protocol + ip + getPath
        def conditions = new PollingConditions(timeout: 15, initialDelay: 1)
        def res = null
        conditions.eventually {
            res = executeHttpCall(header, getURLPrefix + "query=itemName=" + itemName, null, HttpMethod.GET)
            assert res != null
        }
        if (res['data'] == null || res['data'].size() == 0) {
            LOGGER.warn("getItemFromShowcase data return null")
        }
        return (JSONObject) (res['data'][0])
    }

    def modifyItemMultiFields(String itemName, Map<String, String> fields, String filename) {
        List<Object> itemList = new ArrayList<>()
        JSONObject item = getItemFromShowcase(itemName)
        if (fields.isEmpty()) {
            return null
        }
        for (String field : fields.keySet()) {
            JSONObject jsonObject = item
            setValue(field, (String) (fields.get(field)), jsonObject)
        }
        new File(filename).write(new JsonBuilder(item).toPrettyString())
        def updatedItem =  new JsonSlurper().parseText(new File(filename).text)
        new File(filename).delete()
        itemList.add(updatedItem)
        return itemList
    }

    void setValue(String keys, String value, JSONObject jsonObject) {
        println("set key: " + keys + " with value " + value)
        String[] keyMain = keys.split("\\.")
        println("keyMain.length: " + keyMain.length)
        int level = 0
        while (level < keyMain.length) {
            println("level: " + level)
            String key = keyMain[level]
            println("key: " + key)
            if (level == keyMain.length - 1) {
                if (jsonObject.get(key) != null) {
                    println("set key value: " + key + ": " + value)
                    jsonObject.put(key, value)
                } else {
                    println("add additional field" + key + " with value " + value)
                    jsonObject.put(key, value)
                }
                return
            }
            if (jsonObject.optJSONObject(key) != null) {
                jsonObject = jsonObject.getJSONObject(key)
                level++
            } else {
                println("can not find nested json object for key " + key)
                return
            }
        }
    }
}
