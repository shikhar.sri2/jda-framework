package com.jda.dct.e2e.ui.utility.common;


import com.jda.dct.e2e.ui.utility.PropertyFile;

import com.jda.dct.e2e.ui.utility.helper.Resource.ResourceHelper;
import com.jda.dct.e2e.ui.utility.helper.Utility.UtilityHelper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.HashMap;


public class DriverHelper extends PropertyFile {
    private WebDriver driver;
    private Properties properties = getProperties();
    private String defaultDownloadPath = UtilityHelper.getProperty("testFramework","default.download.path");


    public WebDriver getDriver() {
        // System.out.println("Tests are being run for : " + driverName);
        if (driver == null) {
            String driverName = properties.getProperty("driver");
            if (driverName.equals("remote-chrome")) {
                driver = getRemoteChromeDriver();
            } else if (driverName.equals("chrome")) {
                driver = getChromeDriver();
            } else if (driverName.equals("firefox")) {
                driver = getFireFoxDriver();
            } else if (driverName.equals("IE")) {
                driver = getIEDriver();
            }
        }
        driver.get(String.format("https://%s", properties.getProperty("client.url")));
        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
        return driver;
    }

    private WebDriver getChromeDriver() {
        System.setProperty("webdriver.chrome.driver", properties.getProperty("driver.path"));
//        new ChromeOptions().addArguments("headless")
        HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
        chromePrefs.put("profile.default_content_settings.popups", 0);
        UtilityHelper.CreateOrCleanADirectory(defaultDownloadPath);
        chromePrefs.put("download.default_directory", ResourceHelper.getResourcePath(defaultDownloadPath));//change the download path
        ChromeOptions options = new ChromeOptions();
        options.setExperimentalOption("prefs", chromePrefs);
        WebDriver driver = new ChromeDriver(options);
        //WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();

        return driver;
    }

    private WebDriver getRemoteChromeDriver() {
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("headless");
        chromeOptions.addArguments("window-size=1920,1080");
        WebDriver driver = null;
        try {
            driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), chromeOptions);
        } catch (MalformedURLException exception) {
            System.err.println(exception.getMessage());
            System.err.println("How did you manage to break this?");
        }
        return driver;
    }

    private WebDriver getFireFoxDriver() {
        return null;
    }

    private WebDriver getIEDriver() {
        return null;
    }

    public void closeDriver() {
        driver.close();
    }

    public void quitDriver() {
        driver.quit();
        driver = null;
    }
}
