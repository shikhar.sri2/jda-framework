package com.jda.dct.e2e.ui.utility.pageobjects;

import com.jda.dct.e2e.ui.utility.action.CommonActions;
import com.jda.dct.e2e.ui.utility.common.BasePage;
import com.jda.dct.e2e.ui.utility.helper.Action.ActionHelper;
import com.jda.dct.e2e.ui.utility.helper.Wait.WaitHelper;
import com.jda.dct.e2e.ui.utility.helper.genericHelper.GenericHelper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;


public class DemandShipmentListPage extends BasePage {

    WebDriver driver;
    WebDriverWait webDriverWait;
    WaitHelper objWaitHelper;
    GenericHelper objGenericHelper;
    CommonActions commonActions;
    ActionHelper actionHelper;

    @FindBy(xpath = "//*[@id='PanelSummary-RenderedItem-Shipment-No-1-LinkItem']")
    private WebElement demandShipmentListpage;
    @FindBy(xpath = "//*[contains(@id,'shipment-details')]")
    private WebElement demandShipmentDetailsPage;
    @FindBy(xpath = "//*[@id='PanelSummary-RenderedItem-Shipment-No-1-LinkItem']")
    private WebElement firstDemandShipmentNumber;
    @FindBy(id = "ExpansionPanel-ExpandMoreIcon")
    private WebElement expandButton;
    @FindBy(id = "panelDetailSlider")
    private WebElement supplyLinePanelSlider;
    @FindBy(xpath = "//*[@id='PanelSummary-RenderedItem-Shipment-No-1']/following::a[@id='PanelSummary-RenderedItem-Container-No-1-LinkItem']")
    private WebElement containerNumber;
    @FindBy(xpath = "//*[@id='PanelSummary-RenderedItem-Trailer-No-1-LinkItem']")
    private WebElement trailerNumber;
    @FindBy(xpath="//*[@href='/demand/shipment']")
    private WebElement backToDemandShipmentPage;
    @FindBy(xpath = "(//div[@class='css-1uvyb57']/div/span/following-sibling::*)[1]")
    private WebElement firstFilter;
    @FindBy(xpath = "//*[contains(@id,'demand-outboundShipment-Filter-List-Card')]//span")
    private List<WebElement> shipmentExceptionStatusCount;
    @FindBy(xpath = "//*[contains(@id,'demand-outboundDelivery-Filter-List-Card')]/h4")
    private List<WebElement> deliveryExceptionStatusCount;
    @FindBy(xpath ="//a[text()='3']")
    private WebElement lotnumber;
    @FindBy(xpath = "//span[text()='L027']")
    private WebElement lotID;
    @FindBy(xpath = "//span[text()='L014, L015']")
    private WebElement lotIds;
    @FindBy(xpath = "//button[@value='Road']/span/div")
    private WebElement roadIcon;
    @FindBy(xpath ="//button[@value='Ocean']/span/div")
    private WebElement shipIcon;

    public DemandShipmentListPage(WebDriver webdriver) {

        this.driver = webdriver;
        PageFactory.initElements(driver, this);
        webDriverWait = new WebDriverWait(driver, 30);
        objWaitHelper = new WaitHelper(driver);
        objGenericHelper = new GenericHelper(driver);
        commonActions = new CommonActions(driver);
        actionHelper = new ActionHelper(driver);
    }


    //================ Methods========================


    //navigates to demand Shipment page by default

    //Assertion methods

    public boolean isDemandShipmentListPageDisplayed() {
        objWaitHelper.waitElementToBeVisible(demandShipmentListpage);
        return objGenericHelper.isDisplayed(demandShipmentListpage, "Demand Shipment list page");
    }

    public boolean isDemandShipmentDetailsPageDisplayed() {
        objWaitHelper.waitElementToBeVisible(demandShipmentDetailsPage);
        return objGenericHelper.isDisplayed(demandShipmentDetailsPage, "Demand Shipment detail page");
    }


    public boolean displayPanelslider() {

        return supplyLinePanelSlider.isDisplayed();
    }

    //click on first shipment number
    public void clickDemandShipmentNumber() {
        objWaitHelper.waitElementToBeVisible(expandButton);
        objGenericHelper.elementClick(firstDemandShipmentNumber, "First Shipment number");
    }

    public void clickBackToDemandShipmentPage() {
        objWaitHelper.waitElementToBeClickable(backToDemandShipmentPage);
        objGenericHelper.elementClick(backToDemandShipmentPage, "Demand Shipment page link");
    }

    //click on trailer number
    public void clickExpandButton() {
        objWaitHelper.waitElementToBeClickable(expandButton);
        objWaitHelper.waitAndClickElement(expandButton, "Expand button");
    }

    //click on container number
    public void clickDemandContainerNumber() {
        try {
            objWaitHelper.waitElementToBeClickable(expandButton);
            Thread.sleep(3000);
    //        actionHelper.moveToElement(containerNumber, "Container Number");
            objWaitHelper.waitAndClickElement(containerNumber, "Container Number");
        }catch (InterruptedException e){
            e.printStackTrace();
        }
    }

    //click on trailer number
    public void clickDemandTrailerNumber() {
       // objWaitHelper.waitElementToBeClickable(expandButton);
        objWaitHelper.waitElementToBeClickable(trailerNumber);
        actionHelper.moveToElement(trailerNumber, "Trailer Number");
        objGenericHelper.elementClick(trailerNumber, "Trail Number");
    }


    public void clickFirstFilter() {
        objWaitHelper.waitAndClickElement(firstFilter, "First Filter");
    }

    public void clickDemandShipmentExceptionsHavingData() {
        try {
            Thread.sleep(1000);
            commonActions.clickExceptionStatusHavingData(shipmentExceptionStatusCount);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void clickDeliveryExceptionsHavingData() {
        commonActions.clickExceptionStatusHavingData(deliveryExceptionStatusCount);
    }

    public boolean islotIdDisplayed() throws InterruptedException {

        if (lotID.isDisplayed()) {
            return true;
        } else return false;
    }



    public boolean islotIdsDisplayed() throws InterruptedException {

        if (lotIds.isDisplayed()) return true;
        else return false;
    }

    public boolean isLotValueDisplayed() throws InterruptedException {
        objWaitHelper.waitElementToBeVisible(lotnumber);
        return lotnumber.isDisplayed();
    }

    public void clickOceanIcon()  {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        objWaitHelper.waitAndClickElement(shipIcon, "Ship icon");
    }

}





