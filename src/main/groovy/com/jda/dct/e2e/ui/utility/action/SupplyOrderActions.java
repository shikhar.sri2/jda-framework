package com.jda.dct.e2e.ui.utility.action;


import com.jda.dct.e2e.ui.utility.common.BasePage;
import com.jda.dct.e2e.ui.utility.helper.Wait.WaitHelper;
import com.jda.dct.e2e.ui.utility.helper.genericHelper.GenericHelper;
import com.jda.dct.e2e.ui.utility.pageobjects.SupplyOrderDetailsPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import java.util.List;
import java.util.Map;

public class SupplyOrderActions extends BasePage {

    WebDriver driver;
    WaitHelper objWaitHelper;
    GenericHelper objGenericHelper;
    CommonActions commonActions;
    SupplyOrderDetailsPage supplyOrderDetailsPage;

    public SupplyOrderActions(WebDriver webdriver) {

        this.driver = webdriver;
        PageFactory.initElements(driver, this);
        objWaitHelper = new WaitHelper(driver);
        objGenericHelper = new GenericHelper(driver);
        commonActions = new CommonActions(driver);
        supplyOrderDetailsPage = new SupplyOrderDetailsPage(driver);
    }

    /**
     * This method verifies whether Total GR Qty is sum of Qty received of Received Receipt
     * @return true/false
     */
    public boolean verifyTotalGrQtyIsAdditionOfQtyReceivedOfAssociatedReceivedReceipt() {
        List<Map<String, String>> allReceiptDetails = supplyOrderDetailsPage.getAllReceiptDetails();
        String totalGrQty = supplyOrderDetailsPage.getTotalGRQTYValue();
        double totalGRQTYValue = commonActions.convertQtyFromStringToDouble(totalGrQty);
        double calculatedTotalGrValue = 0;
        for (int i = 0; i < allReceiptDetails.size(); i++) {
            String receiptNo = allReceiptDetails.get(i).get("Receipt No.");
            String receiptState = allReceiptDetails.get(i).get("Receipt State");
            String qtyReceived = allReceiptDetails.get(i).get("Quantity Received");
            Reporter("Receipt Details are : Receipt No.-" + receiptNo + ", Receipt State-"+ receiptState+ ", Quantity Received-"+ qtyReceived, "Info");
            if(receiptState.equalsIgnoreCase("Received")){
                Reporter("Receipt '" + receiptNo + "' is considered for calculation as state is 'Received'", "Info");
                calculatedTotalGrValue = calculatedTotalGrValue + commonActions.convertQtyFromStringToDouble(qtyReceived);
            }else{
                Reporter("Receipt '" + receiptNo + "' is not considered for calculation as state is '" + receiptState +"'", "Info");
            }
        }

        if(compareActualAndCalculatedTotalGrQty(totalGrQty,calculatedTotalGrValue)){
            Reporter("Total GR Qty displayed in UI '" + totalGRQTYValue + "' is approximately equal to Calculated GR Qty '" +
                    calculatedTotalGrValue+"'", "Pass");
            return true;
        }else {
            Reporter("Total GR Qty displayed in UI '" + totalGRQTYValue + "' is not equal to Calculated GR Qty '" +
                    calculatedTotalGrValue+"'", "Fail");
            return false;
        }
    }

    /**
     * This method compares Actual and Total Gr qty for equivalence
     * @param totalGrQty
     * @param calculatedTotalGrValue
     * @return
     */
    public boolean compareActualAndCalculatedTotalGrQty(String totalGrQty, double calculatedTotalGrValue){
        double totalGRQTYValue = commonActions.convertQtyFromStringToDouble(totalGrQty);
        String stringValue = totalGrQty;
        if (stringValue.contains("K")) {
            if (calculatedTotalGrValue > totalGRQTYValue && (calculatedTotalGrValue <= totalGRQTYValue + 100)) {
                return true;
            } else if (calculatedTotalGrValue < totalGRQTYValue && (calculatedTotalGrValue >= totalGRQTYValue - 100)) {
                return true;
            } else if (calculatedTotalGrValue == totalGRQTYValue) {
                return true;
            }else {
                return false;
            }
        }else{
            if(calculatedTotalGrValue == totalGRQTYValue){
                return true;
            }else {
                return false;
            }
        }
    }
}





