package com.jda.dct.e2e.ui.navigation;

import com.jda.dct.e2e.ui.utility.action.CommonActions;
import com.jda.dct.e2e.ui.utility.common.BaseTest;
import com.jda.dct.e2e.ui.utility.common.LeftMenu;
import com.jda.dct.e2e.ui.utility.helper.AssertHelper.AssertHelper;
import com.jda.dct.e2e.ui.utility.helper.Browser.BrowserHelper;
import com.jda.dct.e2e.ui.utility.pageobjects.*;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class DemandShipmentDetailsTests extends BaseTest {

    private LeftMenu leftMenu;
    private TopMenuPage topMenu;
    private TopSubMenuPage topSubMenu;
    private SupplyShipmentListPage supplyShipmentListPage;
    private DemandShipmentListPage demandShipmentListPage;
    private DemandShipmentDetailsPage demandShipmentDetailsPage;
    private CommonActions commonActions;
    private WebDriver driver;
    private BrowserHelper browserHelper;
    private AssertHelper assertHelper;


    @BeforeClass
    public void setUp() throws InterruptedException {
        System.out.println("Starting tests for :" + this.getClass().getName());

        //Initialize required objects
        driver = getDriver();
        leftMenu = new LeftMenu(driver);
        topMenu = new TopMenuPage(driver);
        topSubMenu = new TopSubMenuPage(driver);
        supplyShipmentListPage = new SupplyShipmentListPage(driver);
        demandShipmentListPage = new DemandShipmentListPage(driver);
        demandShipmentDetailsPage = new DemandShipmentDetailsPage(driver);
        browserHelper = new BrowserHelper(driver);
        assertHelper = new AssertHelper(driver);
        commonActions = new CommonActions(driver);
        //loginAndNavigateToDemandLandingPage
        logInToApplication();
    }

    @Test(priority = 1,retryAnalyzer = BaseTest.class)
    public void validateDemandDetailspageIsdisplayedWhenDemandShipmentnumberIsClicked() throws InterruptedException {
        leftMenu.clickDemandMenuOption();
        topMenu.goToDemandShipmentTab();
        topMenu.clearFilterIfVisible();
        demandShipmentListPage.clickOceanIcon();
        commonActions.disableAllExceptionFilter();
        demandShipmentListPage.clickDemandShipmentNumber();
        //verify user lands on demand ShipmentDetails page
        assertHelper.assertTrue(demandShipmentListPage.isDemandShipmentDetailsPageDisplayed(), "Demand Shipment details page is displayed successfully");
        demandShipmentListPage.clickBackToDemandShipmentPage();
        browserHelper.refresh();
    }
    //Shipment Notes

    @Test(priority = 2,retryAnalyzer = BaseTest.class)
    public void validateDemandShipmentDetailsPageIsDisplayedWhenNotesIsClicked()throws InterruptedException {
        leftMenu.clickDemandMenuOption();
        topMenu.goToDemandShipmentTab();
        demandShipmentListPage.clickOceanIcon();
        commonActions.disableAllExceptionFilter();
        demandShipmentListPage.clickDemandShipmentNumber();
        demandShipmentDetailsPage.clickNotes();
        //verify note tab is visible
        Assert.assertTrue(demandShipmentDetailsPage.isNotesPageDisplayed());
        browserHelper.refresh();
        //demandShipmentDetailsPage.clickBackToNotes();
        //verify that user lands on demand Shipment Details Page
        assertHelper.assertTrue(demandShipmentListPage.isDemandShipmentDetailsPageDisplayed(), "Demand Shipment Details page is displayed successfully");
        demandShipmentListPage.clickBackToDemandShipmentPage();
        browserHelper.refresh();
    }

    @Test(priority = 3,retryAnalyzer = BaseTest.class)
    public void validateDemandDeliveryListPageIsDisplayedWhenContainerNumberIsClicked() throws InterruptedException {
        leftMenu.clickDemandMenuOption();
        String shipmentNo = "OS0032_SD_S2";
        supplyShipmentListPage.clickOceanIcon();
        topSubMenu.clickSearchButton();
        topSubMenu.goToSearchFeature(shipmentNo);
        commonActions.disableAllExceptionFilter();
        //verify that user lands on demand Shipment Details Page
        // Assert.assertTrue(demandShipmentDetailsPage.isSearchShipmentNumberDisplayed(shipmentNo));
        demandShipmentListPage.clickExpandButton();
        demandShipmentListPage.clickDemandContainerNumber();
        //verify that user lands on SupplyDeliveryList page
        assertHelper.assertTrue(demandShipmentDetailsPage.isDemandDeliveryPageDisplayed(), "Demand Delivery page is displayed successfully");
        topMenu.goToDemandShipmentTab();
        topMenu.clearFilter();
        browserHelper.refresh();
    }


    //validating click on trailer button on Shipment list page

    @Test(priority = 4,retryAnalyzer = BaseTest.class)
    public void validateDemandDeliveryListPageIsDisplayedWhenTrailerNumberIsClicked() throws InterruptedException {
        leftMenu.clickDemandMenuOption();
        String shipmentNo = "OS0038_SD_S1";
        topMenu.clearFilterIfVisible();
        supplyShipmentListPage.clickRoadIcon();
        topSubMenu.clickSearchButton();
        topSubMenu.goToSearchFeature(shipmentNo);
        commonActions.disableAllExceptionFilter();
        //verify that user lands on demand Shipment Details Page
        // Assert.assertTrue(demandShipmentDetailsPage.isSearchShipmentNumberDisplayed(shipmentNo));
        demandShipmentListPage.clickExpandButton();
        demandShipmentListPage.clickDemandTrailerNumber();
        //verify that user lands on SupplyDeliveryDetails page
        assertHelper.assertTrue(demandShipmentDetailsPage.isDemandDeliveryPageDisplayed(), "Demand Delivery Page is displayed successfully");
        topMenu.goToDemandShipmentTab();
        topMenu.clearFilter();
        browserHelper.refresh();
    }

    //demand Shipment all Orders
    @Test(priority = 5,retryAnalyzer = BaseTest.class)
    public void validateDemandOrderListPageIsDisplayedWhenDemandShipmentAllordersIsClicked()throws InterruptedException {
        leftMenu.clickDemandMenuOption();
        topMenu.goToDemandShipmentTab();
        topMenu.clearFilterIfVisible();
        demandShipmentListPage.clickOceanIcon();
        commonActions.disableAllExceptionFilter();
        demandShipmentListPage.clickDemandShipmentNumber();
  //verify that user lands on demand Shipment Details Page
        Assert.assertTrue(demandShipmentListPage.isDemandShipmentDetailsPageDisplayed());
        demandShipmentDetailsPage.clickShipmentAllOrders();
//verify that user lands on demand Order List page
        assertHelper.assertTrue(demandShipmentDetailsPage.isDemandOrderPageDisplayed(), "Demand Order List page is displayed successfully");
       browserHelper.refresh();
    }


    // demand Shipment Hot Orders

    @Test(priority = 6,retryAnalyzer = BaseTest.class)
    public void validateDemandOrderListPageIsDisplayedWhenDemandShipmentHotOrdersIsClicked()throws InterruptedException {
        leftMenu.clickDemandMenuOption();
        topMenu.goToDemandShipmentTab();
        demandShipmentListPage.clickOceanIcon();
        commonActions.disableAllExceptionFilter();
        demandShipmentListPage.clickDemandShipmentNumber();
        //verify that user lands on demand Shipment Details Page
        Assert.assertTrue(demandShipmentListPage.isDemandShipmentDetailsPageDisplayed());
        demandShipmentDetailsPage.clickShipmentHotorders();
//verify that user lands on demand Order List page
        assertHelper.assertTrue(demandShipmentDetailsPage.isDemandOrderPageDisplayed(), "Demand Order page is displayed successfully");
        //driver.navigate().refresh();
    }


//testcases to validate object navigation inside a demand container


    //Shipment Line All Orders
    @Test(priority = 7,retryAnalyzer = BaseTest.class)
    public void validateDemandOrderListPageIsDisplayedWhenShipmentLineAllordersIsClicked() throws InterruptedException{
        leftMenu.clickDemandMenuOption();
        topMenu.goToDemandShipmentTab();
        demandShipmentListPage.clickOceanIcon();
        commonActions.disableAllExceptionFilter();
        demandShipmentListPage.clickDemandShipmentNumber();
        //verify that user lands on demand Shipment Details Page
        assertHelper.assertTrue(demandShipmentListPage.isDemandShipmentDetailsPageDisplayed(), "Demand Shipment List page is displayed successfully");
        // click on demand shipment container All Order
        demandShipmentDetailsPage.clickContainerAllorders();
        //verify that user lands on demand Order List page
        assertHelper.assertTrue(demandShipmentDetailsPage.isDemandOrderPageDisplayed(), "Demand Order page is displayed successfully");
        //driver.navigate().refresh();
    }


    /**
     *  Shipment Line Hot Orders
     */
    @Test(priority = 8,retryAnalyzer = BaseTest.class)
    public void validateDemandOrderListPageIsDisplayedWhenShipmentLineHotordersIsClicked() throws InterruptedException{
        leftMenu.clickDemandMenuOption();
        topMenu.goToDemandShipmentTab();
        demandShipmentListPage.clickOceanIcon();
        commonActions.disableAllExceptionFilter();
        //click the first shipment number
        demandShipmentListPage.clickDemandShipmentNumber();
        //verify that user lands on demand Shipment Details Page
        assertHelper.assertTrue(demandShipmentListPage.isDemandShipmentDetailsPageDisplayed(), "Demand Order List page is displayed successfully");
        // click on demand shipment container HotOrder
        demandShipmentDetailsPage.clickContainerHotorders();
        //verify that user lands on demand Order List page
        assertHelper.assertTrue(demandShipmentDetailsPage.isDemandOrderPageDisplayed(), "Demand Order List page is displayed successfully");
        //driver.navigate().refresh();
    }

    @Test(priority = 9,retryAnalyzer = BaseTest.class)
    public void validateDeliveryListPageIsDisplayedWhenShipmentLineIsClicked()throws InterruptedException{
        leftMenu.clickDemandMenuOption();
        topMenu.goToDemandShipmentTab();
        String shipmentNo = "OS0032_SD_S2";
        topMenu.clearFilterIfVisible();
        supplyShipmentListPage.clickOceanIcon();
//-->search the shipment no that has conatiner number
        topSubMenu.clickSearchButton();
        topSubMenu.goToSearchFeature(shipmentNo);
        commonActions.disableAllExceptionFilter();
        //verify that user lands on demand Shipment Details Page
       // Assert.assertTrue(demandShipmentDetailsPage.isSearchShipmentNumberDisplayed(shipmentNo));
//click on demand shipment container trailer number
        demandShipmentDetailsPage.clickSearchedShipmentNumber(shipmentNo);
        demandShipmentDetailsPage.clickContainerNumber();
//verify that user lands on SupplyDeliverList page
        assertHelper.assertTrue(demandShipmentDetailsPage.isDemandDeliveryPageDisplayed(), "Demand Delivery List page is displayed successfully");
        topMenu.goToDemandShipmentTab();
        topMenu.clearFilter();
        browserHelper.refresh();
    }
}
