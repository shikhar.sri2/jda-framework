package com.jda.dct.e2e.ui.utility.common;


import com.jda.dct.e2e.ui.utility.PropertyFile;
import com.jda.dct.e2e.ui.utility.helper.Resource.ResourceHelper;
import com.jda.dct.e2e.ui.utility.helper.Utility.UtilityHelper;
import com.jda.dct.e2e.ui.utility.pageobjects.LoginPage;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.*;

import java.io.File;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class BaseTest extends PropertyFile implements IRetryAnalyzer {

    public static ExtentReports extent = new ExtentReports();
    public static ExtentTest xtReportLog = null;
    static ExtentTest parentLog = null;
    static ExtentHtmlReporter htmlReporter = null;
    public Logger log = Logger.getLogger(BaseTest.class);
    public String resultsDir;
    public String screenshotPath;

    public Properties properties = getProperties();
    private WebDriver webDriver;
    private DriverHelper driverHelper = new DriverHelper();
    public static Map<String, String> data = new HashMap<>();
    public static List<Map<String, String>> dataItem = new ArrayList<>();
    public static int currentData = 0;
    public static int dataCounter = 0;
    public static String previousMethodName;

    private static int retryCnt = 0, maxRetryCnt = Integer.valueOf(UtilityHelper.getProperty("testFramework", "max.test.retry.count"));

    public WebDriver getDriver() {
        webDriver = driverHelper.getDriver();
        return webDriver;
    }

    public void logInToApplication() {
        webDriver.manage().deleteAllCookies();
        LoginPage loginPage = new LoginPage(webDriver);
//        loginPage.login(System.getProperty("userName"), System.getProperty("password"));
        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        loginPage.login(System.getProperty("client.user"), System.getProperty("client.password"));
    }

    @BeforeSuite
    public void initSuite() {
        PropertyConfigurator.configure((ResourceHelper.getBaseResourcePath() + "\\src\\main\\resources\\" + "log4j.properties"));
        String sdft = new SimpleDateFormat("YYYY-MM-dd_HHmmss").format(new Date());
        resultsDir =  UtilityHelper.getProperty("testFramework","test.artifacts.path") + File.separator + "ExecutionReports" + File.separator + "Results_" + sdft;
        UtilityHelper.CreateADirectory(resultsDir);
        screenshotPath =  UtilityHelper.getProperty("testFramework","test.artifacts.path") + File.separator + "ScreenCapture" + File.separator + "SC_" + sdft;
        UtilityHelper.CreateADirectory(screenshotPath);
        htmlReporter = new ExtentHtmlReporter(ResourceHelper.getResourcePath(File.separator + resultsDir + File.separator + "TestReport_" + sdft + ".html"));
        htmlReporter.loadXMLConfig(ResourceHelper.getBaseResourcePath() + "\\src\\main\\resources\\" + "extent-config.xml");
        htmlReporter.config().setAutoCreateRelativePathMedia(true);
        extent.attachReporter(htmlReporter);
        extent.setSystemInfo("User Name", System.getProperty("user.name"));
        extent.setSystemInfo("OS", System.getProperty("os.name"));
        extent.setSystemInfo("Environment", properties.getProperty("client.url"));
        extent.setSystemInfo("Browser", properties.getProperty("driver"));
        extent.setSystemInfo("Host Name", "LCT_JDA_Automation_Team");
        extent.setReportUsesManualConfiguration(true);
        List statusHierarchy = Arrays.asList(Status.SKIP, Status.ERROR, Status.FAIL, Status.FATAL, Status.WARNING, Status.PASS,
                Status.DEBUG, Status.INFO);

        extent.config().statusConfigurator().setStatusHierarchy(statusHierarchy);
        retryCnt++;
    }

    @BeforeClass
    public void beforeClass() {
        try {
            parentLog = extent.createTest((this.getClass().getSimpleName()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        currentData = 0;
    }


    @BeforeMethod()
    public void beforeMethod(Method method) {
        try {
            if (dataItem.size() != 0 || !dataItem.isEmpty()) {
                data.putAll(dataItem.get(currentData++));
                dataCounter = dataItem.size();
            }
            log.info("******************************************************************************");
            log.info("*********" + this.getClass().getPackage().toString()
                    .substring(this.getClass().getPackage().toString().lastIndexOf(".") + 1) + "::"
                    + this.getClass().getSimpleName() + "::" + method.getName() + "*********");
            if (currentData == 1 || currentData == 0) {
                previousMethodName = method.getName();
                xtReportLog = parentLog.createNode(method.getName());
            }

            if (dataCounter != 0 && !method.getName().equals(previousMethodName)) {
                xtReportLog = parentLog.createNode(method.getName());
            }

            if (dataCounter != 0 && dataCounter >= 2)
                xtReportLog = xtReportLog.createNode(data.get("TestDescription"))
                        .assignCategory(this.getClass().getPackage().toString()
                                .substring(this.getClass().getPackage().toString().lastIndexOf(".") + 1));
            else {
                xtReportLog = xtReportLog.assignCategory(this.getClass().getPackage().toString()
                        .substring(this.getClass().getPackage().toString().lastIndexOf(".") + 1));
            }
            previousMethodName = method.getName();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Exception in Before Method" + e.getMessage());
        }
    }

    @AfterMethod()
    public void afterMethod(ITestResult result) {
        try {
            getresult(result);
            extent.flush();
        } catch (Exception e) {
            Reporter("Exception in @AfterMethod: " + e, "Fail", log);
        }
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() {
        tearDownDriver();
        if (dataItem.size() != 0 || !dataItem.isEmpty()) {
            dataItem.clear();
            dataCounter = 0;
            data.clear();
        }
    }

    @AfterSuite
    public void cleanUp() {
        // If additional activities are required after Suite is complete
    }

    public void tearDownDriver() {
        if (((RemoteWebDriver) webDriver).getSessionId() != null) {
            System.out.println("Tearing down driver...." + ((RemoteWebDriver) webDriver).getSessionId().toString());
            driverHelper.quitDriver();
        }
    }

    public String getSystemTime() {
        return "Current time :" + new SimpleDateFormat("HH:mm:ss:SSS").format((Calendar.getInstance()).getTime());
    }

    /**
     * This method is used for reporting in extent report
     *
     * @param text
     * @param status
     */
    public void Reporter(String text, String status) {
        try {
            if (xtReportLog == null) {
                if (parentLog != null) {
                    xtReportLog = parentLog;
                } else {
                    throw new RuntimeException("Reporter log is null :");
                }
            }
            if (status.equalsIgnoreCase("Pass")) {
                xtReportLog.log(Status.PASS, text);
                log.info(text);
            } else if (status.equalsIgnoreCase("Fail")) {
                xtReportLog.log(Status.FAIL, text);
                log.error(text);
            } else if (status.equalsIgnoreCase("Skip")) {
                xtReportLog.log(Status.SKIP, text);
                log.debug(text);
            } else if (status.equalsIgnoreCase("Fatal")) {
                xtReportLog.log(Status.ERROR, text);
                log.fatal(text);
            } else if (status.equalsIgnoreCase("Error")) {
                xtReportLog.log(Status.ERROR, text);
                log.fatal(text);
            } else {
                xtReportLog.log(Status.INFO, text);
                log.info(text);
            }
            Reporter.log(text);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void Reporter(String text, String status, Logger log) {
        try {
            if (xtReportLog == null) {
                if (parentLog != null) {
                    xtReportLog = parentLog;
                } else {
                    throw new RuntimeException("Reporter log is null :");
                }
            }
            if (status.equalsIgnoreCase("Pass")) {
                xtReportLog.log(Status.PASS, text);
                log.info(text);
            } else if (status.equalsIgnoreCase("Fail")) {
                xtReportLog.log(Status.FAIL, text);
                log.error(text);
            } else if (status.equalsIgnoreCase("Skip")) {
                xtReportLog.log(Status.SKIP, text);
                log.debug(text);
            } else if (status.equalsIgnoreCase("Fatal")) {
                xtReportLog.log(Status.ERROR, text);
                log.fatal(text);
            } else if (status.equalsIgnoreCase("Error")) {
                xtReportLog.log(Status.ERROR, text);
                log.fatal(text);
            } else {
                xtReportLog.log(Status.INFO, text);
                log.info(text);
            }
            Reporter.log(text);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void getresult(ITestResult result) {
        try {
            File screenShotName;
            String sdft = new SimpleDateFormat("YYYY-MM-dd_HHmmss").format(new Date());
            if (result.getStatus() == ITestResult.SUCCESS) {
                xtReportLog.log(Status.PASS, result.getName() + " test is pass");
            } else if (result.getStatus() == ITestResult.SKIP) {
                log.debug(result.getName() + " test is skipped and skip reason is:-" + result.getThrowable());
            } else if (result.getStatus() == ITestResult.FAILURE) {
                String scrPath = takeScreenShot(result.getName() + "_Fail");
                xtReportLog.fail(result.getName() + " test is failed",
                        MediaEntityBuilder.createScreenCaptureFromPath(scrPath).build());
                log.error(result.getName() + " test is failed" + result.getThrowable());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String takeScreenShot(String name) {
        try {
            File screenShotName;
            File scrFile = ((TakesScreenshot) webDriver).getScreenshotAs(OutputType.FILE);
            String scrPath = ResourceHelper.getBaseResourcePath() + File.separator + screenshotPath + File.separator + name + ".png";
            screenShotName = new File(scrPath);
            FileUtils.copyFile(scrFile, screenShotName);
            String filePath = screenShotName.toString();
            return filePath;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * This method is used to re run failed test cases
     *
     * @param result
     * @return
     */
    public boolean retry(ITestResult result) {
        if (retryCnt < maxRetryCnt) {
            Reporter("Retrying " + result.getName() + " again. Please ignore this test.", "Skip");
            retryCnt++;
            if (currentData == 1) {
                dataItem.clear();
            }
            currentData--;
            return true;
        }
        return false;
    }

    /**
     * This method gets data specified in excel
     *
     * @param key
     * @return String
     */
    public String getData(String key) {
        return data.get(key);
    }

    /**
     * This method returns Instance URL
     *
     * @return
     */
    public String getInstanceURL() {
        String url = String.format("https://%s", properties.getProperty("client.url"));
        return url;
    }
}
