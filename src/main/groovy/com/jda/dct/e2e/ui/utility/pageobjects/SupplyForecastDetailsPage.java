package com.jda.dct.e2e.ui.utility.pageobjects;


import com.jda.dct.e2e.ui.utility.helper.Wait.WaitHelper;
import com.jda.dct.e2e.ui.utility.helper.genericHelper.GenericHelper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SupplyForecastDetailsPage {
    WebDriver driver;
    WebDriverWait webDriverWait;
    WaitHelper waitHelper;
    GenericHelper genericHelper;

    public SupplyForecastDetailsPage(WebDriver webdriver) {

        this.driver = webdriver;
        PageFactory.initElements(driver, this);
        webDriverWait = new WebDriverWait(driver, 30);
        waitHelper = new WaitHelper(driver);
        genericHelper = new GenericHelper(driver);
    }

    @FindBy(id ="PanelSummary-RenderedItem-Item-0-LinkItem")
    private WebElement forecastDetailItemNumber;
    @FindBy(id ="breadcrumb-inventory-details")
    private WebElement inventoryDetailsPageFromForecastPage;
    @FindBy(xpath = "//div[contains(text(),'Forecast')]")
    private WebElement forecast;
    @FindBy(xpath = "//div[contains(text(),'Commits')]")
    private WebElement commits;
    @FindBy(id ="PanelSummary-RenderedItem-26-Week Forecast-0-LinkItem")
    private WebElement weekForecast;

   public void clickForecastDetailsItemNumber(){
       waitHelper.waitAndClickElement(forecastDetailItemNumber, "Detail page Item number");
}

    public boolean isMeasureForecastDisplayed() {
       waitHelper.waitElementToBeVisible(forecast);
        return genericHelper.isDisplayed(forecast, "Forecast measure");
    }

    public boolean isMeasureCommitDisplayed() {
        waitHelper.waitElementToBeVisible(commits);
        return genericHelper.isDisplayed(commits, "Commits measure");
    }



    public boolean isInventoryDetailsPageDisplayed() {
       waitHelper.waitElementToBeVisible(inventoryDetailsPageFromForecastPage);
        return genericHelper.isDisplayed(inventoryDetailsPageFromForecastPage, "Inventory details page");
    }

    public boolean isWeeklyForecastDisplayed() {
        waitHelper.waitElementToBeVisible(weekForecast);
        return genericHelper.isDisplayed(weekForecast, "26weekForecastdisplayed");
    }



}